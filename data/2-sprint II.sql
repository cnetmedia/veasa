--En la tabla bitácora se agrego el campo bi_accion varchar 20 y se modifico el  bi_idasociado a varchar 20
ALTER TABLE bitacora ADD bi_accion varchar(20);
ALTER TABLE bitacora ADD bi_idasociado varchar(20);

--En la tabla pedido_proveedores se agrego el campo emp_id_aprobado_dept_prod  int(11) y  fecha_aprobado_dept_prod
ALTER TABLE pedidos_proveedores ADD emp_id_aprobado_dept_prod INT;
ALTER TABLE pedidos_proveedores ADD fecha_aprobado_dept_prod DATETIME;

