ALTER TABLE `productos_almacenes` CHANGE product_cantidad product_cantidad_int INTEGER;
ALTER TABLE `productos_almacenes` ADD product_cantidad float(9,2) NOT NULL DEFAULT 0;
UPDATE `productos_almacenes` SET product_cantidad=product_cantidad_int;
ALTER TABLE `productos_almacenes` DROP product_cantidad_int;

ALTER TABLE `productos_reciclados_almacenes` CHANGE product_cantidad product_cantidad_int INTEGER;
ALTER TABLE `productos_reciclados_almacenes` ADD product_cantidad float(9,2) NOT NULL DEFAULT 0;
UPDATE `productos_reciclados_almacenes` SET product_cantidad=product_cantidad_int;
ALTER TABLE `productos_reciclados_almacenes` DROP product_cantidad_int;