/*TABLA BITACORA*/
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `bitacora` (
  `bi_id` bigint(20) UNSIGNED NOT NULL,
  `bi_tipo` varchar(2) NOT NULL,
  `bi_idasociado` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `bi_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `bitacora`
  ADD UNIQUE KEY `bi_id` (`bi_id`);


ALTER TABLE `bitacora`
  MODIFY `bi_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

/*Crear campo car_adjuntos en tabla carritos: tipo de dato: text*/
ALTER TABLE carritos ADD car_adjuntos text;

/*Modificar campo pe_adjunto1 en la tabla pedido_proveedores: cambiar a tipo text*/
ALTER TABLE pedidos_proveedores MODIFY pe_adjunto1 TEXT;

/*Si el alter anterior da error usar este que esta comentado para corregir las fechas*/
/*UPDATE pedidos_proveedores 
SET fecha_aprobacion = pe_fecha 
where DATE_FORMAT(fecha_aprobacion, '%Y-%m-%d %H:%i:%s') = '0000-00-00 00:00:00';*/