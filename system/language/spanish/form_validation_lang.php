<?php

$lang['required']			= "Campo requerido.";
$lang['isset']				= "Debe tener un valor.";
$lang['valid_email']		= "Debe contener una direccón de email válida.";
$lang['valid_emails']		= "Debe contener direcciones de email válidas.";
$lang['valid_url']			= "Debe tener una URL válida.";
$lang['valid_ip']			= "Debe contener una dirección IP válida.";
$lang['min_length']			= "Campo %s debe tener al menos %s caracteres.";
$lang['max_length']			= "Campo %s no puede exceder de %s caracteres.";
$lang['exact_length']		= "Campo %s debe tener exactamente %s caracteres.";
$lang['alpha']				= "Sólo puede contener caracteres alfabéticos.";
$lang['alpha_numeric']		= "Sólo puede contener caracteres alfanuméricos.";
$lang['alpha_dash']			= "Sólo puede contener caracteres alfanuméricos, guiones y guión bajo.";
$lang['numeric']			= "Sólo puede contener números.";
$lang['is_numeric']			= "Sólo puede contener números.";
$lang['integer']			= "Debe contener un número entero.";
$lang['regex_match']		= "No está en el formato correcto.";
$lang['matches']			= "Campo %s no coincide con el campo %s";
$lang['is_unique'] 			= "Debe contener un valor único.";
$lang['is_natural']			= "Debe contener sólo números positivos.";
$lang['is_natural_no_zero']	= "Debe contener un número mayor que cero.";
$lang['decimal']			= "Debe contener un número decimal.";
$lang['less_than']			= "Campo %s debe contener un número menor que %s.";
$lang['greater_than']		= "Campo %s debe contener un número meyor que %s.";

/*
$lang['required']			= "Campo %s requerido.";
$lang['isset']				= "Campo %s debe tener un valor.";
$lang['valid_email']		= "Campo %s debe contener una direccón de email válida.";
$lang['valid_emails']		= "Campo %s debe contener direcciones de email válidas.";
$lang['valid_url']			= "Campo %s debe tener una URL válida.";
$lang['valid_ip']			= "Campo %s debe contener una dirección IP válida.";
$lang['min_length']			= "Campo %s debe tener alemnos %s caracteres.";
$lang['max_length']			= "Campo %s no puede exceder de %s caracteres.";
$lang['exact_length']		= "Campo %s debe tener exactamente %s caracteres.";
$lang['alpha']				= "Campo %s sólo puede contener caracteres alfabéticos.";
$lang['alpha_numeric']		= "Campo %s sólo puede contener caracteres alfanuméricos.";
$lang['alpha_dash']			= "Campo %s sólo puede contener caracteres alfanuméricos, guiones y guión bajo.";
$lang['numeric']			= "Campo %s sólo puede contener números.";
$lang['is_numeric']			= "Campo %s sólo puede contener números.";
$lang['integer']			= "Campo %s debe contener un número entero.";
$lang['regex_match']		= "Campo %s no está en el formato correcto.";
$lang['matches']			= "Campo %s no coincide con el campo %s";
$lang['is_unique'] 			= "Campo %s debe contener un valor único.";
$lang['is_natural']			= "Campo %s debe contener sólo números positivos.";
$lang['is_natural_no_zero']	= "Campo %s debe contener un número mayor que cero.";
$lang['decimal']			= "Campo %s debe contener un número decimal.";
$lang['less_than']			= "Campo %s debe contener un número menor que %s.";
$lang['greater_than']		= "Campo %s debe contener un número meyor que %s.";
*/


/* End of file form_validation_lang.php */
/* Location: ./system/language/spanish/form_validation_lang.php */