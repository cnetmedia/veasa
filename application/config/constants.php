<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

//CARPETA PRIVADA
define('PRIVADO', BASEPATH.'../company/');
//define('PRIVADOVIEW', 'http://localhost/programa/private/company/');

//UNIDADES DE MEDIDA
define('b', 1);
define('B', 8); //EQUIVALE A 8 bit
define('KB', 1024); //EQUIVALE A 1024 Byte
define('MB', 1048576); //EQUIVALE A 1024 KB
define('GB', 1073741824); //EQUIVALE A 1024 MB
define('TB', 1099511627776); //EQUIVALE A 1024 GB

//EMAIL
define('EMAIL', 'VEASA App <webadmin@cnetmedia.net>');

/* End of file constants.php */
/* Location: ./application/config/constants.php */