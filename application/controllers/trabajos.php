<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trabajos extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	function index() {
		$this->comprobar_usuario('trabajos');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			$acceso = $this->load->view('v_trabajos','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('trabajos'),
	        'TRABAJOS' => 'active',
			'BODY' => $acceso
	    );
	    
		$this->load->view('v_admin',$datos);
	}

	function guardados() {
		$this->comprobar_usuario('trabajos');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			$acceso = $this->load->view('v_trabajos_guardados','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('trabajos.guardados'),
	        'TRABAJOS_GUARDADOS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('trabajos'), 'href'=>'#'],['nombre'=>lang('trabajos.guardados'), 'href'=>''])
	    );
	    
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA TRABAJOS PENDIENTES O PROCESANDOSE PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('trabajos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10') || $this->acceso('13')) {
			if ($this->input->post()) {
				$this->load->model('M_trabajos');
				$array = $this->M_trabajos->get_trabajos_pendientes();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					//SOLO TRABAJOS NO COMPLETADOS
					if ($array[$i]->tr_estado < 2) {
						if ($this->session->userdata('emp_tipo') == 0) {
							$mostrar = true;
						} else if ($this->session->userdata('emp_tipo') == 1 && $this->session->userdata('em_id') == $array[$i]->em_id) {
							$mostrar = true;
						} else {
							//SOLO VE EL TRABAJO EL O LOS EMPLEADOS ASIGNADOS
							/*$empleados = $array[$i]->emp_id;
							$empleados = explode(',',$empleados);
							
							if (in_array($this->session->userdata('emp_id'),$empleados)) {
								$mostrar = true;
							}*/
							if ($this->session->userdata('em_id') == $array[$i]->em_id) {
								$mostrar = true;
							}
						}
					}
					
					if ($mostrar) {
						//COMPROBAMOS EL TIPO DE EMPLEADO
						if ($this->session->userdata('emp_tipo') == 3) {
							//FECHA ACTUAL DE LA SUCURSAL DEL TRABAJO
							//$fecha = $array[$i]->su_zona_horaria;
							$fecha = fechaHora_actual($array[$i]->su_zona_horaria);
							//SI LA FECHA ACTUAL ES MENOR O IGUAL A LA FECHA DE FIN DEL TRABAJO Y SU ESTADO ES CERO, MOSTRAMOS EL TRABAJO
							if ($fecha <= $array[$i]->tr_fecha_fin) {
								array_push($aux, $array[$i]);
							}
						} else {
							//SI NO ES UN SIMPLE EMPLEADO DIRECTAMENTE MOSTRAMOS EL TRABAJO
							array_push($aux, $array[$i]);
						}
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			if ($this->acceso('10')) {
				redirect(base_url().$this->lang->lang().'/trabajos','refresh');
			} else if ($this->acceso('13')) {
				redirect(base_url().$this->lang->lang().'/calendario','refresh');
			}
		}
	}
	
	//BUSCA TRABAJOS PENDIENTES O PROCESANDOSE PARA MOSTRAR
	public function buscador_completados() {
		$this->comprobar_usuario('trabajos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10') || $this->acceso('13')) {
			if ($this->input->post()) {
				$this->load->model('M_trabajos');
				$array = $this->M_trabajos->get_trabajos_completados();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					//SOLO TRABAJOS NO COMPLETADOS
					if ($array[$i]->tr_estado != 0 ) {
						if ($this->session->userdata('emp_tipo') == 0) {
							$mostrar = true;
						} else if ($this->session->userdata('emp_tipo') == 1 and $this->session->userdata('su_id') == $array[$i]->su_id) {
							$mostrar = true;
						} else {
							//SOLO VE EL TRABAJO EL O LOS EMPLEADOS ASIGNADOS
							/*$empleados = $array[$i]->emp_id;
							$empleados = explode(',',$empleados);
							
							if (in_array($this->session->userdata('emp_id'),$empleados)) {
								$mostrar = true;
							}*/
							if ($this->session->userdata('em_id') == $array[$i]->em_id) {
								$mostrar = true;
							}
						}
					}
					
					if ($mostrar) {
						//COMPROBAMOS EL TIPO DE EMPLEADO
						if ($this->session->userdata('emp_tipo') == 3) {
							//FECHA ACTUAL DE LA SUCURSAL DEL TRABAJO
							//$fecha = $array[$i]->su_zona_horaria;
							$fecha = fechaHora_actual($array[$i]->su_zona_horaria);
							//SI LA FECHA ACTUAL ES MENOR O IGUAL A LA FECHA DE FIN DEL TRABAJO Y SU ESTADO ES CERO, MOSTRAMOS EL TRABAJO
							if ($fecha <= $array[$i]->tr_fecha_fin) {
								array_push($aux, $array[$i]);
							}
						} else {
							//SI NO ES UN SIMPLE EMPLEADO DIRECTAMENTE MOSTRAMOS EL TRABAJO
							array_push($aux, $array[$i]);
						}
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			if ($this->acceso('10')) {
				redirect(base_url().$this->lang->lang().'/trabajos','refresh');
			} else if ($this->acceso('13')) {
				redirect(base_url().$this->lang->lang().'/calendario','refresh');
			}
		}
	}
	
	//BUSCA TRABAJOS PENDIENTES O PROCESANDOSE PARA MOSTRAR
	public function buscador_todos() {
		$this->comprobar_usuario('trabajos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10') || $this->acceso('13')) {
			if ($this->input->post()) {
				$this->load->model('M_trabajos');
				$array = $this->M_trabajos->get_trabajos();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					//SOLO TRABAJOS NO COMPLETADOS
					
					if ($this->session->userdata('em_id') == $array[$i]->em_id) {
						$mostrar = true;
					}
				
					
					if ($mostrar) {
						//COMPROBAMOS EL TIPO DE EMPLEADO
						if ($this->session->userdata('emp_tipo') == 3) {
							//FECHA ACTUAL DE LA SUCURSAL DEL TRABAJO
							//$fecha = $array[$i]->su_zona_horaria;
							$fecha = fechaHora_actual($array[$i]->su_zona_horaria);
							//SI LA FECHA ACTUAL ES MENOR O IGUAL A LA FECHA DE FIN DEL TRABAJO Y SU ESTADO ES CERO, MOSTRAMOS EL TRABAJO
							if ($fecha <= $array[$i]->tr_fecha_fin) {
								array_push($aux, $array[$i]);
							}
						} else {
							//SI NO ES UN SIMPLE EMPLEADO DIRECTAMENTE MOSTRAMOS EL TRABAJO
							array_push($aux, $array[$i]);
						}
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			if ($this->acceso('10')) {
				redirect(base_url().$this->lang->lang().'/trabajos','refresh');
			} else if ($this->acceso('13')) {
				redirect(base_url().$this->lang->lang().'/calendario','refresh');
			}
		}
	}
	
	//PAGINA PARA AÑADIR TRABAJOS
	public function nuevo_trabajo() {
		$this->comprobar_usuario('trabajos/nuevo_trabajo');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			if ($this->session->userdata('emp_crear') == 1) {
				$url = explode("/", $_SERVER['HTTP_REFERER']);		
				if(end($url) == 'trabajos'){
					$breadcrumb = array(['nombre'=>lang('trabajos'), 'href'=>base_url().$this->lang->lang().'/trabajos'],['nombre'=>lang('nuevo.trabajo'), 'href'=>'']);
				}else{
					$breadcrumb = array(['nombre'=>lang('trabajos'), 'href'=>'#'],['nombre'=>lang('trabajos.guardados'), 'href'=>base_url().$this->lang->lang().'/trabajos/guardados'],['nombre'=>lang('nuevo.trabajo'), 'href'=>'']);
				}
				$trabajo = array('tr_id' => null, 'tr_repetir' => '');
				$datos = array(
				    'TITULO' => lang('nuevo.trabajo'),
				    'TRABAJOS' => 'active',
					'BODY' => $this->load->view('v_trabajos_nuevo_editar',$trabajo,true),
					'BREADCRUMB' => $breadcrumb
				);
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/trabajos','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}	
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		if ($this->input->post('tr_id') != '') {
			$url = 'trabajos/editar_trabajo/'.$this->input->post('tr_id');	
		} else {
			$url = 'trabajos/nuevo_trabajo';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('tr_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('tr_repetir_cantidad',lang('repeticiones'),'trim|numeric|xss_clean');
					$this->form_validation->set_rules('tr_comentario',lang('comentario'),'trim|max_length[4000]|xss_clean');
					
					if ($this->form_validation->run()) {
						$this->load->model('M_empleados');
						
                        $tr_repetir_cantidad = 0;
                        if ($this->input->post('tr_id') == '') {
                        	//DIA DE LA SEMANA A REPETIR
                            $tr_repetir = $this->input->post('tr_repetir');
                            //CANTIDAD DE VECES QUE SE REPITE EL TRABAJO
                            $tr_repetir_cantidad = $this->input->post('tr_repetir_cantidad');
                            //SI NO TIENE CANTIDAD LA ESTABLECEMOS EN 1
                            if ($tr_repetir_cantidad == '' OR $tr_repetir_cantidad == 0) {
                            	$tr_repetir_cantidad = 1;
                            }
                        }
						
						//EMPLEADOS ENVIADOS POR POST
						$emple_post = $this->input->post('emp_id');
						
						//FECHAS
						$inicio1 = strtotime($this->input->post('tr_fecha_inicio'));
						$fin1 = strtotime($this->input->post('tr_fecha_fin'));
						
						//FECHAS FORMATO DATE
						$inicio2 = date('Y-m-d H:i:s',$inicio1);
						$fin2 = date('Y-m-d H:i:s',$fin1);
						
						//DIA DE LA SEMANA
						$dia = '';
						//CONTADOR DE VUELTAS PARA LAS REPETICIONES
						$parar = 0;
						$entrar = '';
						//GUARDAMOS LAS FECHAS VALIDAS SEGUN LAS REPETICIONES DEL DIA DE LA SEMANA
						$fechas = array();
						
						while ($parar < $tr_repetir_cantidad) {
							if ($parar > 0) {
								//CREAMOS LA FECHA DE FIN IGUAL QUE LA DE INICIO I HAY QUE REPETIR LOS MISMOS DIAS A LA SEMANA
								$dia = date("w",strtotime($inicio2.' +1 day'));
								$inicio1 = strtotime($inicio2.' +1 day');
								$inicio2 = date('Y-m-d H:i:s',$inicio1);
								$aux1 = date('Y-m-d',$inicio1);
								$aux2 = date('H:i:s',$fin1);
								$fin2 = $aux1.' '.$aux2;
							} else {
								$dia = date("w",$inicio1);
								//CREAMOS LA FECHA DE FIN IGUAL QUE LA DE INICIO I HAY QUE REPETIR LOS MISMOS DIAS A LA SEMANA
								if ($tr_repetir_cantidad > 1) {
									$aux1 = date('Y-m-d',$inicio1);
									$aux2 = date('H:i:s',$fin1);
									$fin2 = $aux1.' '.$aux2;
								}
							}
							
							//COMPRUEBA SI EL DIA ESTA ENTRE LOS DIAS A REPETIR SEMANALMENTE
							if (in_array($dia,$tr_repetir) or $parar == 0) {
								$entrar = true;
							} else {
								$entrar = false;
							}							
							
							if ($entrar) {
								//FORMATEAMOS FECHA
								$fecha = $inicio2.' - '.$fin2;
								
								//NUEVO TRABAJO
								//if ($this->input->post('tr_id') == '') {
									//OBTENEMOS LOS EMPLEADOS LIBREs
									$empleados = $this->M_empleados->get_empleados_libres_trabajos($this->input->post('su_id'),$fecha,$this->input->post('tr_id'),$this->input->post('emp_id'));
									
									//BUSCA LOS EMPLEADOS PASADOS POR POST ENTRE LOS EMPLEADOS LIBRES
									for ($t=0; $t<count($emple_post); $t++) {
										$cont = 0;
										for ($i=0; $i<count($empleados); $i++) {
											if ($emple_post[$t] == $empleados[$i]['emp_id']) {
												$cont++;
											}
										}
										
										//SI NO ENCONTRO EL EMPLEADO LO MARCA NO DISPONIBLE
										if ($cont == 0) {
											$empleado = $this->M_empleados->get_empleado($emple_post[$t]);
											$nombre = $empleado->emp_nombre.' '.$empleado->emp_apellido1.' '.$empleado->emp_apellido2;
											
											$f = array('emp_id',lang('empleado.ocupado1').' '.$nombre.' '.lang('empleado.ocupado2'));
														
											array_push($errors,$f);
										}
									}
								/*} else {
									//EDITAR TRABAJO						
									//OBTENEMOS LOS EMPLEADOS LIBRES
									$empleados = $this->M_empleados->get_empleados_libres_trabajos($this->input->post('su_id'),$fecha,$this->input->post('tr_id'),$this->input->post('emp_id'));
									
									//BUSCA LOS EMPLEADOS PASADOS POR POST ENTRE LOS EMPLEADOS LIBRES
									for ($t=0; $t<count($emple_post); $t++) {
										$cont = 0;
										for ($i=0; $i<count($empleados); $i++) {
											if ($emple_post[$t] == $empleados[$i]['emp_id']) {
												$cont++;
											}
										}
										
										//SI NO ENCONTRO EL EMPLEADO LO MARCA NO DISPONIBLE
										if ($cont == 0) {
											$empleado = $this->M_empleados->get_empleado($emple_post[$t]);
											$nombre = $empleado->emp_nombre.' '.$empleado->emp_apellido1.' '.$empleado->emp_apellido2;
											
											$f = array('emp_id',lang('empleado.ocupado1').' '.$nombre.' '.lang('empleado.ocupado2'));
														
											array_push($errors,$f);
										}
									}
								}*/
								
								$response['errors'] = array_filter($errors);
								if ($response['errors'] == '' || $response['errors'] == null) {
									$o = array($inicio2,$fin2);	
									array_push($fechas,$o);		
								}
								
								$parar++;
							}
						}	
											
						//SI NO EXISTEN ERRORES GUARDAMOS EL TRABAJO
						$response['errors'] = array_filter($errors);
						if ($response['errors'] == '' || $response['errors'] == null) {
							$this->load->model('M_trabajos');
							$response['sql'] = $this->M_trabajos->set_trabajo($this->input->post(),$fechas);
							$response['status'] = $response['sql'];


						}
						
						//ENVIAMOS EL EMAIL A LOS EMPLEADOS
						if ($response['status']) {
							$this->load->library('email');
							$nombre = "=?UTF-8?B?".base64_encode($this->session->userdata('em_nombre'))."=?=";
							$this->email->from(EMAIL, $nombre);
							$this->email->subject(lang('nuevo.trabajo'));
							
							//PLANTILLA EMAIL
							$man_id = $this->input->post('man_id');
							
							$this->load->model('M_mantenimientos');
							$datos = array();
							
							$aux = array();
							for ($x=0; $x<count($man_id); $x++) {
								$mantenimiento = $this->M_mantenimientos->get_mantenimiento($man_id[$x][0]);
								array_push($aux,$mantenimiento);
							}
							
							$datos['mantenimientos'] = $aux;
							
							$datos['informacion'] = $this->input->post('tr_fecha_inicio');
							$datos['comentario'] = $this->input->post('tr_comentario');
								
							$data = array(
								'BODY' => $this->load->view('emails/v_email_nuevo_trabajo',$datos, TRUE)
							);
							$email = $this->load->view('emails/v_email', $data, TRUE);

							$this->email->message($email);
							
							$empleados = $this->input->post('emp_id');
							$para = '';
							for ($t=0; $t<count($empleados); $t++) {
								$aux = $this->M_empleados->get_empleado($empleados[$t]);
								$para = $para.$aux->emp_email.',';
							}
							
							//QUITAMOS EL ULTIMO CARACTER QUE ES LA COMA
							$para = trim($para, ',');
							
							//CREAMOS LAS CABECERAS PARA ENVIAR
							$this->email->to($para);
							
							//ENVIAMOS EL EMAIL  	
							$this->email->send();
						}

					}
					
					//MOSTRAMOS LOS ERRORES
					foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors); 
				}
			}
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}
	}
	
	//PAGINA PARA EDITAR TRABAJOS
	public function editar_trabajo($tr_id=null) {
		if ($tr_id != null) {
			$this->comprobar_usuario('trabajos/editar_trabajo/'.$tr_id);	
		} else {
			$this->comprobar_usuario('trabajos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		$tipo = $this->session->userdata('emp_tipo');

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('10')) {
			$this->load->model('M_trabajos');
			$trabajo = $this->M_trabajos->get_trabajo($tr_id);
                        
            //EMPLEADOS EN EL TRABAJO
            $id_empleados = $trabajo->emp_id;
            $id_empleados = explode(',',$id_empleados);
            $id_empleados = array_filter($id_empleados);
                        
            $empleados = array();
                        
            $this->load->model('M_empleados');
            for ($x=0; $x<count($id_empleados); $x++) {
            	$aux = $this->M_empleados->get_empleado_id($id_empleados[$x]);
                array_push($empleados,$aux);
            }
                        
            $trabajo->empleados_asignados = $empleados;
            
            //PRESUPUESTOS EN EL TRABAJO
            $id_presupuestos = $trabajo->pr_id;
            $id_presupuestos = explode(',',$id_presupuestos);
            $id_presupuestos = array_filter($id_presupuestos);
            
            $presupuestos = array();
            
            $this->load->model('M_presupuestos');
            for ($x=0; $x<count($id_presupuestos); $x++) {
            	$aux = $this->M_presupuestos->get_presupuesto($id_presupuestos[$x]);
                array_push($presupuestos,$aux);
            }
                        
            $trabajo->presupuestos_asignados = $presupuestos;
                        
			//PREGUNTAMOS QUE TIPO DE USUARIO ES PARA MOSTRAR LA VISTA DE EDICION O LA DE LECTURA
			$titulo = ($tipo <= 1) ? lang('editar.trabajo') : lang('ver.trabajo');
			$url = explode("/", $_SERVER['HTTP_REFERER']);		
			if(end($url) == 'trabajos'){
				$breadcrumb = array(['nombre'=>lang('trabajos'), 'href'=>base_url().$this->lang->lang().'/trabajos'],['nombre'=>$titulo, 'href'=>'']);
			}elseif(end($url) == 'guardados'){
				$breadcrumb = array(['nombre'=>lang('trabajos'), 'href'=>'#'],['nombre'=>lang('trabajos.guardados'), 'href'=>base_url().$this->lang->lang().'/trabajos/guardados'],['nombre'=>$titulo, 'href'=>'']);
			}elseif(end($url) == 'informes'){
				$breadcrumb = array(['nombre'=>lang('informes'), 'href'=>base_url().$this->lang->lang().'/informes'],['nombre'=>$titulo, 'href'=>'']);
			}

			$datos = array(
			    'TITULO' => $titulo,
				'TRABAJOS' => 'active',
				'BODY' => $this->load->view('v_trabajos_nuevo_editar',$trabajo,true),
				'BREADCRUMB' => $breadcrumb
			);
			$this->load->view('v_admin',$datos);
		} else {
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}
	}
	
	//PAGINA PARA EDITAR TRABAJOS
	public function ver_trabajo($tr_id=null) {
		if ($tr_id != null) {
			$this->comprobar_usuario('trabajos/ver_trabajo/'.$tr_id);	
		} else {
			$this->comprobar_usuario('trabajos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('10')) {
			$this->load->model('M_trabajos');
			$trabajo = $this->M_trabajos->get_trabajo($tr_id);
			$datos = array(
		        'TITULO' => lang('ver.trabajo'),
		        'TRABAJOS' => 'active',
				'BODY' => $this->load->view('v_trabajos_ver',$trabajo,true),
				'BREADCRUMB' => array(['nombre'=>lang('trabajos'), 'href'=>'#'],['nombre'=>lang('trabajos'), 'href'=>base_url().$this->lang->lang().'/trabajos'],['nombre'=>lang('ver.trabajo'), 'href'=>''])
		    );
			$this->load->view('v_admin',$datos);
		} else {
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}
	}
	
	//GUARDAMOS LA INSPECCION DEL MANTENIMIENTO CON EL TRABAJO (SUPERVISORES)
	public function upload() {
		if ($this->input->post('tr_id') != '') {
			$url = 'trabajos/ver_trabajo/'.$this->input->post('tr_id');	
		} else {
			$url = 'trabajos';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			
			//VALIDAMOS LOS CAMPOS
			$this->form_validation->set_rules('ti_comentario',lang('comentario'),'trim|xss_clean');
			$this->form_validation->set_rules('tia_comentario',lang('comentario'),'trim|xss_clean');
					
			if ($this->form_validation->run()) {
				$this->load->model('M_trabajos');
				
				//GUARDAMOS LA INSPECCION DEL TRABAJO BASICA O AVANZADA
				$in_id = $this->input->post('in_id');
				if (!empty($in_id)) {
					$response['sql'] = $this->M_trabajos->set_trabajo_inspeccion($this->input->post());
				} else {
					$response['sql'] = $this->M_trabajos->set_trabajo_inspeccion_avanzada($this->input->post());
				}
				
				if ($response['sql']) {
					//OBTENEMOS LAS INSPECCIONES BASICAS DEL TRABAJO
					$aux = $this->M_trabajos->get_trabajos_inspecciones($this->input->post('tr_id'));
					
					//OBTENEMOS LAS INSPECCIONES AVANZADAS DEL TRABAJO
					$aux2 = $this->M_trabajos->get_trabajos_inspecciones_avanzadas($this->input->post('tr_id'));
					
					//SOLO GUARDAMOS LAS INSPECCIONES TERMINADAS
					$inspecciones = Array();
					//BASICAS
					for ($i=0; $i<count($aux); $i++) {
						if ($aux[$i]->ti_estado == 2) {
							array_push($inspecciones,$aux[$i]);
						}
					}
					//AVANZADAS
					for ($i=0; $i<count($aux2); $i++) {
						if ($aux2[$i]->tia_estado == 2) {
							array_push($inspecciones,$aux2[$i]);
						}
					}
					
					//OBTENEMOS LOS DATOS DEL TRABAJO
					$trabajo = $this->M_trabajos->get_trabajo($this->input->post('tr_id'));
					//SACAMOS LOS MANTENIMIENTOS DEL TRABAJO
					$mant = explode(';',$trabajo->man_id);
					$mant = array_filter($mant);
					//SACAMOS EL NUMERO DE INSPECCIONES BASICAS DEL TRABAJO
					$inspec_trab = explode(',',$trabajo->in_id);
					$inspec_trab = array_filter($inspec_trab);
					//SACAMOS EL NUMERO DE INSPECCIONES AVANZADAS DEL TRABAJO
					$inspec_avanz_trab = explode(',',$trabajo->inv_id);
					$inspec_avanz_trab = array_filter($inspec_avanz_trab);
					//CALCULAMOS LAS INSPECCIONES POR LOS MANTENIMIENTOS
					$total_inspec = 0;
					$total_inspec = count($mant) * count($inspec_trab);
					$total_inspec_avanz = count($mant) * count($inspec_avanz_trab);
					//SI EL TOTAL DE INSPECCIONES TERMIANDAS ES IGUAL AL CALCULO DE INSPECCIONES QUE DEBERIA DE SER, SE PONE EL TRABAJO COMO TERMINADO
					$total = $total_inspec + $total_inspec_avanz;
					if (count($inspecciones) == $total) {
						$datos['tr_id'] = $this->input->post('tr_id');
						$datos['tr_estado'] = 2;
						
						$response['sql'] = $this->M_trabajos->set_trabajo($datos,null);
					}
				}
				
				if ($response['sql']) {
					if (!empty($_FILES['archivos'])) {
						
						if (!empty($in_id)) {
							$ruta = PRIVADO.$this->session->userdata('em_cif').'/in/'.$this->input->post('tr_id').'_'.$in_id.'_'.$this->input->post('man_id');
						} else {
							$ruta = PRIVADO.$this->session->userdata('em_cif').'/inv/'.$this->input->post('tr_id').'_'.$this->input->post('inv_id').'_'.$this->input->post('man_id');
						}
						
						//PREGUNTA SI EXISTE EL DIRECTORIO, SI NO EXISTE LO CREA
						if (!file_exists($ruta)) {
							mkdir($ruta, 0755);
							copy(PRIVADO.'index.html', $ruta.'/index.html');
						}
						
						//configuración para upload
						$config['upload_path'] = $ruta.'/';
						$config['allowed_types'] = 'jpg';
						$config['max_size'] = '8000'; // 8Mb
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						
						$num_archivos = count($_FILES['archivos']['tmp_name']);
						
						if ($num_archivos > 0) {
							for ($i=0;$i<$num_archivos;$i++) {
								//REEMPLAZAMOS ESPACIOS
								$nombre = str_replace(' ','_',$_FILES['archivos']['name'][$i]);
						       	$_FILES['userfile']['name'] = $nombre;
						       	
						       	$_FILES['userfile']['type'] = $_FILES['archivos']['type'][$i];
						       	$_FILES['userfile']['tmp_name'] = $_FILES['archivos']['tmp_name'][$i];
						       	$_FILES['userfile']['error'] = $_FILES['archivos']['error'][$i];
						       	$_FILES['userfile']['size'] = $_FILES['archivos']['size'][$i];           
						       	
						       	if (!$this->upload->do_upload()) {
						       		$aux = array('fotos',$this->upload->display_errors());
									array_push($errors, $aux);
						      		$response['status'] = false;
						      		// esto es muy útil para encontrar qué falla
						       	}
						   	}
						}
					}
					
					$response['status'] = $response['sql'];
				}
			}
			
			//MOSTRAMOS LOS ERRORES
			foreach ($this->input->post() as $key => $value) {
				if (form_error($key) != '') {
					$aux = array($key,form_error($key));
				    array_push($errors,$aux);
				}
			}
			$response['errors'] = array_filter($errors);
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		}
	}
	
	function imagenes() {
		if ($this->input->post('tr_id') != '') {
			$url = 'trabajos/ver_trabajo/'.$this->input->post('tr_id');	
		} else {
			$url = 'trabajos';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			//DIRECCION DE LAS FOTOS
			$ruta = PRIVADO.$this->session->userdata("em_cif").'/in/'.$this->input->post('tr_id').'_'.$this->input->post('in_id').'_'.$this->input->post('man_id');
			
			$aux = '<br>';
			//RECORRE LA CARPETA
			if (file_exists($ruta)) {
				//CREA UN OBJETO DE CARPETA
				$dirint = dir($ruta);
				
				while (($archivo = $dirint->read()) !== false) {
					//SI NO ES JPG NO PASA
					if (preg_match("/jpg/", $archivo)){
						//CREA LA IMAGEN						
						$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata('em_cif').'/in/'.$this->input->post('tr_id').'_'.$this->input->post('in_id').'_'.$this->input->post('man_id').'/'.$archivo;
						
						$nombre = substr($archivo, 0, count($archivo)-5);
						
						//LISTADO DE IMAGENES
						$aux = $aux.'<a rel="'.$this->input->post('tr_id').$this->input->post('in_id').'" class="fancybox" href="'.$img.'" title="'.$nombre.'"><img src="'.$img.'" /></a>';
					}
				}
				
				//CERRAMOS CARPETA
				$dirint->close();
			}
			
			
			//ENVIAMOS LISTA DE IMAGENES
			echo json_encode($aux);
		}
	}
	
	function imagenes_avanzadas() {
		if ($this->input->post('tr_id') != '') {
			$url = 'trabajos/ver_trabajo/'.$this->input->post('tr_id');	
		} else {
			$url = 'trabajos';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			//DIRECCION DE LAS FOTOS
			$ruta = PRIVADO.$this->session->userdata("em_cif").'/inv/'.$this->input->post('tr_id').'_'.$this->input->post('inv_id').'_'.$this->input->post('man_id');
			
			$aux = '<br>';
			//RECORRE LA CARPETA
			if (file_exists($ruta)) {
				//CREA UN OBJETO DE CARPETA
				$dirint = dir($ruta);
				
				while (($archivo = $dirint->read()) !== false) {
					//SI NO ES JPG NO PASA
					if (preg_match("/jpg/", $archivo)){
						//CREA LA IMAGEN						
						$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata('em_cif').'/inv/'.$this->input->post('tr_id').'_'.$this->input->post('inv_id').'_'.$this->input->post('man_id').'/'.$archivo;
						
						$nombre = substr($archivo, 0, count($archivo)-5);
						
						//LISTADO DE IMAGENES
						$aux = $aux.'<a rel="'.$this->input->post('tr_id').$this->input->post('inv_id').'" class="fancybox" href="'.$img.'" title="'.$nombre.'"><img src="'.$img.'" /></a>';
					}
				}
				
				//CERRAMOS CARPETA
				$dirint->close();
			}
			
			
			//ENVIAMOS LISTA DE IMAGENES
			echo json_encode($aux);
		}
	}
}

/* End of file trabajos.php */
/* Location: ./application/controllers/trabajos.php */