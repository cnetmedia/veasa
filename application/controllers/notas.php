<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notas extends MY_Controller {
	
	function __construct() {
		parent::__construct();
	} 
	
	public function index() {
		$this->comprobar_usuario('notas');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		$datos = array(
	        'TITULO' => lang('notas'),
			'BODY' => $this->load->view('v_notas','',true)
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA NOTAS PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('notas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->input->post()) {
			$this->load->model('M_notas');
			$notas = $this->M_notas->get_notas();
			$aux = array();
			
			for ($i=0; $i<count($notas); $i++) {
				//NOTA PUBLICA O PRIVADA
				if ($notas[$i]->no_tipo == 0) {
					
					$mostrar = false;
					
					//QUIEN ESCRIBIO LA NOTA?
					if ($this->session->userdata('emp_id') == $notas[$i]->no_emp_id) {
						$mostrar = true;
					} else {
						//NO LA ESCRIBI YO
						//EXISTE SUCURSAL???
						if ($notas[$i]->su_id != 0) {
							//SUCURSAL IGUAL A LA SESSION
							if ($this->session->userdata('su_id') == $notas[$i]->su_id) {
								//EXISTE DEPARTAMENTO
								if ($notas[$i]->de_id != 0) {
									//DEPARTAMENTO IGUAL
									if ($this->session->userdata('de_id') == $notas[$i]->de_id) {
										//EXISTE EMPLEADO
										if ($notas[$i]->emp_id != 0) {
											if ($this->session->userdata('emp_id') == $notas[$i]->emp_id) {
												//MOSTRAMOS SOLO AL EMPLEADO
												$mostrar = true;
											}
										} else {
											//MOSTRAMOS A TODO EL DEPARTAMENTO
											$mostrar = true;
										}
									} else {
										$mostrar = false;
									}
								} else {
									//MOSTRAMOS A TODA LA SUCURSAL
									$mostrar = true;
								}
							} else {
								$mostrar = false;
							}
						} else {
							//VISIBLE PARA TODA LA EMPRESA
							$mostrar = true;
						}
					}
					
					if ($mostrar) {
						array_push($aux, $notas[$i]);
					}
					
				} else {
					if ($this->session->userdata('emp_id') == $notas[$i]->emp_id) {
						array_push($aux, $notas[$i]);
					}
				}
			}
			
			echo json_encode($aux);
		}
	}
	
	//PAGINA PARA EDITAR NOTAS
	public function ver_nota($no_id=null) {
		if ($no_id != null) {
			$this->comprobar_usuario('notas/ver_nota/'.$no_id);	
		} else {
			$this->comprobar_usuario('notas');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($no_id == null) {
			redirect(base_url().$this->lang->lang().'/notas','refresh');
		} else {
			$this->load->model('M_notas');
			$nota = $this->M_notas->get_nota($no_id);
			//PREGUNTAMOS SE DEVOLVIO RESULTADOS
			if ($nota != null) {
				$datos = array(
			        'TITULO' => lang('ver.nota'),
					'BODY' => $this->load->view('v_notas_nuevo_editar',$nota,true)
			    );
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/notas','refresh');
			}
		}
	}
	
	//PAGINA PARA AÑADIR NOTAS
	public function nueva_nota() {
		$this->comprobar_usuario('nota/nueva_nota');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
			$notas = array('no_id' => '','no_tipo' => 1);
			$datos = array(
			    'TITULO' => lang('nueva.nota'),
				'BODY' => $this->load->view('v_notas_nuevo_editar',$notas,true)
			);
			$this->load->view('v_admin',$datos);	
	}
	
	//GUARDAR NOTA
	public function guardar_editar() {
		$url = 'notas/nueva_nota';
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
		$response['status'] = false;
		$response['crear'] = true;
		$response['editar'] = true;
		$response['sql'] = true;
		$response['errors'] = '';
		$errors = array();
		$aux = array();
			
			if ($this->input->post()) {
				//VALIDAMOS LOS CAMPOS
				$this->form_validation->set_rules('no_titulo',lang('titulo'),'trim|required|max_length[150]|xss_clean');
				$this->form_validation->set_rules('no_mensaje',lang('mensaje'),'trim|required|max_length[4000]|xss_clean');
					
				if ($this->form_validation->run()) {
					$this->load->model('M_notas');
					$response['sql'] = $this->M_notas->set_nota($this->input->post());
					$response['status'] = $response['sql'];
				}
					
				foreach ($this->input->post() as $key => $value) {
			        if (form_error($key) != '') {
						$aux = array($key,form_error($key));
			           	array_push($errors,$aux);
					}
			    }
			    $response['errors'] = array_filter($errors); 
			}
			
		//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
		echo json_encode($response);
	}
	
	//RESPONDER NOTA
	public function responder() {
		$url = 'notas/ver_nota/'.$this->input->post('no_id');
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
		$response['status'] = false;
		$response['crear'] = true;
		$response['editar'] = true;
		$response['sql'] = true;
		$response['errors'] = '';
		$errors = array();
		$aux = array();
		
		$this->form_validation->set_rules('nr_respuesta',lang('mensaje'),'trim|required|max_length[4000]|xss_clean');
		
		if ($this->form_validation->run()) {
			$this->load->model('M_notas');
			$response['sql'] = $this->M_notas->set_respuesta($this->input->post());
			$response['status'] = $response['sql'];
		}
					
		foreach ($this->input->post() as $key => $value) {
	  		if (form_error($key) != '') {
				$aux = array($key,form_error($key));
			    array_push($errors,$aux);
			}
		}
		
		$response['errors'] = array_filter($errors);
		
		//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
		echo json_encode($response); 
	}
	
	//MOSTRAR LAS RESPUESTAS DE CADA NOTA
	public function ver_respuestas() {
		$url = 'notas/ver_nota/'.$this->input->post('no_id');
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		$this->load->model('M_notas');
		$respuestas = $this->M_notas->get_respuestas($this->input->post());
		
		echo json_encode($respuestas);
	}
	
	//MARCA LA NOTA COMO VISTA
	public function nota_vista() {
		$url = 'notas/ver_nota/'.$this->input->post('no_id');
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//RECUPERAMOS LA NOTA
		$this->load->model('M_notas');
		$nota = $this->M_notas->get_nota($this->input->post('no_id'));
		
		//EXTRAEMOS LOS EMPLEADOS QUE VIERON LA NOTA
		$empleados = explode(",", $nota->no_visto);
		//PREGUNTAMOS SI ESTE EMPLEADO YA LA VIO
		if (!in_array($this->session->userdata('emp_id'),$empleados)) {
			//CREAMOS EL STRING DE EMPLEADOS QUE SI LA VIERON
			$visto = $nota->no_visto.$this->session->userdata('emp_id').',';
			//MARCAMOS LA NOTA COMO VISTA
			$nota = $this->M_notas->set_nota_vista($this->input->post('no_id'),$visto);
		}
	}
}

/* End of file notas.php */
/* Location: ./application/controllers/notas.php */