<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inspecciones extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE INSPECCIONES
	public function index() {
		$this->comprobar_usuario('inspecciones');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('9')) {
			$acceso = $this->load->view('v_inspecciones','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('inspecciones'),
	        'INSPECCIONES' => 'active',
	        'BASICAS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('inspecciones'), 'href'=>'#'],['nombre'=>lang('inspecciones').' '.lang('basicas'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA INSPECCIONES PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('inspecciones');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('9')) {
			if ($this->input->post()) {
				$this->load->model('M_inspecciones');
				$array = $this->M_inspecciones->get_inspecciones();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
		}
	}
	
	//BUSCA LAS INSPECCIONES DE LA SUCURSAL
	public function inspecciones_sucursal() {
		$this->comprobar_usuario('trabajos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			if ($this->input->post()) {
				$this->load->model('M_inspecciones');
				echo json_encode($this->M_inspecciones->get_inspecciones_sucursal($this->input->post('su_id')));
			}
		} else {
			//DENEGAMOS EL ACCESO
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}
	}
	
	//BUSCA LAS INSPECCIONES SEGUN EL TRABAJO
	public function inspecciones_trabajo() {
		$this->comprobar_usuario('trabajos/ver_trabajo/'.$this->input->post('tr_id'));
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			if ($this->input->post()) {
				$this->load->model('M_inspecciones');
				$result = Array();
				//OBTENEMOS EL STRING DE INSPECCIONES Y LO PASAMOS A ARRAY
				$in_id = $this->input->post('in_id');
				$in_id = explode(',',$in_id);
				$in_id = array_filter($in_id);
				//RECORREMOS LAS INSPECCIONES DEL TRABAJO
				for ($i=0; $i<count($in_id); $i++) {
					$inspeccion = $this->M_inspecciones->get_inspecciones_trabajo($in_id[$i],$this->input->post('tr_id'),$this->input->post('man_id'));
					
					if (count($inspeccion) == 0) {
						$inspeccion = $this->M_inspecciones->get_inspeccion($in_id[$i]);
					} else {
						//DIRECCION DE LAS FOTOS
						$ruta = PRIVADO.$this->session->userdata("em_cif").'/in/'.$inspeccion->tr_id.'_'.$inspeccion->in_id.'_'.$inspeccion->man_id;
						
						$aux = '<br>';
						//RECORRE LA CARPETA
						if (file_exists($ruta)) {
							//CREA UN OBJETO DE CARPETA
							$dirint = dir($ruta);
							
							while (($archivo = $dirint->read()) !== false) {
								//SI NO ES JPG NO PASA
								if (preg_match("/jpg/", $archivo)){
									//CREA LA IMAGEN						
									$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata('em_cif').'/in/'.$inspeccion->tr_id.'_'.$inspeccion->in_id.'_'.$inspeccion->man_id.'/'.$archivo;
									
									$nombre = substr($archivo, 0, count($archivo)-5);
									
									//LISTADO DE IMAGENES
									$aux = $aux.'<a rel="in'.$inspeccion->tr_id.$inspeccion->in_id.'" class="fancybox" href="'.$img.'" title="'.$nombre.'"><img src="'.$img.'" /></a>';
								}
							}
							
							//CERRAMOS CARPETA
							$dirint->close();
							$inspeccion->imagenes = $aux;
						}
					}
					
					array_push($result,$inspeccion);
				}
				
				echo json_encode($result);
			}
		} else {
			//DENEGAMOS EL ACCESO
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}
	}
	
	//BUSCA LAS INSPECCIONES AVANZADAS SEGUN EL TRABAJO
	public function inspecciones_avanzadas_trabajo() {
		$this->comprobar_usuario('trabajos/ver_trabajo/'.$this->input->post('tr_id'));
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			if ($this->input->post()) {
				$this->load->model('M_inspecciones');
				$result = Array();
				//OBTENEMOS EL STRING DE INSPECCIONES Y LO PASAMOS A ARRAY
				$inv_id = $this->input->post('inv_id');
				$inv_id = explode(',',$inv_id);
				$inv_id = array_filter($inv_id);
				//RECORREMOS LAS INSPECCIONES DEL TRABAJO
				for ($i=0; $i<count($inv_id); $i++) {
					$inspeccion = 0;
					
					$inspeccion = $this->M_inspecciones->get_inspecciones_avanzadas_trabajo($inv_id[$i],$this->input->post('tr_id'),$this->input->post('man_id'));
					
					if (count($inspeccion) == 0) {
						$inspeccion = $this->M_inspecciones->get_inspeccion_avanzada($inv_id[$i]);
					} else {
						//DIRECCION DE LAS FOTOS
						$ruta = PRIVADO.$this->session->userdata("em_cif").'/inv/'.$inspeccion->tr_id.'_'.$inspeccion->inv_id.'_'.$inspeccion->man_id;
						
						$aux = '<br>';
						//RECORRE LA CARPETA
						if (file_exists($ruta)) {
							//CREA UN OBJETO DE CARPETA
							$dirint = dir($ruta);
							
							while (($archivo = $dirint->read()) !== false) {
								//SI NO ES JPG NO PASA
								if (preg_match("/jpg/", $archivo)){
									//CREA LA IMAGEN						
									$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata('em_cif').'/inv/'.$inspeccion->tr_id.'_'.$inspeccion->inv_id.'_'.$inspeccion->man_id.'/'.$archivo;
									
									$nombre = substr($archivo, 0, count($archivo)-5);
									
									//LISTADO DE IMAGENES
									$aux = $aux.'<a rel="inv'.$inspeccion->tr_id.$inspeccion->inv_id.'" class="fancybox" href="'.$img.'" title="'.$nombre.'"><img src="'.$img.'" /></a>';
								}
							}
							
							//CERRAMOS CARPETA
							$dirint->close();
							$inspeccion->imagenes = $aux;
						}
					}
					
					array_push($result,$inspeccion);
				}
				
				echo json_encode($result);
			}
		} else {
			//DENEGAMOS EL ACCESO
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}
	}
	
	//PAGINA PARA A�ADIR INSPECCIONES
	public function nueva_inspeccion() {
		$this->comprobar_usuario('inspecciones/nueva_inspeccion');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('9')) {
			if ($this->session->userdata('emp_crear') == 1) {
				$inspeccion = array('in_id' => '');
				$datos = array(
			        'TITULO' => lang('nueva.inspeccion'),
			        'INSPECCIONES' => 'active',
	        		'BASICAS' => 'active',
					'BODY' => $this->load->view('v_inspecciones_nuevo_editar',$inspeccion,true),
					'BREADCRUMB' => array(['nombre'=>lang('inspecciones'), 'href'=>'#'],['nombre'=>lang('inspecciones').' '.lang('basicas'), 'href'=>base_url().$this->lang->lang().'/inspecciones'],['nombre'=>lang('nueva.inspeccion'), 'href'=>''])
			    );
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
		}
	}
	
	//PAGINA PARA EDITAR INSPECCIONES
	public function editar_inspeccion($in_id=null) {
		if ($in_id != null) {
			$this->comprobar_usuario('inspecciones/editar_inspeccion/'.$in_id);	
		} else {
			$this->comprobar_usuario('inspecciones');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('9')) {
			if ($in_id == null) {
				redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
			} else {
				$this->load->model('M_inspecciones');
				$inspeccion = $this->M_inspecciones->get_inspeccion($in_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($inspeccion != null) {
					$datos = array(
					    'TITULO' => lang('editar.inspeccion'),
					    'INSPECCIONES' => 'active',
	        			'BASICAS' => 'active',
						'BODY' => $this->load->view('v_inspecciones_nuevo_editar',$inspeccion,true),
						'BREADCRUMB' => array(['nombre'=>lang('inspecciones'), 'href'=>'#'],['nombre'=>lang('inspecciones').' '.lang('basicas'), 'href'=>base_url().$this->lang->lang().'/inspecciones'],['nombre'=>lang('editar.inspeccion'), 'href'=>''])
					);
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		if ($this->input->post('in_id') != '') {
			$url = 'inspecciones/editar_inspeccion/'.$this->input->post('in_id');	
		} else {
			$url = 'inspecciones/nueva_inspeccion';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('9')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('in_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('in_nombre',lang('nombre'),'trim|required|max_length[150]|xss_clean');
					
					if ($this->form_validation->run()) {
						$this->load->model('M_inspecciones');
						$response['sql'] = $this->M_inspecciones->set_inspeccion($this->input->post());
						$response['status'] = $response['sql'];

						if($response['sql'] != false){
							$id = $response['sql'];
							$response['sql'] = true;
							$response['status'] = $response['sql'];
						}
					}
					
					foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors); 

				    //GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'IN'; //Inspecciones
						$datos['asociado'] = $id;
						$datos['accion'] = ($this->input->post('in_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}
				}
			}
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
		}
	}
	
	/**
	* INSPECCIONES AVANZADAS
	*/
	
	//MUESTRA LA LISTA DE INSPECCIONES
	public function avanzadas() {
		$this->comprobar_usuario('inspecciones/avanzadas');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('19')) {
			$acceso = $this->load->view('v_inspecciones_avanzadas','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('inspecciones.avanzadas'),
	        'INSPECCIONES' => 'active',
	        'AVANZADAS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('inspecciones'), 'href'=>'#'],['nombre'=>lang('inspecciones.avanzadas'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA INSPECCIONES AVANZADAS PARA MOSTRAR
	public function buscador_avanzadas() {
		$this->comprobar_usuario('inspecciones/avanzadas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('19')) {
			if ($this->input->post()) {
				$this->load->model('M_inspecciones');
				$array = $this->M_inspecciones->get_inspecciones_avanzadas();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/inspecciones/avanzadas','refresh');
		}
	}
	
	//PAGINA PARA A�ADIR INSPECCIONES
	public function nueva_inspeccion_avanzada() {
		$this->comprobar_usuario('inspecciones/nueva_inspeccion_avanzada');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('19')) {
			if ($this->session->userdata('emp_crear') == 1) {
				$inspeccion = array('inv_id' => '');
				$datos = array(
			        'TITULO' => lang('nueva.inspeccion.avanzada'),
			        'INSPECCIONES' => 'active',
	        		'AVANZADAS' => 'active',
					'BODY' => $this->load->view('v_inspecciones_avanzadas_nuevo_editar',$inspeccion,true),
					'BREADCRUMB' => array(['nombre'=>lang('inspecciones'), 'href'=>'#'],['nombre'=>lang('inspecciones.avanzadas'), 'href'=>base_url().$this->lang->lang().'/inspecciones/avanzadas'],['nombre'=>lang('nueva.inspeccion.avanzada'), 'href'=>''])
			    );
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/inspecciones/avanzadas','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/inspecciones/avanzadas','refresh');
		}
	}
	
	//PAGINA PARA EDITAR INSPECCIONES
	public function editar_inspeccion_avanzada($inv_id=null) {
		if ($inv_id != null) {
			$this->comprobar_usuario('inspecciones/editar_inspeccion_avanzada/'.$inv_id);	
		} else {
			$this->comprobar_usuario('inspecciones/avanzadas');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('19')) {
			if ($inv_id == null) {
				redirect(base_url().$this->lang->lang().'/inspecciones/avanzadas','refresh');
			} else {
				$this->load->model('M_inspecciones');
				$inspeccion = $this->M_inspecciones->get_inspeccion_avanzada($inv_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($inspeccion != null) {
					$datos = array(
					    'TITULO' => lang('editar.inspeccion.avanzada'),
					    'INSPECCIONES' => 'active',
	        			'AVANZADAS' => 'active',
						'BODY' => $this->load->view('v_inspecciones_avanzadas_nuevo_editar',$inspeccion,true),
						'BREADCRUMB' => array(['nombre'=>lang('inspecciones'), 'href'=>'#'],['nombre'=>lang('inspecciones.avanzadas'), 'href'=>base_url().$this->lang->lang().'/inspecciones/avanzadas'],['nombre'=>lang('editar.inspeccion.avanzada'), 'href'=>''])
					);
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/inspecciones/avanzadas','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/inspecciones/avanzadas','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO AVANZADO
	public function guardar_editar_avanzada() {
		if ($this->input->post('inv_id') != '') {
			$url = 'inspecciones/editar_inspeccion_avanzada/'.$this->input->post('inv_id');	
		} else {
			$url = 'inspecciones/nueva_inspeccion_avanzada';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('19')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('inv_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('inv_nombre',lang('nombre'),'trim|required|max_length[150]|xss_clean');
					
					if ($this->form_validation->run()) {
						$this->load->model('M_inspecciones');
						$response['sql'] = $this->M_inspecciones->set_inspeccion_avanzada($this->input->post());
						$response['status'] = $response['sql'];

						if($response['sql'] != false){
							$id = $response['sql'];
							$response['sql'] = true;
							$response['status'] = $response['sql'];
						}
					}
					
					foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors); 

				    //GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'IA'; //Inspecciones Avanzadas
						$datos['asociado'] = $id;
						$datos['accion'] = ($this->input->post('inv_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}
				}
			}
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/inspecciones/avanzadas','refresh');
		}
	}
	
	//BUSCA LAS INSPECCIONES AVANZADAS DE LA SUCURSAL
	public function inspecciones_avanzadas_sucursal() {
		$this->comprobar_usuario('trabajos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('10')) {
			if ($this->input->post()) {
				$this->load->model('M_inspecciones');
				echo json_encode($this->M_inspecciones->get_inspecciones_avanzadas_sucursal($this->input->post('su_id')));
			}
		} else {
			//DENEGAMOS EL ACCESO
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}
	}
	
	//BUSCA LAS INSPECCIONES AVANZADAS DE LA SUCURSAL
	public function eliminar() {
		if ($this->input->post('in_id')) {
			$this->comprobar_usuario('inspecciones');
			$tipo = 'IN';
		} else {
			$this->comprobar_usuario('inspecciones/avanzadas');
			$tipo = 'IA';
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('9') || $this->acceso('19')) {
			if ($this->input->post()) {
				$this->load->model('M_trabajos');
				$response['status'] = $this->M_trabajos->get_eliminar_inspeccion($this->input->post());

				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = $tipo;
					$datos['asociado'] = $this->input->post('in_id');
					$datos['accion'] = 'Eliminar';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response['status']);
			}
		} else {
			//DENEGAMOS EL ACCESO
			if ($this->acceso('9')) {
				redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
			} else if ($this->acceso('19')) {
				redirect(base_url().$this->lang->lang().'/inspecciones/avanzadas','refresh');
			}
		}
	}
}

/* End of file inspecciones.php */
/* Location: ./application/controllers/inspecciones.php */