<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Informes extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	function index() {
		$this->comprobar_usuario('informes');
		
		$datos = array(
            'TITULO' => lang('informes'),
            'INFORMES' => 'active',
			'BODY' => $this->load->view('v_informes','',true)
        );
		$this->load->view('v_admin',$datos);
	}
}

/* End of file informes.php */
/* Location: ./application/controllers/informes.php */