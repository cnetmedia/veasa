<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sucursales extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE SUCURSALES
	public function index() {
		$this->comprobar_usuario('sucursales');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6')) {
			$acceso = $this->load->view('v_sucursales','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('sucursales'),
	        'EMPRESA' => 'active',
	        'SUCURSALES' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('sucursales'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA SUCURSALES PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('sucursales');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6') || $this->acceso('7') || $this->acceso('4') || $this->acceso('1') || $this->acceso('3') || $this->acceso('9') || $this->acceso('12') || $this->acceso('10') || $this->acceso('13')) {
			if ($this->input->post()) {
				$this->load->model('M_sucursales');
				$array = $this->M_sucursales->get_sucursales();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('em_id') == $array[$i]->em_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			//DENEGAMOS EL ACCESO
			if ($this->acceso('6')) {
				redirect(base_url().$this->lang->lang().'/sucursales','refresh');
			} else if ($this->acceso('7')) {
				redirect(base_url().$this->lang->lang().'/departamentos','refresh');
			} else if ($this->acceso('4')) {
				redirect(base_url().$this->lang->lang().'/clientes','refresh');
			} else if ($this->acceso('3')) {
				redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
			} else if ($this->acceso('9')) {
				redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
			} else if ($this->acceso('12')) {
				redirect(base_url().$this->lang->lang().'/notas','refresh');
			} else if ($this->acceso('10')) {
				redirect(base_url().$this->lang->lang().'/trabajos','refresh');
			} else if ($this->acceso('13')) {
				redirect(base_url().$this->lang->lang().'/calendario','refresh');
			}
		}
	}
	
	//BUSCA LA SUCURSAL
	public function sucursal() {
		$this->comprobar_usuario('clientes');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3')) {
			if ($this->input->post()) {
				$this->load->model('M_sucursales');
				echo json_encode($this->M_sucursales->get_sucursal($this->input->post('su_id')));
			}
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}
	}
	
	//PAGINA PARA EDITAR SUCURSALES
	public function editar_sucursal($su_id=null) {
		if ($su_id != null) {
			$this->comprobar_usuario('sucursales/editar_sucursal/'.$su_id);	
		} else {
			$this->comprobar_usuario('sucursales');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6')) {
			if ($su_id == null) {
				redirect(base_url().$this->lang->lang().'/sucursales','refresh');
			} else {
				$this->load->model('M_sucursales');
				$sucursal = $this->M_sucursales->get_sucursal($su_id);
				$contadores = $this->M_sucursales->get_contadores($su_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($sucursal != null) {
					
					$sucursal->contadores = $contadores;
					
					$datos = array(
				        'TITULO' => lang('editar.sucursal'),
				        'EMPRESA' => 'active',
				        'SUCURSALES' => 'active',
						'BODY' => $this->load->view('v_sucursales_nuevo_editar',$sucursal,true),
						'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('sucursales'), 'href'=>base_url().$this->lang->lang().'/sucursales'],['nombre'=>lang('editar.sucursal'), 'href'=>''])
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/sucursales','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/sucursales','refresh');
		}
	}
	
	//CERRAR EL A�O DE FACTURACION DE LA SURCURSAL
	public function cerrar_anio() {
		if ($this->input->post('su_id') != null) {
			$this->comprobar_usuario('sucursales/editar_sucursal/'.$this->input->post('su_id'));	
		} else {
			$this->comprobar_usuario('sucursales');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
		$response['status'] = false;
		$response['editar'] = true;
		$response['sql'] = true;
		$response['errors'] = '';
		$errors = array();
		$aux = array();
		$entrar = false;
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6')) {
			if ($this->input->post('su_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				$this->load->model('M_sucursales');
				$response['sql'] = $this->M_sucursales->set_cerrar_anio($this->input->post('su_id'));
				$response['status'] = $response['sql'];
			}
		}
		
		foreach ($this->input->post() as $key => $value) {
		    if (form_error($key) != '') {
				$aux = array($key,form_error($key));
		       	array_push($errors,$aux);
			}
		}
		$response['errors'] = array_filter($errors);
		
		//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
		echo json_encode($response);
	}
	
	//PAGINA PARA A�ADIR SUCURSALES
	public function nueva_sucursal() {
		$this->comprobar_usuario('sucursales/nueva_sucursal');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6')) {
			if ($this->session->userdata('emp_crear') == 1) {
				$sucursal = array('su_id' => '','su_estado' => '1');
				$datos = array(
				    'TITULO' => lang('nueva.sucursal'),
				    'EMPRESA' => 'active',
					'SUCURSALES' => 'active',
					'BODY' => $this->load->view('v_sucursales_nuevo_editar',$sucursal,true),
					'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('sucursales'), 'href'=>base_url().$this->lang->lang().'/sucursales'],['nombre'=>lang('nueva.sucursal'), 'href'=>''])
				);
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/sucursales','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/sucursales','refresh');
		}	
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		if ($this->input->post('su_id') != '') {
			$url = 'sucursales/editar_sucursal/'.$this->input->post('su_id');	
		} else {
			$url = 'sucursales/nueva_sucursal';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('su_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('su_nombre',lang('nombre'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('su_telefono',lang('telefono'),'trim|required|max_length[15]|xss_clean');
					$this->form_validation->set_rules('su_email',lang('email'),'trim|required|valid_email|max_length[100]|xss_clean');
					$this->form_validation->set_rules('su_direccion',lang('direccion'),'trim|required|max_length[150]|xss_clean');
					$this->form_validation->set_rules('su_provincia',lang('provincia'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('su_postal',lang('postal'),'trim|max_length[11]|xss_clean');
					$this->form_validation->set_rules('su_barriada',lang('barriada'),'trim|max_length[100]|xss_clean');
					$this->form_validation->set_rules('su_localidad',lang('localidad'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('su_iva',lang('iva.general'),'trim|required|max_length[100]|numeric|xss_clean');
					$this->form_validation->set_rules('su_iva_reducido',lang('iva.reducido'),'trim|max_length[100]|numeric|xss_clean');
					$this->form_validation->set_rules('su_iva_superreducido',lang('iva.superreducido'),'trim|max_length[100]|numeric|xss_clean');
					
					if ($this->form_validation->run()) {
						$this->load->model('M_sucursales');
						$response['sql'] = $this->M_sucursales->set_sucursal($this->input->post());
						$response['status'] = $response['sql'];
					
						if($response['sql'] != false){
							$id = $response['sql'];
							$response['sql'] = true;
							$response['status'] = $response['sql'];
						}
					}
					
					foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors); 

				    //GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'SU'; //SUCURSAL
						$datos['asociado'] = $id;
						$datos['accion'] = ($this->input->post('su_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}
				}
			}
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/sucursales','refresh');
		}
	}
	
	//HABILITAMOS O DESHABILITAMOS
	public function estado() {
		//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
		$response['status'] = false;
		$response['editar'] = true;
		$response['sql'] = true;
		
		$this->comprobar_usuario('sucursales/editar_sucursal/'.$this->input->post('su_id'));	
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6')) {
			if ($this->session->userdata('emp_editar') == 1) {
				if ($this->input->post()) {
					$this->load->model('M_sucursales');
					$response['sql'] = $this->M_sucursales->set_estado($this->input->post('su_id'));
					$response['status'] = $response['sql'];
				}
			} else {
				$response['editar'] = false;
			}
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/sucursales','refresh');
		}
	}
	
	//DEVOLVEMOS LA LISTA COMPLETA DE PAISES
	public function paises() {
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6') || $this->acceso('8') || $this->acceso('4') || $this->acceso('11') || $this->acceso('14') || $this->session->userdata('emp_tipo') == 0) {
			$this->load->model('M_sucursales');
			$paises = json_encode($this->M_sucursales->get_paises());
			echo $paises;
		} else {
			//DENEGAMOS EL ACCESO
			if ($this->acceso('6')) {
				redirect(base_url().$this->lang->lang().'/sucursales','refresh');
			} else if ($this->acceso('8')) {
				redirect(base_url().$this->lang->lang().'/empleados','refresh');
			} else if ($this->acceso('4')) {
				redirect(base_url().$this->lang->lang().'/clientes','refresh');
			} else if ($this->acceso('11')) {
				redirect(base_url().$this->lang->lang().'/proveedores','refresh');
			} else if ($this->acceso('14')) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			}
		}
	}
}

/* End of file sucursales.php */
/* Location: ./application/controllers/sucursales.php */