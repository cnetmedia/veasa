<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Almacenes extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE ALMACENES
	public function index() {
		$this->comprobar_usuario('almacenes');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id')); 
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14')) {
			$acceso = $this->load->view('v_almacenes','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('almacenes'),
	        'STOCKAGE' => 'active',
			'ALMACENES' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('almacenes'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA ALMACENES PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('almacenes');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14') || $this->acceso('16') || $this->acceso('18') || $this->acceso('17')) {
			if ($this->input->post()) {
				$this->load->model('M_almacenes');
				$array = $this->M_almacenes->get_almacenes();
				
				echo json_encode($array);
			}
		} else {
			if ($this->acceso('14')) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else if ($this->acceso('16')) {
				redirect(base_url().$this->lang->lang().'/almacenes/entradas','refresh');
			} else if ($this->acceso('18')) {
				redirect(base_url().$this->lang->lang().'/almacenes/taller','refresh');
			} else if ($this->acceso('17')) {
				redirect(base_url().$this->lang->lang().'/salidas','refresh');
			}
		}
	}
	
	//BUSCA PRODUCTOS DEL ALMACEN
	public function buscador_productos_almacen() {
		$this->comprobar_usuario('almacenes/editar_almacen/'.$this->input->post('al_id'));
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14') || $this->acceso('18')) {
			if ($this->input->post()) {
				$this->load->model('M_almacenes');
				$almacen = $this->M_almacenes->get_almacen_productos($this->input->post());
				
				//AÑADIMOS LAS FOTOS
				//DIRECCION DE LAS FOTOS
				for ($i=0; $i<count($almacen); $i++) {
					$ruta = PRIVADO.$this->session->userdata("em_cif").'/pro_product/'.$almacen[$i]->product_id;
					
					$aux = '';
					//RECORRE LA CARPETA
					if (file_exists($ruta)) {
						//CREA UN OBJETO DE CARPETA
						$dirint = dir($ruta);
						
						$primera_vez = true;
						
						while (($archivo = $dirint->read()) !== false) {
							//SI NO ES JPG NO PASA
							if (preg_match("/jpg/", $archivo)){
								//CREA LA IMAGEN						
								$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/pro_product/'.$almacen[$i]->product_id.'/'.$archivo;
								
								$nombre = substr($archivo, 0, count($archivo)-5);
								
								//LISTADO DE IMAGENES
								if ($primera_vez) {
									$aux = $aux.'<a rel="product'.$almacen[$i]->product_id.'" class="fancybox" href="'.$img.'"><i class="fa fa-eye"></i></a>';
									
									$primera_vez = false;
								} else {
									$aux = $aux.'<a rel="product'.$almacen[$i]->product_id.'" class="fancybox oculto" href="'.$img.'"><img src="'.$img.'" /></a>';
								}
							}
						}
						
						//CERRAMOS CARPETA
						$dirint->close();
					}
					
					$almacen[$i]->fotos = $aux;
				}
				
				echo json_encode($almacen);
			}
		} else {
			if ($this->acceso('14')) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} if ($this->acceso('18')) {
				redirect(base_url().$this->lang->lang().'/taller','refresh');
			}
		}
	}
	
	//BUSCA PROYECTOS ASOCIADOS A UN PRODUCTO
	public function buscador_proyectos_producto() {
		$this->comprobar_usuario('almacenes/editar_almacen/'.$this->input->post('al_id'));
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14')) {
			
			if ($this->input->post('product_id') != '') {
				$this->load->model('M_almacenes');
				$product_id = $this->input->post('product_id');
				$al_id = $this->input->post('al_id');
				$pedidos_producto = $this->M_almacenes->get_pedidos_producto($product_id); //$this->input->post('product_id')
				$proyectos_producto = array();
				$cont = 0;
				
				if (count($pedidos_producto) > 0) {
					for ($i=0; $i<count($pedidos_producto); $i++) {
						
						if (($pedidos_producto[$i]->man_id != null) || ($pedidos_producto[$i]->man_id != '')) {
							$producto_recibido_almacen = array();
							
							$producto_recibido_almacen = explode(';',$pedidos_producto[$i]->product_cantidad_recibido_almacen);
							$producto_recibido_almacen = array_filter($producto_recibido_almacen);
							
							for ($j=0; $j<count($producto_recibido_almacen); $j++) {
								$aux_almacenes = explode(',',$producto_recibido_almacen[$j]);
								$aux_almacenes = array_filter($aux_almacenes);
								
								//Array aux_almacenes: [0] product_id, [1] cantidad, [2] al_id	
								if (($aux_almacenes[0] == $product_id) && ($aux_almacenes[2] == $al_id)) { //Encontre el producto y el almacen	
									$encontrado = false;
									$posicion = 0;
									$cant_entradas = 0;
									$cant_salidas = 0;
									$detalle = '';
									for ($k=0; $k<$cont; $k++) {
										if ($proyectos_producto[$k]->man_id == $pedidos_producto[$i]->man_id) {
											$encontrado = true;
											$posicion = $k;
											$cant_entradas = (float)$proyectos_producto[$k]->cantidad_entradas + (float)$aux_almacenes[1];
											
										}
									}
									if (!$encontrado) {
										$posicion = $cont;
										$cant_entradas = (float)$aux_almacenes[1];
										$cont++;
									}
									//Una vez actualizado todo, procedo a actualizar el registro
									$sep = ($proyectos_producto[$posicion]->entradas == '') ? '' : ',';
									$proyectos_producto[$posicion]->entradas .= $sep.$pedidos_producto[$i]->pe_id;
									$proyectos_producto[$posicion]->man_id = $pedidos_producto[$i]->man_id;
									$proyectos_producto[$posicion]->man_nombre = $pedidos_producto[$i]->man_nombre;
									$proyectos_producto[$posicion]->cantidad_entradas = $cant_entradas;
									$proyectos_producto[$posicion]->almacen_recibido = $al_id;
									$proyectos_producto[$posicion]->cantidad_salidas = 0;
								}
								
							}
							
						}
					}
					
					//Luego de buscar las entradas busco las salidas asociadas al producto, almacen y proyecto de lo almacenado
					for ($m=0; $m<count($proyectos_producto); $m++) {
						$salidas_producto = $this->M_almacenes->get_salidas_producto($product_id, $al_id, $proyectos_producto[$m]->man_id);
						if (count($salidas_producto) > 0) {
							for($n=0; $n<count($salidas_producto); $n++) {
								$proyectos_producto[$m]->cantidad_salidas = (float)$proyectos_producto[$m]->cantidad_salidas + (float)$salidas_producto[$n]->sa_cantidad;
							}
						}
					}
					
					//Luego de buscar las salidas puras, busco las salidas derivadas del trabajo de taller
					for ($o=0; $o<count($proyectos_producto); $o++) {
						$salidas_taller = $this->M_almacenes->get_salidas_taller_producto($product_id, $proyectos_producto[$o]->man_id);
						if (count($salidas_taller) > 0) {
							for($p=0; $p<count($salidas_taller); $p++) {
								$aux_salidas_taller_prod = explode(';',$salidas_taller[$p]->product_id);
								$aux_salidas_taller_cant = explode(';',$salidas_taller[$p]->product_cantidad);
								if (count($aux_salidas_taller_prod) > 0) {
									for($q=0; $q<count($aux_salidas_taller_prod); $q++) {
										if ($aux_salidas_taller_prod[$q] == $product_id) {
											$proyectos_producto[$o]->cantidad_salidas = (float)$proyectos_producto[$o]->cantidad_salidas + (float)$aux_salidas_taller_cant[$q];
										}
									}
								}
							}
						}
					}
					
				}
			}

			echo json_encode($proyectos_producto);
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		if ($this->input->post('al_id') != '') {
			$this->comprobar_usuario('almacenes/editar_almacen/'.$this->input->post('al_id'));	
		} else {
			$this->comprobar_usuario('almacenes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('14')) {
			if ($this->input->post()) {
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = false;
				$response['crear'] = true;
				$response['editar'] = true;
				$response['sql'] = true;
				$response['errors'] = '';
				$errors = array();
				$aux = array();
				$entrar = false;
				
				//VALIDAMOS LOS CAMPOS
				$this->form_validation->set_rules('al_telefono',lang('telefono'),'trim|max_length[15]|xss_clean');
				$this->form_validation->set_rules('al_barriada',lang('barriada'),'trim|max_length[50]|xss_clean');
				$this->form_validation->set_rules('al_localidad',lang('localidad'),'trim|max_length[100]required|xss_clean');
				$this->form_validation->set_rules('al_postal',lang('postal'),'trim|max_length[30]|xss_clean');
				$this->form_validation->set_rules('al_provincia',lang('provincia'),'trim|max_length[50]|required|xss_clean');
				$this->form_validation->set_rules('al_direccion',lang('direccion'),'trim|max_length[150]|required|xss_clean');
				$this->form_validation->set_rules('al_nombre',lang('nombre'),'trim|max_length[80]|xss_clean');
				
				if ($this->form_validation->run()) {
					if ($this->input->post('al_id') != '') {
						if ($this->session->userdata('emp_editar') == 0) {
							$response['editar'] = false;
						}
					} else {
						if ($this->session->userdata('emp_crear') == 0) {
							$response['crear'] = false;
						}
					}
					
					$this->load->model('M_almacenes');
					$response['sql'] = $this->M_almacenes->set_almacen($this->input->post());						
					$response['status'] = $response['sql'];

					if($response['sql'] != false){
						$id = $response['sql'];
						$response['sql'] = true;
						$response['status'] = $response['sql'];
					}
				}
				
				foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				       	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);
				
				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'AL'; //Almacen
					$datos['asociado'] = $id;
					$datos['accion'] = ($this->input->post('al_id') != '')?'Editar':'Crear';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	//PAGINA PARA AÑADIR ALMACENES
	public function nuevo_almacen() {
		$this->comprobar_usuario('almacenes/nuevo_almacen');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14')) {
			if ($this->session->userdata('emp_crear') == 1) {
				$almacen = array('al_id' => null,'al_tipo' => 0);
				$datos = array(
				    'TITULO' => lang('nuevo.almacen'),
				    'STOCKAGE' => 'active',
				   	'ALMACENES' => 'active',
					'BODY' => $this->load->view('v_almacenes_nuevo_editar',$almacen,true),
					'BREADCRUMB' => array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('almacenes'), 'href'=>base_url().$this->lang->lang().'/almacenes'],['nombre'=>lang('nuevo.almacen'), 'href'=>''])
				);
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}	
	}
	
	//PAGINA PARA EDITAR ALMACENES
	public function editar_almacen($al_id=null) {
		if ($al_id != null) {
			$this->comprobar_usuario('almacenes/editar_almacen/'.$al_id);	
		} else {
			$this->comprobar_usuario('almacenes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14')) {
			if ($al_id == null) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else {
				$this->load->model('M_almacenes');
				$almacen = $this->M_almacenes->get_almacen($al_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($almacen != null) {
					$datos = array(
				        'TITULO' => lang('editar.almacen').': '.$almacen->al_nombre,
				        'STOCKAGE' => 'active',
				        'ALMACENES' => 'active',
						'BODY' => $this->load->view('v_almacenes_nuevo_editar',$almacen,true),
						'BREADCRUMB' => array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('almacenes'), 'href'=>base_url().$this->lang->lang().'/almacenes'],['nombre'=>lang('editar.almacen'), 'href'=>''])
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/almacenes','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	//EDITAMOS EL STOCK DEL PRODUCTO GUARDADO EN EL ALMACEN
	public function actualizar_producto() {
		if ($this->input->post('al_id') != '') {
			$this->comprobar_usuario('almacenes/editar_almacen/'.$this->input->post('al_id'));	
		} else {
			$this->comprobar_usuario('almacenes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('14')) {
			if ($this->input->post()) {
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = false;
				$response['editar'] = $this->session->userdata('emp_editar');
				$response['sql'] = true;
				$response['errors'] = '';
				$errors = array();
				$aux = array();
				
				if ($response['editar'] == 1) {
					$this->load->model('M_almacenes');
					$response['sql'] = $this->M_almacenes->set_product_almacen($this->input->post());
					$response['status'] = $response['sql'];
				}	
				
				foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				       	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);
				
				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'PD'; //Almacen
					$datos['asociado'] = $this->input->post('product_id');
					$datos['accion'] = 'Actualizar';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	//MOVEMOS EL PRODUCO DE ALMACEN
	public function mover_producto() {
		if ($this->input->post('al_id') != '') {
			$this->comprobar_usuario('almacenes/editar_almacen/'.$this->input->post('al_id'));	
		} else {
			$this->comprobar_usuario('almacenes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('14')) {
			if ($this->input->post()) {
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = false;
				$response['editar'] = $this->session->userdata('emp_editar');
				$response['sql'] = true;
				$response['errors'] = '';
				$errors = array();
				$aux = array();
				
				if ($response['editar'] == 1) {
					$this->load->model('M_almacenes');
					$response['sql'] = $this->M_almacenes->set_mover_product_almacen($this->input->post());
					$response['status'] = $response['sql'];
				}	
				
				foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				       	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);
				
				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'PD'; //Almacen
					$datos['asociado'] = $this->input->post('product_id');
					$datos['accion'] = 'Mover';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	//AÑADIR PRODUCTO INDIVIDUAL DIRECTAMENTE AL ALMACEN
	public function anadir_producto() {
		if ($this->input->post('al_id') != '') {
			$this->comprobar_usuario('almacenes/editar_almacen/'.$this->input->post('al_id'));	
		} else {
			$this->comprobar_usuario('almacenes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('14')) {
			if ($this->input->post()) {
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = false;
				$response['editar'] = $this->session->userdata('emp_editar');
				$response['sql'] = true;
				$response['errors'] = '';
				$errors = array();
				$aux = array();
				
				if ($response['editar'] == 1) {
					$this->load->model('M_almacenes');
					$response['sql'] = $this->M_almacenes->set_anadir_product_almacen($this->input->post());
					$response['status'] = $response['sql'];

					if($response['sql'] != false){
						$id = $response['sql'];
						$response['sql'] = true;
						$response['status'] = $response['sql'];
					}
				}	
				
				foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				       	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);
				
				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'PD'; //Producto
					$datos['asociado'] = $this->input->post('product_id');
					$datos['accion'] = 'Enviar';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	public function enviar_producto() {
		if ($this->input->post('al_id') != null) {
			$this->comprobar_usuario('almacenes/editar_almacen/'.$this->input->post('al_id'));	
		} else {
			$this->comprobar_usuario('almacenes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14')) {
			if ($this->input->post('al_id') == null) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else {
				if ($this->input->post()) {
					//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
					$response['status'] = false;
					$response['editar'] = $this->session->userdata('emp_editar');
					$response['sql'] = true;
					$response['errors'] = '';
					$errors = array();
					$aux = array();
					
					if ($response['editar'] == 1) {
						$this->load->model('M_almacenes');
						$response['sql'] = $this->M_almacenes->set_salida_producto($this->input->post());
						$response['status'] = $response['sql'];
					}	
					
					foreach ($this->input->post() as $key => $value) {
					        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
					       	array_push($errors,$aux);
						}
					}
					$response['errors'] = array_filter($errors);
					
					//GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'PD'; //Producto
						$datos['asociado'] = $this->input->post('product_id');
						$datos['accion'] = 'Enviar';
						$this->M_bitacora->set_bitacora($datos);
					}

					//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
					echo json_encode($response);
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	public function enviar_productos() {
		if ($this->input->post('al_id') != null) {
			$this->comprobar_usuario('almacenes/editar_almacen/'.$this->input->post('al_id'));	
		} else {
			$this->comprobar_usuario('almacenes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14')) {
			if ($this->input->post('al_id') == null) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else {
				if ($this->input->post()) {
					//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
					$response['status'] = false;
					$response['editar'] = $this->session->userdata('emp_editar');
					$response['sql'] = true;
					$response['errors'] = '';
					$errors = array();
					$aux = array();
					
					if ($response['editar'] == 1) {
						$this->load->model('M_almacenes');
						$response['sql'] = $this->M_almacenes->set_salida_productos($this->input->post());
						$response['status'] = $response['sql'];
					}	
					
					foreach ($this->input->post() as $key => $value) {
					        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
					       	array_push($errors,$aux);
						}
					}
					$response['errors'] = array_filter($errors);
							
					//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
					echo json_encode($response);
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	//ENTRADAS DE PRODUCTOS (AUN NO SE GUARDA EN EL ALMACEN)
	public function entradas($tipo="") {
		$this->comprobar_usuario('almacenes/entradas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('16')) {
			$data_entradas = array('tipo_entrada' => $tipo);
			$acceso = $this->load->view('v_almacenes_entradas',$data_entradas,true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$titulo = ($tipo == "aprobadas") ? lang('entradas.aprobadas') : lang('entradas');
		$datos = array(
	        'TITULO' => $titulo,
	        'STOCKAGE' => 'active',
			'ENTRADAS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>$titulo, 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA PROVEEDORES PARA MOSTRAR ENTRADAS
	public function buscador_entradas_proveedores() {
		$this->comprobar_usuario('almacenes');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14') || $this->acceso('16') || $this->acceso('18') || $this->acceso('17')) {
			if ($this->input->post()) {
				$this->load->model('M_almacenes');
				$array = $this->M_almacenes->get_almacenes_entradas_proveedores();
				
				echo json_encode($array);
			}
		} else {
			if ($this->acceso('14')) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else if ($this->acceso('16')) {
				redirect(base_url().$this->lang->lang().'/almacenes/entradas','refresh');
			} else if ($this->acceso('18')) {
				redirect(base_url().$this->lang->lang().'/almacenes/taller','refresh');
			} else if ($this->acceso('17')) {
				redirect(base_url().$this->lang->lang().'/salidas','refresh');
			}
		}
	}
	
	//BUSCA ENTRADAS DE LOS ALMACENES
	public function buscador_entradas($tipo="") {
		$this->comprobar_usuario('almacenes/entradas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('16') || $this->acceso('20')) {
			if ($this->input->post()) {
				$this->load->model('M_almacenes');
				$entradas = $this->M_almacenes->get_almacenes_entradas($tipo);
				
				//$this->load->model('M_proveedores');
				for ($i=0; $i<count($entradas); $i++) {	
					//DATOS DEL EMPLEADO QUE REALIZO EL PEDIDO
					$this->load->model('M_empleados');
					$empleado = $this->M_empleados->get_empleado_id($entradas[$i]->emp_id);
					$entradas[$i]->empleado = $empleado->emp_nombre.' '.$empleado->emp_apellido1.' '.$empleado->emp_apellido2;
					
					//EMPLEADO QUE APROBO EL PEDIDO
					$emp_aprobado = $this->M_empleados->get_empleado_id($entradas[$i]->emp_id_aprobado);
					$entradas[$i]->empleado_aprobado = $emp_aprobado->emp_nombre.' '.$emp_aprobado->emp_apellido1.' '.$emp_aprobado->emp_apellido2;
					
					//EMPLEADOS QUE RECIBIERON LOS PRODUCTOS
					$aux = $entradas[$i]->emp_id_recepcion;
					$aux = explode(';',$aux);
					$aux = array_filter($aux);
					$em_recepcion = array();
					for ($e=0; $e<count($aux); $e++) {
						$emp = $this->M_empleados->get_empleado_id($aux[$e]);
						array_push($em_recepcion,$emp);
					}
					$entradas[$i]->empleados_recepcion = $em_recepcion;
					
					$proveedores = array();
					$products = array();
					
					$productos = explode(';',$entradas[$i]->product_id);
					$productos = array_filter($productos);
                                        
					
					//ALMACENES QUE GUARDARON EL PRODUCTO
					$almacenes = explode(';',$entradas[$i]->product_cantidad_recibido_almacen);
					$almacenes = array_filter($almacenes);
					$almacenes_guardado = array();
					
					for ($a=0; $a<count($almacenes); $a++) {
						$aux_id_al = '';
						$aux_prod_id = '';
						$suma_guardados = 0;
						$encontrado = false;
						
						$aux_almacenes = explode(',',$almacenes[$a]);
						$aux_almacenes = array_filter($aux_almacenes);
						
						for ($o=0; $o<count($almacenes_guardado); $o++) {
							if ($almacenes_guardado[$o][0] == $aux_almacenes[0] && $almacenes_guardado[$o][2] == $aux_almacenes[2]) {
								$encontrado = true;
							}
						}
						
						if (!$encontrado) {
							for ($w=0; $w<count($almacenes); $w++) {
								$aux_almacenes2 = explode(',',$almacenes[$w]);
								$aux_almacenes2 = array_filter($aux_almacenes2);
								if ($aux_almacenes[0] == $aux_almacenes2[0] && $aux_almacenes[2] == $aux_almacenes2[2]) {
									$suma_guardados = (int)$suma_guardados + (int)$aux_almacenes2[1];
									$aux_id_al = $aux_almacenes[2];
									$aux_prod_id = $aux_almacenes[0];
								}
							}
							
							$almacen = $this->M_almacenes->get_almacen($aux_id_al);
							$alm = array($aux_prod_id,$suma_guardados,$aux_id_al,$almacen->al_nombre);
							array_push($almacenes_guardado,$alm);
						}
					}
					$entradas[$i]->almacenes = $almacenes_guardado;

					
					//OBTENEMOS LOS NOMBRES DE LOS PROVEEDORES, REFERENTE A LOS PRODUCTOS DEL PEDIDO
					$this->load->model('M_proveedores');

					for ($y=0; $y<count($productos); $y++) {
						$producto = $this->M_proveedores->get_producto($productos[$y]);
						
                        $proveedor = $this->M_proveedores->get_proveedor($producto->pro_id);
						
						if (array_search($proveedor->pro_nombre, $proveedores) === false) {
							array_push($proveedores,$proveedor->pro_nombre);
							
						}
						
						$prod = array($producto->product_id,$producto->product_nombre);
						array_push($products,$prod);
					}
					//$proveedores = substr($proveedores, 0, -3);
					$entradas[$i]->proveedores = $proveedores;
					
					//$products = substr($products, 0, -3);
					$entradas[$i]->productos = $products;
					
					//OBTENEMOS EL NOMBRE EL PROYECTO PARA EL QUE SE REALIZO EL PEDIDO
					$this->load->model('M_presupuestos');
					$presupuesto = $this->M_presupuestos->get_presupuesto($entradas[$i]->pr_id);
					$entradas[$i]->man_nombre = $presupuesto->man_nombre;
				}
				
				echo json_encode($entradas);
			}
		} else {
			if ($this->acceso('16') ) {
				redirect(base_url().$this->lang->lang().'/almacenes/entradas','refresh');
			} else if ($this->acceso('20') ) {
				redirect(base_url().$this->lang->lang().'/informes','refresh');
			}
			
		}
	}
	
	//PAGINA PARA EDITAR ENTRADAS (PEDIDOS)
	public function editar_entrada($pe_id=null) {
		if ($pe_id != null) {
			$this->comprobar_usuario('almacenes/editar_entrada/'.$pe_id);	
		} else {
			$this->comprobar_usuario('almacenes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('16')) {
			if ($pe_id == null) {
				redirect(base_url().$this->lang->lang().'/almacenes/entradas','refresh');
			} else {
				$this->load->model('M_almacenes');
				$entrada = $this->M_almacenes->get_almacenes_entrada($pe_id);
				
				//TITULO DE LA ENTRADA
				$titulo_editar_entrada = lang('editar.entrada');
				if ($entrada->pe_estado == 0) { //pendiente
					$titulo_editar_entrada = lang('editar.entrada.pendiente');
					$breadcrumb = array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('entradas'), 'href'=>base_url().$this->lang->lang().'/almacenes/entradas'],['nombre'=>lang('editar.entrada'), 'href'=>'']);
				}
				if ($entrada->pe_estado == 1) { //Aprobado
					$titulo_editar_entrada = lang('editar.entrada.aprobada');
					$breadcrumb = array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('entradas.aprobadas'), 'href'=>base_url().$this->lang->lang().'/almacenes/entradas/aprobadas'],['nombre'=>lang('editar.entrada.aprobada'), 'href'=>'']);
				}
				
				//EMPLEADOS IMPLICADOS EN LA ENTRADA
				$this->load->model('M_empleados');
				$entrada->empleado_solicito = $this->M_empleados->get_empleado_id($entrada->emp_id);
				$entrada->empleado_aprobo = $this->M_empleados->get_empleado_id($entrada->emp_id_aprobado);
				$entrada->empleado_aprobo_dept_prod = $this->M_empleados->get_empleado_id($entrada->emp_id_aprobado_dept_prod);

				$aux = $entrada->emp_id_recepcion;
				$aux = explode(';',$aux);
				$aux = array_filter($aux);
				$em_recepcion = array();
				for ($e=0; $e<count($aux); $e++) {
					$emp = $this->M_empleados->get_empleado_id($aux[$e]);
					array_push($em_recepcion,$emp);
				}
				$entrada->empleados_recepcion = $em_recepcion;
				
				//MANTENIMIENTO (PROYECTO) DE LA ENTRADA
				$this->load->model('M_presupuestos');
				$entrada->man_nombre = '';
				$presupuesto = $this->M_presupuestos->get_presupuesto($entrada->pr_id);
				$entrada->man_nombre = $presupuesto->man_nombre;
				$entrada->man_id = $presupuesto->man_id;
				$entrada->moneda = $presupuesto->currrency_symbol;
				
				
				//PRODUCTOS DE LA ENTRADA
				$productos = $entrada->product_id;
				$productos = explode(';',$productos);
				$productos = array_filter($productos);
				
				//CANTIDADES DE CADA PRODUCTO DE LA ENTRADA
				$cantidad = $entrada->product_cantidad;
				$cantidad = explode(';',$cantidad);
				$cantidad = array_filter($cantidad);
				
				//CANTIDADES RECIBIDAS DE CADA PRODUCTO DE LA ENTRADA
				//$cantidad_recibido = '';
				//if ($entrada->product_cantidad_recibido != null) {
				$cantidad_recibido = $entrada->product_cantidad_recibido;
				$cantidad_recibido = explode(';',$cantidad_recibido);
				//$cantidad_recibido = array_filter($cantidad_recibido);
				//}
				
				$this->load->model('M_proveedores');
				$array_productos = array();
				$cont=0;
				for ($i=0; $i<count($productos); $i++) {
					$producto = $this->M_proveedores->get_producto($productos[$i]);
					$producto->cantidad = $cantidad[$i];
					
					/*if ($cantidad_recibido != '') {
						$producto->product_cantidad_recibido = $cantidad_recibido[$i];
					} else {
						$producto->product_cantidad_recibido = $cantidad_recibido;
					}*/
					
					$producto->product_cantidad_recibido = $cantidad_recibido[$i];
					
					$proveedor = $this->M_proveedores->get_proveedor($producto->pro_id);
					$producto->pro_nombre = $proveedor->pro_nombre;
					array_push($array_productos, $producto);
					$cont++;
				}
				
				$entrada->productos = $array_productos;
				
				$entrada->cont=$cont;
				
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($entrada != null) {
					$datos = array(
				        'TITULO' => $titulo_editar_entrada,
				        'STOCKAGE' => 'active',
				        'ENTRADAS' => 'active',
						'BODY' => $this->load->view('v_almacenes_entradas_editar',$entrada,true),
						'BREADCRUMB' => $breadcrumb
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/almacenes/entradas','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes/entradas','refresh');
		}
	}
	
	/* ACTUALIZAMOS INFORMACION DE LA ENTRADA (PEDIDO) */
	public function actualizar_entrada() {
		if ($this->input->post('pe_id') != '') {
			$this->comprobar_usuario('almacenes/editar_entrada/'.$this->input->post('pe_id') );
		} else {
			$this->comprobar_usuario('almacenes/entradas');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('16')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			
			if ($this->input->post()) {
				//print_r($this->input->post()); exit();
				$this->load->model('M_almacenes');
				$response['sql'] = $this->M_almacenes->set_entrada($this->input->post());
				$response['status'] = $response['sql'];
				
				//ENVIAMOS EMAIL DE CONFIRMACION AL PROVEEDOR
				if ($response['status'] && $this->input->post('pe_estado') == 1) {
					$this->pedido_email_proveedor($this->input->post('pe_id'),1);
				}
				
				foreach ($this->input->post() as $key => $value) {
					if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				      	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);

				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'SC'; //Solicitud Cotizacion
					$datos['asociado'] = $this->input->post('pe_id');
					$datos['accion'] = 'Editar';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'almacenes/entradas','refresh');
		}
	}
	
	/* REENVIAMOS EL EMAIL DEL PEDIDO AL PROVEEDOR */
	public function pedido_email_proveedor() {
		$pe_id = $this->input->post('pe_id');
		$confirmacion = $this->input->post('confirmacion');
		$adjuntos = $this->input->post('adjuntos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('16')) {
			
			$this->load->model('M_empresa');
			$empresa = $this->M_empresa->get_empresa($this->session->userdata('em_id'));
			$this->load->model('M_almacenes');
			$this->load->model('M_proveedores');
			$entrada = $this->M_almacenes->get_almacenes_entrada($pe_id);
			
			//CONVERTIMOS EN ARRAY EL STRING PASADO POR AJAX
			$pro_id = explode(';',$this->input->post('pro_id'));
			$pro_id = array_filter($pro_id);
			
			$product_cantidad = explode(';',$this->input->post('product_cantidad'));
			$product_cantidad = array_filter($product_cantidad);
			
			$product_id = explode(';',$this->input->post('product_id'));
			$product_id = array_filter($product_id);
					
			//VARIABLES AUXILIARES PARA LOS EMAILS
			$product_cantidad_aux = array();
			$product_nombre_aux = array();
			$product_referencia_aux = array();
			$product_caracteristicas_aux = array();
			
			$resultado = false;
			
			//RECORREMOS TODOS LOS PROVEEDORES
			for ($i=0; $i<count($pro_id); $i++) {
				//RECOGEMOS LOS DATOS DEL PRODUCTO
				$producto = $this->M_proveedores->get_producto($product_id[$i]);
					
				//AÑADIMOS EL PRODUCTO A LA LISTA PARA EL EMAIL
				array_push($product_cantidad_aux,$product_cantidad[$i]);
				array_push($product_nombre_aux,$producto->product_nombre);
				array_push($product_referencia_aux,$producto->product_referencia);
				
				//CARACTERISTICAS DEL PRODUCTO
				$caracteristicas = $producto->product_caracteristicas;
				
				$caracteristicas = explode(';',$caracteristicas);
				$caracteristicas = array_filter($caracteristicas);
				$opcion_aux = '';

				
				for ($y=0; $y<count($caracteristicas); $y++) {
					$aux = explode('/',$caracteristicas[$y]);
					$aux = array_filter($aux);
					
					//OCULTAMOS EL PRECIO DE VENTA AL PROVEEDOR
					if (strpos(strtolower($aux[0]), 'tarifa') === FALSE) {
						$opcion_aux .= $aux[0].': '.$aux[1].', ';
					}
				}
				$opcion_aux = substr($opcion_aux, 0, -2);
				array_push($product_caracteristicas_aux, $opcion_aux);
						
				//PREGUNTAMOS SI EL SIGUIENTE PROVEEDOR ES IGUAL O NO PARA ENVIAR YA EL EMAIL
				$enviar = false;
				if (isset($pro_id[$i+1])) {
					if ($pro_id[$i] != $pro_id[$i+1]) {
						$enviar = true;
					}
				} else {
					$enviar = true;
				}
				
				if ($enviar) {
					//EXTRAEMOS EL EMAIL DEL PROVEEDOR
					$proveedor = $this->M_proveedores->get_proveedor($pro_id[$i]);
					
					//EXTRAEMOS EL NOMBRE DEL EMPLEADO
					$this->load->model('M_empleados');
					$empleado = $this->M_empleados->get_empleado_id($this->session->userdata('emp_id'));
					
					//GENERAMOS EL EMAIL
					$this->load->library('email');
					$pathFiles = FCPATH.'/img/dynamic/adjuntos-carrito/';
					$nombre = utf8_decode($this->session->userdata('em_nombre'));
					$this->email->from(EMAIL, $nombre);
					
					$textPedido = ' - ' . $proveedor->pro_nombre . ' (' . lang('pedido.nro') . ' ' . $entrada->pe_id . ')';
					if($entrada->pe_estado == 1){
						$subject = utf8_decode(lang('confirmacion.pedido.carrito') . $textPedido  );	
					}else{
						$subject = utf8_decode(lang('pedido.carrito') . $textPedido );						
					}
					
					if ($confirmacion == 1) {
						$list = array($empresa->em_email_contabilidad,$proveedor->pro_email,$empresa->em_email_opcional,$empresa->em_email_opcional2);
						$this->email->to($list);
						$this->email->subject($subject);
					} else {
						$this->email->to($proveedor->pro_email);
						$this->email->subject($subject);
					}
					
					$productos = array(
						'nombre' => $product_nombre_aux,
						'cantidad' => $product_cantidad_aux,
						'referencia' => $product_referencia_aux,
						'caracteristicas' => $product_caracteristicas_aux,
						'nombre_proveedor' => $proveedor->pro_nombre,
						'nombre_empleado' => $empleado->emp_nombre,
						'apellido1_empleado' => $empleado->emp_apellido1,
						'apellido2_empleado' => $empleado->emp_apellido2,
						'fecha' => $entrada->pe_fecha,
						'observaciones' => $entrada->pe_observaciones_proveedor,
						'pe_id' => $entrada->pe_id,
						'confirmacion' => $confirmacion
					);

					if(!empty($adjuntos)){
						$adjunto = explode('|',$adjuntos );
						$pedido = array(
							'pe_adjunto1' => $adjuntos,
							'pe_id' => $entrada->pe_id
						);

						/*$this->M_proveedores->set_adjuntos_pedido($pedido);*/
						foreach ($adjunto as $ind => $file) {
							if(!empty($file) && $file != '' && strlen($file) > 0){
								$this->email->attach($pathFiles.$file);
								$response['adjuntos'][] = $pathFiles.$file;								
							}
						}				
					}
							
					$data = array(
						'BODY' => $this->load->view('emails/v_email_procesar_pedido', $productos, TRUE)
					);
							
					$email = $this->load->view('emails/v_email', $data, TRUE);
					$this->email->message($email);
					
					if ($this->email->send()) {
						$resultado = true;
					} else {
						$resultado = false;
					}
					
					unset($product_cantidad_aux);
					unset($product_nombre_aux);
					unset($product_referencia_aux);
					unset($product_caracteristicas_aux);
					$product_cantidad_aux = array();
					$product_nombre_aux = array();
					$product_referencia_aux = array();
					$product_caracteristicas_aux = array();
				}
			}

			echo $resultado;
		} else {
			redirect(base_url().$this->lang->lang().'almacenes/entradas','refresh');
		}
	}
	
	//PAGINA PARA VER LAS SALIDAS
	public function salidas() {
		$this->comprobar_usuario('almacenes/salidas');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('17')) {
			$acceso = $this->load->view('v_almacenes_salidas','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('salidas'),
	        'STOCKAGE' => 'active',
			'SALIDAS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('salidas'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA LAS SALIDAS DEL TALLER PARA MOSTRAR
	public function buscador_salidas() {
		$this->comprobar_usuario('almacenes/salidas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('17') || $this->acceso('20')) {
			if ($this->input->post()) {
				$this->load->model('M_almacenes');
				$this->load->model('M_empleados');
				$estado = ($this->input->post("estado") == 1) ? true:false;
				$array = $this->M_almacenes->get_almacenes_salidas($estado);
				
				for ($i=0; $i<count($array); $i++) {
					$solicito = $this->M_empleados->get_empleado_id($array[$i]->emp_id);
					$array[$i]->emp_solicito = $solicito->emp_nombre.' '.$solicito->emp_apellido1.' '.$solicito->emp_apellido2;
					
					$aprobo = $this->M_empleados->get_empleado_id($array[$i]->emp_id_aprobo);
					$array[$i]->emp_aprobo = $aprobo->emp_nombre.' '.$aprobo->emp_apellido1.' '.$aprobo->emp_apellido2;
					
					$cancelo = $this->M_empleados->get_empleado_id($array[$i]->emp_id_cancelo);
					$array[$i]->emp_cancelo = $cancelo->emp_nombre.' '.$cancelo->emp_apellido1.' '.$cancelo->emp_apellido2;
					
					$salida = $this->M_empleados->get_empleado_id($array[$i]->emp_id_salida);
					$array[$i]->emp_salida = $salida->emp_nombre.' '.$salida->emp_apellido1.' '.$salida->emp_apellido2;
				}
				
				echo json_encode($array);
			}
		} else {
			if ($this->acceso('17')) {
				redirect(base_url().$this->lang->lang().'/almacenes/salidas','refresh');
			} else if ($this->acceso('20')) {
				redirect(base_url().$this->lang->lang().'/informes','refresh');
			}
			
		}
	}
        
        //GUARDAMOS LA SALIDA MANUAL
	public function salida_manual() {
		$this->comprobar_usuario('salidas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('17')) {
			if ($this->input->post()) {
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = false;
				$response['sql'] = true;
				$response['errors'] = '';
				$errors = array();
				$aux = array();
				
				if ($this->session->userdata('emp_crear') == 0) {
                                    $response['crear'] = false;
				}
						
				$this->load->model('M_almacenes');
				$response['sql'] = $this->M_almacenes->set_salida_manual($this->input->post());						
				$response['status'] = $response['sql'];

				if($response['sql'] != false){
					$id = $response['sql'];
					$response['sql'] = true;
					$response['status'] = $response['sql'];
				}
				
				foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				       	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);
				
				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'SA'; //Salida
					$datos['asociado'] = $id;
					$datos['accion'] = 'Crear';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/salidas','refresh');
		}
	}
        
        //CAMBIAR ESTADO DE LAS SALIDAS
	public function cambiar_estados_salidas() {
		$this->comprobar_usuario('salidas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('17')) {
			if ($this->input->post()) {
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = false;
				$response['sql'] = true;
				$response['errors'] = '';
				$errors = array();
				$aux = array();
						
				$this->load->model('M_almacenes');
				$response['sql'] = $this->M_almacenes->set_estado_salida($this->input->post());						
				$response['status'] = $response['sql'];
				
				foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				       	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);
						
				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/salidas','refresh');
		}
	}
        
        //MOSTRAR TODOS LOS MATERIALES DE TODOS LOS ALMACENES
	public function buscar_materiales_almacenes() {
		$this->comprobar_usuario('salidas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('17')) {
			if ($this->input->post()) {
				$this->load->model('M_almacenes');
				$array = $this->M_almacenes->get_productos_almacenes();
				
				echo json_encode($array);
			}
		} else {
                    redirect(base_url().$this->lang->lang().'/salidas','refresh');
		}
	}
        
    //GUARDAMOS ENTRADAS MANUALES
    public function guardar_entrada_manual() {
        $this->comprobar_usuario('almacenes/entradas');

		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('16')) {
			if ($this->input->post()) {
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = false;
				$response['crear'] = true;
				$response['sql'] = true;
				$response['errors'] = '';
				$errors = array();
				$aux = array();
				$entrar = false;
				
					
				if ($this->session->userdata('emp_crear') == 0) {
					if ($this->session->userdata("de_id") != '5') {
						$response['crear'] = false;
					}
				}
			
                if ($response['crear']) {
                    $this->load->model('M_almacenes');
                    $response['sql'] = $this->M_almacenes->set_entrada_manual($this->input->post());						
                    $response['status'] = $response['sql'];

                    if($response['sql'] != false){
						$id = $response['sql'];
						$response['sql'] = true;
						$response['status'] = $response['sql'];
					}
                }
					
				
				
				foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				       	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);	
				
				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'SC'; //Solicitud Cotizacion
					$datos['asociado'] = $id;
					$datos['accion'] = 'Crear';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes/entradas','refresh');
		}
    }

    public function upload_adjuntos(){
		$archivos = '';
		$pe_id = null;
		$response = array();
		$allInputs = $this->input->post(); 

		foreach($_FILES as $index => $file){
			$pe_id = explode('-', $index)[1];
			// for easy access
			$fileName     = $file['name'];
			// for easy access
			$fileTempName = $file['tmp_name'];
			// check if there is an error for particular entry in array
			if(!empty($file['error'][$index])){
				$response['errors'] = true;
				$response['status'] = false;
				echo json_encode($response);
			}
	 
			if(!empty($fileTempName) && is_uploaded_file($fileTempName)){
				// move the file from the temporary directory to somewhere of your choosing
				move_uploaded_file($fileTempName, "img/dynamic/adjuntos-carrito/" . $fileName);
				$archivos .= $fileName . '|';
			}
		}

		$adjIniciales = array();
		if(!empty($allInputs['adjuntos_iniciales'])){
			$adjIniciales = explode('|', $allInputs['adjuntos_iniciales']);			
		}
		$adjEliminados = array();
		if(!empty($allInputs['eliminados'])){
			$adjEliminados = explode(',', $allInputs['eliminados']);
		}

		foreach ($adjIniciales as $ind => $file) {
			if(!in_array($file, $adjEliminados)){
				$archivos .= $file . '|';
			}else{
				//eliminamos el archivo de carpeta
				unlink(FCPATH.'/img/dynamic/adjuntos-carrito/' . $file);
			}
		}		

		$pedido['pe_id'] = $pe_id;
		$pedido['pe_adjunto1'] = $archivos;

		$response['result'] = $pedido['pe_adjunto1'];	
		$this->load->model('M_proveedores');	
		$response['sql'] = $this->M_proveedores->set_adjuntos_pedido($pedido);
		$response['status'] = $response['sql'];
		echo json_encode($response);
	}
}

/* End of file almacenes.php */
/* Location: ./application/controllers/almacenes.php */