<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE CLIENTES
	public function index() {
		$this->comprobar_usuario('clientes');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('4')) {
			$acceso = $this->load->view('v_clientes','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('clientes'),
	        'CLIENTES' => 'active',
			'BODY' => $acceso
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA CLIENTES PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('clientes');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('4') || $this->acceso('5')) {
			if ($this->input->post()) {
				$this->load->model('M_clientes');
				$array = $this->M_clientes->get_clientes();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			if ($this->acceso('4')) {
				redirect(base_url().$this->lang->lang().'/clientes','refresh');
			} else if ($this->acceso('5')) {
				redirect(base_url().$this->lang->lang().'/mantenimientos','refresh');
			}
		}
	}
	
	//BUSCA CLIENTES PARA MOSTRAR SEGUN LA SUCURSAL
	public function clientes_sucursal() {
		$this->comprobar_usuario('clientes');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3') || $this->acceso('9') || $this->acceso('10')) {
			if ($this->input->post()) {
				$this->load->model('M_clientes');
				echo json_encode($this->M_clientes->get_clientes_sucursal($this->input->post('su_id')));
			}
		} else {
			if ($this->acceso('3')) {
				redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
			} else if ($this->acceso('9')) {
				redirect(base_url().$this->lang->lang().'/inspecciones','refresh');
			} else if ($this->acceso('10')) {
				redirect(base_url().$this->lang->lang().'/trabajos','refresh');
			}
		}
	}
	
	//BUSCA EL CLIENTE PARA MOSTRAR
	public function cliente() {
		$this->comprobar_usuario('clientes');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3')) {
			if ($this->input->post()) {
				$this->load->model('M_clientes');
				echo json_encode($this->M_clientes->get_cliente($this->input->post('cl_id')));
			}
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}
	}
	
	//PAGINA PARA AÑADIR CLIENTES
	public function nuevo_cliente() {
		$this->comprobar_usuario('clientes/nuevo_cliente');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('4')) {
			$cliente = array('cl_id' => null);
			$datos = array(
		        'TITULO' => lang('nuevo.cliente'),
		        'CLIENTES' => 'active',
				'BODY' => $this->load->view('v_clientes_nuevo_editar',$cliente,true),
				'BREADCRUMB' => array(['nombre'=>lang('clientes'), 'href'=>base_url().$this->lang->lang().'/clientes'],['nombre'=>lang('nuevo.cliente'), 'href'=>''])
		    );
			$this->load->view('v_admin',$datos);
		} else {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
	}
	
	//PAGINA PARA EDITAR CLIENTES
	public function editar_cliente($cl_id=null) {
		if ($cl_id != null) {
			$this->comprobar_usuario('clientes/editar_cliente/'.$cl_id);	
		} else {
			$this->comprobar_usuario('clientes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('4')) {
			if ($cl_id == null) {
				redirect(base_url().$this->lang->lang().'/clientes','refresh');
			} else {
				$this->load->model('M_clientes');
				$cliente = $this->M_clientes->get_cliente($cl_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($cliente != null) {
					$datos = array(
				        'TITULO' => lang('editar.cliente'),
				        'CLIENTES' => 'active',
						'BODY' => $this->load->view('v_clientes_nuevo_editar',$cliente,true),
						'BREADCRUMB' => array(['nombre'=>lang('clientes'), 'href'=>base_url().$this->lang->lang().'/clientes'],['nombre'=>lang('editar.cliente'), 'href'=>''])
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/clientes','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/clientes','refresh');
		}
	}
	
	//PREGUNTAMOS SI EXISTE UN CLIENTE CON ESTE DNI
	public function cliente_dni($dni) {
		$this->load->model('M_clientes');
		$cliente = $this->M_clientes->get_cliente_dni($dni);
		if ($cliente->num_rows() > 0) {
			$this->form_validation->set_message('cliente_dni', lang('cliente.existente'));
			return false;
		} else {
			return true;
		}
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		if ($this->input->post('cl_id') != '') {
			$this->comprobar_usuario('clientes/editar_cliente/'.$this->input->post('cl_id'));	
		} else {
			$this->comprobar_usuario('clientes');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('4')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('cl_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('cl_nombre',lang('nombre'),'trim|required|max_length[80]|xss_clean');
					$this->form_validation->set_rules('cl_telefono',lang('telefono'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('cl_email',lang('email'),'trim|required|valid_email|max_length[50]|xss_clean');
					$this->form_validation->set_rules('cl_direccion',lang('direccion'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('cl_provincia',lang('provincia'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('cl_postal',lang('postal'),'trim|max_length[11]|xss_clean');
					$this->form_validation->set_rules('cl_barriada',lang('barriada'),'trim|max_length[50]|xss_clean');
					$this->form_validation->set_rules('cl_localidad',lang('localidad'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('cl_pais',lang('pais'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('cl_emails',lang('emails'),'trim|max_length[2000]|xss_clean');
					$this->form_validation->set_rules('cl_id','','trim|xss_clean');
					$this->form_validation->set_rules('foto','error','trim|xss_clean');
					
					if ($this->input->post('cl_id') == '') {
						$this->form_validation->set_rules('cl_dni',lang('dni'),'trim|required|max_length[30]|callback_cliente_dni|xss_clean');
					} else {
						$this->form_validation->set_rules('cl_dni',lang('dni'),'trim|required|max_length[30]|xss_clean');
					}
					
					if ($this->form_validation->run()) {					
						if (!empty($_FILES['foto'])) {
							$name2 = $_FILES['foto']['name']; //get the name of the image
							list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
							
							if ($ext == 'jpg') {
								if ($_FILES['foto']['size'] <= 500000) {
									$response['status'] = true;
								} else {
									$f = array('foto',lang('max.size'));
									array_push($errors,$f);
								}
							} else {
								$f = array('foto',lang('jpg'));
								array_push($errors,$f);
							}	
						} else {
							$response['status'] = true;
						}
						
						if ($response['status']) {
							$this->load->model('M_clientes');
							$response['sql'] = $this->M_clientes->set_cliente($this->input->post());
							$response['status'] = $response['sql'];

							if($response['sql'] != false){
								$id = $response['sql'];
								$response['sql'] = true;
								$response['status'] = $response['sql'];
							}
						}
					}
					
					foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors);

				    //GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'CL'; //Clientes
						$datos['asociado'] = $id;
						$datos['accion'] = ($this->input->post('cl_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}

				    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				    echo json_encode($response);
				}
			} else {
				redirect(base_url().$this->lang->lang().'/clientes','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/clientes','refresh');
		}
	}
	
	/*
	FIN CLIENTES DE LA APLICACION
	INICIO INFORMACION DE CLIENTES
	*/
	
	//BOTON ENVIAR DEL LOGIN
	public function entrar() {
			//REGUNTAMOS SI EXISTE ENVIO DE INFORMACION POR POST
			if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
				
				$this->load->library('aes');
				$password = AesCtr::decrypt($this->input->post('cl_clave'), $this->session->userdata('token'), 256);
				
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = FALSE;
				$errors = array();
				$aux = array();
				//VALIDAMOS LOS CAMPOS
				$this->form_validation->set_rules('cliente',lang('email.mantenimiento'),'trim|required|max_length[50]|xss_clean');
				$this->form_validation->set_rules($password,lang('login.clave'),'trim|exact_length[6]|max_length[6]|xss_clean');
				//COMPROBAMOS LA VALIDACION
				if ($this->form_validation->run()) {
					//RECOGEMOS LOS CAMPOS ENVIADOS
					$cliente = $this->input->post('cliente');
		            //$password = $this->input->post('cl_clave');
					
					//SI NO ENCUENTRA @ ES UN MANTENIMIENTO
					if (strpos($cliente, '@') === false) {
						//CARGAMOS EL MODELO
						$this->load->model('M_mantenimientos');
						//LLAMAMOS AL METODO DEL MODELO QUE NOS DEVUELVE EL CLIENTE O NULL
						$user = $this->M_mantenimientos->get_mantenimiento_cliente($cliente);
						//SI LO ENCUENTRA ENTRA
						if ($user != null) {
								//PREGUNTAMOS SI LA CLAVE ES IGUAL Y CARGAMOS LA LIBERIA ENCRYPT
								$this->load->library('encrypt');
								if ($this->encrypt->decode($user->man_clave) == $password) {
									//VALIDAMOS OK Y GUARDAMOS LA INFO EN LA SESSION
									$response['status'] = true;
									//CREAMOS LA SESSION
									$this->cl_session($cliente);
				                    //ELIMINAMOS LA URL DE LA SESSION
				                    $url = $this->session->userdata('url');
				                    //SI EXISTE URL LA PASAMOS SINO VAMOS A UNA POR DEFECTO
				                    if (!isset($url) || (trim($url) != '')) {
				                    	$this->session->unset_userdata('url');
				                    	$response['url'] = base_url().$this->lang->lang()."/".$url;
				                    } else {
										$response['url'] = base_url().$this->lang->lang().'/clientes/panel_mantenimiento';
									}
								} else {
									$aux = array('cl_clave',lang('login.clave.error'));
									array_push($errors, $aux);
								}
						} else {
							$aux = array('cliente',lang('login.mantenimiento.error'));
							array_push($errors, $aux);
						}
					} else {
						//ENCONTRO UN @ POR LO QUE ES UN CLIENTE
						//CARGAMOS EL MODELO
						$this->load->model('M_clientes');
						//LLAMAMOS AL METODO DEL MODELO QUE NOS DEVUELVE EL CLIENTE O NULL
						$user = $this->M_clientes->get_cliente_mantenimiento_email($cliente);
						//SI LO ENCUENTRA ENTRA
						if ($user != null) {
								//PREGUNTAMOS SI LA CLAVE ES IGUAL Y CARGAMOS LA LIBERIA ENCRYPT
								$this->load->library('encrypt');
								if ($this->encrypt->decode($user->cl_clave) == $password || $this->encrypt->decode($user->man_clave) == $password) {
									//VALIDAMOS OK Y GUARDAMOS LA INFO EN LA SESSION
									$response['status'] = true;
									//CREAMOS LA SESSION
									$this->cl_session($cliente);
				                    //ELIMINAMOS LA URL DE LA SESSION
				                    $url = $this->session->userdata('url');
				                    //SI EXISTE URL LA PASAMOS SINO VAMOS A UNA POR DEFECTO
				                    if (!isset($url) || (trim($url) != '')) {
				                    	$this->session->unset_userdata('url');
				                    	$response['url'] = base_url().$this->lang->lang()."/".$url;
				                    } else {
										$response['url'] = base_url().$this->lang->lang().'/clientes/mantenimientos';
									}
								} else {
									$aux = array('cl_clave',lang('login.clave.error'));
									array_push($errors, $aux);
								}
						} else {
							$aux = array('cliente',lang('login.email.error'));
							array_push($errors, $aux);
						}
					}
				}
				
			    //GENERAMOS UN ARRAY CON LOS ERRORES
			    if ($response['status'] == false) {
			        foreach ($this->input->post() as $key => $value) {
			            if (form_error($key) != '') {
							$aux = array($key,form_error($key));
			            	array_push($errors,$aux);
						}
			        }
			        $response['errors'] = array_filter($errors);
				}
			    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			    echo json_encode($response);
			}
	}
	
	//PANEL MANTENIMIENTOS DEL CLIENTE
	public function mantenimientos() {
		$this->comprobar_cliente('clientes/mantenimientos');
		//REFRESCAMOS LA SESSION
		$this->cl_session($this->session->userdata('cliente'));
		
		$datos = array(
	        'TITULO' => lang('mantenimientos'),
	        'MANTENIMIENTOS' => 'active',
			'BODY' => $this->load->view('v_clientes_mantenimientos','',true)
	    );
		$this->load->view('v_admin_clientes',$datos);		
	}
	
	//PANEL MANTENIMIENTO DEL CLIENTE
	public function panel_mantenimiento() {
		$this->comprobar_cliente('clientes/panel_mantenimiento');
		//REFRESCAMOS LA SESSION
		$this->cl_session($this->session->userdata('cliente'));
		
		$datos = array(
	        'TITULO' => lang('panel.cliente'),
			'BODY' => $this->load->view('v_clientes_panel_mantenimiento','',true)
	    );
		$this->load->view('v_admin_clientes',$datos);		
	}
	
	//RECORDAMOS LA CLAVE DEL CLIENTE O DEL MANTENIMIENTO
	public function recordar_clave() {
		//REGUNTAMOS SI EXISTE ENVIO DE INFORMACION POR POST
		if ($this->input->post()) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['sql'] = true;
			$errors = array();
			$aux = array();
			//VALIDAMOS LOS CAMPOS
			$this->form_validation->set_rules('cliente',lang('email.mantenimiento'),'trim|required|max_length[50]|xss_clean');
			//COMPROBAMOS LA VALIDACION
			if ($this->form_validation->run()) {
		    	//SI NO ENCUENTRA @ ES UN MANTENIMIENTO
				if (strpos($this->input->post('cliente'), '@') === false) {
					//CARGAMOS EL MODELO
					$this->load->model('M_mantenimientos');
					//LLAMAMOS AL METODO DEL MODELO QUE NOS DEVUELVE EL EMPLEADO O NULL
					$user = $this->M_mantenimientos->get_mantenimiento_cliente($this->input->post('cliente'));
					//SI LO ENCUENTRA ENTRA
					if ($user != null) {
						$response['sql'] = $this->M_mantenimientos->set_recordar_clave($user);
						$response['status'] = $response['sql'];
					} else {
						$aux = array('cliente',lang('login.mantenimiento.error'));
						array_push($errors, $aux);
					}
				} else {
					//ENCONTRO UN @ POR LO QUE ES UN CLIENTE
					//CARGAMOS EL MODELO
					$this->load->model('M_clientes');
					//LLAMAMOS AL METODO DEL MODELO QUE NOS DEVUELVE EL EMPLEADO O NULL
					$user = $this->M_clientes->get_cliente_email($this->input->post('cliente'));
					//SI LO ENCUENTRA ENTRA
					if ($user != null) {
						$response['sql'] = $this->M_clientes->set_recordar_clave($user);
						$response['status'] = $response['sql'];
					} else {
						$aux = array('cliente',lang('login.email.error'));
						array_push($errors, $aux);
					}
				}
			}
		    //GENERAMOS UN ARRAY CON LOS ERRORES
		    foreach ($this->input->post() as $key => $value) {
		        if (form_error($key) != '') {
					$aux = array($key,form_error($key));
		           	array_push($errors,$aux);
				}
		    }
		    $response['errors'] = array_filter($errors);
		        
		    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
		    echo json_encode($response);
		}
	}
	
	public function salir() {
		$sessionData = array('loggedIn' => false);
		$this->session->set_userdata($sessionData);
    	// y eliminamos la sesión
    	$this->session->sess_destroy();
    	
    	// redirigimos al controlador principal
    	redirect(base_url(), 'refresh');
	}
	
	//BUSCA MANTENIMIENTOS SEGUN SU CLIENTE
	public function mantenimientos_panel_cliente() {
		$this->comprobar_cliente('clientes/mantenimientos');
		
		//REFRESCAMOS LA SESSION
		$this->cl_session($this->session->userdata('cliente'));
		
		if ($this->input->post()) {
			$this->load->model('M_mantenimientos');
			echo json_encode($this->M_mantenimientos->get_mantenimientos_cliente($this->session->userdata('cl_id')));
		}
	}
	
	//PAGINA PARA EDITAR MANTENIMIENTOS
	public function ver_mantenimiento($man_id=null) {
		if ($man_id != null) {
			$this->comprobar_cliente('clientes/ver_mantenimiento/'.$man_id);	
		} else {
			$this->comprobar_cliente('clientes/mantenimientos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->cl_session($this->session->userdata('cliente'));
		
		if ($man_id == null) {
			redirect(base_url().$this->lang->lang().'/clientes/mantenimientos','refresh');
		} else {
			$this->load->model('M_mantenimientos');
			$mantenimiento['mantenimiento'] = $this->M_mantenimientos->get_mantenimiento($man_id);
			//PREGUNTAMOS SI DEVOLVIO RESULTADOS
			if ($mantenimiento['mantenimiento'] != null) {
				$this->load->model('M_trabajos');
				$this->load->model('M_empleados');
				//OBTENEMOS LOS TRABAJOS DEL MANTENIMIENTO
				$mantenimiento['trabajos'] = $this->M_trabajos->get_trabajos_mantenimiento($mantenimiento['mantenimiento']->man_id);
				
				for($i=0; $i<count($mantenimiento['trabajos']); $i++) {
					//PREGUNTAMOS SI EL TRABAJO ESTA COMPLETADO O PROCESANDO
					if ($mantenimiento['trabajos'][$i]->tr_estado >= 1) {
						//OBTENEMOS LAS INSPECCIONES DEL TRABAJO
						$in_ids = explode(',',$mantenimiento['trabajos'][$i]->in_id);
						$in_ids = array_filter($in_ids);
						
						//RECORREMOS LAS INSPECCIONES
						$inspeccion = array();
						for($x=0; $x<count($in_ids); $x++) {
							//OBTENEMOS LA INSPECCION DEL TRABAJO
							array_push($inspeccion,$this->M_trabajos->get_mantenimiento_inspecciones($in_ids[$x],$mantenimiento['trabajos'][$i]->tr_id,$man_id));
						}
						
						$mantenimiento['trabajos'][$i]->inspecciones = $inspeccion;
					}
					
					if ($mantenimiento['trabajos'][$i]->tr_estado <= 1) {
						//TRABAJO PENDIENTE O EN PROCESO
						//OBTENEMOS LOS EMPLEADOS
						$emp_ids = explode(',',$mantenimiento['trabajos'][$i]->emp_id);
						$emp_ids = array_filter($emp_ids);
						
						//RECORREMOS LOS EMPLEADOS
						$empleado = array();
						$empeado_aux = array();
						for ($t=0; $t<count($emp_ids); $t++) {
							//array_push($empleado,$this->M_empleados->get_empleado_id($emp_ids[$t]));
							$empeado_aux[0] = $this->M_empleados->get_empleado_id($emp_ids[$t]);
							//PREGUNTAMOS SI DEBEMOS MOSTRAR EL BOTON DE NOTIFICAR ASISTENCIA
							$empeado_aux[1] = $this->M_empleados->get_trabajo_asistencia($emp_ids[$t],$mantenimiento['trabajos'][$i]->tr_fecha_inicio,$mantenimiento['trabajos'][$i]->tr_fecha_fin);
							
							array_push($empleado,$empeado_aux);
						}
						
						$mantenimiento['trabajos'][$i]->empleados = $empleado;
					}
				}
				
				$datos = array(
			        'TITULO' => lang('mantenimiento').': '.$mantenimiento['mantenimiento']->man_id,
			        'MANTENIMIENTOS' => 'active',
					'BODY' => $this->load->view('v_clientes_mantenimiento_ver',$mantenimiento,true)
			    );
				$this->load->view('v_admin_clientes',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/clientes/mantenimientos','refresh');
			}
		}
	}
	
	//BUSCA LOS TRABAJOS DEL CLIENTE
	public function trabajos_mantenimiento() {
		$this->comprobar_cliente('clientes/ver_mantenimiento/'.$this->input->post('man_id'));
		
		//REFRESCAMOS LA SESSION
		$this->cl_session($this->session->userdata('cliente'));
		
		if ($this->input->post()) {
			$this->load->model('M_trabajos');
			echo json_encode($this->M_trabajos->get_trabajos_mantenimiento($this->input->post('man_id')));
		}
	}
	
	//BUSCA LAS INSPECCIONES DEL TRABAJO
	public function mantenimiento_inspecciones() {
		$this->comprobar_cliente('clientes/ver_mantenimiento/'.$this->input->post('man_id'));
		
		//REFRESCAMOS LA SESSION
		$this->cl_session($this->session->userdata('cliente'));
		
		if ($this->input->post()) {
			$this->load->model('M_trabajos');
			echo json_encode($this->M_trabajos->get_mantenimiento_inspecciones($this->input->post('in_id'),$this->input->post('tr_id')));
		}
	}
}

/* End of file clientes.php */
/* Location: ./application/controllers/clientes.php */