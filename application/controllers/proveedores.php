<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proveedores extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE PROVEEDORES
	public function index() {
		$this->comprobar_usuario('proveedores');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('11')) {
			$acceso = $this->load->view('v_proveedores','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('proveedores'),
	        'MATERIALES' => 'active',
			'PROVEEDORES' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('proveedores'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA PROVEEDORES PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('proveedores');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('11') || $this->acceso('15') || $this->acceso('14') || $this->acceso('17')) {
			if ($this->input->post()) {
				$this->load->model('M_proveedores');
				$array = $this->M_proveedores->get_proveedores();
				$aux = array();
				
				echo json_encode($array);
			}
		} else {
			if ($this->acceso('11')) {
				redirect(base_url().$this->lang->lang().'/proveedores','refresh');
			} else if ($this->acceso('15')) {
				redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
			} else if ($this->acceso('14')) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else if ($this->acceso('17')) {
				redirect(base_url().$this->lang->lang().'/salidas','refresh');
			}
		}
	}
	
	//PAGINA PARA A�ADIR PROVEEDORES
	public function nuevo_proveedor() {
		$this->comprobar_usuario('proveedores/nuevo_proveedor');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('11')) {
			$proveedor = array('pro_id' => null);
			$datos = array(
		        'TITULO' => lang('nuevo.proveedor'),
		        'MATERIALES' => 'active',
		        'PROVEEDORES' => 'active',
				'BODY' => $this->load->view('v_proveedores_nuevo_editar',$proveedor,true),
				'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('proveedores'), 'href'=>base_url().$this->lang->lang().'/proveedores'],['nombre'=>lang('nuevo.proveedor'), 'href'=>''])
		    );
			$this->load->view('v_admin',$datos);
		} else {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
	}
	
	//PAGINA PARA EDITAR PROVEEDORES
	public function editar_proveedor($pro_id=null) {
		if ($pro_id != null) {
			$this->comprobar_usuario('proveedores/editar_proveedor/'.$pro_id);	
		} else {
			$this->comprobar_usuario('proveedores');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('11')) {
			if ($pro_id == null) {
				redirect(base_url().$this->lang->lang().'/proveedores','refresh');
			} else {
				$this->load->model('M_proveedores');
				$proveedor = $this->M_proveedores->get_proveedor($pro_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($proveedor != null) {
					$datos = array(
				        'TITULO' => lang('editar.proveedor'),
				        'MATERIALES' => 'active',
				        'PROVEEDORES' => 'active',
						'BODY' => $this->load->view('v_proveedores_nuevo_editar',$proveedor,true),
						'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('proveedores'), 'href'=>base_url().$this->lang->lang().'/proveedores'],['nombre'=>lang('editar.proveedor'), 'href'=>''])
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/proveedores','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/proveedores','refresh');
		}
	}
	
	//PREGUNTAMOS SI EXISTE UN PROVEEDOR CON ESTE CIF
	public function proveedor_cif($cif) {
		$this->load->model('M_proveedores');
		$cliente = $this->M_proveedores->get_proveedor_cif($cif);
		if ($cliente->num_rows() > 0) {
			$this->form_validation->set_message('pro_cif', lang('proveedor.existente'));
			return false;
		} else {
			return true;
		}
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO DEL PROVEEDOR
	public function guardar_editar() {
		if ($this->input->post('pro_id') != '') {
			$this->comprobar_usuario('proveedores/editar_proveedor/'.$this->input->post('pro_id'));	
		} else {
			$this->comprobar_usuario('proveedores');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('11')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('pro_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('pro_nombre',lang('nombre'),'trim|required|max_length[80]|xss_clean');
					$this->form_validation->set_rules('pro_telefono',lang('telefono'),'trim|required|max_length[15]|xss_clean');
					$this->form_validation->set_rules('pro_email',lang('email'),'trim|required|valid_email|max_length[50]|xss_clean');
					$this->form_validation->set_rules('pro_direccion',lang('direccion'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('pro_provincia',lang('provincia'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('pro_postal',lang('postal'),'trim|max_length[11]|xss_clean');
					$this->form_validation->set_rules('pro_barriada',lang('barriada'),'trim|max_length[50]|xss_clean');
					$this->form_validation->set_rules('pro_localidad',lang('localidad'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('pro_pais',lang('pais'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('pro_emails',lang('emails'),'trim|max_length[2000]|xss_clean');
					$this->form_validation->set_rules('pro_id','','trim|xss_clean');
					$this->form_validation->set_rules('foto','error','trim|xss_clean');
					
					if ($this->input->post('pro_id') == '') {
						$this->form_validation->set_rules('pro_cif',lang('cif'),'trim|required|max_length[30]|callback_proveedor_cif|xss_clean');
					} else {
						$this->form_validation->set_rules('pro_cif',lang('cif'),'trim|required|max_length[30]|xss_clean');
					}
					
					if ($this->form_validation->run()) {					
						if (!empty($_FILES['foto'])) {
							$name2 = $_FILES['foto']['name']; //get the name of the image
							list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
							
							if ($ext == 'jpg') {
								if ($_FILES['foto']['size'] <= 500000) {
									$response['status'] = true;
								} else {
									$f = array('foto',lang('max.size'));
									array_push($errors,$f);
								}
							} else {
								$f = array('foto',lang('jpg'));
								array_push($errors,$f);
							}	
						} else {
							$response['status'] = true;
						}
						
						if ($response['status']) {
							$this->load->model('M_proveedores');
							$response['sql'] = $this->M_proveedores->set_proveedor($this->input->post());
							$response['status'] = $response['sql'];

							if($response['sql'] != false){
								$id = $response['sql'];
								$response['sql'] = true;
								$response['status'] = $response['sql'];
							}
						}
					}
					
					foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors);

				    //GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'PV'; //Proveedores
						$datos['asociado'] = $id;
						$datos['accion'] = ($this->input->post('pro_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}

				    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				    echo json_encode($response);
				}
			} else {
				redirect(base_url().$this->lang->lang().'/proveedores','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/proveedores','refresh');
		}
	}
	
	/* PRODUCTOS DEL PROVEEDOR */
	
	//MUESTRA LA LISTA DE PRODUCTOS
	public function productos() {
		$this->comprobar_usuario('proveedores/productos');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			$acceso = $this->load->view('v_proveedores_productos','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		


		$datos = array(
	        'TITULO' => lang('tienda.online'),
	        'MATERIALES' => 'active',
			'TIENDA' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('tienda.online'), 'href'=>''])
	    );
	    
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA PRODUCTOS PARA MOSTRAR
	public function buscador_productos() {
		$this->comprobar_usuario('proveedores/productos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			if ($this->input->post()) {
				$this->load->model('M_proveedores');
				$this->load->library('Cart');
				//LISTA DE PRODUCTOS DE LOS PROVEEDORES (Solo de la tienda online)
				$productos = $this->M_proveedores->get_productos_tienda();
				
				for ($i=0; $i<count($productos); $i++) {
					//PREGUNTAMOS SI ESTE PRODUCTO ESTA EN EL CARRITO
					$cantidad = $this->cart->in_cart($productos[$i]->product_id);
					//SI LO ENNCUENTRA GUARDAMOS SU CANTIDAD
					if ($cantidad != null) {
						$productos[$i]->cantidad = $cantidad;
					}
					
					//A�ADIMOS LAS FOTOS
					//DIRECCION DE LAS FOTOS
					$ruta = PRIVADO.$this->session->userdata("em_cif").'/pro_product/'.$productos[$i]->product_id;
					
					$aux = '';
					//RECORRE LA CARPETA
					if (file_exists($ruta)) {
						//CREA UN OBJETO DE CARPETA
						$dirint = dir($ruta);
						
						$primera_vez = true;
						
						while (($archivo = $dirint->read()) !== false) {
							//SI NO ES JPG NO PASA
							if (preg_match("/jpg/", $archivo)){
								//CREA LA IMAGEN						
								$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/pro_product/'.$productos[$i]->product_id.'/'.$archivo;
								
								$nombre = substr($archivo, 0, count($archivo)-5);
								
								//LISTADO DE IMAGENES
								if ($primera_vez) {
									$aux = $aux.'<a rel="product'.$productos[$i]->product_id.'" class="fancybox" href="'.$img.'"><i class="fa fa-eye"></i></a>';
									
									$primera_vez = false;
								} else {
									$aux = $aux.'<a rel="product'.$productos[$i]->product_id.'" class="fancybox oculto" href="'.$img.'"><img src="'.$img.'" /></a>';
								}
							}
						}
						
						//CERRAMOS CARPETA
						$dirint->close();
					}
					
					$productos[$i]->fotos = $aux;					
				}
				
				echo json_encode($productos);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
		}
	}
	
	//BUSCA PRODUCTOS PARA MOSTRAR
	public function buscador_productos_proveedor() {
		$this->comprobar_usuario('almacenes/editar_almacen/'.$this->session->userdata('al_id'));
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('14')) {
			$this->load->model('M_proveedores');
			$array = $this->M_proveedores->get_productos_proveedor($this->input->post('pro_id'));
				
			echo json_encode($array);
		} else {
			redirect(base_url().$this->lang->lang().'/almacenes','refresh');
		}
	}
	
	//PAGINA PARA A�ADIR PRODUCTOS
	public function nuevo_producto() {
		$this->comprobar_usuario('proveedores/nuevo_producto');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			$producto = array('product_id' => null);
			$datos = array(
		        'TITULO' => lang('nuevo.producto'),
		        'MATERIALES' => 'active',
		        'TIENDA' => 'active',
				'BODY' => $this->load->view('v_proveedores_productos_nuevo_editar',$producto,true),
				'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('tienda.online'), 'href'=>base_url().$this->lang->lang().'/proveedores/productos'],['nombre'=>lang('nuevo.producto'), 'href'=>''])
		    );
			$this->load->view('v_admin',$datos);
		} else {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
	}
	
	//PAGINA PARA EDITAR PRODUCTOS
	public function editar_producto($product_id=null) {
		if ($product_id != null) {
			$this->comprobar_usuario('proveedores/editar_producto/'.$product_id);	
		} else {
			$this->comprobar_usuario('proveedores/productos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			if ($product_id == null) {
				redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
			} else {
				$this->load->model('M_proveedores');
				$producto = $this->M_proveedores->get_producto($product_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($producto != null) {
					$datos = array(
				        'TITULO' => lang('editar.producto'),
				        'MATERIALES' => 'active',
				        'TIENDA' => 'active',
						'BODY' => $this->load->view('v_proveedores_productos_nuevo_editar',$producto,true),
						'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('tienda.online'), 'href'=>base_url().$this->lang->lang().'/proveedores/productos'],['nombre'=>lang('editar.producto'), 'href'=>''])
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO DEL PRODUCTO
	public function productos_guardar_editar() {
		//print_r($this->input->post());	
		if ($this->input->post('product_id') != '') {
			$this->comprobar_usuario('proveedores/editar_producto/'.$this->input->post('product_id'));	
		} else {
			$this->comprobar_usuario('proveedores/productos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			$product_id = '';
			
			if ($this->input->post('product_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('product_nombre',lang('descripcion'),'trim|required|max_length[80]|xss_clean');
					$this->form_validation->set_rules('product_referencia',lang('referencia'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('product_precio',lang('precio'),'trim|required|max_length[15]|xss_clean');
					$this->form_validation->set_rules('pro_id',lang('proveedor'),'trim|required|xss_clean');
					$this->form_validation->set_rules('product_id','','trim|xss_clean');
					
					
					if ($this->form_validation->run()) {	
						$this->load->model('M_proveedores');
						$response['sql'] = $this->M_proveedores->set_producto($this->input->post());
						if ($response['sql'] != false) {
							$product_id = $response['sql'];
							$response['sql'] = true;
						}
							
						$response['status'] = $response['sql'];
						
						//GUARDAMOS LAS IMAGENES Y ARCHIVOS
						if ($response['status']) {
							//COMPRUEBA LAS IMAGENES
							if (!empty($_FILES['fotos']) && count($errors) == 0) {
								$num_archivos = count($_FILES['fotos']['tmp_name']);		
								if ($num_archivos > 0) {
									for ($i=0;$i<$num_archivos;$i++) {
										$name2 = $_FILES['fotos']['name'][$i];
										list($txt, $ext) = explode(".", $name2);
										
										if ($ext == 'jpg') {
											if (($_FILES['fotos']['size'][$i]/KB) > 500) {
												$f = array('fotos',$name2.' - '.lang('max.size'));
												array_push($errors,$f);
											}
										} else {
											$f = array('fotos',$name2.': '.lang('jpg'));
											array_push($errors,$f);
										}
									}
								}		
							}
							
							//COMPRUEBA LOS ARCHIVOS
							if (!empty($_FILES['archivos']) && count($errors) == 0) {
								$num_archivos = count($_FILES['archivos']['tmp_name']);		
								if ($num_archivos > 0) {
									
									$extensiones = array('text','doc','docx','xls','xlsx','pdf','ppt','pptx');
									
									for ($i=0;$i<$num_archivos;$i++) {
										$name2 = $_FILES['archivos']['name'][$i];
										list($txt, $ext) = explode(".", $name2);
										
										if (in_array($ext,$extensiones)) {
											if (($_FILES['archivos']['size'][$i]/KB) > 8000) {
												$f = array('archivos',$name2.' - '.lang('file.max.size'));
												array_push($errors,$f);
											}
										} else {
											$f = array('archivos',$name2.': '.lang('file.error').' (text, doc, docx, xls, xlsx, pdf, ppt, pptx)');
											array_push($errors,$f);
										}
									}
								}	
							}
							
							if (count($errors) == 0) {
								$response['status'] = true;
							} else {
								$response['status'] = false;
							}
							
							if ($response['status'] && count($errors) == 0) {
								//GUARDA LAS IMAGENES
								if (!empty($_FILES['fotos'])) {
									//configuraci�n para upload
									$ruta = PRIVADO.$this->session->userdata('em_cif').'/pro_product/'.$product_id;
									$img['upload_path'] = $ruta.'/';
									$img['allowed_types'] = 'jpg';
									$img['overwrite'] = false;
									$this->load->library('upload', $img);
									$this->upload->initialize($img);
											
									$num_archivos = count($_FILES['fotos']['tmp_name']);
											
									if ($num_archivos > 0) {
										for ($i=0;$i<$num_archivos;$i++) {
											//REEMPLAZAMOS ESPACIOS
											$nombre = str_replace(' ','_',$_FILES['fotos']['name'][$i]);
									       	$_FILES['userfile']['name'] = $nombre;
									       	$_FILES['userfile']['type'] = $_FILES['fotos']['type'][$i];
									       	$_FILES['userfile']['tmp_name'] = $_FILES['fotos']['tmp_name'][$i];
									       	$_FILES['userfile']['error'] = $_FILES['fotos']['error'][$i];
									       	$_FILES['userfile']['size'] = $_FILES['fotos']['size'][$i];           
									       	
									       	if (!$this->upload->do_upload()) {
									       		$aux = array('fotos',$this->upload->display_errors());
												array_push($errors, $aux);
									      		$response['status'] = false;
									      		// esto es muy �til para encontrar qu� falla
									       	}
									   	}
									}
								}
								
								//GUARDA LOS ARCHIVOS
								if (!empty($_FILES['archivos'])) {
									//configuraci�n para upload
									$ruta = PRIVADO.$this->session->userdata('em_cif').'/pro_product/'.$product_id;
									$file['upload_path'] = $ruta.'/';
									$file['allowed_types'] = 'txt|pdf|doc|docx|xls|xlsx|ppt|pptx';
									$file['overwrite'] = false;
									$this->load->library('upload', $file);
									$this->upload->initialize($file);
											
									$num_archivos = count($_FILES['archivos']['tmp_name']);
											
									if ($num_archivos > 0) {
										for ($i=0;$i<$num_archivos;$i++) {
											//REEMPLAZAMOS ESPACIOS
											$nombre = str_replace(' ','_',$_FILES['archivos']['name'][$i]);
									       	$_FILES['userfile']['name'] = $nombre;
									       	$_FILES['userfile']['type'] = $_FILES['archivos']['type'][$i];
									       	$_FILES['userfile']['tmp_name'] = $_FILES['archivos']['tmp_name'][$i];
									       	$_FILES['userfile']['error'] = $_FILES['archivos']['error'][$i];
									       	$_FILES['userfile']['size'] = $_FILES['archivos']['size'][$i];
									       	
									       	if (!$this->upload->do_upload()) {
									       		$aux = array('archivos',$this->upload->display_errors());
												array_push($errors, $aux);
									      		$response['status'] = false;
									      		// esto es muy �til para encontrar qu� falla
									       	}
									   	}
									}
								}
							}
						}
					}
					
					foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors);

				    //GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'TO'; //Tienda Online
						$datos['asociado'] = $product_id;
						$datos['accion'] = ($this->input->post('product_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}
				    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				    echo json_encode($response);
				}
			} else {
				redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
		}
	}
	
	//BUSCA CATEGORIAS
	public function categorias() {
		$this->comprobar_usuario('proveedores/productos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('11') || $this->acceso('15') || $this->acceso('14') || $this->acceso('17')) {
			if ($this->input->post()) {
				$this->load->model('M_proveedores');
				$array = $this->M_proveedores->get_categorias();
				$aux = array();
				
				echo json_encode($array);
			}
		} else {
			if ($this->acceso('11')) {
				redirect(base_url().$this->lang->lang().'/proveedores','refresh');
			} else if ($this->acceso('15')) {
				redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
			} else if ($this->acceso('14')) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else if ($this->acceso('17')) {
				redirect(base_url().$this->lang->lang().'/salidas','refresh');
			}
		}
	}
	
	//IMAGENES DEL PRODUCTO
	function producto_imagenes() {
		if ($this->input->post('product_id') != '') {
			$url = 'proveedores/editar_producto'.$this->input->post('product_id');
		} else {
			$url = 'proveedores/productos';
		}
		
		
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			//DIRECCION DE LAS FOTOS
			$ruta = PRIVADO.$this->session->userdata("em_cif").'/pro_product/'.$this->input->post('product_id');
			
			$aux = '';
			//RECORRE LA CARPETA
			if (file_exists($ruta)) {
				//CREA UN OBJETO DE CARPETA
				$dirint = dir($ruta);
				
				while (($archivo = $dirint->read()) !== false) {
					//SI NO ES JPG NO PASA
					if (preg_match("/jpg/", $archivo)){
						//CREA LA IMAGEN						
						$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/pro_product/'.$this->input->post('product_id').'/'.$archivo;
						
						$nombre = substr($archivo, 0, count($archivo)-5);
						
						//LISTADO DE IMAGENES
						$aux = $aux.'<a rel="product'.$this->input->post('product_id').'" class="fancybox" href="'.$img.'" title="'.$nombre.'"><img src="'.$img.'" /></a>';
					}
				}
				
				//CERRAMOS CARPETA
				$dirint->close();
			}
			
			
			//ENVIAMOS LISTA DE IMAGENES
			echo json_encode($aux);
		}
	}
	
	//ARCHIVOS DEL PRODUCTO
	function producto_archivos() {
		if ($this->input->post('product_id') != '') {
			$url = 'proveedores/editar_producto/'.$this->input->post('product_id');	
		} else {
			$url = 'proveedores/productos';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			//DIRECCION DE LAS FOTOS
			$ruta = PRIVADO.$this->session->userdata("em_cif").'/pro_product/'.$this->input->post('product_id');
			
			$aux = '';
			//RECORRE LA CARPETA
			if (file_exists($ruta)) {
				//CREA UN OBJETO DE CARPETA
				$dirint = dir($ruta);
				
				$extensiones = array('text','doc','docx','xls','xlsx','pdf','ppt','pptx');
				
				while (($archivo = $dirint->read()) !== false) {
					list($txt, $ext) = explode(".", $archivo);
										
					if (in_array($ext,$extensiones)) {
						//LISTADO DE ARCHIVOS
						$aux = $aux.'<a href="'.base_url().$this->lang->lang().'/proveedores/producto_archivos_descargar/'.$this->input->post('product_id').'/'.$archivo.'"><i class=" fa fa-download"></i>'.$archivo.'</a><br>';
					}
				}
				
				//CERRAMOS CARPETA
				$dirint->close();
			}
			
			
			//ENVIAMOS LISTA DE IMAGENES
			echo json_encode($aux);
		}
	}
	
	//DESCARGAR ARCHIVOS DEL PRODUCTO
	function producto_archivos_descargar($product_id, $archivo) {
		$this->comprobar_usuario('proveedores/editar_producto/'.$product_id);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
	        $this->load->helper('download');
	        $data = file_get_contents(PRIVADO.$this->session->userdata("em_cif").'/pro_product/'.$product_id.'/'.$archivo);
	        $name = $archivo;

	        force_download($name, $data);
		}
	}
	
	/* PEDIR PRODUCTO - A�ADIR AL CARRITO */
	public function pedir_producto() {
		$this->comprobar_usuario('proveedores/productos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			$this->load->library('Cart');
			
			$caracteristicas = $this->input->post('caracteristicas');
			$caracteristicas = explode(';',$caracteristicas);
			$caracteristicas = array_filter($caracteristicas);
			$opciones = array();
			for ($i=0; $i<count($caracteristicas); $i++) {
				$aux = explode('/',$caracteristicas[$i]);
				$aux = array_filter($aux);
				
				/*$opt = array(
					$aux[0] => $aux[1]
				);
				
				array_push($opciones,$opt);*/
				$opciones[$aux[0]] = $aux[1];
			}
			
			$data = array(
               'id'      => $this->input->post('product_id'),
               'qty'     => $this->input->post('cantidad'),
               'price'   => $this->input->post('precio'),
               'name'    => $this->input->post('nombre'),
               'options' => $opciones
            );
            
			$this->cart->insert($data);

			//GUARDAMOS LA BITACORA
			
			$this->load->model('M_bitacora');
			$datos = array();
			$datos['tipo'] = 'TO'; //Tienda Online
			$datos['asociado'] = $this->input->post('product_id');
			$datos['accion'] = 'Pedir';
			$this->M_bitacora->set_bitacora($datos);
			
			echo json_encode($this->cart->contents());
		} else {
			redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
		}
	}
	
	//BUSCA CARRITOS PARA MOSTRAR
	public function buscador_carritos() {
		$this->comprobar_usuario('proveedores');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('11') || $this->acceso('15') || $this->acceso('14') || $this->acceso('17')) {
			if ($this->input->post()) {
				$this->load->model('M_proveedores');
				$array = $this->M_proveedores->get_carritos();

				echo json_encode($array);
			}
		} else {
			if ($this->acceso('11')) {
				redirect(base_url().$this->lang->lang().'/proveedores','refresh');
			} else if ($this->acceso('15')) {
				redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
			} else if ($this->acceso('14')) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else if ($this->acceso('17')) {
				redirect(base_url().$this->lang->lang().'/salidas','refresh');
			}
		}
	}
	
	//MUESTRA LA LISTA DE CARRITOS
	public function carritos() {
		$this->comprobar_usuario('proveedores/carritos');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			$acceso = $this->load->view('v_proveedores_carritos','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('carritos'),
	        'MATERIALES' => 'active',
			'CARRITOS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('carritos'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	/* VER CARRITO */
	public function carrito() {
		$this->comprobar_usuario('proveedores/carrito');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			$this->load->library('Cart');
			$acceso = $this->load->view('v_carrito','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('carrito'),
	        'MATERIALES' => 'active',
			'CARRITO' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('carrito'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
		} else {
			redirect(base_url().$this->lang->lang().'proveedores/carrito','refresh');
		}
	}
	
	/* ACTUALIZAR CARRITO */
	public function actualizar_carrito() {
		$this->comprobar_usuario('proveedores/carrito');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
	
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			$this->load->library('Cart');
			$carrito = $this->input->post('datos');
			$this->cart->update($carrito);

			$this->load->model('M_bitacora');
			$datos = array();
			$datos['tipo'] = 'CC'; //Carrito Compra
			$datos['asociado'] = 'temporal';
			$datos['accion'] = 'Editar';
			$this->M_bitacora->set_bitacora($datos);

			echo json_encode(true);
		} else {
			redirect(base_url().$this->lang->lang().'proveedores/carrito','refresh');
		}
	}
	
	/* ELIMINAR CARRITO */
	public function eliminar_carrito() {
		$this->comprobar_usuario('proveedores/carrito');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			
			if ($this->input->post()) {
				$this->load->model('M_proveedores');
				$response['sql'] = $this->M_proveedores->set_eliminar_carrito($this->input->post());
				$response['status'] = $response['sql'];
	
				foreach ($this->input->post() as $key => $value) {
					if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				      	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);

				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'CG'; //Carrito Compra
					$datos['asociado'] = $this->input->post('car_id');
					$datos['accion'] = 'Eliminar';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
				
			}
			
		} else {
			redirect(base_url().$this->lang->lang().'proveedores/carrito','refresh');
		}
	}
	
	//PAGINA PARA EDITAR PRODUCTOS
	public function editar_carrito($car_id=null) {
		if ($car_id != null) {
			$this->comprobar_usuario('proveedores/carrito/'.$car_id);	
		} else {
			$this->comprobar_usuario('proveedores/carritos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			if ($car_id == null) {
				redirect(base_url().$this->lang->lang().'/proveedores/carritos','refresh');
			} else {
				$this->load->model('M_proveedores');
				$carrito = $this->M_proveedores->get_carrito($car_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($carrito != null) {
					$datos = array(
				        'TITULO' => lang('editar.carrito'),
				        'MATERIALES' => 'active',
				        'CARRITOS' => 'active',
						'BODY' => $this->load->view('v_proveedores_carrito_editar',$carrito,true),
						'BREADCRUMB' => array(['nombre'=>lang('materiales'), 'href'=>'#'],['nombre'=>lang('carritos'), 'href'=>base_url().$this->lang->lang().'/proveedores/carritos'], ['nombre'=>lang('editar.carrito'), 'href'=>''])
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/proveedores/carritos','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/proveedores/carritos','refresh');
		}
	}
	
	/* ACTUALIZAR CARRITO */
	public function actualizar_carritos() {
		$this->comprobar_usuario('proveedores/carritos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			$carrito = $this->input->post();
			
			if ($this->input->post()) {
				$this->load->model('M_proveedores');
				$response['sql'] = $this->M_proveedores->set_actualizar_carrito($carrito);
				$response['status'] = $response['sql'];
	
				foreach ($this->input->post() as $key => $value) {
					if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				      	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);

				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'CG'; //Carritos Guardados
					$datos['asociado'] = $carrito['car_id'];
					$datos['accion'] = 'Editar';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
				
			}
			
		} else {
			redirect(base_url().$this->lang->lang().'proveedores/carritos','refresh');
		}
	}
	
	/* GUARDAR CARRITO */
	public function guardar_carrito() {
		$this->comprobar_usuario('proveedores/carrito');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post()) {
				$this->load->model('M_proveedores');
				$result = $this->M_proveedores->set_guardar_carrito($this->input->post());

				if(empty($result)){
					$response['sql'] = false;
				}else{
					$response['sql'] = true;
				}

				$response['status'] = $response['sql'];
				$response['car_id'] = $result;
				
				if ($response['status']) {					
					//DESTRUIMOS EL CARRITO
					$this->load->library('Cart');
					$this->cart->destroy();
				}
				
				foreach ($this->input->post() as $key => $value) {
					if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				      	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);

				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = 'CC'; //Clientes
					$datos['asociado'] = $response['car_id'];
					$datos['accion'] = 'Guardar';
					$this->M_bitacora->set_bitacora($datos);
				}

				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'proveedores/carrito','refresh');
		}
	}
	
	/* COMPRAR CARRITO, TERMINAR PEDIDO */
	public function procesar_pedido() {
		$this->comprobar_usuario('proveedores/carrito');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			if ($this->input->post()) {
				$this->load->model('M_proveedores');
				$result = $this->M_proveedores->set_procesar_pedido($this->input->post());

				if(!empty($result['status']) && $result['status']){
					$response['sql'] = TRUE;					
				}else{
					$response['sql'] = FALSE;					
				}
				$response['status'] = $response['sql'];
				
				if ($response['status']) {	
					$response['objCorreos'] = array(
						'datos' => $result['datos']					
					);
					//DESTRUIMOS EL CARRITO
					$this->load->library('Cart');
					$this->cart->destroy();
				}
				
				foreach ($this->input->post() as $key => $value) {
					if (form_error($key) != '') {
						$aux = array($key,form_error($key));
				      	array_push($errors,$aux);
					}
				}
				$response['errors'] = array_filter($errors);
				
				//GUARDAMOS LA BITACORA
				if ($response['status']) {
					$this->load->model('M_bitacora');
					$datos = array();
					$datos['tipo'] = ($this->input->post('car_id') != '')?'CG':'CC'; //Carrito Compra
					$datos['asociado'] = $result['datos'][0]['productos']['pe_id'];
					$datos['accion'] = 'Procesar';
					$this->M_bitacora->set_bitacora($datos);
				}
				//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				echo json_encode($response);
			}
		} else {
			redirect(base_url().$this->lang->lang().'proveedores/carrito','refresh');
		}
	}
	
	/* Importar Productos Excel */
	public function importar_productos() {
		$this->comprobar_usuario('proveedores/productos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('15')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$errors = '';
			
			if (!empty($_FILES['documento'])) {
			$ruta = PRIVADO.$this->session->userdata('em_cif').'/pro_product/';
                        array_map('unlink', glob($ruta."*.xls"));
                        array_map('unlink', glob($ruta."*.xls"));	
                            
                $name2 = $_FILES['documento']['name']; //get the name of the image
				list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
							
				if ($ext == 'xls' || $ext == 'xlsx') {
					//configuraci�n para upload
					
					$file['upload_path'] = $ruta;
					$file['allowed_types'] = '*';
					$this->load->library('upload', $file);
					$this->upload->initialize($file);

					//REEMPLAZAMOS ESPACIOS
					$nombre = 'importExcel.'.$ext;
					$_FILES['userfile']['name'] = $nombre;
					$_FILES['userfile']['type'] = $_FILES['documento']['type'];
					$_FILES['userfile']['tmp_name'] = $_FILES['documento']['tmp_name'];
					$_FILES['userfile']['error'] = $_FILES['documento']['error'];
					$_FILES['userfile']['size'] = $_FILES['documento']['size'];
									       	
					if (!$this->upload->do_upload()) {
						$errors = $this->upload->display_errors();
						// esto es muy �til para encontrar qu� falla
					} else {
						//RECUPERAMOS EL EXCEL Y LO CARGAMOS A LA BBDD
						$this->load->library('Excel');
						$objPHPExcel = PHPExcel_IOFactory::load($ruta.$nombre);
						//se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
      					$total_sheets=$objPHPExcel->getSheetCount();
      					$allSheetName=$objPHPExcel->getSheetNames();
      					$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
      					//Se obtiene el n�mero m�ximo de filas
      					$highestRow = $objWorksheet->getHighestRow();
      					//Se obtiene el n�mero m�ximo de columnas
      					$highestColumn = $objWorksheet->getHighestColumn();
      					$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
      					//$headingsArray contiene los titulos de columnas
				      	$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
				      	$headingsArray = $headingsArray[1];
				      	//Se recorre toda la hoja excel desde la fila 2 y se almacenan los datos
				       	$namedDataArray = array();
						$namedDataArrayID = array();
				       	for ($row = 2; $row <= $highestRow; ++$row) {
				        	$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
				        	
				        	if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
				            	$caracteristicas = '';
				            	$producto = array();
				            	
                                    foreach($headingsArray as $columnKey => $columnHeading) {
				                	$celdaHead = trim($columnHeading);
				                	$celdaHead = str_replace('/','-',$celdaHead);
				                	$celdaHead = str_replace(';',',',$celdaHead);
				                	
				                	$celda = trim($dataRow[$row][$columnKey]);
				                	
				                	if ($celdaHead == 'pro_id' || $celdaHead == 'product_referencia' 
				                		|| $celdaHead == 'product_nombre' || $celdaHead == 'product_precio'
				                		|| $celdaHead == 'cate_id') {
				                		
                                        $producto[$celdaHead] = $celda;
									} else if ($celdaHead == 'product_cantidad') {
										if($_POST['al_id']) {
											$producto[$celdaHead] = $celda;
										}
									} else {
                                                           
										if ($celda != '' || $celda != NULL) {
											$caracteristicas = $caracteristicas.$celdaHead.'/'.$celda.';';
										}
							}	
				                }
				                
				                if ($caracteristicas != '') {
									$producto['product_caracteristicas'] = $caracteristicas;
								}
								
								$producto['em_id'] = $this->session->userdata('em_id');
								$producto['product_tienda'] = 1;
								array_push($namedDataArray, $producto);
				            }
				        }
						
						$this->load->model('M_proveedores');
				       
						if($_POST['al_id'] && ($_POST['al_id'] != '')) {
							$response['sql'] = $this->M_proveedores->set_importar_productos_almacen($namedDataArray,$_POST['al_id']);
						} else {
							$response['sql'] = $this->M_proveedores->set_importar_productos($namedDataArray);
						}
				        
				        //BORRAR EXCEL
				        if ($response['sql']) {
                        	unlink($ruta.$nombre);
						}
					}			
				} else {
					$errors = lang('excel.error');
				}	
			} else {
					$errors = lang('excel.requerido');
				}
			
			$response['errors'] = $errors;

			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/proveedores/productos','refresh');
		}
	}
	
	public function upload_adjuntos(){
		$archivos = '';
		$car_id = null;
		$response = array();
		$allInputs = $this->input->post(); 

		foreach($_FILES as $index => $file){
			$car_id = explode('-', $index)[1];
			// for easy access
			$fileName     = $file['name'];
			// for easy access
			$fileTempName = $file['tmp_name'];
			// check if there is an error for particular entry in array
			if(!empty($file['error'][$index])){
				$response['errors'] = true;
				$response['status'] = false;
				echo json_encode($response);
			}
	 
			if(!empty($fileTempName) && is_uploaded_file($fileTempName)){
				// move the file from the temporary directory to somewhere of your choosing
				move_uploaded_file($fileTempName, "img/dynamic/adjuntos-carrito/" . $fileName);
				$archivos .= $fileName . '|';
			}
		}

		$adjIniciales = array();
		if(!empty($allInputs['adjuntos_iniciales'])){
			$adjIniciales = explode('|', $allInputs['adjuntos_iniciales']);			
		}
		$adjEliminados = array();
		if(!empty($allInputs['eliminados'])){
			$adjEliminados = explode(',', $allInputs['eliminados']);
		}

		foreach ($adjIniciales as $ind => $file) {
			if(!in_array($file, $adjEliminados)){
				$archivos .= $file . '|';
			}else{
				//eliminamos el archivo de carpeta
				unlink(FCPATH.'/img/dynamic/adjuntos-carrito/' . $file);
			}
		}		

		$carrito['car_id'] = $car_id;
		$carrito['adjuntos'] = $archivos;

		$response['result'] = $carrito['adjuntos'];	
		$this->load->model('M_proveedores');	
		$response['sql'] = $this->M_proveedores->set_adjuntos_carrito($carrito);
		$response['status'] = $response['sql'];
		echo json_encode($response);
	}

	public function send_mail_proveedores(){
		$objCorreos = $this->input->post();
		$this->load->model('M_proveedores');	

		//GENERAMOS EL EMAIL
		$this->load->library('email');
		$pathFiles = FCPATH.'/img/dynamic/adjuntos-carrito/';

		foreach ($objCorreos['datos'] as $key => $c) {
			$data = null;
			//print_r($c);
			$this->email->from(EMAIL, $c['datos_correo']['from']);
			$this->email->to($c['datos_correo']['to']);
			$this->email->cc($c['datos_correo']['cc']);			
			
			$mail_bcc = '';
			foreach ($c['datos_correo']['bbc'] as $ind_bcc => $bcc) {
				$mail_bcc .= $bcc . ',';
			}

			$this->email->bcc(substr($mail_bcc, 0, -1));
			$this->email->subject($c['datos_correo']['subject']);

			if(!empty($objCorreos['adjuntos'])){
				$adjuntos = explode('|',$objCorreos['adjuntos'] );
				$pedido = array(
					'pe_adjunto1' => $objCorreos['adjuntos'],
					'pe_id' => $c['productos']['pe_id']
				);

				$this->M_proveedores->set_adjuntos_pedido($pedido);
				foreach ($adjuntos as $ind => $file) {
					if(!empty($file) && $file != '' && strlen($file) > 0){
						$this->email->attach($pathFiles.$file);
						$response['adjuntos'][] = $pathFiles.$file;
					}
				}				
			}

			$data = array(
				'BODY' => $this->load->view('emails/v_email_procesar_pedido', $c['productos'], TRUE)
			);
			
			$email = $this->load->view('emails/v_email', $data, TRUE);
			$this->email->message($email);
			$this->email->send();
		}

		$response['sql'] = TRUE;
		$response['status'] = $response['sql'];
		echo json_encode($response);
	}

	public function pruebas() {
		$this->load->library('Excel');
		$objPHPExcel = PHPExcel_IOFactory::load(PRIVADO.'../../pruebas.xlsx');
		//se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
      	$total_sheets=$objPHPExcel->getSheetCount();
      	$allSheetName=$objPHPExcel->getSheetNames();
      	$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
      	//Se obtiene el n�mero m�ximo de filas
      $highestRow = $objWorksheet->getHighestRow();
      //Se obtiene el n�mero m�ximo de columnas
      $highestColumn = $objWorksheet->getHighestColumn();
      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
      //$headingsArray contiene las cabeceras de la hoja excel. Llos titulos de columnas
      $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
      $headingsArray = $headingsArray[1];
      //Se recorre toda la hoja excel desde la fila 2 y se almacenan los datos
       $r = -1;
       $namedDataArray = array();
       for ($row = 2; $row <= $highestRow; ++$row) {
            $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                  ++$r;
                  foreach($headingsArray as $columnKey => $columnHeading) {
                          $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                  } //endforeach
            } //endif
        }
       var_dump($namedDataArray);
		/*$objHoja = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		foreach ($objHoja as $iIndice => $objCelda) {
			echo $objCelda['A'];
		}*/
	}

	public function get_producto($product_id){
		$this->load->model('M_proveedores');
		$producto = $this->M_proveedores->get_producto($product_id);
		echo json_encode($producto);
	}
}

/* End of file proveedores.php */
/* Location: ./application/controllers/proveedores.php */