<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mantenimientos extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE MANTENIMIENTOS
	public function index() {
		$this->comprobar_usuario('mantenimientos');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('5')) {
			$acceso = $this->load->view('v_mantenimientos','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('mantenimientos'),
	        'MANTENIMIENTOS' => 'active',
			'BODY' => $acceso
	    );
		$this->load->view('v_admin',$datos);		
	}
	
	//BUSCA MANTENIMIENTOS PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('mantenimientos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('5') || $this->acceso('15') || $this->acceso('18') || $this->acceso('14') || $this->acceso('17')) {
			if ($this->input->post()) {
				$this->load->model('M_mantenimientos');
				$array = $this->M_mantenimientos->get_mantenimientos();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			if ($this->acceso('5')) {
				redirect(base_url().$this->lang->lang().'/mantenimientos','refresh');
			} else if ($this->acceso('15')) {
				redirect(base_url().$this->lang->lang().'/proveedores/carrito','refresh');
			} else if ($this->acceso('18')) {
				redirect(base_url().$this->lang->lang().'/taller','refresh');
			} else if ($this->acceso('14')) {
				redirect(base_url().$this->lang->lang().'/almacenes','refresh');
			} else if ($this->acceso('17')) {
				redirect(base_url().$this->lang->lang().'/salidas','refresh');
			}
		}
	}
	
	//BUSCA MANTENIMIENTOS PARA MOSTRAR
	public function buscar_mantenimiento() {
		$this->comprobar_usuario('mantenimientos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->input->post()) {
			$this->load->model('M_mantenimientos');
			$array = $this->M_mantenimientos->get_mantenimiento($this->input->post('id'));
			$aux = array();
			
			for ($i=0; $i<count($array); $i++) {
				$mostrar = false;
				
				if ($this->session->userdata('emp_tipo') == 0) {
					$mostrar = true;
				} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
					$mostrar = true;
				}
				
				if ($mostrar) {
					array_push($aux, $array[$i]);
				}
			}
			
			echo json_encode($aux);
		}
	}
	
	//BUSCA mantenimientos segun su cliente
	public function mantenimientos_cliente() {
		$this->comprobar_usuario('trabajos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->input->post()) {
			$this->load->model('M_mantenimientos');
			echo json_encode($this->M_mantenimientos->get_mantenimientos_cliente($this->input->post('cl_id')));
		}
	}
	
	//PAGINA PARA AÑADIR SUCURSALES
	public function nuevo_mantenimiento() {
		$this->comprobar_usuario('mantenimientos/nuevo_mantenimiento');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('5')) {
			if ($this->session->userdata('emp_crear') == 1) {
				$mantenimiento = array('man_id' => null);
				$datos = array(
				    'TITULO' => lang('nuevo.mantenimiento'),
				    'MANTENIMIENTOS' => 'active',
					'BODY' => $this->load->view('v_mantenimientos_nuevo_editar',$mantenimiento,true),
					'BREADCRUMB' => array(['nombre'=>lang('mantenimientos'), 'href'=>base_url().$this->lang->lang().'/mantenimientos'],['nombre'=>lang('nuevo.mantenimiento'), 'href'=>''])
				);
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/mantenimientos','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/mantenimientos','refresh');
		}	
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		if ($this->input->post('man_id') != '') {
			$this->comprobar_usuario('mantenimientos/editar_mantenimiento/'.$this->input->post('man_id'));	
		} else {
			$this->comprobar_usuario('mantenimientos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->input->post()) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			//VALIDAMOS LOS CAMPOS
			$this->form_validation->set_rules('man_contacto',lang('contacto'),'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('man_telefono',lang('telefono'),'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('man_barriada',lang('barriada'),'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('man_localidad',lang('localidad'),'trim|max_length[100]required||xss_clean');
			$this->form_validation->set_rules('man_postal',lang('postal'),'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('man_provincia',lang('provincia'),'trim|max_length[50]|required|xss_clean');
			$this->form_validation->set_rules('man_direccion',lang('direccion'),'trim|max_length[150]|required|xss_clean');
			$this->form_validation->set_rules('man_nombre',lang('nombre'),'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('man_latitud',lang('latitud'),'trim|max_length[15]|required|xss_clean');
			$this->form_validation->set_rules('man_longitud',lang('longitud'),'trim|max_length[15]|required|xss_clean');
			$this->form_validation->set_rules('man_email',lang('email'),'trim|required|valid_email|max_length[50]|xss_clean');
			$this->form_validation->set_rules('man_observaciones',lang('observaciones'),'xss_clean');
			
			if ($this->form_validation->run()) {
				if ($this->input->post('man_id') != '') {
					if ($this->session->userdata('emp_editar') == 0) {
						$response['editar'] = false;
					}
				} else {
					if ($this->session->userdata('emp_crear') == 0) {
						$response['crear'] = false;
					}
				}
				
				$this->load->model('M_mantenimientos');
				$response['sql'] = $this->M_mantenimientos->set_mantenimiento($this->input->post());						
				$response['status'] = $response['sql'];

				if($response['sql'] != false){
					$id = $response['sql'];
					$response['sql'] = true;
					$response['status'] = $response['sql'];
				}
			}
			
			foreach ($this->input->post() as $key => $value) {
			        if (form_error($key) != '') {
					$aux = array($key,form_error($key));
			       	array_push($errors,$aux);
				}
			}
			$response['errors'] = array_filter($errors);
			
			//GUARDAMOS LA BITACORA
			if ($response['status']) {
				$this->load->model('M_bitacora');
				$datos = array();
				$datos['tipo'] = 'PR'; //Proyectos
				$datos['asociado'] = $id;
				$datos['accion'] = ($this->input->post('man_id') != '')?'Editar':'Crear';
				$this->M_bitacora->set_bitacora($datos);
			}

			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		}
	}

	//CALCULO DE PRESUPUESTO POR COTIZACION
	private function mantenimiento_categoria_cotizacion($pr_id) {
		$this->load->model('M_mantenimientos');
		$this->load->model('M_presupuestos');
		$this->load->model('M_facturas');
		$this->load->model('M_proveedores');
		$productos = array();
		$facturas = array();
		$talleres = array();
		$result = array();

			
		//FACTURAS
		$aux_facturas = $this->M_facturas->get_facturas_presupuesto($pr_id);
		$aux_rectificativas = $this->M_facturas->get_rectificativas_presupuesto($pr_id);
		for ($f=0; $f<count($aux_facturas); $f++) {
			$auxre = false;
			if($aux_facturas[$f]->fa_estado <= 1){
				if(!empty($aux_rectificativas)){
					for ($fr=0; $fr<count($aux_rectificativas); $fr++) {
						if($aux_rectificativas[$fr]->fa_id == $aux_facturas[$f]->fa_id){
							array_push($facturas, $aux_rectificativas[$fr]);
							$auxre = true;
						}
					}
				}
				
				if(!$auxre){
					array_push($facturas, $aux_facturas[$f]);
				}
			}
		}	

		//PEDIDOS PRODUCTOS
		$pedidos = $this->M_proveedores->get_pedidos_presupuesto($pr_id);
		for ($p=0; $p<count($pedidos); $p++) {
			if ($pedidos[$p]->pe_estado > 0 && $pedidos[$p]->pe_estado != 3) {
				$product_id = explode(';',$pedidos[$p]->product_id);
				$product_id = array_filter($product_id);
				
				$product_cantidad = explode(';',$pedidos[$p]->product_cantidad);
				$product_cantidad = array_filter($product_cantidad);
				
				$product_recibido = explode(';',$pedidos[$p]->product_cantidad_recibido);
				$product_recibido = array_filter($product_recibido);

				for ($pr=0; $pr<count($product_id); $pr++) {
					$producto = $this->M_proveedores->get_producto($product_id[$pr]);
					$producto->cantidad = $product_cantidad[$pr];
					$producto->recibido = $product_recibido[$pr];
					array_push($productos,$producto);
				}
			}
		}

		//TALLER
		$taller = $this->M_taller->get_taller_presupuesto($pr_id);
		for ($t=0; $t<count($taller); $t++) {
			$product_taller_id = explode(';',$taller[$t]->product_id);
			$product_taller_id = array_filter($product_taller_id);
			
			$product_taller_cantidad = explode(';',$taller[$t]->product_cantidad);
			$product_taller_cantidad = array_filter($product_taller_cantidad);
			
			for ($pr=0; $pr<count($product_taller_id); $pr++) {
				$producto_taller = $this->M_proveedores->get_producto($product_taller_id[$pr]);
				$producto_taller->cantidad = $product_taller_cantidad[$pr];
				
				array_push($talleres,$producto_taller);
			}
		}
		

		$categorias = $this->db->from("categorias")->get();
		$auxProd = array();
		foreach($categorias->result() as $key => $cat) {
			foreach($productos as $key2 => $pro) {

				if($pro->cate_id == $cat->cate_id){
					if($auxProd[$cat->cate_id]){
						$auxProd[$cat->cate_id] += floatval($pro->product_precio) * floatval($pro->cantidad);
					}else{
						$auxProd[$cat->cate_id] = floatval($pro->product_precio) * floatval($pro->cantidad);
					}
				}else{
					if(!$auxProd[$cat->cate_id]){
						$auxProd[$cat->cate_id] = 0;
					}
				}
			}
		}
	
		$auxFac = array();
		foreach($facturas as $key => $fac) {
			if($fac->re_costos){
				$costos_apart = explode('/',$fac->re_costos);
			}else{
				$costos_apart = explode('/',$fac->fa_costos);
			}
			
			$costos = explode(';',$costos_apart[1]);
			$costos = array_filter($costos, "strlen");
			
			$id_costos = explode(';',$costos_apart[0]);
			$id_costos = array_filter($id_costos, "strlen");

			for ($f=0; $f<count($id_costos); $f++) {
				if($auxFac[$id_costos[$f]]){
					$auxFac[$id_costos[$f]] += floatval($costos[$f]);
				}else{
					$auxFac[$id_costos[$f]] = floatval($costos[$f]);
				}
			}
		}		
		
		$auxTaller = array();
		foreach($categorias->result() as $key => $cat) {
			foreach($talleres as $key2 => $taller) {

				if($taller->cate_id == $cat->cate_id){
					if($auxTaller[$cat->cate_id]){
						$auxTaller[$cat->cate_id] += floatval($taller->product_precio) * floatval($taller->cantidad);
					}else{
						$auxTaller[$cat->cate_id] = floatval($taller->product_precio) * floatval($taller->cantidad);
					}
				}else{
					if(!$auxTaller[$cat->cate_id]){
						$auxTaller[$cat->cate_id] = 0;
					}
				}
			}
		}

		array_push($result,$auxProd);
		array_push($result,$auxFac);
		array_push($result,$auxTaller);

		return $result;	
	}

	//CALCULO DE PRESUPUESTO
	private function mantenimiento_categoria($man_id, $pr_id = NULL) {
		$this->load->model('M_mantenimientos');
		$this->load->model('M_presupuestos');
		$this->load->model('M_facturas');
		$this->load->model('M_proveedores');
		$this->load->model('M_taller');
		$productos = array();
		$facturas = array();
		$talleres = array();
		$result = array();

		//PRESUPUESTOS DEL MANTENIMIENTO
		$presupuestos = $this->M_presupuestos->get_presupuestos_mantenimiento($man_id, $pr_id);
		//RECORREMOS LOS PRESUPUESTOS
		for ($i=0; $i<count($presupuestos); $i++) {
			
			//FACTURAS
			$aux_facturas = $this->M_facturas->get_facturas_presupuesto($presupuestos[$i]->pr_id);
			$aux_rectificativas = $this->M_facturas->get_rectificativas_presupuesto($presupuestos[$i]->pr_id);
			for ($f=0; $f<count($aux_facturas); $f++) {
				$auxre = false;
				if($aux_facturas[$f]->fa_estado <= 1){
					if(!empty($aux_rectificativas)){
						for ($fr=0; $fr<count($aux_rectificativas); $fr++) {
							if($aux_rectificativas[$fr]->fa_id == $aux_facturas[$f]->fa_id){
								array_push($facturas, $aux_rectificativas[$fr]);
								$auxre = true;
							}
						}
					}
					
					if(!$auxre){
						array_push($facturas, $aux_facturas[$f]);
					}
				}
			}	

			//PEDIDOS PRODUCTOS
			$pedidos = $this->M_proveedores->get_pedidos_presupuesto($presupuestos[$i]->pr_id);
			for ($p=0; $p<count($pedidos); $p++) {
				if ($pedidos[$p]->pe_estado > 0 && $pedidos[$p]->pe_estado != 3) {
					$product_id = explode(';',$pedidos[$p]->product_id);
					$product_id = array_filter($product_id);
					
					$product_cantidad = explode(';',$pedidos[$p]->product_cantidad);
					$product_cantidad = array_filter($product_cantidad);
					
					$product_recibido = explode(';',$pedidos[$p]->product_cantidad_recibido);
					$product_recibido = array_filter($product_recibido);

					for ($pr=0; $pr<count($product_id); $pr++) {
						$producto = $this->M_proveedores->get_producto($product_id[$pr]);
						$producto->cantidad = $product_cantidad[$pr];
						$producto->recibido = $product_recibido[$pr];
						array_push($productos,$producto);
					}
				}
			}

			//TALLER
			$taller = $this->M_taller->get_taller_presupuesto($presupuestos[$i]->pr_id);
			for ($t=0; $t<count($taller); $t++) {
				$product_taller_id = explode(';',$taller[$t]->product_id);
				$product_taller_id = array_filter($product_taller_id);
				
				$product_taller_cantidad = explode(';',$taller[$t]->product_cantidad);
				$product_taller_cantidad = array_filter($product_taller_cantidad);
				
				for ($pr=0; $pr<count($product_taller_id); $pr++) {
					$producto_taller = $this->M_proveedores->get_producto($product_taller_id[$pr]);
					$producto_taller->cantidad = $product_taller_cantidad[$pr];
					
					array_push($talleres,$producto_taller);
				}
			}
		}

		$categorias = $this->db->from("categorias")->get();
		$auxProd = array();
		foreach($categorias->result() as $key => $cat) {
			foreach($productos as $key2 => $pro) {

				if($pro->cate_id == $cat->cate_id){
					if($auxProd[$cat->cate_id]){
						$auxProd[$cat->cate_id] += floatval($pro->product_precio) * floatval($pro->cantidad);
					}else{
						$auxProd[$cat->cate_id] = floatval($pro->product_precio) * floatval($pro->cantidad);
					}
				}else{
					if(!$auxProd[$cat->cate_id]){
						$auxProd[$cat->cate_id] = 0;
					}
				}
			}
		}
	
		$auxFac = array();
		foreach($facturas as $key => $fac) {
			if($fac->re_costos){
				$costos_apart = explode('/',$fac->re_costos);
			}else{
				$costos_apart = explode('/',$fac->fa_costos);
			}
			
			$costos = explode(';',$costos_apart[1]);
			$costos = array_filter($costos, "strlen");
			
			$id_costos = explode(';',$costos_apart[0]);
			$id_costos = array_filter($id_costos, "strlen");

			for ($f=0; $f<count($id_costos); $f++) {
				if($auxFac[$id_costos[$f]]){
					$auxFac[$id_costos[$f]] += floatval($costos[$f]);
				}else{
					$auxFac[$id_costos[$f]] = floatval($costos[$f]);
				}
			}
		}		
		
		$auxTaller = array();
		foreach($categorias->result() as $key => $cat) {
			foreach($talleres as $key2 => $taller) {

				if($taller->cate_id == $cat->cate_id){
					if($auxTaller[$cat->cate_id]){
						$auxTaller[$cat->cate_id] += floatval($taller->product_precio) * floatval($taller->cantidad);
					}else{
						$auxTaller[$cat->cate_id] = floatval($taller->product_precio) * floatval($taller->cantidad);
					}
				}else{
					if(!$auxTaller[$cat->cate_id]){
						$auxTaller[$cat->cate_id] = 0;
					}
				}
			}
		}

		array_push($result,$auxProd);
		array_push($result,$auxFac);
		array_push($result,$auxTaller);

		return $result;	
	}

	//COSULTA DE PRESUPUESTO
	public function get_presupuesto($man_id){
		$pr_id = NULL;
		if (!empty($this->input->post('pr_id'))) {
			$pr_id = $this->input->post('pr_id');	
		} 

		$response = $this->mantenimiento_categoria($man_id, $pr_id);
		echo json_encode($response); 
	}
	
	//PAGINA PARA EDITAR MANTENIMIENTOS
	public function editar_mantenimiento($man_id=null) {
		if ($man_id != null) {
			$this->comprobar_usuario('mantenimientos/editar_mantenimiento/'.$man_id);	
		} else {
			$this->comprobar_usuario('mantenimientos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('5')) {
			if ($man_id == null) {
				redirect(base_url().$this->lang->lang().'/mantenimientos','refresh');
			} else {
				$this->load->model('M_mantenimientos');
				$this->load->model('M_presupuestos');
				$this->load->model('M_facturas');
				$this->load->model('M_proveedores');
				$this->load->model('M_empleados');
				$this->load->model('M_taller');
				$this->load->model('M_trabajos');
				$this->load->model('M_inspecciones');
				$this->load->model('M_almacenes');
				
				$mantenimiento = $this->M_mantenimientos->get_mantenimiento($man_id);
				
				$informe = array();
				
				//PRESUPUESTOS DEL MANTENIMIENTO
				$presupuestos = $this->M_presupuestos->get_presupuestos_mantenimiento($man_id);
				
				//RECORREMOS LOS PRESUPUESTOS
				for ($i=0; $i<count($presupuestos); $i++) {
					//PRESUPUESTO
					$aux['presupuesto'] = $presupuestos[$i];
					
					//FACTURAS
					$aux['facturas'] = $this->M_facturas->get_facturas_presupuesto($presupuestos[$i]->pr_id);
					
					//RECTIFICATIVAS
					$aux['rectificativas'] = $this->M_facturas->get_rectificativas_presupuesto($presupuestos[$i]->pr_id);
					
					//PEDIDOS PRODUCTOS
					$pedidos = $this->M_proveedores->get_pedidos_presupuesto($presupuestos[$i]->pr_id);
					
					for ($p=0; $p<count($pedidos); $p++) {
						//ALMACENES QUE GUARDARON EL PRODUCTO
						$almacenes = explode(';',$pedidos[$p]->product_cantidad_recibido_almacen);
						$almacenes = array_filter($almacenes);
						$almacenes_guardado = array();
						
						for ($a=0; $a<count($almacenes); $a++) {
							$aux_id_al = '';
							$aux_prod_id = '';
							$suma_guardados = 0;
							$encontrado = false;
							
							$aux_almacenes = explode(',',$almacenes[$a]);
							$aux_almacenes = array_filter($aux_almacenes);
							
							for ($o=0; $o<count($almacenes_guardado); $o++) {
								if ($almacenes_guardado[$o][0] == $aux_almacenes[0] && $almacenes_guardado[$o][2] == $aux_almacenes[2]) {
									$encontrado = true;
								}
							}
							
							if (!$encontrado) {
								for ($w=0; $w<count($almacenes); $w++) {
									$aux_almacenes2 = explode(',',$almacenes[$w]);
									$aux_almacenes2 = array_filter($aux_almacenes2);
									if ($aux_almacenes[0] == $aux_almacenes2[0] && $aux_almacenes[2] == $aux_almacenes2[2]) {
										$suma_guardados = (int)$suma_guardados + (int)$aux_almacenes2[1];
										$aux_id_al = $aux_almacenes[2];
										$aux_prod_id = $aux_almacenes[0];
									}
								}
								
								$this->load->model('M_almacenes');
								$almacen = $this->M_almacenes->get_almacen($aux_id_al);
								$alm = array($aux_prod_id,$suma_guardados,$aux_id_al,$almacen->al_nombre);
								array_push($almacenes_guardado,$alm);
							}
						}
						
						
						
						
						$product_id = explode(';',$pedidos[$p]->product_id);
						$product_id = array_filter($product_id);
						
						$product_cantidad = explode(';',$pedidos[$p]->product_cantidad);
						$product_cantidad = array_filter($product_cantidad);
						
						$product_recibido = explode(';',$pedidos[$p]->product_cantidad_recibido);
						$product_recibido = array_filter($product_recibido);
						$productos = array();
						for ($pr=0; $pr<count($product_id); $pr++) {
							$producto = $this->M_proveedores->get_producto($product_id[$pr]);
							$produ = array(
								'product_id' => $producto->product_id,
								'product_nombre' => $producto->product_nombre,
								'proveedor' => $producto->pro_nombre,
								'cantidad' => $product_cantidad[$pr],
								'recibido' => $product_recibido[$pr],
								'product_referencia' => $producto->product_referencia,
								'product_caracteristicas' => $producto->product_caracteristicas,
								'product_precio' => $producto->product_precio,
								'cate_id' => $producto->cate_id,
								'almacenes' => $almacenes_guardado
							);
							
							array_push($productos,$produ);
						}
						$pedidos[$p]->productos = $productos;
						$pedidos[$p]->empleado = $this->M_empleados->get_empleado($pedidos[$p]->emp_id);
						$pedidos[$p]->emp_aprobado = $this->M_empleados->get_empleado($pedidos[$p]->emp_id_aprobado);
						
						//EMPLEADOS QUE RECIBIERON LOS PRODUCTOS
						$emp_rec = $pedidos[$p]->emp_id_recepcion;
						$emp_rec = explode(';',$emp_rec);
						$emp_rec = array_filter($emp_rec);
						$em_recepcion = '';
						for ($e=0; $e<count($emp_rec); $e++) {
							$emp = $this->M_empleados->get_empleado_id($emp_rec[$e]);
							$empleado = $emp->emp_nombre.' '.$emp->emp_apellido1.' '.$emp->emp_apellido2.'<br>';
							$em_recepcion = $em_recepcion.$empleado;
						}
						$pedidos[$p]->empleados_recepcion = $em_recepcion;
					}
					$aux['pedidos'] = $pedidos;
					
					//TALLER
					$taller = $this->M_taller->get_taller_presupuesto($presupuestos[$i]->pr_id);
					
					for ($t=0; $t<count($taller); $t++) {
						$product_id = explode(';',$taller[$t]->product_id);
						$product_id = array_filter($product_id);
						
						$product_cantidad = explode(';',$taller[$t]->product_cantidad);
						$product_cantidad = array_filter($product_cantidad);
						
						$productos = array();
						for ($pr=0; $pr<count($product_id); $pr++) {
							$producto = $this->M_proveedores->get_producto($product_id[$pr]);
							$produ = array(
								'product_nombre' => $producto->product_nombre,
								'cantidad' => $product_cantidad[$pr],
								'product_referencia' => $producto->product_referencia,
								'product_caracteristicas' => $producto->product_caracteristicas
							);
							
							array_push($productos,$produ);
						}
						$taller[$t]->productos = $productos;
					}
					$aux['taller'] = $taller;
					
					//TRABAJOS
					$trabajos = $this->M_trabajos->get_trabajos_presupuesto($presupuestos[$i]->pr_id);
					
					for ($tb=0; $tb<count($trabajos); $tb++) {
						//EMPLEADOS
						$emp_id = explode(',',$trabajos[$tb]->emp_id);
						$emp_id = array_filter($emp_id);
						$empleados = array();
						for ($emp=0; $emp<count($emp_id); $emp++) {
							$empleado = $this->M_empleados->get_empleado($emp_id[$emp]);
							$aux_emp = array(
								'emp_nombre' => $empleado->emp_nombre.' '.$empleado->emp_apellido1.' '.$empleado->emp_apellido2,
								'emp_dni' => $empleado->emp_dni,
								'emp_id' => $empleado->emp_id,
								'emp_email' => $empleado->emp_email,
								'emp_telefono' => $empleado->emp_telefono
							);
							array_push($empleados,$aux_emp);
						}
						$trabajos[$tb]->empleados = $empleados;
						
						//INSPECCIONES SIMPLES
						$in_id = explode(',',$trabajos[$tb]->in_id);
						$in_id = array_filter($in_id);
						$inspeccs = array();
						for ($in=0; $in<count($in_id); $in++) {
							$inspecc = $this->M_inspecciones->get_inspecciones_trabajo($in_id[$in],$trabajos[$tb]->tr_id,$mantenimiento->man_id);
							if ($inspecc == null) {
							$inspecc = $this->M_inspecciones->get_inspeccion($in_id[$in]);	
							}
							
							$inspecc->empleado = $this->M_empleados->get_empleado($inspecc->emp_id);
							
							//IMAGENES SIMPLES
							//DIRECCION DE LAS FOTOS
							$ruta = PRIVADO.$this->session->userdata("em_cif").'/in/'.$trabajos[$tb]->tr_id.'_'.$in_id[$in].'_'.$mantenimiento->man_id;
							
							$simples = array();
							//RECORRE LA CARPETA
							if (file_exists($ruta)) {
								//CREA UN OBJETO DE CARPETA
								$dirint = dir($ruta);
								
								while (($archivo = $dirint->read()) !== false) {
									//SI NO ES JPG NO PASA
									if (preg_match("/jpg/", $archivo)){
										//CREA LA IMAGEN						
										$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata('em_cif').'/in/'.$trabajos[$tb]->tr_id.'_'.$in_id[$in].'_'.$mantenimiento->man_id.'/'.$archivo;
										
										$nombre = substr($archivo, 0, count($archivo)-5);
										
										//LISTADO DE IMAGENES
										$foto = '<a rel="'.$trabajos[$tb]->tr_id.$in_id[$in].'" class="fancybox" href="'.$img.'" title="'.$nombre.'"><img src="'.$img.'" /></a>';
										array_push($simples,$foto);
									}
								}
								
								//CERRAMOS CARPETA
								$dirint->close();
								$inspecc->fotos = $simples;
							}
							
							array_push($inspeccs,$inspecc);
						}
						$trabajos[$tb]->simples = $inspeccs;
						
						//INSPECCIONES AVANZADAS
						$inv_id = explode(',',$trabajos[$tb]->inv_id);
						$inv_id = array_filter($inv_id);
						$inspeccs_v = array();
						for ($inv=0; $inv<count($inv_id); $inv++) {
							$inspecc_v = $this->M_inspecciones->get_inspecciones_avanzadas_trabajo($inv_id[$inv],$trabajos[$tb]->tr_id,$mantenimiento->man_id);
							if ($inspecc_v == null) {
							$inspecc_v = $this->M_inspecciones->get_inspeccion_avanzada($inv_id[$in]);	
							}
							
							$inspecc_v->empleado = $this->M_empleados->get_empleado($inspecc_v->emp_id);
							
							//IMAGENES AVANZADAS
							//DIRECCION DE LAS FOTOS
							$ruta = PRIVADO.$this->session->userdata("em_cif").'/inv/'.$trabajos[$tb]->tr_id.'_'.$inv_id[$inv].'_'.$mantenimiento->man_id;
							
							$avanzadas = array();
							//RECORRE LA CARPETA
							if (file_exists($ruta)) {
								//CREA UN OBJETO DE CARPETA
								$dirint = dir($ruta);
								
								while (($archivo = $dirint->read()) !== false) {
									//SI NO ES JPG NO PASA
									if (preg_match("/jpg/", $archivo)){
										//CREA LA IMAGEN						
										$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata('em_cif').'/inv/'.$trabajos[$tb]->tr_id.'_'.$inv_id[$inv].'_'.$mantenimiento->man_id.'/'.$archivo;
										
										$nombre = substr($archivo, 0, count($archivo)-5);
										
										//LISTADO DE IMAGENES
										$foto = '<a rel="'.$trabajos[$tb]->tr_id.$inv_id[$inv].'" class="fancybox" href="'.$img.'" title="'.$nombre.'"><img src="'.$img.'" /></a>';
										array_push($avanzadas,$foto);
									}
								}
								
								//CERRAMOS CARPETA
								$dirint->close();
								$inspecc_v->fotos = $avanzadas;
							}
							
							array_push($inspeccs_v,$inspecc_v);
						}
						$trabajos[$tb]->avanzadas = $inspeccs_v;
					}
					$aux['trabajos'] = $trabajos;

					//CATEGORIAS
					$aux['categoria'] = $this->mantenimiento_categoria_cotizacion($presupuestos[$i]->pr_id);
					array_push($informe,$aux);

				}
				
				$mantenimiento->informe = $informe;
				$mantenimiento->categorias = $this->mantenimiento_categoria($man_id);
				$mantenimiento->costoTaller = $this->M_almacenes->get_costos_pedidos_cotizacion($man_id);
				
				//PREGUNTAMOS SI DEVOLVIO RESULTADOS
				if ($mantenimiento != null) {
					$datos = array(
				        'TITULO' => lang('editar.mantenimiento'),
				        'MANTENIMIENTOS' => 'active',
						'BODY' => $this->load->view('v_mantenimientos_nuevo_editar',$mantenimiento,true),
						'BREADCRUMB' => array(['nombre'=>lang('mantenimientos'), 'href'=>base_url().$this->lang->lang().'/mantenimientos'],['nombre'=>lang('editar.mantenimiento'), 'href'=>''])
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/mantenimientos','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/mantenimientos','refresh');
		}
	}
	
	public function enviar_email() {
		if ($this->input->post('man_id') != '') {
			$this->comprobar_usuario('mantenimientos/editar_mantenimiento/'.$this->input->post('man_id'));	
		} else {
			$this->comprobar_usuario('mantenimientos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('5')) {
			if ($this->session->userdata('emp_editar') == 1) {				
				$this->load->library('email');
				$nombre = "=?UTF-8?B?".base64_encode($this->session->userdata('em_nombre'))."=?=";
				$this->email->from(EMAIL, $nombre);
				$this->email->subject(lang('recordar.mantenimiento').': '.$this->input->post('man_id'));
				//PLANTILLA EMAIL
				$this->load->model('M_mantenimientos');
				$mantenimiento = $this->M_mantenimientos->get_mantenimiento($this->input->post('man_id'));
				$this->load->library('encrypt');
				$datos = array (
					'man_id'	=>	$this->input->post('man_id'),
					'clave'		=>	$this->encrypt->decode($mantenimiento->man_clave)
				);
					
				$data = array(
					'BODY' => $this->load->view('emails/v_email_recordar_mantenimiento',$datos, TRUE)
				);
				$email = $this->load->view('emails/v_email', $data, TRUE);

				$this->email->message($email);
				
				//STRING PARA LOS EMAILS
				$para = '';
				$copia = '';
				$oculta = '';
				
				//GENERAMOS LOS STRING DE EMAILS
				$datos = $this->input->post('datos');
				for ($x=0;$x<count($datos);$x++) {
					if ($datos[$x]['value'] == 1) {
						$para = $para.$datos[$x]['email'].',';
					} else if ($datos[$x]['value'] == 2) {
						$copia = $copia.$datos[$x]['email'].',';
					} else if ($datos[$x]['value'] == 3) {
						$oculta = $oculta.$datos[$x]['email'].',';
					}
				}
				
				//QUITAMOS EL ULTIMO CARACTER QUE ES LA COMA
				$para = trim($para, ',');
				$copia = trim($copia, ',');
				$oculta = trim($oculta, ',');
				
				//CREAMOS LAS CABECERAS PARA ENVIAR
				$this->email->to($para);
				$this->email->cc($copia);
				$this->email->bcc($oculta);
				
				//ENVIAMOS EL EMAIL  	
				$this->email->send();
			} else {
				$this->comprobar_usuario('mantenimientos/editar_mantenimiento/'.$this->input->post('man_id'));
			}
		} else {
			redirect(base_url().$this->lang->lang().'/mantenimientos','refresh');
		}
	}
}

/* End of file mantenimientos.php */
/* Location: ./application/controllers/mantenimientos.php */