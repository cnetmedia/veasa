<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departamentos extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE DEPARTAMENTOS
	public function index() {
		$this->comprobar_usuario('departamentos');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('7')) {
			$acceso = $this->load->view('v_departamentos','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('departamentos'),
	        'EMPRESA' => 'active',
	        'DEPARTAMENTOS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('departamentos'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA DEPARTAMENTOS PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('departamentos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6') || $this->acceso('7') || $this->acceso('8')) {
			if ($this->input->post()) {
				$this->load->model('M_departamentos');
				$array = $this->M_departamentos->get_departamentos();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			//DENEGAMOS EL ACCESO
			if ($this->acceso('7')) {
				redirect(base_url().$this->lang->lang().'/departamentos','refresh');
			} else if ($this->acceso('8')) {
				redirect(base_url().$this->lang->lang().'/empleados','refresh');
			}
		}
	}
	
	//BUSCA DEPARTAMENTOS PARA MOSTRAR SEGUN SU SUCURSAL
	public function departamento_sucursal() {
		$this->comprobar_usuario('departamentos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('6') || $this->acceso('12') || $this->acceso('10')) {
			if ($this->input->post()) {
				$this->load->model('M_departamentos');
				echo json_encode($this->M_departamentos->get_departamento_sucursal($this->input->post('su_id')));
			}
		} else {
			//DENEGAMOS EL ACCESO
			if ($this->acceso('6')) {
				redirect(base_url().$this->lang->lang().'/sucursales','refresh');
			} else if ($this->acceso('12')) {
				redirect(base_url().$this->lang->lang().'/notas','refresh');
			} else if ($this->acceso('10')) {
				redirect(base_url().$this->lang->lang().'/trabajos','refresh');
			}
		}
	}
	
	//PAGINA PARA EDITAR DEPARTAMENTOS
	public function editar_departamento($de_id=null) {
		if ($de_id != null) {
			$this->comprobar_usuario('departamentos/editar_departamento/'.$de_id);	
		} else {
			$this->comprobar_usuario('departamentos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('7')) {
			if ($de_id == null) {
				redirect(base_url().$this->lang->lang().'/departamentos','refresh');
			} else {
				$this->load->model('M_departamentos');
				$departamento = $this->M_departamentos->get_departamento_id($de_id);
				//PREGUNTAMOS SE DEVOLVIO RESULTADOS
				if ($departamento != null) {
					$datos = array(
					    'TITULO' => lang('editar.departamento'),
					    'EMPRESA' => 'active',
					    'DEPARTAMENTOS' => 'active',
						'BODY' => $this->load->view('v_departamentos_nuevo_editar',$departamento,true),
						'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('departamentos'), 'href'=>base_url().$this->lang->lang().'/departamentos'],['nombre'=>lang('editar.departamento'), 'href'=>''])
					);
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/departamentos','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/departamentos','refresh');
		}
	}
	
	//PAGINA PARA AÑADIR DEPARTAMENTOS
	public function nuevo_departamento() {
		$this->comprobar_usuario('departamentos/nuevo_departamento');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('7')) {
			if ($this->session->userdata('emp_crear') == 1) {
				$departamento = array('de_id' => null,'de_estado' => '1','su_id' => null);
				$datos = array(
			        'TITULO' => lang('nuevo.departamento'),
			        'EMPRESA' => 'active',
			        'DEPARTAMENTOS' => 'active',
					'BODY' => $this->load->view('v_departamentos_nuevo_editar',$departamento,true),
					'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('departamentos'), 'href'=>base_url().$this->lang->lang().'/departamentos'],['nombre'=>lang('nuevo.departamento'), 'href'=>''])
			    );
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/departamentos','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/departamentos','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS LOS DEPARTAMENTOS
	public function guardar_editar() {
		if ($this->input->post('de_id') != '') {
			$url = 'departamentos/editar_departamento/'.$this->input->post('de_id');	
		} else {
			$url = 'departamentos/nuevo_departamento';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('7')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('de_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('de_nombre',lang('nombre'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('de_id',lang('dni'),'trim|xss_clean');
					$this->form_validation->set_rules('su_id','','trim|required|xss_clean');
					
					if ($this->form_validation->run()) {
						$this->load->model('M_departamentos');
						$depart = $this->M_departamentos->get_departamento($this->input->post('de_nombre'),$this->input->post('su_id'));
						if (count($depart) > 0) {
							$f = array('de_nombre',lang('departamento.exite'));
							array_push($errors,$f);
						} else {
							$response['sql'] = $this->M_departamentos->set_departamento($this->input->post());
							$response['status'] = $response['sql'];

							if($response['sql'] != false){
								$id = $response['sql'];
								$response['sql'] = true;
								$response['status'] = $response['sql'];
							}
					
						}
					}
					
					//GENERAMOS ERRORES SI LOS HUBIESE
					foreach ($this->input->post() as $key => $value) {
				        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors);

				    //GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'DE'; //Departamento
						$datos['asociado'] = $id;
						$datos['accion'] = ($this->input->post('de_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}
				}
			}
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/departamentos','refresh');
		}
	}
	
	//HABILITAMOS O DESHABILITAMOS
	public function estado() {
		//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
		$response['status'] = false;
		$response['editar'] = true;
		$response['sql'] = true;
		
		$this->comprobar_usuario('departamentos/editar_departamento/'.$this->input->post('de_id'));	
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('7')) {
			if ($this->session->userdata('emp_editar') == 1) {
				if ($this->input->post()) {
					$this->load->model('M_departamentos');
					$response['sql'] = $this->M_departamentos->set_estado($this->input->post('de_id'));
					$response['status'] = $response['sql'];
				}
			} else {
				$response['editar'] = false;
			}
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/departamentos','refresh');
		}
	}
}

/* End of file departamentos.php */
/* Location: ./application/controllers/departamentos.php */