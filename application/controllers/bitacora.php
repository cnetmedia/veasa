<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bitacora extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	
	//BUSCA LISTA DE BITACORA PARA MOSTRAR
	public function buscar_todo() {	
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18') || $this->acceso('20')) {
			if ($this->input->post()) {
				$this->load->model('M_bitacora');
				$array = $this->M_bitacora->get_lista_bitacora($this->input->post());
				
				echo json_encode($array);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/informes','refresh');
		}
	}
}

/* End of file bitacora.php */
/* Location: ./application/controllers/bitacora.php */