<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendario extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE CLIENTES
	public function index() {
		$this->comprobar_usuario('calendario');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('13')) {
			$acceso = $this->load->view('v_calendario','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('calendario'),
	        'CALENDARIO' => 'active',
			'BODY' => $acceso
	    );
		$this->load->view('v_admin',$datos);
	}
}

/* End of file calendario.php */
/* Location: ./application/controllers/calendario.php */