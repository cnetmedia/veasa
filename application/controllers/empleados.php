<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empleados extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE DEPARTAMENTOS
	public function index() {
		$this->comprobar_usuario('empleados');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('8')) {
			$acceso = $this->load->view('v_empleados','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('empleados'),
	        'EMPRESA' => 'active',
	        'EMPLEADOS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('empleados'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);		
	}
	
	//BUSCA EMPLEADOS PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('empleados');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('8')) {
			if ($this->input->post()) {
				$this->load->model('M_empleados');
				$array = $this->M_empleados->get_empleados();
				$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}
				
				echo json_encode($aux);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/empleados','refresh');
		}
	}
	
	//BUSCA EMPLEADOS PARA MOSTRAR SEGUN SU DEPARTAMENTO
	public function empleados_departamento() {
		$this->comprobar_usuario('notas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->input->post()) {
			$this->load->model('M_empleados');
			echo json_encode($this->M_empleados->get_empleados_departamento($this->input->post('su_id'),$this->input->post('de_id')));
		}
	}
	
	//BUSCA EMPLEADOS LIBRES, QUE NO ESTEN TRABAJANDO
	public function empleados_libres_trabajos() {
		$this->comprobar_usuario('trabajos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->input->post()) {
			$this->load->model('M_empleados');
			echo json_encode($this->M_empleados->get_empleados_libres_trabajos($this->input->post('su_id'),$this->input->post('fecha'),$this->input->post('tr_id'),$this->input->post('emp_id')));
		}
	}
	
	//PAGINA PARA AÑADIR EMPLEADOS
	public function nuevo_empleado() {
		$this->comprobar_usuario('empleados/nuevo_empleado');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('8')) {
			if ($this->session->userdata('emp_crear') == 1) {
				//OPCIONES POR DEFECTO PARA NUEVO EMPLEADO
				$empleado = array(
					'emp_id' => null,
					'emp_tipo' => $this->session->userdata('emp_tipo'),
					'emp_acceso' => '0',
					'emp_editar' => '0',
					'emp_crear' => '0',
					'emp_ver' => '',
					'de_id' => ''
				);
				$datos = array(
			        'TITULO' => lang('nuevo.empleado'),
			        'EMPRESA' => 'active',
			        'EMPLEADOS' => 'active',
					'BODY' => $this->load->view('v_empleados_nuevo_editar',$empleado,true),
					'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('empleados'), 'href'=>base_url().$this->lang->lang().'/empleados'],['nombre'=>lang('nuevo.empleado'), 'href'=>''])
			    );
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/empleados','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/empleados','refresh');
		}	
	}
	
	//PAGINA PARA EDITAR EMPLEADOS
	public function editar_empleado($emp_id=null) {
		if ($emp_id != null) {
			$this->comprobar_usuario('empleados/editar_empleado/'.$emp_id);	
		} else {
			$this->comprobar_usuario('empleados');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//COMPROBAMOS QUIEN INTENTA ACCEDER PARA EDITAR
		if ($emp_id == $this->session->userdata('emp_id')) {
			$ver = true;
		} else {
			if ($this->acceso('8')) {
				$ver = true;
			} else {
				$ver = false;
			}
		}
		
		if ($ver) {
			$this->load->model('M_empleados');
			$empleado = $this->M_empleados->get_empleado($emp_id);			
			$datos = array(
		        'TITULO' => lang('editar.empleado'),
		        'EMPRESA' => 'active',
		        'EMPLEADOS' => 'active',
				'BODY' => $this->load->view('v_empleados_nuevo_editar',$empleado,true),
				'BREADCRUMB' => array(['nombre'=>lang('empresa'), 'href'=>'#'],['nombre'=>lang('empleados'), 'href'=>base_url().$this->lang->lang().'/empleados'],['nombre'=>lang('editar.empleado'), 'href'=>''])
		    );
			$this->load->view('v_admin',$datos);
		} else {
			redirect(base_url().$this->lang->lang().'/empleados','refresh');
		}
	}
	
	//PREGUNTAMOS SI EXISTE UN EMPLEADO CON ESTE DNI
	public function empleado_dni($dni) {
		if ($this->input->post('emp_id') == '' || $this->input->post('emp_id') == null) {
			$this->load->model('M_empleados');
			$empleado = $this->M_empleados->get_empleado_dni($dni);
			if (count($empleado) > 0) {
				$this->form_validation->set_message('empleado_dni', lang('empleado.existente.dni'));
		        return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
	
	//PREGUNTAMOS SI EXISTE UN EMPLEADO CON ESTE EMAIL
	/*public function empleado_email($email) {
		if ($this->input->post('emp_id') == '' || $this->input->post('emp_id') == null) {
			$this->load->model('M_empleados');
			$empleado = $this->M_empleados->get_empleado_email($email);
			if (count($empleado) > 0) {
				$this->form_validation->set_message('empleado_email', lang('empleado.existente.email'));
		        return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}*/
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		if ($this->input->post('emp_id') != '') {
			$this->comprobar_usuario('empleados/editar_empleado/'.$this->input->post('emp_id'));	
		} else {
			$this->comprobar_usuario('empleados');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
				if ($this->input->post()) {
					//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
					$response['status'] = false;
					$response['crear'] = true;
					$response['editar'] = true;
					$response['sql'] = true;
					$response['errors'] = '';
					$errors = array();
					$aux = array();
					$entrar = false;
			
					//VALIDAMOS LOS CAMPOS
					$this->form_validation->set_rules('emp_nombre',lang('nombre'),'trim|required|max_length[20]|xss_clean');
					$this->form_validation->set_rules('emp_apellido1',lang('apellido1'),'trim|required|max_length[40]|xss_clean');
					$this->form_validation->set_rules('emp_apellido2',lang('apellido2'),'trim|max_length[40]|xss_clean');
					$this->form_validation->set_rules('emp_dni',lang('dni'),'trim|required|max_length[15]|callback_empleado_dni|xss_clean');
					$this->form_validation->set_rules('emp_telefono',lang('telefono'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('emp_email',lang('email'),'trim|required|valid_email|max_length[50]|xss_clean');
					$this->form_validation->set_rules('emp_provincia',lang('provincia'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('emp_postal',lang('postal'),'trim|max_length[11]|xss_clean');
					$this->form_validation->set_rules('emp_direccion',lang('direccion'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('emp_barriada',lang('barriada'),'trim|max_length[50]|xss_clean');
					$this->form_validation->set_rules('emp_localidad',lang('localidad'),'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('emp_pais',lang('pais'),'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('emp_nacionalidad',lang('nacionalidad'),'trim|required|max_length[60]|xss_clean');
					$this->form_validation->set_rules('emp_nacimiento',lang('nacimiento'),'trim|required|max_length[10]|xss_clean');
					$this->form_validation->set_rules('emp_id','','trim|xss_clean');
					$this->form_validation->set_rules('de_id',lang('departamento'),'trim|required|xss_clean');
					$this->form_validation->set_rules('su_id',lang('sucursal'),'trim|required|xss_clean');
					$this->form_validation->set_rules('emp_acceso','','trim|required|xss_clean');
					$this->form_validation->set_rules('emp_editar','','trim|required|xss_clean');
					$this->form_validation->set_rules('emp_crear','','trim|required|xss_clean');
					$this->form_validation->set_rules('emp_tipo','','trim|required|xss_clean');
					if ($this->input->post('emp_ver')) {
						$this->form_validation->set_rules('emp_ver[]','','trim|required|[xss_clean');
					}
					$this->form_validation->set_rules('emp_clave_uno',lang('login.clave'),'trim|min_length[6]|max_length[6]|xss_clean');
					$this->form_validation->set_rules('emp_clave_dos',lang('login.clave'),'trim|min_length[6]|max_length[6]|xss_clean');
					
					if ($this->form_validation->run()) {
						if ($this->input->post('emp_id') != '') {
							if ($this->session->userdata('emp_editar') == 0) {
								$response['editar'] = false;
							}
						} else {
							if ($this->session->userdata('emp_crear') == 0) {
								$response['crear'] = false;
							}
						}
						
						//COMPROBAMOS QUE TIENE SECCIONES VISIBLES MARCADAS
						if ($this->input->post('emp_acceso') == '1') {
							if ($this->input->post('emp_ver') == null) {
								$f = array('emp_ver',lang('empleado.ver.ko'));
								array_push($errors,$f);
							} else {
								$response['status'] = true;
							}
						} else {
							$response['status'] = true;
						}
						
						if ($response['status']) {
							$response['status'] = false;
							//COMPROBAMOS LA CLAVE
							$clave1 = $this->input->post('emp_clave_uno');
							$clave2 = $this->input->post('emp_clave_dos');
							
							if ($clave1 != '') {
								if ($clave2 != '') {
									$response['status'] = true;
								} else {
									$f = array('emp_clave_dos',lang('empleado.clave.ko'));
									array_push($errors,$f);
								}
							} else if ($clave2 != '') {
								if ($clave1 != '') {
									$response['status'] = true;
								} else {
									$f = array('emp_clave_uno',lang('empleado.clave.ko'));
									array_push($errors,$f);
								}
							} else {
								$response['status'] = true;
							}
							
							if ($response['status'] && $clave1 != '' && $clave2 != '') {
								$response['status'] = false;
								if ($clave1 != $clave2) {
									$aux = array('emp_clave_uno',lang('claves.errorenas'));
									array_push($errors, $aux);
									$aux = array('emp_clave_dos','');
									array_push($errors, $aux);
									$response['clave'] = true;
								} else {
									$response['status'] = true;
								}
							}
							
							if ($response['status']) {
								$response['status'] = false;
								//COMPROBAMOS LA FOTO
								if (!empty($_FILES['foto'])) {
									$name2 = $_FILES['foto']['name']; //get the name of the image
									list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
									
									if ($ext == 'jpg') {
										if ($_FILES['foto']['size'] <= 500000) {
											$response['status'] = true;
										} else {
											$f = array('foto',lang('max.size'));
											array_push($errors,$f);
										}
									} else {
										$f = array('foto',lang('jpg'));
										array_push($errors,$f);
									}
								} else {
									$response['status'] = true;
								}
							}
						}
						
						if ($response['status']) {
							$this->load->model('M_empleados');
							
							$clave = '';
							if ($clave1 != '') {
								$this->load->library('encrypt');
								$clave = $this->encrypt->encode($clave1);
							}
							//print_r($this->input->post());
							$response['sql'] = $this->M_empleados->set_empleado($this->input->post(),$clave);
							if($response['sql'] != false){
								if(!empty($response['sql']['emp_id_nuevo'])){
									$id = $response['sql']['emp_id_nuevo'];
								}else{
									$id = $response['sql'];
								}
								$response['sql'] = true;
								$response['status'] = $response['sql'];
							}
						}
					}
					
					foreach ($this->input->post() as $key => $value) {
				           if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors);
					
					//GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'EM'; //Empleados
						$datos['asociado'] = $id;
						$datos['accion'] = ($this->input->post('emp_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}

				    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
				    echo json_encode($response);
				}
	}
	
	public function login() {
		$this->data['token'] = $this->token();
		
		$datos = array(
            'TITULO' => lang('login.titulo'),
            'BODY' => $this->load->view('v_login',$this->data,true)
        );
		$this->load->view('v_1column',$datos);
	}
	
	//BOTON ENVIAR DEL LOGIN
	public function entrar() {
			//REGUNTAMOS SI EXISTE ENVIO DE INFORMACION POR POST
			if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
				
				$this->load->library('aes');
				$password = AesCtr::decrypt($this->input->post('emp_clave'), $this->session->userdata('token'), 256);
				
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = FALSE;
				$errors = array();
				$aux = array();
				//VALIDAMOS LOS CAMPOS
				$this->form_validation->set_rules('emp_dni',lang('dni'),'trim|required|max_length[30]|xss_clean');
				$this->form_validation->set_rules($password,lang('login.clave'),'trim|exact_length[6]|max_length[6]|xss_clean');
				//COMPROBAMOS LA VALIDACION
				if ($this->form_validation->run()) {
					//RECOGEMOS LOS CAMPOS ENVIADOS
					$userDni = $this->input->post('emp_dni');
		            //$password = $this->input->post('emp_clave');
		            //CARGAMOS EL MODELO
					$this->load->model('M_empleados');
					//LLAMAMOS AL METODO DEL MODELO QUE NOS DEVUELVE EL EMPLEADO O NULL
					$user = $this->M_empleados->get_empleado_dni($userDni);
					//SI LO ENCUENTRA ENTRA
					if ($user != null) {
						//PREGUNTA SI ESTE EMPLEADO TIENE ACCESO A LA APLICACION
						if ($user->emp_acceso == 1 && $user->emp_estado == 1) {
							//PREGUNTAMOS SI LA CLAVE ES IGUAL Y CARGAMOS LA LIBERIA ENCRYPT
							$this->load->library('encrypt');
							if ($this->encrypt->decode($user->emp_clave) == $password) {
								//VALIDAMOS OK Y GUARDAMOS LA INFO EN LA SESSION
								$response['status'] = true;
								//CREAMOS LA SESSION
								$this->emp_session($user->emp_dni);
			                    //ELIMINAMOS LA URL DE LA SESSION
			                    $url = $this->session->userdata('url');
			                    //SI EXISTE URL LA PASAMOS SINO VAMOS A UNA POR DEFECTO
			                    if (!isset($url) || (trim($url) != '')) {
			                    	$this->session->unset_userdata('url');
			                    	$response['url'] = base_url().$this->lang->lang()."/".$url;
			                    } else {
			                    	//if ($user->emp_tipo == '0') {
									if ($user->emp_tipo <= 1) {
										$response['url'] = base_url().$this->lang->lang().'/informes';
									} else {
										$response['url'] = base_url().$this->lang->lang().'/trabajos';
									}
								}
								
								//CREAMOS UNA COOKIE DE SESSION
								$this->load->helper('cookie');
					    		$this->input->set_cookie(array(
								    'name' => 'RememberMe', 
								    'value' => $user->emp_dni, 
								    'expire' => $this->config->item('sess_expiration'),
								    'path' => '/'
								));
							} else {
								$aux = array('emp_clave',lang('login.clave.error'));
								array_push($errors, $aux);
							}
						} else {
							$aux = array('emp_dni',lang('login.acceso.error'));
							array_push($errors, $aux);
						}
					} else {
						$aux = array('emp_dni',lang('login.dni.error'));
						array_push($errors, $aux);
					}
				}
			    //GENERAMOS UN ARRAY CON LOS ERRORES
			    if ($response['status'] == false) {
			        foreach ($this->input->post() as $key => $value) {
			            if (form_error($key) != '') {
							$aux = array($key,form_error($key));
			            	array_push($errors,$aux);
						}
			        }
			        $response['errors'] = array_filter($errors);
				}
			    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			    echo json_encode($response);
			}
	}
	
	//VALIDACION DE NUEVO EMPLEADO SEGUN EL EMP_ID PASADO COMO PARAMETRO
	public function validar($emp_id) {
		$this->load->model('M_empleados');
		$empleado = $this->M_empleados->get_valiar($emp_id);

		$dat_emp = array(
		    'DATOS_EMPLEADO' => $empleado
		);
			
		$datos = array(
		    'TITULO' => lang('activar.empleado'),
			'BODY' => $this->load->view('v_empleados_validar',$dat_emp,true)
		);
			
		$this->load->view('v_1column',$datos);
	}
	
	//ACTIVA EL EMPLEADO
	public function activar() {
		if ($this->input->post()) {
			$response['status'] = FALSE;
			$response['sql'] = true;
			$errors = array();
			$aux = array();
			
			$this->form_validation->set_rules('emp_clave_uno',lang('login.clave'),'trim|required|min_length[6]|max_length[6]|xss_clean');
			$this->form_validation->set_rules('emp_clave_dos',lang('login.clave'),'trim|required|min_length[6]|max_length[6]|xss_clean');
			if ($this->form_validation->run()) {
				$clave1 = $this->input->post('emp_clave_uno');
		        $clave2 = $this->input->post('emp_clave_dos');
		        
		        if ($clave1 != $clave2) {
					$aux = array('emp_clave_uno',lang('claves.errorenas'));
					array_push($errors, $aux);
					$aux = array('emp_clave_dos','');
					array_push($errors, $aux);
					$response['clave'] = true;
				} else {
					//CARGAMOS EL MODELO
					$this->load->model('M_empleados');
					$this->load->library('encrypt');
					$empleado = array(
			    		'emp_clave' => $this->encrypt->encode($clave1),
			    		'emp_id' => $this->input->post('emp_id')
					);
					$response['sql'] = $this->M_empleados->set_validar($empleado);
					$response['status'] = $response['sql'];
				}
			}
			
			foreach ($this->input->post() as $key => $value) {
				if (form_error($key) != '') {
					$aux = array($key,form_error($key));
					array_push($errors,$aux);
				}
			}
				
			$response['errors'] = array_filter($errors);
			
			echo json_encode($response);
		}
	}
	
	//RECORDAMOS LA CLAVE DEL EMPLEADO
	public function recordar_clave() {
		//REGUNTAMOS SI EXISTE ENVIO DE INFORMACION POR POST
		if ($this->input->post()) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['sql'] = true;
			$response['email'] = "";
			$errors = array();
			$aux = array();
			//VALIDAMOS LOS CAMPOS
			$this->form_validation->set_rules('emp_dni',lang('dni'),'trim|required|max_length[30]|xss_clean');
			//COMPROBAMOS LA VALIDACION
			if ($this->form_validation->run()) {
		           //CARGAMOS EL MODELO
				$this->load->model('M_empleados');
				//LLAMAMOS AL METODO DEL MODELO QUE NOS DEVUELVE EL EMPLEADO O NULL
				$user = $this->M_empleados->get_empleado_dni($this->input->post('emp_dni'));
				//SI LO ENCUENTRA ENTRA
				if ($user != null) {
					//PREGUNTA SI ESTE EMPLEADO TIENE ACCESO A LA APLICACION
					if ($user->emp_acceso == 1) {
						$this->load->model('M_empleados');
						$response['sql'] = $this->M_empleados->set_recordar_clave($user);
						$response['status'] = $response['sql'];
						$response['email'] = $user->emp_email;
					} else {
						$aux = array('emp_dni',lang('login.acceso.error'));
						array_push($errors, $aux);
					}
				} else {
					$aux = array('emp_dni',lang('login.dni.error'));
					array_push($errors, $aux);
				}
			}
		    //GENERAMOS UN ARRAY CON LOS ERRORES
		    foreach ($this->input->post() as $key => $value) {
		        if (form_error($key) != '') {
					$aux = array($key,form_error($key));
		           	array_push($errors,$aux);
				}
		    }
		    $response['errors'] = array_filter($errors);
		        
		    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
		    echo json_encode($response);
		}
	}
	
	//RECORDAMOS LA CLAVE DEL EMPLEADO
	public function restaurar_clave_empleado($rc_id,$emp_id) {
		$this->load->model('M_empleados');
		$empleado = $this->M_empleados->get_restaurar_clave($rc_id,$emp_id);

		$dat_emp = array(
		    'DATOS_EMPLEADO' => $empleado
		);
		
		$datos = array(
		    'TITULO' => lang('restaurar.clave.empleado'),
			'BODY' => $this->load->view('v_empleados_restaurar_clave',$dat_emp,true)
		);
		
		$this->load->view('v_1column',$datos);
	}
	
	//RESTAURA LA CLAVE DEL EMPLEADO
	public function restaurar_clave() {
		if ($this->input->post()) {
			$response['status'] = FALSE;
			$response['sql'] = true;
			$errors = array();
			$aux = array();
			
			$this->form_validation->set_rules('emp_clave_uno',lang('login.clave'),'trim|required|min_length[6]|max_length[6]|xss_clean');
			$this->form_validation->set_rules('emp_clave_dos',lang('login.clave'),'trim|required|min_length[6]|max_length[6]|xss_clean');
			if ($this->form_validation->run()) {
				$clave1 = $this->input->post('emp_clave_uno');
		        $clave2 = $this->input->post('emp_clave_dos');
		        
		        if ($clave1 != $clave2) {
					$aux = array('emp_clave_uno',lang('claves.errorenas'));
					array_push($errors, $aux);
					$aux = array('emp_clave_dos','');
					array_push($errors, $aux);
					$response['clave'] = true;
				} else {
					//CARGAMOS EL MODELO
					$this->load->model('M_empleados');
					$this->load->library('encrypt');
					$empleado = array(
			    		'emp_clave' => $this->encrypt->encode($clave1),
			    		'emp_id' => $this->input->post('emp_id')
					);
					$response['sql'] = $this->M_empleados->set_restaurar_clave($empleado);
					$response['status'] = $response['sql'];
				}
			}
			
			foreach ($this->input->post() as $key => $value) {
				if (form_error($key) != '') {
					$aux = array($key,form_error($key));
					array_push($errors,$aux);
				}
			}
				
			$response['errors'] = array_filter($errors);
			
			echo json_encode($response);
		}
	}
	
	public function salir() {
		$sessionData = array('loggedIn' => false);
		$this->session->set_userdata($sessionData);
    	// y eliminamos la sesión
    	$this->session->sess_destroy();
    	
    	//BORRAR COOKIE DE RECORDARME
		$this->load->helper('cookie');
		delete_cookie('RememberMe');
    	
    	// redirigimos al controlador principal
    	redirect(base_url(), 'refresh');
	}
	
	//HABILITAMOS O DESHABILITAMOS
	public function estado() {
		//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
		$response['status'] = false;
		$response['editar'] = true;
		$response['sql'] = true;
		
		$this->comprobar_usuario('empleados/editar_empleado/'.$this->input->post('emp_id'));	
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('8')) {
			if ($this->session->userdata('emp_editar') == 1) {
				if ($this->input->post()) {
					$this->load->model('M_empleados');
					$response['sql'] = $this->M_empleados->set_estado($this->input->post('emp_id'));
					$response['status'] = $response['sql'];
				}
			} else {
				$response['editar'] = false;
			}
			
			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/sucursales','refresh');
		}
	}
	
	/*
	FIN Empleados
	INICIO EMPLEADOS CLIENTES
	*/
	
	//PREGUNTAMOS SI EXISTE UN EMPLEADO CON ESTE EMAIL
	public function empleado_id() {
		$this->load->model('M_empleados');
		echo json_encode($this->M_empleados->get_empleado_id($this->input->post('emp_id')));
	}
	
	//GUARDAMOS LA ASISTENCIA DEL EMPLEADO
	public function trabajo_asistencia() {
		$this->comprobar_cliente('clientes/ver_mantenimiento/'.$this->input->post('man_id'));
		
		//REFRESCAMOS LA SESSION
		$this->cl_session($this->session->userdata('cliente'));
		
		$response['sql'] = true;
		$response['status'] = false;
		
		$this->load->model('M_empleados');
		
		$response['sql'] = $this->M_empleados->set_trabajo_asistencia($this->input->post('ea_estado'),$this->input->post('emp_id'),$this->input->post('tr_id'),$this->input->post('man_id'));
		
		$response['status'] = $response['sql'];
		
		echo json_encode($response);
	}

	//BOTON ENVIAR DEL LOGIN
	public function validar_solicitud() {
			//REGUNTAMOS SI EXISTE ENVIO DE INFORMACION POR POST
			if ($this->input->post('token')) {
				
				$this->load->library('aes');
				$password = AesCtr::decrypt($this->input->post('emp_clave'), $this->input->post('token'), 256);
				
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = FALSE;
				$errors = array();
				$aux = array();
				//VALIDAMOS LOS CAMPOS
				$this->form_validation->set_rules('emp_dni',lang('dni'),'trim|required|max_length[30]|xss_clean');
				$this->form_validation->set_rules($password,lang('login.clave'),'trim|exact_length[6]|max_length[6]|xss_clean');
				//COMPROBAMOS LA VALIDACION
				if ($this->form_validation->run()) {
					//RECOGEMOS LOS CAMPOS ENVIADOS
					$userDni = $this->input->post('emp_dni');
		            //$password = $this->input->post('emp_clave');
		            //CARGAMOS EL MODELO
					$this->load->model('M_empleados');
					//LLAMAMOS AL METODO DEL MODELO QUE NOS DEVUELVE EL EMPLEADO O NULL
					$user = $this->M_empleados->get_empleado_dni($userDni);
					//print_r($user->emp_tipo); exit();
					//SI LO ENCUENTRA ENTRA
					//NOTA: SI ES SUPER USUARIO
					if ($user != null && $user->emp_tipo == 0) {
						//PREGUNTA SI ESTE EMPLEADO TIENE ACCESO A LA APLICACION
						if ($user->emp_acceso == 1 && $user->emp_estado == 1) {
							//PREGUNTAMOS SI LA CLAVE ES IGUAL Y CARGAMOS LA LIBERIA ENCRYPT
							$this->load->library('encrypt');
							if ($this->encrypt->decode($user->emp_clave) == $password) {
								//VALIDAMOS OK Y GUARDAMOS LA INFO EN LA SESSION
								$response['status'] = true;
								$response['emp_id'] = $user->emp_id;
			                   
							} else {
								$aux = array('emp_clave',lang('login.clave.error'));
								array_push($errors, $aux);
							}
						} else {
							$aux = array('emp_dni',lang('login.acceso.error'));
							array_push($errors, $aux);
						}
					} else {
						if($user == null){
							$aux = array('emp_dni',lang('login.dni.error'));
							array_push($errors, $aux);
						}

						if($user->emp_tipo != 0){
							$aux = array('emp_tipo',lang('empleado.error.tipo'));
							array_push($errors, $aux);
						}
					}
				}
			    //GENERAMOS UN ARRAY CON LOS ERRORES
			    if ($response['status'] == false) {
			        foreach ($this->input->post() as $key => $value) {
			            if (form_error($key) != '') {
							$aux = array($key,form_error($key));
			            	array_push($errors,$aux);
						}
			        }
			        $response['errors'] = array_filter($errors);
				}
			    //DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			    echo json_encode($response);
			}
	}
}

/* End of file Empleados.php */
/* Location: ./application/controllers/Empleados.php */