<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taller extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE TRABAJOS DEL TALLER
	public function index() {
		$this->comprobar_usuario('taller');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18')) {
			$acceso = $this->load->view('v_taller','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('taller'),
	        'STOCKAGE' => 'active',
	        'TALLER' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('taller'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA TRABAJOS DEL TALLER PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('taller');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18') || $this->acceso('20')) {
			if ($this->input->post()) {
				$this->load->model('M_taller');
				$array = $this->M_taller->get_trabajos_taller();
				
				echo json_encode($array);
			}
		} else {
			if ($this->acceso('18')) {
				redirect(base_url().$this->lang->lang().'/taller','refresh');
			} else if ($this->acceso('20')) {
				redirect(base_url().$this->lang->lang().'/informes','refresh');
			}
		}
	}
	
	//BUSCA TRABAJOS DEL TALLER PARA MOSTRAR
	public function buscador_todos() {
		$this->comprobar_usuario('taller');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18') || $this->acceso('20')) {
			if ($this->input->post()) {
				$this->load->model('M_taller');
				$array = $this->M_taller->get_trabajos_taller_todos();
				
				echo json_encode($array);
			}
		} else {
			if ($this->acceso('18')) {
				redirect(base_url().$this->lang->lang().'/taller','refresh');
			} else if ($this->acceso('20')) {
				redirect(base_url().$this->lang->lang().'/informes','refresh');
			}
		}
	}
	
	//PAGINA PARA A�ADIR TRABAJOS AL ALMACEN
	public function nuevo_trabajo_taller() {
		
		$this->comprobar_usuario('taller/nuevo_trabajo_taller');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
 
		$url = explode("/", $_SERVER['HTTP_REFERER']);		
		if(end($url) == 'talleres'){
			$breadcrumb = array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('talleres.guardados'), 'href'=>base_url().$this->lang->lang().'/talleres'],['nombre'=>lang('nuevo.trabajo.taller'), 'href'=>'']);
		}else{
			$breadcrumb = array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('taller'), 'href'=>base_url().$this->lang->lang().'/taller'],['nombre'=>lang('nuevo.trabajo.taller'), 'href'=>'']);
		}
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18')) {
			if ($this->session->userdata('emp_crear') == 1) {
				$taller = array('ta_id' => null, 'ta_estado' => 0);
				$datos = array(
				    'TITULO' => lang('nuevo.trabajo.taller'),
				    'STOCKAGE' => 'active',
				   	'TALLER' => 'active',
					'BODY' => $this->load->view('v_taller_nuevo_editar',$taller,true),
					'BREADCRUMB' => $breadcrumb
				);
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/taller','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/taller','refresh');
		}	
	}
	
	//PAGINA PARA EDITAR TRABAJOS DEL TALLER
	public function editar_trabajo_taller($ta_id=null) {
		if ($ta_id != null) {
			$this->comprobar_usuario('taller/editar_trabajo_taller/'.$ta_id);	
		} else {
			$this->comprobar_usuario('taller');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18')) {
			if ($this->session->userdata('emp_editar') == 1) {
				if ($ta_id == null) {
					redirect(base_url().$this->lang->lang().'/taller','refresh');
				} else {
					$this->load->model('M_taller');
					$taller = $this->M_taller->get_taller($ta_id);
					
					$product_id = explode(';',$taller->product_id);
					$product_id = array_filter($product_id);
					$productos = array();
					
					for ($i=0; $i<count($product_id); $i++) {
						$producto = $this->M_taller->get_producto_taller($product_id[$i]);
						
						//A�ADIMOS LAS FOTOS
						//DIRECCION DE LAS FOTOS
						$ruta = PRIVADO.$this->session->userdata("em_cif").'/pro_product/'.$product_id[$i];
						
						$aux = '';
						//RECORRE LA CARPETA
						if (file_exists($ruta)) {
							//CREA UN OBJETO DE CARPETA
							$dirint = dir($ruta);
							
							$primera_vez = true;
							
							while (($archivo = $dirint->read()) !== false) {
								//SI NO ES JPG NO PASA
								if (preg_match("/jpg/", $archivo)){
									//CREA LA IMAGEN						
									$img = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/pro_product/'.$product_id[$i].'/'.$archivo;
									
									$nombre = substr($archivo, 0, count($archivo)-5);
									
									//LISTADO DE IMAGENES
									if ($primera_vez) {
										$aux = $aux.'<a rel="product'.$product_id[$i].'" class="fancybox" href="'.$img.'"><i class="fa fa-eye"></i></a>';
										
										$primera_vez = false;
									} else {
										$aux = $aux.'<a rel="product'.$product_id[$i].'" class="fancybox oculto" href="'.$img.'"><img src="'.$img.'" /></a>';
									}
								}
							}
							
							//CERRAMOS CARPETA
							$dirint->close();
						}
						
						$producto->fotos = $aux;
						
						array_push($productos,$producto);
					}
					$taller->productos = $productos;
					
					//PREGUNTAMOS SE DEVOLVIO RESULTADOS
					if ($taller != null) {
						$url = explode("/", $_SERVER['HTTP_REFERER']);		
						if(end($url) == 'talleres'){
							$breadcrumb = array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('talleres.guardados'), 'href'=>base_url().$this->lang->lang().'/talleres'],['nombre'=>lang('editar.trabajo.taller'), 'href'=>'']);
						}elseif(end($url) == 'taller'){
							$breadcrumb = array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('taller'), 'href'=>base_url().$this->lang->lang().'/taller'],['nombre'=>lang('editar.trabajo.taller'), 'href'=>'']);
						}elseif(end($url) == 'informes'){
							$breadcrumb = array(['nombre'=>lang('informes'), 'href'=>base_url().$this->lang->lang().'/informes'],['nombre'=>lang('editar.trabajo.taller'), 'href'=>'']);
						}
						$datos = array(
					        'TITULO' => lang('editar.trabajo.taller'),
					        'STOCKAGE' => 'active',
					        'TALLER' => 'active',
							'BODY' => $this->load->view('v_taller_nuevo_editar',$taller,true),
							'BREADCRUMB' => $breadcrumb
					    );
						$this->load->view('v_admin',$datos);
					} else {
						redirect(base_url().$this->lang->lang().'/taller','refresh');
					}
				}
			} else {
				redirect(base_url().$this->lang->lang().'/taller','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/taller','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		if ($this->input->post('ta_id') != '') {
			$this->comprobar_usuario('taller/editar_trabajo_taller/'.$this->input->post('ta_id'));	
		} else {
			$this->comprobar_usuario('taller');
		}
		//print_r($this->input->post()); exit();
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('18')) {
			if ($this->session->userdata('emp_tipo') <= 2) {
				if ($this->input->post()) {
					//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
					$response['status'] = false;
					$response['crear'] = true;
					$response['editar'] = true;
					$response['sql'] = true;
					$response['errors'] = '';
					$errors = array();
					$aux = array();
					$entrar = false;
					
					if ($this->input->post('ta_id') != '') {
						if ($this->session->userdata('emp_editar') == 0) {
							$response['editar'] = false;
						}
					} else {
						if ($this->session->userdata('emp_crear') == 0) {
							$response['crear'] = false;
						}
					}
					
					if ($response['crear'] || $response['editar']) {
						$this->load->model('M_taller');
						//print_r($this->input->post()); exit();
						$sql = $this->M_taller->set_trabajo_taller($this->input->post());
						
						//SI ES UN ARRAY HUBO UN ERROR EN EL STOCK
						if (is_array($sql)) {
							$response['stock'] = $sql;
						} else {
							$id = $sql;
							$response['sql'] = true;
							$response['status'] = $response['sql'];
						}	
					}	
					
					foreach ($this->input->post() as $key => $value) {
					        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
					       	array_push($errors,$aux);
						}
					}
					$response['errors'] = array_filter($errors);
					
					//GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'TA'; //Taller
						$datos['asociado'] = $id;
						$datos['accion'] = ($this->input->post('ta_id') != '')?'Editar':'Crear';
						$this->M_bitacora->set_bitacora($datos);
					}

					//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
					echo json_encode($response);
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/taller','refresh');
		}
	}
	
	//TERMINAR Y CERRAR TALLER
	public function cerrar_taller() {
		if ($this->input->post('ta_id') != '') {
			$this->comprobar_usuario('taller/editar_trabajo_taller/'.$this->input->post('ta_id'));	
		} else {
			$this->comprobar_usuario('taller');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		if ($this->acceso('18')) {
			if ($this->session->userdata('emp_tipo') <= 2) {
				if ($this->input->post()) {
					//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
					$response['status'] = false;
					$response['editar'] = true;
					$response['sql'] = true;
					$response['errors'] = '';
					$errors = array();
					$aux = array();
					$entrar = false;
					
					if ($this->input->post('ta_id') != '') {
						if ($this->session->userdata('emp_editar') == 0) {
							$response['editar'] = false;
						}
					}
					
					if ($response['editar']) {
						$this->load->model('M_taller');
						$sql = $this->M_taller->set_cerrar_trabajo_taller($this->input->post());
						$response['status'] = $response['sql'];	
					}	
					
					foreach ($this->input->post() as $key => $value) {
					        if (form_error($key) != '') {
							$aux = array($key,form_error($key));
					       	array_push($errors,$aux);
						}
					}
					$response['errors'] = array_filter($errors);
							
					//GUARDAMOS LA BITACORA
					if ($response['status']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'TA'; //Taller
						$datos['asociado'] = $this->input->post('ta_id');
						$datos['accion'] = 'Cerrar';
						$this->M_bitacora->set_bitacora($datos);
					}

					//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
					echo json_encode($response);
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/taller','refresh');
		}
	}

	public function get_costos_talleres($pr_id=null){
		$this->load->model('M_almacenes');
		$this->load->model('M_presupuestos');
		$presupuesto = $this->M_presupuestos->get_presupuesto($pr_id);
		$costos = $this->M_almacenes->get_costos_pedidos_cotizacion($presupuesto->man_id);
		echo json_encode($costos);		
	}

	public function get_validacion_talleres($pr_id=null){
		$this->load->model('M_almacenes');
		$this->load->model('M_presupuestos');
		$this->load->model('M_proveedores');		
		$this->load->model('M_taller');		
		$this->load->model('M_categorias');	

		$ta_id = empty($this->input->post('ta_id')) ? null : $this->input->post('ta_id');
		$product_list = empty($this->input->post('product_list')) ? null : $this->input->post('product_list');

		$presupuesto = $this->M_presupuestos->get_presupuesto($pr_id);
		$costos = $this->M_almacenes->get_costos_pedidos_cotizacion($presupuesto->man_id, $pr_id, $ta_id);	

		$taller_actual = $this->M_taller->get_taller($ta_id);
		$categoria_autorizadas = json_decode($taller_actual->ta_autorizaciones,TRUE);

		$product_category_list = array();
		$cat_por_aprobar = array();

		$categorias = (array)$this->M_categorias->get_categorias();
		$categorias_final = array();
		foreach ($categorias as $key => $cat) {
			$categorias_final[$cat->cate_id] = $cat->descripcion;
		}

		foreach ($product_list as $key => $producto) {
			$producto_detalle = $this->M_proveedores->get_producto($producto['product_id']);
			$cantidad_pedida = empty($costos['cotizacion'][$pr_id]['productos'][$producto['product_id']]['cantidad_pedida']) ? 0 : (float)$costos['cotizacion'][$pr_id]['productos'][$producto['product_id']]['cantidad_pedida'];

			if($cantidad_pedida < $producto['cantidad']){
				$costo = ($producto['cantidad'] - $cantidad_pedida) * $producto_detalle->product_precio;
			}else{
				$costo = 0;
			}

			if(empty($product_category_list[$producto_detalle->cate_id]['costo'])){
				$product_category_list[$producto_detalle->cate_id]['costo'] = $costo;
			}else{
				$product_category_list[$producto_detalle->cate_id]['costo'] += $costo;				
			}
			$product_category_list[$producto_detalle->cate_id]['categoria'] = $producto_detalle->cate_id;				
		}
		
		$aplica_validacion = FALSE;

		foreach ($product_category_list as $key => $categoria) {
			$nuevo_usado = $categoria['costo'];

			if(!empty($costos['cotizacion']['']['presupuesto'][$categoria['categoria']])){
				$usado = $costos['cotizacion']['']['categorias'][$categoria['categoria']]['usado'];
				$presupuesto = $costos['cotizacion']['']['presupuesto'][$categoria['categoria']];
				$disponible = $presupuesto - $usado;
				$aprobado = $usado;
				$costo = $nuevo_usado - ($presupuesto - $usado);
			}else{
				$presupuesto = $costos['cotizacion'][$pr_id]['presupuesto'][$categoria['categoria']];
				$pedido = $costos['cotizacion'][$pr_id]['categorias'][$categoria['categoria']]['pedido'];
				$usado = $costos['cotizacion'][$pr_id]['categorias'][$categoria['categoria']]['usado'];

				if($pedido > $presupuesto){
					$costo = $nuevo_usado - ($pedido - $usado);
					$disponible = $pedido - $usado;
					$aprobado = $pedido;
				}else{
					$costo = $nuevo_usado - ($presupuesto - $usado);
					$disponible = $presupuesto - $usado;
					$aprobado = $presupuesto;
				}							
			}

			if($nuevo_usado > $disponible){
				if(array_key_exists($categoria['categoria'],$categoria_autorizadas)){
					if($categoria_autorizadas[$categoria['categoria']]['costo'] != $costo){
						$aplica_validacion = TRUE;
						$cat_por_aprobar[$categoria['categoria']] = array(
							'categoria' => $categoria['categoria'],
							'nombre_categoria' => $categorias_final[$categoria['categoria']],
							'aprobado' => $aprobado,
							'costo' => $costo,
							'disponible' => $disponible,
							'nuevo_usado' => $nuevo_usado,
						);
					}
				}else{					
					$aplica_validacion = TRUE;
					$cat_por_aprobar[$categoria['categoria']] = array(
						'categoria' => $categoria['categoria'],
						'nombre_categoria' => $categorias_final[$categoria['categoria']],
						'aprobado' => $aprobado,
						'costo' => $costo,
						'disponible' => $disponible,
						'nuevo_usado' => $nuevo_usado,
					);
				}
			}
		}

		echo json_encode(array(
			'aplica_validacion' => $aplica_validacion,
			'cat_por_aprobar' => $cat_por_aprobar,
		));		
	}

	public function get_autorizaciones_taller($ta_id=null){

		if(!empty($ta_id)){			
			$this->load->model('M_taller');	
			$this->load->model('M_categorias');	
			$taller_actual = $this->M_taller->get_taller($ta_id);
			$categoria_autorizadas = json_decode($taller_actual->ta_autorizaciones,TRUE);

			$categorias = (array)$this->M_categorias->get_categorias();
			$categorias_final = array();
			foreach ($categorias as $key => $cat) {
				$categorias_final[$cat->cate_id] = $cat->descripcion;
			}
			
			foreach ($categoria_autorizadas as $key => $categoria) {
				$categoria_autorizadas[$key]['nombre_categoria'] = $categorias_final[$key];
			}
		}else{
			$categoria_autorizadas = array();
		}

		echo json_encode(array(
			'categoria_autorizadas' => $categoria_autorizadas,
		));
	}
}

/* End of file taller.php */
/* Location: ./application/controllers/taller.php */