<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends MY_Controller {
	function __construct() {
		parent::__construct();
	}
	
	//PAGINA PARA EDITAR EMPRESA
	public function editar_empresa() {
		//GUARDAMOS LA URL ACTUAL
		$url = $_SERVER['REQUEST_URI'];
		$this->comprobar_usuario('empresa/editar_empresa');	
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//COMPROBAMOS QUE SEA SUPER USUARIO
		if ($this->session->userdata('emp_tipo') == 0) {
			$this->load->model('M_empresa');
			$empresa = $this->M_empresa->get_empresa($this->session->userdata('em_id'));
			
			if ($empresa != null) {
				$datos = array(
					'TITULO' => lang('editar.empresa'),
					'BODY' => $this->load->view('v_empresa_editar',$empresa,true)
				   );
				$this->load->view('v_admin',$datos);
			}
		}
	}
	
	//GUARDAMOS O EDITAMOS EL CONTENIDO
	public function guardar_editar() {
		$this->comprobar_usuario('empresa/editar_empresa/'.$this->session->userdata('em_id'));	
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//COMPROBAMOS QUE SEA SUPER USUARIO
		if ($this->session->userdata('emp_tipo') == 0) {
			if ($this->input->post()) {
				//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
				$response['status'] = false;
				$errors = array();
				$aux = array();
				
				//VALIDAMOS LOS CAMPOS
				$this->form_validation->set_rules('em_nombre',lang('nombre'),'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('em_telefono',lang('telefono'),'trim|required|max_length[15]|xss_clean');
				$this->form_validation->set_rules('em_email',lang('email'),'trim|required|valid_email|max_length[100]|xss_clean');
				$this->form_validation->set_rules('em_email_contabilidad',lang('email.contabilidad'),'trim|required|valid_email|max_length[100]|xss_clean');
				$this->form_validation->set_rules('em_email_opcional',lang('email.opcional'),'trim|valid_email|max_length[100]|xss_clean');
				$this->form_validation->set_rules('em_email_opcional2',lang('email.opcional'),'trim|valid_email|max_length[100]|xss_clean');
				$this->form_validation->set_rules('em_direccion',lang('direccion'),'trim|required|max_length[150]|xss_clean');
				$this->form_validation->set_rules('em_provincia',lang('provincia'),'trim|required|max_length[50]|xss_clean');
				$this->form_validation->set_rules('em_postal',lang('postal'),'trim|max_length[11]|xss_clean');
				$this->form_validation->set_rules('em_barriada',lang('barriada'),'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('em_localidad',lang('localidad'),'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('em_pais',lang('pais'),'trim|required|max_length[50]|xss_clean');
				$this->form_validation->set_rules('foto','error','trim|xss_clean');
			}
			
			if ($this->form_validation->run()) {					
				if (!empty($_FILES['foto'])) {
					$name2 = $_FILES['foto']['name']; //get the name of the image
					list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
							
					if ($ext == 'jpg') {
						if ($_FILES['foto']['size'] <= 500000) {
							$response['status'] = true;
						} else {
							$f = array('foto',lang('max.size'));
							array_push($errors,$f);
						}
					} else {
						$f = array('foto',lang('jpg'));
						array_push($errors,$f);
					}	
				} else {
					$response['status'] = true;
				}
			}
			$response['status'] = true;
			if ($response['status'] == true) {
				$this->load->model('M_empresa');
				$this->M_empresa->set_empresa($this->input->post());
			} else {
				foreach ($this->input->post() as $key => $value) {
			           if (form_error($key) != '') {
						$aux = array($key,form_error($key));
			           	array_push($errors,$aux);
					}
			       }
			       $response['errors'] = array_filter($errors);
			}

			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		}
	}
	
	function geo_test() {
		$this->load->library('geoip_lib');
		print_r($this->geoip_lib->result_array());
	}
}

/* End of file empresa.php */
/* Location: ./application/controllers/empresa.php */