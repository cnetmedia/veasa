<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Presupuestos extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE PRESUPUESTOS
	public function index() {
		$this->comprobar_usuario('presupuestos');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3')) {
			$acceso = $this->load->view('v_presupuestos','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('presupuestos'),
	        'FACTURACION' => 'active',
	        'PRESUPUESTOS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('presupuestos'), 'href'=>''])	
	    );
		$this->load->view('v_admin',$datos);		
	}
	
	//BUSCA PRESUPUESTOS PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('presupuestos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3') || $this->acceso('15') || $this->acceso('16') || $this->acceso('18') || $this->acceso('10')) {
			if ($this->input->post()) {
				$this->load->model('M_presupuestos');

				$params = $this->input->post();

				if(!empty($params['soloAnuladas']) && $params['soloAnuladas'] == 'true'){
					$array = $array = $this->M_presupuestos->get_presupuestos_anulados();					
				}else{					
					$array = $this->M_presupuestos->get_presupuestos();
				}
				$aux = array();
				
				/*$aux = array();
				
				for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}*/
				
				echo json_encode($array);
			}
		} else if ($this->acceso('13')) {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		} else if ($this->acceso('15')) {
			redirect(base_url().$this->lang->lang().'/proveedores/carrito','refresh');
		} else if ($this->acceso('16')) {
			redirect(base_url().$this->lang->lang().'/almacenes/entradas','refresh');
		} else if ($this->acceso('18')) {
			redirect(base_url().$this->lang->lang().'/taller','refresh');
		} else if ($this->acceso('10')) {
			redirect(base_url().$this->lang->lang().'/trabajos','refresh');
		}
	}
	
	//PAGINA PARA EDITAR PRESUPUESTOS
	public function editar_presupuesto($pr_id=null) {
		if ($pr_id != null) {
			$this->comprobar_usuario('presupuestos/editar_presupuesto/'.$pr_id);	
		} else {
			$this->comprobar_usuario('presupuestos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3')) {
			if ($pr_id == null) {
				redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
			} else {
				$this->load->model('M_presupuestos');
				$presupuesto = $this->M_presupuestos->get_presupuesto($pr_id);
				//PREGUNTAMOS SI DEVOLVIO RESULTADOS
				if ($presupuesto != null) {
					$url = explode("/", $_SERVER['HTTP_REFERER']);
					if($url[count($url)-2] == 'editar_factura'){
						$breadcrumb = array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('facturas'), 'href'=>base_url().$this->lang->lang().'/facturas'],['nombre'=>lang('editar.factura'), 'href'=>base_url().$this->lang->lang().'/facturas/editar_factura/'.end($url)],['nombre'=>lang('editar.presupuesto'), 'href'=>'']);
					}elseif(end($url) == 'informes'){
						$breadcrumb = array(['nombre'=>lang('informes'), 'href'=>base_url().$this->lang->lang().'/informes'],['nombre'=>lang('editar.presupuesto'), 'href'=>'']);
					}else{
						$breadcrumb = array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('presupuestos'), 'href'=>base_url().$this->lang->lang().'/presupuestos'],['nombre'=>lang('editar.presupuesto'), 'href'=>'']);
					}
					$datos = array(
				        'TITULO' => lang('editar.presupuesto'),
				        'FACTURACION' => 'active',
				        'PRESUPUESTOS' => 'active',
						'BODY' => $this->load->view('v_presupuestos_nuevo_editar',$presupuesto,true),
						'BREADCRUMB' => $breadcrumb
				    );
					$this->load->view('v_admin',$datos);
				} else {
					redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}
	}
	
	//PAGINA PARA A�ADIR PRESUPUESTOS
	public function nuevo_presupuesto() {
		$this->comprobar_usuario('presupuestos/nuevo_presupuesto');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3')) {
			if ($this->session->userdata('emp_crear') == 1) {
				//OPCIONES POR DEFECTO PARA NUEVO EMPLEADO
				$presupuesto = array(
					'pr_id' => null,
					'pr_estado' => '0',
					'su_iva' => '0',
					'su_iva_reducido' => '0',
					'su_iva_superreducido' => '0',
					'pr_iva' => '',
					'su_id' => ''
				);
				$datos = array(
			        'TITULO' => lang('nuevo.presupuesto'),
			        'FACTURACION' => 'active',
			        'PRESUPUESTOS' => 'active',
					'BODY' => $this->load->view('v_presupuestos_nuevo_editar',$presupuesto,true),
					'BREADCRUMB' => array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('presupuestos'), 'href'=>base_url().$this->lang->lang().'/presupuestos'],['nombre'=>lang('nuevo.presupuesto'), 'href'=>''])
			    );
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}	
	}
	
	private function get_pdf($pr_id,$tipo,$datos=null) {
        $this->load->model('M_presupuestos');
        $presupuesto = $this->M_presupuestos->get_presupuesto($pr_id);
        $this->load->library('pdf');
  
		$this->pdf = new Pdf();
		// Define el alias para el n�mero de p�gina que se imprimir� en el pie
		$this->pdf->AliasNbPages();
		// Agregamos una p�gina
		$this->pdf->AddPage();
		$this->pdf->SetAuthor(lang('aplicacion'), true);
		$this->pdf->SetSubject(lang('presupuesto'), true);
		$this->pdf->SetTitle(lang('presupuesto').': '.$presupuesto->pr_numero, true);
		
		//CONFIGURACION GLOBAL
		$this->pdf->SetDrawColor(170,170,170); //Color lineas y bordes
		$this->pdf->SetTextColor(0, 0, 0); //Color texto
		$this->pdf->SetFillColor(246,246,246); //Color fondo celdas
		//$this->pdf->SetAutoPageBreak(true,30);
		
		//LOGO
		$this->pdf->Image(PRIVADO.$this->session->userdata('em_cif')."/logo.jpg",null,null,0,0,"JPG");
		
		//CONTACTO EMPRESA
		$this->pdf->SetFont('Arial','',10);
		$this->pdf->SetXY(9,40);
		
		if ($presupuesto->em_postal != '') {
			$em_postal = $presupuesto->em_postal.', ';
		} else {
			$em_psotal = '';
		}
		
		$this->pdf->MultiCell(85,5,utf8_decode($presupuesto->em_nombre)."\n".$presupuesto->em_cif."\n".utf8_decode($presupuesto->em_direccion)."\n".utf8_decode($em_postal).utf8_decode($presupuesto->em_localidad).", ".utf8_decode($presupuesto->em_provincia).", ".utf8_decode($presupuesto->name)."\n".utf8_decode(lang('telefono')).": ".$presupuesto->em_telefono."\n".lang('email').": ".$presupuesto->em_email,1,"L",false);
		
		//CONTACTO CLIENTE
		$this->pdf->SetXY(113,40);
		
		if ($presupuesto->cl_postal != '') {
			$cl_postal = $presupuesto->cl_postal.', ';
		} else {
			$cl_postal = '';
		}
		
		$this->pdf->MultiCell(85,5,utf8_decode($presupuesto->cl_nombre)."\n".$presupuesto->cl_dni."\n".utf8_decode($presupuesto->cl_direccion)."\n".utf8_decode($cl_postal).utf8_decode($presupuesto->cl_localidad).", ".utf8_decode($presupuesto->cl_provincia).", ".utf8_decode($presupuesto->name)."\n".utf8_decode(lang('telefono')).": ".$presupuesto->cl_telefono."\n".lang('email').": ".$presupuesto->cl_email,1,"L",false);
		
		//FACTURA
		$this->pdf->SetFont('Arial','',10);
		$this->pdf->SetXY(9,80);
		$this->pdf->Cell(100,6,utf8_decode(lang('presupuesto')).': '.$presupuesto->pr_numero,0,0,"L",false);
		
		//FECHA
		$this->pdf->SetXY(9,86);
		$this->pdf->Cell(100,6,lang('fecha').': '.$presupuesto->pr_fecha,0,0,"L",false);
		
		//SERVICIOS
		$this->pdf->SetXY(9,100); 
		$this->pdf->Cell(77,7,utf8_decode(lang('descripcion')),1,0,"L",true);
		
		//COSTE UNIDAD
		$this->pdf->SetXY(86,100);
		$symbol = ' / '.iconv('UTF-8', 'windows-1252', $presupuesto->currrency_symbol);
		$this->pdf->Cell(30,7,lang('und').$symbol,1,0,"R",true);
		
		//CANTIDAD
		$this->pdf->SetXY(116,100); 
		$this->pdf->Cell(22,7,lang('cantidad'),1,0,"R",true);
		
		//DESCUENTO
		$this->pdf->SetXY(138,100); 
		$this->pdf->Cell(25,7,lang('desc').' / % ',1,0,"R",true);
		
		//Precio
		$this->pdf->SetXY(163,100);
		$this->pdf->Cell(35,7,lang('precio').$symbol,1,0,"R",true);
		
		$servicios = explode(";", $presupuesto->pr_servicio);
		$cantidades = explode(";", $presupuesto->pr_cantidad);
		$precios = explode(";", $presupuesto->pr_precio);
		$descuentos = explode(";", $presupuesto->pr_descuento);
		
		$subtotal = 0;
		$descuento_total = 0;
		
		$y = 110;
		$y2 = 0;
		$alto_celdas = 0;
		
		for ($i = 0; $i < count($servicios)-1; $i++) {
			//calculamos el coste: precio unidad por cantidad
			$coste_total = (float)$precios[$i]*(int)$cantidades[$i];
			//calculamos el descuento si lo tienes del coste
			$coste_descuento = ($coste_total*$descuentos[$i])/100;
			//suma total sin iva de los servicios
			$subtotal = $subtotal + $coste_total;
			//suma total del descuento
			$descuento_total = $descuento_total + $coste_descuento;
			
			//RESULT SERVICIOS
			$this->pdf->SetXY(9,$y); 
			$this->pdf->MultiCell(77,5,$servicios[$i],'B',"L",false);
			$y2 = $this->pdf->GetY();
			$alto_celdas = $y2-$y;
			
			//RESULT COSTE UNIDAD
			$this->pdf->SetXY(86,$y); 
			$this->pdf->Cell(30,$alto_celdas,round($precios[$i], 2),'B',0,"R",false);
			
			//RESULT CANTIDAD
			$this->pdf->SetXY(116,$y); 
			$this->pdf->Cell(22,$alto_celdas,$cantidades[$i],'B',0,"R",false);
			
			//RESULT DESCUENTO
			$this->pdf->SetXY(138,$y); 
			$this->pdf->Cell(25,$alto_celdas,$descuentos[$i],'B',0,"R",false);
			
			//RESULT PRECIO
			$this->pdf->SetXY(163,$y); 
			$this->pdf->Cell(35,$alto_celdas,round($coste_total, 2),'B',0,"R",false);
			
			if ($y2 >= 245) {
				$this->pdf->AddPage();
				$y2 = 10;
			}
			
			$y = $y2 + 2;
		}
		
		//SUBTOTAL
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('subtotal'),0,0,"R",false);
		
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,round($subtotal, 2),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//DESCUENTO
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('descuento'),0,0,"R",false);
		
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,round($descuento_total, 2),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//BASE IMPONIBLE
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('base.imponible'),0,0,"R",false);
		
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,round($subtotal - $descuento_total, 2),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//IMPUESTOS
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,'('.$presupuesto->pr_iva.' %) '.lang('impuestos'),0,0,"R",false);
		
		$impuestos = (($subtotal-$descuento_total)*$presupuesto->pr_iva)/100;
		
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,round($impuestos, 2),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//TOTAL
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('total'),0,0,"R",false);
		
		$this->pdf->SetXY(163,$y);
		$this->pdf->SetFont('Arial','B',10);
		$this->pdf->Cell(35,7,round(($subtotal-$descuento_total) + $impuestos, 2),0,0,"R",false);
		$this->pdf->SetFont('Arial','',10);
		
		$y = $this->pdf->GetY()+20;
		
		//EMPLEADO
		$this->pdf->SetXY(118,$y);
		$this->pdf->Cell(80,7,utf8_decode(lang('presupuesto.emitido.por')),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//NOMBRE EMPLEADO
		$empleado = utf8_decode($presupuesto->emp_nombre.' '.$presupuesto->emp_apellido1);
		
		if ($presupuesto->emp_apellido2 != '') {
			$empleado = $empleado.' '.utf8_decode($presupuesto->emp_apellido2);
		}
		
		$this->pdf->SetXY(78,$y);
		$this->pdf->Cell(120,7,$empleado,0,0,"R",false);
		$this->pdf->SetFont('Arial','',10);
		
		$y = $this->pdf->GetY();



		if ($tipo == 'email') {
			//GENERAMOS EL PDF EN UNA CARPETA GENERICA PARA TODOS LOS PDF
			$this->pdf->Output(PRIVADO.$presupuesto->em_cif.'/pdfs/'.$presupuesto->pr_numero.'.pdf','F');
			
			$this->load->library('email');
			$nombre = "=?UTF-8?B?".base64_encode($presupuesto->em_nombre)."=?=";
			$this->email->from(EMAIL, $nombre);
			$this->email->subject(lang('presupuesto').': '.$presupuesto->pr_numero);
			//PLANTILLA EMAIL	
			$data = array(
				'BODY' => $this->load->view('emails/v_email_presupuesto','', TRUE)
			);
			$email = $this->load->view('emails/v_email', $data, TRUE);

			$this->email->message($email);
			//ADJUNTAMOS EL PDF
			$this->email->attach(PRIVADO.$presupuesto->em_cif.'/pdfs/'.$presupuesto->pr_numero.'.pdf');
			//STRING PARA LOS EMAILS
			$para = '';
			$copia = '';
			$oculta = '';
			
			//GENERAMOS LOS STRING DE EMAILS
			for ($x=0;$x<count($datos);$x++) {
				if ($datos[$x]['value'] == 1) {
					$para = $para.$datos[$x]['email'].',';
				} else if ($datos[$x]['value'] == 2) {
					$copia = $copia.$datos[$x]['email'].',';
				} else if ($datos[$x]['value'] == 3) {
					$oculta = $oculta.$datos[$x]['email'].',';
				}
			}
			
			//QUITAMOS EL ULTIMO CARACTER QUE ES LA COMA
			$para = trim($para, ',');
			$copia = trim($copia, ',');
			$oculta = trim($oculta, ',');
			
			//CREAMOS LAS CABECERAS PARA ENVIAR
			$this->email->to($para);
			$this->email->cc($copia);
			$this->email->bcc($oculta);
			
			//ENVIAMOS EL EMAIL  	
			$this->email->send();
			//BORRAMOS EL PDF CREADO EN LA CARPETA GENERICA			
			unlink(PRIVADO.$presupuesto->em_cif.'/pdfs/'.$presupuesto->pr_numero.'.pdf');
		} else {
			//GENERAMOS EL PDF
			$this->pdf->Output($presupuesto->pr_numero.'.pdf','D');
		}  
    }
    
    public function enviar_email() {
		if ($this->input->post('pr_id') != '') {
			$this->comprobar_usuario('presupuestos/editar_presupuesto/'.$this->input->post('pr_id'));	
		} else {
			$this->comprobar_usuario('presupuestos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3')) {
			if ($this->session->userdata('emp_editar') == 1) {
				$this->get_pdf($this->input->post('pr_id'),'email',$this->input->post('datos'));
			} else {
				$this->comprobar_usuario('presupuestos/editar_presupuesto/'.$this->input->post('pr_id'));
			}
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}
	}
	
	public function generar_pdf($pr_id=null) {
		if ($pr_id != '') {
			$this->comprobar_usuario('presupuestos/generar_pdf/'.$pr_id);	
		} else {
			$this->comprobar_usuario('presupuestos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->acceso('3')) {
			if ($this->session->userdata('emp_editar') == 1) {
				$this->get_pdf($pr_id,'pdf');
			} else {
				$this->comprobar_usuario('presupuestos/editar_presupuesto/'.$pr_id);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS EL PRESUPUESTO
	public function guardar_editar() {
		if ($this->input->post('pr_id') != '') {
			$url = 'presupuestos/editar_presupuesto/'.$this->input->post('pr_id');	
		} else {
			$url = 'presupuestos/nuevo_presupuesto';
		}
		$this->comprobar_usuario($url);
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('pr_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					$this->load->model('M_presupuestos');
					$response['sql'] = $this->M_presupuestos->set_presupuesto($this->input->post());
					$response['status'] = $response['sql'];
					
					if($response['sql'] != false){
						$id = $response['sql'];
						$response['sql'] = true;
						$response['status'] = $response['sql'];
					}

				}
			}
			
			//GUARDAMOS LA BITACORA
			if ($response['status']) {
				$this->load->model('M_bitacora');
				$datos = array();
				$datos['tipo'] = 'CO'; //Cotizacion
				$datos['asociado'] = $id;
				$datos['accion'] = ($this->input->post('pr_id') != '')?'Editar':'Crear';
				$this->M_bitacora->set_bitacora($datos);
			}
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}
	}
	
	//GUARDAMOS LAS MODIFICACIONES DE LA FACTURA Y LA CERRAMOS
	public function cerrar() {
		if ($this->input->post('pr_id') != '') {
			$this->comprobar_usuario('presupuestos/editar_presupuesto/'.$this->input->post('pr_id'));
		} else {
			$this->comprobar_usuario('presupuestos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->acceso('3')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->session->userdata('emp_editar') == 0) {
				$response['editar'] = false;
			} else {
				$entrar = true;
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					$this->load->model('M_presupuestos');
					$response['sql'] = $this->M_presupuestos->set_cerrar_presupuesto($this->input->post());	
					$response['status'] = $response['sql'];
				}
			}
			
			//GUARDAMOS LA BITACORA
			if ($response['status']) {
				$this->load->model('M_bitacora');
				$datos = array();
				$datos['tipo'] = 'CO'; //Cotizacion
				$datos['asociado'] = $this->input->post('pr_id');
				$datos['accion'] = 'Aprobar';
				$this->M_bitacora->set_bitacora($datos);
			}

			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
	
	//ANULAMOS UN PRESUPUESTO YA APROBADO
	public function anular() {
		if ($this->input->post('pr_id') != '') {
			$this->comprobar_usuario('presupuestos/editar_presupuesto/'.$this->input->post('pr_id'));
		} else {
			$this->comprobar_usuario('presupuestos');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->acceso('3')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->session->userdata('emp_editar') == 0) {
				$response['editar'] = false;
			} else {
				$entrar = true;
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					$this->load->model('M_presupuestos');
					$response['sql'] = $this->M_presupuestos->set_anular_presupuesto($this->input->post());	
					$response['status'] = $response['sql'];
				}
			}

			//GUARDAMOS LA BITACORA
			if ($response['status']) {
				$this->load->model('M_bitacora');
				$datos = array();
				$datos['tipo'] = 'CO'; //Cotizacion
				$datos['asociado'] = $this->input->post('pr_id');
				$datos['accion'] = 'Anular';
				$this->M_bitacora->set_bitacora($datos);
			}

			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}
	}
}

/* End of file presupuestos.php */
/* Location: ./application/controllers/presupuestos.php */