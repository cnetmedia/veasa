<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facturas extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE FACTURAS
	public function index() {
		$this->comprobar_usuario('facturas');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('1')) {
			$acceso = $this->load->view('v_facturas','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('facturas'),
	        'FACTURACION' => 'active',
	        'FACTURAS' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('facturas'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);		
	}
	
	//BUSCA FACTURAS PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('facturas');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('1')) {
			if ($this->input->post()) {
				$this->load->model('M_facturas');

				$params = $this->input->post();

				//var_dump($params);

				if(!empty($params['soloAnuladas']) && $params['soloAnuladas'] == 'true'){
					$array = $this->M_facturas->get_facturas_anuladas();					
				}else{					
					$array = $this->M_facturas->get_facturas();
				}
				$aux = array();
				
				/*for ($i=0; $i<count($array); $i++) {
					$mostrar = false;
					
					if ($this->session->userdata('emp_tipo') == 0) {
						$mostrar = true;
					} else if ($this->session->userdata('su_id') == $array[$i]->su_id) {
						$mostrar = true;
					}
					
					if ($mostrar) {
						array_push($aux, $array[$i]);
					}
				}*/
				
				echo json_encode($array);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
	
	//BUSCA LA FACTURA SEGUN SU PRESUPUESTO
	public function factura_presupuesto() {
		$this->comprobar_usuario('presupuestos');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('3')) {
			if ($this->input->post()) {
				$this->load->model('M_facturas');
				echo json_encode($this->M_facturas->get_factura_presupuesto($this->input->post('pr_id')));
			}
		} else {
			redirect(base_url().$this->lang->lang().'/presupuestos','refresh');
		}
	}
	
	//PAGINA PARA EDITAR FACTURAS
	public function editar_factura($fa_id=null) {
		if ($fa_id != null) {
			$this->comprobar_usuario('facturas/editar_factura/'.$fa_id);	
		} else {
			$this->comprobar_usuario('facturas');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('1')) {
			if ($fa_id == null) {
				redirect(base_url().$this->lang->lang().'/facturas','refresh');
			} else {
				$this->load->model('M_facturas');
				//COMPROBAMOS SI EXISTE FACTURA RECTIFICATIVA
				$rectificativa = $this->M_facturas->get_rectificativa($fa_id);
				//PREGUNTAMOS SI ENCONTRO FACTURA RECTIFICATIVA
				if ($rectificativa != null) {
					$url = explode("/", $_SERVER['HTTP_REFERER']);
					if($url[count($url)-2] == 'editar_presupuesto'){
						$breadcrumb = array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('presupuestos'), 'href'=>base_url().$this->lang->lang().'/presupuestos'],['nombre'=>lang('editar.presupuesto'), 'href'=>base_url().$this->lang->lang().'/presupuestos/editar_presupuesto/'.end($url)],['nombre'=>lang('editar.factura.rectificativa'), 'href'=>'']);
					}elseif(end($url) == 'informes'){
						$breadcrumb = array(['nombre'=>lang('informes'), 'href'=>base_url().$this->lang->lang().'/informes'],['nombre'=>lang('editar.factura.rectificativa'), 'href'=>'']);
					}else{
						$breadcrumb = array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('facturas'), 'href'=>base_url().$this->lang->lang().'/facturas'],['nombre'=>lang('editar.factura.rectificativa'), 'href'=>'']);
					}
					$datos = array(
					    'TITULO' => lang('editar.factura.rectificativa'),
					    'FACTURACION' => 'active',
					    'FACTURAS' => 'active',
						'BODY' => $this->load->view('v_facturas_rectificativas_nuevo_editar',$rectificativa,true),
						'BREADCRUMB' => $breadcrumb
				   );
					$this->load->view('v_admin',$datos);
				} else {
					//NO ENCONTRO FACTURA RECTIFICATVA POR LO TANTO MUESTRO LA FACTURA NORMAL
					$factura = $this->M_facturas->get_factura($fa_id);
					//PREGUNTAMOS SE DEVOLVIO RESULTADOS
					if ($factura != null) {
						$url = explode("/", $_SERVER['HTTP_REFERER']);
						if($url[count($url)-2] == 'editar_presupuesto'){
							$breadcrumb = array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('presupuestos'), 'href'=>base_url().$this->lang->lang().'/presupuestos'],['nombre'=>lang('editar.presupuesto'), 'href'=>base_url().$this->lang->lang().'/presupuestos/editar_presupuesto/'.end($url)],['nombre'=>lang('editar.factura'), 'href'=>'']);
						}elseif(end($url) == 'informes'){
							$breadcrumb = array(['nombre'=>lang('informes'), 'href'=>base_url().$this->lang->lang().'/informes'],['nombre'=>lang('editar.factura'), 'href'=>'']);
						}else{
							$breadcrumb = array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('facturas'), 'href'=>base_url().$this->lang->lang().'/facturas'],['nombre'=>lang('editar.factura'), 'href'=>'']);
						}
						$datos = array(
					        'TITULO' => lang('editar.factura'),
					        'FACTURACION' => 'active',
					        'FACTURAS' => 'active',
							'BODY' => $this->load->view('v_facturas_nuevo_editar',$factura,true),
							'BREADCRUMB' => $breadcrumb
					    );
						$this->load->view('v_admin',$datos);
					} else {
						redirect(base_url().$this->lang->lang().'/facturas','refresh');
					}
				}
			}
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
	
	//PAGINA PARA AADIR FACTURAS
	public function nueva_factura() {
		$this->comprobar_usuario('facturas/nueva_factura');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('1')) {
			if ($this->session->userdata('emp_crear') == 1) {
				//OPCIONES POR DEFECTO PARA NUEVO EMPLEADO
				$factura = array(
					'fa_id' => null,
					'fa_estado' => '0',
					'su_iva' => '0',
					'su_iva_reducido' => '0',
					'su_iva_superreducido' => '0',
					'fa_iva' => '',
					'pr_id' => '',
					'fa_numero' => '',
					'su_id' => ''
				);
				$datos = array(
			        'TITULO' => lang('nueva.factura'),
			        'FACTURACION' => 'active',
			        'FACTURAS' => 'active',
					'BODY' => $this->load->view('v_facturas_nuevo_editar',$factura,true),
					'BREADCRUMB' => array(['nombre'=>lang('facturacion'), 'href'=>'#'],['nombre'=>lang('facturas'), 'href'=>base_url().$this->lang->lang().'/facturas'],['nombre'=>lang('nueva.factura'), 'href'=>''])
			    );
				$this->load->view('v_admin',$datos);
			} else {
				redirect(base_url().$this->lang->lang().'/facturas','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}	
	}
	
	private function get_pdf($fa_id,$envio,$tipo,$datos=null) {
        $this->load->model('M_facturas');
        if ($tipo == 'F') {
			$factura = $this->M_facturas->get_factura($fa_id);
		} else {
			$factura = $this->M_facturas->get_rectificativa($fa_id);
		}
        
        $this->load->library('pdf');
  
		$this->pdf = new Pdf();
		// Define el alias para el n�mero de p�gina que se imprimir� en el pie
		$this->pdf->AliasNbPages();
		// Agregamos una p�gina
		$this->pdf->AddPage();
		$this->pdf->SetAuthor(lang('aplicacion'), true);
		
		if ($tipo == 'F') {
			$this->pdf->SetSubject(lang('factura'), true);
		} else {
			$this->pdf->SetSubject(lang('factura.rectificativa'), true);
		}
		
		//CONFIGURACION GLOBAL
		$this->pdf->SetDrawColor(170,170,170); //Color lineas y bordes
		$this->pdf->SetTextColor(0, 0, 0); //Color texto
		$this->pdf->SetFillColor(246,246,246); //Color fondo celdas
		//$this->pdf->SetAutoPageBreak(true,30);
		
		//LOGO
		$this->pdf->Image(PRIVADO.$this->session->userdata('em_cif')."/logo.jpg",null,null,0,0,"JPG");
		
		//CONTACTO EMPRESA
		$this->pdf->SetFont('Arial','',10);
		$this->pdf->SetXY(9,40);
		$this->pdf->MultiCell(85,5,utf8_decode($factura->em_nombre)."\n".$factura->em_cif."\n".utf8_decode($factura->em_direccion)."\n".$factura->em_postal.", ".utf8_decode($factura->em_localidad).", ".utf8_decode($factura->em_provincia).", ".utf8_decode($factura->em_pais)."\n".utf8_decode(lang('telefono')).": ".$factura->em_telefono."\n".lang('email').": ".$factura->em_email,1,"L",false);
		
		//CONTACTO CLIENTE
		$this->pdf->SetXY(113,40);
		$this->pdf->MultiCell(85,5,utf8_decode($factura->cl_nombre)."\n".$factura->cl_dni."\n".utf8_decode($factura->cl_direccion)."\n".$factura->cl_postal.", ".utf8_decode($factura->cl_localidad).", ".utf8_decode($factura->cl_provincia).", ".utf8_decode($factura->name)."\n".utf8_decode(lang('telefono')).": ".$factura->cl_telefono."\n".lang('email').": ".$factura->cl_email,1,"L",false);
		
		//FACTURA
		$this->pdf->SetFont('Arial','',10);
		$this->pdf->SetXY(9,80); 
		
		//COMPROBAMOS QUE LA FACTURA ESTE CERRADA O NO
		//MOSTRAMOS SU NUMERO SI ESTA, SI NO PONEMOS PENDIENTE
		//SI ESTA CERRADA EL ARCHIVO TENDRA SU NUMERO, SI NO SERA UN NOMBRE PENDIENTE
		if ($factura->fa_numero != '') {
			$num_fact = $factura->fa_numero;
			$nomb_archivo = $factura->fa_numero;
		} else {
			$num_fact = lang('pendiente');
			$nomb_archivo = lang('factura').'_'.lang('pendiente').'_'.$factura->cl_dni;
		}
		
		$this->pdf->Cell(100,6,lang('factura').': '.$num_fact,0,0,"L",false);
		$this->pdf->SetXY(9,86);
		$this->pdf->Cell(100,6,lang('fecha.factura').': '.$factura->fa_fecha,0,0,"L",false);
		$y = 100;
		
		//FACTURA RECTIFICATIVA Y FECHA RECTIFICATIVA
		if ($tipo == 'R') {
			$this->pdf->SetXY(9,92);
			$this->pdf->Cell(100,6,lang('factura.rectificativa').': '.$factura->re_numero,0,0,"L",false);
			$this->pdf->SetXY(9,98);
			$this->pdf->Cell(100,6,lang('fecha.factura.rectificativa').': '.$factura->re_fecha,0,0,"L",false);
			$y = 110;
		}
		
		//SERVICIOS
		$this->pdf->SetXY(9,$y); 
		$this->pdf->Cell(77,7,utf8_decode(lang('descripcion')),1,0,"L",true);
		
		//COSTE UNIDAD
		$this->pdf->SetXY(86,$y); 
		$symbol = ' / '.iconv('UTF-8', 'windows-1252', $factura->currrency_symbol);
		$this->pdf->Cell(30,7,lang('und').$symbol,1,0,"R",true);
		
		//CANTIDAD
		$this->pdf->SetXY(116,$y); 
		$this->pdf->Cell(22,7,lang('cantidad'),1,0,"R",true);
		
		//DESCUENTO
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('desc').' / %',1,0,"R",true);
		
		//Precio
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,lang('precio').$symbol,1,0,"R",true);
		
		if ($tipo == 'F') {
			$servicios = explode(";", $factura->fa_servicio);
			$cantidades = explode(";", $factura->fa_cantidad);
			$precios = explode(";", $factura->fa_precio);
			$descuentos = explode(";", $factura->fa_descuento);
		} else {
			$servicios = explode(";", $factura->re_servicio);
			$cantidades = explode(";", $factura->re_cantidad);
			$precios = explode(";", $factura->re_precio);
			$descuentos = explode(";", $factura->re_descuento);
		}
		
		$subtotal = 0;
		$descuento_total = 0;
		
		$y = $y + 9;
		$y2 = 0;
		$alto_celdas = 0;
		
		for ($i = 0; $i < count($servicios)-1; $i++) {
			//calculamos el coste: precio unidad por cantidad
			$coste_total = (float)$precios[$i]*(int)$cantidades[$i];
			//calculamos el descuento si lo tienes del coste
			$coste_descuento = ($coste_total*$descuentos[$i])/100;
			//suma total sin iva de los servicios
			$subtotal = $subtotal + $coste_total;
			//suma total del descuento
			$descuento_total = $descuento_total + $coste_descuento;
			
			//RESULT SERVICIOS
			$this->pdf->SetXY(9,$y); 
			$this->pdf->MultiCell(77,5,$servicios[$i],'B',"L",false);
			$y2 = $this->pdf->GetY();
			$alto_celdas = $y2-$y;
			
			//RESULT COSTE UNIDAD
			$this->pdf->SetXY(86,$y); 
			$this->pdf->Cell(30,$alto_celdas,round($precios[$i], 2),'B',0,"R",false);
			
			//RESULT CANTIDAD
			$this->pdf->SetXY(116,$y); 
			$this->pdf->Cell(22,$alto_celdas,$cantidades[$i],'B',0,"R",false);
			
			//RESULT DESCUENTO
			$this->pdf->SetXY(138,$y); 
			$this->pdf->Cell(25,$alto_celdas,$descuentos[$i],'B',0,"R",false);
			
			//RESULT PRECIO
			$this->pdf->SetXY(163,$y); 
			$this->pdf->Cell(35,$alto_celdas,round($coste_total, 2),'B',0,"R",false);
			
			if ($y2 >= 245) {
				$this->pdf->AddPage();
				$y2 = 10;
			}
			
			$y = $y2 + 2;
		}
		
		//SUBTOTAL
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('subtotal'),0,0,"R",false);
		
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,round($subtotal, 2),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//DESCUENTO
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('descuento'),0,0,"R",false);
		
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,round($descuento_total, 2),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//BASE IMPONIBLE
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('base.imponible'),0,0,"R",false);
		
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,round($subtotal - $descuento_total, 2),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//IMPUESTOS
		$this->pdf->SetXY(138,$y);
		
		if ($tipo == 'F') {
			$this->pdf->Cell(25,7,'('.$factura->fa_iva.' %) '.lang('impuestos'),0,0,"R",false);
			$iva = $factura->fa_iva;
		} else {
			$this->pdf->Cell(25,7,'('.$factura->re_iva.' %) '.lang('impuestos'),0,0,"R",false);
			$iva = $factura->re_iva;
		}
		
		$impuestos = (($subtotal-$descuento_total)*$iva)/100;
		$this->pdf->SetXY(163,$y); 
		$this->pdf->Cell(35,7,round($impuestos, 2),0,0,"R",false);
		
		$y = $this->pdf->GetY()+7;
		
		//TOTAL
		$this->pdf->SetXY(138,$y); 
		$this->pdf->Cell(25,7,lang('total'),0,0,"R",false);
		
		$this->pdf->SetXY(163,$y);
		$this->pdf->SetFont('Arial','B',10);
		$this->pdf->Cell(35,7,round(($subtotal-$descuento_total) + $impuestos, 2),0,0,"R",false);
		$this->pdf->SetFont('Arial','',10);
		
		$y = $this->pdf->GetY()+15;
		
		if ($tipo == 'R') {
			if ($factura->re_info != '') {
				$this->pdf->SetXY(9,$y);
				$this->pdf->Cell(50,7,utf8_decode(lang('rectificativa.info')),0,0,"L",false);
				$y = $this->pdf->GetY()+7;
				$this->pdf->SetXY(9,$y);
				$this->pdf->MultiCell(188,5,utf8_decode($factura->re_info),0,"L",false);
				$y = $this->pdf->GetY()+15;
			}
		}
		
		if ($tipo == 'F') {
			if ($factura->fa_numero != '') {
				$nombre_archivo = $factura->fa_numero;
			} else {
				$nombre_archivo = lang('pendiente').'_'.lang('cliente').'_'.$factura->cl_dni;
			}
		}

		if ($envio == 'email') {
			//GENERAMOS EL PDF EN UNA CARPETA GENERICA PARA TODOS LOS PDF
			if ($tipo == 'F') {
				$this->pdf->Output(PRIVADO.$this->session->userdata('em_cif').'/pdfs/'.$nombre_archivo.'.pdf','F');
			 } else {
			 	$this->pdf->Output(PRIVADO.$this->session->userdata('em_cif').'/pdfs/'.$factura->re_numero.'.pdf','F');
			 }
			
			$this->load->library('email');
			$nombre = "=?utf-8?B?".base64_encode($factura->em_nombre)."=?=";
			$this->email->from(EMAIL, $nombre);
			
			if ($tipo == 'F') {
				$this->email->subject(lang('factura').': '.$nombre_archivo);
			} else {
				$this->email->subject(lang('factura.rectificativa').': '.$factura->re_numero);
			}
			
			//PLANTILLA EMAIL
			if ($tipo == 'F') {
				$data = array(
					'BODY' => $this->load->view('emails/v_email_factura','', TRUE)
				);
			} else {
				$data = array(
					'BODY' => $this->load->view('emails/v_email_factura_rectificativa','', TRUE)
				);
			}
			
			$email = $this->load->view('emails/v_email', $data, TRUE);

			$this->email->message($email);
			
			//ADJUNTAMOS EL PDF
			if ($tipo == 'F') {
				$this->email->attach(PRIVADO.$this->session->userdata('em_cif').'/pdfs/'.$nombre_archivo.'.pdf');
			} else {
				$this->email->attach(PRIVADO.$this->session->userdata('em_cif').'/pdfs/'.$factura->re_numero.'.pdf');
			}
			
			//STRING PARA LOS EMAILS
			$para = '';
			$copia = '';
			$oculta = '';
			
			//GENERAMOS LOS STRING DE EMAILS
			for ($x=0;$x<count($datos);$x++) {
				if ($datos[$x]['value'] == 1) {
					$para = $para.$datos[$x]['email'].',';
				} else if ($datos[$x]['value'] == 2) {
					$copia = $copia.$datos[$x]['email'].',';
				} else if ($datos[$x]['value'] == 3) {
					$oculta = $oculta.$datos[$x]['email'].',';
				}
			}
			
			//QUITAMOS EL ULTIMO CARACTER QUE ES LA COMA
			$para = trim($para, ',');
			$copia = trim($copia, ',');
			$oculta = trim($oculta, ',');
			
			//CREAMOS LAS CABECERAS PARA ENVIAR
			$this->email->to($para);
			$this->email->cc($copia);
			$this->email->bcc($oculta);
			
			//ENVIAMOS EL EMAIL  	
			$this->email->send();
			print_r($this->email->print_debugger());
			//BORRAMOS EL PDF CREADO EN LA CARPETA GENERICA
			if ($tipo == 'F') {
				unlink(PRIVADO.$this->session->userdata('em_cif').'/pdfs/'.$nombre_archivo.'.pdf');
			} else {
			 	unlink(PRIVADO.$this->session->userdata('em_cif').'/pdfs/'.$factura->re_numero.'.pdf');
			 }		
		} else {
			//GENERAMOS EL PDF
			if ($tipo == 'F') {
				$this->pdf->Output($nombre_archivo.'.pdf','D');
			} else {
				$this->pdf->Output($factura->re_numero.'.pdf','D');
			}
		}  
    }
    
    //MANDAR FACTURA POR EMAIL
    public function enviar_email() {
		if ($this->input->post('fa_id') != '') {
			$this->comprobar_usuario('facturas/editar_factura/'.$this->input->post('fa_id'));	
		} else {
			$this->comprobar_usuario('facturas');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('1')) {
			if ($this->session->userdata('emp_editar') == 1) {
				$this->get_pdf($this->input->post('fa_id'),'email',$this->input->post('tipo'),$this->input->post('datos'));
			} else {
				redirect(base_url().$this->lang->lang().'/facturas','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
	
	//GENERAR PDF FACTURA
	public function generar_pdf($fa_id=null,$tipo=null) {
		if ($fa_id != '') {
			$this->comprobar_usuario('facturas/editar_factura/'.$fa_id);	
		} else {
			$this->comprobar_usuario('facturas');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->acceso('1')) {
			if ($this->session->userdata('emp_editar') == 1) {
				$this->get_pdf($fa_id,'pdf',$tipo);
			} else {
				redirect(base_url().$this->lang->lang().'/facturas','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS LA FACTURA
	public function guardar_editar() {
		$this->comprobar_usuario('facturas');	
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('1')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->input->post('fa_id') != '') {
				if ($this->session->userdata('emp_editar') == 0) {
					$response['editar'] = false;
				} else {
					$entrar = true;
				}
			} else {
				if ($this->session->userdata('emp_crear') == 0) {
					$response['crear'] = false;
				} else {
					$entrar = true;
				}
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					$this->load->model('M_facturas');
					$response['sql'] = $this->M_facturas->set_factura($this->input->post());
					$response['status'] = $response['sql'];
					if($response['sql'] != false){
						$id = $response['sql'];
						$response['sql'] = true;
						$response['status'] = $response['sql'];
					}
				}
			}
			
			//GUARDAMOS LA BITACORA
			if ($response['status']) {
				$this->load->model('M_bitacora');
				$datos = array();
				$datos['tipo'] = 'FA'; //Facturas
				$datos['asociado'] = $id;
				$datos['accion'] = ($this->input->post('fa_id') != '')?'Editar':'Crear';
				$this->M_bitacora->set_bitacora($datos);
			}

			//DEVOLVEMOS UN ARRAY DE ARRAYS CON LOS RESULTADOS
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
	
	//GUARDAMOS O EDITAMOS LA FACTURA RECTIFICATIVA
	public function rectificativas_guardar_editar() {
		$this->comprobar_usuario('facturas');	
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));

		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('1')) {
			//VARIABLES PARA GUARDAR LOS POSIBLES ERRORES
			$response['status'] = false;
			$response['crear'] = true;
			$response['editar'] = true;
			$response['sql'] = true;
			$response['errors'] = '';
			$errors = array();
			$aux = array();
			$entrar = false;
			
			if ($this->session->userdata('emp_editar') == 0) {
				$response['editar'] = false;
			} else {
				$entrar = true;
			}
			
			if ($entrar) {
				if ($this->input->post()) {
					
					$this->form_validation->set_rules('re_info',lang('rectificativa.info'),'trim|max_length[4000]|xss_clean');
					if ($this->form_validation->run()) {
						$this->load->model('M_facturas');
						$response['sql'] = $this->M_facturas->set_rectificar_factura($this->input->post());
						$response['status'] = $response['sql'];
						if($response['sql'] != false){
							$id = $response['sql'];
							$response['sql'] = true;
							$response['status'] = $response['sql'];
						}
					}
					
					foreach ($this->input->post() as $key => $value) {
				           if (form_error($key) != '') {
							$aux = array($key,form_error($key));
				           	array_push($errors,$aux);
						}
				    }
				    $response['errors'] = array_filter($errors);
				}
			}

			//GUARDAMOS LA BITACORA
			if ($response['sql']) {
				$this->load->model('M_bitacora');
				$datos = array();
				$datos['tipo'] = 'FR'; //Facturas Rectificada
				$datos['asociado'] = $id;
				$datos['accion'] = ($this->input->post('re_numero') != '')?'Editar':'Crear';
				$this->M_bitacora->set_bitacora($datos);
			}
			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
	
	//GUARDAMOS LAS MODIFICACIONES DE LA FACTURA Y LA CERRAMOS
	public function cerrar() {
		if ($this->input->post('fa_id') != '') {
			$this->comprobar_usuario('facturas/editar_factura/'.$this->input->post('fa_id'));	
		} else {
			$this->comprobar_usuario('facturas');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->acceso('1')) {
			if ($this->session->userdata('emp_editar') == 1) {
				if ($this->input->post()) {
					$this->load->model('M_facturas');
					$response['sql'] = $this->M_facturas->set_cerrar_factura($this->input->post());	
					$response['status'] = $response['sql'];
				}
			}
			
			//GUARDAMOS LA BITACORA
			if ($response['status']) {
				$this->load->model('M_bitacora');
				$datos = array();
				$datos['tipo'] = 'FA'; //Facturas
				$datos['asociado'] = $this->input->post('fa_id');
				$datos['accion'] = 'Cerrar';
				$this->M_bitacora->set_bitacora($datos);
			}

			echo json_encode($response);
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
	
	//FACTURA RECTIFICATIVA
	public function rectificar() {
		if ($this->input->post('fa_id') != '') {
			$this->comprobar_usuario('facturas/editar_factura/'.$this->input->post('fa_id'));	
		} else {
			$this->comprobar_usuario('facturas');
		}
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->acceso('1')) {
			if ($this->session->userdata('emp_editar') == 1) {
				if ($this->input->post()) {

					$this->load->model('M_facturas');
					$response['sql'] = $this->M_facturas->set_rectificar_factura($this->input->post());
					
					//GUARDAMOS LA BITACORA
					if ($response['sql']) {
						$this->load->model('M_bitacora');
						$datos = array();
						$datos['tipo'] = 'FA'; //Facturas
						$datos['asociado'] = $this->input->post('fa_id');
						$datos['accion'] = 'Rectificar';
						$this->M_bitacora->set_bitacora($datos);
					}

					echo json_encode($response);	
				}
			} else {
				redirect(base_url().$this->lang->lang().'/facturas','refresh');
			}
		} else {
			redirect(base_url().$this->lang->lang().'/facturas','refresh');
		}
	}
}

/* End of file facturas.php */
/* Location: ./application/controllers/facturas.php */