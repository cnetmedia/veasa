<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Talleres extends MY_Controller {

	function __construct() {
		parent::__construct();
	}
	
	//MUESTRA LA LISTA DE TRABAJOS DEL TALLER
	public function index() {
		$this->comprobar_usuario('taller');
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18')) {
			$acceso = $this->load->view('v_talleres','',true);
		} else {
			$acceso = '<h2><i class="fa fa-unlock-alt"></i> '.lang('no.acceso').'</h2>';
		}
		
		$datos = array(
	        'TITULO' => lang('talleres.guardados'),
	        'STOCKAGE' => 'active',
	        'TALLERES' => 'active',
			'BODY' => $acceso,
			'BREADCRUMB' => array(['nombre'=>lang('stockage'), 'href'=>'#'],['nombre'=>lang('talleres.guardados'), 'href'=>''])
	    );
		$this->load->view('v_admin',$datos);
	}
	
	//BUSCA TRABAJOS DEL TALLER PARA MOSTRAR
	public function buscador() {
		$this->comprobar_usuario('taller');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18') || $this->acceso('20')) {
			if ($this->input->post()) {
				$this->load->model('M_taller');
				$array = $this->M_taller->get_trabajos_taller_guardado();
				
				echo json_encode($array);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/talleres','refresh');
		}
	}
	
	//BUSCA TRABAJOS DEL TALLER PARA MOSTRAR
	public function buscador_todos() {
		$this->comprobar_usuario('taller');
		
		//REFRESCAMOS LA SESSION
		$this->emp_session($this->session->userdata('emp_id'));
		
		//PREGUNTAMOS SI TIENE ACCESO A LA PLICACION
		if ($this->session->userdata('emp_acceso') == 0) {
			redirect(base_url().$this->lang->lang().'/empleados/salir','refresh');
		}
		
		//PREGUNTAMOS SI TIENE ACCESO A ESTA SECCION
		if ($this->acceso('18') || $this->acceso('20')) {
			if ($this->input->post()) {
				$this->load->model('M_taller');
				$array = $this->M_taller->get_trabajos_taller_todos();
				
				echo json_encode($array);
			}
		} else {
			redirect(base_url().$this->lang->lang().'/talleres','refresh');
		}
	}
}

/* End of file talleres.php */
/* Location: ./application/controllers/talleres.php */