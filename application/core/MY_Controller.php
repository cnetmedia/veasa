<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
		//date_default_timezone_set('Europe/London');
        //$this->output->enable_profiler(TRUE);
    }
	
	//COMPRUEBA SI EXISTE SESSION DE USUARIO
	public function comprobar_usuario($url=null) {
		$this->comprobar_cookie();
		$usuario = $this->session->userdata('emp_id');
		//SI NO ENCUENTRA LA SESSION
		if (!isset($usuario) || (trim($usuario) == '')) {
            //LIMPIAMOS LA SESSION, SIN BORRARLA
            /*$user_data = $this->session->all_userdata();
		    foreach ($user_data as $key => $value) {
		        if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
		            $this->session->unset_userdata($key);
		        }
		    }*/
            
            //PREGUNTAMOS SI TENEMOS URL DONDE IR AL TERMINAR EL LOGIN
            if (isset($url)) {
            	//GUARDAMOS ESA URL EN LA SESSION
                $dato = array('url' => $url);
                $this->session->set_userdata($dato);
            }
            //VAMOS AL LOGIN
            redirect(base_url().$this->lang->lang().'/empleados/login','refresh');
        }
	}
	
	//COMPRUEBA SI EXISTE SESSION DE CLIENTE
	public function comprobar_cliente($url=null) {
		$usuario = $this->session->userdata('cliente');
		//SI NO ENCUENTRA LA SESSION
		if (!isset($usuario) || (trim($usuario) == '')) {
            //LIMPIAMOS LA SESSION, SIN BORRARLA
            /*$user_data = $this->session->all_userdata();
		    foreach ($user_data as $key => $value) {
		        if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
		            $this->session->unset_userdata($key);
		        }
		    }*/
            
            //PREGUNTAMOS SI TENEMOS URL DONDE IR AL TERMINAR EL LOGIN
            if (isset($url)) {
            	//GUARDAMOS ESA URL EN LA SESSION
                $dato = array('url' => $url);
                $this->session->set_userdata($dato);
            }
            //VAMOS AL LOGIN
            redirect(base_url().$this->lang->lang().'/empleados/login','refresh');
        }
	}
	
	//DATOS DE LA SESSION DEL EMPLEADO
	public function emp_session($emp_dni) {
		$user = $this->db->from('empleados')
                        ->select('empleados.*, empresas.em_id, empresas.em_nombre, empresas.em_cif, sucursales.su_zona_horaria, sucursales.su_id')
			->join('empresas', 'empresas.em_id = empleados.em_id')
                        ->join('sucursales', 'sucursales.em_id = empresas.em_id')
			->join('departamentos', 'departamentos.su_id = sucursales.su_id')
			->where('empleados.emp_dni',$emp_dni)
			->get()->row();
			
        if ($user != null) {
        	
			$sessionData = array(
			    'emp_id' => $user->emp_id,
			    'emp_nombre' => $user->emp_nombre,
			    'emp_apellido1' => $user->emp_apellido1,
			    'emp_apellido2' => $user->emp_apellido2,
			    'emp_tipo' => $user->emp_tipo,
			    'emp_crear' => $user->emp_crear,
			    'emp_editar' => $user->emp_editar,
			    'emp_acceso' => $user->emp_acceso,
			    'emp_ver' => $user->emp_ver,
			    'emp_dni' => $user->emp_dni,
			    'em_id' => $user->em_id,
			    'em_nombre' => $user->em_nombre,
			    'em_cif' => $user->em_cif,
			    'su_zona_horaria' => $user->su_zona_horaria,
			    'su_id' => $user->su_id,
			    'de_id' => $user->de_id,
                'loggedIn' => true
			);
			$this->session->set_userdata($sessionData); // agregamos info a la session
		}
		
		$this->session->unset_userdata('token');	
	}
	
	//DATOS DE LA SESSION DEL CLIENTE
	public function cl_session($cliente) {
		//SI NO ENCUENTRA UN @ ES UN MANTENIMIENTO
		if (strpos($cliente, '@') === false) {
			$user = $this->db->from('mantenimientos')
				->join('empresas', 'empresas.em_id = mantenimientos.em_id')
				->where('mantenimientos.man_id',$cliente)
				->get()->row();
				
				if ($user != null) {
				$sessionData = array(
				    'cl_id' => $user->cl_id,
				    'em_id' => $user->em_id,
				    'em_nombre' => $user->em_nombre,
				    'em_cif' => $user->em_cif,
				    'cliente' => $cliente,
				    'cl_tipo' => 2,
				    'loggedIn' => true
				);
				$this->session->set_userdata($sessionData); // agregamos info a la session
			}
		} else {
			//ES UN CLIENTE
			$user = $this->db->from('clientes')
				->join('mantenimientos', 'mantenimientos.cl_id = clientes.cl_id', 'left')
				->join('sucursales', 'sucursales.su_id = clientes.su_id')
				->join('empresas', 'empresas.em_id = clientes.em_id')
				->where('clientes.cl_email',$cliente)
				->or_where('mantenimientos.man_email',$cliente)
				->get()->row();
				
	        if ($user != null) {
				$sessionData = array(
				    'cl_id' => $user->cl_id,
				    'su_id' => $user->su_id,
				    'su_zona_horaria' => $user->su_zona_horaria,
				    'em_id' => $user->em_id,
				    'em_nombre' => $user->em_nombre,
				    'em_cif' => $user->em_cif,
				    'cliente' => $cliente,
					'loggedIn' => true
				);
				$this->session->set_userdata($sessionData); // agregamos info a la session
				
				//PREGUNTAMOS SI TIENE ID DE MANTENIMIENTO PARA SABER SI ES EL CLIENTE PRINCIPAL O UN SUBCLIENTE CON ACCESO SOLO AL MANTENIMIENTO/S
				if ($user->cl_email == $cliente) {
					$this->session->set_userdata('cl_tipo',0);
				} else if ($user->man_email == $cliente) {
					$this->session->set_userdata('cl_tipo',1);
				}
			}
		}
		
		$this->session->unset_userdata('token');
	}
	
	//COMPRUEBA SI TIENE ACCESO A LA SECCION
	public function acceso($num) {
		$ver = explode(",", $this->session->userdata('emp_ver'));
		$ver = array_filter($ver);
		
		return in_array($num,$ver);
	}
	
	//CREAMOS UN TOKEN CADA VEZ QUE CARGAMOS EL LOGIN
	public function token() {
        $token = md5(uniqid(rand(),true));
        $this->session->set_userdata('token',$token);
        return $token;
    }
	
	public function view_file($file1=null,$file2=null,$file3=null,$file4=null) {
		$empleado = $this->session->userdata('emp_id');
		$cliente = $this->session->userdata('cliente');
		//SI NO ENCUENTRA LA SESSION
		if (!isset($empleado) || (trim($empleado) == '')) {
			if (!isset($cliente) || (trim($cliente) == '')) {
				//VAMOS AL LOGIN
				redirect($_SERVER['PHP_SELF'],'refresh');
			}
		}
		
		$this->load->helper('file');
		//RUTA ARCHIVO
		$archivo = PRIVADO;
		
		if ($file1 != null) {
			$archivo = $archivo.$file1.'/';
		}
		
		if ($file2 != null) {
			$archivo = $archivo.$file2.'/';
		}
		
		if ($file3 != null) {
			$archivo = $archivo.$file3.'/';
		}
		
		if ($file4 != null) {
			$archivo = $archivo.$file4.'/';
		}
		
		//ELIMINAMOS EL ULTIMO CARACTER
		$archivo = substr($archivo, 0, -1);
		
		//CARGAR ARCHIVO
		$datafile = file_get_contents($archivo);
		//ENVIAR ARCHIVO
		echo $datafile;
	}
        
    //COMPROBAR COOKIE DE RECUERDAME
	function comprobar_cookie() {
		if (isset($_COOKIE['RememberMe'])) {
			$this->emp_session($_COOKIE['RememberMe']);
		}
	}   
}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */