<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MY_Model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
 
    public function contadores() {
        $contadores = $this->db->from('contadores')
            ->where('em_id',$this->session->userdata('em_id'))
            ->where('su_id',$this->session->userdata('su_id'))
            ->get()->result();
        
        $anio = date_create(fechaHora_actual($this->session->userdata('su_zona_horaria')));
        $anio = date_format($anio, 'Y');
        
        $num = 0;
        for ($x=0; $x<count($contadores); $x++) {
            if ((int)$contadores[$x]->co_year != (int)$anio) {
                $num++;
            }
        }
        
        if ($num == count($contadores)) {
            $datos = array(
                    'co_contador' => 0,
                    'co_year' => $anio
            );
            
            $this->db->where('su_id', $this->session->userdata('su_id'));
            $this->db->where('em_id', $this->session->userdata('em_id'));
            $this->db->update('contadores', $datos); 
        }        
    }     
}