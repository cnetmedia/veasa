<?php
//CREAMOS LOS INPUT DEL FORMULARIO
$input_nombre = array(
	'name'		=>	'pro_nombre',
	'id'		=>	'pro_nombre',
	'class'		=>	'form-control',
	'maxlength'	=>	'80',
	'value'		=>	isset($pro_nombre)?$pro_nombre:set_value('pro_nombre')
);

$input_cif = array(
	'name'		=>	'pro_cif',
	'id'		=>	'pro_cif',
	'class'		=>	'form-control',
	'maxlength'	=>	'30',
	'value'		=>	isset($pro_cif)?$pro_cif:set_value('pro_cif')
);

$input_telefono = array(
	'name'		=>	'pro_telefono',
	'id'		=>	'pro_telefono',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'value'		=>	isset($pro_telefono)?$pro_telefono:set_value('pro_telefono')
);

$input_email = array(
	'name'		=>	'pro_email',
	'id'		=>	'pro_email',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($pro_email)?$pro_email:set_value('pro_email')
);

$input_provincia = array(
	'name'		=>	'pro_provincia',
	'id'		=>	'pro_provincia',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($pro_provincia)?$pro_provincia:set_value('pro_provincia')
);

$input_postal = array(
	'name'		=>	'pro_postal',
	'id'		=>	'pro_postal',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	isset($pro_postal)?$pro_postal:set_value('pro_postal')
);

$input_direccion = array(
	'name'		=>	'pro_direccion',
	'id'		=>	'pro_direccion',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($pro_direccion)?$pro_direccion:set_value('pro_direccion')
);

$input_barriada = array(
	'name'		=>	'pro_barriada',
	'id'		=>	'pro_barriada',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($pro_barriada)?$pro_barriada:set_value('pro_barriada')
);

$input_localidad = array(
	'name'		=>	'pro_localidad',
	'id'		=>	'pro_localidad',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($pro_localidad)?$pro_localidad:set_value('pro_localidad')
);

$input_id = array(
	'name'		=>	'pro_id',
	'id'		=>	'pro_id',
	'type'		=>	'hidden',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	isset($pro_id)?$pro_id:set_value('pro_id')
);
?>

<form id="datos_proveedor" action="<?php echo base_url().$this->lang->lang() ?>/proveedores/guardar_editar" method="post" enctype="multipart/form-data">

<div class="row">
	<!-- MOSTRAMOS LA FOTO SI EL PROVEEDOR ES A EDITAR -->
	<?php if ($pro_id != null) { ?>
	<div class="col-lg-4 col-md-4 col-xs-12 text-center">
		<img src="<?php echo isset($pro_id)?base_url().$this->lang->lang().'/empresa/view_file/'.$em_cif.'/pro/'.$pro_cif.'/logo.jpg':base_url().$this->lang->lang().'/empresa/view_file/'.'logo.jpg'; ?>" id="perfil" class="img-thumbnail" />			
	</div>
	<?php } ?>
	
	<!-- FORMULARIO PROVEEDOR -->
	<div class="<?php echo ($pro_id != '')?'col-lg-8':'col-lg-12' ?>">
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('nombre') ?></label>
					    <?php echo form_input($input_nombre) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_nombre') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('cif') ?></label>
					    <?php echo form_input($input_cif) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_cif') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('telefono') ?></label>
					    <?php echo form_input($input_telefono) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_telefono') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('email') ?></label>
					    <?php echo form_input($input_email) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_email') ?></div>
					</div>
				</div>
				
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('provincia') ?></label>
					    <?php echo form_input($input_provincia) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_provincia') ?></div>
					</div>
				</div>
				
				<div class="col-lg-2 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('postal') ?></label>
					    <?php echo form_input($input_postal) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_postal') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('direccion') ?></label>
					    <?php echo form_input($input_direccion) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_direccion') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="<?php echo ($pro_id != '')?'col-lg-4':'col-lg-3' ?> col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('barriada') ?></label>
					    <?php echo form_input($input_barriada) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_barriada') ?></div>
					</div>
				</div>
			
				<div class="<?php echo ($pro_id != '')?'col-lg-4':'col-lg-3' ?> col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('localidad') ?></label>
					    <?php echo form_input($input_localidad) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_localidad') ?></div>
					</div>
				</div>
				
				<div class="<?php echo ($pro_id != '')?'col-lg-4':'col-lg-3' ?> col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('pais') ?></label>
					    <select name="pro_pais" id="paises" class="form-control"></select>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_pais') ?></div>
					</div>
				</div>
				
				<div class="<?php echo ($pro_id != '')?'col-lg-12':'col-lg-3' ?> col-xs-12">
					<label><?php echo lang('foto.proveedor') ?></label>					
					<div class="input-group">
						<span class="input-group-btn">
							<span class="btn btn-primary btn-file">
								<!--<input type="file" multiple>-->
								<i class="fa fa-camera"></i> <?php echo lang('cargar.foto') ?>
								<input type="file" id="foto" name="foto">
							</span>
						</span>
						<input type="text" class="form-control" readonly>
					</div>
					<div class="text-danger"><?php echo form_error('foto') ?></div>
				</div>
			</div>
			
			<?php echo form_input($input_id) ?>
	</div>
</div>

<div class="row emails"></div>

<div class="row">
	<hr>
	<div class="col-lg-12 col-xs-12 text-right">
		<?php if ($this->session->userdata('de_id') != '2' && $this->session->userdata('de_id') != '3' && $this->session->userdata('de_id') != '5') { ?>
			<!-- AÑADIR EMAIL ADICIONAL -->
			<?php if ($this->session->userdata('emp_editar') == '1') { ?>
				<button id="btn_email_adicional" type="button" class="btn btn-primary ladda-button" data-style="zoom-out">
					<i class="fa fa-envelope"></i> <?php echo lang('email.adicional') ?>
				</button>
			<?php } ?>
			
			<!-- SI PUEDE CREAR O EDITAR CONTENIDO MUESTRA EL BOTON -->
			<?php if ($this->session->userdata('emp_crear') == '1' || $this->session->userdata('emp_editar') == '1') { ?>
				<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
					<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
				</button>
			<?php } ?>
		<?php } ?>
					
		<a href="<?php echo base_url().$this->lang->lang().'/proveedores' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
	</div>
</div>

</form>

<script>
/* ESTILO Y BOTON FOTO */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function(){
	//VARIABLES GLOBALES
	var pro_id = '<?php echo isset($pro_id)?$pro_id:set_value("pro_id") ?>';
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	
	//PREGUNTAMOS SI PUEDE EDITAR
	var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	if (editar == 0) {
		$('#btn_guardar').hide();
		$('#datos_cliente *').attr('readonly', true);
	}
	
	//RELLENAMOS EL CAMPO DE PAISES PARA AÑADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/sucursales/paises",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
				//PAIS
				if (data[indice]["id_countries"] == '<?php echo isset($pro_pais)?$pro_pais:"" ?>') {
					$('#paises').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
				} else {
					$('#paises').append('<option value="'+data[indice]["id_countries"]+'">'+data[indice]["name"]+'</option>');
				}
		});
	}, "json");
	
	//BLOQUEAMOS O NO EL DNI SI EL CLIENTE ES A EDITAR
	if (($('#pro_cif').val() != '') && (tipo != 0)) {
		$('#pro_cif').attr('readonly', true);
	}
	
	//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO
	$('#datos_proveedor .form-group span').hide();
	
	//ESTILO Y BOTON FOTO
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {	
		var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}	
	});
	
	//EJECUTAMOS EL FORMULARIO AL DARLE AL BOTON
	$('#btn_guardar').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#datos_proveedor .form-group, #datos_proveedor .input-group").removeClass("has-error has-feedback");
	    $('#datos_proveedor .glyphicon-remove').hide();
	    $("#datos_proveedor .text-danger").html('');
	    
	    var enviar = true;
	    var emails = '';
	    var emails_info = '';
	    
	    $('.emails .email_adic').each(function(){
	    	if ($(this).find('input:eq(0)').val() != '' && $(this).find('input:eq(1)').val() != '') {
	    		emails = emails + $(this).find('input:eq(0)').val() + ';';
				emails_info = emails_info + $(this).find('input:eq(1)').val() + ';';
			} else if ($(this).find('input:eq(0)').val() == '') {
				$(this).find('input:eq(0)').next().html('<?php echo lang("requerido") ?>');
				$(this).find('input:eq(0)').parent().addClass('has-error');
				enviar = false;
			} else if ($(this).find('input:eq(1)').val() == '') {
				$(this).find('input:eq(1)').next().html('<?php echo lang("requerido") ?>');
				$(this).find('input:eq(1)').parent().addClass('has-error');
				enviar = false;
			}
	    });
	    
	    if (enviar) {
			jQuery("#datos_proveedor").ajaxForm({
			   	dataType: 'json',
			   	data: {pro_emails:emails,pro_emails_info:emails_info},
				success: function(data) {
					if (data.sql) {
						if (data.status) {
							//TODO CORRECTO
							//SI ES NUEVO PROVEEDOR
							if ($('#pro_id').val() == '') {
								$('#datos_proveedor input').val('');
								bootbox.dialog({
									message: "<?php echo lang('proveedor.nuevo.ok') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('nuevo') ?>",
											className: "btn-success",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/proveedores/nuevo_proveedor';
												$(location).attr('href',url);
											}
										},
										main: {
											label: "<?php echo lang('proveedores') ?>",
											className: "btn-primary",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/proveedores';
												$(location).attr('href',url);
											}
										}
									}
								});
							} else {
							//SI ES UN PROVEEDOR EDITADO
								$('#perfil').attr("src","<?php echo isset($pro_id)?base_url().$this->lang->lang().'/empresa/view_file/'.$em_cif.'/pro/'.$pro_cif.'/logo.jpg':base_url().$this->lang->lang().'/empresa/view_file/'.'logo.jpg'; ?>"+"?timestamp=" + new Date().getTime());
								bootbox.alert('<?php echo lang("proveedor.editado.ok") ?>');
							}
						} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									//SI EXSITE ERROR EN EL CAMPO FOTO ENTRAMOS POR AQUI
									if (data.errors[ind][0] == 'foto') {
										$('input[name='+data.errors[ind][0]+']').parent().parent().parent().parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('input[name='+data.errors[ind][0]+']').parent().parent().parent().addClass("has-error has-feedback");
									}
									//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
								}); 
							});
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
					l.stop();
				}
			}).submit();
		}
	});
	
	//LABELS EMAILS ADICIONALES
	function add_label_emails() {
		$('.emails .email_adic').each(function(indice){
			indice = indice + 1;
			$(this).find('label:eq(0)').html('<?php echo lang("email.adicional") ?> '+indice);
			$(this).find('label:eq(1)').html('<?php echo lang("titulo.email.adicional") ?> '+indice);
			$(this).find('label:eq(2)').html('<?php echo lang("eliminar.email.adicional") ?> '+indice);
		});
	}
	
	//AÑADIR EMAILS ADICIONAL
	var cont_emails = 0;
	function add_emails(email,info) {
		if (cont_emails == 0) {
			$('.emails').append('<hr>');
		}
		
		var aux = '<div class="col-lg-12 email_adic"><div class="row"><div class="col-lg-5 col-xs-12"><label></label><input class="form-control" type="text" value="'+email+'"><div class="text-danger"></div></div><div class="col-lg-5 col-xs-12"><label></label><input class="form-control" type="text" value="'+info+'"><div class="text-danger"></div></div><div class="col-lg-2 col-xs-12 text-right"><div><label></label></div><button class="btn btn-default ladda-button" data-style="zoom-out"><?php echo lang("eliminar") ?></button></div></div></div>';
		
		$('.emails').append(aux);
		cont_emails++;
		add_label_emails();
	}
	
	//EMAILS ADICIONALES
	if (pro_id != '') {
		var pro_emails = '<?php echo isset($pro_emails)?$pro_emails:"" ?>';
		pro_emails = pro_emails.split(';');
		
		var pro_emails_info = '<?php echo isset($pro_emails_info)?$pro_emails_info:"" ?>';
		pro_emails_info = pro_emails_info.split(';');
		
		for (var i=0; i<pro_emails.length-1; i++) {
			add_emails(pro_emails[i],pro_emails_info[i]);
		}
	}
	
	//BOTON AÑADIR EMAIL ADICIONAL
	$('#btn_email_adicional').click(function(e){
		e.preventDefault();
		add_emails('','');
	});
	
	//BORRAR EMAIL ADICIONAL
	$('.emails .email_adic button').click(function(e){
		e.preventDefault();
		$(this).parent().parent().parent().remove();
		add_label_emails();
	});
});
</script>