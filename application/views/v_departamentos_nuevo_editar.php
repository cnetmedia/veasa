<div id="nuevo_editar">
	<?php
	$input_nombre = array(
		'name'		=>	'de_nombre',
		'id'		=>	'de_nombre',
		'class'		=>	'form-control',
		'maxlength'	=>	'50',
		'value'		=>	isset($de_nombre)?$de_nombre:set_value('de_nombre')
	);
	
	$input_de_id = array(
		'name'		=>	'de_id',
		'id'		=>	'de_id',
		'type'		=>	'hidden',
		'class'		=>	'form-control',
		'maxlength'	=>	'11',
		'value'		=>	isset($de_id)?$de_id:set_value('de_id')
	);
	?>
			<div class="row">
				<form id="datos_departamentos" action="" method="post" enctype="multipart/form-data">
				
					<div class="col-lg-3 col-xs-12">
						<div class="row">
							<div class="col-lg-12 col-xs-12">
								<div class="form-group">
									<label><?php echo lang('nombre') ?></label>
									<?php echo form_input($input_nombre) ?>
									<span class="glyphicon glyphicon-remove form-control-feedback"></span>
									<div class="text-danger"><?php echo form_error('de_nombre') ?></div>
								</div>
							</div>
							
							<div class="col-lg-12 col-xs-12">
								<div class="form-group">
									<label><?php echo lang('sucursal') ?></label>
									<select id="sucursales" name="su_id" class="form-control"></select>
									<span class="glyphicon glyphicon-remove form-control-feedback"></span>
									<div class="text-danger"><?php echo form_error('su_id') ?></div>
								</div>
							</div>
						</div>
					</div>
					
					
					
					<div class="col-lg-12 col-xs-12 btn_departamentos">
						<!-- SI PUEDE CREAR O EDITAR CONTENIDO MUESTRA EL BOTON -->
						<?php if ($this->session->userdata('emp_crear') == '1' || $this->session->userdata('emp_editar') == '1') { ?>
							<?php if ($de_estado == 1) { ?>
								<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
									<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
								</button>
							<?php } ?>
						<?php } ?>
						
						<!-- SI PUEDE EDITAR MOSTRAMOS EL BOTON DE HABILITAR Y DESHABILITAR -->
					<?php if ($de_id != '' && $this->session->userdata('emp_editar') == '1') { ?>
						<?php if ($de_estado == 0) { ?>
							<button id="btn_habilitar" type="button" class="btn btn-warning ladda-button" data-style="zoom-out">
								<i class="fa fa-check"></i> <?php echo lang('habilitar') ?>
							</button>
						<?php } else {?>
							<button id="btn_deshabilitar" type="button" class="btn btn-danger ladda-button" data-style="zoom-out">
								<i class="fa fa-times"></i> <?php echo lang('deshabilitar') ?>
							</button>
						<?php } ?>
					<?php } ?>

					<a href="<?php echo base_url().$this->lang->lang().'/departamentos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
						</button>
					</div>
					
					<?php echo form_input($input_de_id) ?>
				</form>
			</div>
		</div>

<script>
$(document).ready(function(){
	//PREGUNTAMOS SI PUEDE EDITAR
	var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	if (editar == 0) {
		$('#btn_guardar, #btn_habilitar, #btn_deshabilitar').hide();
		$('#datos_departamentos *').attr('readonly', true);
	}
	
	//HABILITAMOS LA VISION
	$('#btn_habilitar').click(function(e){
		e.preventDefault();
		var l = Ladda.create( document.querySelector( "#btn_habilitar" ) );
	 	l.start();

		$.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/departamentos/estado",
		  	data: {'de_id':'<?php echo $de_id ?>'},
		  	success: function(data) {
		  		if (data.sql) {
					if (!data.editar) {
						bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
						//REDIRIGIMOS A LOS 5 SEGUNDOS
						setTimeout("location.href='"+location+"'", 5000);
					} else {
						location.reload(true);
					}
		  		} else {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
				}
				l.stop();
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
	
	//ALERT CONFIRMANDO LA DESHABILITACION DEL ELEMENTO
	$('#btn_deshabilitar').click(function(e){
		e.preventDefault();
		bootbox.dialog({
			message: "<?php echo lang('confirmar.deshabilitar') ?>",
			buttons: {
				success: {
					label: "<?php echo lang('cancelar') ?>",
					className: "btn-default"
				},
				danger: {
					label: "<?php echo lang('deshabilitar') ?>",
					className: "btn-danger",
					callback: function(result) {
						var l = Ladda.create( document.querySelector( "#btn_deshabilitar" ) );
	 					l.start();
		    			
		    			$.ajax({
							type: "POST",
							dataType: "json",
						  	url: "<?php echo base_url().$this->lang->lang() ?>/departamentos/estado",
						  	data: {'de_id':'<?php echo $de_id ?>'},
						  	success: function(data) {
						  		if (data.sql) {
									if (!data.editar) {
										bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										setTimeout("location.href='"+location+"'", 5000);
									} else {
										location.reload(true);
									}
						  		} else {
									bootbox.alert('<?php echo lang("error.ajax") ?>');
								}
								l.stop();
						  	},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});
					}
				}
			}
		});
	});
	
	//SI EL DEPARTAMENTO ESTA DESACTIVADO BLOQUEAMOS LOS CAMPOS
	var desactivado = '<?php echo isset($de_estado)?$de_estado:"" ?>';
	if ($('#de_id').val() != '' && desactivado == 0) {
		$('#datos_departamentos *').attr('readonly', true);
	}
	
	//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO
	$('#datos_departamentos .form-group span').hide();
	
	//RELLENAMOS EL CAMPO DE SUCURSALES PARA A�ADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
				if (data[indice]['su_estado'] == '1') {
					if (data[indice]["su_id"] == '<?php echo $su_id ?>') {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
					} else if (data[indice]["su_id"] == '<?php echo $this->session->userdata("su_id") ?>') {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
					} else if (data[indice]["su_estado"] == 1) {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
					}
				}
		});
	}, "json");
	
	//GUARDAR O EDITAR CONTENIDO
	$("#btn_guardar").click(function(e){
	    e.preventDefault();
	    
	    var l = Ladda.create( document.querySelector( '#btn_guardar' ) );
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#datos_departamentos .form-group, #datos_departamentos .input-group").removeClass("has-error has-feedback");
	    $('#datos_departamentos .glyphicon-remove').hide();
	    $("#datos_departamentos .text-danger").html('');
	    
	    var datos = {
			'de_id' : '<?php echo isset($de_id)?$de_id:"" ?>',
			'de_nombre' : $('#de_nombre').val(),
			'su_id' : $('#sucursales option:selected').val()
		};
	    
	    $.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/departamentos/guardar_editar",
		  	data: datos,
		  	success: function(data) {
		  		if (data.sql) {
		  			
		  			//MENSAJES ALERTA PARA EDITAR O CREAR
		  			if (datos['de_id'] != '') {
						if (!data.editar) {
							bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
							//REDIRIGIMOS A LOS 5 SEGUNDOS
							setTimeout("location.href='"+location+"'", 5000);
						}
					} else {
						if (!data.crear) {
							bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
							//REDIRIGIMOS A LOS 5 SEGUNDOS
							setTimeout("location.href='"+location+"'", 5000);
						}
					}
		  			
		  			if (data.status) {
						//TODO CORRECTO
						//SI ES NUEVO DEPARTAMENTO
						if ($('#de_id').val() == '') {
							$('#datos_departamentos #de_nombre').val('');
							bootbox.dialog({
								message: "<?php echo lang('departamento.nuevo.ok') ?>",
								buttons: {
									success: {
										label: "<?php echo lang('nuevo') ?>",
										className: "btn-success",
										callback: function(result) {
											var url = '<?php echo base_url().$this->lang->lang() ?>/departamentos/nuevo_departamento';
											$(location).attr('href',url);
										}
									},
									main: {
										label: "<?php echo lang('departamentos') ?>",
										className: "btn-primary",
										callback: function(result) {
											var url = '<?php echo base_url().$this->lang->lang() ?>/departamentos';
											$(location).attr('href',url);
										}
									}
								}
							});
						} else {
							bootbox.alert('<?php echo lang("departamento.editado.ok") ?>');
						}
					} else {
						//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
						//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
						if (data.errors.length > 0) {
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
								}); 
							});
						}
					}
		  		} else {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
				}
				l.stop();
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
});
</script>