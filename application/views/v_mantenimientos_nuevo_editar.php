<?php
$input_contacto = array(
	'name'		=>	'man_contacto',
	'id'		=>	'man_contacto',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($man_contacto)?$man_contacto:set_value('man_contacto')
);

$input_telefono = array(
	'name'		=>	'man_telefono',
	'id'		=>	'man_telefono',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'value'		=>	isset($man_telefono)?$man_telefono:set_value('man_telefono')
);

$input_email = array(
	'name'		=>	'man_email',
	'id'		=>	'man_email',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($man_email)?$man_email:set_value('man_email')
);

$input_barriada = array(
	'name'		=>	'man_barriada',
	'id'		=>	'man_barriada',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($man_barriada)?$man_barriada:set_value('man_barriada')
);

$input_localidad = array(
	'name'		=>	'man_localidad',
	'id'		=>	'man_localidad',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($man_localidad)?$man_localidad:set_value('man_localidad')
);

$input_postal = array(
	'name'		=>	'man_postal',
	'id'		=>	'man_postal',
	'class'		=>	'form-control',
	'maxlength'	=>	'30',
	'value'		=>	isset($man_postal)?$man_postal:set_value('man_postal')
);

$input_provincia = array(
	'name'		=>	'man_provincia',
	'id'		=>	'man_provincia',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($man_provincia)?$man_provincia:set_value('man_provincia')
);

$input_direccion = array(
	'name'		=>	'man_direccion',
	'id'		=>	'man_direccion',
	'class'		=>	'form-control',
	'maxlength'	=>	'150',
	'value'		=>	isset($man_direccion)?$man_direccion:set_value('man_direccion')
);

$input_nombre = array(
	'name'		=>	'man_nombre',
	'id'		=>	'man_nombre',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($man_nombre)?$man_nombre:set_value('man_nombre')
);

$input_latitud = array(
	'name'		=>	'man_latitud',
	'id'		=>	'man_latitud',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'disabled'	=>	true,
	'value'		=>	isset($man_latitud)?$man_latitud:set_value('man_latitud')
);

$input_longitud = array(
	'name'		=>	'man_longitud',
	'id'		=>	'man_longitud',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'disabled'	=>	true,
	'value'		=>	isset($man_longitud)?$man_longitud:set_value('man_longitud')
);
?>
<div id="mantenimiento">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('sucursal') ?></label>
				<select id="sucursales" name="su_id" class="form-control"></select>
			</div>
		</div>
	
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('cliente') ?></label>
				<select id="clientes" name="cl_id" class="selectpicker form-control" data-live-search="true"></select>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_contacto') ?></div>
			</div>
		</div>
		
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="form-group">
				<label><?php echo lang('contacto') ?></label>
				<?php echo form_input($input_contacto) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_contacto') ?></div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('nombre.mantenimiento') ?></label>
				<?php echo form_input($input_nombre) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_nombre') ?></div>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('email') ?></label>
				<?php echo form_input($input_email) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_email') ?></div>
			</div>
		</div>
		
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="form-group">
				<label><?php echo lang('direccion') ?></label>
				<?php echo form_input($input_direccion) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_direccion') ?></div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('barriada') ?></label>
				<?php echo form_input($input_barriada) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_barriada') ?></div>
			</div>
		</div>
	
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('postal') ?></label>
				<?php echo form_input($input_postal) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_postal') ?></div>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('localidad') ?></label>
				<?php echo form_input($input_localidad) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_localidad') ?></div>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('provincia') ?></label>
				<?php echo form_input($input_provincia) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_provincia') ?></div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('pais') ?></label>
				<select name="emp_pais" id="paises_pais" class="form-control"></select>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_pais') ?></div>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('telefono') ?></label>
				<?php echo form_input($input_telefono) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_telefono') ?></div>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('latitud') ?></label>
				<?php echo form_input($input_latitud) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_latitud') ?></div>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-3">
			<div class="form-group">
				<label><?php echo lang('longitud') ?></label>
				<?php echo form_input($input_longitud) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('man_longitud') ?></div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<label><?php echo lang('mantenimiento.gps') ?></label>
			<div>
				<div class="input-group">
                	<span class="input-group-addon" id="man_buscar" title="<?php echo lang('buscar') ?>"><i class="fa fa-search"></i></span>
                    <input class="form-control" type="text" id="input_buscar">
                </div>
				<div id="map-canvas"></div>
			</div>
		</div>
	</div>
	
	<div class="row bottom30">
		<div class="col-lg-12">
			<label><?php echo lang('observaciones') ?></label>
			<textarea class="form-control" style="max-width: 100%; width: 100%; height: 120px;" name="man_observaciones" id="man_observaciones"><?php echo isset($man_observaciones)?$man_observaciones:'' ?></textarea>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12 text-right">
			<?php if (($this->session->userdata('de_id') != '5') && ($this->session->userdata('de_id') != '7')) { ?>
			<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
				<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
			</button>
			<?php } ?>
			
			<?php if ($man_id != '' && $this->session->userdata('emp_tipo') != '1') { ?>
			<a id="mandar_email" href="#" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-envelope"></i> <?php echo lang('email') ?></a>
			<?php } ?>
			
			<a href="<?php echo base_url().$this->lang->lang().'/mantenimientos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
		</div>
	</div>
</div>

<hr/>

<?php if ($man_id != null) { 
$auxProd = $categorias[0];
$auxFac = $categorias[1];
$auxTaller = $categorias[2];

/*print_r($costoTaller);*/

?>

<div class="row">
	<div class="col-xs-12">
	    <table id="costos" class="table table-bordered table-striped table-hover dataTable">
	        <thead>
	            <tr>
	                <th class="text-center" style="vertical-align: middle;"><?php echo lang("descripcion") ?></th>
	                <th class="text-center" style="vertical-align: middle;"><?php echo lang("coste") ?></th>
	                <th class="text-center"><?php echo lang("entradas.aprobadas.compra") ?></th>
	                <th class="text-center" style="vertical-align: middle;"><?php echo lang("costo.disponible") ?></th>
	            </tr>
	        </thead>
	        <tbody>
	        <?php 
			
				$total_costo = 0;
				$total_entrada = 0;
				$total_costo_disp = 0;
				$categorias = $this->db->from("categorias")->get();
				foreach($categorias->result() as $key => $row) { 
					$taller = abs($costoTaller['general'][$row->cate_id]['usado'] - $auxProd[$row->cate_id]);
					$entrada = $auxProd[$row->cate_id] + $taller;
					$disponible = $auxFac[$row->cate_id] - $entrada;
					?>
					<tr>
						<td class="vcenter text-center"><?php echo $row->descripcion; ?></td>
						<td class="text-center" id="<?php echo $id_costos[$key] ?>"><?php echo 'B/.'. number_format($auxFac[$row->cate_id], 2, '.', ',') ?></td>
						<td class="text-center" id="<?php echo $id_costos[$key] ?>"><?php echo 'B/.'. number_format($entrada, 2, '.', ',') ?></td>
						<?php 
						$disp =  $disponible;
						if ($disp > 0) { ?>
							<td class="text-center" style="background-color: #5cb85c; color: white;" id="<?php echo $id_costos[$key] ?>"><?php echo 'B/.'. number_format($disp, 2, '.', ',') ?></td>
						<?php }else if($disp < 0){?>
							<td class="text-center" style="background-color: #d9534f; color: white;" id="<?php echo $id_costos[$key] ?>"><?php echo '(B/.'. number_format($disp, 2, '.', ',').')' ?></td>
						<?php }else{?>
							<td class="text-center" id="<?php echo $id_costos[$key] ?>"><?php echo '(B/.'. number_format($disp, 2, '.', ',').')' ?></td>
						<?php }?>
					</tr>
					<?php
					$total_costo += $auxFac[$row->cate_id];					
					$total_entrada += $entrada;
					$total_costo_disp += $auxFac[$row->cate_id] - $entrada;
				}?>
	        </tbody>
	        <tfoot>
	            <tr>
	                <td class="text-center"><strong><?php echo lang('costos') ?></strong></td>
	                <td class="text-center"><?php echo 'B/.'. number_format($total_costo, 2, '.', ',') ?></td>
	                <td class="text-center"><?php echo 'B/.'. number_format($total_entrada, 2, '.', ',') ?></td>
	                <?php 
					if ($total_costo_disp > 0) { ?>
						<td class="text-center" style="background-color: #5cb85c; color: white;"><?php echo 'B/.'. number_format($total_costo_disp, 2, '.', ',') ?></td>
					<?php }else if($total_costo_disp < 0){?>
						<td class="text-center" style="background-color: #d9534f; color: white;"><?php echo '(B/.'. number_format($total_costo_disp, 2, '.', ',').')' ?></td>
					<?php }else{?>
						<td class="text-center"><?php echo '(B/.'. number_format($total_costo_disp, 2, '.', ',').')' ?></td>
					<?php }?>
	            </tr>
	        </tfoot>
	    </table>
	</div>	
</div>


<aside id="historico">
	<section class="content">
		<div class="row">
        	<div class="col-lg-12">
        		<ul class="timeline">
				<?php
				if ($man_id != null && $informe != null) {
					$result = '';
					
					for ($i=0; $i<count($informe); $i++) {

						//INICIAR
						$result = $result.'<li class="time-label pointer"><span class="bg-red">'.lang('presupuesto').'<br>'.$informe[$i]['presupuesto']->pr_fecha.'</span></li>';
						
						//PRESUPUESTO
						$estado = '';
						if ($informe[$i]['presupuesto']->pr_estado == 0) {
							$estado = '<span style="float: right; font-size: 90%;" class="label label-info">'. lang("pendiente") .'</span>';
						} else if ($informe[$i]['presupuesto']->pr_estado == 1) {
							$estado = '<span style="float: right; font-size: 90%;" class="label label-primary">'. lang("aprobada") .'</span>';
						} else if ($informe[$i]['presupuesto']->pr_estado == 2) {
							$estado = '<span style="float: right; font-size: 90%;" class="label label-warning">'. lang("caliente") .'</span>';
						} else if ($informe[$i]['presupuesto']->pr_estado == 3) {
							$estado = '<span style="float: right; font-size: 90%;" class="label label-danger">'. lang("cancelada") .'</span>';
						} else if ($informe[$i]['presupuesto']->pr_estado == 4) {
							$estado = '<span style="float: right; font-size: 90%;" class="label label-danger">'. lang("anulada") .'</span>';
						}

						$result = $result.'<li class="contenido"><i class="fa fa-signal bg-aqua pointer colapsar"></i>'.
											'<div class="timeline-item"><h3 class="timeline-header">'.lang('presupuesto').$estado.'</h3>'.
											'<div class="timeline-body"><table class="table"><thead><tr><th>'.lang('fecha').'</th><th>'.lang('numero.corto').'</th><th>'.lang('importe').'</th><th></th></tr></thead>'.
											'<tbdoy><tr><td>'.$informe[$i]['presupuesto']->pr_fecha.'</td><td>'.$informe[$i]['presupuesto']->pr_numero.'</td><td>'.$informe[$i]['presupuesto']->pr_total.'</td><td>'.
											'<a href="'.base_url().$this->lang->lang().'/presupuestos/editar_presupuesto/'.$informe[$i]['presupuesto']->pr_id.'" target="_blank" title="'.lang('ver').'"><i class="fa fa-eye"></i></a>'.
											'<a href="'.base_url().$this->lang->lang().'/presupuestos/generar_pdf/'.$informe[$i]['presupuesto']->pr_id.'/F" title="'.lang('pdf').'"><i class="fa fa-file-pdf-o"></i></a>'.
											'</td></tr></tbody></table></div></div></li>';
						
						//FACTURAS
						if (count($informe[$i]['facturas']) > 0) {							
							$result = $result.'<li class="contenido"><i class="fa fa-signal bg-aqua pointer colapsar"></i><div class="timeline-item"><h3 class="timeline-header">'.lang('facturas').'</h3><div class="timeline-body"><table class="table"><thead><tr><th>'.lang('fecha').'</th><th>'.lang('estado').'</th><th>'.lang('numero.corto').'</th><th>'.lang('importe').'</th><th></th></tr></thead><tbdoy>';
						
							for ($f=0; $f<count($informe[$i]['facturas']); $f++) {
								$estado = '';
								if ($informe[$i]['facturas'][$f]->fa_estado == 0) {
									$estado = '<span style="font-size: 90%;" class="label label-info">'. lang("pendiente") .'</span>';
								} else if ($informe[$i]['facturas'][$f]->fa_estado == 1) {
									$estado = '<span style="font-size: 90%;" class="label label-primary">'. lang("aprobada") .'</span>';
								} else if ($informe[$i]['facturas'][$f]->fa_estado == 2) {
									$estado = '<span style="font-size: 90%;" class="label label-warning">'. lang("caliente") .'</span>';
								} else if ($informe[$i]['facturas'][$f]->fa_estado == 3) {
									$estado = '<span style="font-size: 90%;" class="label label-danger">'. lang("cancelada") .'</span>';
								} else if ($informe[$i]['facturas'][$f]->fa_estado == 4) {
									$estado = '<span style="font-size: 90%;" class="label label-danger">'. lang("anulada") .'</span>';
								}


								$result = $result.'<tr><td>'.$informe[$i]['facturas'][$f]->fa_fecha.'</td>';
								$result = $result.'<td>'.$estado.'</td>';
								$result = $result.'<td>'.$informe[$i]['facturas'][$f]->fa_numero.'</td>';
								$result = $result.'<td>'.$informe[$i]['facturas'][$f]->fa_total.'</td>';
								$result = $result.'<td><a href="'.base_url().$this->lang->lang().'/facturas/editar_factura/'.$informe[$i]['facturas'][$f]->fa_id.'" target="_blank" title="'.lang('ver').'"><i class="fa fa-eye"></i></a><a href="'.base_url().$this->lang->lang().'/facturas/generar_pdf/'.$informe[$i]['facturas'][$f]->fa_id.'/F" title="'.lang('pdf').'"><i class="fa fa-file-pdf-o"></i></a></td></tr>';
							}
							
							$result = $result.'</tbody></table></div></div></li>';
						}
						
						//RECTIFICATIVAS
						if (count($informe[$i]['rectificativas']) > 0) {
							$result = $result.'<li class="contenido"><i class="fa fa-signal bg-aqua pointer colapsar"></i><div class="timeline-item"><h3 class="timeline-header">'.lang('rectificativas').'</h3><div class="timeline-body"><table class="table"><thead><tr><th>'.lang('fecha').'</th><th>'.lang('estado').'</th><th>'.lang('factura').'</th><th>'.lang('numero.corto').'</th><th>'.lang('importe').'</th><th></th></tr></thead><tbdoy>';
						
							for ($r=0; $r<count($informe[$i]['rectificativas']); $r++) {
								$estado = '';
								if ($informe[$i]['rectificativas'][$r]->fa_estado == 0) {
									$estado = '<span style="font-size: 90%;" class="label label-info">'. lang("pendiente") .'</span>';
								} else if ($informe[$i]['rectificativas'][$r]->fa_estado == 1) {
									$estado = '<span style="font-size: 90%;" class="label label-primary">'. lang("aprobada") .'</span>';
								} else if ($informe[$i]['rectificativas'][$r]->fa_estado == 2) {
									$estado = '<span style="font-size: 90%;" class="label label-warning">'. lang("caliente") .'</span>';
								} else if ($informe[$i]['rectificativas'][$r]->fa_estado == 3) {
									$estado = '<span style="font-size: 90%;" class="label label-danger">'. lang("cancelada") .'</span>';
								} else if ($informe[$i]['rectificativas'][$r]->fa_estado == 4) {
									$estado = '<span style="font-size: 90%;" class="label label-danger">'. lang("anulada") .'</span>';
								}

								$result = $result.'<tr><td>'.$informe[$i]['rectificativas'][$r]->re_fecha.'</td>';
								$result = $result.'<td>'.$estado.'</td>';
								$result = $result.'<td>'.$informe[$i]['rectificativas'][$r]->fa_numero.'</td>';								
								$result = $result.'<td>'.$informe[$i]['rectificativas'][$r]->re_numero.'</td>';
								$result = $result.'<td>'.$informe[$i]['rectificativas'][$r]->re_total.'</td>';
								$result = $result.'<td><a href="'.base_url().$this->lang->lang().'/facturas/editar_factura/'.$informe[$i]['rectificativas'][$r]->fa_id.'" target="_blank" title="'.lang('ver').'"><i class="fa fa-eye"></i></a><a href="'.base_url().$this->lang->lang().'/facturas/generar_pdf/'.$informe[$i]['rectificativas'][$r]->fa_id.'/R" title="'.lang('pdf').'"><i class="fa fa-file-pdf-o"></i></a></td></tr>';
							}
							
							$result = $result.'</tbody></table></div></div></li>';
						}

						//CUADRO PRESUPUESO POR CATEGORIA 
						$result = $result.'<li class="contenido"><i class="fa fa-signal bg-aqua pointer colapsar"></i><div class="timeline-item"><h3 class="timeline-header">Resumen '.lang('presupuesto').'</h3><div class="timeline-body"><table class="table"><thead><tr><th>'.lang('descripcion').'</th><th class="text-center">'.lang('coste').'</th><th class="text-center">'.lang('entradas.aprobadas.compra').'</th><th class="text-center">'.lang('costo.disponible').'</th><th></th></tr></thead><tbdoy>';
						$total_costo = 0;
						$total_entrada = 0;
						$total_costo_disp = 0;
						

						foreach($categorias->result() as $key => $row) {
							$taller = abs( $costoTaller['cotizacion'][$informe[$i]['presupuesto']->pr_id]['categorias'][$row->cate_id]['usado'] - $informe[$i]['categoria'][0][$row->cate_id]);

							$entrada = $informe[$i]['categoria'][0][$row->cate_id] + $taller;
							$disponible = $informe[$i]['categoria'][1][$row->cate_id] - $entrada;

							$result = $result.'<tr><td >'.$row->descripcion.'</td>';
							$result = $result.'<td class="text-center"> B/.'.number_format($informe[$i]['categoria'][1][$row->cate_id], 2, '.', ',').'</td>';
							$result = $result.'<td class="text-center">B/.'. number_format($entrada, 2, '.', ',').'</td>';
							$disp = $disponible;
							if ($disp > 0) { 
								$result = $result.'<td class="text-center" style="background-color: #5cb85c; color: white;"> B/.'. number_format($disp, 2, '.', ',').'</td></tr>';
							}else if($disp < 0){
								$result = $result.'<td class="text-center" style="background-color: #d9534f; color: white;"> B/.'. number_format($disp, 2, '.', ',').'</td></tr>';
							}else{
								$result = $result.'<td class="text-center"> B/.'. number_format($disp, 2, '.', ',').'</td></tr>';
							}

							$total_costo += $informe[$i]['categoria'][1][$row->cate_id];
							$total_entrada += $entrada;
							$total_costo_disp += $informe[$i]['categoria'][1][$row->cate_id] - $entrada;
						}

						$result = $result.'<tr><td >Total de costos</td>';
						$result = $result.'<td class="text-center"> B/.'.number_format($total_costo, 2, '.', ',').'</td>';
						$result = $result.'<td class="text-center">B/.'. number_format($total_entrada, 2, '.', ',').'</td>';
						if ($total_costo_disp > 0) { 
							$result = $result.'<td class="text-center" style="background-color: #5cb85c; color: white;"> B/.'. number_format($total_costo_disp, 2, '.', ',').'</td></tr>';
						}else if($total_costo_disp < 0){
							$result = $result.'<td class="text-center" style="background-color: #d9534f; color: white;"> B/.'. number_format($total_costo_disp, 2, '.', ',').'</td></tr>';
						}else{
							$result = $result.'<td class="text-center"> B/.'. number_format($total_costo_disp, 2, '.', ',').'</td></tr>';
						}

						$result = $result.'</tbody></table></div></div></li>';
						
						//PEDIDOS
						if (count($informe[$i]['pedidos']) > 0) {
							
							$result = $result.'<li class="contenido"><i class="fa fa-archive bg-maroon pointer colapsar"></i><div class="timeline-item"><h3 class="timeline-header">'.lang('pedido').'</h3><div class="timeline-body">';
						
							for ($p=0; $p<count($informe[$i]['pedidos']); $p++) {
								
								if ($informe[$i]['pedidos'][$p]->pe_estado > 0) {		
								
									switch($informe[$i]['pedidos'][$p]->pe_estado) {
										case 0:
											$estado = lang('pendiente');
											break;
										case 1:
											$estado = lang('aprobado');
											break;
										case 2:
											$estado = lang('enviado');
											break;
										case 3:
											$estado = lang('anulado');
											break;
										case 4:
											$estado = lang('completado');
											break;
									}
									
									$result = $result.'<div class="row bottom30"><div class="col-lg-4 col-md-4"><strong>'.lang('entrada').':</strong> <a href="'.base_url().$this->lang->lang().'/almacenes/editar_entrada/'.$informe[$i]['pedidos'][$p]->pe_id.'" target="_blank">'.$informe[$i]['pedidos'][$p]->pe_id.'</a><br><strong>'.lang("estado").':</strong> '.$estado.'</div><div class="col-lg-4 col-md-4"><strong>'.lang('fecha.pedido').':</strong> '.$informe[$i]['pedidos'][$p]->pe_fecha.'<br><strong>'.lang('fecha.aprobado').':</strong> '.$informe[$i]['pedidos'][$p]->fecha_aprobacion.'<br><strong>'.lang('fecha.completado').':</strong> '.$informe[$i]['pedidos'][$p]->fecha_completado.'<br></div><div class="col-lg-4 col-md-4"><strong>'.lang('empleado.pedido').'</strong><p>'.$informe[$i]['pedidos'][$p]->empleado->emp_nombre.' '.$informe[$i]['pedidos'][$p]->empleado->emp_apellido1.' '.$informe[$i]['pedidos'][$p]->empleado->emp_apellido2.'</p><strong>'.lang('empleado.pedido.aprobado').'</strong><p>'.$informe[$i]['pedidos'][$p]->emp_aprobado->emp_nombre.' '.$informe[$i]['pedidos'][$p]->emp_aprobado->emp_apellido1.' '.$informe[$i]['pedidos'][$p]->emp_aprobado->emp_apellido2.'</p><strong>'.lang("empleados.pedido.recepcion").'</strong><p>'.$informe[$i]['pedidos'][$p]->empleados_recepcion.'</p></div></div><div class="row bottom30"><div class="col-lg-6">'.lang('observaciones.empresa').'<br/>'.$informe[$i]['pedidos'][$p]->pe_observaciones_empresa.'</div><div class="col-lg-6">'.lang('observaciones.proveedor').'<br/>'.$informe[$i]['pedidos'][$p]->pe_observaciones_proveedor.'</div></div><table class="table"><thead><tr><th>'.lang('referencia').'</th><th>'.lang('producto').'</th><th>'.lang('caracteristicas').'</th><th>'.lang('cantidad.pedida').'</th><th>'.lang('cantidad.recibida').'</th><th>'.lang('almacen').'</th><th>'.lang('proveedor').'</th></tr></thead><tbdoy>';
									
									for ($pr=0; $pr<count($informe[$i]['pedidos'][$p]->productos); $pr++) {
										$opciones = '';
										$aux = $informe[$i]['pedidos'][$p]->productos[$pr]['product_caracteristicas'];
										if ($aux != '') {
											$aux = explode(';',$aux);
											$aux = array_filter($aux);
											for ($t=0; $t<count($aux); $t++) {
												$aux2 = explode('/',$aux[$t]);
												$aux2 = array_filter($aux2);
												
												$opciones = $opciones.'<span class="opc_product_list"><strong>'.$aux2[0].'</strong>: <span>'.$aux2[1].'</span></span>';	
											}
										}
										
										$result = $result.'<tr><td>'.$informe[$i]['pedidos'][$p]->productos[$pr]['product_referencia'].'</td>';
										$result = $result.'<td>'.$informe[$i]['pedidos'][$p]->productos[$pr]['product_nombre'].'</td>';
										$result = $result.'<td>'.$opciones.'</td>';
										$result = $result.'<td>'.$informe[$i]['pedidos'][$p]->productos[$pr]['cantidad'].'</td>';
										$result = $result.'<td>'.$informe[$i]['pedidos'][$p]->productos[$pr]['recibido'].'</td>';
										
										$alm = '';
										for ($a=0; $a<count($informe[$i]['pedidos'][$p]->productos[$pr]['almacenes']); $a++) {
											if ($informe[$i]['pedidos'][$p]->productos[$pr]['almacenes'][$a][0] == $informe[$i]['pedidos'][$p]->productos[$pr]['product_id']) {
												$alm = $alm.'<div>'.$informe[$i]['pedidos'][$p]->productos[$pr]['almacenes'][$a][1].' - '.$informe[$i]['pedidos'][$p]->productos[$pr]['almacenes'][$a][3].'</div>';
											}
										}
										$result = $result.'<td>'.$alm.'</td>';
										
										
										$result = $result.'<td>'.$informe[$i]['pedidos'][$p]->productos[$pr]['proveedor'].'</td></tr>';
									}
									
									$result = $result.'</tbody></table>';
								}
							
							}
							
							$result = $result.'</div></div></li>';
						}
						
						//TALLER
						if (count($informe[$i]['taller']) > 0) {
							$result = $result.'<li class="contenido"><i class="fa fa-cog bg-orange pointer colapsar"></i><div class="timeline-item"><h3 class="timeline-header">'.lang('taller').'</h3><div class="timeline-body">';
						
							for ($t=0; $t<count($informe[$i]['taller']); $t++) {
								$result = $result.'<div class="row bottom30"><div class="col-lg-4 col-md-4"><a href="'.base_url().$this->lang->lang().'/taller/editar_trabajo_taller/'.$informe[$i]['taller'][$t]->ta_id.'" target="_blank">'.lang('referencia').': '.$informe[$i]['taller'][$t]->ta_id.'</a></div><div class="col-lg-4 col-md-4">'.lang('fecha.inicio').': '.$informe[$i]['taller'][$t]->ta_inicio.'<br>'.lang('fecha.fin').': '.$informe[$i]['taller'][$t]->ta_final.'<br>'.lang('fecha.terminado').': '.$informe[$i]['taller'][$t]->ta_terminado.'</div><div class="col-lg-4 col-md-4">'.lang('observaciones').': '.$informe[$i]['taller'][$t]->ta_observaciones.'</div></div><strong><i class="fa fa-cubes"></i> '.lang('productos.usados.taller').'</strong><table class="table"><thead><tr><th>'.lang('referencia').'</th><th>'.lang('producto').'</th><th>'.lang('caracteristicas').'</th><th>'.lang('cantidad').'</th></tr></thead><tbdoy>';
								
								for ($prt=0; $prt<count($informe[$i]['taller'][$t]->productos); $prt++) {
									$opciones = '';
									$aux = $informe[$i]['taller'][$t]->productos[$prt]['product_caracteristicas'];
									if ($aux != '') {
										$aux = explode(';',$aux);
										$aux = array_filter($aux);
										for ($pct=0; $pct<count($aux); $pct++) {
											$aux2 = explode('/',$aux[$pct]);
											$aux2 = array_filter($aux2);
											
											$opciones = $opciones.'<span class="opc_product_list"><strong>'.$aux2[0].'</strong>: <span>'.$aux2[1].'</span></span>';	
										}
									}
									
									$result = $result.'<tr><td>'.$informe[$i]['taller'][$t]->productos[$prt]['product_referencia'].'</td>';
									$result = $result.'<td>'.$informe[$i]['taller'][$t]->productos[$prt]['product_nombre'].'</td>';
									$result = $result.'<td>'.$opciones.'</td>';
									$result = $result.'<td>'.$informe[$i]['taller'][$t]->productos[$prt]['cantidad'].'</td></tr>';
								}
								
								$result = $result.'</tbody></table>';
								
								$result = $result.'<strong><i class="fa fa-gears"></i> '.lang('trabajos.taller').'</strong><table class="table"><thead><tr><th>'.lang('referencia').'</th><th>'.lang('descripcion').'</th><th>'.lang('ancho').'</th><th>'.lang('alto').'</th><th>'.lang('longitud').'</th><th>'.lang('cantidad').'</th><th>'.lang('trabajos.enviados').'</th><th>'.lang('observaciones').'</th><th>'.lang('reposicion').'</th></tr></thead><tbdoy>';
								//TRABAJOS SIMPLES
								$ta_codigo = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_codigo);
								$ta_codigo = array_filter($ta_codigo);
								$ta_cantid = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_cantidad);
								$ta_cantid = array_filter($ta_cantid);
								$ta_terminad = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_terminados);
								$ta_terminad = array_filter($ta_terminad);
								$ta_ancho = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_ancho);
								$ta_ancho = array_filter($ta_ancho);
								$ta_alto = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_alto);
								$ta_alto = array_filter($ta_alto);
								$ta_longi = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_longitud);
								$ta_longi = array_filter($ta_longi);
								$ta_desc = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_descripcion);
								$ta_desc = array_filter($ta_desc);
								$ta_obsv = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_observaciones);
								$ta_obsv = array_filter($ta_obsv);
								$ta_repos = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_reposiciones);
								$ta_repos = array_filter($ta_repos);
								for ($tc=0; $tc<count($ta_codigo); $tc++) {
									$result = $result.'<tr><td>'.$ta_codigo[$tc].'</td>';
									$result = $result.'<td>'.$ta_desc[$tc].'</td>';
									$result = $result.'<td>'.$ta_ancho[$tc].'</td>';
									$result = $result.'<td>'.$ta_alto[$tc].'</td>';
									$result = $result.'<td>'.$ta_longi[$tc].'</td>';
									$result = $result.'<td>'.$ta_cantid[$tc].'</td>';
									$result = $result.'<td>'.$ta_terminad[$tc].'</td>';
									$result = $result.'<td>'.$ta_obsv[$tc].'</td>';
									$reposicion = '';
									if ($ta_repos[$tc] === 'true') {
										$reposicion = '<i class="fa fa-check"></i>';
									}
									$result = $result.'<td class="text-center">'.$reposicion.'</td></tr>';
								}
								
								//TRABAJOS COMPUESTOS
								$ta_codigo_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_codigo);
								$ta_codigo_comp = array_filter($ta_codigo_comp);
								$ta_cantid_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_cantidad);
								$ta_cantid_comp = array_filter($ta_cantid_comp);
								$ta_terminad_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_terminados);
								$ta_terminad_comp = array_filter($ta_terminad_comp);
								$ta_ancho_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_ancho);
								$ta_ancho_comp = array_filter($ta_ancho_comp);
								$ta_alto_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_alto);
								$ta_alto_comp = array_filter($ta_alto_comp);
								$ta_longi_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_longitud);
								$ta_longi_comp = array_filter($ta_longi_comp);
								$ta_desc_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_descripcion);
								$ta_desc_comp = array_filter($ta_desc_comp);
								$ta_obsv_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_observaciones);
								$ta_obsv_comp = array_filter($ta_obsv_comp);
								$ta_repos_comp = explode(';',$informe[$i]['taller'][$t]->ta_trabajos_compuestos_reposiciones);
								$ta_repos_comp = array_filter($ta_repos_comp);
								for ($tc=0; $tc<count($ta_codigo_comp); $tc++) {
									$aux_ta_codigo_comp = explode('|',$ta_codigo_comp[$tc]);
									$aux_ta_codigo_comp = array_filter($aux_ta_codigo_comp);
									$aux_ta_cantid_comp = explode('|',$ta_cantid_comp[$tc]);
									$aux_ta_cantid_comp = array_filter($aux_ta_cantid_comp);
									$aux_ta_terminad_comp = explode('|',$ta_terminad_comp[$tc]);
									$aux_ta_terminad_comp = array_filter($aux_ta_terminad_comp);
									$aux_ta_ancho_comp = explode('|',$ta_ancho_comp[$tc]);
									$aux_ta_ancho_comp = array_filter($aux_ta_ancho_comp);
									$aux_ta_alto_comp = explode('|',$ta_alto_comp[$tc]);
									$aux_ta_alto_comp = array_filter($aux_ta_alto_comp);
									$aux_ta_longi_comp = explode('|',$ta_longi_comp[$tc]);
									$aux_ta_longi_comp = array_filter($aux_ta_longi_comp);
									$aux_ta_desc_comp = explode('|',$ta_desc_comp[$tc]);
									$aux_ta_desc_comp = array_filter($aux_ta_desc_comp);
									$aux_ta_obsv_comp = explode('|',$ta_obsv_comp[$tc]);
									$aux_ta_obsv_comp = array_filter($aux_ta_obsv_comp);
									$aux_ta_repos_comp = explode('|',$ta_repos_comp[$tc]);
									$aux_ta_repos_comp = array_filter($aux_ta_repos_comp);
									for ($tcc=0; $tcc<count($aux_ta_codigo_comp); $tcc++) {
										if ($tcc == 0) {
											$result = $result.'<tr class="fila_color"><td colspan="9">'.$aux_ta_codigo_comp[$tcc].'</td></tr>';
										} else {
											$result = $result.'<tr><td>'.$aux_ta_codigo_comp[$tcc].'</td>';
											$result = $result.'<td>'.$aux_ta_desc_comp[$tcc-1].'</td>';
											$result = $result.'<td>'.$aux_ta_ancho_comp[$tcc-1].'</td>';
											$result = $result.'<td>'.$aux_ta_alto_comp[$tcc-1].'</td>';
											$result = $result.'<td>'.$aux_ta_longi_comp[$tcc-1].'</td>';
											$result = $result.'<td>'.$aux_ta_cantid_comp[$tcc-1].'</td>';
											$result = $result.'<td>'.$aux_ta_terminad_comp[$tcc-1].'</td>';
											$result = $result.'<td>'.$aux_ta_obsv_comp[$tcc-1].'</td>';
											$reposicion = '';
											if ($aux_ta_repos_comp[$tcc-1] === 'true') {
												$reposicion = '<i class="fa fa-check"></i>';
											}
											$result = $result.'<td class="text-center">'.$reposicion.'</td></tr>';
										}
									}
								}
								
								$result = $result.'</tbody></table>';
							}
							$result = $result.'</div></div></li>';
						}
						
						//TRABAJOS
						if (count($informe[$i]['trabajos']) > 0) {
							$result = $result.'<li class="contenido"><i class="fa fa-cube bg-danger pointer colapsar"></i><div class="timeline-item"><h3 class="timeline-header">'.lang('trabajos').'</h3><div class="timeline-body">';
							
							for ($t=0; $t<count($informe[$i]['trabajos']); $t++) {
								$result = $result.'<div class="row bottom30"><div class="col-lg-4 col-md-4"><a href="'.base_url().$this->lang->lang().'/trabajos/editar_trabajo/'.$informe[$i]['trabajos'][$t]->tr_id.'" target="_blank">'.lang('referencia').': '.$informe[$i]['trabajos'][$t]->tr_id.'</a></div><div class="col-lg-4 col-md-4">'.lang('fecha.inicio').': '.$informe[$i]['trabajos'][$t]->tr_fecha_inicio.'<br>'.lang('fecha.fin').': '.$informe[$i]['trabajos'][$t]->tr_fecha_fin.'</div><div class="col-lg-4 col-md-4">'.lang('observaciones').': '.$informe[$i]['trabajos'][$t]->tr_comentario.'</div></div><strong><i class="fa fa-users"></i> '.lang('empleados.trabajo').'</strong><table class="table"><thead><tr><th>'.lang('nombre').'</th><th>'.lang('dni').'</th><th>'.lang('telefono').'</th><th>'.lang('email').'</th></tr></thead><tbdoy>';
								
								//EMPLEADOS
								for ($ep=0; $ep<count($informe[$i]['trabajos'][$t]->empleados); $ep++) {
									$result = $result.'<tr><td>'.$informe[$i]['trabajos'][$t]->empleados[$ep]['emp_nombre'].'</td>';
									$result = $result.'<td>'.$informe[$i]['trabajos'][$t]->empleados[$ep]['emp_dni'].'</td>';
									$result = $result.'<td>'.$informe[$i]['trabajos'][$t]->empleados[$ep]['emp_telefono'].'</td>';
									$result = $result.'<td>'.$informe[$i]['trabajos'][$t]->empleados[$ep]['emp_email'].'</td></tr>';
								}
								
								$result = $result.'</tbody></table>';
								
								//INSPECCIONES SIMPLES
								if (count($informe[$i]['trabajos'][$t]->simples) > 0) {
									$result = $result.'<div class="bottom30"><strong><i class="fa fa-file-o"></i> '.lang('inspecciones').'</strong></div>';
									for ($ti=0; $ti<count($informe[$i]['trabajos'][$t]->simples); $ti++) {
										$cont_preg = 0;
										$cont_resp_marcas = 0;
										
										//TITULOS DE LAS PREGUNTAS
										$titulos = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->in_titulo);
										$titulos = array_filter($titulos);
										
										//DESCRIPCIONES DE LAS PREGUNTAS
										$descripciones = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->in_descripcion);
										$descripciones = array_filter($descripciones);
										
										//ANCHOS DE LAS PREGUNTAS
										$anchos = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->in_ancho);
										$anchos = array_filter($anchos);
										
										//ALTOS DE LAS PREGUNTAS
										$altos = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->in_alto);
										$altos = array_filter($altos);
										
										//CANTIDADES DE LAS PREGUNTAS
										$cantidades = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->in_cantidad);
										$cantidades = array_filter($cantidades);
										
										//MONTOS DE LAS PREGUNTAS
										$montos = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->in_monto);
										$montos = array_filter($montos);
										
										//PREGUNTAS
										$preguntas = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->in_preguntas);
										$preguntas = array_filter($preguntas);
										
										//RESPUESTAS
										$respuestas = explode('/',$informe[$i]['trabajos'][$t]->simples[$ti]->in_respuestas);
										$respuestas = array_filter($respuestas);
										
										//RESPUESTAS MARCADAS
										if (isset($informe[$i]['trabajos'][$t]->simples[$ti]->in_resp_marcas)) {
											$resp_marcas = explode('/',$informe[$i]['trabajos'][$t]->simples[$ti]->in_resp_marcas);
											$resp_marcas = array_filter($resp_marcas);	
										}
										
										$result = $result.'<div class="row"><div class="col-lg-12"><div class="box box-primary collapsed-box box-solid"><div class="box-header with-border"><h6 class="box-title">'.$informe[$i]['trabajos'][$t]->simples[$ti]->in_nombre.'</h6><div class="box-tools pull-right"><i class="fa fa-minus minimizar pointer"></i></div></div><div class="box-body"><div class="row"><div class="col-lg-4 col-md-4">'.lang('empleado.edito.inspeccion').':<br/>'.$informe[$i]['trabajos'][$t]->simples[$ti]->empleado->emp_nombre.' '.$informe[$i]['trabajos'][$t]->simples[$ti]->empleado->emp_apellido1.' '.$informe[$i]['trabajos'][$t]->simples[$ti]->empleado->emp_apellido2.'</div><div class="col-lg-4 col-md-4">'.lang('fecha.inicio').':<br/>'.$informe[$i]['trabajos'][$t]->simples[$ti]->ti_fecha_inicio.'</div><div class="col-lg-4 col-md-4">'.lang('fecha.fin').':<br/>'.$informe[$i]['trabajos'][$t]->simples[$ti]->ti_fecha_fin.'</div></div><br/>';
										for ($tt=0; $tt<count($titulos); $tt++) {
											
											$result = $result.'<div class=""><div class="row">';
											
											$result = $result.'<div class="col-lg-1"><div class="form-group "><label>'.lang("tipo").'</label><input class="form-control" type="text" value="'.$titulos[$tt].'" disabled></div></div>';
											$result = $result.'<div class="col-lg-2"><div class="form-group "><label>'.lang("descripcion").'</label><input class="form-control" type="text" value="'.$descripciones[$tt].'" disabled></div></div>';
											$result = $result.'<div class="col-lg-1"><div class="form-group "><label>'.lang("ancho").'</label><input class="form-control" type="text" value="'.$anchos[$tt].'" disabled></div></div>';
											$result = $result.'<div class="col-lg-1"><div class="form-group "><label>'.lang("alto").'</label><input class="form-control" type="text" value="'.$altos[$tt].'" disabled></div></div>';
											$result = $result.'<div class="col-lg-1"><div class="form-group "><label>'.lang("cantidad").'</label><input class="form-control" type="text" value="'.$cantidades[$tt].'" disabled></div></div>';
											$result = $result.'<div class="col-lg-1"><div class="form-group "><label>'.lang("monto").'</label><input class="form-control" type="text" value="'.$montos[$tt].'" disabled></div></div>';
										
											//PREGUNTAS SEPARADAS POR TITULOS
											$preg_final = explode(',',$preguntas[$tt]);
											$preg_final = array_filter($preg_final);
											
											
											//RESPUESTAS SEPARADAS POR TITULOS
											$resp_final = explode(';',$respuestas[$tt]);
											$resp_final = array_filter($resp_final);
											
											//RESPUESTAS MARCAS SEPARADAS POR TITULO
											if (isset($resp_marcas[$tt])) {
												$resp_marcas_final = explode(';',$resp_marcas[$tt]);
												$resp_marcas_final = array_filter($resp_marcas_final);
											}
											
											
											
											for ($pf=0; $pf<count($preg_final); $pf++) {
												
												$cont_preg++;
												
												$result = $result.'<div class="col-lg-1"><div class="form-group "><label>'.$preg_final[$pf].'</label><input class="form-control" type="text" value="'.$resp_final[$pf].'" disabled></div></div>';
												
												$cont_resp_marcas++;
												
												//RESPUESTAS SEPARADAS POR PREGUNTAS
												/*$resp = explode(',',$resp_final[$pf]);
												$resp = array_filter($resp);
												for ($r=0; $r<count($resp); $r++) {
													$active = '';
													$check = '';
													if (isset($resp_marcas_final[$pf])) {
														if ($resp[$r] == $resp_marcas_final[$pf]) {
															$cont_resp_marcas++;
															$active = 'active';
															$check = '<i class="fa fa-check"></i>';
														}	
													}
													
													$result = $result.'<div class="'.$active.'">'.$resp[$r].' '.$check.'</div>';
												}*/
											}
											$result = $result.'</div></div>';
										}
										
										//AÑADIMOS COMENTARIO SI EXIST
										if ($informe[$i]['trabajos'][$t]->simples[$ti]->ti_comentario != '') {
											$result = $result.'<h6 class="timeline-header">'.lang('comentario').'</h6><div class="timeline-body">'.$informe[$i]['trabajos'][$t]->simples[$ti]->ti_comentario.'</div>';
										}
										
										//AÑADIMOS LAS FOTOS
										if (count($informe[$i]['trabajos'][$t]->simples[$ti]->fotos) > 0) {
											$result = $result.'<h6 class="timeline-header">'.lang('fotos').'</h6><div class="timeline-body row fot_incid">';
											for ($f=0; $f<count($informe[$i]['trabajos'][$t]->simples[$ti]->fotos); $f++) {
												$result = $result.'<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">'.$informe[$i]['trabajos'][$t]->simples[$ti]->fotos[$f].'</div>';	
											}
											$result = $result.'</div>';
										}
										
										//BARRA PROGRESO
										if ($informe[$i]['trabajos'][$t]->tr_estado == 1) {
											$progreso = ($cont_resp_marcas*100)/$cont_preg;
											if($progreso > 50) { $class = 'progress-bar-success'; } else { $class = 'progress-bar-warning'; }
											$result = $result.'<div class="timeline-body"><div class="progress progress-striped active"><div class="progress-bar '.$class.'" role="progressbar" aria-valuenow="'.$progreso.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$progreso.'%"><span>'.round($progreso).'% '.lang("completado").'</span></div></div></div>';
										}
										
										$result = $result.'</div></div></div></div>';
									}
								}
								
								//INSPECCIONES AVANZADAS
								if (count($informe[$i]['trabajos'][$t]->avanzadas) > 0) {
									$result = $result.'<div class="bottom30"><strong><i class="fa fa-file-o"></i> '.lang('inspecciones.avanzadas').'</strong></div>';
									for ($ti=0; $ti<count($informe[$i]['trabajos'][$t]->avanzadas); $ti++) {
										$cont_resp = 0;
										$cont_resp_marcas = 0;
										
										//EDIFICIOS
										$edificio = explode(';',$informe[$i]['trabajos'][$t]->avanzadas[$ti]->inv_edificio);
										$edificio = array_filter($edificio);
										
										//PLANTAS
										$planta = explode('|',$informe[$i]['trabajos'][$t]->avanzadas[$ti]->inv_planta);
										$planta = array_filter($planta);
										
										//PRODUCTO
										$producto = explode('^',$informe[$i]['trabajos'][$t]->avanzadas[$ti]->inv_producto);
										$producto = array_filter($producto);
										
										//DESCRIPCIONES DE LAS PREGUNTAS
										$descripcion = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->inv_descripcion);
										$descripcion = array_filter($descripcion);
										
										//ANCHOS DE LAS PREGUNTAS
										$ancho = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->inv_ancho);
										$ancho = array_filter($ancho);
										
										//ALTOS DE LAS PREGUNTAS
										$alto = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->inv_alto);
										$alto = array_filter($alto);
										
										//CANTIDADES DE LAS PREGUNTAS
										$cantidade = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->inv_cantidad);
										$cantidade = array_filter($cantidad);
										
										//MONTOS DE LAS PREGUNTAS
										$monto = explode(';',$informe[$i]['trabajos'][$t]->simples[$ti]->inv_monto);
										$monto = array_filter($monto);
										
										//RESPUESTA
										$respuesta = explode('^',$informe[$i]['trabajos'][$t]->avanzadas[$ti]->inv_respuesta);
										$respuesta = array_filter($respuesta);
										
										//RESPUESTAS MARCADAS
										if (isset($informe[$i]['trabajos'][$t]->avanzadas[$ti]->inv_resp_marca)) {
											$respuesta_marcas = explode('^',$informe[$i]['trabajos'][$t]->avanzadas[$ti]->inv_resp_marca);
											$respuesta_marcas = array_filter($respuesta_marcas);
										}
										
										$result = $result.'<div class="row"><div class="col-lg-12"><div class="box box-primary collapsed-box box-solid"><div class="box-header with-border"><h6 class="box-title">'.$informe[$i]['trabajos'][$t]->avanzadas[$ti]->inv_nombre.'</h6><div class="box-tools pull-right"><i class="fa fa-minus minimizar pointer"></i></div></div><div class="box-body"><div class="row"><div class="col-lg-4 col-md-4">'.lang('empleado.edito.inspeccion').':<br/>'.$informe[$i]['trabajos'][$t]->avanzadas[$ti]->empleado->emp_nombre.' '.$informe[$i]['trabajos'][$t]->avanzadas[$ti]->empleado->emp_apellido1.' '.$informe[$i]['trabajos'][$t]->avanzadas[$ti]->empleado->emp_apellido2.'</div><div class="col-lg-4 col-md-4">'.lang('fecha.inicio').':<br/>'.$informe[$i]['trabajos'][$t]->avanzadas[$ti]->tia_fecha_inicio.'</div><div class="col-lg-4 col-md-4">'.lang('fecha.inicio').':<br/>'.$informe[$i]['trabajos'][$t]->avanzadas[$ti]->tia_fecha_fin.'</div></div><br/>';
										for ($tt=0; $tt<count($edificio); $tt++) {
											$result = $result.'<div class="box box-success collapsed-box box-solid"><div class="box-header with-border"><h6 class="box-title titulo">'.$edificio[$tt].'</h6><div class="box-tools pull-right"><i class="fa fa-minus minimizar pointer"></i></div></div><div class="box-body"><div class="">';
										
											$aux_planta = explode(';',$planta[$tt]);
											$aux_planta = array_filter($aux_planta);
											
											$aux_producto = explode('|',$producto[$tt]);
											$aux_producto = array_filter($aux_producto);
											
											$aux_respuesta = explode('|',$respuesta[$tt]);
											$aux_respuesta = array_filter($aux_respuesta);
											
											//RESPUESTAS MARCADAS
											if (isset($respuesta_marcas[$tt])) {
												$aux_respuesta_marcas = explode('|',$respuesta_marcas[$tt]);
												$aux_respuesta_marcas = array_filter($aux_respuesta_marcas);
											}
											
											for ($pf=0; $pf<count($aux_planta); $pf++) {
												$result = $result.'<div class="box box-sucess collapsed-box"><div class="box-header with-border"><h6 class="box-title">'.$aux_planta[$pf].'</h6><div class="box-tools pull-right"><i class="fa fa-minus minimizar pointer"></i></div></div><div class="box-body row">';
												
												$aux2_producto = explode(';',$aux_producto[$pf]);
												$aux2_producto = array_filter($aux2_producto);
												$aux2_respuesta = explode('}',$aux_respuesta[$pf]);
												$aux2_respuesta = array_filter($aux2_respuesta);
												if (isset($aux_respuesta_marcas[$pf])) {
													$aux2_respuesta_marcas = explode('}',$aux_respuesta_marcas[$pf]);
													$aux2_respuesta_marcas = array_filter($aux2_respuesta_marcas);
												}
												
												for ($po=0; $po<count($aux2_producto); $po++) {
													$result = $result.'<div class="col-lg-12"><div class="">'.$aux2_producto[$po].'<div class="box-body">';
													
													$aux3_respuesta = explode(';',$aux2_respuesta[$po]);
													$aux3_respuesta = array_filter($aux3_respuesta);
													if (isset($aux2_respuesta_marcas[$po])) {
														$aux3_respuesta_marcas = explode(';',$aux2_respuesta_marcas[$po]);
														$aux3_respuesta_marcas = array_filter($aux3_respuesta_marcas);
													}
													
													for ($py=0; $py<count($aux3_respuesta); $py++) {
														$active = '';
														$check = '';
														$cont_resp++;
														if (isset($aux3_respuesta_marcas[$py])) {
															if ($aux3_respuesta[$py] == $aux3_respuesta_marcas[$py]) {
																$cont_resp_marcas++;
																$active = 'active';
																$check = '<i class="fa fa-check"></i>';
															}	
														}
														
														$result = $result.'<div class="'.$active.'">'.$aux3_respuesta[$py].' '.$check.'</div>';
													}
													
													$result = $result.'</div></div></div>';
												}
												
												$result = $result.'</div></div>';
											}
											$result = $result.'</div></div></div>';
										}
										
										//AÑADIMOS COMENTARIO SI EXIST
										if ($informe[$i]['trabajos'][$t]->avanzadas[$ti]->tia_comentario != '') {
											$result = $result.'<h6 class="timeline-header">'.lang('comentario').'</h6><div class="timeline-body">'.$informe[$i]['trabajos'][$t]->avanzadas[$ti]->tia_comentario.'</div>';
										}
										
										//AÑADIMOS LAS FOTOS
										if (count($informe[$i]['trabajos'][$t]->avanzadas[$ti]->fotos) > 0) {
											$result = $result.'<h6 class="timeline-header">'.lang('fotos').'</h6><div class="timeline-body row fot_incid">';
											for ($f=0; $f<count($informe[$i]['trabajos'][$t]->avanzadas[$ti]->fotos); $f++) {
												$result = $result.'<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">'.$informe[$i]['trabajos'][$t]->avanzadas[$ti]->fotos[$f].'</div>';	
											}
											$result = $result.'</div>';
										}
										
										//BARRA PROGRESO
										if ($informe[$i]['trabajos'][$t]->tr_estado == 1) {
											$progreso = ($cont_resp_marcas*100)/$cont_resp;
											$result = $result.'<div class="timeline-body"><div class="progress progress-striped active"><div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="'.$progreso.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$progreso.'%"><span>'.round($progreso).'% '.lang("completado").'</span></div></div></div>';
										}
										
										$result = $result.'</div></div></div></div>';
									}
								}
							}
							$result = $result.'</div></div></li>';
						}
					}
					
					echo $result;
				}
				?>
				</ul>
			</div>
		</div>
	</section>
</aside>

<?php } ?>

<script>
	$(document).ready(function() {
		//DATOS EMPLEADO DE LA SESSION
		var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
		var su_id = '<?php echo $this->session->userdata("su_id") ?>';
		var emp_editar = '<?php echo $this->session->userdata("emp_editar") ?>';
		
		//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#mantenimiento .form-group, #mantenimiento .input-group").removeClass("has-error has-feedback");
	    $('#mantenimiento .glyphicon-remove').hide();
	    $("#mantenimiento .text-danger").html('');
		
		//OPCIONES GENERALES
		if (emp_editar == 0 || emp_tipo == 1) {
			$('#mantenimiento *').attr('readonly', true);
			$('#mantenimiento select').attr('disabled', true);
			$('#mantenimiento #btn_guardar').hide();
		}
		
		var primera_vez = false;
		var primera_vez_clientes = false;
		var man_id = '<?php echo isset($man_id)?$man_id:"" ?>';
		
		//RELLENAMOS LOS PAISES
		function paises() {
			$.post(
				"<?php echo base_url().$this->lang->lang() ?>/sucursales/paises",
				{'buscar':''},
				function(data){
					$.each(data,function(indice) {
						//PAIS
						if (data[indice]["id_countries"] == '<?php echo isset($man_pais)?$man_pais:"" ?>') {
							$('#paises_pais').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
						} else if (data[indice]["id_countries"] == $('#sucursales option:selected').attr('pais')) {
							$('#paises_pais').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
						} else {
							$('#paises_pais').append('<option value="'+data[indice]["id_countries"]+'">'+data[indice]["name"]+'</option>');
						}
					});
					
					if (latitud != '') {
						buscar(latitud + ',' + longitud);
					} else {
						buscar($("#mantenimiento #paises_pais option:selected").html());
					}
					
					primera_vez = true;
					if ($('#man_latitud').val() == '') {
						direcciones();
					}
				}, "json");
		}
		
		//RELLENAMOS LOS CLIENTES
		function clientes() {
			$.post(
				"<?php echo base_url().$this->lang->lang() ?>/clientes/clientes_sucursal",
				{'su_id':$('#sucursales option:selected').val()},
				function(data){
					$('#clientes').html('');
					
					$.each(data,function(indice) {
						if (data[indice]["cl_id"] == '<?php echo isset($cl_id)?$cl_id:"" ?>') {
							$('#clientes').append('<option value="'+data[indice]["cl_id"]+'" selected>'+data[indice]["cl_nombre"]+'</option>');
						} else {
							$('#clientes').append('<option value="'+data[indice]["cl_id"]+'">'+data[indice]["cl_nombre"]+'</option>');
						}
					});
                                        
                                        $('#clientes').selectpicker('refresh');
					
					paises();
				}, "json"
			);
		}
		
		//RELLENAMOS LAS SUCURSALES
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
			{'buscar':''},
			function(data){
				$.each(data,function(indice) {					
						if (data[indice]["su_id"] == '<?php echo isset($su_id)?$su_id:"" ?>') {
							$('#sucursales').append('<option pais="'+data[indice]["su_pais"]+'" value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
						} else if (data[indice]["su_id"] == su_id) {
							$('#sucursales').append('<option pais="'+data[indice]["su_pais"]+'" value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
						} else if (data[indice]["su_estado"] == 1) {
							$('#sucursales').append('<option pais="'+data[indice]["su_pais"]+'" value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
						}
				});
				
				primera_vez_clientes = true;
				clientes();
                                $('#clientes').selectpicker();
			}, "json"
		);
		
		//CARGAMOS LA INFORMACION DEL CLIENTE
		$("#sucursales").change(function () {
			if (primera_vez_clientes) {
				clientes();
			}	
		}).trigger('change');
		
		//MAPA		
		var elevator;
		var map;
		var marker = null;
		var geocoder = new google.maps.Geocoder();
		var latitud = '<?php echo isset($man_latitud)?$man_latitud:"" ?>';
		var longitud = '<?php echo isset($man_longitud)?$man_longitud:"" ?>';
		var direccion = '';
		
		function coords(lati, longi) {
			jQuery('#man_latitud').val(lati.toFixed(8));
			jQuery('#man_longitud').val(longi.toFixed(8));
		}
		
		function buscar(direcc) {
			var addressField = direcc;
			geocoder.geocode(
				{'address': addressField}, 
				function(results, status) { 
					if (status == google.maps.GeocoderStatus.OK) {
						var loc = results[0].geometry.location;
						// use loc.lat(), loc.lng()
						
						var zom = 10;
						if (direccion.length >= 8 && direccion.length < 15) {
							zom = 12;
						} else if (direccion.length >= 15 && direccion.length < 22) {
							zom = 13;
						} else if (direccion.length >= 22 || $('#man_latitud').val() != '') {
							zom = 16;
						}
						
						var mapOptions = {
							zoom: zom,
							center: loc,
							mapTypeId: 'roadmap',
							scrollwheel: false
						}
						map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
						
						var center = new google.maps.LatLng(loc.lat(), loc.lng());
						map.panTo(center);
						
						marker = new google.maps.Marker({
							position: loc,
							map: map,
							icon: "<?php echo base_url() ?>img/pointer-view.png"
						});
						elevator = new google.maps.ElevationService();
						google.maps.event.addListener(map, 'click', getElevation);
						coords(loc.lat(), loc.lng());
					} else {
						bootbox.alert('<?php echo lang("direccion.ko") ?>');
					}
				}
			);
		}
		
		$('#man_buscar').click(function(e){
			e.preventDefault();
			if($('#input_buscar').val() != '') {
				buscar($('#input_buscar').val());
			}
		});
		
		//Detectamos el enter en el buscador de direcciones
		$('#input_buscar').keypress(function(e) {
			if(e.which == 13 && $('#input_buscar').val() != '') {
				buscar($('#input_buscar').val());
			}
		});

		function getElevation(event) {
			if (emp_editar == 1) {
				var locations = [];

				// Retrieve the clicked location and push it on the array
				var clickedLocation = event.latLng;
				locations.push(clickedLocation);

				// Create a LocationElevationRequest object using the array's one value
				var positionalRequest = {
					'locations': locations
				}

				// Initiate the location request
				elevator.getElevationForLocations(positionalRequest, function(results, status) {
					marker.setMap(null);
					marker = new google.maps.Marker({
						position: clickedLocation,
						map: map,
						icon: "<?php echo base_url() ?>img/pointer-view.png"
					});
				});
				coords(event.latLng.lat(), event.latLng.lng());
			}
		}
		
		//BUSCADORES DE DIRECCIONES EN EL MAPA
		function direcciones(){
			direccion = '';
			
			if (jQuery('#man_direccion').val() != '') {
				direccion = jQuery('#man_direccion').val() + ', ';
			}
			
			if (jQuery('#man_postal').val() != '') {
				direccion = direccion + jQuery('#man_postal').val() + ', ';
			}
			
			if (jQuery('#man_localidad').val() != '') {
				direccion = direccion + jQuery('#man_localidad').val() + ', ';
			}
			
			if (jQuery('#man_provincia').val() != '') {
				direccion = direccion + jQuery('#man_provincia').val() + ', ';
			}
			
			direccion = direccion + jQuery('#mantenimiento #paises_pais option:selected').html();
			$('#input_buscar').val('');
			$('#input_buscar').val(direccion);
		}
		
		$("#mantenimiento #paises_pais").change(function () {
			if (primera_vez) {
				direcciones();
				buscar(direccion);
			}
		}).trigger('change');
		
		jQuery('#man_direccion, #man_postal, #man_localidad, #man_provincia').focusout(function(){
			direcciones();
			buscar(direccion);
		});
		
		//GUARDAR
		$('#btn_guardar').click(function(e){
			e.preventDefault();
			//LOADING BOTON
			var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
		 	//l.start();
		 	
		 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    	$("#mantenimiento .form-group, #mantenimiento .input-group").removeClass("has-error has-feedback");
	    	$('#mantenimiento .glyphicon-remove').hide();
	    	$("#mantenimiento .text-danger").html('');
		 	
		 	var cliente_id = $('#clientes option').size();
		 	if (cliente_id > 0) {
		 		var datos = new Array();
			 	
				datos = {
					'cl_id'			:	$('#clientes option:selected').val(),
					'man_contacto'	:	$('#man_contacto').val(),
					'man_telefono'	:	$('#man_telefono').val(),
					'man_nombre'	:	$('#man_nombre').val(),
					'man_direccion'	:	$('#man_direccion').val(),
					'man_barriada'	:	$('#man_barriada').val(),
					'man_postal'	:	$('#man_postal').val(),
					'man_localidad'	:	$('#man_localidad').val(),
					'man_provincia'	:	$('#man_provincia').val(),
					'man_pais'		:	$('#paises_pais option:selected').val(),
					'man_latitud'	:	$('#man_latitud').val(),
					'man_longitud'	:	$('#man_longitud').val(),
					'man_id'		:	man_id,
					'man_email'		:	$('#man_email').val(),
					'man_observaciones' : $('#man_observaciones').val()
				};
			 	
			 	$.ajax({
					type: "POST",
					dataType: "json",
				  	url: "<?php echo base_url().$this->lang->lang() ?>/mantenimientos/guardar_editar",
				  	data: datos,
				  	success: function(data) {
				  		if (data.sql) {
				  			//MENSAJES ALERTA PARA EDITAR O CREAR
				  			if (man_id != '') {
								if (!data.editar) {
									bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
									//REDIRIGIMOS A LOS 5 SEGUNDOS
									setTimeout("location.href='"+location+"'", 5000);
								}
							} else {
								if (!data.crear) {
									bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
									//REDIRIGIMOS A LOS 5 SEGUNDOS
									setTimeout("location.href='"+location+"'", 5000);
								}
							}
							
							//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
			  				if (data.status) {
			  					//TODO CORRECTO
								//SI ES NUEVA SUCURSAL
								if (man_id == '') {
									$('#mantenimiento input').val('');
									bootbox.dialog({
										message: "<?php echo lang('mantenimiento.nuevo.ok') ?>",
										buttons: {
											success: {
												label: "<?php echo lang('nuevo') ?>",
												className: "btn-success",
												callback: function(result) {
													var url = '<?php echo base_url().$this->lang->lang() ?>/mantenimientos/nuevo_mantenimiento';
													$(location).attr('href',url);
												}
											},
											main: {
												label: "<?php echo lang('mantenimientos') ?>",
												className: "btn-primary",
												callback: function(result) {
													var url = '<?php echo base_url().$this->lang->lang() ?>/mantenimientos';
													$(location).attr('href',url);
												}
											}
										}
									});
								} else {
								//SI ES UNA SUCURSAL EDITADA
									bootbox.alert('<?php echo lang("mantenimiento.editado.ok") ?>');
								}
			  				} else {
								//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
								//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
								if (data.errors.length > 0) {
									$.each(data.errors, function (ind, elem) {
										$.each(data.errors[ind], function (ind2, elem2) {
											//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
											$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
											$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
											$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
										}); 
									});
								}
							}
				  		} else {
							bootbox.alert('<?php echo lang("error.ajax") ?>');
						}
						l.stop();
				  	},
				  	error: function(XMLHttpRequest, textStatus, errorThrown) {
				   		bootbox.alert('<?php echo lang("error.ajax") ?>');
				   		l.stop();
				  	}
				});
		 	} else {
				$('#clientes').next().next().html("<?php echo lang('requerido') ?>");
				$("#clientes").parent().addClass("has-error has-feedback");
	    		$('#clientes .glyphicon-remove').next().show();
	    		l.stop();
			}
	 	});
	 	
	 	//MANDAMOS LA FACTURA POR EMAIL
		$('#mandar_email').click(function(e){
			e.preventDefault();
						
			//LOADING BOTON
			var l = Ladda.create( document.querySelector( "#mandar_email" ) );
			l.start();
				 	
				 	var man_email = '<?php echo isset($man_email)?$man_email:"" ?>';
				 	var cl_email = '<?php echo isset($cl_email)?$cl_email:"" ?>';
				 	var emails = '<?php echo isset($cl_emails)?$cl_emails:"" ?>';
				 	var emails_info = '<?php echo isset($cl_emails_info)?$cl_emails_info:"" ?>';
				 	
					emails = emails.split(";");
					emails_info = emails_info.split(";");
				 	
				 	var listar = '<div class="row"><div class="col-lg-8 col-xs-12"><strong><?php echo lang("email") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("no") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("para") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("cc") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("cco") ?></strong></div></div><br><div class="row"><div class="col-lg-8 col-xs-12">'+man_email+'</div><div class="col-lg-1 col-xs-4 text-center"><input type="radio" name="email0" value="0"></div><div class="col-lg-1 col-xs-4 text-center"><input type="radio" name="email0" value="1" checked></div><div class="col-lg-1 col-xs-4 text-center"><input type="radio" name="email0" value="2"></div><div class="col-lg-1 col-xs-4 text-center"><input type="radio" name="email0" value="3"></div></div>';
				 	
				 	listar = listar + '<div class="row"><div class="col-lg-8 col-xs-12">'+cl_email+'</div><div class="col-lg-1 col-xs-4 text-center"><input type="radio" name="email1" value="0" checked></div><div class="col-lg-1 col-xs-4 text-center"><input type="radio" name="email1" value="1"></div><div class="col-lg-1 col-xs-4 text-center"><input type="radio" name="email1" value="2"></div><div class="col-lg-1 col-xs-4 text-center"><input type="radio" name="email1" value="3"></div></div>';
				 	
				 	for (var i=0; i<emails.length-1;i++) {
						listar = listar + '<div class="row">';
						
						listar = listar + '<div class="col-lg-8 col-xs-12">';
						listar = listar + emails[i];
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+(i+2)+'" value="0" checked>';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+(i+2)+'" value="1">';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+(i+2)+'" value="2">';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+(i+2)+'" value="3">';
						listar = listar + '</div>';
						
						var info = emails_info[i];
						
						listar = listar + '<div class="col-lg-12">';
						listar = listar + '<p class="text-muted">' + info + '</p>';
						listar = listar + '</div>';
						
						listar = listar + '</div>';
					}
					
					bootbox.dialog({
						title: "<?php echo lang('emails.cliente') ?>",
						message: listar,
						buttons: {
							success: {
								label: "<?php echo lang('cancelar') ?>",
								className: "btn-default",
								callback: function(result) {
									l.stop();
								}
							},
							danger: {
								label: "<i class='fa fa-envelope'></i> <?php echo lang('enviar') ?>",
								className: "btn-primary",
								callback: function(result) {
									var datos = new Array();
									var cont = 0;
									$('.modal-content .modal-body .row').each(function(index) {
										if (index > 0) {
											var email = $(this).find('div:eq(0)').html();
											var value = '';
											
											$('.modal-content .modal-body .row:eq('+index+') input').each(function(){
												if ($(this).is(':checked')) {
													value = $(this).val();
													if (value > 0) {
														cont++;
													}
												}
											});
											
											var aux = {
												'email'	:	email,
												'value'	:	value
											};
											datos[datos.length] = aux;
										}
									});
									
									if (cont > 0) {
										$.post(
										    "<?php echo base_url().$this->lang->lang() ?>/mantenimientos/enviar_email",
										    {'man_id':man_id,'datos':datos},
										    function(data){
										    	bootbox.alert('<?php echo lang("mantenimientos.email.ok") ?>');
										    	l.stop();
										    }
										);
									} else {
										bootbox.alert('<?php echo lang("emails.seleccion") ?>');
										l.stop();
									}
								}
							}
						}
					});
				});
				
		//COLAPSAR Y EXPANDIR
		$("#historico").on('click', '.time-label', function () {
			var contenido = $(this);
			var parar = true;
			while (parar) {
				contenido = contenido.next();
				if (contenido.hasClass('contenido')) {
					//COLAPSAR O EXPANDIR
					contenido.slideToggle(500, function () {
						
					});
				} else {
					parar = false;
				}
			}
		});
		
		$("#historico").on('click', '.colapsar', function () {
			//BOTON EXPANDIR Y COLAPSAR
			$header = $(this);
			//CONTENIDO
			$content = $header.parent().find('.timeline-item');
			//COLAPSAR O EXPANDIR
			$content.slideToggle(500, function () {
				
			});
		});
		
		//FANCYBOX
		$(".fancybox").fancybox({
			helpers : {
				title : false
			},
			padding: 0,
			openEffect : 'elastic',
			openSpeed  : 150,
			closeEffect : 'elastic',
			closeSpeed  : 150,
			closeClick : false
		});
		
		$("#historico").on('click', '.minimizar', function () {
			//BOTON EXPANDIR Y COLAPSAR
			header = $(this);
			//CONTENIDO
			content = header.parent().parent().next();
			//COLAPSAR O EXPANDIR
			content.slideToggle(500, function () {
				if (header.hasClass('fa-minus')) {
					header.removeClass('fa-minus');
					header.addClass('fa-plus');
				} else {
					header.removeClass('fa-plus');
					header.addClass('fa-minus');
				}
			});
		});
	});
</script>