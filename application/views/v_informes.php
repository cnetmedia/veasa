<div class="nav-tabs-custom" id="informes">
	<ul class="nav nav-tabs">
    	<li class="active">
        	<a href="#talleres" class="talleres_btn" data-toggle="tab"><?php echo lang('talleres') ?></a>
        </li>
        <li class="">
        	<a href="#salidas" class="salidas_btn" data-toggle="tab"><?php echo lang('salidas') ?></a>
        </li>
        <li class="">
        	<a href="#entradas" class="entradas_btn" data-toggle="tab"><?php echo lang('entradas') ?></a>
        </li>
        <li class="">
        	<a href="#reposiciones" class="reposiciones_btn" data-toggle="tab"><?php echo lang('reposiciones') ?></a>
        </li>
        <li class="">
        	<a href="#trabajos" class="trabajos_btn" data-toggle="tab"><?php echo lang('trabajos') ?></a>
        </li>
        <li class="">
        	<a href="#presupuestos" class="presupuestos_btn" data-toggle="tab"><?php echo lang('presupuestos') ?></a>
        </li>
        <li class="">
        	<a href="#facturas" class="facturas_btn" data-toggle="tab"><?php echo lang('facturas') ?></a>
        </li>
        <li class="">
        	<a href="#bitacora" class="bitacora_btn" data-toggle="tab"><?php echo lang('bitacora') ?></a>
        </li>
    </ul>
        
    <div class="tab-content">
    	<div class="tab-pane active" id="talleres">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="talleres_table" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th width="3%">ID</th>
                                <th width="20%"><?php echo lang("fecha") ?></th>
                                <th><?php echo lang("observaciones") ?></th>
                                <th width="10%"><?php echo lang("presupuesto") ?></th>
                                <th width="20%"><?php echo lang("mantenimiento") ?></th>
                                <th width="5%"><?php echo lang("estado") ?></th>
                            </tr>
                        </thead>
                                                
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th><?php echo lang("fecha") ?></th>
                                <th><?php echo lang("observaciones") ?></th>
                                <th><?php echo lang("presupuesto") ?></th>
                                <th><?php echo lang("mantenimiento") ?></th>
                                <th><?php echo lang("estado") ?></th>
                            </tr>
                        </tfoot>
                        
                        <tbody aria-relevant="all" aria-live="polite" role="alert">
                            
                        </tbody>
                    </table>		
				</div>
			</div>
    	</div>
        
    	<div class="tab-pane" id="salidas">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="salidas_table" class="table table-bordered table-striped table-hover dataTable">
		            	<thead>
		                	<tr>
		                		<th class="col-md-2"><?php echo lang("fecha") ?></th>
		                		<th><?php echo lang("cliente") ?></th>
		                		<th><?php echo lang("mantenimiento") ?></th>
		                		<th><?php echo lang("producto") ?></th>
		                		<th width="1%"><?php echo lang("cantidad") ?></th>
		                        <th width="1%"><?php echo lang("estado") ?></th>
		                        <th width="10%" class="col-md-2"><?php echo lang("empleados") ?></th>
		                        <th width="1%" class="col-md-1"><?php echo lang("temporal") ?></th>
		                        <th width="1%"class="col-md-1"><?php echo lang("reposicion") ?></th>
		                	</tr>
						</thead>
		                                        
						<tfoot>
		                	<tr>
		                		<th><?php echo lang("fecha") ?></th>
		                		<th><?php echo lang("cliente") ?></th>
		                		<th><?php echo lang("mantenimiento") ?></th>
		                		<th><?php echo lang("producto") ?></th>
		                		<th><?php echo lang("cantidad") ?></th>
		                        <th><?php echo lang("estado") ?></th>
		                        <th><?php echo lang("empleados") ?></th>
		                        <th><?php echo lang("temporal") ?></th>
		                        <th class="col-md-1"><?php echo lang("reposicion") ?></th>
		                	</tr>
						</tfoot>
						
						<tbody aria-relevant="all" aria-live="polite" role="alert">
							
						</tbody>
					</table>	
				</div>
			</div>
    	</div>
    	
    	<div class="tab-pane" id="entradas">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="entradas_table" class="table table-bordered table-striped table-hover dataTable">
		            	<thead>
		                	<tr>
                            	<th class="col-md-2"><?php echo lang("proveedor") ?></th>
		                		<th class="col-md-1"><?php echo lang("entrada") ?></th>
		                		<th class="col-md-2" style="min-width: 100px !important;"><?php echo lang("fecha") ?></th>
		                		<th class="col-md-2"><?php echo lang("productos") ?></th>
		                		<th><?php echo lang("mantenimiento") ?></th>
								<th><?php echo lang("stock.col") ?></th>
								<th><?php echo lang("entrada.auto.col") ?></th>
		                		<th class="col-md-1"><?php echo lang("estado") ?></th>
		                	</tr>
						</thead>
		                                        
						<tfoot>
		                	<tr>
                            	<th class="col-md-2"><?php echo lang("proveedor") ?></th>
		                		<th><?php echo lang("entrada") ?></th>
		                		<th><?php echo lang("fecha") ?></th>
		                		<th><?php echo lang("productos") ?></th>
		                		<th><?php echo lang("mantenimiento") ?></th>
								<th><?php echo lang("stock.col") ?></th>
								<th><?php echo lang("entrada.auto.col") ?></th>
		                		<th><?php echo lang("estado") ?></th>
		                	</tr>
						</tfoot>
						
						<tbody aria-relevant="all" aria-live="polite" role="alert">
							
						</tbody>
					</table>	
				</div>
			</div>
    	</div>
    	
    	<div class="tab-pane" id="reposiciones">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="reposiciones_table" class="table table-bordered table-striped table-hover dataTable">
		            	<thead>
		                	<tr>
		                		<th class="col-md-2"><?php echo lang("mantenimiento") ?></th>
                                <th class="col-md-1"><?php echo lang("fecha") ?></th>
		                		<th class="col-md-1"><?php echo lang("referencia") ?></th>
		                		<th class="col-md-2"><?php echo lang("descripcion") ?></th>
		                		<th class="col-md-1"><?php echo lang("ancho") ?></th>
		                		<th class="col-md-1"><?php echo lang("alto") ?></th>
		                		<th class="col-md-1"><?php echo lang("longitud") ?></th>
		                		<th class="col-md-1"><?php echo lang("cantidad") ?></th>
		                		<th class="col-md-2"><?php echo lang("observaciones") ?></th>
		                	</tr>
						</thead>
		                                        
						<tfoot>
		                	<tr>
		                		<th><?php echo lang("mantenimiento") ?></th>
                                <th class="col-md-1"><?php echo lang("fecha") ?></th>
		                		<th><?php echo lang("referencia") ?></th>
		                		<th><?php echo lang("descripcion") ?></th>
		                		<th><?php echo lang("ancho") ?></th>
		                		<th><?php echo lang("alto") ?></th>
		                		<th><?php echo lang("longitud") ?></th>
		                		<th><?php echo lang("cantidad") ?></th>
		                		<th><?php echo lang("observaciones") ?></th>
		                	</tr>
						</tfoot>
						
						<tbody aria-relevant="all" aria-live="polite" role="alert">
							
						</tbody>
					</table>	
				</div>
			</div>
    	</div>
        
        <div class="tab-pane" id="trabajos">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="trabajos_table" class="table table-bordered table-striped table-hover dataTable">
		            	<thead>
                            <tr>
								<th><?php echo lang("mantenimiento") //Agrego la columna adicional para que se prueda hacer el rowGrouping ?></th>
								<th width="15%"><?php echo lang("fecha") ?></th>
                                <th width="5%"><?php echo lang("estado") ?></th>
                                <th><?php echo lang("mantenimientos") ?></th>
                                <th><?php echo lang("sucursal") ?></th>
                            </tr>
                        </thead>
                                                
                        <tfoot>
                            <tr>
                                <th><?php echo lang("mantenimiento") ?></th>
								<th><?php echo lang("fecha") ?></th>
                                <th><?php echo lang("estado") ?></th>
                                <th><?php echo lang("mantenimientos") ?></th>
                                <th><?php echo lang("sucursal") ?></th>
                            </tr>
                        </tfoot>
                        
                        <tbody aria-relevant="all" aria-live="polite" role="alert">
							
						</tbody>
					</table>	
				</div>
			</div>
    	</div>
        
        <div class="tab-pane" id="presupuestos">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="presupuestos_table" class="table table-bordered table-striped table-hover dataTable">
	            	<thead>
	                	<tr>
	                		<th><?php echo lang("numero.corto") ?></th>
	                		<th><?php echo lang("fecha") ?></th>
	                		<th><?php echo lang("cliente") ?></th>
	                        <th><?php echo lang("mantenimiento") ?></th>
	                		<th><?php echo lang("importe") ?></th>
	                		<th><?php echo lang("estado") ?></th>
	                		<th><?php echo lang("sucursal") ?></th>
	                	</tr>
					</thead>
	                                        
					<tfoot>
	                	<tr>
	                		<th><?php echo lang("numero.corto") ?></th>
	                		<th><?php echo lang("fecha") ?></th>
	                		<th><?php echo lang("cliente") ?></th>
	                        <th><?php echo lang("mantenimiento") ?></th>
	                		<th><?php echo lang("importe") ?></th>
	                		<th><?php echo lang("estado") ?></th>
	                		<th><?php echo lang("sucursal") ?></th>
	                	</tr>
					</tfoot>
					
					<tbody aria-relevant="all" aria-live="polite" role="alert">
						
					</tbody>
				</table>	
				</div>
			</div>
    	</div>

    	<div class="tab-pane" id="facturas">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="facturas_table" class="table table-bordered table-striped table-hover dataTable">
		            	<thead>
		                	<tr>
		                		<th><?php echo lang("numero.corto") ?></th>
		                		<th><?php echo lang("fecha") ?></th>
		                		<th><?php echo lang("cliente") ?></th>
		                        <th><?php echo lang("mantenimiento") ?></th>
		                		<th><?php echo lang("importe") ?></th>
								<th><?php echo lang("rectificativa.col") ?></th>
								<th><?php echo lang("estado") ?></th>
		                		<th><?php echo lang("sucursal") ?></th>
		                	</tr>
						</thead>
		                                        
						<tfoot>
		                	<tr>
		                		<th><?php echo lang("numero.corto") ?></th>
		                		<th><?php echo lang("fecha") ?></th>
		                		<th><?php echo lang("cliente") ?></th>
		                        <th><?php echo lang("mantenimiento") ?></th>
		                		<th><?php echo lang("importe") ?></th>
								<th><?php echo lang("rectificativa.col") ?></th>
								<th><?php echo lang("estado") ?></th>
		                		<th><?php echo lang("sucursal") ?></th>
		                	</tr>
						</tfoot>
						
						<tbody aria-relevant="all" aria-live="polite" role="alert">
							
						</tbody>
					</table>	
				</div>
			</div>
    	</div>

    	<div class="tab-pane" id="bitacora">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="bitacora_table" class="table table-bordered table-striped table-hover dataTable">
		            	<thead>
    						<th class="col-sm-2"><?php echo lang('fecha') ?></th>
                            <th class="col-sm-3"><?php echo lang('empleados') ?></th>
                            <th class="col-sm-3"><?php echo lang('tipo') ?></th>
                            <th class="col-sm-3"><?php echo lang('accion') ?></th>
    					</thead>
    					
    					<tbody></tbody>
    					
    					<tfoot>
	    					<tr>
	    						
	    					</tr>
    					</tfoot>
						
						<tbody aria-relevant="all" aria-live="polite" role="alert">
							
						</tbody>
					</table>	
				</div>
			</div>
    	</div>

    </div>
</div>

<script>
$(document).ready(function(){	
	mostrar_trabajos_taller();
	
	$('.talleres_btn').click(function(){
		mostrar_trabajos_taller();
	});
	
	$('.salidas_btn').click(function(){
		mostrar_salidas();
	});
	
	$('.entradas_btn').click(function(){
		mostrar_entradas();
	});
	
	$('.reposiciones_btn').click(function(){
		mostrar_reposiciones();
	});
	
	$('.trabajos_btn').click(function(){
		mostrar_trabajos();
	});

	$('.presupuestos_btn').click(function(){
		mostrar_presupuestos();
	});

	$('.facturas_btn').click(function(){
		mostrar_facturas();
	});

	$('.bitacora_btn').click(function(){
		mostrar_bitacora();
	});
	
	//MOSTRAR EMPLEADOS
	function mostrar_trabajos_taller() {
		
		$("#talleres_table").dataTable().fnDestroy();
		
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/taller/buscador_todos",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE ALMACENES GENERANDO LAS FILAS
					var table = '';
					var mostrar = false;
					var em_id = '<?php echo $this->session->userdata("em_id") ?>';
					var su_id = '<?php echo $this->session->userdata("su_id") ?>';
					var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					var emp_crear = '<?php echo $this->session->userdata("emp_crear") ?>';
					var emp_editar = '<?php echo $this->session->userdata("emp_editar") ?>';
					
					$.each(data,function(indice,valor) {
						//if (data[indice]['ta_estado'] == 3) {
							/*if (emp_tipo <= 1) {
								mostrar = true;
							} else if (emp_tipo > 1 && em_id == data[indice]['em_id']) {
								mostrar = true;
							} else {
								mostrar = false;
							}*/
							mostrar = true;
							
							if (mostrar) {
								table = table + '<tr id="'+data[indice]['ta_id']+'">';
								table = table + '<td>'+data[indice]['ta_id']+'</td>';
		  						var fecha_inicio = '<strong><?php echo lang("fecha.inicio") ?>:<br/></strong>'+data[indice]['ta_inicio'];
								var fecha_fin = (data[indice]['ta_terminado'] !== null) ? '<br/><strong><?php echo lang("fecha.fin") ?>:<br/></strong>'+data[indice]['ta_terminado'] : '';
								table = table + '<td>'+fecha_inicio+fecha_fin+'</td>';
		  						table = table + '<td>'+data[indice]['ta_observaciones']+'</td>';
		  						table = table + '<td>'+data[indice]['pr_numero']+'</td>';
		  						
		  						var man_nombre = (data[indice]['man_nombre'] !== null) ? data[indice]['man_nombre'] : '';
		  						table = table + '<td>'+man_nombre+'</td>';
		  						
		  						var estado = '<span class="label label-danger"><?php echo lang("pendiente") ?></span';
		  						if (data[indice]['ta_estado'] == 1) {
									estado = '<span class="label label-warning"><?php echo lang("procesando") ?></span>';
								} else if (data[indice]['ta_estado'] == 2) {
									estado = '<span class="label label-success"><?php echo lang("completado") ?></span>';
								} else if (data[indice]['ta_estado'] == 3) {
									estado = '<span class="label label-default"><?php echo lang("cerrado") ?></span>';
								}
		  						
		  						table = table + '<td>'+estado+'</td>';
		  						table = table + '</tr>';
							}
						//}
					});
					$('#talleres_table tbody').html(table);
					$("#talleres_table").dataTable({
						"aaSorting": [[ 1, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "",
				           	"sNuevo": "",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
				}
				
				$("#btn_refrecar, #btn_nuevo").hide();
					
		    }, "json");
			
			$("#talleres_table").on("mouseover","tbody tr",function(event) {
				$(this).find('td').addClass("fila_tabla");
			});
		   
			$("#talleres_table").on("mouseout","tbody tr",function(event) {
				$(this).find('td').removeClass("fila_tabla");
			});

			$("#talleres_table").on("click", "tbody tr", function(e){
				e.preventDefault();
				$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/taller/editar_trabajo_taller/'+$(this).attr('id')+'?view=true');
			});
	}
	
	function mostrar_salidas() {	    
	    $("#salidas_table").dataTable().fnDestroy();
	    
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador_salidas",
		    {'buscar':''},
		    function(data){
			    var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';     
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE ALMACENES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
                            var ta_id = '';
                            if (data[indice]['ta_id'] != null) {
                                ta_id = data[indice]['ta_id'];
                            }
                            
                            var al_id = '';
                            if (data[indice]['al_id'] != null) {
                                al_id = data[indice]['al_id'];
                            }
                            
                            var product_id = '';
                            if (data[indice]['product_id'] != null) {
                                product_id = data[indice]['product_id'];
                            }
                            
                            var al_tipo = '';
                            if (data[indice]['al_tipo'] != null) {
                                al_tipo = data[indice]['al_tipo'];
                            }
                            
                            table = table + '<tr ta_id="'+ta_id+'" al_id="'+al_id+'" al_tipo="'+al_tipo+'" product_id="'+product_id+'" id="'+data[indice]['sa_id']+'">';
							
							var mantenimiento = '';
                            if (data[indice]['man_nombre'] != null) {
                                mantenimiento = '<?php echo lang("mantenimiento") ?>: ' + data[indice]['man_nombre'];
                            }
							table = table + '<td class="vcenter read_only">'+mantenimiento+'</td>';
							
							table = table + '<td width="15% class="vcenter">';
							table = table + '<div><strong><?php echo lang("fecha.solicitud") ?></strong><br>' + data[indice]['sa_fecha'] + '</div>';
							
							if (data[indice]['fecha_cancelacion']) {
								table = table + '<div><strong><?php echo lang("fecha.cancelacion") ?></strong><br>' + data[indice]['fecha_cancelacion'] + '</div>';
								
							}
							
							if (data[indice]['fecha_aprobo']) {
								table = table + '<div><strong><?php echo lang("fecha.aprobado") ?></strong><br>' + data[indice]['fecha_aprobo'] + '</div>';
							}
							
							if (data[indice]['sa_fecha_salida']) {
								table = table + '<div><strong><?php echo lang("fecha.salida") ?></strong><br>' + data[indice]['sa_fecha_salida'] + '</div>';
							}
							
							table = table + '</td>';
	
                            var cliente = '';
                            if (data[indice]['cl_nombre'] != null) {
                                cliente = data[indice]['cl_nombre'];
                            }
                            table = table + '<td width="25% class="vcenter">'+cliente+'</td>';
      
                            var producto = '';
                            if (data[indice]['ta_trabajo'] == null) {
                                producto = '<b>'+data[indice]['product_referencia']+'</b><br>';
                                producto = producto + data[indice]['product_nombre'];
                                var caracteristicas = data[indice]['product_caracteristicas'];
                                var opciones = '';
                                if (caracteristicas) {
									caracteristicas = caracteristicas.split(';');
									//ELIMINAR ESPACIOS EN BLANCO
									caracteristicas = caracteristicas.filter(Boolean);
									for (var i=0; i<caracteristicas.length; i++) {
										caracteristicas[i] = caracteristicas[i].split('/');
									}
											
									
									for (var i=0; i<caracteristicas.length; i++) {
										if (caracteristicas[i][0].toLowerCase().search("tarifa") == -1) {
											opciones = opciones + '<span class="opc_product_list"><strong>' + caracteristicas[i][0] + '</strong>: <span>' + caracteristicas[i][1] + '</span></span>';
										}
									}
								}
				  				
                                
                                producto = producto + '<div>' + opciones + '</div>';
                            } else {
                                if (data[indice]['ta_trabajo'].indexOf('|') > -1) {
                                    var trabajo = data[indice]['ta_trabajo'].split('|');
                                    trabajo = trabajo.filter(Boolean);
                                    producto = '<span class="compuesto_padre">'+trabajo[0]+'</span>: <span class="compuesto_item">'+trabajo[1]+'</span>';
                                } else {
                                    producto = data[indice]['ta_trabajo'];
                                }
                            }
                            
							table = table + '<td class="vcenter">'+producto+'</td>';
							table = table + '<td class="vcenter text-center">'+data[indice]['sa_cantidad']+'</td>';
                            
                            var estado = '';
                            
                            if (data[indice]['sa_estado'] == 0) {
                            	estado = '<span class="label label-warning"><?php echo lang("pendiente") ?></span';
                            } else if (data[indice]['sa_estado'] == 1) {
                            	estado = '<span class="label label-info"><?php echo lang("aprobado") ?></span';
                            } else if (data[indice]['sa_estado'] == 2) {
                            	estado = '<span class="label label-success"><?php echo lang("enviado") ?></span';
                            } else if (data[indice]['sa_estado'] == 3) {
                            	estado = '<span class="label label-danger"><?php echo lang("cancelada") ?></span';
                            } else if (data[indice]['sa_estado'] == 4) {
                            	estado = '<span class="label label-success"><?php echo lang("completado") ?></span';
                            }
                            
                            table = table + '<td class="vcenter text-center">'+estado+'</td>';
                            
                            table = table + '<td class="vcenter">';
                            
                            table = table + '<div><strong><?php echo lang("solicitud") ?></strong><br>' + data[indice]['emp_solicito'] + '</div>';
                            
                            if (data[indice]['fecha_cancelacion']) {
								table = table + '<div><strong><?php echo lang("cancelo") ?></strong><br>' + data[indice]['emp_cancelo'] + '</div>';
							} else if (data[indice]['fecha_aprobo']){
								table = table + '<div><strong><?php echo lang("aprobo") ?></strong><br>' + data[indice]['emp_aprobo'] + '</div>';
							}
							
							if (data[indice]['emp_id_salida'] > 0) {
								table = table + '<div><strong><?php echo lang("salida") ?></strong><br>' + data[indice]['emp_salida'] + '</div>';
							}
							
                            table = table + '</td>';
                            
                            var temporal = '';
                            if (data[indice]['sa_temporal'] > 0) {
								temporal = '<i class="fa fa-check"></i> <?php echo lang("si") ?>';
							}
							table = table + '<td class="text-center vcenter">'+temporal+'</td>';
							
							var reposicion = '';
                            if (data[indice]['ta_reposicion'] > 0) {
								reposicion = '<i class="fa fa-check"></i> <?php echo lang("si") ?>';
							}
							table = table + '<td class="text-center vcenter">'+reposicion+'</td>';
                            
	  						table = table + '</tr>';
                        
					});
                                        
					$('#salidas_table tbody').html(table);
                                        
					$("#salidas_table").dataTable({
						"bPaginate": false,
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "",
				           	"sNuevo": "",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					}).rowGrouping({
						bExpandableGrouping: true,
						bExpandSingleGroup: false,
						iExpandGroupOffset: -1,
						asExpandedGroups: [""]
					});
					
					GridRowCount();
				
					function GridRowCount() {
						$('span.rowCount-grid').remove();
						$('input.expandedOrCollapsedGroup').remove();
			
						$('.dataTables_wrapper').find('[id|=group-id]').each(function () {
							$(this).addClass('group');
							var rowCount = $(this).nextUntil('[id|=group-id]').length;
							$(this).find('td').append($('<span />', { 'class': 'rowCount-grid' }).append($('<b />', { 'text': ' ('+rowCount+')' })));
						});
			
						$('.dataTables_wrapper').find('.dataTables_filter').append($('<input />', { 'type': 'button', 'class': 'expandedOrCollapsedGroup collapsed', 'value': 'Expanded All Group' }));
			
						
					};
						
					$("#btn_refrecar, #btn_nuevo").hide();
				}
		    }, "json");
	}	
	
	function mostrar_entradas() {
	    $("#entradas_table").dataTable().fnDestroy();
	    
	    $.ajax({
		url: '<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador_entradas',
		type: 'POST',
		data: {'buscar':''},
		dataType: "json",
			  success: function(data) {
				  
				if (data != null) {
						//CREAMOS LA TABLA
						//RECORREMOS ARRAY DE ALMACENES GENERANDO LAS FILAS
						var table = '';
			
						$.each(data,function(indice,valor) {
			
							//LISTA DE PRODUCTOS
							var info = '<div class="list oculto row">';
							
							info = info + '<div class="col-lg-12"><div class="row">';
							info = info + '<div class="col-lg-2"><strong><?php echo lang("entrada") ?></strong><p>'+data[indice]["pe_id"]+'</p></div>';
							var mant = '';
							if (data[indice]['man_nombre']) {
								mant = data[indice]['man_nombre'];
							}
							
							info = info + '<div class="col-lg-10"><strong><?php echo lang("mantenimiento") ?></strong><p>'+mant+'</p></div>';
							info = info + '</div></div>';
							
							info = info + '<div class="col-lg-12"><div  class="row">';
							info = info + '<div class="col-lg-4"><strong><?php echo lang("fecha.pedido"); ?></strong><p>'+data[indice]['pe_fecha']+'</p></div>';
							info = info + '<div class="col-lg-4"><strong><?php echo lang("fecha.aprobado"); ?></strong><p>'+data[indice]['fecha_aprobacion']+'</p></div>';
							info = info + '<div class="col-lg-4"><strong><?php echo lang("fecha.completado"); ?></strong><p>'+data[indice]['fecha_completado']+'</p></div>';
							info = info + '</div></div>';
							
							info = info + '<div class="col-lg-12">';
							var aux = data[indice]['productos'];
							
							var cantidad = data[indice]['product_cantidad'];
							cantidad = cantidad.split(";");
							cantidad = cantidad.filter(Boolean);
							
							var recibido = data[indice]['product_cantidad_recibido'];
							recibido = recibido.split(";");
							recibido = recibido.filter(Boolean);
							
							var almacenes = data[indice]['almacenes'];
							
							info = info + '<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label><?php echo lang("productos") ?></label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label><?php echo lang("cantidad") ?><label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label><?php echo lang("recibido") ?><label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label><?php echo lang("almacen") ?><label></div></div>';
							
							for (var i=0; i<aux.length; i++) {
								//ALAMCENES
								var alm = '';
								for (var a=0; a<almacenes.length; a++) {
									if (almacenes[a][0] == aux[i][0]) {
										alm = alm + '<div>' + almacenes[a][1] + ' - ' + almacenes[a][3] + '</div>';
									}
								}
								
								
								info = info + '<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">'+aux[i][1]+'</div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">'+cantidad[i]+'</div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">'+recibido[i]+'</div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">'+alm+'</div></div><hr>';
							}
							
							info = info + '<br></div>';
							
							info = info + '<div class="col-lg-12">';
							info = info + '<label><?php echo lang("observaciones.proveedor") ?></label><div>'+data[indice]["pe_observaciones_proveedor"]+'</div><br><label><?php echo lang("observaciones.empresa") ?></label><div>'+data[indice]["pe_observaciones_empresa"]+'</div><br>';
							info = info + '</div>';
							
							info = info + '<div class="col-lg-12">';
							info = info + '<label><?php echo lang("empleado.realizo.pedido") ?></label>';
							info = info + '<div><a href="<?php echo base_url().$this->lang->lang() ?>/empleados/editar_empleado/'+data[indice]["emp_id"]+'">'+data[indice]["empleado"]+'</a></div>';
							info = info + '<br></div>';
							
							info = info + '<div class="col-lg-12">';
							info = info + '<label><?php echo lang("empleado.pedido.aprobado") ?></label>';
							info = info + '<div><a href="<?php echo base_url().$this->lang->lang() ?>/empleados/editar_empleado/'+data[indice]["emp_id_aprobado"]+'">'+data[indice]["empleado_aprobado"]+'</a></div>';
							info = info + '<br></div>';
							
							info = info + '<div class="col-lg-12">';
							info = info + '<label><?php echo lang("empleados.pedido.recepcion") ?></label><div>';
							for (var e=0; e<data[indice]["empleados_recepcion"].length; e++) {
								info = info + '<a href="<?php echo base_url().$this->lang->lang() ?>/empleados/editar_empleado/'+data[indice]["empleados_recepcion"][e]["emp_id"]+'">'+data[indice]["empleados_recepcion"][e]["emp_nombre"]+' '+data[indice]["empleados_recepcion"][e]["emp_apellido1"]+' '+data[indice]["empleados_recepcion"][e]["emp_apellido2"]+'</a><br>';
							}
							info = info + '<br></div>';
							
							info = info + '<div class="col-lg-12"><a href="<?php echo base_url().$this->lang->lang() ?>/almacenes/editar_entrada/'+data[indice]['pe_id']+'"><i class="fa fa-eye"></i> <?php echo lang("ver.listado") ?></a></div>';
							
							info = info + '</div>';
							
							
							//TR
							table = table + '<tr>';
							table = table + '<td class="read_only">'+data[indice]['proveedores'].join(", ")+'</td>';
							table = table + '<td>'+data[indice]['pe_id']+info+'</td>';
							table = table + '<td>'+data[indice]['pe_fecha']+'</td>';
							
							var productos = data[indice]['product_id'];
							if(productos) { 
								productos = productos.split(";");
								productos = productos.filter(Boolean);
								table = table + '<td class="text-center">'+productos.length+'</td>';	
							} else {
								table = table + '<td class="text-center"></td>';	
							}
	
							var proyecto = (data[indice]['man_nombre'] !== null) ? data[indice]['man_nombre'] : '';
							table = table + '<td>'+proyecto+'</td>';
							
							var stock = (data[indice]['pr_id'] == 0) ? '<i class="fa fa-check"></i>' : '';
	  						table = table + '<td>'+stock+'</td>';
							
							var autom = (data[indice]['pr_id'] == null) ? '<i class="fa fa-check"></i>' : '';
							table = table + '<td>'+autom+'</td>';
							
							var estado;
		  					if (data[indice]['pe_estado'] == 0) {
								estado = '<span class="label label-danger"><?php echo lang("pendiente") ?></span>';
							} else if (data[indice]['pe_estado'] == 1) {
								estado = '<span class="label label-warning"><?php echo lang("aprobado") ?></span>';
							} else if (data[indice]['pe_estado'] == 2) {
								estado = '<span class="label label-success"><?php echo lang("enviado") ?></span>';
							} else if (data[indice]['pe_estado'] == 3) {
								estado = '<span class="label label-default"><?php echo lang("anulado") ?></span>';
							} else if (data[indice]['pe_estado'] == 4) {
								estado = '<span class="label label-primary"><?php echo lang("completado") ?></span>';
							}
							table = table + '<td class="text-center">'+estado+'</td>';
							table = table + '</tr>';
						});
					}
					
				$('#entradas_table tbody').html(table);
				
				$("#entradas_table").dataTable({
					"bPaginate": false,
					"oLanguage": {
						"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						"oPaginate": {
							"sPrevious": "",
							"sNext":""
						},
						"sRefresh": "",
						"sNuevo": "",
						"sLengthMenu": '<select class="form-control">'+
							'<option value="10">10</option>'+
							'<option value="20">20</option>'+
							'<option value="30">30</option>'+
							'<option value="40">40</option>'+
							'<option value="50">50</option>'+
							'<option value="-1">All</option>'+
							'</select> <?php echo lang("por.pagina") ?>'
					}
				}).rowGrouping({
					bExpandableGrouping: true,
					bExpandSingleGroup: false,
					iExpandGroupOffset: -1,
					asExpandedGroups: [""]
				});
				
				
				GridRowCount();
				
				function GridRowCount() {
					$('span.rowCount-grid').remove();
					$('input.expandedOrCollapsedGroup').remove();
		
					$('.dataTables_wrapper').find('[id|=group-id]').each(function () {
						$(this).addClass('group');
						var rowCount = $(this).nextUntil('[id|=group-id]').length;
						$(this).find('td').append($('<span />', { 'class': 'rowCount-grid' }).append($('<b />', { 'text': ' ('+rowCount+')' })));
					});
		
					$('.dataTables_wrapper').find('.dataTables_filter').append($('<input />', { 'type': 'button', 'class': 'expandedOrCollapsedGroup collapsed', 'value': 'Expanded All Group' }));
		
					
				};
				
				//EDITAR O VER PEDIDO
				$("#entradas_table").on("mouseover","tbody tr:not(.group)",function(event) {
					$(this).find('td').addClass("fila_tabla");
				});
			   
				$("#entradas_table").on("mouseout","tbody tr:not(.group)",function(event) {
					$(this).find('td').removeClass("fila_tabla");
				});
				
				$("#entradas_table").on("click", "tbody tr:not(.group)", function(e){
					e.preventDefault();
					
					if (!$(this).hasClass('list')) {
						var info = $(this).find('td:first .list').html();
						var result = '<div class="infotr row">'+info+'</div>';
						
						bootbox.alert(result);
					}
				});
							
				$("#btn_refrescar, #btn_nuevo").hide();
			}
		});
	}
	
	function mostrar_reposiciones() {
	    $("#reposiciones_table").dataTable().fnDestroy();
	    
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/taller/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE ALMACENES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {				
						//TRABAJOS SIMPLES
						var codigo = data[indice]['ta_trabajos_codigo'].split(';');
						codigo = codigo.filter(Boolean);
						
						var descripcion = data[indice]['ta_trabajos_descripcion'].split(';');
						descripcion = descripcion.filter(Boolean);
						
						var ancho = data[indice]['ta_trabajos_ancho'].split(';');
						ancho = ancho.filter(Boolean);
						
						var alto = data[indice]['ta_trabajos_alto'].split(';');
						alto = alto.filter(Boolean);
						
						var longitud = data[indice]['ta_trabajos_longitud'].split(';');
						longitud = longitud.filter(Boolean);
						
						var cantidad = data[indice]['ta_trabajos_cantidad'].split(';');
						cantidad = cantidad.filter(Boolean);
						
						var observaciones = data[indice]['ta_trabajos_observaciones'].split(';');
						observaciones = observaciones.filter(Boolean);
						
						var reposiciones = data[indice]['ta_trabajos_reposiciones'].split(';');
						reposiciones = reposiciones.filter(Boolean);
						
						for (var i=0; i<codigo.length; i++) {
							
							if (reposiciones[i] == 'true') {
								
								table = table + '<tr id="'+data[indice]['ta_id']+'">';
								table = table + '<td><?php echo lang("mantenimiento") ?>: '+data[indice]['man_nombre']+'</td>'; //'+data[indice]['pr_numero']+'
								table = table + '<td>'+data[indice]['ta_inicio']+'</td>';
								table = table + '<td>'+codigo[i]+'</td>';
								table = table + '<td>'+descripcion[i]+'</td>';
								table = table + '<td>'+ancho[i]+'</td>';
								table = table + '<td>'+alto[i]+'</td>';
								table = table + '<td>'+longitud[i]+'</td>';
								table = table + '<td>'+cantidad[i]+'</td>';
								table = table + '<td>'+observaciones[i]+'</td>';
								table = table + '</tr>';
							}
						}
						
						//TRABAJOS COMPUESTOS
						var codigo = data[indice]['ta_trabajos_compuestos_codigo'].split(';');
						codigo = codigo.filter(Boolean);
						
						var descripcion = data[indice]['ta_trabajos_compuestos_descripcion'].split(';');
						descripcion = descripcion.filter(Boolean);
						
						var ancho = data[indice]['ta_trabajos_compuestos_ancho'].split(';');
						ancho = ancho.filter(Boolean);
						
						var alto = data[indice]['ta_trabajos_compuestos_alto'].split(';');
						alto = alto.filter(Boolean);
						
						var longitud = data[indice]['ta_trabajos_compuestos_longitud'].split(';');
						longitud = longitud.filter(Boolean);
						
						var cantidad = data[indice]['ta_trabajos_compuestos_cantidad'].split(';');
						cantidad = cantidad.filter(Boolean);
						
						var observaciones = data[indice]['ta_trabajos_compuestos_observaciones'].split(';');
						observaciones = observaciones.filter(Boolean);
						
						var reposiciones = data[indice]['ta_trabajos_compuestos_reposiciones'].split(';');
						reposiciones = reposiciones.filter(Boolean);
						
						for (var i=0; i<codigo.length; i++) {
							var codigo_aux = codigo[i].split('|');
							codigo_aux = codigo_aux.filter(Boolean);
								
							var descripcion_aux = descripcion[i].split('|');
							descripcion_aux = descripcion_aux.filter(Boolean);
								
							var ancho_aux = ancho[i].split('|');
							ancho_aux = ancho_aux.filter(Boolean);
								
							var alto_aux = alto[i].split('|');
							alto_aux = alto_aux.filter(Boolean);
								
							var longitud_aux = longitud[i].split('|');
							longitud_aux = longitud_aux.filter(Boolean);
								
							var cantidad_aux = cantidad[i].split('|');
							cantidad_aux = cantidad_aux.filter(Boolean);
								
							var observaciones_aux = observaciones[i].split('|');
							observaciones_aux = observaciones_aux.filter(Boolean);
								
							var reposiciones_aux = reposiciones[i].split('|');
							reposiciones_aux = reposiciones_aux.filter(Boolean);
							
							for (var x=0; x<reposiciones_aux.length; x++) {
								
								if (reposiciones_aux[x] == 'true') {
								
									table = table + '<tr id="'+data[indice]['ta_id']+'">';
									table = table + '<td><?php echo lang("mantenimiento") ?>: '+data[indice]['man_nombre']+'</td>'; //'+data[indice]['pr_numero']+'
									table = table + '<td>'+data[indice]['ta_inicio']+'</td>';
									table = table + '<td>'+codigo_aux[0]+': '+codigo_aux[x+1]+'</td>';
									table = table + '<td>'+descripcion_aux[x]+'</td>';
									table = table + '<td>'+ancho_aux[x]+'</td>';
									table = table + '<td>'+alto_aux[x]+'</td>';
									table = table + '<td>'+longitud_aux[x]+'</td>';
									table = table + '<td>'+cantidad_aux[x]+'</td>';
									table = table + '<td>'+observaciones_aux[x]+'</td>';
									table = table + '</tr>';
								}
							}
						}
					});
					
					$('#reposiciones_table tbody').html(table);
					
					$("#reposiciones_table").dataTable({
						"aaSorting": [[ 0, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "",
				           	"sNuevo": "",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					}).rowGrouping({
						bExpandableGrouping: true,
						bExpandSingleGroup: false,
						iExpandGroupOffset: -1,
						asExpandedGroups: [""]
					});
					
					GridRowCount();
				
					function GridRowCount() {
						$('span.rowCount-grid').remove();
						$('input.expandedOrCollapsedGroup').remove();
			
						$('.dataTables_wrapper').find('[id|=group-id]').each(function () {
							$(this).addClass('group');
							var rowCount = $(this).nextUntil('[id|=group-id]').length;
							$(this).find('td').append($('<span />', { 'class': 'rowCount-grid' }).append($('<b />', { 'text': ' ('+rowCount+')' })));
						});
			
						$('.dataTables_wrapper').find('.dataTables_filter').append($('<input />', { 'type': 'button', 'class': 'expandedOrCollapsedGroup collapsed', 'value': 'Expanded All Group' }));
			
						
					};
                                
					$("#btn_refrecar, #btn_nuevo").hide();
				}
		    }, "json");
		}
		
		//MOSTRAR TRABAJOS
		function mostrar_trabajos() {
			
			$("#trabajos_table").dataTable().fnDestroy();
			
			//INICIO PETICION AJAX
			$.post(
				"<?php echo base_url().$this->lang->lang() ?>/trabajos/buscador_todos",
				{'buscar':''},
				function(data){
					
					if (data != null) {
						//CREAMOS LA TABLA
						//RECORREMOS ARRAY DE SUCURSALES GENERANDO LAS FILAS
						var table = '';
						
						$.each(data,function(indice,valor) {
							//PREGUNTAMOS SI ES GERENTE PARA MOSTRAR LOS DESHABILITADOS O NO Y LA CLASE DE COLOR
							
							//DETERMINO LOS PROYECTOS POR TRABAJO (EN CASO QUE UN TRABAJO ESTE ASOCIADO A VARIOS PROYECTOS)
							var mantdata = data[indice]['man_id'].split(";");
							
							for(count = 0; count < mantdata.length-1; count++){
								
								mant = mantdata[count].split(",");
								
								table = table + '<tr id="'+data[indice]['tr_id']+'">';
							
								table = table + '<td><?php echo lang("mantenimiento") ?>: ' + mant[3] + '</td>';
								
								table = table + '<td>'+data[indice]['tr_fecha_inicio']+'</td>';
								
								var estado;
								if (data[indice]['tr_estado'] == 0) {
									estado = '<span class="label label-danger"><?php echo lang("pendiente") ?></span>';
								} else if (data[indice]['tr_estado'] == 1) {
									estado = '<span class="label label-warning"><?php echo lang("procesando") ?></span>';
								} else if (data[indice]['tr_estado'] == 2) {
									estado = '<span class="label label-success"><?php echo lang("completado") ?></span>';
								}
								table = table + '<td>'+estado+'</td>';
								
								table = table + '<td><strong>' + mant[0] + ': </strong>' + mant[3] + '</td>';
								
								table = table + '<td>'+data[indice]['su_nombre']+'</td>';
								table = table + '</tr>';
								
							}
							
						});
						
						$('#trabajos_table tbody').html(table);
						
						$("#trabajos_table").dataTable({
							"aaSorting": [[ 0, "desc"]],
							"bPaginate": false,
							"oLanguage": {
								"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
								"oPaginate": {
									"sPrevious": "",
									"sNext":""
								},
								"sLengthMenu": '<select class="form-control">'+
									'<option value="10">10</option>'+
									'<option value="20">20</option>'+
									'<option value="30">30</option>'+
									'<option value="40">40</option>'+
									'<option value="50">50</option>'+
									'<option value="-1">All</option>'+
									'</select> <?php echo lang("por.pagina") ?>'
							}
						}).rowGrouping({
							bExpandableGrouping: true,
							bExpandSingleGroup: false,
							iExpandGroupOffset: -1,
							asExpandedGroups: [""]
						});
						
						GridRowCount();
					
						function GridRowCount() {
							$('span.rowCount-grid').remove();
							$('input.expandedOrCollapsedGroup').remove();
				
							$('.dataTables_wrapper').find('[id|=group-id]').each(function () {
								$(this).addClass('group');
								var rowCount = $(this).nextUntil('[id|=group-id]').length;
								$(this).find('td').append($('<span />', { 'class': 'rowCount-grid' }).append($('<b />', { 'text': ' ('+rowCount+')' })));
							});
				
							$('.dataTables_wrapper').find('.dataTables_filter').append($('<input />', { 'type': 'button', 'class': 'expandedOrCollapsedGroup collapsed', 'value': 'Expanded All Group' }));
				
							
						};
						
						//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
						/*var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						if (tipo > 0) {
							$('#trabajos_table thead tr th:last').hide();
							$('#trabajos_table tbody tr').each(function(indice,valor) {
								$(this).find('td:last').hide();
							});
							$('#trabajos_table tfoot tr th:last').hide();
						}*/
						
						$("#btn_refrecar, #btn_nuevo").hide();
						
						//REFRESCAR
						$("#btn_refrecar").click(function(e){
							e.preventDefault();
							window.location.reload(true); 
						});
		
						//EDITAR O VER TRABAJO
						$("#trabajos_table").on("mouseover","tbody tr:not(.group)",function(event) {
							$(this).find('td').addClass("fila_tabla");
						});
					   
						$("#trabajos_table").on("mouseout","tbody tr:not(.group)",function(event) {
							$(this).find('td').removeClass("fila_tabla");
						});
						
						$("#trabajos_table").on("click", "tbody tr:not(.group)", function(e){
							e.preventDefault();
							//$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/trabajos/ver_trabajo/'+$(this).attr('id'));
							$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/trabajos/editar_trabajo/'+$(this).attr('id'));
						});
						
						//$("#trabajos_table, #trabajos_table thead th").removeAttr('style');
						
					}
				}, "json");
		}

	function mostrar_presupuestos() {
		$("#presupuestos_table").dataTable().fnDestroy();
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/presupuestos/buscador",
		    {'buscar':'','soloAnuladas':true},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						var su_id = '<?php echo $this->session->userdata("su_id") ?>';
						var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						/*var mostrar = false;
						if (tipo == 0) {
							mostrar = true;
						} else if (su_id == data[indice]['su_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}*/
						
							table = table + '<tr id="'+data[indice]['pr_id']+'">';
	  						table = table + '<td>'+data[indice]['pr_numero']+'</td>';
	  						table = table + '<td>'+data[indice]['pr_fecha']+'</td>';
	  						table = table + '<td>'+data[indice]['cl_nombre']+'</td>';
                            table = table + '<td>'+data[indice]['man_nombre']+'</td>';
	  						table = table + '<td>'+data[indice]['pr_total']+' '+data[indice]['currrency_symbol']+'</td>';
	  						
	  						var estado;
	  						if (data[indice]['pr_estado'] == 0) {
								estado = '<span class="label label-info"><?php echo lang("pendiente") ?></span>';
							} else if (data[indice]['pr_estado'] == 1) {
								estado = '<span class="label label-primary"><?php echo lang("aprobada") ?></span>';
							} else if (data[indice]['pr_estado'] == 2) {
								estado = '<span class="label label-warning"><?php echo lang("caliente") ?></span>';
							} else if (data[indice]['pr_estado'] == 3) {
								estado = '<span class="label label-danger"><?php echo lang("cancelada") ?></span>';
							} else if (data[indice]['pr_estado'] == 4) {
								estado = '<span class="label label-danger"><?php echo lang("anulada") ?></span>';
							}
							
	  						
	  						table = table + '<td class="text-center">'+estado+'</td>';
	  						table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  						table = table + '</tr>';
					});
					$('#presupuestos tbody').html(table);
					
					$("#presupuestos_table").dataTable({
						"aaSorting": [[ 1, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-signal'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					if (tipo > 0) {
						$('#presupuestos table thead tr th:last').hide();
						$('#presupuestos table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#presupuestos table tfoot tr th:last').hide();
					}        			        
			        
			        
			        //EDITAR O VER SUCURSAL
			        $("#presupuestos_table").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#presupuestos_table").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
                    $("#presupuestos_table").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/presupuestos/editar_presupuesto/'+$(this).attr('id'));
					});

					$("#btn_refrecar, #btn_nuevo").hide();
				}
		    }, "json");
	}

	function mostrar_facturas() {
		$("#facturas_table").dataTable().fnDestroy();
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/facturas/buscador",
		    {'buscar':'','soloAnuladas':true},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						var su_id = '<?php echo $this->session->userdata("su_id") ?>';
						var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						/*var mostrar = false;
						if (tipo == 0) {
							mostrar = true;
						} else if (su_id == data[indice]['su_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}*/
						
						table = table + '<tr id="'+data[indice]['fa_id']+'">';
  						table = table + '<td>'+data[indice]['fa_numero']+'</td>';
  						table = table + '<td>'+data[indice]['fa_fecha']+'</td>';
  						table = table + '<td>'+data[indice]['cl_nombre']+'</td>';
                        table = table + '<td>'+data[indice]['man_nombre']+'</td>';
  						
						var total = (data[indice]['re_numero'] !== null) ? data[indice]['re_total'] : data[indice]['fa_total'];
						table = table + '<td>'+total+' '+data[indice]['currrency_symbol']+'</td>';
						
						var rect = (data[indice]['re_numero'] !== null) ? '<i class="fa fa-check"></i>' : '';
						table = table + '<td>'+rect+'</td>';
						
						var estado;
  						if (data[indice]['fa_estado'] == 0) {
							estado = '<span class="label label-info"><?php echo lang("pendiente") ?></span>';
						} else if (data[indice]['fa_estado'] == 1) {
							estado = '<span class="label label-primary"><?php echo lang("aprobada") ?></span>';
						} else if (data[indice]['fa_estado'] == 2) {
							estado = '<span class="label label-warning"><?php echo lang("caliente") ?></span>';
						} else if (data[indice]['fa_estado'] == 3) {
							estado = '<span class="label label-danger"><?php echo lang("cancelada") ?></span>';
						} else if (data[indice]['fa_estado'] == 4) {
							estado = '<span class="label label-danger"><?php echo lang("anulada") ?></span>';
						}
  						
  						table = table + '<td class="text-center">'+estado+'</td>';
  						table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  						table = table + '</tr>';
					});
					$('#facturas tbody').html(table);
					
					$("#facturas_table").dataTable({
						"aaSorting": [[ 1, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-signal'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					if (tipo > 0) {
						$('#facturas table thead tr th:last').hide();
						$('#facturas table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#facturas table tfoot tr th:last').hide();
					}
			        

			        $("#btn_refrecar, #btn_nuevo").hide();
			        
			        //EDITAR O VER SUCURSAL
			        $("#facturas_table").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#facturas_table").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#facturas_table").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/facturas/editar_factura/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}

	function mostrar_bitacora(){		
		$.post(
		"<?php echo base_url().$this->lang->lang() ?>/bitacora/buscar_todo",
		{'bi_idasociado':null ,'bi_tipo':null},
		function(data){
			
			var table = '';
			var tipo;			
			$.each(data,function(indice) {
				tipo = '';
				table = table + '<tr id="'+data[indice]['bi_id']+'">';
				table = table + '<td>' + data[indice]['bi_fecha'] + '</td>';	
				var empleado = data[indice]['emp_nombre'] + ' ' + data[indice]['emp_apellido1']+ ' ' + data[indice]['emp_apellido2'];	
				table = table + '<td>' + empleado + '</td>';			

				/* tipos bitacora
				TR	Trabajo
				TA	Taller
				AL	Almacen 
				PE	Pedido (Entradas)
				SU	Sucursal
				DE	Departamento
				EM	Empleados
				CL	Clientes
				PR	Proyectos
				FA	Factura
				FR	Factura Rectificada
				CO	cortizacion(Presupuesto)
				PS	Presupuesto
				PV	Proveedores
				PD	Producto
				SC	Solicitud Cotizacion
				SA	Salida
				IN	Inspecciones
				IA	Inspecciones Avanzadas
				TO	Tienda Online
				CC	Carrito Compra
				CG	Carrito Guardado
				*/

				if(data[indice]['bi_tipo'] == 'TR'){ 
					tipo =	'Trabajo'; 
				}

				if(data[indice]['bi_tipo'] == 'TA'){ 
					tipo =	'Taller'; 
				}

				if(data[indice]['bi_tipo'] == 'AL'){ 
					tipo =	'Almacen '; 
				}

				if(data[indice]['bi_tipo'] == 'PE'){ 
					tipo =	'Pedido (Entradas)'; 
				}

				if(data[indice]['bi_tipo'] == 'SU'){ 
					tipo =	'Sucursal'; 
				}

				if(data[indice]['bi_tipo'] == 'DE'){ 
					tipo =	'Departamento'; 
				}

				if(data[indice]['bi_tipo'] == 'EM'){ 
					tipo =	'Empleados'; 
				}

				if(data[indice]['bi_tipo'] == 'CL'){ 
					tipo =	'Clientes'; 
				}

				if(data[indice]['bi_tipo'] == 'PR'){ 
					tipo =	'Proyectos'; 
				}

				if(data[indice]['bi_tipo'] == 'FA'){ 
					tipo =	'Factura'; 
				}

				if(data[indice]['bi_tipo'] == 'FR'){ 
					tipo =	'Factura Rectificada'; 
				}

				if(data[indice]['bi_tipo'] == 'CO'){ 
					tipo =	'cortizacion(Presupuesto)'; 
				}

				if(data[indice]['bi_tipo'] == 'PS'){ 
					tipo =	'Presupuesto'; 
				}

				if(data[indice]['bi_tipo'] == 'PV'){ 
					tipo =	'Proveedores'; 
				}

				if(data[indice]['bi_tipo'] == 'PD'){ 
					tipo =	'Producto'; 
				}

				if(data[indice]['bi_tipo'] == 'SC'){ 
					tipo =	'Solicitud Cotizacion'; 
				}

				if(data[indice]['bi_tipo'] == 'SA'){ 
					tipo =	'Salida'; 
				}

				if(data[indice]['bi_tipo'] == 'IN'){ 
					tipo =	'Inspecciones'; 
				}

				if(data[indice]['bi_tipo'] == 'IA'){ 
					tipo =	'Inspecciones Avanzadas'; 
				}

				if(data[indice]['bi_tipo'] == 'TO'){ 
					tipo =	'Tienda Online'; 
				}

				if(data[indice]['bi_tipo'] == 'CC'){ 
					tipo =	'Carrito Compra'; 
				}

				if(data[indice]['bi_tipo'] == 'CG'){ 
					tipo =	'Carrito Guardado'; 
				}

				table = table + '<td>' + tipo + '</td>';

				table = table + '<td>' + data[indice]['bi_accion'] + '</td>';				

				table = table + '</tr>';
			});

			$('#bitacora tbody').html(table);

			$("#bitacora_table").dataTable({
				"aaSorting": [[ 1, "desc"]],
				"oLanguage": {
				  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
				  	"oPaginate": {
		            	"sPrevious": "",
		            	"sNext":""
		           	},
		           	"sRefresh": "",
		           	"sNuevo": "",
		           	"sLengthMenu": '<select class="form-control">'+
	             		'<option value="10">10</option>'+
	             		'<option value="20">20</option>'+
	             		'<option value="30">30</option>'+
	             		'<option value="40">40</option>'+
	             		'<option value="50">50</option>'+
	             		'<option value="-1">All</option>'+
	             		'</select> <?php echo lang("por.pagina") ?>'
				}
			});

			$("#btn_refrecar, #btn_nuevo").hide();

			$("#bitacora_table").on("mouseover","tbody tr",function(event) {
				$(this).find('td').addClass("fila_tabla");
			});
		   
			$("#bitacora_table").on("mouseout","tbody tr",function(event) {
				$(this).find('td').removeClass("fila_tabla");
			});
	                
		}, "json");
	}

});
</script>