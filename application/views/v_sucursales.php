
<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("email") ?></th>
                		<th><?php echo lang("pais") ?></th>
                		<th><?php echo lang("localidad") ?></th>
                		<th><?php echo lang("departamentos") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("email") ?></th>
                		<th><?php echo lang("pais") ?></th>
                		<th><?php echo lang("localidad") ?></th>
                		<th><?php echo lang("departamentos") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//MOSTRAR SUCURSALES
	function mostrar_sucursales() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE SUCURSALES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						//PREGUNTAMOS SI ES GERENTE PARA MOSTRAR LOS DESHABILITADOS O NO Y LA CLASE DE COLOR
						var mostrar = false;
						var desact = '';
						var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						if (data[indice]['su_estado'] == 0) {
							if (tipo <= 1) {
								mostrar = true;
								desact = 'desactivado';
							}
						} else {
							mostrar = true;
						}
						
						//MOSTRAMOS TODOS LOS DATOS SI ES SUPERUSUARIO O SOLO AL QUE PERTENECE SI NO LO ES
						/*var su_id = '<?php echo $this->session->userdata("su_id") ?>';
						if (tipo == 0) {
							mostrar = true;
						} else if (su_id == data[indice]['su_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}*/
						
						if (mostrar) {
							table = table + '<tr id="'+data[indice]['su_id']+'" class="'+desact+'">';
	  						table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  						table = table + '<td>'+data[indice]['su_telefono']+'</td>';
	  						table = table + '<td>'+data[indice]['su_email']+'</td>';
	  						table = table + '<td>'+data[indice]['name']+'</td>';
	  						table = table + '<td>'+data[indice]['su_localidad']+'</td>';
	  						table = table + '<td>'+data[indice]['de_cantidad']+'</td>';
	  						table = table + '</tr>';
						}
						
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 3, "asc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-sitemap'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//TIPO DE USUARIO
					var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					
					//PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
			        if (crear == '0') {
			        	$('#listar #btn_nuevo').hide();
			        }
					
					//NUEVA SUCURSAL
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/sucursales/nueva_sucursal');
					});
					
					//REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
			        
			        //EDITAR O VER SUCURSAL
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
                    $("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/sucursales/editar_sucursal/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_sucursales();
});
</script>