<div id="inspeccion">
	<?php 
	$input_nombre = array(
		'name'		=>	'in_nombre',
		'id'		=>	'in_nombre',
		'class'		=>	'form-control',
		'maxlength'	=>	'150',
		'value'		=>	isset($in_nombre)?$in_nombre:set_value('in_nombre')
	);
	?>
	
	<div class="row">
		<div class="col-lg-3">
			<div class="form-group">
				<label><?php echo lang('nombre') ?></label>
				<?php echo form_input($input_nombre) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('de_nombre') ?></div>
			</div>
		</div>
		
		<div class="col-lg-3">
			<div class="form-group">
				<label><?php echo lang('sucursal') ?></label>
				<select id="sucursales" name="su_id" class="form-control"></select>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('su_id') ?></div>
			</div>
		</div>
	</div>
	
	<div id="opciones" class="row"></div>
	
	<div class="row">
		<div class="col-lg-12 text-right">
			
		<?php if (($this->session->userdata('de_id') != '3') && ($this->session->userdata('de_id') != '4') && ($this->session->userdata('de_id') != '5')) { ?>
			<button id="eliminar_inspeccion" type="button" class="btn btn-danger ladda-button" data-style="zoom-out"><i class="fa fa-times"></i> <?php echo lang('eliminar.inspeccion') ?>
			</button>
			
			<button id="nuevo_grupo" type="button" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-th"></i> <?php echo lang('nuevo.grupo') ?>
			</button>
			
			<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
				<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
			</button>
		<?php } ?>
			
			<a href="<?php echo base_url().$this->lang->lang().'/inspecciones' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//VARIABLES SESSION EMPLEADO
	var su_id = '<?php echo $this->session->userdata("su_id") ?>';
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	var in_id = '<?php echo isset($in_id)?$in_id:"" ?>';
	var emp_editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	
	if (emp_editar == 0) {
		$('#nuevo_grupo, #btn_guardar').hide();
	}
	
	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	$("#inspeccion .form-group, #inspeccion .input-group").removeClass("has-error has-feedback");
	$('#inspeccion .glyphicon-remove').hide();
	$("#inspeccion .text-danger").html('');
	
	//RELLENAMOS EL CAMPO DE SUCURSALES PARA A�ADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
				if (data[indice]['su_estado'] == '1') {
					if (data[indice]["su_id"] == '<?php echo isset($su_id)?$su_id:"" ?>') {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
						mostrar = false;
					} else if (data[indice]["su_id"] == su_id) {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
					} else if (data[indice]["su_estado"] == 1) {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
					}
				}
		});
	}, "json");
	
	//CONTADOR PARA LOS GRUPOS DE PREGUNTAS
	var cont_grup = 0;
	
	//A�ADIR UNA PREGUNTA
	function add_pregunta(cont_grup) {
		var clas_preg = $("#opciones #grup"+cont_grup+" .box-body .preguntas").length;
		
		var aux = '<div class="row"><div class="col-lg-12"><table><tbody><tr>';
		
		var aux = aux + '<td class="preguntas row preg'+clas_preg+'"><div class="col-lg-12"><div class="form-group"><input class="form-control" type="text" value="<?php echo lang("suministro") ?>" disabled><div class="text-danger"></div></div></div></td>';
		var aux = aux + '<td class="preguntas row preg'+clas_preg+'"><div class="col-lg-12"><div class="form-group"><input class="form-control" type="text" value="<?php echo lang("marco") ?>" disabled><div class="text-danger"></div></div></div></td>';
		var aux = aux + '<td class="preguntas row preg'+clas_preg+'"><div class="col-lg-12"><div class="form-group"><input class="form-control" type="text" value="<?php echo lang("hoja") ?>" disabled><div class="text-danger"></div></div></div></td>';
		var aux = aux + '<td class="preguntas row preg'+clas_preg+'"><div class="col-lg-12"><div class="form-group"><input class="form-control" type="text" value="<?php echo lang("silicon") ?>" disabled><div class="text-danger"></div></div></div></td>';
		var aux = aux + '<td class="preguntas row preg'+clas_preg+'"><div class="col-lg-12"><div class="form-group"><input class="form-control" type="text" value="<?php echo lang("limpieza") ?>" disabled><div class="text-danger"></div></div></div></td>';
		
		var aux = aux + '</tr><tr></tr></tbody></table></div></div>';
		
		$('#opciones #grup'+cont_grup+' .box-body').append(aux);
	}
	
	//A�ADIR UN NUEVO GRUPO
	function add_grupo() {
		var aux = '<div id="grup'+cont_grup+'" class="col-lg-12 grupo"><div class="box box-solid box-primary"><div class="box-header"><div class="row"><div class="col-lg-2"><div class="form-group"><label><?php echo lang("tipo") ?></label><input class="form-control tipo" type="text"><div class="text-danger"></div></div></div><div class="col-lg-4"><div class="form-group"><label><?php echo lang("descripcion") ?></label><input class="form-control descripcion" type="text" value=""><div class="text-danger"></div></div></div><div class="col-lg-1"><div class="form-group"><label><?php echo lang("ancho") ?></label><input class="form-control ancho" type="text" value=""><div class="text-danger"></div></div></div><div class="col-lg-1"><div class="form-group"><label><?php echo lang("alto") ?></label><input class="form-control alto" type="text" value=""><div class="text-danger"></div></div></div><div class="col-lg-1"><div class="form-group"><label><?php echo lang("cantidad") ?></label><input class="form-control cantidad" type="text" value=""><div class="text-danger"></div></div></div><div class="col-lg-1"><div class="form-group"><label><?php echo lang("monto") ?></label><input class="form-control monto" type="text" value=""><div class="text-danger"></div></div></div>  <div class="box-tools pull-right col-lg-2 text-right"><button class="btn btn-info btn-sm copy" data-widget="copy"><i class="fa fa-files-o"></i></button><button class="btn btn-info btn-sm remove" data-widget="remove"><i class="fa fa-times"></i></button></div></div></div><div class="box-body"></div></div></div>';
		
		$('#opciones').append(aux);
		add_pregunta(cont_grup);
		cont_grup++;
	}
	
	//NUEVA INSPECCION
	if (in_id == '') {
		add_grupo();
	} else {
	//EDITAR INSPECCION
		//RECOGEMOS LOS DATOS
		var in_titulo = '<?php echo isset($in_titulo)?$in_titulo:"" ?>';
		in_titulo = in_titulo.split(';');
		in_titulo = in_titulo.splice( in_titulo.length.length-2 , in_titulo.length-1);
		
		var in_descripcion = '<?php echo isset($in_descripcion)?$in_descripcion:"" ?>';
		in_descripcion = in_descripcion.split(';');
		in_descripcion = in_descripcion.splice( in_descripcion.length.length-2 , in_descripcion.length-1);
		
		var in_ancho = '<?php echo isset($in_ancho)?$in_ancho:"" ?>';
		in_ancho = in_ancho.split(';');
		in_ancho = in_ancho.splice( in_ancho.length.length-2 , in_ancho.length-1);
		
		var in_alto = '<?php echo isset($in_alto)?$in_alto:"" ?>';
		in_alto = in_alto.split(';');
		in_alto = in_alto.splice( in_alto.length.length-2 , in_alto.length-1);
		
		var in_cantidad = '<?php echo isset($in_cantidad)?$in_cantidad:"" ?>';
		in_cantidad = in_cantidad.split(';');
		in_cantidad = in_cantidad.splice( in_cantidad.length.length-2 , in_cantidad.length-1);
		
		var in_monto = '<?php echo isset($in_monto)?$in_monto:"" ?>';
		in_monto = in_monto.split(';');
		in_monto = in_monto.splice( in_monto.length.length-2 , in_monto.length-1);
		
		//RECOGEMOS LAS PREGUNTAS
		var in_preguntas = '<?php echo isset($in_preguntas)?$in_preguntas:"" ?>';
		in_preguntas = in_preguntas.split(';');
		in_preguntas = in_preguntas.splice( in_preguntas.length.length-2 , in_preguntas.length-1);
	
		var aux = '';
		for (var i=0; i < in_titulo.length; i++) {
			//ABRRIR GRUPO
			aux = aux + '<div id="grup'+cont_grup+'" class="col-lg-12 grupo"><div class="box box-solid box-primary"><div class="box-header"><div class="row"><div class="col-lg-2"><div class="form-group"><label><?php echo lang("tipo") ?></label><input class="form-control tipo" type="text" value="'+in_titulo[i]+'"><div class="text-danger"></div></div></div><div class="col-lg-4"><div class="form-group"><label><?php echo lang("descripcion") ?></label><input class="form-control descripcion" type="text" value="'+in_descripcion[i]+'"><div class="text-danger"></div></div></div><div class="col-lg-1"><div class="form-group"><label><?php echo lang("ancho") ?></label><input class="form-control ancho" type="text" value="'+in_ancho[i]+'"><div class="text-danger"></div></div></div><div class="col-lg-1"><div class="form-group"><label><?php echo lang("alto") ?></label><input class="form-control alto" type="text" value="'+in_alto[i]+'"><div class="text-danger"></div></div></div><div class="col-lg-1"><div class="form-group"><label><?php echo lang("cantidad") ?></label><input class="form-control cantidad" type="text" value="'+in_cantidad[i]+'"><div class="text-danger"></div></div></div><div class="col-lg-1"><div class="form-group"><label><?php echo lang("monto") ?></label><input class="form-control monto" type="text" value="'+in_monto[i]+'"><div class="text-danger"></div></div></div> <div class="box-tools pull-right col-lg-2 text-right"><button class="btn btn-info btn-sm copy" data-widget="copy"><i class="fa fa-files-o"></i></button><button class="btn btn-info btn-sm remove" data-widget="remove"><i class="fa fa-times"></i></button></div></div></div>';
			
			aux = aux + '<div class="box-body"><div class="row"><div class="col-lg-12"><table><tr>';
			
			var array_preguntas = in_preguntas[i].split(',');
			
			for (var j=0; j < array_preguntas.length; j++) {
				aux = aux + '<td class="preguntas row preg'+j+'"><div class="col-lg-12"><div class="form-group"><input class="form-control" type="text" value="'+array_preguntas[j]+'" disabled><div class="text-danger"></div></div></div></td>';
			}
			
			aux = aux + '<tr></table></div></div></div>';
			
			//CERRAR GRUPO
			aux = aux + '</div></div></div>';
			
			cont_grup++;
			
		}
		
		$('#opciones').html(aux);	
	}
	
	
	//BORRAR GRUPO
	$('#opciones').on('click', '.box-header .remove', function(){
		$(this).parent().parent().parent().parent().parent().remove();
		cont_grup--;
	});
        
	//COPIAR GRUPO
	$('#opciones').on('click', '.box-header .copy', function(){
			var old_id = $(this).parent().parent().parent().parent().parent().attr('id');
			
			var grupo = '<div id="grup'+cont_grup+'" class="col-lg-12 grupo">' + $(this).parent().parent().parent().parent().parent().html() + '</div>';   
			
			$('#opciones').append(grupo);
			
			$('#opciones #'+old_id+' input').each(function(index){
				$('#opciones #grup'+cont_grup+' input:eq('+index+')').val($('#opciones #'+old_id+' input:eq('+index+')').val());
			});
			
			cont_grup++;
	});
	
	//NUEVO GRUPO
	$('#nuevo_grupo').click(function(){
		add_grupo();
	});
	
	//EJECUTAMOS EL FORMULARIO AL DARLE AL BOTON
	$('#btn_guardar').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#inspeccion .form-group, #inspeccion .input-group").removeClass("has-error has-feedback");
	    $('#inspeccion .glyphicon-remove').hide();
	    $("#inspeccion .text-danger").html('');
	    
	    var in_titulo = '';
		var in_descripcion = '';
		var in_ancho = '';
		var in_alto = '';
		var in_cantidad = '';
		var in_monto = '';
	    var in_preguntas = '';
	    var entrar = true;
	    
	    $('#inspeccion .grupo').each(function(indice){
	    	var titulo = $.trim($(this).find('.box-header input.tipo').val());
	    	if (titulo != '') {
				in_titulo = in_titulo + titulo + ';';
			} else {
				entrar = false;
				$(this).find('.box-header .text-danger').html('<?php echo lang("requerido") ?>');
				l.stop();
			}
			
			var descripcion = $.trim($(this).find('.box-header input.descripcion').val());
	    	if (descripcion != '') {
				in_descripcion = in_descripcion + descripcion + ';';
			} else {
				entrar = false;
				$(this).find('.box-header .text-danger').html('<?php echo lang("requerido") ?>');
				l.stop();
			}
			
			var ancho = $.trim($(this).find('.box-header input.ancho').val());
	    	if (ancho != '') {
				in_ancho = in_ancho + ancho + ';';
			} else {
				entrar = false;
				$(this).find('.box-header .text-danger').html('<?php echo lang("requerido") ?>');
				l.stop();
			}
			
			var alto = $.trim($(this).find('.box-header input.alto').val());
	    	if (alto != '') {
				in_alto = in_alto + alto + ';';
			} else {
				entrar = false;
				$(this).find('.box-header .text-danger').html('<?php echo lang("requerido") ?>');
				l.stop();
			}
			
			var cantidad = $.trim($(this).find('.box-header input.cantidad').val());
	    	if (cantidad != '') {
				in_cantidad = in_cantidad + cantidad + ';';
			} else {
				entrar = false;
				$(this).find('.box-header .text-danger').html('<?php echo lang("requerido") ?>');
				l.stop();
			}
			
			var monto = $.trim($(this).find('.box-header input.monto ').val());
	    	if (monto  != '') {
				in_monto  = in_monto  + monto  + ';';
			} else {
				entrar = false;
				$(this).find('.box-header .text-danger').html('<?php echo lang("requerido") ?>');
				l.stop();
			}
	    	
	    	if (entrar) {
				$(this).find('.box-body .preguntas input').each(function(indice2){
		    		var pregunta = $.trim($(this).val());
		    		if (pregunta != '') {
						in_preguntas = in_preguntas + pregunta + ',';
					} else {
						entrar = false;
						$(this).parent().next().html('<?php echo lang("requerido") ?>');
						l.stop();
					}
		    	});
		    	
		    	if (entrar) {
					in_preguntas = in_preguntas.substring(0, in_preguntas.length-1);
		    		in_preguntas = in_preguntas + ';';
				}
			}
	    });
	    
	    if (entrar) {
			var datos = {
				'su_id' : $('#sucursales option:selected').val(),
				'in_nombre' : $('#in_nombre').val(),
				'in_titulo' : in_titulo,
				'in_descripcion' : in_descripcion,
				'in_ancho' : in_ancho,
				'in_alto' : in_alto,
				'in_cantidad' : in_cantidad,
				'in_monto' : in_monto,
				'in_preguntas' : in_preguntas,
				'in_id' : in_id
			};
			
			$.ajax({
		    	type: "POST",
				dataType: "json",
			  	url: "<?php echo base_url().$this->lang->lang() ?>/inspecciones/guardar_editar",
			  	data: datos,
			  	success: function(data) {
			  		if (data.sql) {
			  			//MENSAJES ALERTA PARA EDITAR O CREAR
			  			if (datos['su_id'] != '') {
							if (!data.editar) {
								bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								setTimeout("location.href='"+location+"'", 5000);
							}
						} else {
							if (!data.crear) {
								bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								setTimeout("location.href='"+location+"'", 5000);
							}
						}
						
						//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
			  			if (data.status) {
			  				//TODO CORRECTO
							//SI ES NUEVA SUCURSAL
							if (in_id == '') {
								$('#inspeccion input').val('');
								bootbox.dialog({
									message: "<?php echo lang('inspeccion.nuevo.ok') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('nuevo') ?>",
											className: "btn-success",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/inspecciones/nueva_inspeccion';
												$(location).attr('href',url);
											}
										},
										main: {
											label: "<?php echo lang('inspecciones') ?>",
											className: "btn-primary",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/inspecciones';
												$(location).attr('href',url);
											}
										}
									}
								});
							} else {
							//SI ES UNA SUCURSAL EDITADA
								bootbox.alert('<?php echo lang("inspeccion.editado.ok") ?>');
							}
			  			} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							if (data.errors.length > 0) {
								$.each(data.errors, function (ind, elem) {
									$.each(data.errors[ind], function (ind2, elem2) {
										//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
										$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
										$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
									}); 
								});
							}
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
			  	},
			  	error: function(XMLHttpRequest, textStatus, errorThrown) {
			   		bootbox.alert('<?php echo lang("error.ajax") ?>');
			   		l.stop();
			  	}
		    });
		}	    
	});
	
	//ELIMINAR INSPECCION
	$('#eliminar_inspeccion').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#eliminar_inspeccion" ) );
	 	l.start();
		
		bootbox.dialog({
			message: "<?php echo lang('eliminar.inspeccion.confirmacion') ?>",
			buttons: {
				success: {
					label: "<?php echo lang('eliminar') ?>",
					className: "btn-danger",
					callback: function(result) {
						$.ajax({
					    	type: "POST",
						  	url: "<?php echo base_url().$this->lang->lang() ?>/inspecciones/eliminar",
						  	data: {'in_id':in_id},
						  	dataType: "json",
						  	success: function(data) {
						  		if (data) {
									bootbox.alert('<?php echo lang("eliminar.inspeccion.ok") ?>');
									
									var url = 'inspecciones/avanzadas';
									if (typeof inv_id == "undefined") {
										url = 'inspecciones';
									}
									
									url = '<?php echo base_url().$this->lang->lang() ?>/'+url;
									
									setTimeout("location.href='"+url+"'", 1500);
								} else {
									bootbox.alert('<?php echo lang("eliminar.inspeccion.ko") ?>');
								}
						  	},
						  	complete: function() {
								l.stop();
							},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});						
					}
				},
				main: {
					label: "<?php echo lang('cancelar') ?>",
					className: "btn-default"
				}
			}
		});
	});
});
</script>