<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("tipo") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("direccion") ?></th>
                		<th><?php echo lang("localidad") ?></th>
                		<th><?php echo lang("provincia") ?></th>
                		<th width="5%"><?php echo lang("productos") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("tipo") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("direccion") ?></th>
                		<th><?php echo lang("localidad") ?></th>
                		<th><?php echo lang("provincia") ?></th>
                		<th width="5%"><?php echo lang("productos") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	//MOSTRAR EMPLEADOS
	function mostrar_almacenes() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE ALMACENES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						
						var tipo = '<?php echo lang("deposito") ?>';
						if (data[indice]['al_tipo'] == 1) {
							tipo = '<?php echo lang("reciclaje") ?>';
						}
						
						table = table + '<tr id="'+data[indice]['al_id']+'">';
	  					table = table + '<td>'+data[indice]['al_nombre']+'</td>';
	  					table = table + '<td>'+tipo+'</td>';
	  					table = table + '<td>'+data[indice]['al_telefono']+'</td>';
	  					table = table + '<td>'+data[indice]['al_direccion']+'</td>';
	  					table = table + '<td>'+data[indice]['al_localidad']+'</td>';
	  					table = table + '<td>'+data[indice]['al_provincia']+'</td>';
	  					
	  					var cantidad = 0;
	  					if (data[indice]['al_tipo'] == 0) {
							cantidad = data[indice]['pro_cantidad'];
						} else {
							cantidad = data[indice]['pro_rec_cantidad'];
						}
	  					
	  					table = table + '<td>'+cantidad+'</td>';
	  					table = table + '</tr>';
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 1, "asc"],[ 0, "asc"]],
						"iDisplayLength": 20,
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-archive'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
			        
			        //PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
					var dep = '<?php echo $this->session->userdata("de_id") ?>';
			        if (crear == '0') {
                        $('#listar #btn_nuevo').hide();
			        } else {
						if ((dep == '2')||(dep == '3')||(dep == '4')) { //Si pertenece a contabilidad o a cotizaciones no puede crear
							 $('#listar #btn_nuevo').hide();
						}
					}
					
					//NUEVO ALMACEN
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/almacenes/nuevo_almacen');
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
			        
			        //EDITAR O VER ALMACEN
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/almacenes/editar_almacen/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_almacenes();
});
</script>