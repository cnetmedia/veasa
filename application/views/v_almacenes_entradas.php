<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th class="col-md-2"><?php echo lang("proveedor") ?></th>
                		<th class="col-md-1"><?php echo lang("entrada") ?></th>
                		<th class="col-md-2"><?php echo lang("fecha") ?></th>
                		<th class="col-md-2"><?php echo lang("productos") ?></th>
                		<th><?php echo lang("observaciones.empresa") ?></th>
                		<th><?php echo lang("mantenimiento") ?></th>
						<th><?php echo lang("stock.col") ?></th>
						<th><?php echo lang("entrada.auto.col") ?></th>
                		<th class="col-md-1"><?php echo lang("estado") ?></th>
                	</tr>
				</thead>                                        
				<tfoot>
                	<tr>
                		<th class="col-md-2"><?php echo lang("proveedor") ?></th>
                		<th><?php echo lang("entrada") ?></th>
                		<th><?php echo lang("fecha") ?></th>
                		<th><?php echo lang("productos") ?></th>
                		<th><?php echo lang("observaciones.empresa") ?></th>
                		<th><?php echo lang("mantenimiento") ?></th>
						<th><?php echo lang("stock.col") ?></th>
						<th><?php echo lang("entrada.auto.col") ?></th>
                		<th><?php echo lang("estado") ?></th>
                	</tr>
				</tfoot>				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	//MOSTRAR EMPLEADOS
	function mostrar_entradas() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador_entradas/<?php echo $tipo_entrada; ?>",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE ALMACENES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						if (data[indice]['pe_estado'] < 3) {
							table = table + '<tr id="'+data[indice]['pe_id']+'">';
							table = table + '<td class="read_only">'+data[indice]['proveedores']+'</td>';
							
							table = table + '<td>'+data[indice]['pe_id']+'</td>';
	  						table = table + '<td>'+data[indice]['pe_fecha']+'</td>';
	  						
	  						var productos = data[indice]['product_id'];
	  						if (data[indice]['product_id'] != null) {
								productos = productos.split(";");
								productos = productos.filter(Boolean);
								table = table + '<td>'+productos.length+'</td>';
							} else {
								table = table + '<td>0</td>';
							}
							
	  						if (data[indice]['pe_observaciones_empresa'] != null) {
								table = table + '<td>'+data[indice]['pe_observaciones_empresa']+'</td>';
							} else {
								table = table + '<td></td>';
							}
	  						
	  						var proyecto = (data[indice]['man_nombre'] !== null) ? data[indice]['man_nombre'] : '';
							table = table + '<td>'+proyecto+'</td>';
							
							var stock = (data[indice]['pr_id'] == 0) ? '<i class="fa fa-check"></i>' : '';
	  						table = table + '<td>'+stock+'</td>';
							
							var autom = (data[indice]['pr_id'] == null) ? '<i class="fa fa-check"></i>' : '';
							table = table + '<td>'+autom+'</td>';
	  						
	  						var estado;
		  					if (data[indice]['pe_estado'] == 0) {
								estado = '<span class="label label-danger"><?php echo lang("pendiente") ?></span>';
							} else if (data[indice]['pe_estado'] == 1) {
								estado = '<span class="label label-warning"><?php echo lang("aprobado") ?></span>';
							} else if (data[indice]['pe_estado'] == 2) {
								estado = '<span class="label label-success"><?php echo lang("enviado") ?></span>';
							} else if (data[indice]['pe_estado'] == 3) {
								estado = '<span class="label label-default"><?php echo lang("anulado") ?></span>';
							} else if (data[indice]['pe_estado'] == 4) {
								estado = '<span class="label label-primary"><?php echo lang("completado") ?></span>';
							}
	  						table = table + '<td class="text-center">'+estado+'</td>';
	  						table = table + '</tr>';
						}
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 2, "desc"]],
						"bPaginate": false,
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-archive'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					if (data != '') {
						$("#example1").rowGrouping({
							bExpandableGrouping: true,
							bExpandSingleGroup: false,
							iExpandGroupOffset: -1,
							asExpandedGroups: [""]
						});
						
						GridRowCount();
					
						function GridRowCount() {
							$('span.rowCount-grid').remove();
							$('input.expandedOrCollapsedGroup').remove();
				
							$('.dataTables_wrapper').find('[id|=group-id]').each(function () {
								$(this).addClass('group');
								var rowCount = $(this).nextUntil('[id|=group-id]').length;
								$(this).find('td').append($('<span />', { 'class': 'rowCount-grid' }).append($('<b />', { 'text': ' ('+rowCount+')' })));
							});
				
							$('.dataTables_wrapper').find('.dataTables_filter').append($('<input />', { 'type': 'button', 'class': 'expandedOrCollapsedGroup collapsed', 'value': 'Expanded All Group' }));
				
							
						};
					}
			        
			       	//PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
					var dep = '<?php echo $this->session->userdata("de_id") ?>';
			        if (crear == '0') {
						if (dep != '5') { //Con excepcion de los usuarios de Puerta que pueden crear entradas
							$('#listar #btn_nuevo').hide();
						}
			        } else {
						if ((dep == '3')||(dep == '4')) { //Si pertenece a contabilidad o a cotizaciones no puede crear
							 $('#listar #btn_nuevo').hide();
						}
					}
			        
                                //CREAR ENTRADAS MANUALES
                                function enviar_entrada_manual(datos) {
									
                                    $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/guardar_entrada_manual",
                                        data:datos,
                                        success: function(data) {
                                            if (data.status) {
                                                bootbox.alert('<?php echo lang("entrada.manual.ok") ?>');
                                            } else {
                                                bootbox.alert('<?php echo lang("error.ajax") ?>');
                                            }
                                        },
                                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                                            bootbox.alert('<?php echo lang("error.ajax") ?>');
                                        }
                                    });
									
                                }
                                
                                $("#btn_nuevo").click(function(e){
                                    e.preventDefault();
                                    
                                    //RELLENAMOS EL CAMPO DE MANTENIMIENTOS
                                    $.post(
                                    "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscar_materiales_almacenes",
                                    {'buscar':''},
                                    function(data){
                                            var materiales = '';
                                            
                                            var datos = {
                                                'product_id' : [],
												'pra_id' : [],
                                                'al_id' : [],
                                                'al_tipo' : [],
                                                'product_cantidad' : [],
                                                'product_nombre' : [],
                                                'product_referencia' : [],
                                                'product_precio' : [],
                                                'pr_id' : [],
                                                'pe_observaciones_empresa' : [],
                                                'product_tienda' : 0
                                            };
        
                                            materiales = materiales + '<option value="" selected>Nuevo producto</option>';
											
											$.each(data,function(indice) {
												
												if(data[indice]["pra_caracteristicas"]) {
													var caracteristicas = data[indice]["pra_caracteristicas"];
												} else {
													var caracteristicas = data[indice]["product_caracteristicas"];
												}
												
												if(data[indice]["pra_id"]) {
													var pra_id = data[indice]["pra_id"];
												} else {
													var pra_id = 0;
												}
                                                   materiales = materiales + '<option value="'+data[indice]["product_id"]+'" al_id="'+data[indice]["al_id"]+'" pra_id="'+pra_id+'" al_tipo="'+data[indice]["al_tipo"]+'">'+data[indice]["product_nombre"]+' ('+data[indice]["product_referencia"]+') | '+data[indice]["al_nombre"]+' | '+caracteristicas+'</option>';
                                                    
                                            });
                                            
                                            bootbox.dialog({
                                                message: "<div id='new_entrada'><label><?php echo lang('producto.nueva.entrada.manual') ?></label><select multiple id='productos' class='selectpicker form-control' data-live-search='true' name='product_id[]'>"+materiales+"</select></div><script>$(document).ready(function(){$('#new_entrada #productos').selectpicker();});<\/script>",
                                                buttons: {
                                                        success: {
                                                                label: "<?php echo lang('seleccionar') ?>",
                                                                className: "btn-success",
                                                                callback: function() {
                                                                    
                                                                    //datos['product_id'] = $('#new_entrada #productos option:selected').val();
																	
																	var product_nombre = []
																	
																	$('#new_entrada #productos option:selected').each(function(index, element) {
                                                                        datos['product_id'].push($(this).val());
																		datos['al_id'].push($(this).attr('al_id'));
																		datos['al_tipo'].push($(this).attr('al_tipo'));
																		datos['pra_id'].push($(this).attr('pra_id'));
																		
																		product_nombre.push($(this).text());
                                                                    });
                                                                    
                                                                    //PRODUCTO ENCONTRADO SOLO AÑADIMOS LA CANTIDAD
                                                                    if ((datos['product_id'].length > 0) && (datos['product_id'] != '')) {
																		
																		var Message = '';
																		
																		$.each(datos['product_id'],function(indice, valor) {
																			
																			Message = Message + "<div id='new_entrada' class='form-inline margin' style='padding-bottom:10px;background-color:aliceblue;' product_id='"+valor+"'><div class='form-group item margin' style='width:512px'><strong>"+product_nombre[indice]+"</strong></div><div class='form-group item margin' style='vertical-align:top'><input placeholder='<?php echo lang('cantidad') ?>' type='text' name='product_cantidad[]' id='product_cantidad' value='' class='form-control'></div></div>";
																			
																		});
																		
																		//RELLENAMOS EL CAMPO DE MANTENIMIENTOS
																		$.post(
																		"<?php echo base_url().$this->lang->lang() ?>/presupuestos/buscador",
																		{'buscar':''},
																		function(data){
																			var cotizaciones = '<option selected></option>';
																			
																			//****** 
																			
																			$.each(data,function(indice) {
																				if (data[indice]["pr_estado"] != '3') {
																					cotizaciones = cotizaciones + '<option value="'+data[indice]["pr_id"]+'">'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>';
																				}
																			});
																			
																			bootbox.dialog({
                                                                            message:  "<label><?php echo lang('presupuesto') ?></label><select id='cotizaciones' class='selectpicker form-control' data-live-search='true' name='pr_id[]'>"+cotizaciones+"</select></div><script>$(document).ready(function(){$('#cotizaciones').selectpicker();});<\/script><br>"+Message+"<br><label><?php echo lang('observaciones.empresa') ?></label><br><textarea class='observaciones' style='width:99%' name='observaciones' class='form-control'></textarea>",
																			callback: function() {
																				$('textarea').textareaAutoSize();
																			},
                                                                            buttons: {
                                                                                    success: {
                                                                                            label: "<?php echo lang('guardar') ?>",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
																								//VERIFICAMOS SI HAY COTIZACION SELECCIONADA
																								if($('#cotizaciones option:selected').val()) {
																								$.each(datos['product_id'],function(indice, valor) {
																									datos['product_cantidad'].push($('#new_entrada[product_id="'+valor+'"]').find('#product_cantidad').val());
																									//datos['pe_observaciones_empresa'].push($('#new_entrada[product_id="'+valor+'"]').find('textarea').val());
																								});
                                                                                               //datos['product_cantidad'] = $('#new_entrada #product_cantidad').val();
                                                                                               datos['pe_observaciones_empresa'] = $('textarea.observaciones').val(); 
																							   datos['nuevo_producto'] = 0;
																							   datos['pr_id'] = $('#cotizaciones option:selected').val();
																							   
																							   enviar_entrada_manual(datos);
																								} else {
																									bootbox.alert('<?php echo lang("error.carrito.cotizaciones") ?>');
																								}
                                                                                            }
                                                                                    },
                                                                                    cancel: {
                                                                                            label: "<?php echo lang('cancelar') ?>",
                                                                                            className: "btn-default"
                                                                                    }
																				}
																			});
																			
																		}, "json");
                                                                    } else {
                                                                        //PRODUCTO NO ENCONTRADO
																		
																		//RELLENAMOS EL CAMPO DE MANTENIMIENTOS
																		$.post(
																		"<?php echo base_url().$this->lang->lang() ?>/presupuestos/buscador",
																		{'buscar':''},
																		function(data){
																			var cotizaciones = '<option value="" selected>Para stock de inventario</option>';
																			
																			$.each(data,function(indice) {
																				if (data[indice]["pr_estado"] != '3') {
																					cotizaciones = cotizaciones + '<option value="'+data[indice]["pr_id"]+'">'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>';
																				}
																			});
																			
																			//PREGUNTAMOS PARA QUE PROVEEDOR LE ASIGNAMOS
																			$.post(
																				"<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador",
																				{'buscar':''},
																				function(data){
																					var proveedores = '';
																					
																					proveedores = proveedores + '<option value="" selected></option>';
																					
																					$.each(data,function(indice) {
																						proveedores = proveedores + '<option value="'+data[indice]["pro_id"]+'">'+data[indice]["pro_nombre"]+'</option>';
																					});
																					
																					//PREGUNTAMOS QUE ALMACEN SE GUARDARA
																					$.post(
																						"<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador",
																						{'buscar':''},
																						function(data){
																							var almacenes = '';
	
																							$.each(data,function(indice) {
																								var tipo_almacen = '<?php echo lang("deposito") ?>';
																								if (data[indice]["al_tipo"] == 1) {
																									tipo_almacen = '<?php echo lang("reciclaje") ?>';
																								}
																								
																								almacenes = almacenes + '<option value="'+data[indice]["al_id"]+'" al_tipo="'+data[indice]["al_tipo"]+'">'+data[indice]["al_nombre"]+' ('+tipo_almacen+')</option>';
																							});
																							
																							bootbox.dialog({
																								message: "<div id='new_entrada' class='form-inline'><div class='productos'><div class='form-group item margin'><label><?php echo lang('referencia'); ?></label><br/><input type='text' name='product_referencia' style='width:100px' id='product_referencia' value='' class='form-control'></div><div class='form-group item margin'><label><?php echo lang('nombre'); ?></label><br/><input type='text' style='width:325px' name='product_nombre' id='product_nombre' value='' class='form-control'></div><div class='form-group item margin'><label><?php echo lang('cantidad'); ?></label><br/><input type='text' name='product_cantidad' id='product_cantidad' style='width:60px' value='' class='form-control'></div><div class='form-group item margin' style='width:350px'><label><?php echo lang('tienda.online'); ?>&nbsp;<input type='checkbox' class='form-control'></label></div><div class='tienda' style='display: none;'><div class='form-group item margin'><label><?php echo lang('precio') ?></label><br/><input type='text' name='product_precio' id='product_precio' value='' class='form-control' onkeyup='var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \"\");'></div><div class='form-group item margin' style='width:350px'><label><?php echo lang('proveedores'); ?></label><select id='proveedores_0' class='selectpicker form-control proveedores' data-live-search='true'>"+proveedores+"</select></div></div><hr></div><div class='text-right'><button id='agregar_producto' type='button' class='btn btn-default ladda-button' data-style='zoom-out'><i class='fa fa-plus'></i> Agregar otro</button></div><div class='form-group item margin' style='width:350px'><label><?php echo lang('almacenes'); ?></label><br/><select id='almacenes' class='selectpicker form-control' data-live-search='true'>"+almacenes+"</select></div><div class='form-group item margin' style='width:90%'><label><?php echo lang('presupuesto') ?></label><select id='cotizaciones' class='selectpicker form-control' data-live-search='true' name='pr_id'>"+cotizaciones+"</select><script>$(document).ready(function(){$('#cotizaciones').selectpicker();});<\/script><br><br><label><?php echo lang('observaciones.empresa'); ?></label><br><textarea style='width:100%;max-width:525px' class='form-control'></textarea></div></div></div>",
																								buttons: {
																										success: {
																												label: "<?php echo lang('guardar') ?>",
																												className: "btn-success",
																												callback: function() {
																	
																													//if($('#new_entrada #cotizaciones option:selected').val()) {
													
																														datos['product_cantidad'] = [];
																														datos['product_nombre'] = [];
																														datos['product_referencia'] = [];
																														datos['product_precio'] = [];
																														datos['pro_id'] = [];
																														datos['product_tienda'] = [];
																														
																														$('#new_entrada .productos').each(function(index, element) {
																															datos['product_cantidad'].push($(this).find('#product_cantidad').val());
																															datos['product_nombre'].push($(this).find('#product_nombre').val());
																															datos['product_referencia'].push($(this).find('#product_referencia').val());
																															if ($(this).find('input[type="checkbox"]').is(':checked')) {
																																datos['product_tienda'].push(1);
																																datos['product_precio'].push($(this).find('#product_precio').val());
																																datos['pro_id'].push($(this).find('.proveedores option:selected').val());																																
																															} else {
																																datos['product_tienda'].push(0);
																																datos['product_precio'].push(0);
																																datos['pro_id'].push(1); //ESTE CORRESPONDE CON EL ID DEL PROVEEDOR "ENTRADAS TEMPORALES"																																
																															}
																														});
																														
																														datos['al_id'] = $('#new_entrada #almacenes option:selected').val();
																														datos['al_tipo'] = $('#new_entrada #almacenes option:selected').attr('al_tipo');
																														datos['pe_observaciones_empresa'] = $('#new_entrada textarea').val();
																														datos['pr_id'] = $('#new_entrada #cotizaciones option:selected').val();																									
																														datos['nuevo_producto'] = 1;
																														
																														enviar_entrada_manual(datos);
																													/*} else {
																														bootbox.alert('<?php echo lang("error.carrito.cotizaciones") ?>');
																														return false;
																													}*/
																												}
																										},
																										cancel: {
																												label: "<?php echo lang('cancelar') ?>",
																												className: "btn-default"
																										}
																								}
																							});
																							
																							$('#new_entrada input[type="checkbox"]').change(function() {
																								if ($(this).is(':checked')) {
																									$(this).parent().parent().parent().find('.tienda').show();
																								} else {
																									$(this).parent().parent().parent().find('.tienda').hide();
																								}
																							});
																							
																							$('#agregar_producto').click(function(e){
																								e.preventDefault();
																								
																								var $select = $('select[id^="proveedores_"]:last');
																								var num = parseInt( $select.prop("id").match(/\d+/g), 10 ) +1;
																								$('div.productos:last').clone().insertAfter('div.productos:last').find('select.proveedores').attr('id', 'proveedores_'+num).next('.bootstrap-select').remove();
																								$('select#proveedores_'+num).selectpicker();
																								$('div.productos:last').find('input:text').val('');
																								$('div.productos:last').find('input:checkbox').checked = false;
																								$('div.productos:last').find('tienda').hide('fast');
																								
																								$('#new_entrada input[type="checkbox"]').change(function() {
																									if ($(this).is(':checked')) {
																										$(this).parent().parent().parent().find('.tienda').show();
																									} else {
																										$(this).parent().parent().parent().find('.tienda').hide();
																									}
																								});
																							});
																							
																							$('#new_entrada .proveedores').selectpicker();
																							$('#new_entrada #almacenes').selectpicker();
																							
																						}, "json");
																				}, "json");
																		}, "json");
                                                                    }
                                                                }
                                                        },
                                                        cancel: {
                                                                label: "<?php echo lang('cancelar') ?>",
                                                                className: "btn-default"
                                                        }
                                                }
                                            });
                                            
                                            
                                    }, "json");
				});
                                
			        //REFRESCAR
                                $("#btn_refrecar").click(function(e){
                                    e.preventDefault();
                                    window.location.reload(true); 
				});
					
					//EDITAR O VER PEDIDO
			        $("#example1").on("mouseover","tbody tr:not(.group)",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr:not(.group)",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click", "tbody tr:not(.group)", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/almacenes/editar_entrada/'+$(this).attr('id')+'?tag=true');
					});
				}
		    }, "json");
	}
	
	mostrar_entradas();
});
</script>