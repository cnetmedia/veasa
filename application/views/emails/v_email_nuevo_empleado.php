<h1><?php echo lang('nuevo.empleado') ?></h1>

<p>
	<?php echo $nombre.lang('nuevo.empleado.email1'); ?></br>
	<?php echo lang('nuevo.empleado.email2') ?>
</p>

<center>                                 	
	<table border="0" cellpadding="0" cellspacing="0" class="emailButton">
		<tr>
			<td align="center" valign="middle" class="emailButtonContent">
            	<p><b>Nombre: </b><?php echo $nombre.' '.$apellido; ?><br>
                <b>Cédula: <?php echo $dni; ?></b></p>
				<a href="<?php echo base_url().$this->lang->lang().'/empleados/validar/'.$emp_id_nuevo ?>" target="_blank" ><?php echo lang('nuevo.empleado.acceder') ?></a>
			</td>
		</tr>
	</table>
</center>