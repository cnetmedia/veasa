<?php if ($confirmacion == 1) { ?>
	<h1><?php echo lang('confirmacion.lista.pedido.productos') ?></h1>
<?php } else { ?>
	<h1><?php echo lang('lista.pedido.productos') ?></h1>
<?php } ?>

<h3><?php echo lang('proveedor').': '.$nombre_proveedor ?></h3>
<h3><?php echo lang('fecha.pedido').': '.$fecha ?></h3>
<h3><?php echo lang('codigo.pedido').': '.$pe_id ?></h3>

<?php if ($confirmacion == 1) { ?>
	<p><?php echo lang('mensaje.pedido.email.confirmacion') ?></p>
<?php } else { ?>
	<p><?php echo lang('mensaje.pedido.email') ?></p>
<?php } ?>

<table width="100%">
	<thead>
		<tr>
			<th style="text-align: left"><?php echo lang('referencia') ?></th>
			<th style="text-align: left"><?php echo lang('descripcion') ?></th>
			<th style="text-align: left"><?php echo lang('caracteristicas') ?></th>
			<th style="text-align: left"><?php echo lang('cantidad') ?></th>
		</tr>
	</thead>
	
	<tbody>
		<?php
		for ($i=0; $i<count($nombre); $i++) {
			echo '<tr style="border-bottom: 1px solid #f1f1f1 !important;">';
			echo '<td style="text-align: left; padding-right: 10px;">'.$referencia[$i].'</td>';
			echo '<td style="text-align: left; padding-right: 10px;">'.$nombre[$i].'</td>';
			$txt = (empty($caracteristicas[$i]) || $caracteristicas[$i] == 'false') ? '' : $caracteristicas[$i];
			echo '<td style="text-align: left; padding-right: 10px;">'. $txt .'</td>';
			echo '<td style="text-align: center;">'.$cantidad[$i].'</td>';
			echo '</tr>';
		}
		?>
	</tbody>
</table>

<?php
if ($observaciones != '') {
	echo '<br/><br/><strong>'.lang("observaciones.proveedor").'</strong><p>'.$observaciones.'</p><hr />';
}
?>

<br/>

<p>
	<?php echo lang('mensaje.pedido.email2') ?>
	<br/>
	<?php echo $nombre_empleado.' '.$apellido1_empleado.' '.$apellido2_empleado ?>
</p>