<h1><?php echo lang('empleado.asistencia') ?></h1>

<p>
	<?php echo lang('empleado.asistencia.email1') ?>
</p>

<p>
	<?php echo lang('empleado.asistencia.email2') ?><br>
	<?php echo lang('nombre') ?>: <?php echo $emp_nombre.' '.$emp_apellido1.' '.$emp_apellido2 ?><br>
	<?php echo lang('dni') ?>: <?php echo $emp_dni ?><br>
	<?php echo lang('estado') ?>: <?php echo ($ea_estado == 1)?lang('retraso'):lang('ausente') ?><br>
	<?php echo lang('fecha.puesto.trabajo').': '.$fecha_actual ?>
</p>

<p>
	<?php echo lang('empleado.asistencia.email3') ?><br>
	<?php echo lang('contacto') ?>: <?php echo $man_contacto ?><br>
	<?php echo lang('nombre') ?>: <?php echo $man_nombre ?><br>
	<?php echo lang('telefono') ?>: <?php echo $man_telefono ?><br>
	<?php echo lang('email') ?>: <?php echo $man_email ?><br>
	<?php echo lang('fecha.trabajo') ?>: <?php echo $tr_fecha_inicio ?>
</p>