<h1><?php echo lang('nuevo.trabajo') ?></h1>

<p>
	<?php echo lang('nuevo.trabajo.email1') ?>
</p>

<p>
	<?php echo lang('trabajo.fecha.hora').': '.$informacion ?>
</p>

<?php
	if ($comentario != '') {
		echo '<p>'.lang('comentario').'<br>'.$comentario.'</p>';
	}
?>

<?php
	for ($x=0; $x<count($mantenimientos); $x++) {
		$barriada = '';
		if ($mantenimientos[$x]->man_barriada != '') {
			$barriada = ', ('.$mantenimientos[$x]->man_barriada.')';
		}
		
		echo '<p>'.lang('nombre').': '.$mantenimientos[$x]->man_nombre.'<br>'.lang('contacto').': '.$mantenimientos[$x]->man_contacto.'<br>'.lang('direccion').': '.$mantenimientos[$x]->man_direccion.$barriada.'<br>'.$mantenimientos[$x]->man_postal.', '.$mantenimientos[$x]->man_localidad.'<br>'.$mantenimientos[$x]->man_provincia.'<br>'.lang('telefono').': '.$mantenimientos[$x]->man_telefono.'<br>'.lang('latitud').': '.$mantenimientos[$x]->man_latitud.'<br>'.lang('longitud').': '.$mantenimientos[$x]->man_longitud.'</p>';
	}
?>