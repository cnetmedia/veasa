<!DOCTYPE html>
<html lang="<?php echo $this->lang->lang() ?>">
    <head>
        <meta charset="<?php echo $this->config->item('charset');?>" />
        <title><?php echo isset($TITULO)?$TITULO.' - '.$this->session->userdata('em_nombre'):'' ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="robots" content="noindex, nofollow" />
        
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Ionicons -->
        <link href="<?php echo base_url() ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Morris chart -->
        <link href="<?php echo base_url() ?>css/morris/morris.css" rel="stylesheet" type="text/css" />
        
        <!-- jvectormap -->
        <link href="<?php echo base_url() ?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        
        <!-- fullCalendar -->
        <link href="<?php echo base_url() ?>css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        
        <!-- Daterange picker -->
        <link href="<?php echo base_url() ?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo base_url() ?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        
        <!-- Ladda jQuery -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/ladda-themeless.min.css">
        
        <!-- Estilos jQuery Ui -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/themes/base/jquery.ui.all.css">
        
        <!-- FancyBox -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/jquery.fancybox.css" />
        
        <!-- Estilo personal -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/style.css" />
        
        
        
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo base_url() ?>js/jquery.min.js"></script>
        
        <!-- jQuery UI 1.10.3 -->
        <script src="<?php echo base_url() ?>js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        
        <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
        
        <!-- Morris.js charts -->
        <script src="<?php echo base_url() ?>js/raphael-min.js"></script>
        <script src="<?php echo base_url() ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
        
        <!-- Sparkline -->
        <script src="<?php echo base_url() ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        
        <!-- jvectormap -->
        <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        
        <!-- fullCalendar -->
        <script src="<?php echo base_url() ?>js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url() ?>js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        
        <!-- daterangepicker -->
        <script src="<?php echo base_url() ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/daterangepicker/moment.js" type="text/javascript"></script>
        
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url() ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url() ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        
        <!-- InputMask -->
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        
        <!-- date-range-picker -->
        <script src="<?php echo base_url() ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url() ?>js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url() ?>js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() ?>js/AdminLTE/app.js" type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!--<script src="<?php echo base_url() ?>js/AdminLTE/dashboard.js" type="text/javascript"></script>--> 
        
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() ?>js/AdminLTE/demo.js" type="text/javascript"></script>
        
        <!-- Ladda jQuery -->
        <script src="<?php echo base_url() ?>js/spin.min.js"></script>
		<script src="<?php echo base_url() ?>js/ladda.min.js"></script>
		
		<!-- Pagination Plugin -->
		<link href="<?php echo base_url() ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url() ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		
		<!-- ENVIO DE IMAGENES POR AJAX-->
		<script src="<?php echo base_url() ?>js/jquery.form.min.js"></script>
		
		<!-- ALERT BOOTSTRAP -->
		<script src="<?php echo base_url() ?>js/bootbox.min.js"></script>
		
		<!-- FancyBox -->
		<script src="<?php echo base_url() ?>js/jquery.fancybox.js"></script>
		
		<!-- MAPA Y COORDENADAS -->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&language&=<?php echo $this->lang->lang() ?>&file=api&sensor=true&key=AIzaSyAAiZvwS2hwTnBDUsQOGew0p6UbI1gV_fg"></script>
		<script src="<?php echo base_url() ?>js/markerclusterer.js"></script>
		<script src="<?php echo base_url() ?>js/markerwithlabel_packed.js"></script>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?php echo base_url() ?>js/html5shiv.js"></script>
          <script src="<?php echo base_url() ?>js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">   	
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="#" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <img src="<?php echo base_url() ?>img/Logo_veasa_admin.png" class="icon" />
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                                <span class="label label-warning">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"></li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                    	
                                    </ul>
                                </li>
                                <li class="footer"></li>
                            </ul>
                        </li>
                        <li class="dropdown tasks-menu">
                        	<a title="<?php echo lang('salir') ?>" href="<?php echo base_url().$this->lang->lang() ?>/clientes/salir">
                                <i class="fa fa-lock"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        
                        <li class="<?php echo isset($MANTENIMIENTOS)?$MANTENIMIENTOS:'' ?>">
                            <a href="<?php echo base_url().$this->lang->lang() ?>/clientes/mantenimientos">
                                <i class="fa fa-building-o"></i>
                                <span><?php echo lang('mantenimientos') ?></span>
                            </a>
                        </li>
                        
                        <li class="<?php echo isset($TRABAJOS)?$TRABAJOS:'' ?>">
                            <a href="<?php echo base_url().$this->lang->lang() ?>/clientes/trabajos">
                                <i class="fa fa-gears"></i>
                                <span><?php echo lang('trabajos') ?></span>
                            </a>
                        </li>
                        
                        <li class="<?php echo isset($MATERIAL)?$MATERIAL:'' ?>">
                            <a href="<?php echo base_url().$this->lang->lang() ?>/clientes/material">
                                <i class="fa fa-th-large"></i>
                                <span><?php echo lang('materiales') ?></span>
                            </a>
                        </li>
                        
                        <li class="<?php echo isset($CALENDARIO)?$CALENDARIO:'' ?>">
                            <a href="<?php echo base_url().$this->lang->lang() ?>/clientes/calendario">
                                <i class="fa fa-calendar"></i>
                                <span><?php echo lang('calendario') ?></span>
                            </a>
                        </li>
                        
                        <!-- SOLO PARA CLIENTES -->
                        <?php if ($this->session->userdata('cl_tipo') == 0) { ?>
	                        <li class="<?php echo isset($FACTURAS)?$FACTURAS:'' ?>">
	                        	<a href="<?php echo base_url().$this->lang->lang() ?>/clientes/facturas">
	                        		<i class="fa fa-signal"></i>
	                        		<span><?php echo lang('facturas') ?></span>
	                        	</a>
	                        </li>
	                        
	                        <li class="<?php echo isset($PRESUPUESTOS)?$PRESUPUESTOS:'' ?>">
	                        	<a href="<?php echo base_url().$this->lang->lang() ?>/clientes/presupuestos">
	                        		<i class="fa fa-file-text-o"></i>
	                        		<span><?php echo lang('presupuestos') ?></span>
	                        	</a>
	                        </li>
                        <?php } ?>                       
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo isset($TITULO)?$TITULO:'' ?>
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">  
                	<?php echo isset($BODY)?$BODY:'' ?>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script>
        	//FIXED BODY, PARA DEJAR EL MENU FIJO
        	change_layout();
        	
        	$(document).ready(function(){
        		//MOSTRAR NOTAS SIN LEER
        		//CREAMOS LA FUNCION DE ESTA MANERA PARA PODER SER
        		//LLAMADA DESDE OTRO SCRIPT
				$.notas_no_leidas = function(){
					$.post(
					    "<?php echo base_url().$this->lang->lang() ?>/notas/buscador",
					    {'buscar':''},
					    function(data){
					    	var aux = '';
					    	var emp_id = '<?php echo $this->session->userdata("emp_id") ?>';
					    	var cont = 0;
					  		$.each(data, function (ind, elem) {
					  			//ARRAY DE EMPLEADOS QUE VIERON LA NOTA
					  			var array = data[ind]['no_visto'].split(',');
					  			//PREGUNTA SI ESTE EMPLEADO LA VIO
					  			if ($.inArray(emp_id,array) == -1) {
					  				var mensaje = '';
					  				//CREA UN MENSAJE NO MUY LARGO
					  				if (data[ind]["no_mensaje"].length > 42) {
										mensaje = data[ind]["no_mensaje"].substring(0,42) + '...';
									} else {
										mensaje = data[ind]["no_mensaje"];
									}
					  				//CONCATENA LA NOTA
									aux = aux + '<li><a href="<?php echo base_url().$this->lang->lang() ?>/notas/ver_nota/'+data[ind]["no_id"]+'"><h4>'+data[ind]["no_titulo"]+'</h4><p>'+mensaje+'</p></a></li>';
									cont++;
								}
					  		});
					  		//A�ADE LAS NOTAS
					  		$('.messages-menu ul.menu').html(aux);
					  		$('.messages-menu .label-success').html(cont);
					  		$('.messages-menu .header').html('<?php echo lang("tiene") ?> '+cont+' <?php echo lang("mensajes.sin.leer") ?>');
					    }, "json"
					);
				}
				//$.notas_no_leidas();
        	});
        </script>
    </body>
</html>