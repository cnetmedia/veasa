<div class="row">
	<div class="col-lg-4 col-md-4">
		<div class="box box-primary">
			<div class="box-header">
            	<h4 class="box-title"><?php echo lang('sucursal') ?></h4>
            </div>
            
        	<div class="box-body">
            	<select id="sucursales" name="su_id" class="form-control"></select>
        	</div>
    	</div>
	</div>

	<div class="col-lg-8 col-md-8">
		<div class="box box-primary">
			<div class="box-header">
            	<h4 class="box-title"><?php echo lang('leyenda') ?></h4>
            </div>
            
        	<div class="box-body">
            	<div id='external-events'>                                        
            		<div class='external-event bg-green'>Lunch</div>
                	<div class='external-event bg-red'><?php echo lang('trabajos') ?></div>
                	<div class='external-event bg-aqua'>Do homework</div>
                	<div class='external-event bg-yellow'>Work on UI design</div>
                	<div class='external-event bg-navy'>Sleep tight</div>
            	</div>
        	</div>
    	</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="box box-primary">                                
        	<div class="box-body no-padding">
            	<!-- THE CALENDAR -->
                <div id="calendar"></div>
            </div><!-- /.box-body -->
        </div><!-- /. box -->
    </div><!-- /.col -->
</div>
                    
<script>
$(document).ready(function(){
	var su_id = '<?php echo $this->session->userdata("su_id") ?>';
	var eventos = [];
	var primera_vez_sucursales = false;
	
	//EJECUTAR CALENDARIO
	function calendario() {
		$('#calendar').fullCalendar({
			firstDay: 1,
			axisFormat: 'HH:mm',
			header: {
		    	left: 'prev,next today',
		        center: 'title',
		        right: 'month,agendaWeek,agendaDay'
		    },
		    allDayText: '<?php echo lang("all.today") ?>',
		    monthNames: ['<?php echo lang("enero") ?>','<?php echo lang("febrero") ?>','<?php echo lang("marzo") ?>','<?php echo lang("abril") ?>','<?php echo lang("mayo") ?>','<?php echo lang("junio") ?>','<?php echo lang("julio") ?>','<?php echo lang("agosto") ?>','<?php echo lang("septiembre") ?>','<?php echo lang("octubre") ?>','<?php echo lang("noviembre") ?>','<?php echo lang("diciembre") ?>'],
		    monthNamesShort: ['<?php echo lang("ene") ?>','<?php echo lang("feb") ?>','<?php echo lang("mar") ?>','<?php echo lang("abr") ?>','<?php echo lang("may") ?>','<?php echo lang("jun") ?>','<?php echo lang("jul") ?>','<?php echo lang("ago") ?>','<?php echo lang("sep") ?>','<?php echo lang("oct") ?>','<?php echo lang("nov") ?>','<?php echo lang("dic") ?>'],
		    dayNames: ['<?php echo lang("domingo") ?>','<?php echo lang("lunes") ?>','<?php echo lang("martes") ?>','<?php echo lang("miercoles") ?>','<?php echo lang("jueves") ?>','<?php echo lang("viernes") ?>','<?php echo lang("sabado") ?>'],
		    dayNamesShort: ['<?php echo lang("d") ?>','<?php echo lang("l") ?>','<?php echo lang("m") ?>','<?php echo lang("x") ?>','<?php echo lang("j") ?>','<?php echo lang("v") ?>','<?php echo lang("s") ?>'],
		    buttonText: {//This is to add icons to the visible buttons
		    	prev: "<span class='fa fa-caret-left'></span>",
		        next: "<span class='fa fa-caret-right'></span>",
		        today: '<?php echo lang("today") ?>',
		        month: '<?php echo lang("month") ?>',
		        week: '<?php echo lang("week") ?>',
		        day: '<?php echo lang("day") ?>'
			},
		    //Random default events
		    events: eventos
		});
	}
	
	//OBTENEMOS LOS TRABAJOS
	function trabajos() {
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/trabajos/buscador",
			{'buscar':''},
			function(data){
				$.each(data,function(indice) {
					if (data[indice]['su_id'] == su_id) {
						var aux = {
							title: data[indice]['tr_fecha_inicio'] + ' / ' + data[indice]['tr_fecha_fin'],
				        	start: data[indice]['tr_fecha_inicio'],
				        	end: data[indice]['tr_fecha_fin'],
				        	backgroundColor: "#f56954", //red 
				        	borderColor: "#f56954" //red
						};
						
						eventos.push(aux);
					}
				});
				
				calendario();
		}, "json");
	}
	
	
	//RELLENAMOS EL CAMPO DE SUCURSALES PARA AÑADIR O EDITAR
	function sucursales() {
		$('#sucursales').html('');
		
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
			{'buscar':''},
			function(data){
				$.each(data,function(indice) {
					if (data[indice]['su_estado'] == '1') {
						if (data[indice]["su_id"] == su_id) {
							$('#sucursales').append('<option value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
						} else {
							$('#sucursales').append('<option value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
						}
					}
				});
		}, "json");
		
		primera_vez_sucursales = true;
		trabajos();
	}
	
	$("#sucursales").change(function () {
		if (primera_vez_sucursales) {
			$('#calendar').fullCalendar('destroy');
			su_id = $('#sucursales option:selected').val();
			eventos = [];
			trabajos();
		}
	}).trigger('change');
	
	sucursales();	
	
	/*
	backgroundColor: "#f56954", //red 
	borderColor: "#f56954" //red
	
	backgroundColor: "#f39c12", //yellow
	borderColor: "#f39c12" //yellow
	
	backgroundColor: "#0073b7", //Blue
	borderColor: "#0073b7" //Blue
	
	backgroundColor: "#00c0ef", //Info (aqua)
	borderColor: "#00c0ef" //Info (aqua)
	
	backgroundColor: "#00a65a", //Success (green)
	borderColor: "#00a65a" //Success (green)
	
	backgroundColor: "#3c8dbc", //Primary (light-blue)
	borderColor: "#3c8dbc" //Primary (light-blue)
	*/
});
</script>