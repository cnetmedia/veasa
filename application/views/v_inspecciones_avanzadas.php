<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("edificios") ?></th>
                		<th><?php echo lang("plantas") ?></th>
                		<th><?php echo lang("productos") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("edificios") ?></th>
                		<th><?php echo lang("plantas") ?></th>
                		<th><?php echo lang("productos") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//DATOS DE LA SESSION
	var su_id = '<?php echo $this->session->userdata("su_id") ?>';
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	
	//MOSTRAR EMPLEADOS
	function mostrar_inspecciones_avanzadas() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/inspecciones/buscador_avanzadas",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						//MOSTRAMOS TODOS LOS DATOS SI ES SUPERUSUARIO O SOLO AL QUE PERTENECE SI NO LO ES
						/*if (tipo == 0) {
							mostrar = true;
						} else if (su_id == data[indice]['su_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}*/
						
							var pisos = data[indice]['inv_edificio'].split(';');
							
							var plantas = data[indice]['inv_planta'].split('|');
							plantas = plantas.filter(Boolean);
							
							var cont_plantas = plantas.length - 1;
							for (var x=0; x<plantas.length; x++) {
								var aux = plantas[x].split(';');
								aux = aux.filter(Boolean);
								cont_plantas = cont_plantas + aux.length;
							}
							
							var producto = data[indice]['inv_producto'].split('^');
							producto = producto.filter(Boolean);
							var cont_productos = 0;
							for (var x=0; x<producto.length; x++) {
								var aux = producto[x].split('|');
								aux = aux.filter(Boolean);
								for (var y=0; y<aux.length; y++) {
									var aux2 = aux[y].split(';');
									aux2 = aux2.filter(Boolean);
									for (var z=0; z<aux2.length; z++) {
										cont_productos++;
									}
								}
							}
							
							table = table + '<tr id="'+data[indice]['inv_id']+'">';
	  						table = table + '<td>'+data[indice]['inv_nombre']+'</td>';
	  						table = table + '<td>'+(pisos.length-1)+'</td>';
	  						table = table + '<td>'+cont_plantas+'</td>';
	  						table = table + '<td>'+cont_productos+'</td>';
	  						table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  						table = table + '</tr>';
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 0, "asc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-th-large'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					if (tipo > 0) {
						$('#listar table thead tr th:last').hide();
						$('#listar table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#listar table tfoot tr th:last').hide();
					}
			        
			        //PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
					var dep = '<?php echo $this->session->userdata("de_id") ?>';
			        if (crear == '0') {
                        $('#listar #btn_nuevo').hide();
			        } else {
						if ((dep == '3') || (dep == '4') || (dep == '5')) { //Si pertenece a contabilidad, cotizaciones o operaciones no puede crear
							 $('#listar #btn_nuevo').hide();
						}
					}
					
					//NUEVA INSPECCION
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/inspecciones/nueva_inspeccion_avanzada');
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
			        
			        //EDITAR O VER SUCURSAL
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/inspecciones/editar_inspeccion_avanzada/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_inspecciones_avanzadas();
});
</script>