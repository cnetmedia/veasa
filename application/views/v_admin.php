<!DOCTYPE html>
<html lang="<?php echo $this->lang->lang() ?>">
    <head>
        <meta charset="<?php echo $this->config->item('charset');?>" />
        <title><?php echo isset($TITULO)?$TITULO.' - '.$this->session->userdata('em_nombre'):'' ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="robots" content="noindex, nofollow" />
        
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Ionicons -->
        <link href="<?php echo base_url() ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Morris chart -->
        <link href="<?php echo base_url() ?>css/morris/morris.css" rel="stylesheet" type="text/css" />
        
        <!-- jvectormap -->
        <link href="<?php echo base_url() ?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        
        <!-- fullCalendar -->
        <link href="<?php echo base_url() ?>css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        
        <!-- Daterange picker -->
        <link href="<?php echo base_url() ?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo base_url() ?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        
        <!-- Ladda jQuery -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/ladda-themeless.min.css">
        
        <!-- Estilos jQuery Ui -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/themes/base/jquery.ui.all.css">
        
        <!-- FancyBox -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/jquery.fancybox.css">
        
        <!-- Estilo personal -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/style.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/jquery.fancybox-thumbs.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/jquery.fancybox-buttons.css" />
        
        <!-- Select con buscador -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-select.css" />
        
        
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo base_url() ?>js/jquery.min.js"></script>
        
        <!-- jQuery UI 1.10.3 -->
        <script src="<?php echo base_url() ?>js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        
        <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
        
        <!-- Morris.js charts -->
        <script src="<?php echo base_url() ?>js/raphael-min.js"></script>
        <script src="<?php echo base_url() ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
        
        <!-- Sparkline -->
        <script src="<?php echo base_url() ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        
        <!-- jvectormap -->
        <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        
        <!-- fullCalendar -->
        <script src="<?php echo base_url() ?>js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url() ?>js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        
        <!-- daterangepicker -->
        <script src="<?php echo base_url() ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/daterangepicker/moment.js" type="text/javascript"></script>
        
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url() ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url() ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        
        <!-- InputMask -->
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        
        <!-- date-range-picker -->
        <script src="<?php echo base_url() ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url() ?>js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url() ?>js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() ?>js/AdminLTE/app.js" type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!--<script src="<?php echo base_url() ?>js/AdminLTE/dashboard.js" type="text/javascript"></script>--> 
        
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() ?>js/AdminLTE/demo.js" type="text/javascript"></script>
        
        <!-- Ladda jQuery -->
        <script src="<?php echo base_url() ?>js/spin.min.js"></script>
		<script src="<?php echo base_url() ?>js/ladda.min.js"></script>
		
		<!-- Pagination Plugin -->
		<link href="<?php echo base_url() ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url() ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
		
		<!-- ENVIO DE IMAGENES POR AJAX-->
		<script src="<?php echo base_url() ?>js/jquery.form.min.js"></script>
		
		<!-- ALERT BOOTSTRAP -->
		<script src="<?php echo base_url() ?>js/bootbox.min.js"></script>

		<!-- MAPA Y COORDENADAS -->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&language&=<?php echo $this->lang->lang() ?>&file=api&sensor=true&key=<?php echo $this->config->item('key_maps') ?>"></script>
		<script src="<?php echo base_url() ?>js/markerclusterer.js"></script>
		<script src="<?php echo base_url() ?>js/markerwithlabel_packed.js"></script>
		
		<!-- FancyBox -->
		<script src="<?php echo base_url() ?>js/jquery.fancybox.js"></script>
                
        <!-- Select con buscador -->
        <script src="<?php echo base_url() ?>js/bootstrap-select.js"></script>
        
        <!-- Encriptacion AES -->
        <script src="<?php echo base_url() ?>js/aes.js"></script>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?php echo base_url() ?>js/html5shiv.js"></script>
          <script src="<?php echo base_url() ?>js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
    
    <?php
    	$menu = $this->session->userdata('emp_ver');
        $menu = explode(',',$menu);
    ?>
      	
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="#" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <img src="<?php echo base_url() ?>img/Logo_veasa_admin.png" class="icon" />
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-file-text"></i>
                                <span class="label label-success"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"></li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu"></ul>
                                </li>
                                
                                <li class="footer"><a href="<?php echo base_url().$this->lang->lang() ?>/notas"><?php echo lang('ver.todos') ?></a></li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <!-- VER CARRITO -->
                        <?php if (in_array(15,$menu)) { ?>
                        <!--<li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 10 notifications</li>
                                <li>
                                    
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-people info"></i> 5 new members joined today
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-warning danger"></i> Very long description here that may not fit into the page and may cause design problems
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users warning"></i> 5 new members joined
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-cart success"></i> 25 sales made
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-person danger"></i> You changed your username
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">View all</a></li>
                            </ul>
                        </li>-->
                        <?php } ?>
                        <!-- Tasks: style can be found in dropdown.less -->
                        <!--<li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-tasks"></i>
                                <span class="label label-danger">9</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 9 tasks</li>
                                <li>
                                    
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Design some buttons
                                                    <small class="pull-right">20%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">20% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Create a nice theme
                                                    <small class="pull-right">40%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">40% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Some task I need to do
                                                    <small class="pull-right">60%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">60% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Make beautiful transitions
                                                    <small class="pull-right">80%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">80% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">View all tasks</a>
                                </li>
                            </ul>
                        </li>-->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $this->session->userdata('emp_nombre') ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?php echo base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata('em_cif').'/emp/'.$this->session->userdata('emp_dni').'/perfil.jpg'; ?>" class="img" alt="Imagen" />
                                    <p>
                                        <?php
                                        	$apellido2 = ' '.$this->session->userdata('emp_apellido2');
                                        	echo $this->session->userdata('emp_nombre').' '.$this->session->userdata('emp_apellido1').$apellido2;
                                        ?>
                                        <small></small>
                                    </p>
                                </li>
                                
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                    	<?php /*if ($this->session->userdata('emp_editar') == '1') {*/ ?>
                                        	<a href="<?php echo base_url().$this->lang->lang() ?>/empleados/editar_empleado/<?php echo $this->session->userdata('emp_id') ?>"><?php echo lang('perfil') ?></a>
                                        <?php /*}*/ ?>
                                    </div>
                                    
                                    <div class="col-xs-4 text-center">
                                        <?php if ($this->session->userdata('emp_tipo') == '0') { ?>
                                        	<a href="<?php echo base_url().$this->lang->lang() ?>/empresa/editar_empresa"><?php echo lang('empresa') ?></a>
                                        <?php } else if ($this->session->userdata('emp_tipo') == '1') { ?>
                                        	<a href="<?php echo base_url().$this->lang->lang() ?>/sucursales/editar_sucursal/<?php echo $this->session->userdata('su_id') ?>"><?php echo lang('sucursal') ?></a>
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="col-xs-4 text-center">
                                        <a href="<?php echo base_url().$this->lang->lang() ?>/empleados/salir"><?php echo lang('salir') ?></a>
                                    </div>
                                </li>
                                
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left"></div>
                                    
                                    <div class="pull-right"></div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <!--
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url() ?>img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php echo lang('hola').', '.$this->session->userdata('emp_nombre') ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    -->
                    <!-- search form
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    ---- end search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    
                    
                    <ul class="sidebar-menu">
                    	<?php if (in_array(6,$menu) || in_array(7,$menu) || in_array(8,$menu)) { ?>
	                    	<li class="treeview <?php echo isset($EMPRESA)?$EMPRESA:'' ?>">
	                            <a href="#">
	                                <i class="fa fa-sitemap"></i>
	                                <span><?php echo lang('empresa') ?></span>
	                                <i class="fa fa-angle-left pull-right"></i>
	                            </a>
	                            <ul class="treeview-menu">
	                            	<?php if (in_array(6,$menu)) { ?>
	                                	<li class="<?php echo isset($SUCURSALES)?$SUCURSALES:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/sucursales"><i class="fa fa-angle-double-right"></i> <?php echo lang('sucursales') ?></a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(7,$menu)) { ?>
	                                	<li class="<?php echo isset($DEPARTAMENTOS)?$DEPARTAMENTOS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/departamentos"><i class="fa fa-angle-double-right"></i> <?php echo lang('departamentos') ?></a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(8,$menu)) { ?>
	                                	<li class="<?php echo isset($EMPLEADOS)?$EMPLEADOS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/empleados"><i class="fa fa-angle-double-right"></i> <?php echo lang('empleados') ?></a></li>
	                                <?php } ?>
	                            </ul>
	                        </li>
                        <?php } ?>
                        
                        <?php if (in_array(4,$menu)) { ?>
	                        <li class="<?php echo isset($CLIENTES)?$CLIENTES:'' ?>">
	                            <a href="<?php echo base_url().$this->lang->lang() ?>/clientes">
	                                <i class="fa fa-users"></i>
	                                <span><?php echo lang('clientes') ?></span>
	                            </a>
	                        </li>
	                    <?php } ?>
                        
                        <?php if (in_array(5,$menu)) { ?>
	                        <li class="<?php echo isset($MANTENIMIENTOS)?$MANTENIMIENTOS:'' ?>">
	                            <a href="<?php echo base_url().$this->lang->lang() ?>/mantenimientos">
	                                <i class="fa fa-building-o"></i>
	                                <span><?php echo lang('mantenimientos') ?></span>
	                            </a>
	                        </li>
	                    <?php } ?>
                        
                        <?php if (in_array(1,$menu) || in_array(3,$menu)) { ?>
	                        <li class="treeview <?php echo isset($FACTURACION)?$FACTURACION:'' ?>">
	                            <a href="#">
	                                <i class="fa fa-signal"></i>
	                                <span><?php echo lang('facturacion') ?></span>
	                                <i class="fa fa-angle-left pull-right"></i>
	                            </a>
	                            <ul class="treeview-menu">
	                            	<?php if (in_array(1,$menu)) { ?>
	                                	<li class="<?php echo isset($FACTURAS)?$FACTURAS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/facturas"><i class="fa fa-angle-double-right"></i> <?php echo lang('facturas') ?></a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(3,$menu)) { ?>
	                                	<li class="<?php echo isset($PRESUPUESTOS)?$PRESUPUESTOS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/presupuestos"><i class="fa fa-angle-double-right"></i> <?php echo lang('presupuestos') ?></a></li>
	                                <?php } ?>
	                            </ul>
	                        </li>
                        <?php } ?>
                        
                        <?php if (in_array(11,$menu) || in_array(15,$menu)) { ?>

	                        <li class="treeview <?php echo isset($MATERIALES)?$MATERIALES:'' ?>">
	                            <a href="#">
	                                <i class="fa fa-th-large"></i>
	                                <span><?php echo lang('materiales') ?></span>
	                                <i class="fa fa-angle-left pull-right"></i>
	                            </a>
	                            <ul class="treeview-menu">
	                            	<?php if (in_array(11,$menu)) { ?>
	                                	<li class="<?php echo isset($PROVEEDORES)?$PROVEEDORES:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/proveedores"><i class="fa fa-angle-double-right"></i> <?php echo lang('proveedores') ?></a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(15,$menu)) { ?>
	                                	<li class="<?php echo isset($TIENDA)?$TIENDA:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/proveedores/productos"><i class="fa fa-angle-double-right"></i> <?php echo lang('tienda.online') ?></a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(15,$menu)) { ?>
	                                	<li class="<?php echo isset($CARRITO)?$CARRITO:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/proveedores/carrito"><i class="fa fa-angle-double-right"></i> <?php echo lang('carrito') ?></a></li>
	                                <?php } ?>
                                    
                                    <?php if (in_array(15,$menu)) { ?>
	                                	<li class="<?php echo isset($CARRITOS)?$CARRITOS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/proveedores/carritos"><i class="fa fa-angle-double-right"></i> <?php echo lang('carritos') ?></a></li>
	                                <?php } ?>
	                            </ul>
	                        </li>
	                    <?php } ?>
                        
                        <?php if (in_array(14,$menu) || in_array(16,$menu) || in_array(17,$menu) || in_array(18,$menu)) { ?>

	                        <li class="treeview <?php echo isset($STOCKAGE)?$STOCKAGE:'' ?>">
	                            <a href="#">
	                                <i class="fa fa-archive"></i>
	                                <span><?php echo lang('stockage') ?></span>
	                                <i class="fa fa-angle-left pull-right"></i>
	                            </a>
	                            <ul class="treeview-menu">
	                                <?php if (in_array(14,$menu)) { ?>
	                                	<li class="<?php echo isset($ALMACENES)?$ALMACENES:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/almacenes"><i class="fa fa-angle-double-right"></i> <?php echo lang('almacenes') ?></a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(18,$menu)) { ?>
                                        <li class="<?php echo isset($TALLER)?$TALLER:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/taller"><i class="fa fa-angle-double-right"></i> <?php echo lang('taller') ?></a></li>
                                    <?php } ?>

                                    <?php if (in_array(18,$menu)) { ?>
	                                	<li class="<?php echo isset($TALLERES)?$TALLERES:'' ?>">
                                            <a href="<?php echo base_url().$this->lang->lang() ?>/talleres"><i class="fa fa-angle-double-right"></i> <?php echo lang('talleres.guardados') ?>
                                            </a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(16,$menu)) { ?>
	                                <li class="<?php echo isset($ENTRADAS)?$ENTRADAS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/almacenes/entradas"><i class="fa fa-angle-double-right"></i> <?php echo lang('entradas') ?></a></li>
	                                <?php } ?>
									
									<?php if (in_array(16,$menu)) { ?>
	                                <li class="<?php echo isset($ENTRADAS)?$ENTRADAS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/almacenes/entradas/aprobadas"><i class="fa fa-angle-double-right"></i> <?php echo lang('entradas.aprobadas') ?></a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(17,$menu)) { ?>
	                                <li class="<?php echo isset($SALIDAS)?$SALIDAS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/almacenes/salidas"><i class="fa fa-angle-double-right"></i> <?php echo lang('salidas') ?></a></li>
	                                <?php } ?>
	                            </ul>
	                        </li>
	                    <?php } ?>
	                    
	                    <?php if (in_array(9,$menu) || in_array(19,$menu)) { ?>

	                        <li class="treeview <?php echo isset($INSPECCIONES)?$INSPECCIONES:'' ?>">
	                            <a href="#">
	                                <i class="fa fa-th-large"></i>
	                                <span><?php echo lang('inspecciones') ?></span>
	                                <i class="fa fa-angle-left pull-right"></i>
	                            </a>
	                            <ul class="treeview-menu">
	                            	<?php if (in_array(9,$menu)) { ?>
	                                	<li class="<?php echo isset($BASICAS)?$BASICAS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/inspecciones"><i class="fa fa-angle-double-right"></i> <?php echo lang('basicas') ?></a></li>
	                                <?php } ?>
	                                
	                                <?php if (in_array(19,$menu)) { ?>
	                                	<li class="<?php echo isset($AVANZADAS)?$AVANZADAS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/inspecciones/avanzadas"><i class="fa fa-angle-double-right"></i> <?php echo lang('avanzadas') ?></a></li>
	                                <?php } ?>
	                            </ul>
	                        </li>
	                    <?php } ?>
                        
                        <?php if (in_array(10,$menu)) { ?>
                            <li class="treeview <?php echo isset($TRABAJOS)?$TRABAJOS:'' ?>">
                                <a href="#">
                                    <i class="fa fa-gears"></i>
                                    <span><?php echo lang('trabajos') ?></span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <?php if (in_array(10,$menu)) { ?>
                                        <li class="<?php echo isset($TRABAJOS)?$TRABAJOS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/trabajos"><i class="fa fa-angle-double-right"></i> <?php echo lang('trabajos') ?></a></li>
                                    <?php } ?>
                                    
                                    <?php if (in_array(10,$menu)) { ?>
                                        <li class="<?php echo isset($TRABAJOS_GUARDADOS)?$TRABAJOS_GUARDADOS:'' ?>"><a href="<?php echo base_url().$this->lang->lang() ?>/trabajos/guardados"><i class="fa fa-angle-double-right"></i> <?php echo lang('trabajos.guardados') ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
	                    <?php } ?>
                        
                        <?php if (in_array(20,$menu)) { ?>
	                        <li class="<?php echo isset($INFORMES)?$INFORMES:'' ?>">
	                            <a href="<?php echo base_url().$this->lang->lang() ?>/informes">
	                                <i class="fa fa-bar-chart-o"></i>
	                                <span><?php echo lang('informes') ?></span>
	                            </a>
	                        </li>
	                    <?php } ?>
                        
                        <?php if (in_array(13,$menu)) { ?>
	                        <li class="<?php echo isset($CALENDARIO)?$CALENDARIO:'' ?>">
	                            <a href="<?php echo base_url().$this->lang->lang() ?>/calendario">
	                                <i class="fa fa-calendar"></i>
	                                <span><?php echo lang('calendario') ?></span>
	                            </a>
	                        </li>
                        <?php } ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <!-- <h1>
                        <?php echo isset($TITULO)?$TITULO:'' ?>
                        <small></small>
                    </h1> -->
                    <ol class="breadcrumb">
                        <?php 
                        $length = count($BREADCRUMB); $i=1;
                        foreach ($BREADCRUMB as $key => $value) {
                            if($i == $length){
                                echo '<li class="breadcrumb-item active">'.$value['nombre'].'</li>';   
                            }else{
                                echo '<li class="breadcrumb-item"><a href="'.$value['href'].'">'.$value['nombre'].'</a></li>';
                            }
                            $i++;
                        } ?>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">  
                	<?php echo isset($BODY)?$BODY:'' ?>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        
        <!-- AUTOSIZE -->
		<script src="<?php echo base_url() ?>js/jquery.textarea_autosize.js"></script>
        <script>// <![CDATA[
			$(document).ready(function(){			
				$('textarea').textareaAutoSize();
			});	
			// ]]></script>

        <script>
        	//FIXED BODY, PARA DEJAR EL MENU FIJO
        	change_layout();
        	
        	$(document).ready(function(){
        		//MOSTRAR NOTAS SIN LEER
        		//CREAMOS LA FUNCION DE ESTA MANERA PARA PODER SER
        		//LLAMADA DESDE OTRO SCRIPT
				$.notas_no_leidas = function(){
					$.post(
					    "<?php echo base_url().$this->lang->lang() ?>/notas/buscador",
					    {'buscar':''},
					    function(data){
					    	var aux = '';
					    	var emp_id = '<?php echo $this->session->userdata("emp_id") ?>';
					    	var cont = 0;
					  		$.each(data, function (ind, elem) {
					  			//ARRAY DE EMPLEADOS QUE VIERON LA NOTA
					  			var array = data[ind]['no_visto'].split(',');
					  			//PREGUNTA SI ESTE EMPLEADO LA VIO
					  			if ($.inArray(emp_id,array) == -1) {
					  				var mensaje = '';
					  				//CREA UN MENSAJE NO MUY LARGO
					  				if (data[ind]["no_mensaje"].length > 42) {
										mensaje = data[ind]["no_mensaje"].substring(0,42) + '...';
									} else {
										mensaje = data[ind]["no_mensaje"];
									}
					  				//CONCATENA LA NOTA
									aux = aux + '<li><a href="<?php echo base_url().$this->lang->lang() ?>/notas/ver_nota/'+data[ind]["no_id"]+'"><h4>'+data[ind]["no_titulo"]+'</h4><p>'+mensaje+'</p></a></li>';
									cont++;
								}
					  		});
					  		//AÑADE LAS NOTAS
					  		$('.messages-menu ul.menu').html(aux);
					  		$('.messages-menu .label-success').html(cont);
					  		$('.messages-menu .header').html('<?php echo lang("tiene") ?> '+cont+' <?php echo lang("mensajes.sin.leer") ?>');
					    }, "json"
					);
				}
				$.notas_no_leidas();
        	});
        </script>
    </body>
</html>