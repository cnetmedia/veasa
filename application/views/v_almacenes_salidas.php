<script>
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
return ((key >= 48 && key <= 57) || (key==8) || (key == 46));
}
</script>

<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                    	<th><?php echo lang("mantenimiento") ?></th>
                		<th><?php echo lang("fecha") ?></th>
                		<th><?php echo lang("cliente") ?></th>
                		<th><?php echo lang("producto") ?></th>
                		<th><?php echo lang('temporal') ?></th>
                		<th><?php echo lang('reposicion') ?></th>
                		<th><?php echo lang("cantidad") ?></th>
                        <th><?php echo lang("estado") ?></th>
                        <th></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("mantenimiento") ?></th>
                		<th><?php echo lang("fecha") ?></th>
                		<th><?php echo lang("cliente") ?></th>
                		<th><?php echo lang("producto") ?></th>
                		<th><?php echo lang('temporal') ?></th>
                		<th><?php echo lang('reposicion') ?></th>
                		<th><?php echo lang("cantidad") ?></th>
                        <th class="col-md-2"><?php echo lang("estado") ?></th>
                        <th></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
            <?php if (($this->session->userdata('de_id') != '3') && ($this->session->userdata('de_id') != '4')) { ?>
				<button type="button" class="btn btn-warning ladda-button seleccionar_todos" data-style="zoom-out"><i class="fa fa-th-large"></i> Seleccionar todos</button>
				<?php if (($this->session->userdata('de_id') != '2') && ($this->session->userdata('de_id') != '5')) { ?>
					<button type="button" class="btn btn-success ladda-button generar" data-style="zoom-out" value="1"><i class="fa fa-check"></i> Aprobar</button>
					<button type="button" class="btn btn-danger ladda-button generar" value="3" data-style="zoom-out"><i class="fa fa-times"></i> Cancelar</button>
				<?php } ?>
				<button type="button" class="btn btn-primary ladda-button generar" value="2" data-style="zoom-out"><i class="fa fa-truck"></i> Generar Salidas</button>
			<?php } ?>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	//MOSTRAR EMPLEADOS
	function mostrar_salidas() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador_salidas",
		    {'buscar':'','estado':'1'},
		    function(data){
			    var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
			    var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
                            
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE ALMACENES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
                        //if (data[indice]['sa_estado'] < 2) {
                            var man_id = '';
                            if (data[indice]['man_id'] != null) {
                                man_id = data[indice]['man_id'];
                            }
							
							var ta_id = '';
                            if (data[indice]['ta_id'] != null) {
                                ta_id = data[indice]['ta_id'];
                            }
                            
                            var al_id = '';
                            if (data[indice]['al_id'] != null) {
                                al_id = data[indice]['al_id'];
                            }
                            
                            var product_id = '';
                            if (data[indice]['product_id'] != null) {
                                product_id = data[indice]['product_id'];
                            }
                            
                            var al_tipo = '';
                            if (data[indice]['al_tipo'] != null) {
                                al_tipo = data[indice]['al_tipo'];
                            }
                            table = table + '<tr sa_estado="'+data[indice]['sa_estado']+'" man_id="'+man_id+'" ta_id="'+ta_id+'" al_id="'+al_id+'" al_tipo="'+al_tipo+'" product_id="'+product_id+'" id="'+data[indice]['sa_id']+'">';
							var mantenimiento = '';
                            if (data[indice]['man_nombre'] != null) {
                                mantenimiento = data[indice]['man_nombre'];
                            }
							table = table + '<td class="vcenter read_only">'+mantenimiento+'</td>';
							table = table + '<td class="vcenter">'+data[indice]['sa_fecha']+'</td>';
	
                            var cliente = '';
                            if (data[indice]['cl_nombre'] != null) {
                                cliente = data[indice]['cl_nombre'];
                            }
                            table = table + '<td class="vcenter">'+cliente+'</td>';
                            
                            var producto = '';
                            if (data[indice]['ta_trabajo'] == null) {
                                
								if (data[indice]['product_referencia'] != null) {
									producto = '<div><strong>' + data[indice]['product_referencia'] + '</strong></div>';
								}
								if (data[indice]['product_nombre'] != null) {
									producto = producto + data[indice]['product_nombre'];
								}
                                
								var caracteristicas = data[indice]['product_caracteristicas'];
								var opciones = '';
				  				if (caracteristicas != null) {
									caracteristicas = caracteristicas.split(';');
									//ELIMINAR ESPACIOS EN BLANCO
									caracteristicas = caracteristicas.filter(Boolean);
									for (var i=0; i<caracteristicas.length; i++) {
										caracteristicas[i] = caracteristicas[i].split('/');
									}

									for (var i=0; i<caracteristicas.length; i++) {
										if (caracteristicas[i][0].toLowerCase().search("tarifa") == -1) {
											opciones = opciones + '<span class="opc_product_list"><strong>' + caracteristicas[i][0] + '</strong>: <span>' + caracteristicas[i][1] + '</span></span>';
										}
									}
								}
								
                                //producto = producto + '<div>' + '' + '</div>';
                                producto = producto + '<div>' + opciones + '</div>';
                            } else {
                                if (data[indice]['ta_trabajo'].indexOf('|') > -1) {
                                    var trabajo = data[indice]['ta_trabajo'].split('|');
                                    trabajo = trabajo.filter(Boolean);
                                    producto = '<span class="compuesto_padre">'+trabajo[0]+'</span>: <span class="compuesto_item">'+trabajo[1]+'</span>';
                                } else {
                                    producto = data[indice]['ta_trabajo'];
                                }
                            }
                            
                            table = table + '<td class="vcenter">'+producto+'</td>';
                            
                            if (data[indice]['sa_temporal'] == 1) {
								table = table + '<td class="text-center vcenter"><i class="fa fa-check"></i> <?php echo lang("si") ?></td>';
								
							} else {
								table = table + '<td></td>';
							}
							
							if (data[indice]['ta_reposicion'] == 1) {
								table = table + '<td class="text-center vcenter"><i class="fa fa-check"></i> <?php echo lang("si") ?></td>';
								
							} else {
								table = table + '<td></td>';
							}
	
							table = table + '<td class="text-center vcenter">'+data[indice]['sa_cantidad']+'</td>';
                            
                            var estado = '';
							var dep = '<?php echo $this->session->userdata("de_id") ?>';	
								
							if (tipo <= 1) {
								if ((dep != '3') && (dep != '4') && (dep != '5')) {
									if (data[indice]['sa_estado'] == 0) {
										estado = '<div class="btn-group-horizontal text-center"><button value="1" type="button" class="btn btn-success aprobar" title="<?php echo lang("aprobar") ?>"><i class="fa fa-check"></i></button><button value="3" type="button" class="btn btn-danger cancelar" title="<?php echo lang("cancelar") ?>"><i class="fa fa-times"></i></button><button value="2" type="button" class="btn btn-primary enviar" title="<?php echo lang("enviar") ?>"><i class="fa fa-truck"></i></button></div>';
									} else if (data[indice]['sa_estado'] == 1) {
										estado = '<div class="btn-group-horizontal text-center"><button value="3" type="button" class="btn btn-danger cancelar" title="<?php echo lang("cancelar") ?>"><i class="fa fa-times"></i></button><button value="2" type="button" class="btn btn-primary enviar" title="<?php echo lang("enviar") ?>"><i class="fa fa-truck"></i></button></div>';
									} else if (data[indice]['sa_estado'] == 3) {
										estado = '<div class="btn-group-horizontal text-center"><button value="1" type="button" class="btn btn-success aprobar" title="<?php echo lang("aprobar") ?>"><i class="fa fa-check"></i></button><button value="2" type="button" class="btn btn-primary enviar" title="<?php echo lang("enviar") ?>"><i class="fa fa-truck"></i></button></div>';
									}
								}
							} else if (tipo == 2) {
								if (data[indice]['sa_estado'] == 1) {
									if ((dep != '3') && (dep != '4')) {
										estado = '<div class="btn-group-horizontal text-center"><button value="2" type="button" class="btn btn-primary enviar" title="<?php echo lang("enviar") ?>"><i class="fa fa-truck"></i></button></div>';
									}
								} else if (data[indice]['sa_estado'] == 0){
									estado = '<i class="fa fa-clock-o" title="<?php echo lang("pendiente") ?>"></i>';
								}
							}
							
                            table = table + '<td class="vcenter">'+estado+'</td>';
							table = table + '<td class="vcenter"><input type="checkbox" readonly="readonly" name="product_id[]" value="'+data[indice]['sa_id']+'" /></td>';
	  					table = table + '</tr>';
                        //}
					});
                                        
					$('#listar tbody').html(table);
					
					
                                        
					$("#example1").dataTable({
						"aaSorting": [[ 1, "desc"]],
						"iDisplayLength": 50,
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-archive'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					}).rowGrouping({
						bExpandableGrouping: true,
						bExpandSingleGroup: false,
						iExpandGroupOffset: -1,
						asExpandedGroups: [""]
					});
					
					GridRowCount();
				
					function GridRowCount() {
						$('span.rowCount-grid').remove();
						$('input.expandedOrCollapsedGroup').remove();
			
						$('.dataTables_wrapper').find('[id|=group-id]').each(function () {
							$(this).addClass('group');
							var rowCount = $(this).nextUntil('[id|=group-id]').length;
							$(this).find('td').append($('<span />', { 'class': 'rowCount-grid' }).append($('<b />', { 'text': ' ('+rowCount+')' })));
						});
			
						$('.dataTables_wrapper').find('.dataTables_filter').append($('<input />', { 'type': 'button', 'class': 'expandedOrCollapsedGroup collapsed', 'value': 'Expanded All Group' }));
			
						
					};
					
			        //PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
			        var dep = '<?php echo $this->session->userdata("de_id") ?>';
			        var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					if (crear == '0') {
                        $('#listar #btn_nuevo').hide();
			        } else if ((dep == '3')||(dep == '4')||(dep == '5')) { //Si pertenece a contabilidad o a cotizaciones no puede crear
						$('#listar #btn_nuevo').hide();
					} else if (tipo > 0){
						$('#listar #btn_nuevo').hide();
					}
					
					//SI PERTENECE A CONTABILIDAD, COTIZACIONES O PUERTA NO SE LES MUESTRA LA COLUMNA DE ESTADO Y CHECK
					/*if ((dep == '3')||(dep == '4')||(dep == '5')) {
						$('#listar table thead tr th:last').hide();
						$('#listar table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#listar table tfoot tr th:last').hide();
					}
					
					$("#listar table, #listar table thead tr th").removeAttr('style');*/
                                
                                //CAMBIAR ESTADOS DE LA SALIDA
                                var estado_salida = '';
                                var sa_id = '';
                                var sa_cancelacion_salida = '';
                                var man_id = '';
								var al_id = '';
                                var ta_id = '';
                                var product_id = '';
                                var sa_cantidad = '';
                                var ta_trabajo = '';
                                var al_tipo = '';
                                var compuesto_padre = '';
                                var sa_temporal = '';
                                
                                function cambiar_estado() {
                                    var datos = {
                                        'sa_estado' : estado_salida,
                                        'sa_id' : sa_id,
                                        'sa_cancelacion_salida' : sa_cancelacion_salida,
                                        'man_id' : man_id,
										'al_id' : al_id,
                                        'ta_id' : ta_id,
                                        'product_id' : product_id,
                                        'sa_cantidad' : sa_cantidad,
                                        'ta_trabajo' : ta_trabajo,
                                        'al_tipo' : al_tipo,
                                        'compuesto_padre' : compuesto_padre,
                                        'sa_temporal' : sa_temporal
                                    };
                                    
                                    $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/cambiar_estados_salidas",
                                        data:datos,
                                        success: function(data) {
                                            if (data.sql) {
                                                //PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
                                                if (data.status) {
                                                    bootbox.alert('<?php echo lang("salida.editada.ok") ?>');
                                                   setTimeout("location.href='"+location+"'", 1000);
                                                } else {
                                                    bootbox.alert('<?php echo lang("error.ajax") ?>');
                                                }
                                            } else {
                                                bootbox.alert('<?php echo lang("error.ajax") ?>');
                                            }
                                        },
                                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                                            bootbox.alert('<?php echo lang("error.ajax") ?>');
                                        }
                                    });
                                }
                                
                                $(document).on('click',"#listar tr button",function(e){
                                    estado_salida = '';
                                    sa_id = '';
                                    sa_cancelacion_salida = '';
                                    al_id = '';
                                    ta_id = '';
                                    product_id = '';
                                    sa_cantidad = '';
                                    ta_trabajo = '';
                                    al_tipo = '';
                                    compuesto_padre = '';
                                    man_id = '';
                                    
                                    estado_salida = $(this).attr('value');
                                    sa_id = $(this).parent().parent().parent().attr('id');
                                    al_id = $(this).parent().parent().parent().attr('al_id');
                                    ta_id = $(this).parent().parent().parent().attr('ta_id');
                                    man_id = $(this).parent().parent().parent().attr('man_id');
                                    product_id = $(this).parent().parent().parent().attr('product_id');
                                    sa_cantidad = $(this).parent().parent().prev().html();
                                    
                                    //alert('col: '+ $(this).parent().parent().prev().prev().prev().html());
									if ($(this).parent().parent().prev().prev().prev().html() != '') {
										sa_temporal = 1;
									} else {
										sa_temporal = 0;
									}
                                    
                                    
                                    if (product_id == '') {
                                        ta_trabajo = $(this).parent().parent().prev().prev().find('.compuesto_item').html();
                                        compuesto_padre = $(this).parent().parent().prev().prev().find('.compuesto_padre').html();
                                    } else {
                                       al_tipo = $(this).parent().parent().parent().attr('al_tipo'); 
                                    }
                                    
                                    if (estado_salida == 3) {
                                        bootbox.dialog({
                                            message: "<div id='observaciones'><label><?php echo lang('observaciones.salida.cancelada') ?></label><textarea name='sa_cancelacion_salida' class='form-control'></textarea></div>",
                                            buttons: {
                                                success: {
                                                    label: "<?php echo lang('cancelar') ?>",
                                                    className: "btn-danger",
                                                    callback: function() {
                                                            sa_cancelacion_salida = $('#observaciones textarea').val();
                                                            cambiar_estado();
                                                    }
                                                }
                                            }
                                        });
                                    } else {
                                        cambiar_estado()
                                    }
                                });
								
								$(document).on('click',".generar",function(e){
									
									estado_salida = $(this).attr('value');
									
									sa_id = [];
									al_id = [];
									ta_id = [];
									product_id = [];
									sa_cantidad = [];
									ta_trabajo = [];
									al_tipo = [];
									compuesto_padre = [];
									sa_temporal = [];
									
									sa_cancelacion_salida = '';
									
									var count = 0;
										
									$('#example1 tbody tr').find('input:checkbox').each(function(index, element) {
										var $row = $(this).parent().parent();
										if(this.checked) {
											if($row.attr('sa_estado') != estado_salida) {
												
												sa_id.push($row.attr('id'));
												al_id.push($row.attr('al_id'));
												ta_id.push($row.attr('ta_id'));
												product_id.push($row.attr('product_id'));
												sa_cantidad.push($(this).parent().prev().prev().html());
												
												//alert('col: '+ $(this).parent().prev().prev().prev().prev().html());
												if ($(this).parent().prev().prev().prev().prev().html() != '') {
													sa_temporal.push(1);
												} else {
													sa_temporal.push(0);
												}
												
												
												if (product_id == '') {
													ta_trabajo.push($(this).parent().prev().prev().find('.compuesto_item').html());
													compuesto_padre.push($(this).parent().prev().prev().find('.compuesto_padre').html());
												} else {
												   al_tipo.push($row.attr('al_tipo')); 
												}
												
											}
											count++;
										}
									});
									
									if(count>0) {
										if (estado_salida == 3) {
											bootbox.dialog({
												message: "<div id='observaciones'><label><?php echo lang('observaciones.salida.cancelada') ?></label><textarea name='sa_cancelacion_salida' class='form-control'></textarea></div>",
												buttons: {
													success: {
														label: "<?php echo lang('cancelar') ?>",
														className: "btn-danger",
														callback: function() {
																sa_cancelacion_salida = $('#observaciones textarea').val();
																cambiar_estado();
														}
													}
												}
											});
										} else {
											//alert(JSON.stringify(sa_id) + ">>" + JSON.stringify(al_id) + ">>" + JSON.stringify(ta_id) + ">>" + JSON.stringify(product_id) + ">>" + JSON.stringify(sa_cantidad) + ">>"  + JSON.stringify(sa_temporal)); 								
											cambiar_estado()
										}
									}
												
								});
								
                                
                                //NUEVA SALIDA
				$("#btn_nuevo").click(function(e){
                                    e.preventDefault();
                                    
                                    //RELLENAMOS EL CAMPO DE MANTENIMIENTOS
                                    $.post(
                                    "<?php echo base_url().$this->lang->lang() ?>/mantenimientos/buscador",
                                    {'buscar':''},
                                    function(data){
                                            var mantenimientos = '';
        
                                            $.each(data,function(indice) {
                                                    if (indice == 0) {
                                                        mantenimientos = mantenimientos + '<option value="'+data[indice]["man_id"]+'" selected>'+data[indice]["man_nombre"]+' ('+data[indice]["cl_nombre"]+')</option>';
                                                    } else {
                                                        mantenimientos = mantenimientos + '<option value="'+data[indice]["man_id"]+'">'+data[indice]["man_nombre"]+' ('+data[indice]["cl_nombre"]+')</option>';
                                                    }
                                            });
											
											//RELLENAMOS EL CAMPO DE MANTENIMIENTOS
											$.post(
											"<?php echo base_url().$this->lang->lang() ?>/almacenes/buscar_materiales_almacenes",
											{'buscar':''},
											function(data){
													var materiales = '';
													
													var datos = {
														'product_id' : [],
														'pra_id' : [],
														'al_id' : [],
														'al_tipo' : [],
														'product_cantidad' : [],
														'product_nombre' : [],
														'product_referencia' : [],
														'product_precio' : [],
														'pr_id' : [],
														'pe_observaciones_empresa' : [],
														'product_tienda' : 0
													};
				
													$.each(data,function(indice) {
														
														if(data[indice]["pra_caracteristicas"]) {
															var caracteristicas = data[indice]["pra_caracteristicas"];
														} else {
															var caracteristicas = data[indice]["product_caracteristicas"];
														}
														
														if(data[indice]["pra_id"]) {
															var pra_id = data[indice]["pra_id"];
														} else {
															var pra_id = 0;
														}
														   materiales = materiales + '<option value="'+data[indice]["product_id"]+'" al_id="'+data[indice]["al_id"]+'" pra_id="'+pra_id+'" al_tipo="'+data[indice]["al_tipo"]+'">'+data[indice]["product_nombre"]+' ('+data[indice]["product_referencia"]+') | '+data[indice]["al_nombre"]+' | '+caracteristicas+'</option>';
															
													});

                                            bootbox.dialog({
                                                message: "<div id='new_salida'><label><?php echo lang('mantenimiento') ?></label><select id='man_id' class='selectpicker form-control' data-live-search='true'>"+mantenimientos+"</select><br><br><label><?php echo lang('producto') ?></label><br><select id='productos' class='selectpicker form-control' data-live-search='true' name='product_id'>"+materiales+"</select><script>$(document).ready(function(){$('#productos').selectpicker();});<\/script><br><br><label><?php echo lang('descripcion') ?></label><input type='text' name='ta_trabajo' id='ta_trabajo' value='' class='form-control'><br><label><?php echo lang('cantidad') ?></label><input type='text' name='sa_cantidad' id='sa_cantidad' value='' class='form-control' onKeypress='return soloNumeros(event)'><br><b><?php echo lang('salida.temporal') ?> <input type='checkbox' id='sa_temporal' style='margin-left: 10px;' name='sa_temporal'></b></div>",
                                                buttons: {
                                                        success: {
                                                                label: "<?php echo lang('guardar') ?>",
                                                                className: "btn-success",
                                                                callback: function() {
                                                                	
                                                                	var sa_temporal = 0;
                                                                	if ($('#sa_temporal').is(':checked')) {
														sa_temporal = 1;
													}
																	
                                                                	
                                                                    var datos = {
                                                                        'man_id':$('#man_id option:selected').val(),
                                                                        'descripcion':$('#new_salida #ta_trabajo').val(),
																		'product_id':$('#new_salida #productos').val(),
                                                                        'sa_cantidad':$('#new_salida #sa_cantidad').val(),
																		'pra_id':$('#new_salida #productos option:selected').attr('pra_id'),
                                                                        'sa_temporal' : sa_temporal
                                                                    };
                                                                    
                                                                    $.ajax({
                                                                        type: "POST",
                                                                        dataType: "json",
                                                                        url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/salida_manual",
                                                                        data:datos,
                                                                        success: function(data) {
                                                                            if (data.sql) {
                                                                                //PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
                                                                                if (data.status) {
                                                                                    bootbox.alert('<?php echo lang("salida.manual.ok") ?>');
                                                                                    setTimeout("location.href='"+location+"'", 1500);
                                                                                } else {
                                                                                    bootbox.alert('<?php echo lang("error.ajax") ?>');
                                                                                }
                                                                            } else {
                                                                                bootbox.alert('<?php echo lang("error.ajax") ?>');
                                                                            }
                                                                        },
                                                                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                        bootbox.alert('<?php echo lang("error.ajax") ?>');
                                                                        }
                                                                });
                                                                }
                                                        },
                                                        cancel: {
                                                                label: "<?php echo lang('cancelar') ?>",
                                                                className: "btn-default"
                                                        }
                                                }
                                            });
                                            
                                            $('#man_id').selectpicker();
                                    }, "json");
					}, "json");
					});
					
					//SELECCIONAR TODOS
					$(document).on('click','.seleccionar_todos',function(e){
						e.preventDefault();
						$('#example1 tbody tr:visible').find('input:checkbox').each(function(index, element) {
                            this.checked = true;
                        });
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
					
					$("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
					$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
		
					$("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(this).find(':checkbox').each(function(index, element) {
							if(this.checked) {
								this.checked = false;
							} else {
								this.checked = true;
							}
						});
					});
					
					$("#example1").on("click", "td:last-child", function(e) {
						e.stopPropagation();
					});
				}
		    }, "json");
	}
	
	mostrar_salidas();
});
</script>