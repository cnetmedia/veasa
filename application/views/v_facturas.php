<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("numero.corto") ?></th>
                		<th><?php echo lang("fecha") ?></th>
                		<th><?php echo lang("cliente") ?></th>
                        <th><?php echo lang("mantenimiento") ?></th>
                		<th><?php echo lang("importe") ?></th>
						<th><?php echo lang("rectificativa.col") ?></th>
						<th><?php echo lang("estado") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("numero.corto") ?></th>
                		<th><?php echo lang("fecha") ?></th>
                		<th><?php echo lang("cliente") ?></th>
                        <th><?php echo lang("mantenimiento") ?></th>
                		<th><?php echo lang("importe") ?></th>
						<th><?php echo lang("rectificativa.col") ?></th>
						<th><?php echo lang("estado") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//MOSTRAR EMPLEADOS
	function mostrar_facturas() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/facturas/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						var su_id = '<?php echo $this->session->userdata("su_id") ?>';
						var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						/*var mostrar = false;
						if (tipo == 0) {
							mostrar = true;
						} else if (su_id == data[indice]['su_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}*/
						
							table = table + '<tr id="'+data[indice]['fa_id']+'">';
	  						table = table + '<td>'+data[indice]['fa_numero']+'</td>';
	  						table = table + '<td>'+data[indice]['fa_fecha']+'</td>';
	  						table = table + '<td>'+data[indice]['cl_nombre']+'</td>';
                            table = table + '<td>'+data[indice]['man_nombre']+'</td>';
	  						
							var total = (data[indice]['re_numero'] !== null) ? data[indice]['re_total'] : data[indice]['fa_total'];
							table = table + '<td>'+total+' '+data[indice]['currrency_symbol']+'</td>';
							
							var rect = (data[indice]['re_numero'] !== null) ? '<i class="fa fa-check"></i>' : '';
							table = table + '<td>'+rect+'</td>';
							
							var estado;
	  						if (data[indice]['fa_estado'] == 0) {
								estado = '<span class="label label-info"><?php echo lang("pendiente") ?></span>';
							} else if (data[indice]['fa_estado'] == 1) {
								estado = '<span class="label label-primary"><?php echo lang("aprobada") ?></span>';
							} else if (data[indice]['fa_estado'] == 2) {
								estado = '<span class="label label-warning"><?php echo lang("caliente") ?></span>';
							} else if (data[indice]['fa_estado'] == 3) {
								estado = '<span class="label label-danger"><?php echo lang("cancelada") ?></span>';
							} else if (data[indice]['fa_estado'] == 4) {
								estado = '<span class="label label-danger"><?php echo lang("anulada") ?></span>';
							}
	  						
	  						table = table + '<td class="text-center">'+estado+'</td>';
	  						table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  						table = table + '</tr>';
					});
					$('#listar tbody').html(table);
					
					$("#example1").dataTable({
						"aaSorting": [[ 1, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-signal'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					if (tipo > 0) {
						$('#listar table thead tr th:last').hide();
						$('#listar table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#listar table tfoot tr th:last').hide();
					}
			        
			        //PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
			        //if (crear == '0') {
			        	$('#listar #btn_nuevo').hide();
			        //}
					
					//NUEVA FACTURA
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/facturas/nueva_factura');
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					    window.location.reload(true); 
					});
			        
			        //EDITAR O VER SUCURSAL
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/facturas/editar_factura/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_facturas();
});
</script>