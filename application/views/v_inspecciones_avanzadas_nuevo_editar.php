<div id="inspeccion">
	<?php 
	$input_nombre = array(
		'name'		=>	'inv_nombre',
		'id'		=>	'inv_nombre',
		'class'		=>	'form-control',
		'maxlength'	=>	'150',
		'value'		=>	isset($inv_nombre)?$inv_nombre:set_value('inv_nombre')
	);
	?>
	
	<div class="row">
		<div class="col-lg-3">
			<div class="form-group">
				<label><?php echo lang('nombre') ?></label>
				<?php echo form_input($input_nombre) ?>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('inv_nombre') ?></div>
			</div>
		</div>
		
		<div class="col-lg-3">
			<div class="form-group">
				<label><?php echo lang('sucursal') ?></label>
				<select id="sucursales" name="su_id" class="form-control"></select>
				<span class="glyphicon glyphicon-remove form-control-feedback"></span>
				<div class="text-danger"><?php echo form_error('su_id') ?></div>
			</div>
		</div>
	</div>
	
	<div id="opciones" class="row"></div>
	
	<div class="row">
		<div class="col-lg-12 text-right">
			
		<?php if (($this->session->userdata('de_id') != '3') && ($this->session->userdata('de_id') != '4') && ($this->session->userdata('de_id') != '5')) { ?>	
			<button id="eliminar_inspeccion" type="button" class="btn btn-danger ladda-button" data-style="zoom-out"><i class="fa fa-times"></i> <?php echo lang('eliminar.inspeccion') ?>
			</button>
			
			<button id="nuevo_edificio" type="button" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-building-o"></i> <?php echo lang('nuevo.edificio.local') ?>
			</button>
			
			<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
				<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
			</button>
		<?php } ?>
			
			<a href="<?php echo base_url().$this->lang->lang().'/inspecciones/avanzadas' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
		</div>
	</div>
</div>


<script>
$(document).ready(function(){
	//VARIABLES SESSION EMPLEADO
	var su_id = '<?php echo $this->session->userdata("su_id") ?>';
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	var inv_id = '<?php echo isset($inv_id)?$inv_id:"" ?>';
	var emp_editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	
	if (emp_editar == 0) {
		$('#nuevo_grupo, #btn_guardar').hide();
	}
	
	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	$("#inspeccion .form-group, #inspeccion .input-group").removeClass("has-error has-feedback");
	$('#inspeccion .glyphicon-remove').hide();
	$("#inspeccion .text-danger").html('');
	
	//RELLENAMOS EL CAMPO DE SUCURSALES PARA A?ADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
				if (data[indice]['su_estado'] == '1') {
					if (data[indice]["su_id"] == '<?php echo isset($su_id)?$su_id:"" ?>') {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
						mostrar = false;
					} else if (data[indice]["su_id"] == su_id) {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
					} else if (data[indice]["su_estado"] == 1) {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
					}
				}
		});
	}, "json");
	
	//CONTADOR PARA LOS GRUPOS
	var cont_edificio = 0;
	//CONTADOR PARA LOS GRUPOS
	var cont_plant = 0;
	
	//A�ADIR NUEVA RESPUESTA
	function add_respuestas(id_edificio,cont_plant,cont_product) {
	
		var aux = '<div class="col-lg-1"><div class="input-group respuesta"><label><?php echo lang("suministro") ?></label><input class="form-control" type="text" name="respuesta" readonly ></div></div>';
		aux = aux + '<div class="col-lg-1"><div class="input-group respuesta"><label><?php echo lang("marco") ?></label><input class="form-control" type="text" name="respuesta" readonly ></div></div>';
		aux = aux + '<div class="col-lg-1"><div class="input-group respuesta"><label><?php echo lang("hoja") ?></label><input class="form-control" type="text" name="respuesta"  readonly ></div></div>';
		aux = aux + '<div class="col-lg-1"><div class="input-group respuesta"><label><?php echo lang("silicon") ?></label><input class="form-control" type="text" name="respuesta"  readonly ></div></div>';
		aux = aux + '<div class="col-lg-1"><div class="input-group respuesta"><label><?php echo lang("limpieza") ?></label><input class="form-control" type="text" name="respuesta" readonly ></div></div>';
		
		aux = aux + '<div class="box-tools pull-right"><button class="btn btn-default btn-sm copiar_producto" title="<?php echo lang("copiar.producto") ?>"><i class="fa fa-files-o"></i></button> <button class="btn btn-default btn-sm borrar_producto" title="<?php echo lang("borrar.producto") ?>"><i class="fa fa-times"></i></button> <button class="btn btn-default btn-sm encoger_producto"><i class="fa fa-minus"></i></button></div></div></div></div>';
		
		$('#opciones #'+id_edificio+' .plant'+cont_plant+' .product'+cont_product+' .box-header').append(aux);
	}
	
	//A�ADIR UN PRODUCTO
	function add_producto(id_edificio, cont_plant) {
		var cont_product = parseInt($("#opciones #"+id_edificio+" .plant"+cont_plant+" .productos").length) + 1;
		
		var aux = '<div class="col-lg-12"><div class="box productos product'+cont_product+'"><div class="box-header">';
		
		aux = aux + '<div class="col-lg-1"><div class="form-group "><label><?php echo lang("tipo") ?></label><input name="producto" class="form-control" type="text" placeholder="<?php echo lang("nuevo.producto") ?>"></div></div>';
		aux = aux + '<div class="col-lg-2"><div class="form-group"><label><?php echo lang("descripcion") ?></label><input name="descripcion" class="form-control" type="text" placeholder="<?php echo lang("descripcion") ?>"></div></div>';
		aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("ancho") ?></label><input name="ancho" class="form-control" type="text" placeholder="<?php echo lang("ancho") ?>"></div></div>';
		aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("alto") ?></label><input name="alto" class="form-control" type="text" placeholder="<?php echo lang("alto") ?>"></div></div>';
		aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("cantidad") ?></label><input name="cantidad" class="form-control" type="text" placeholder="<?php echo lang("cantidad") ?>"></div></div>';
		aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("monto") ?></label><input name="monto" class="form-control" type="text" placeholder="<?php echo lang("monto") ?>"></div></div>';
		
		$('#opciones #'+id_edificio+' .plant'+cont_plant+' .body_planta').append(aux);
		
		add_respuestas(id_edificio,cont_plant,cont_product);
	}
	
	//A�ADIR UNA PLANTA
	function add_planta(id_edificio) {
		var cont_plant = $("#opciones #"+id_edificio+" .body_edificio .plantas").length;
		
		var aux = '<div class="box box-solid box-info plantas plant'+cont_plant+'"><div class="box-header"><h3 class="box-title"><input name="planta" class="form-control" type="text" placeholder="<?php echo lang("nueva.planta") ?>"></h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm nuevo_producto" title="<?php echo lang("nuevo.producto") ?>"><i class="fa fa-tag"></i></button> <button class="btn btn-primary btn-sm copiar_planta" title="<?php echo lang("copiar.planta") ?>"><i class="fa fa-clipboard"></i></button> <button class="btn btn-primary btn-sm borrar_planta" title="<?php echo lang("borrar.planta") ?>"><i class="fa fa-times"></i></button> <button class="btn btn-primary btn-sm encoger_planta"><i class="fa fa-minus"></i></button></div></div><div class="box-body"><div class="row body_planta"></div></div></div>';
		
		$('#opciones #'+id_edificio+' .body_edificio').append(aux);
		add_producto(id_edificio, cont_plant);
	}
	
	//A�ADIR UN NUEVO PISO O LOCAL
	function add_edificio() {
		var aux = '<div class="col-lg-12 box box-solid box-primary edificios" id="'+cont_edificio+'"><div class="box-header"><h3 class="box-title" style="width: 50%;"><input name="edificio" class="form-control" type="text" placeholder="<?php echo lang("nuevo.edificio.local") ?>"></h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm nueva_planta" title="<?php echo lang("nueva.planta") ?>"><i class="fa fa-bars"></i></button> <button class="btn btn-primary btn-sm copiar_edificio" title="<?php echo lang("copiar.edificio.local") ?>"><i class="fa fa-clipboard"></i></button> <button class="btn btn-primary btn-sm borrar_edificio" title="<?php echo lang("borrar.edificio.local") ?>"><i class="fa fa-times"></i></button> <button class="btn btn-primary btn-sm encoger_edificio"><i class="fa fa-minus"></i></button></div></div><div class="box-body"><div class="body_edificio"></div></div></div>';
		
		$('#opciones').append(aux);
		add_planta(cont_edificio);
		cont_edificio++;
	}
	
	//NUEVA INSPECCION AVANZADA
	if (inv_id == '') {
		add_edificio();
	} else {
	//EDITAR INSPECCION
	//RECOGEMOS LOS DATOS
		var inv_edificio = '<?php echo isset($inv_edificio)?$inv_edificio:"" ?>';
		inv_edificio = inv_edificio.split(';');
		inv_edificio = inv_edificio.filter(Boolean);
		
		var inv_planta = '<?php echo isset($inv_planta)?$inv_planta:"" ?>';
		inv_planta = inv_planta.split('|');
		inv_planta = inv_planta.filter(Boolean);
		
		var inv_producto = '<?php echo isset($inv_producto)?$inv_producto:"" ?>';
		inv_producto = inv_producto.split('^');
		inv_producto = inv_producto.filter(Boolean);
		
		var inv_descripcion = '<?php echo isset($inv_descripcion)?$inv_descripcion:"" ?>';
		inv_descripcion = inv_descripcion.split('^');
		inv_descripcion = inv_descripcion.filter(Boolean);
		
		var inv_ancho = '<?php echo isset($inv_ancho)?$inv_ancho:"" ?>';
		inv_ancho = inv_ancho.split('^');
		inv_ancho = inv_ancho.filter(Boolean);
		
		var inv_alto = '<?php echo isset($inv_alto)?$inv_alto:"" ?>';
		inv_alto = inv_alto.split('^');
		inv_alto = inv_alto.filter(Boolean);
		
		var inv_cantidad = '<?php echo isset($inv_cantidad)?$inv_cantidad:"" ?>';
		inv_cantidad = inv_cantidad.split('^');
		inv_cantidad = inv_cantidad.filter(Boolean);
		
		var inv_monto = '<?php echo isset($inv_monto)?$inv_monto:"" ?>';
		inv_monto = inv_monto.split('^');
		inv_monto = inv_monto.filter(Boolean);
		
		var inv_respuesta = '<?php echo isset($inv_respuesta)?$inv_respuesta:"" ?>';
		inv_respuesta = inv_respuesta.split('^');
		inv_respuesta = inv_respuesta.filter(Boolean);
		
		var aux = '';
		for (var e=0; e<inv_edificio.length; e++) {
			//ABRIR EDIFICIO
			aux = aux + '<div class="col-lg-12 box box-solid box-primary edificios" id="'+cont_edificio+'"><div class="box-header"><h3 class="box-title" style="width: 50%;"><input name="edificio" class="form-control" type="text" value="'+inv_edificio[e]+'" placeholder="<?php echo lang("nuevo.edificio.local") ?>"></h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm nueva_planta" title="<?php echo lang("nueva.planta") ?>"><i class="fa fa-bars"></i></button> <button class="btn btn-primary btn-sm copiar_edificio" title="<?php echo lang("copiar.edificio.local") ?>"><i class="fa fa-clipboard"></i></button> <button class="btn btn-primary btn-sm borrar_edificio" title="<?php echo lang("borrar.edificio.local") ?>"><i class="fa fa-times"></i></button> <button class="btn btn-primary btn-sm encoger_edificio"><i class="fa fa-minus"></i></button></div></div><div class="box-body"><div class="body_edificio">';
			
			var aux_planta = inv_planta[e].split(';');
			aux_planta = aux_planta.filter(Boolean);
			
			var aux_producto = inv_producto[e].split('|');
			aux_producto = aux_producto.filter(Boolean);
			
			var aux_descripcion = inv_descripcion[e].split('|');
			aux_descripcion = aux_descripcion.filter(Boolean);
			
			var aux_ancho = inv_ancho[e].split('|');
			aux_ancho = aux_ancho.filter(Boolean);
			
			var aux_alto = inv_alto[e].split('|');
			aux_alto = aux_alto.filter(Boolean);
			
			var aux_cantidad = inv_cantidad[e].split('|');
			aux_cantidad = aux_cantidad.filter(Boolean);
			
			var aux_monto = inv_monto[e].split('|');
			aux_monto = aux_monto.filter(Boolean);
			
			var aux_respuesta = inv_respuesta[e].split('|');
			aux_respuesta = aux_respuesta.filter(Boolean);
			
			for (var p=0; p<aux_planta.length; p++) {
				//ABRIMOS LAS PLANTAS
				aux = aux + '<div class="box box-solid box-info plantas plant'+p+'"><div class="box-header"><h3 class="box-title"><input name="planta" class="form-control" type="text" value="'+aux_planta[p]+'" placeholder="<?php echo lang("nueva.planta") ?>"></h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm nuevo_producto" title="<?php echo lang("nuevo.producto") ?>"><i class="fa fa-tag"></i></button> <button class="btn btn-primary btn-sm copiar_planta" title="<?php echo lang("copiar.planta") ?>"><i class="fa fa-clipboard"></i></button> <button class="btn btn-primary btn-sm borrar_planta" title="<?php echo lang("borrar.planta") ?>"><i class="fa fa-times"></i></button> <button class="btn btn-primary btn-sm encoger_planta"><i class="fa fa-minus"></i></button></div></div><div class="box-body"><div class="row body_planta">';
				
				var aux2_producto = aux_producto[p].split(';');
				aux2_producto = aux2_producto.filter(Boolean);
				
				var aux2_descripcion = aux_descripcion[p].split(';');
				aux2_descripcion = aux2_descripcion.filter(Boolean);
				
				var aux2_ancho = aux_ancho[p].split(';');
				aux2_ancho = aux2_ancho.filter(Boolean);
				
				var aux2_alto = aux_alto[p].split(';');
				aux2_alto = aux2_alto.filter(Boolean);
				
				var aux2_cantidad = aux_cantidad[p].split(';');
				aux2_cantidad = aux2_cantidad.filter(Boolean);
				
				var aux2_monto = aux_monto[p].split(';');
				aux2_monto = aux2_monto.filter(Boolean);
				
				var aux2_respuesta = aux_respuesta[p].split('}');
				aux2_respuesta = aux2_respuesta.filter(Boolean);
				
				for (var a2p=0; a2p<aux2_producto.length; a2p++) {
					//ABRIMOS EL PRODUCTO
					aux = aux + '<div class="col-lg-12"><div class="box productos product'+a2p+'"><div class="box-header">';
					aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("tipo") ?></label><input name="producto" class="form-control" type="text" value="'+aux2_producto[a2p]+'" placeholder="<?php echo lang("nuevo.producto") ?>"></div></div>';
					aux = aux + '<div class="col-lg-2"><div class="form-group"><label><?php echo lang("descripcion") ?></label><input name="descripcion" class="form-control" type="text" value="'+aux2_descripcion[a2p]+'" placeholder="<?php echo lang("descripcion") ?>"></div></div>';
					aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("ancho") ?></label><input name="ancho" class="form-control" type="text" value="'+aux2_ancho[a2p]+'" placeholder="<?php echo lang("ancho") ?>"></div></div>';
					aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("alto") ?></label><input name="alto" class="form-control" type="text" value="'+aux2_alto[a2p]+'" placeholder="<?php echo lang("alto") ?>"></div></div>';
					aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("cantidad") ?></label><input name="cantidad" class="form-control" type="text" value="'+aux2_cantidad[a2p]+'" placeholder="<?php echo lang("cantidad") ?>"></div></div>';
					aux = aux + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("monto") ?></label><input name="monto" class="form-control" type="text" value="'+aux2_monto[a2p]+'" placeholder="<?php echo lang("monto") ?>"></div></div>';
					
					
					var aux3_respuesta = aux2_respuesta[a2p].split(';');
					aux3_respuesta = aux3_respuesta.filter(Boolean);
					
					for (var r=0; r<aux3_respuesta.length; r++) {
						//MOSTRAMOS LAS RESPUESTAS
						aux = aux + '<div class="col-lg-1"><div class="input-group respuesta"><label>'+aux3_respuesta[r]+'</label><input class="form-control" type="text" name="respuesta" value="" readonly ></div></div>';
					}
					
					aux = aux + '<div class="box-tools pull-right"><button class="btn btn-default btn-sm copiar_producto" title="<?php echo lang("copiar.producto") ?>"><i class="fa fa-files-o"></i></button> <button class="btn btn-default btn-sm borrar_producto" title="<?php echo lang("borrar.producto") ?>"><i class="fa fa-times"></i></button> <button class="btn btn-default btn-sm encoger_producto"><i class="fa fa-minus"></i></button></div>';
					
					//CERAMOS EL PRODUCTO
					aux = aux + '</div></div></div>';
				}
				
				//CERRAMOS LA PLANTA
				aux = aux + '</div></div></div>';
			}
			
			//CERRAR EDIFICIO
			aux = aux + '</div></div></div>'
			
			cont_edificio;
		}
		
		$('#opciones').html(aux);	
	}
	
	//COLAPSAR Y EXPANDIR
	$("#opciones").on('click', '.encoger_edificio', function () {
		//BOTON EXPANDIR Y COLAPSAR
		$header = $(this);
		//CONTENIDO
		$content = $header.parent().parent().next();
		//COLAPSAR O EXPANDIR
		$content.slideToggle(500, function () {
		//CAMBIAR ICONO
		$header.html(function () {
			return $content.is(":visible") ? '<i class="fa fa-minus"></i>' : '<i class="fa fa-plus"></i>';
		    });
		});
	});
	
	$("#opciones").on('click', '.encoger_planta', function () {
		//BOTON EXPANDIR Y COLAPSAR
	    $header = $(this);
	    //CONTENIDO
	    $content = $header.parent().parent().next();
	    //COLAPSAR O EXPANDIR
	    $content.slideToggle(500, function () {
	        //CAMBIAR ICONO
	        $header.html(function () {
	            return $content.is(":visible") ? '<i class="fa fa-minus"></i>' : '<i class="fa fa-plus"></i>';
	        });
	    });
	});
	
	$("#opciones").on('click', '.encoger_producto', function () {
		//BOTON EXPANDIR Y COLAPSAR
	    $header = $(this);
	    //CONTENIDO
	    $content = $header.parent().parent().next();
	    //COLAPSAR O EXPANDIR
	    $content.slideToggle(500, function () {
	        //CAMBIAR ICONO
	        $header.html(function () {
	            return $content.is(":visible") ? '<i class="fa fa-minus"></i>' : '<i class="fa fa-plus"></i>';
	        });
	    });
	});
	
	//EDIFICIOS
	//BORRAR EDIFICIO
	$('#opciones').on('click', '.borrar_edificio', function(){
		$(this).parent().parent().parent().remove();
		cont_edificio--;
	});
        
    //COPIAR EDIFICIO
	$('#opciones').on('click', '.copiar_edificio', function(){
        var old_id = $(this).parent().parent().parent().attr('id');
        
        var edificio = '<div class="col-lg-12 box box-solid box-primary" id="'+cont_edificio+'">' + $(this).parent().parent().parent().html() + '</div>';

        $('#opciones').append(edificio);
        
        $('#opciones #'+old_id+' input').each(function(index){
        	$('#opciones #'+cont_edificio+' input:eq('+index+')').val($('#opciones #'+old_id+' input:eq('+index+')').val());
        });
        
        cont_edificio++;
	});
	
	//NUEVO EDIFICIO
	$('#nuevo_edificio').click(function(){
		add_edificio();
	});
	
	//PLANTAS
	//BORRAR PLANTA
	$('#opciones').on('click', '.borrar_planta', function(){
		$(this).parent().parent().parent().remove();
	});
        
    //COPIAR PLANTA
	$('#opciones').on('click', '.copiar_planta', function(){
            var id_edificio = $(this).parent().parent().parent().parent().parent().parent().attr('id');
            
            var old_plant = parseInt($("#opciones #"+id_edificio+" .body_edificio .plantas").length - 1);
            var cont_plant = old_plant + 1;
            
            var planta = '<div class="box box-solid box-info plantas plant'+cont_plant+'">' + $(this).parent().parent().parent().html() + '</div>'; 
            
            $('#opciones #'+id_edificio+' .body_edificio').append(planta);
            
            $('#opciones #'+id_edificio+' .plant'+old_plant+' input').each(function(index){
        		$('#opciones #'+id_edificio+' .plant'+cont_plant+' input:eq('+index+')').val($('#opciones #'+id_edificio+' .plant'+old_plant+' input:eq('+index+')').val());
       	 	});
	});
	
	//NUEVO PLANTA
	$('#opciones').on('click','.nueva_planta', function(){
		var id_edificio = $(this).parent().parent().parent().attr('id');
		add_planta(id_edificio);
	});
	
	//PRODUCTOS
	//BORRAR PRODUCTO
	$('#opciones').on('click', '.borrar_producto', function(){
		$(this).parent().parent().parent().remove();
	});
	
	//NUEVO PRODUCTO
	$('#opciones').on('click','.nuevo_producto', function(){
		var id_edificio = $(this).parent().parent().parent().parent().parent().parent().attr('id');
		
		var cont_plant = $(this).parent().parent().parent().attr('class');
		cont_plant = cont_plant.substring(36, cont_plant.length);
		
		add_producto(id_edificio,cont_plant);
	});
	
	//COPIAR PRODUCTO
	$('#opciones').on('click', '.copiar_producto', function(){
            var planta = $(this).parent().parent().parent().parent().parent();
            var cont_product = $(planta).find('.productos').size();
            
            var producto = $(this).parent().parent();
            
            producto = '<div class="col-lg-12"><div class="box productos product'+cont_product+'"><div class="box-header">' + $(producto).html() + '</div></div></div>';
            
            $(planta).append(producto);
	});
	
	//RESPUESTAS
	//BORRAR O A�ADIR RESPUESTA
	$('#opciones').on('click', '.opts_respuesta', function(){
		//PREGUNTAMOS SI ES PARA A�ADIR O BORRAR
		if ($(this).find('i').hasClass('fa-plus')) {
			//NUEVA PREGUNTA
			var aux = '<div>'+$(this).parent().parent().html()+'</div>';
			$(this).parent().parent().parent().append(aux);
			$(this).find('i').removeClass('fa-plus');
			$(this).find('i').addClass('fa-times');
		} else {
			//BORRAR PREGUNTA
			$(this).parent().parent().remove();
		}
	});
	
	//EJECUTAMOS EL FORMULARIO AL DARLE AL BOTON
	$('#btn_guardar').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#inspeccion .form-group, #inspeccion .input-group").removeClass("has-error has-feedback");
	    $('#inspeccion .glyphicon-remove').hide();
	    $("#inspeccion .text-danger").html('');
	    
	    var inv_edificio = '';
	    var inv_planta = '';
	    var inv_producto = '';
		var inv_descripcion = '';
		var inv_ancho = '';
		var inv_alto = '';
		var inv_cantidad = '';
		var inv_monto = '';
	    var inv_respuesta = '';
	    var entrar = true;
	    
	    $('#inspeccion .edificios').each(function(indice){
	    	var edificio = $.trim($(this).find('.box-header input').val());
	    	if (edificio != '') {
				inv_edificio = inv_edificio + edificio + ';';
			} else {
				entrar = false;
				$(this).find('.box-header .text-danger').html('<?php echo lang("requerido") ?>');
			}
	    	
	    	if (entrar) {
				$(this).find('.box-body .plantas').each(function(indice2){
		    		var planta = $.trim($(this).find('.box-header input').val());
		    		if (planta != '') {
						inv_planta = inv_planta + planta + ';';
					} else {
						entrar = false;
						$(this).parent().next().html('<?php echo lang("requerido") ?>');
					}
					
					if (entrar) {
						$(this).find('.box-body .productos').each(function(indice3){
							var producto = $.trim($(this).find('.box-header input[name=producto]').val());
							if (producto != '') {
								inv_producto = inv_producto + producto + ';';
							} else {
								entrar = false;
								$(this).parent().next().html('<?php echo lang("requerido") ?>');
								l.stop();
							}
							
							var descripcion = $.trim($(this).find('.box-header input[name=descripcion]').val());
							if (descripcion != '') {
								inv_descripcion = inv_descripcion + descripcion + ';';
							} else {
								entrar = false;
								$(this).parent().next().html('<?php echo lang("requerido") ?>');
								l.stop();
							}
							
							var ancho = $.trim($(this).find('.box-header input[name=ancho]').val());
							if (ancho != '') {
								inv_ancho = inv_ancho + ancho + ';';
							} else {
								entrar = false;
								$(this).parent().next().html('<?php echo lang("requerido") ?>');
								l.stop();
							}
							
							var alto = $.trim($(this).find('.box-header input[name=alto]').val());
							if (alto != '') {
								inv_alto = inv_alto + alto + ';';
							} else {
								entrar = false;
								$(this).parent().next().html('<?php echo lang("requerido") ?>');
								l.stop();
							}
							
							var cantidad = $.trim($(this).find('.box-header input[name=cantidad]').val());
							if (cantidad != '') {
								inv_cantidad = inv_cantidad + cantidad + ';';
							} else {
								entrar = false;
								$(this).parent().next().html('<?php echo lang("requerido") ?>');
								l.stop();
							}
							
							var monto = $.trim($(this).find('.box-header input[name=monto]').val());
							if (monto != '') {
								inv_monto = inv_monto + monto + ';';
							} else {
								entrar = false;
								$(this).parent().next().html('<?php echo lang("requerido") ?>');
								l.stop();
							}
							
							if (entrar) {
								$(this).find('.box-header .respuesta').each(function(indice4){
									var respuesta = $.trim($(this).find('label').text());
									if (respuesta != '') {
										inv_respuesta = inv_respuesta + respuesta + ';';
									}
								});
								
								inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
				    			inv_respuesta = inv_respuesta + '}';
							}
						});
				    	
				    	if (entrar) {
							inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
				    		inv_respuesta = inv_respuesta + '|';
				    		
				    		inv_producto = inv_producto.substring(0, inv_producto.length-1);
				    		inv_producto = inv_producto + '|';
							
							inv_descripcion = inv_descripcion.substring(0, inv_descripcion.length-1);
							inv_descripcion = inv_descripcion + '|';
							
							inv_ancho = inv_ancho.substring(0, inv_ancho.length-1);
							inv_ancho = inv_ancho + '|';
							
							inv_alto = inv_alto.substring(0, inv_alto.length-1);
							inv_alto = inv_alto + '|';
							
							inv_cantidad = inv_cantidad.substring(0, inv_cantidad.length-1);
							inv_cantidad = inv_cantidad + '|';
							
							inv_monto = inv_monto.substring(0, inv_monto.length-1);
							inv_monto = inv_monto + '|';
							
						}
					}
		    	});
		    	
		    	if (entrar) {
					inv_planta = inv_planta.substring(0, inv_planta.length-1);
				    inv_planta = inv_planta + '|';
				    
				    inv_producto = inv_producto.substring(0, inv_producto.length-1);
				    inv_producto = inv_producto + '^';
					
					inv_descripcion = inv_descripcion.substring(0, inv_descripcion.length-1);
				    inv_descripcion = inv_descripcion + '^';
					
					inv_ancho = inv_ancho.substring(0, inv_ancho.length-1);
				    inv_ancho = inv_ancho + '^';
					
					inv_alto = inv_alto.substring(0, inv_alto.length-1);
				    inv_alto = inv_alto + '^';
					
					inv_cantidad = inv_cantidad.substring(0, inv_cantidad.length-1);
				    inv_cantidad = inv_cantidad + '^';
					
					inv_monto = inv_monto.substring(0, inv_monto.length-1);
				    inv_monto = inv_monto + '^';
				    
				    inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
				    inv_respuesta = inv_respuesta + '^';
				}
			}
	    });
	    
	    if (entrar) {
			var datos = {
				'su_id' : $('#sucursales option:selected').val(),
				'inv_nombre' : $('#inv_nombre').val(),
				'inv_edificio' : inv_edificio,
				'inv_planta' : inv_planta,
				'inv_producto' : inv_producto,
				'inv_descripcion' : inv_descripcion,
				'inv_ancho' : inv_ancho,
				'inv_alto' : inv_alto,
				'inv_cantidad' : inv_cantidad,
				'inv_monto' : inv_monto,
				'inv_respuesta' : inv_respuesta,
				'inv_id' : inv_id
			};
			
			$.ajax({
		    	type: "POST",
				dataType: "json",
			  	url: "<?php echo base_url().$this->lang->lang() ?>/inspecciones/guardar_editar_avanzada",
			  	data: datos,
			  	success: function(data) {
			  		if (data.sql) {
			  			//MENSAJES ALERTA PARA EDITAR O CREAR
			  			if (datos['su_id'] != '') {
							if (!data.editar) {
								bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								setTimeout("location.href='"+location+"'", 5000);
							}
						} else {
							if (!data.crear) {
								bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								setTimeout("location.href='"+location+"'", 5000);
							}
						}
						
						//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
			  			if (data.status) {
			  				//TODO CORRECTO
							//SI ES NUEVA SUCURSAL
							if (inv_id == '') {
								$('#inspeccion input').val('');
								bootbox.dialog({
									message: "<?php echo lang('inspeccion.avanzada.nuevo.ok') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('nuevo') ?>",
											className: "btn-success",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/inspecciones/nueva_inspeccion_avanzada';
												$(location).attr('href',url);
											}
										},
										main: {
											label: "<?php echo lang('inspecciones') ?>",
											className: "btn-primary",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/inspecciones/avanzadas';
												$(location).attr('href',url);
											}
										}
									}
								});
							} else {
							//SI ES UNA SUCURSAL EDITADA
								bootbox.alert('<?php echo lang("inspeccion.avanzada.editado.ok") ?>');
							}
			  			} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							if (data.errors.length > 0) {
								$.each(data.errors, function (ind, elem) {
									$.each(data.errors[ind], function (ind2, elem2) {
										//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
										$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
										$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
									}); 
								});
							}
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
			  	},
			  	error: function(XMLHttpRequest, textStatus, errorThrown) {
			   		bootbox.alert('<?php echo lang("error.ajax") ?>');
			   		l.stop();
			  	}
		    });
		}	    
	});
	
	//ELIMINAR INSPECCION
	$('#eliminar_inspeccion').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#eliminar_inspeccion" ) );
	 	l.start();
		
		bootbox.dialog({
			message: "<?php echo lang('eliminar.inspeccion.confirmacion') ?>",
			buttons: {
				success: {
					label: "<?php echo lang('eliminar') ?>",
					className: "btn-danger",
					callback: function(result) {
						$.ajax({
					    	type: "POST",
						  	url: "<?php echo base_url().$this->lang->lang() ?>/inspecciones/eliminar",
						  	data: {'inv_id':inv_id},
						  	dataType: "json",
						  	success: function(data) {
						  		if (data) {
									bootbox.alert('<?php echo lang("eliminar.inspeccion.ok") ?>');
									
									var url = 'inspecciones/avanzadas';
									if (typeof inv_id == "undefined") {
										url = 'inspecciones';
									}
									
									url = '<?php echo base_url().$this->lang->lang() ?>/'+url;
									
									setTimeout("location.href='"+url+"'", 1500);
								} else {
									bootbox.alert('<?php echo lang("eliminar.inspeccion.ko") ?>');
								}
						  	},
						  	complete: function() {
								l.stop();
							},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});						
					}
				},
				main: {
					label: "<?php echo lang('cancelar') ?>",
					className: "btn-default"
				}
			}
		});
	});
});
</script>






<!--
<div class="col-lg-4">
	<div class="box productos product2">
		<div class="box-header">
			<h3 class="box-title">
				<input name="producto" class="form-control" value="V-1" placeholder="Nuevo producto" type="text">
			</h3>
			
			<div class="box-tools pull-right">
				<button class="btn btn-default btn-sm copiar_producto" title="Copiar producto"><i class="fa fa-files-o"></i></button>
				<button class="btn btn-default btn-sm borrar_producto" title="Borrar producto"><i class="fa fa-times"></i></button>
				<button class="btn btn-default btn-sm encoger_producto"><i class="fa fa-minus"></i></button>
			</div>
		</div>
		
		<div class="box-body body_producto">
			<div>
				<label class="respuesta"></label>
				<div class="input-group">
					<input class="form-control" name="respuesta" value="Silic�n" placeholder="Nuevo item" type="text">
					<span class="input-group-addon opts_respuesta" title="Nuevo item"><i class="fa fa-plus"></i></span>
				</div>
				
				<div class="text-danger"></div>
			</div>
			
			<div>
				<label class="respuesta"></label>
				
				<div class="input-group">
					<input class="form-control" name="respuesta" value="Cristal" placeholder="Nuevo item" type="text">
					<span class="input-group-addon opts_respuesta" title="Nuevo item"><i class="fa fa-plus"></i></span>
				</div>
				
				<div class="text-danger"></div>
			</div>
		</div>
	</div>
</div>-->


