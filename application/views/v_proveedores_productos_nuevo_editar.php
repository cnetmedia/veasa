<?php
//CREAMOS LOS INPUT DEL FORMULARIO
$input_nombre = array(
	'name'		=>	'product_nombre',
	'id'		=>	'product_nombre',
	'class'		=>	'form-control',
	'maxlength'	=>	'80',
	'value'		=>	isset($product_nombre)?$product_nombre:set_value('product_nombre')
);

$input_referencia = array(
	'name'		=>	'product_referencia',
	'id'		=>	'product_referencia',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($product_referencia)?$product_referencia:set_value('product_referencia')
);

$input_precio = array(
	'name'		=>	'product_precio',
	'id'		=>	'product_precio',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'value'		=>	isset($product_precio)?$product_precio:set_value('product_precio')
);

$input_id = array(
	'name'		=>	'product_id',
	'id'		=>	'product_id',
	'type'		=>	'hidden',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	isset($product_id)?$product_id:set_value('product_id')
);
?>

<form id="datos_producto" action="<?php echo base_url().$this->lang->lang() ?>/proveedores/productos_guardar_editar" method="post" enctype="multipart/form-data">

<div class="row">	
	<!-- FORMULARIO PRODUCTO -->
	<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-5 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('descripcion') ?></label>
					    <?php echo form_input($input_nombre) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('product_nombre') ?></div>
					</div>
				</div>
				
				<div class="col-lg-2 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('referencia') ?></label>
					    <?php echo form_input($input_referencia) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('product_referencia') ?></div>
					</div>
				</div>
				
				<div class="col-lg-2 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('precio') ?></label>
					    <?php echo form_input($input_precio) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('product_precio') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('proveedor') ?></label>
						<select id="proveedores" name="pro_id" class="selectpicker form-control" data-live-search="true"></select>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('pro_id') ?></div>
					</div>
				</div>
			</div>
			
			<?php echo form_input($input_id) ?>
	</div>
</div>

<hr />

<div class="row">
	<div class="col-lg-3 col-xs-12">
        <div class="form-group">
            <label><?php echo lang('categorias') ?></label>
            <select id="categorias" name="cate_id" class="selectpicker form-control" data-live-search="true"></select>
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>	<div class="text-danger"><?php echo form_error('pro_id') ?></div>
        </div>
    </div>
</div>

<br>

<div class="row">
	<div class="col-lg-12">
		<label><?php echo lang('caracteristicas') ?></label>
	</div>
	
	<div class="col-lg-12 caracteristicas"></div>
</div>

<hr />

<div class="row">
	<div class="col-lg-6 fotos">
		<label><?php echo lang('foto.producto') ?></label>					
		<div class="input-group">
			<span class="input-group-btn">
				<span class="btn btn-primary btn-file">
					<!--<input type="file" multiple>-->
					<i class="fa fa-camera"></i> <?php echo lang('cargar') ?>
					<input type="file" id="foto" name="fotos[]" multiple>
				</span>
			</span>
			<input type="text" class="form-control" readonly>
		</div>
		<div class="text-danger"><?php echo form_error('fotos') ?></div>
		
		<div id="imagenes"></div>
	</div>
	
	<div class="col-lg-6 archivos">
		<label><?php echo lang('archivos.producto') ?></label>					
		<div class="input-group">
			<span class="input-group-btn">
				<span class="btn btn-primary btn-file">
					<!--<input type="file" multiple>-->
					<i class="fa fa-file"></i> <?php echo lang('cargar') ?>
					<input type="file" id="archivos" name="archivos[]" multiple>
				</span>
			</span>
			<input type="text" class="form-control" readonly>
		</div>
		<div class="text-danger"><?php echo form_error('archivos') ?></div>
		
		<div id="documentos"></div>
	</div>
</div>

<div class="row">
	<hr>
	<div class="col-lg-12 col-xs-12 text-right">		
		<!-- SI PUEDE CREAR O EDITAR CONTENIDO MUESTRA EL BOTON -->
		<?php if ($this->session->userdata('emp_crear') == '1' || $this->session->userdata('emp_editar') == '1') { ?>
		<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
			<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
		</button>
		<?php } ?>
					
		<a href="<?php echo base_url().$this->lang->lang().'/proveedores/productos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
	</div>
</div>

</form>

<script>
console.log('aqui');
/* ESTILO Y BOTON FOTO */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function(){
	//VARIABLES GLOBALES
	var product_id = '<?php echo isset($product_id)?$product_id:set_value("product_id") ?>';
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	
	//PREGUNTAMOS SI PUEDE EDITAR
	var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	if (editar == 0) {
		$('#btn_guardar').hide();
		$('#datos_cliente *').attr('readonly', true);
	}
	
	$('#clientes').selectpicker();
	
	//RELLENAMOS EL CAMPO DE PROVEEDORES PARA A�ADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
					if (data[indice]["pro_id"] == '<?php echo isset($pro_id)?$pro_id:"" ?>') {
						$('#proveedores').append('<option value="'+data[indice]["pro_id"]+'" selected>'+data[indice]["pro_nombre"]+'</option>');
					} else {
						$('#proveedores').append('<option value="'+data[indice]["pro_id"]+'">'+data[indice]["pro_nombre"]+'</option>');
					}
		});
                
                $('#proveedores').selectpicker('refresh');
	}, "json");
	
	//RELLENAMOS EL CAMPO DE PROVEEDORES PARA A�ADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/proveedores/categorias",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
					if (data[indice]["cate_id"] == '<?php echo isset($cate_id)?$cate_id:"" ?>') {
						$('#categorias').append('<option value="'+data[indice]["cate_id"]+'" selected>'+data[indice]["descripcion"]+'</option>');
					} else {
						$('#categorias').append('<option value="'+data[indice]["cate_id"]+'">'+data[indice]["descripcion"]+'</option>');
					}
		});
                
                $('#categorias').selectpicker('refresh');
	}, "json");
	
	//CARACTERISTICAS
	var caracteristicas = '';
	var primera_vez = true;
	
	//RECOGEMOS LAS CARACTERISTICAS DEL PRODUCTO
	if (product_id != '') {
		caracteristicas = '<?php echo isset($product_caracteristicas)?$product_caracteristicas:"" ?>';
		caracteristicas = caracteristicas.split(';');
		//ELIMINAR ESPACIOS EN BLANCO
		caracteristicas = caracteristicas.filter(Boolean);
		for (var i=0; i<caracteristicas.length; i++) {
			caracteristicas[i] = caracteristicas[i].split('/');
		}
	}
	
	//RECOGEMOS LAS CATEGORIA DEL PRODUCTO
	if (product_id != '') {
		cate_id = '<?php echo isset($cate_id)?$cate_id:"" ?>';
	}
	
	//CREAMOS EL DIV DE CARACTERISTICAS
	function div_caracteristica() {
		var icono = 'fa-check';
		if (!primera_vez) {
			icono = 'fa-times';
		} 
		
		var div = '';
			
		if (primera_vez) {
			div = '<div class="input-group product_caracteristica"><input class="form-control opcion1" type="text" placeholder="<?php echo lang("producto.opcion1") ?>"><input class="form-control opcion2" type="text" placeholder="<?php echo lang("producto.opcion2") ?>"><span class="input-group-addon"><i class="fa '+icono+'"></i></span></div>';
		} else {
			var opcion1 = $('.caracteristicas .product_caracteristica:first .opcion1').val();
			var opcion2 = $('.caracteristicas .product_caracteristica:first .opcion2').val();
			
			if (opcion1 != '' && opcion2 != '') {
				div = '<div class="input-group product_caracteristica"><input class="form-control opcion1" type="text" value="'+opcion1+'"><input class="form-control opcion2" type="text" value="'+opcion2+'"><span class="input-group-addon"><i class="fa '+icono+'"></i></span></div>';
			
				$('.caracteristicas .product_caracteristica:first .opcion1').val('');
				$('.caracteristicas .product_caracteristica:first .opcion2').val('');
			}
		}
		
		if (caracteristicas.length > 0 && primera_vez) {
			for (var i=0; i<caracteristicas.length; i++) {
				div = div + '<div class="input-group product_caracteristica"><input class="form-control opcion1" type="text" value="'+caracteristicas[i][0]+'"><input class="form-control opcion2" type="text" value="'+caracteristicas[i][1]+'"><span class="input-group-addon"><i class="fa fa-times"></i></span></div>';
			}
		}
		
		primera_vez = false;
		
		//AGREGAMOS EL DIV DE CARACTERISTICA
		$('.caracteristicas').append($(div));
		
		//PREGUNTAMOS PARA ELIMINAR EL DIV O NO
		$('.product_caracteristica span').click(function(e){
			e.preventDefault();
				
			if ($(this).find('i').hasClass('fa-times')) {
				$(this).parent().remove();
			}
		});
	}
	
	//CARGAMOS POR PRIMERA VEZ
	div_caracteristica();
	
	//A�ADIMOS CARACTERISTICA
	$('.product_caracteristica span').click(function(e){
		e.preventDefault();
			
		if ($(this).find('i').hasClass('fa-check')) {
			div_caracteristica();
		}
	});
	
	//RECUPERAMOS LAS FOTOS DEL PRODUCTO
	function cargar_imagenes() {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/producto_imagenes",
			data: 'product_id='+product_id,
			success: function(data) {
				$('#imagenes').html(data);
			}
		});
	}
	
	//RECUPERAMOS LOS ARCHIVOS DEL PRODUCTO
	function cargar_archivos() {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/producto_archivos",
			data: 'product_id='+product_id,
			success: function(data) {
				$('#documentos').html(data);
				$('#documentos a').css({"margin-top":"10px"});
				$('#documentos a i').css("margin-right","10px");
			}
		});
	}
	
	//CARGAMOS LAS IMAGENES Y ARCHIVOS SI NO ES UN PRODUCTO NUEVO
	if (product_id != '') {
		cargar_imagenes();
		cargar_archivos();
		
		//IMAGENES
		$(".fancybox").fancybox({
			helpers : {
		   		title : false
		   	},
		   	padding: 0,
			openEffect : 'elastic',
			openSpeed  : 150,
			closeEffect : 'elastic',
			closeSpeed  : 150,
			closeClick : false
		});
	}		
	
	//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO
	$('#datos_producto .form-group span').hide();
	
	//ESTILO Y BOTON FOTO
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {	
		var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}	
	});
	
	//EJECUTAMOS EL FORMULARIO AL DARLE AL BOTON
	$('#btn_guardar').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#datos_producto .form-group, #datos_producto .input-group").removeClass("has-error has-feedback");
	    $('#datos_producto .glyphicon-remove').hide();
	    $("#datos_producto .text-danger").html('');
	    
	    var opciones = '';
	    
	    $('.caracteristicas .product_caracteristica').each(function(indice){
	    	if (indice > 0) {
				opciones = opciones + $.trim($(this).find('.opcion1').val()) + '/' + $.trim($(this).find('.opcion2').val()) + ';';
			}
	    });
		console.log(opciones);
		
	    
			jQuery("#datos_producto").ajaxForm({
			   	dataType: 'json',
			   	data: {'product_caracteristicas':opciones},
				success: function(data) {
					//console.log(data);
					if (data.sql) {
						if (data.status) {
							//TODO CORRECTO
							//SI ES NUEVO PRODUCTO
							if ($('#product_id').val() == '') {
								$('#datos_producto input').val('');
								bootbox.dialog({
									message: "<?php echo lang('proveedores.producto.nuevo.ok') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('nuevo') ?>",
											className: "btn-success",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/proveedores/nuevo_producto';
												$(location).attr('href',url);
											}
										},
										main: {
											label: "<?php echo lang('tienda.online') ?>",
											className: "btn-primary",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/proveedores/productos';
												$(location).attr('href',url);
											}
										}
									}
								});
							} else {
								//SI ES UN PRODUCTO EDITADO
								bootbox.alert('<?php echo lang("proveedores.producto.editado.ok") ?>');
							}
							
							if ($('.fotos input').val() != '') {
								cargar_imagenes();
							}
							
							if ($('.archivos input').val() != '') {
								cargar_archivos();
							}
							
							$('.fotos input, .archivos input').val('');
						} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									//SI EXSITE ERROR EN EL CAMPO FOTO ENTRAMOS POR AQUI
									if (data.errors[ind][0] == 'fotos') {
										$('.fotos').find('.text-danger').html(data.errors[ind][ind2]);
										$('.fotos').find('.text-danger').addClass("has-error has-feedback");
									}
									
									if (data.errors[ind][0] == 'archivos') {
										$('.archivos').find('.text-danger').html(data.errors[ind][ind2]);
										$('.archivos').find('.text-danger').addClass("has-error has-feedback");
									}
									
									//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
								}); 
							});
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
					l.stop();
				}
			}).submit();
	});
});
</script>