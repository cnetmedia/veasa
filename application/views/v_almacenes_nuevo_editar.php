<?php
$input_nombre = array(
	'name'		=>	'al_nombre',
	'id'		=>	'al_nombre',
	'class'		=>	'form-control',
	'maxlength'	=>	'80',
	'value'		=>	isset($al_nombre)?$al_nombre:set_value('al_nombre')
);

$input_telefono = array(
	'name'		=>	'al_telefono',
	'id'		=>	'al_telefono',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'value'		=>	isset($al_telefono)?$al_telefono:set_value('al_telefono')
);

$input_barriada = array(
	'name'		=>	'al_barriada',
	'id'		=>	'al_barriada',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($al_barriada)?$al_barriada:set_value('al_barriada')
);

$input_localidad = array(
	'name'		=>	'al_localidad',
	'id'		=>	'al_localidad',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($al_localidad)?$al_localidad:set_value('al_localidad')
);

$input_postal = array(
	'name'		=>	'al_postal',
	'id'		=>	'al_postal',
	'class'		=>	'form-control',
	'maxlength'	=>	'30',
	'value'		=>	isset($al_postal)?$al_postal:set_value('al_postal')
);

$input_provincia = array(
	'name'		=>	'al_provincia',
	'id'		=>	'al_provincia',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($al_provincia)?$al_provincia:set_value('al_provincia')
);

$input_direccion = array(
	'name'		=>	'al_direccion',
	'id'		=>	'al_direccion',
	'class'		=>	'form-control',
	'maxlength'	=>	'150',
	'value'		=>	isset($al_direccion)?$al_direccion:set_value('al_direccion')
);

$input_tipo = array(
	'name'		=>	'al_tipo',
	'id'		=>	'al_tipo',
	'class'		=>	'form-control',
	'maxlength'	=>	'1',
	'value'		=>	isset($al_tipo)?$al_tipo:set_value('al_tipo')
);

?>

<script>
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
return ((key >= 48 && key <= 57) || (key==8) || (key == 46));
}
</script>

<div id="almacen">

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
    	<li class="active">
        	<a href="#tab_1" data-toggle="tab"><?php echo lang('informacion') ?></a>
        </li>
        <li class="">
        	<a href="#tab_2" data-toggle="tab"><?php echo lang('productos') ?></a>
        </li>
    </ul>
        
    <div class="tab-content">
    	<div class="tab-pane active" id="tab_1">
    		<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="form-group">
						<label><?php echo lang('nombre.almacen') ?></label>
						<?php echo form_input($input_nombre) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('al_nombre') ?></div>
					</div>
				</div>
				
				<div class="col-lg-9 col-md-9 col-sm-9">
					<div class="form-group">
						<label><?php echo lang('direccion') ?></label>
						<?php echo form_input($input_direccion) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('al_direccion') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="form-group">
						<label><?php echo lang('barriada') ?></label>
						<?php echo form_input($input_barriada) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('al_barriada') ?></div>
					</div>
				</div>
			
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="form-group">
						<label><?php echo lang('postal') ?></label>
						<?php echo form_input($input_postal) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('al_postal') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="form-group">
						<label><?php echo lang('localidad') ?></label>
						<?php echo form_input($input_localidad) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('al_localidad') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="form-group">
						<label><?php echo lang('provincia') ?></label>
						<?php echo form_input($input_provincia) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('al_provincia') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="form-group">
						<label><?php echo lang('pais') ?></label>
						<select name="emp_pais" id="paises_pais" class="form-control"></select>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('al_pais') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="form-group">
						<label><?php echo lang('telefono') ?></label>
						<?php echo form_input($input_telefono) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('al_telefono') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="form-group" id="tipo_almacen">
						<label><?php echo lang('tipo') ?></label>
						
	                    <div class="radio">
							<label style="margin-right: 20px;">
						    	<input type="radio" name="al_tipo" value="0" <?php echo ($al_tipo == 0)?'checked':'' ?>>
						    	<?php echo lang('deposito') ?>
						  	</label>
						  	
						  	<label>
						    	<input type="radio" name="al_tipo" value="1" <?php echo ($al_tipo == 1)?'checked':'' ?>>
						    	<?php echo lang('reciclaje') ?>
						  	</label>
						</div>
	            	</div>
				</div>
				
				
			</div>
			
			<div class="row">
				<div class="col-lg-12 text-right">
					<?php if ($this->session->userdata('de_id') != '2' && $this->session->userdata('de_id') != '3' && $this->session->userdata('de_id') != '4' && $this->session->userdata('de_id') != '5') { ?>
					<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
						<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
					</button>
					<?php } ?>
					
					<a href="<?php echo base_url().$this->lang->lang().'/almacenes' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
				</div>
			</div>
    	</div>
    	
    	<div class="tab-pane" id="tab_2">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
		            	<thead>
		                	<tr>
		                		<th width="5%" class="col-md-2"><?php echo lang("referencia") ?></th>
		                		<th width="20%"><?php echo lang("descripcion") ?></th>
		                		<th width="40%"><?php echo lang("caracteristicas") ?></th>
		                		<th width="15%" class="col-md-2"><?php echo lang("proveedor") ?></th>
		                		<th width="5%" class="col-md-1"><?php echo lang("vista.previa") ?></th>
		                		<th width="5%" class="col-md-1"><?php echo lang("precio") ?></th>
		                		<th width="10%" class="col-md-2"><?php echo lang("cantidad") ?></th>
                                <th width="2%" class="col-md-2"></th>
		                	</tr>
						</thead>
		                                        
						<tfoot>
		                	<tr>
		                		<th width="5%"><?php echo lang("referencia") ?></th>
		                		<th width="20%"><?php echo lang("descripcion") ?></th>
		                		<th width="40%"><?php echo lang("caracteristicas") ?></th>
		                		<th width="15%"><?php echo lang("proveedor") ?></th>
		                		<th width="5%"><?php echo lang("vista.previa") ?></th>
		                		<th width="5%"><?php echo lang("precio") ?>/$</th>
		                		<th width="10%"><?php echo lang("cantidad") ?></th>
                                <th width="2%" class="col-md-2"></th>
		                	</tr>
						</tfoot>
						
						<tbody aria-relevant="all" aria-live="polite" role="alert">
							
						</tbody>
					</table>	
                    
					<?php if ($this->session->userdata('de_id') != '3' && $this->session->userdata('de_id') != '4' && $this->session->userdata('de_id') != '5') { ?>
						<button type="button" class="btn btn-warning ladda-button seleccionar_todos" data-style="zoom-out"><i class="fa fa-th-large"></i> Seleccionar todos</button>
						<button type="button" class="btn btn-success ladda-button enviar_productos" data-style="zoom-out"><i class="fa fa-truck"></i> Generar Salida</button>
					<?php } ?>
				</div>
			</div>
    	</div>
    </div>
</div>



	
</div>

<script>
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
return ((key >= 48 && key <= 57) || (key==8) || (key == 46));
}

	$(document).ready(function() {
		//DATOS EMPLEADO DE LA SESSION
		var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
		var su_id = '<?php echo $this->session->userdata("su_id") ?>';
		var emp_editar = '<?php echo $this->session->userdata("emp_editar") ?>';
		
		//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#almacen .form-group, #almacen .input-group").removeClass("has-error has-feedback");
	    $('#almacen .glyphicon-remove').hide();
	    $("#almacen .text-danger").html('');
		
		//OPCIONES GENERALES
		if (emp_editar == 0) {
			$('#almacen *').attr('readonly', true);
			$('#almacen #btn_guardar').hide();
		}
		
		var al_id = '<?php echo isset($al_id)?$al_id:"" ?>';
		var al_tipo = '<?php echo isset($al_tipo)?$al_tipo:"" ?>';
		
		//RELLENAMOS LOS PAISES
			$.post(
				"<?php echo base_url().$this->lang->lang() ?>/sucursales/paises",
				{'buscar':''},
				function(data){
					$.each(data,function(indice) {
						//PAIS
						if (data[indice]["id_countries"] == '<?php echo isset($al_pais)?$al_pais:"" ?>') {
							$('#paises_pais').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
						} else if (data[indice]["id_countries"] == $('#sucursales option:selected').attr('pais')) {
							$('#paises_pais').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
						} else {
							$('#paises_pais').append('<option value="'+data[indice]["id_countries"]+'">'+data[indice]["name"]+'</option>');
						}
					});
				}, "json");
		
		//GUARDAR
		$('#btn_guardar').click(function(e){
			e.preventDefault();
			//LOADING BOTON
			var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
		 	l.start();
		 	
		 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    	$("#almacen .form-group, #almacen .input-group").removeClass("has-error has-feedback");
	    	$('#almacen .glyphicon-remove').hide();
	    	$("#almacen .text-danger").html('');
		 	
		 		var datos = new Array();
			 	datos = {
					'al_nombre'	:	$('#al_nombre').val(),
					'al_telefono'	:	$('#al_telefono').val(),
					'al_direccion'	:	$('#al_direccion').val(),
					'al_barriada'	:	$('#al_barriada').val(),
					'al_postal'	:	$('#al_postal').val(),
					'al_localidad'	:	$('#al_localidad').val(),
					'al_provincia'	:	$('#al_provincia').val(),
					'al_pais'		:	$('#paises_pais option:selected').val(),
					'al_tipo'		:	$('input[name="al_tipo"]:checked').val(),
					'al_id'		:	al_id
				};
			 	
			 	$.ajax({
					type: "POST",
					dataType: "json",
				  	url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/guardar_editar",
				  	data: datos,
				  	success: function(data) {
				  		if (data.sql) {
				  			//MENSAJES ALERTA PARA EDITAR O CREAR
				  			if (al_id != '') {
								if (!data.editar) {
									bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
									//REDIRIGIMOS A LOS 5 SEGUNDOS
									setTimeout("location.href='"+location+"'", 5000);
								}
							} else {
								if (!data.crear) {
									bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
									//REDIRIGIMOS A LOS 5 SEGUNDOS
									setTimeout("location.href='"+location+"'", 5000);
								}
							}
							
							//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
			  				if (data.status) {
			  					//TODO CORRECTO
								//SI ES NUEVA SUCURSAL
								if (al_id == '') {
									$('#almacen input').val('');
									bootbox.dialog({
										message: "<?php echo lang('almacenes.nuevo.ok') ?>",
										buttons: {
											success: {
												label: "<?php echo lang('nuevo') ?>",
												className: "btn-success",
												callback: function(result) {
													var url = '<?php echo base_url().$this->lang->lang() ?>/almacenes/nuevo_almacen';
													$(location).attr('href',url);
												}
											},
											main: {
												label: "<?php echo lang('almacenes') ?>",
												className: "btn-primary",
												callback: function(result) {
													var url = '<?php echo base_url().$this->lang->lang() ?>/almacenes';
													$(location).attr('href',url);
												}
											}
										}
									});
								} else {
								//SI ES UNA SUCURSAL EDITADA
									bootbox.alert('<?php echo lang("almacenes.editado.ok") ?>');
								}
			  				} else {
								//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
								//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
								if (data.errors.length > 0) {
									$.each(data.errors, function (ind, elem) {
										$.each(data.errors[ind], function (ind2, elem2) {
											//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
											$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
											$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
											$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
										}); 
									});
								}
							}
				  		} else {
							bootbox.alert('<?php echo lang("error.ajax") ?>');
						}
						l.stop();
				  	},
				  	error: function(XMLHttpRequest, textStatus, errorThrown) {
				   		bootbox.alert('<?php echo lang("error.ajax") ?>');
				   		l.stop();
				  	}
				});
		 	
	 	});
	 	
	 	/* PRODUCTOS DEL ALMACEN */
	 	var productos_almacen = new Array();
	 	
	 	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador_productos_almacen",
			data: {'al_id':al_id,'al_tipo':al_tipo},
			success: function(data) {				
				var table = '';
				$(data).each(function(indice,valor){
					productos_almacen.push(data[indice]['product_id']);
					
					table = table + '<tr id="'+data[indice]['product_id']+'" pra_id="'+data[indice]['pra_id']+'">';
					table = table + '<td class="vcenter">'+data[indice]['product_referencia']+'</td>';
					table = table + '<td class="vcenter">'+data[indice]['product_nombre']+'</td>';
					
					var caracteristicas = data[indice]['product_caracteristicas'];
	  				caracteristicas = caracteristicas.split(';');
					//ELIMINAR ESPACIOS EN BLANCO
					caracteristicas = caracteristicas.filter(Boolean);
					for (var i=0; i<caracteristicas.length; i++) {
						caracteristicas[i] = caracteristicas[i].split('/');
					}
							
					var opciones = '';
					for (var i=0; i<caracteristicas.length; i++) {
						opciones = opciones + '<span class="opc_product_list"><strong>' + caracteristicas[i][0] + '</strong>: <span>' + caracteristicas[i][1] + '</span></span>';
					}
                                        
                    var reciclaje = '';
					if (data[indice]['pra_caracteristicas'] != null) {
						reciclaje = '<br><?php echo lang("reciclado") ?>: '+data[indice]['pra_caracteristicas'];
					}
					
					table = table + '<td class="vcenter">'+opciones+reciclaje+'</td>';
					table = table + '<td class="vcenter">'+data[indice]['pro_nombre']+'</td>';
					table = table + '<td class="text-center vcenter">'+data[indice]['fotos']+'</td>';
					table = table + '<td class="vcenter">'+data[indice]['product_precio']+' '+data[indice]['currrency_symbol']+'</td>';
					
					var disable = '';
					if (emp_tipo > 1) {
						disable = 'disabled';
					}
					
					if (emp_editar == 0) {
						table = table + '<td class="vcenter">'+data[indice]['product_cantidad']+'</td>';
					} else {
						table = table + '<td class="vcenter"><div class="input-group">';
						table = table + '<input type="text" name="product_cantidad" onKeypress="return soloNumeros(event)" class="text-center form-control" value="'+data[indice]['product_cantidad']+'" style="min-width: 70px !important;" '+disable+'>';
						
						<?php if (($this->session->userdata('de_id') != '3') && ($this->session->userdata('de_id') != '4') && ($this->session->userdata('de_id') != '5')) { ?>
							<?php if ($this->session->userdata('de_id') != '2') { ?>	
								table = table + '<span class="pointer input-group-addon actualizar_producto" title="<?php echo lang("actualizar") ?>"><i class="fa fa-refresh"></i></span>';
								table = table + '<span class="pointer input-group-addon mover_producto" title="<?php echo lang("mover") ?>"><i class="fa fa-th-large"></i></span>';
							<?php } ?>
							table = table + '<span class="pointer input-group-addon enviar_producto" title="<?php echo lang("enviar") ?>"><i class="fa fa-truck"></i></span>';
						<?php } ?>
						//Boton de ver proyectos asociados a un producto
						table = table + '<span class="pointer input-group-addon ver_proyectos_producto" title="<?php echo lang("mantenimientos") ?>"><i class="fa fa-building-o"></i></span>';
						table = table + '</div></td>';
					}
					
					table = table + '<td class="vcenter"><div class="checkbox"><label><input type="checkbox" readonly="readonly" name="product_id[]" pra_id="'+data[indice]['pra_id']+'" value="'+data[indice]['product_id']+'" /></label></div></td>';
					
					
					table = table + '</tr>';
				});
				
				$('#tab_2 table tbody').html(table);
				
				$("#example1").dataTable({
						"aaSorting": [[ 3, "asc"],[ 0, "asc"]],
						"iDisplayLength": 20,
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-th-large'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					$("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
					$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
		
					$("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(this).find(':checkbox').each(function(index, element) {
							if(this.checked) {
								this.checked = false;
							} else {
								this.checked = true;
							}
						});
					});
					
					$("#example1").on("click", "td:last-child", function(e) {
						e.stopPropagation();
					});
						
					//REFRESCAR
					$("#btn_refrecar").hide()
					
					//BOTON NUEVO
					if (emp_tipo > 0) {
						$('#btn_nuevo').hide();
					}
					
					//NUEVO PRODUCTO
					var proveedores = '';
					var productos = '';
					var primera_vez = false;
					var alerta_primera_vez = true;
					
					function alert_nuevo_producto() {
						bootbox.dialog({
							message: "<div id='nuevo_producto'><div><label><?php echo lang('proveedores') ?></label><br><select id='proveedores' class='form-control'></select></div><br><div><label><?php echo lang('productos') ?></label><select id='productos' class='form-control'></select></div><br><div><label><?php echo lang('cantidad') ?></label><input name='product_cantidad' class='text-center form-control' value='' onKeypress='return soloNumeros(event)' type='text'></div></div>",
							buttons: {
								success: {
									label: "<?php echo lang('anadir.producto') ?>",
									className: "btn-success",
									callback: function() {
										//AÑADIMOS EL PRODUCTO NUEVO
										if ($('#nuevo_producto input').val() == '') {
											bootbox.alert('<?php echo lang("producto.anadir.vacio") ?>');
										} else if ($('#nuevo_producto input').val() == 0) {
											bootbox.alert('<?php echo lang("producto.cantidad.cero") ?>');
										} else {
											$.ajax({
												type: "POST",
												dataType: "json",
												async: true,
												url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/anadir_producto",
												data: {'product_id':$('#nuevo_producto #productos option:selected').val(), 'al_id':al_id, 'product_cantidad':$('#nuevo_producto input').val(),'al_tipo':al_tipo},
												success: function(datos) {
													if (datos.status) {					
														bootbox.alert('<?php echo lang("producto.anadido.ok") ?>');
														location.reload(true);
													} else {
														bootbox.alert('<?php echo lang("error.ajax") ?>');
													}
												},
											  	error: function(XMLHttpRequest, textStatus, errorThrown) {
											   		bootbox.alert('<?php echo lang("error.ajax") ?>');
											  	}
											});
										}
									}
								}
							}
						});
					}
					
					//PROYECTOS ASOCIADOS AL PRODUCTO
					function buscar_proyectos_producto(id_producto, product_cantidad) {
						var proyectos_producto = '';
						$('#proyectos_producto').html('');
						
						$.ajax({
							type: "POST",
							dataType: "json",
							async: true,
							url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador_proyectos_producto",
							data: {'product_id':id_producto, 'al_id':al_id},
							success: function(datas) {
								if (datas.length > 0) {
									
									var cant_total = 0;
									var disponible = 0;
									var cant_total_proyecto = 0;
									var detalle_total_proyecto = '';
									proyectos_producto += "<table class='table table-bordered table-striped table-hover dataTable'>";
									proyectos_producto += "<thead><tr><th>Proyecto</th><th>Cantidad</th></tr></thead><tbody>";
									$.each(datas,function(indice) {
										proyectos_producto += "<tr>";
											proyectos_producto += "<td>" + datas[indice]["man_nombre"] + "</td>";
											cant_total_proyecto = parseFloat(datas[indice]["cantidad_entradas"]) - parseFloat(datas[indice]["cantidad_salidas"]);
											detalle_total_proyecto = 'Entradas: ' + datas[indice]["cantidad_entradas"] + '; Salidas: ' + datas[indice]["cantidad_salidas"];
											proyectos_producto += "<td title='"+detalle_total_proyecto+"'>" + cant_total_proyecto + "</td>";											
										proyectos_producto += "</tr>";
										cant_total = cant_total + cant_total_proyecto;
									});
									disponible = parseFloat(product_cantidad) - cant_total;
									proyectos_producto += "<tfoot><tr><th>Disponible en almacen</th><th>"+disponible+"</th></tr></tfoot>";
									proyectos_producto += "</tbody></table>";
									
									$('.num_proyectos_producto').html(' ('+datas.length+')');
								} else {
									proyectos_producto += "No hay proyectos asociados a este producto";
								}
								
								$('#proyectos_producto').html(proyectos_producto);
							},
							  	error: function(XMLHttpRequest, textStatus, errorThrown) {
							   	bootbox.alert('<?php echo lang("error.ajax") ?>');
							}
						});
					}
					
					function buscar_productos() {
						productos = '';
						
						$.ajax({
							type: "POST",
							dataType: "json",
							async: true,
							url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador_productos_proveedor",
							data: {'pro_id':$('#nuevo_producto option:selected').val()},
							success: function(datas) {
								$.each(datas,function(indice) {
									//MOSTRAR SOLO PRODUCTOS QUE YA ESTEN EN ESTE ALMACEN
									//if ($.inArray(datas[indice]["product_id"], productos_almacen) == (-1)) {
										if (indice == 0) {
											productos = productos + '<option value="'+datas[indice]["product_id"]+'" selected>'+datas[indice]["product_nombre"]+' ('+datas[indice]["product_referencia"]+')</option>';
										} else {
											productos = productos + '<option value="'+datas[indice]["product_id"]+'">'+datas[indice]["product_nombre"]+' ('+datas[indice]["product_referencia"]+')</option>';
										}
									//}
								});
								
								$('#nuevo_producto #productos').html(productos);
							},
							  	error: function(XMLHttpRequest, textStatus, errorThrown) {
							   	bootbox.alert('<?php echo lang("error.ajax") ?>');
							}
						});
					}
					
					function buscar_proveedores() {
						proveedores = '';
						
						//RELLENAMOS EL CAMPO DE PROVEEDORES
						$.ajax({
							type: "POST",
							dataType: "json",
							async: true,
							url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador",
							data: {'buscar':''},
							success: function(data) {
								$.each(data,function(indice) {
									if (indice == 0) {
										proveedores = proveedores + '<option value="'+data[indice]["pro_id"]+'" selected>'+data[indice]["pro_nombre"]+'</option>';
									} else {
										proveedores = proveedores + '<option value="'+data[indice]["pro_id"]+'">'+data[indice]["pro_nombre"]+'</option>';
									}
								});
								
								
								$("#nuevo_producto #proveedores").change(function () {
									if (primera_vez) {
										buscar_productos();
									}
								}).trigger('change');
								
								primera_vez = true;
								$('#nuevo_producto #proveedores').html(proveedores);
								buscar_productos();
							},
							  	error: function(XMLHttpRequest, textStatus, errorThrown) {
							   	bootbox.alert('<?php echo lang("error.ajax") ?>');
							}
						});
					}
					
					$("#btn_nuevo").click(function(e){
						e.preventDefault();
						primera_vez = false;
						alert_nuevo_producto();
						buscar_proveedores();
					});
					
					//IMAGENES
					$(".fancybox").fancybox({
						helpers : {
					   		title : false
					   	},
					   	padding: 0,
						openEffect : 'elastic',
						openSpeed  : 150,
						closeEffect : 'elastic',
						closeSpeed  : 150,
						closeClick : false
					});
					
					//ACTUALIZAR PRODUCTO
					$(document).on('click','.actualizar_producto',function(e){
						e.preventDefault();
						
						if (emp_tipo <= 1) {
							var product_cantidad = $(this).parent().find('input[name="product_cantidad"]').val();
							
							if (product_cantidad != '') {
								var product_id = $(this).parent().parent().parent().attr('id');
								
								$.ajax({
									type: "POST",
									dataType: "json",
									url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/actualizar_producto",
									data: {'product_cantidad':product_cantidad, 'al_id':al_id, 'product_id':product_id,'al_tipo':al_tipo},
									success: function(data) {
										if (data.status) {					
											bootbox.alert('<?php echo lang("almacen.producto.editado.ok") ?>');
											
											//BORRAMOS LA FILA DEL PRODUCTO
											if (product_cantidad == 0) {
												$('#example1 tbody tr#'+product_id).remove();
											}
										} else {
											bootbox.alert('<?php echo lang("error.ajax") ?>');
										}
									},
								  	error: function(XMLHttpRequest, textStatus, errorThrown) {
								   		bootbox.alert('<?php echo lang("error.ajax") ?>');
								  	}
								});
							} else {
								bootbox.alert('<?php echo lang("product.cantidad.error") ?>');
							}
						}
					});
					
					//VER PROYECTOS ASOCIADOS A UN PRODUCTO
					$(document).on('click','.ver_proyectos_producto',function(e){
						e.preventDefault();
						
						var product_id = $(this).parent().parent().parent().attr('id');
						var product_codigo = $(this).parent().parent().parent().find('td').eq(0).html();
						var product_nombre = $(this).parent().parent().parent().find('td').eq(1).html();
						var product_cantidad = $(this).parent().find('input[name="product_cantidad"]').val();
						
						bootbox.dialog({
							message: "<div id='proyectos_producto'></div>",
							title: "<?php echo lang('mantenimientos') ?> que usan <br><b>"+product_codigo+" "+product_nombre+"</b> <span class='num_proyectos_producto'></span><br><small>De "+product_cantidad+" unidades</small>",
							buttons: {
								success: {
									label: "<?php echo lang('atras') ?>",
									className: "btn-default"
								}
							}
						});
						buscar_proyectos_producto(product_id, product_cantidad);
					});
					
					//MOVER PRODUCTO DE ALMACEN
					$(document).on('click','.mover_producto',function(e){
						e.preventDefault();
						
						var product_cantidad = $(this).parent().find('input[name="product_cantidad"]').val();
						var product_id = $(this).parent().parent().parent().attr('id');
						
						$.ajax({
							type: "POST",
							dataType: "json",
							url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador",
							data: {'buscar':''},
							success: function(data) {
								var options = '';
								
								$.each(data,function(indice,valor) {
					   				if (data[indice]['al_id'] != al_id && data[indice]['al_tipo'] == al_tipo) {
										if (indice == 0) {
											options = options + '<option value="'+data[indice]['al_id']+'" selected>'+data[indice]['al_nombre']+'</option>';		
										} else {
											options = options + '<option value="'+data[indice]['al_id']+'">'+data[indice]['al_nombre']+'</option>';		
										}
									}    				
					    		});
					    		
					    		//PREGUNTAMOS EN QUE ALMACEN SE MOVERA EL PRODUCTO
					    		bootbox.dialog({
									message: "<div><label><?php echo lang('almacen') ?></label><br><select class='form-control' id='al_id'>"+options+"</select></div><br><div><label><?php echo lang('cantidad') ?></label><input type='text' name='aux_product_cantidad' onKeypress='return soloNumeros(event)' class='text-center form-control' value="+product_cantidad+"></div>",
									title: "<?php echo lang('mover.producto.almacen') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('seleccionar') ?>",
											className: "btn-success",
											callback: function() {
												var aux_product_cantidad = $('input[name="aux_product_cantidad"]').val();
												
												if (parseInt(aux_product_cantidad) > parseInt(product_cantidad)) {
													bootbox.alert('<?php echo lang("producto.cantidad.mayor") ?>');
												} else if (parseInt(aux_product_cantidad) == 0) {
													bootbox.alert('<?php echo lang("producto.cantidad.cero") ?>');
												} else {
													var aux_al_id = $('#al_id option:selected').val();
													
													$.ajax({
														type: "POST",
														dataType: "json",
														url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/mover_producto",
														data: {'product_cantidad':aux_product_cantidad, 'al_id':aux_al_id, 'product_id':product_id, 'old_al_id':al_id, 'old_product_cantidad':product_cantidad,'al_tipo':al_tipo},
														success: function(data) {
															if (data.status) {					
																bootbox.alert('<?php echo lang("almacen.producto.movido.ok") ?>');
																if (product_cantidad == aux_product_cantidad) {
																	$('#example1 tbody tr#'+product_id).remove();
																} else {
																	$('#example1 tbody tr#'+product_id).find('input').val(product_cantidad - aux_product_cantidad);
																}
															} else {
																bootbox.alert('<?php echo lang("error.ajax") ?>');
															}
														},
													  	error: function(XMLHttpRequest, textStatus, errorThrown) {
													   		bootbox.alert('<?php echo lang("error.ajax") ?>');
													  	}
													});
												}	
											}
										}
									}
								});
							},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						  	}
						});
					});
					
					//ENVIAR PRODUCTO AL CLIENTE
					$(document).on('click','.enviar_producto',function(e){
						e.preventDefault();
						
						var product_cantidad = $(this).parent().find('input[name="product_cantidad"]').val();
						var product_id = $(this).parent().parent().parent().attr('id');
						
						$.ajax({
							type: "POST",
							dataType: "json",
							url: "<?php echo base_url().$this->lang->lang() ?>/mantenimientos/buscador",
							data: {'buscar':''},
							success: function(data) {
								var options = '';
								//MANTENIMIENTOS
								$.each(data,function(indice) {
									if (indice == 0) {
										options = options + '<option value="'+data[indice]["man_id"]+'" selected>'+data[indice]["man_nombre"]+' ('+data[indice]["cl_nombre"]+')</option>';
									} else {
										options = options + '<option value="'+data[indice]["man_id"]+'" >'+data[indice]["man_nombre"]+' ('+data[indice]["cl_nombre"]+')</option>';
									}
								});
								
								bootbox.dialog({
									message: "<div><label><?php echo lang('mantenimiento') ?></label><br><select class='form-control' id='man_id'>"+options+"</select></div><br><div><label><?php echo lang('cantidad') ?></label><input type='text' name='aux_product_cantidad' onKeypress='return soloNumeros(event)' class='text-center form-control' value="+product_cantidad+"><br><b><?php echo lang('salida.temporal') ?> <input type='checkbox' id='sa_temporal' style='margin-left: 10px;' name='sa_temporal'></b></div>",
									title: "<?php echo lang('enviar.producto.mantenimiento') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('seleccionar') ?>",
											className: "btn-success",
											callback: function() {
												var aux_product_cantidad = $('input[name="aux_product_cantidad"]').val();
														
												if (parseInt(aux_product_cantidad) > parseInt(product_cantidad)) {
													bootbox.alert('<?php echo lang("producto.cantidad.mayor") ?>');
												} else if (parseInt(aux_product_cantidad) == 0) {
													bootbox.alert('<?php echo lang("producto.cantidad.cero") ?>');
												} else {
													var aux_man_id = $('#man_id option:selected').val();
													
													var sa_temporal = 0;
													if ($('#sa_temporal').is(':checked')) {
														sa_temporal = 1;
													}
													
													$.ajax({
														type: "POST",
														dataType: "json",
														url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/enviar_producto",
														data: {'sa_cantidad':aux_product_cantidad, 'man_id':aux_man_id, 'product_id':product_id, 'al_id':al_id, 'old_product_cantidad':product_cantidad,'al_tipo':al_tipo,'sa_temporal':sa_temporal},
														success: function(data) {
															if (data.status) {					
																bootbox.alert('<?php echo lang("almacen.producto.enviado.ok") ?>');
																if (product_cantidad == aux_product_cantidad) {
																	$('#example1 tbody tr#'+product_id).remove();
																} else {
																	$('#example1 tbody tr#'+product_id).find('input').val(product_cantidad - aux_product_cantidad);
																}
															} else {
																bootbox.alert('<?php echo lang("error.ajax") ?>');
															}
														},
													  	error: function(XMLHttpRequest, textStatus, errorThrown) {
													   		bootbox.alert('<?php echo lang("error.ajax") ?>');
													  	}
													});
												}	
											}
										}
									}
								});
							}
						});
					});
					
					//SELECCIONAR TODOS
					$(document).on('click','.seleccionar_todos',function(e){
						e.preventDefault();
						$('#example1 tbody tr').find('input:checkbox').each(function(index, element) {
                            this.checked = true;
                        });
					});
					
					//ENVIAR PRODUCTOS MASIVO AL CLIENTE
					$(document).on('click','.enviar_productos',function(e){
						e.preventDefault();
						
						var product_cantidad = [];
						var product_id = [];
						var product_nombre = [];
						var pra_id = [];
						var sa_temporal = [];
						
						var w=0;
						
						$('#example1 tbody tr').find('input:checkbox').each(function(index, element) {
							if(this.checked) {
								product_cantidad.push($(this).parent().parent().parent().parent().find('input[name="product_cantidad"]').val());
								product_id.push($(this).val());
								pra_id.push($(this).attr('pra_id'));
								product_nombre.push($(this).parent().parent().parent().parent().find('td:eq(1)').text() +' | '+ $(this).parent().parent().parent().parent().find('td:eq(2)').text() +' | '+ $(this).parent().parent().parent().parent().find('td:eq(3)').text());
								w++;
							}
                        });
						
						if(w>0) {
						
						$.ajax({
							type: "POST",
							dataType: "json",
							url: "<?php echo base_url().$this->lang->lang() ?>/mantenimientos/buscador",
							data: {'buscar':''},
							success: function(data) {
								var options = '';
								//MANTENIMIENTOS
								$.each(data,function(indice) {
									if (indice == 0) {
										options = options + '<option value="'+data[indice]["man_id"]+'" selected>'+data[indice]["man_nombre"]+' ('+data[indice]["cl_nombre"]+')</option>';
									} else {
										options = options + '<option value="'+data[indice]["man_id"]+'" >'+data[indice]["man_nombre"]+' ('+data[indice]["cl_nombre"]+')</option>';
									}
								});
								
								var Message = "<div><label><?php echo lang('mantenimiento') ?></label><br><select class='form-control' id='man_id'>"+options+"</select></div><br>";
								
								//PRODUCTOS
								$.each(product_id,function(indice,valor) {
									
									Message = Message + "<div class='aux_product' id='"+indice+"'><div><label>"+product_nombre[indice]+"</label></div><div><label><?php echo lang('cantidad') ?></label><input type='text' name='aux_product_cantidad[]' onKeypress='return soloNumeros(event)' class='text-center aux_product_cantidad form-control' value="+product_cantidad[indice]+"><br></div>"
									Message = Message + "<div class='checkbox'><label><input type='checkbox' id='sa_temporal' style='margin-left: 10px;' name='sa_temporal[]'>&nbsp;<?php echo lang('salida.temporal') ?></label></div></div><hr/>";
								});
								
								bootbox.dialog({
									message: Message,
									title: "<?php echo lang('enviar.productos.mantenimiento') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('seleccionar') ?>",
											className: "btn-success",
											callback: function() {
												
												var aux_product_cantidad = [];
												var enviar;
												var ind;
												
												$.each(product_id,function(indice,valor) {
													
													aux_product_cantidad.push($('.aux_product#'+indice+' input.aux_product_cantidad').val());
													
													if (parseInt(aux_product_cantidad[indice]) > parseInt(product_cantidad[indice])) {
														enviar = 'mayor';
														ind = indice;
														return false;
													} else if (parseInt(aux_product_cantidad[indice]) == 0) {
														enviar = 'cero';
														ind = indice;
														return false;
													} else {
														enviar = 'enviar';
													}
													
												});
												if (enviar=='mayor') {
													
													bootbox.alert('<?php echo lang("producto.cantidad.mayor") ?> '+'<br><strong>Producto:</strong> '+product_nombre[ind]);
													
												} else if(enviar=='cero') {
													
													bootbox.alert('<?php echo lang("producto.cantidad.cero") ?> '+'<br><strong>Producto:</strong> '+product_nombre[ind]);
													
												} else if(enviar=='enviar') {
													
													var aux_man_id = $('#man_id option:selected').val();
													
													$('.aux_product').find('input:checkbox').each(function(index, element) {
														if (this.checked) {
															sa_temporal.push(1);
														} else {
															sa_temporal.push(0);
														}
													});
													
													$.ajax({
														type: "POST",
														dataType: "json",
														url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/enviar_productos",
														data: {'sa_cantidad':aux_product_cantidad, 'man_id':aux_man_id, 'product_id':product_id, 'al_id':al_id, 'old_product_cantidad':product_cantidad,'al_tipo':al_tipo,'sa_temporal':sa_temporal,'pra_id':pra_id},
														success: function(data) {
															if (data.status) {					
																bootbox.alert('<?php echo lang("almacen.producto.enviado.ok") ?>');
																
																$.each(product_id,function(indice,valor) {
																	if (product_cantidad[indice] == aux_product_cantidad[indice]) {
																		$('#example1 tbody tr#'+valor+'[pra_id="'+pra_id[indice]+'"]').remove();
																	} else {
																		$('#example1 tbody tr#'+valor+'[pra_id="'+pra_id[indice]+'"]').find('input[type=text]').val(product_cantidad[indice] - aux_product_cantidad[indice]);
																	}
																});
																
															} else {
																bootbox.alert('<?php echo lang("error.ajax") ?>');
															}
														},
													  	error: function(XMLHttpRequest, textStatus, errorThrown) {
													   		bootbox.alert('<?php echo lang("error.ajax") ?>');
													  	}
													});
												}	
											}
										}
									}
								});
							}
						});
						} else {
							bootbox.alert('<?php echo lang("error.seleccionar.productos") ?>');
						}
					});
			},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		  	}
		});
	});
</script>