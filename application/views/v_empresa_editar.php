<?php
//CREAMOS LOS INPUT DEL FORMULARIO
$input_nombre = array(
	'name'		=>	'em_nombre',
	'id'		=>	'em_nombre',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($em_nombre)?$em_nombre:set_value('em_nombre')
);

$input_dni = array(
	'name'		=>	'em_cif',
	'id'		=>	'em_cif',
	'class'		=>	'form-control',
	'maxlength'	=>	'30',
	'value'		=>	isset($em_cif)?$em_cif:set_value('em_cif')
);

$input_telefono = array(
	'name'		=>	'em_telefono',
	'id'		=>	'em_telefono',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'value'		=>	isset($em_telefono)?$em_telefono:set_value('cl_telefono')
);

$input_email = array(
	'name'		=>	'em_email',
	'id'		=>	'em_email',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($em_email)?$em_email:set_value('em_email')
);

$input_email_contabilidad = array(
	'name'		=>	'em_email_contabilidad',
	'id'		=>	'em_email_contabilidad',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($em_email_contabilidad)?$em_email_contabilidad:set_value('em_email_contabilidad')
);

$input_email_opcional = array(
	'name'		=>	'em_email_opcional',
	'id'		=>	'em_email_opcional',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($em_email_opcional)?$em_email_opcional:set_value('em_email_opcional')
);

$input_email_opcional2 = array(
	'name'		=>	'em_email_opcional2',
	'id'		=>	'em_email_opcional2',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($em_email_opcional2)?$em_email_opcional2:set_value('em_email_opcional2')
);

$input_provincia = array(
	'name'		=>	'em_provincia',
	'id'		=>	'em_provincia',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($em_provincia)?$em_provincia:set_value('em_provincia')
);

$input_postal = array(
	'name'		=>	'em_postal',
	'id'		=>	'em_postal',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	isset($em_postal)?$em_postal:set_value('em_postal')
);

$input_direccion = array(
	'name'		=>	'em_direccion',
	'id'		=>	'em_direccion',
	'class'		=>	'form-control',
	'maxlength'	=>	'150',
	'value'		=>	isset($em_direccion)?$em_direccion:set_value('em_direccion')
);

$input_barriada = array(
	'name'		=>	'em_barriada',
	'id'		=>	'em_barriada',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($em_barriada)?$em_barriada:set_value('em_barriada')
);

$input_localidad = array(
	'name'		=>	'em_localidad',
	'id'		=>	'em_localidad',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($em_localidad)?$em_localidad:set_value('em_localidad')
);
?>

<div class="row">
	<div class="col-lg-4 col-md-4 col-xs-12 text-center">
		<img src="<?php echo base_url().$this->lang->lang().'/empresa/view_file/'.$em_cif.'/logo.jpg' ?>" id="em_logo" class="img-thumbnail" />			
	</div>
	
	<!-- FORMULARIO CLIENTE -->
	<div class="col-lg-8 col-md-8 col-xs-12">
		<form id="datos_empresa" action="<?php echo base_url().$this->lang->lang() ?>/empresa/guardar_editar" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('nombre') ?></label>
					    <?php echo form_input($input_nombre) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_nombre') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('cif') ?></label>
					    <?php echo form_input($input_dni) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_cif') ?></div>
					</div>
				</div>
				
				<div class="col-lg-3 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('telefono') ?></label>
					    <?php echo form_input($input_telefono) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_telefono') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('email') ?></label>
					    <?php echo form_input($input_email) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_email') ?></div>
					</div>
				</div>
				
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('email.contabilidad') ?></label>
					    <?php echo form_input($input_email_contabilidad) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_email_contabilidad') ?></div>
					</div>
				</div>
			</div>
            
            <div class="row">
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('email.opcional') ?> 1</label>
					    <?php echo form_input($input_email_opcional) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_email_opcional') ?></div>
					</div>
				</div>
				
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('email.opcional') ?> 2</label>
					    <?php echo form_input($input_email_opcional2) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_email_opcional2') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
					    <label><?php echo lang('direccion') ?></label>
					    <?php echo form_input($input_direccion) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_direccion') ?></div>
					</div>
				</div>
				
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('provincia') ?></label>
					    <?php echo form_input($input_provincia) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_provincia') ?></div>
					</div>
				</div>
				
				<div class="col-lg-2 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('postal') ?></label>
					    <?php echo form_input($input_postal) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_postal') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('barriada') ?></label>
					    <?php echo form_input($input_barriada) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_barriada') ?></div>
					</div>
				</div>
			
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('localidad') ?></label>
					    <?php echo form_input($input_localidad) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_localidad') ?></div>
					</div>
				</div>
				
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('pais') ?></label>
					    <select name="em_pais" id="paises" class="form-control"></select>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('em_pais') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12 col-xs-12">					
					<div class="input-group">
						<span class="input-group-btn">
							<span class="btn btn-primary btn-file">
								<!--<input type="file" multiple>-->
								<i class="fa fa-camera"></i> <?php echo lang('cargar.logo') ?>
								<input type="file" id="foto" name="foto">
							</span>
						</span>
						<input type="text" class="form-control" readonly>
					</div>
					<div class="text-danger"><?php echo form_error('foto') ?></div>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="row text-right">
	<div class="col-lg-12 col-xs-12">
		<br>
		<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-save"></i> <?php echo lang('guardar') ?></button>
	</div>
</div>

<script>
/* ESTILO Y BOTON FOTO */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function(){
	//RELLENAMOS EL CAMPO DE PAISES PARA AÑADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/sucursales/paises",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
				//PAIS
				if (data[indice]["id_countries"] == '<?php echo isset($em_pais)?$em_pais:"" ?>') {
					$('#paises').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
				} else {
					$('#paises').append('<option value="'+data[indice]["id_countries"]+'">'+data[indice]["name"]+'</option>');
				}
		});
	}, "json");
	
	
	//BLOQUEAMOS EL CAMPO CIF
	$('#em_cif').attr('readonly', true);
	
	//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO
	$('#datos_empresa .form-group span').hide();
	
	//ESTILO Y BOTON FOTO
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {	
		var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}	
	});
	
	//EJECUTAMOS EL FORMULARIO AL DARLE AL BOTON
	$('#btn_guardar').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
	 	//l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#datos_empresa .form-group, #datos_empresa .input-group").removeClass("has-error has-feedback");
	    $('#datos_empresa .glyphicon-remove').hide();
	    $("#datos_empresa .text-danger").html('');
	    
	    jQuery("#datos_empresa").ajaxForm({
	    	dataType: 'json',
			success: function(data) {
				if (data.status == false) {
					//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
					//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
					$.each(data.errors, function (ind, elem) {
						$.each(data.errors[ind], function (ind2, elem2) {
							//SI EXSITE ERROR EN EL CAMPO FOTO ENTRAMOS POR AQUI
							if (data.errors[ind][0] == 'foto') {
								$('input[name='+data.errors[ind][0]+']').parent().parent().parent().parent().find('.text-danger').html(data.errors[ind][ind2]);
								$('input[name='+data.errors[ind][0]+']').parent().parent().parent().addClass("has-error has-feedback");
							}
							//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
							$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
							$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
							$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
						}); 
					}); 
				} else {
					//TODO CORRECTO
					$('#em_logo').attr("src","<?php echo base_url().$this->lang->lang().'/empresa/view_file/'.$em_cif.'/logo.jpg'; ?>"+"?timestamp=" + new Date().getTime());
					bootbox.alert('<?php echo lang("empresa.editado.ok") ?>');
				}
				//PARAMOS EL LOADING DEL BOTON
				l.stop();
			}
		}).submit();
	});
	
});
</script>