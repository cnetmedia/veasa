<?php
$disable = '';
$disableTrabajador = '';
$disableGuardar = '';
if (( ($this->session->userdata('de_id') == '3') || ($this->session->userdata('de_id') == '4') || ($this->session->userdata('de_id') == '5') || ($this->session->userdata('emp_tipo') > 2)) && $tr_id != '') {
	$disable = 'disabled';
}

if ($this->session->userdata('emp_tipo') > 1 && $tr_id != '') {
	$disableTrabajador = 'disabled';
}

if ($this->session->userdata('emp_tipo') > 1 && $tr_estado >= 1) {
	$disableGuardar = 'disabled';
}

?>
<script>
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
return ((key >= 48 && key <= 57) || (key==8) || (key == 46));
}
</script>
<div class="nav-tabs-custom" id="trabajos">
        <?php if (isset($tr_id)) { ?>
	        <ul class="nav nav-tabs">
	            <li class="active">
	            	<a href="#tab_1" data-toggle="tab"><?php echo lang('informacion') ?></a>
	            </li>
	            
	            <li class="">
	            	<a href="#tab_2" data-toggle="tab"><?php echo lang('inspecciones') ?></a>
	            </li>

	            <li class="">
		        	<a href="#tab_3" data-toggle="tab"><?php echo lang('bitacora') ?></a>
		        </li>
	        </ul>
        <?php } ?>
        
        <div class="tab-content">
        	<div class="tab-pane active" id="tab_1">
        		<div class="row">
					<div class="col-lg-6 col-xs-12">
						<div class="row">
							
								<div class="<?php echo ($tr_id != '')?'col-lg-12':'col-lg-6'; ?>">
									<label><?php echo lang('jornada.trabajo') ?></label>
									<div class="form-group">
						                <input class="form-control" name="tr_fecha_inicio" id="jornada_trabajo" type="text" <?php echo $disable; ?>>
										<div class="text-danger"><?php echo form_error('tr_fecha_inicio') ?></div>
						            </div>
								</div>
                                
								<?php if ($tr_id == '') { ?>
								<div class="col-lg-3">
									<label><?php echo lang('repetir.dia.semana') ?></label>
									<select id="tr_repetir" name="tr_repetir[]" class="form-control" multiple>
										<option value="" selected><?php echo lang('no.repetir') ?></option>
										<option value="1"><?php echo lang('lunes') ?></option>
										<option value="2"><?php echo lang('martes') ?></option>
										<option value="3"><?php echo lang('miercoles') ?></option>
										<option value="4"><?php echo lang('jueves') ?></option>
										<option value="5"><?php echo lang('viernes') ?></option>
										<option value="6"><?php echo lang('sabado') ?></option>
										<option value="0"><?php echo lang('domingo') ?></option>
									</select>
								</div>
                                                                
								
								<div class="col-lg-3">
									<label><?php echo lang('repeticiones') ?></label>
									<input id="tr_repetir_cantidad" name="tr_repetir_cantidad" class="form-control" value="" />
									<div class="text-danger"><?php echo form_error('cl_id') ?></div>
								</div>
                                <?php } ?>
							
						</div>
                                            
                        <div class="form-group">
							<label><?php echo lang('inspecciones') ?></label>
							<select id="inspecciones" name="in_id" class="form-control" multiple <?php echo $disable; ?>></select>
							<div class="text-danger"></div>
						</div>
						
						<div class="form-group">
							<label><?php echo lang('inspecciones.avanzadas') ?></label>
							<select id="inspecciones_avanzadas" name="inv_id" class="form-control" multiple <?php echo $disable; ?>></select>
							<div class="text-danger"></div>
						</div>
						
						<div class="form-group">
							<label><?php echo lang('empleados.registro') ?></label>
						</div>
					</div>
					
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label><?php echo lang('sucursal') ?></label>
							<select id="sucursales" name="su_id" class="form-control" <?php echo $disable; ?>></select>
							<div class="text-danger"><?php echo form_error('su_id') ?></div>
						</div>
						
						<div class="form-group">
							<label><?php echo lang('departamento') ?></label>
							<select id="departamentos" name="de_id" class="form-control" <?php echo $disable; ?>></select>
							<div class="text-danger"><?php echo form_error('de_id') ?></div>
						</div>
						
						<div class="form-group">
							<label><?php echo lang('empleados') ?></label>
							<select id="empleados" name="emp_id" class="selectpicker form-control" data-live-search="true" <?php echo $disable; ?>></select>
							<div class="text-danger"><?php echo form_error('emp_id') ?></div>
                             
							 <div id="empleados_asignados">
                             <?php 
                             $empleados_asignados = isset($empleados_asignados)?$empleados_asignados:"";
                                                            
                             if ($empleados_asignados != '') {
                             	echo '<br><label>'.lang('empleados.asignados').'</label><br>';
                                                                
                                for ($x=0; $x<count($empleados_asignados); $x++) {
                                	if ($disable == "") {
										echo '<span emp_id="'.$empleados_asignados[$x]->emp_id.'"><i class="fa fa-times pointer"></i> '.$empleados_asignados[$x]->emp_nombre.' '.$empleados_asignados[$x]->emp_apellido1.' '.$empleados_asignados[$x]->emp_apellido2.'</span><br>';
									} else {
										echo $empleados_asignados[$x]->emp_nombre.' '.$empleados_asignados[$x]->emp_apellido1.' '.$empleados_asignados[$x]->emp_apellido2.'<br>';
									}
                                }
                             }
                                                            
                             ?>
                             </div>
						</div>
						
						<div class="form-group">
							<label><?php echo lang('presupuesto') ?></label>
							<select id="cotizaciones" name="pr_id" class="selectpicker form-control" data-live-search="true" <?php echo $disable; ?>></select>
							<div class="text-danger"><?php echo form_error('pr_id') ?></div>
							
							<div id="presupuestos_asignados">
                             <?php 
                             $presupuestos_asignados = isset($presupuestos_asignados)?$presupuestos_asignados:"";
                                                            
                             if ($presupuestos_asignados != '') {
                             	echo '<br><label>'.lang('presupuestos.asignados').'</label><br>';
                                                                
                                for ($x=0; $x<count($presupuestos_asignados); $x++) {
                                	if ($disable == "") {
										echo '<span pr_id="'.$presupuestos_asignados[$x]->pr_id.'" man_id="'.$presupuestos_asignados[$x]->man_id.'"  man_latitud="'.$presupuestos_asignados[$x]->man_latitud.'" man_longitud="'.$presupuestos_asignados[$x]->man_longitud.'" pr_id="'.$presupuestos_asignados[$x]->man_nombre.'"><i class="fa fa-times pointer"></i> '.$presupuestos_asignados[$x]->pr_numero.' - '.$presupuestos_asignados[$x]->man_nombre.' - '.$presupuestos_asignados[$x]->pr_fecha.'</span><br>';
									} else {
										echo $presupuestos_asignados[$x]->pr_numero.' - '.$presupuestos_asignados[$x]->man_nombre.' - '.$presupuestos_asignados[$x]->pr_fecha.'<br>';
									}									
                                }
                             }
                                                            
                             ?>
                             </div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div></div>
						<div id="map-trabajos"></div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label><?php echo lang('comentario') ?></label>
							<textarea id="tr_comentario" name="tr_comentario" class="form-control" style="min-width: 100%; max-width: 100%; min-height: 100px; max-height: 100px;" <?php echo $disable; ?>><?php echo isset($tr_comentario)?$tr_comentario:'' ?></textarea>
							<div class="text-danger"><?php echo form_error('tr_comentario') ?></div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12 text-right">
						<!-- SI PUEDE CREAR O EDITAR CONTENIDO MUESTRA EL BOTON -->
							<?php if($tr_estado < 1 || $disableGuardar == ''): ?>
								<?php if ($this->session->userdata('emp_crear') == '1' || $this->session->userdata('emp_editar') == '1') { ?>
									<?php if (($this->session->userdata('de_id') == '1') || 
											  ($this->session->userdata('de_id') == '2') ||
											  ($this->session->userdata('de_id') == '7') ) { ?>
										<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
											<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
										</button>
										
										
										<?php if($tr_estado < 1): ?>
											<button id="btn_procesar" type="button" class="btn btn-warning ladda-button" data-style="zoom-out" data-estado="1"><i class="fa fa-check"></i> <?php echo lang('trabajo.procesar') ?></button>
										<?php endif; ?>
									<?php } ?>	
								<?php } ?>
							<?php endif; ?>
						
						<?php if(isset($_GET['trg'])): ?>							
							<a href="<?php echo base_url().$this->lang->lang().'/trabajos/guardados' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
						<?php else: ?>
	                     	<a href="<?php echo base_url().$this->lang->lang().'/trabajos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
	                    <?php endif; ?>
						
					</div>
				</div>
        	</div>    	
        	
        	<div class="tab-pane" id="tab_2">
        		<div id="ver_trabajo">
					<!-- SOLO PARA SUPERVISORES -->
					<?php if ($this->session->userdata('emp_tipo') <= 2) { ?>
						<div class="row">
					    	<div class="col-lg-4 col-ms-4 col-sm-6 col-xs-12">
					        	<label><?php echo lang('mantenimientos') ?></label>
					            <select id="inspecc_mant" class="form-control"></select>
							</div>
					    </div>
					            	
					    <br>
					            	
					    <div class="row" id="inspec"></div>
					<?php } ?>
						
					<div class="row">
						<div class="col-lg-12 text-right">	
							<a href="<?php echo base_url().$this->lang->lang().'/trabajos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
						</div>
					</div>
				</div>
        	</div>

        	<div class="tab-pane" id="tab_3">
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<table id="bitacora" class="table table-bordered">
	    					<thead>
	    						<th class="col-sm-2"><?php echo lang('fecha') ?></th>
	                            <th class="col-sm-3"><?php echo lang('empleados') ?></th>
	                            <th class="col-sm-3"><?php echo lang('accion') ?></th>
	                            
	    					</thead>
	    					
	    					<tbody></tbody>
	    					
	    					<tfoot>
		    					<tr>
		    						
		    					</tr>
	    					</tfoot>
	    				</table>
	    			</div>
	    		</div>
	    	</div>
        </div>
</div>

<script>
$(document).ready(function(){

	//DATOS EMPLEADO DE LA SESSION
	var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	var su_id = '<?php echo $this->session->userdata("su_id") ?>';
	var emp_editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	
	//OPCIONES GENERALES
	var primera_vez = false;
	var primera_vez_departamentos = false;
	var primera_vez_empleados = false;
	var primera_vez_inspecciones = false;
	var primera_vez_presupuestos = false;
	var pr_id = '<?php echo isset($pr_id)?$pr_id:"" ?>';
	var tr_id = '<?php echo isset($tr_id)?$tr_id:"" ?>';
	var emple = '<?php echo isset($emp_id)?$emp_id:"" ?>';
	var tr_estado = '<?php echo isset($tr_estado)?$tr_estado:"" ?>';

	if (tr_estado == 2) {
		$('select,textarea, #jornada_trabajo').attr('disabled',true);
		$('#btn_guardar').hide();
		$('#btn_procesar').hide();
	}

	if (emple == '') {
		emple = new Array();
	} else {
		emple = emple.split(',');
		if (emple[emple.length-1] == '') {
			emple.splice(emple.length-1,1);
		}
	}
	var presu = '<?php echo isset($pr_id)?$pr_id:"" ?>';
	if (presu == '') {
		presu = new Array();
	} else {
		presu = presu.split(',');
		if (presu[presu.length-1] == '') {
			presu.splice(presu.length-1,1);
		}
	}
	
	//VARIABLES MAPA
	/*var locations = new Array();*/
	var locations = '<?php echo isset($man_id)?$man_id:"" ?>';
	var tiene_panama = false;
	if (locations.length == 0) {
		locations = new Array();
		var mantenimientos = new Array(
			"PANAMA",
			"8.98029389",
			"-79.52030727",
			"PANAMA"
		);		
		locations.push(mantenimientos);
		tiene_panama = true;
	} else {
		locations = locations.split(';');
		//BORRA ELEMENTOS VACIOS
		locations = locations.filter(Boolean);
		
		for (var x=0; x<locations.length; x++) {
			locations[x] = locations[x].split(',');
			//BORRA ELEMENTOS VACIOS
			locations[x] = locations[x].filter(Boolean);
		}
	}
	
	var map;
	var directionsDisplay;
	var directionsService = new google.maps.DirectionsService();
	
	//CALENDARIO
	var meses = '';
	var dias = '';
	var primerDia = 0;
	if ('<?php echo $this->lang->lang() ?>' == 'es') {
		meses = ['<?php echo lang("enero") ?>', '<?php echo lang("febrero") ?>', '<?php echo lang("marzo") ?>', '<?php echo lang("abril") ?>', '<?php echo lang("mayo") ?>', '<?php echo lang("junio") ?>', '<?php echo lang("julio") ?>', '<?php echo lang("agosto") ?>', '<?php echo lang("septiembre") ?>', '<?php echo lang("octubre") ?>', '<?php echo lang("noviembre") ?>', '<?php echo lang("diciembre") ?>'];
		
		dias = ['<?php echo lang("d") ?>', '<?php echo lang("l") ?>', '<?php echo lang("m") ?>', '<?php echo lang("x") ?>', '<?php echo lang("j") ?>', '<?php echo lang("v") ?>','<?php echo lang("s") ?>'];
		primerDia = 1;
	}
	
	var dateInicio = '<?php echo isset($tr_fecha_inicio)?$tr_fecha_inicio:"" ?>';
	if (dateInicio == '') {
		dateInicio = moment();
	}
	
	var dateFin = '<?php echo isset($tr_fecha_fin)?$tr_fecha_fin:"" ?>';
	if (dateFin == '') {
		dateFin = moment();
	}
	
	//CALENDARIO PARA LA JORNADA DE TRABAJO
	$('#jornada_trabajo').daterangepicker({
		timePicker: true,
		timePickerIncrement: 1,
		format: 'YYYY-MM-DD HH:mm',
		language: '<?php echo $this->lang->lang() ?>',
		timePicker12Hour: false,
		locale: {
            applyLabel: '<?php echo lang("aplicar") ?>',
            cancelLabel: '<?php echo lang("cancelar") ?>',
            fromLabel: '<?php echo lang("desde") ?>',
            toLabel: '<?php echo lang("hasta") ?>',
            firstDay: primerDia,
            daysOfWeek: dias,
            monthNames: meses
        },
        showDropdowns: true,
        showWeekNumbers: true,
        startDate: dateInicio,
        endDate: dateFin,
        minDate: dateInicio
	});
	
	//RELLENAMOS EL CAMPO DE PRESUPUESTOS
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/presupuestos/buscador",
	{'buscar':''},
	function(data){
		//MANTENIMIENTO ENCONTRADO
		var encontrado = false;
		$('#cotizaciones').append('<option value="">&nbsp;</option>');
		
		$.each(data,function(indice) {
			if (data[indice]["pr_estado"] != '3') {
				var aux = $.inArray(data[indice]["pr_id"], presu);
				if (aux == -1) {
					$('#cotizaciones').append('<option value="'+data[indice]["pr_id"]+'" man_id="'+data[indice]["man_id"]+'" man_nombre="'+data[indice]["man_nombre"]+'" man_latitud="'+data[indice]["man_latitud"]+'" man_longitud="'+data[indice]["man_longitud"]+'" pr_numero="'+data[indice]["pr_numero"]+'">'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>');
				}
			}
		});
                
        $('#cotizaciones').selectpicker();
        $('#cotizaciones').selectpicker('refresh');
        
        primera_vez_presupuestos = true;
	}, "json");
	
	//A�ADIR PRESUPUESTO Y MANTENIMIENTO
    $('#cotizaciones').change(function(){
    	if (primera_vez_presupuestos) {
    		var obj = $(this).find('option:selected');
			if (obj.val() != '') {		    	
		    	var aux = $.inArray(obj.val(), presu);
		    	
		    	if (aux == -1) {
		    		var man_longitud = obj.attr('man_longitud'); var man_latitud = obj.attr('man_latitud');
		    		if(man_longitud == '' && man_latitud == ''){
		    			man_latitud = "8.98029389";
		    			man_longitud = "-79.52030727";
		    		}
					var mantenimientos = new Array(
						obj.attr('man_id'),
						man_latitud,
						man_longitud,
						obj.attr('man_nombre')
					);

					if(tiene_panama && locations.length == 1){
						locations = new Array();
						tiene_panama = false;
					}
					

					locations.push(mantenimientos);
					setTimeout(mapa(''),1000);
					
					presu.push(obj.val());
					
					var aux = '<span pr_id="'+obj.val()+'" man_id="'+obj.attr('man_id')+'" man_latitud="'+man_latitud+'" man_longitud="'+man_longitud+'" man_nombre="'+obj.attr('man_nombre')+'"><i class="fa fa-times pointer"></i> '+obj.html()+'</span><br>';
					
					obj.parent().next().next().next().append(aux);
				}
			}
		}
    }).trigger('change');
    
    //ELIMINAR COTIZACION
    $('#presupuestos_asignados').on('click', 'i', function(){
    	var pr_id = $(this).parent().attr('pr_id');
    	
    	var posicion;
    	$('#presupuestos_asignados span').each(function(index){
    		if ($(this).attr('pr_id') == pr_id) {
				posicion = index;
			}
    	});
    	
    	if (locations.length == 1) {
			locations = new Array();
		} else {
			locations.splice(posicion,posicion);
		}
    	
    	$(this).parent().next().remove();
    	$(this).parent().remove();
    	
    	
    	var aux = $.inArray(pr_id, presu);
    	presu.splice(aux,aux);
    	setTimeout(mapa(''),1000);
    });
	
	//RELLENAMOS LAS SUCURSALES CUANDO ELEGIMOS UNA FECHA
	$('.daterangepicker .applyBtn').on('click', function() {
		sucursales();
	});
	
	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	$("#trabajos .form-group, #trabajos .input-group").removeClass("has-error has-feedback");
	$("#trabajos .text-danger").html('');
	
	//RELLENAMOS LAS INSPECCIONES
	function inspecciones() {
		//INSPECCIONES BASICAS
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/inspecciones/inspecciones_sucursal",
			{'su_id':$('#sucursales option:selected').val()},
			function(data){
				$('#inspecciones').html('');
				$.each(data,function(indice) {

						if (data[indice]["in_id"] == '<?php echo isset($in_id)?$in_id:"" ?>') {
							$('#inspecciones').append('<option value="'+data[indice]["in_id"]+'" selected>'+data[indice]["in_nombre"]+'</option>');
						} else {
							$('#inspecciones').append('<option value="'+data[indice]["in_id"]+'">'+data[indice]["in_nombre"]+'</option>');
						}
				});
				
				//SI ES UN TRABAJO A EDITAR MARCAMOS LAS INSPECCIONES GUARDADAS
				if (tr_id != '') {
					var inspec_php = '<?php echo isset($in_id)?$in_id:"" ?>';
					inspec_php = inspec_php.split(',');
					
					$('#inspecciones option').each(function(indi){
						for (var i=0; i < inspec_php.length; i++) {
							if ($(this).val() == inspec_php[i]) {
								$(this).prop("selected", true);
							}
						}
					});
				}
			}, "json"
		);
		
		//INSPECCIONES AVANZADAS
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/inspecciones/inspecciones_avanzadas_sucursal",
			{'su_id':$('#sucursales option:selected').val()},
			function(datas){
				$('#inspecciones_avanzadas').html('');
							
				$.each(datas,function(indice) {					
						if (datas[indice]["inv_id"] == '<?php echo isset($inv_id)?$inv_id:"" ?>') {
							$('#inspecciones_avanzadas').append('<option value="'+datas[indice]["inv_id"]+'" selected>'+datas[indice]["inv_nombre"]+'</option>');
						} else {
							$('#inspecciones_avanzadas').append('<option value="'+datas[indice]["inv_id"]+'">'+datas[indice]["inv_nombre"]+'</option>');
						}
				});
							
				//SI ES UN TRABAJO A EDITAR MARCAMOS LAS INSPECCIONES GUARDADAS
				if (tr_id != '') {
					var inspec_avanz_php = '<?php echo isset($inv_id)?$inv_id:"" ?>';
					inspec_avanz_php = inspec_avanz_php.split(',');
								
					$('#inspecciones_avanzadas option').each(function(indi){
						for (var i=0; i < inspec_avanz_php.length; i++) {
							if ($(this).val() == inspec_avanz_php[i]) {
								$(this).prop("selected", true);
							}
						}
					});
				}
			}, "json"
		);
		
		primera_vez_inspecciones = true;
	}
	
	//RELLENAMOS LOS EMPLEADOS
	function empleados() {
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/empleados/empleados_libres_trabajos",
			{'su_id':$('#sucursales option:selected').val(),'emp_id':emple,'fecha':$('#jornada_trabajo').val(),'tr_id':tr_id},
			function(data){
				
				$('#empleados').html('');
				$('#empleados').append('<option value="">&nbsp;</option>');
						
				var cont_emp = 0;
				$.each(data,function(indice) {
					var aux = $.inArray(data[indice]["emp_id"], emple);
					
					//$('#empleados').append('<option value="'+data[indice]["emp_id"]+'">'+data[indice]["emp_nombre"]+' '+data[indice]["emp_apellido1"]+' '+data[indice]["emp_apellido2"]+'</option>');
					
					if (data[indice]["de_id"] == $('#departamentos option:selected').val() && aux == -1) {
						$('#empleados').append('<option value="'+data[indice]["emp_id"]+'">'+data[indice]["emp_nombre"]+' '+data[indice]["emp_apellido1"]+' '+data[indice]["emp_apellido2"]+'</option>');
						cont_emp++;
					}
				});
				if (cont_emp == 0) {
					$('#empleados').append('<option value="">No hay m�s empleados disponibles en la fecha seleccionada</option>');
				}
				
				$('#empleados').selectpicker();
        		$('#empleados').selectpicker('refresh');
			}, "json"
		);
	}
	
	//A�ADIR EMPLEADO
    $('#empleados').change(function(){
    	if (primera_vez_empleados) {
    		var obj = $(this).find('option:selected');
			if (obj.val() != '') {		    	
		    	var aux = $.inArray(obj.val(), emple);
		    	
		    	if (aux == -1) {					
					emple.push(obj.val());
					
					var aux = '<span emp_id="'+obj.val()+'"><i class="fa fa-times pointer"></i> '+obj.html()+'</span><br>';
					
					obj.parent().next().next().next().append(aux);
				}
			}
		}
    }).trigger('change');
    
    //ELIMINAR EMPLEADO
    $('#empleados_asignados').on('click', 'i', function(){
    	var emp_id = $(this).parent().attr('emp_id');
    	
    	var posicion = $.inArray(emp_id, emple);
    	
    	if (emple.length == 1) {
			emple = new Array();
		} else {
			emple.splice(posicion,posicion);
		}
    	
    	$(this).parent().remove();
    });
	
	//RELLENAMOS LOS DEPARTAMENTOS
	function departamentos() {
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/departamentos/departamento_sucursal",
			{'su_id':$('#sucursales option:selected').val()},
			function(data){
				$('#departamentos').html('');
				
				$.each(data,function(indice) {
					if (data[indice]["de_id"] == '<?php echo isset($de_id)?$de_id:"" ?>') {
						$('#departamentos').append('<option value="'+data[indice]["de_id"]+'" selected>'+data[indice]["de_nombre"]+'</option>');
					} else if (data[indice]["de_estado"] == 1) {
						$('#departamentos').append('<option value="'+data[indice]["de_id"]+'">'+data[indice]["de_nombre"]+'</option>');
					}
				});
				
				primera_vez_empleados = true;
				empleados();
			}, "json"
		);
	}
	
	//CARGAMOS LOS EMPLEADOS
	$("#departamentos").change(function () {
		if (primera_vez_empleados) {
			empleados();
		}	
	}).trigger('change');
		
	//RELLENAMOS LAS SUCURSALES
	function sucursales() {		
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
			{'buscar':''},
			function(data){
				$('#sucursales').html('');
				
				$.each(data,function(indice) {					
						if (data[indice]["su_id"] == '<?php echo isset($su_id)?$su_id:"" ?>') {
							$('#sucursales').append('<option pais="'+data[indice]["su_pais"]+'" name_pais="'+data[indice]["name"]+'" value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
						} else if (data[indice]["su_id"] == su_id) {
							$('#sucursales').append('<option pais="'+data[indice]["su_pais"]+'" name_pais="'+data[indice]["name"]+'" value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
						} else if (data[indice]["su_estado"] == 1) {
							$('#sucursales').append('<option pais="'+data[indice]["su_pais"]+'" name_pais="'+data[indice]["name"]+'" value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
						}
				});
				
				$('#map-trabajos').prev().html("<label><?php echo lang('mapa.ruta') ?> <span id='km'></span></label>");
				
				$('#map-trabajos').css({'height':'370px','margin-bottom':'20px'});
				
				primera_vez_departamentos = true;
				departamentos();
				if (!primera_vez_inspecciones) {
					inspecciones();
				}
			}, "json"
		);
	}
		
	//CARGAMOS LOS DEPARTAMENTOS
	$("#sucursales").change(function () {
		if (primera_vez_departamentos) {
			primera_vez_empleados = false;
			primera_vez_inspecciones = false;
			locations = new Array();
			emple = new Array();
			mapa($('#sucursales option:selected').attr('name_pais'));
			departamentos();
			if (!primera_vez_inspecciones) {
				inspecciones();
			}
		}	
	}).trigger('change');
	
	//EDITAR TRABAJO
	if (tr_id != '') {
		$('#jornada_trabajo').val('<?php echo isset($tr_fecha_inicio)?$tr_fecha_inicio:"" ?> - <?php echo isset($tr_fecha_fin)?$tr_fecha_fin:"" ?>');
		
		sucursales();
	}
	
	//MAPA		
	function mapa(direcc) {
		//MAPA PARA SUCURSALES SOLO SIN PUNTOS DE LOCALIZACION
		if (locations.length == 0) {
			var geocoder = new google.maps.Geocoder();
			var addressField = direcc;
			geocoder.geocode(
				{'address': addressField}, 
				function(results, status) { 
					if (status == google.maps.GeocoderStatus.OK) {
						var loc = results[0].geometry.location;
						
						var mapOptions = {
							zoom: 9,
							center: loc,
							mapTypeId: 'roadmap'
						}
						map = new google.maps.Map(document.getElementById('map-trabajos'), mapOptions);
					}
				}
			);
		} else {
			//MAPA CON PUNTOS DE LOCALIZACION
			directionsDisplay = new google.maps.DirectionsRenderer({
			    //COLOR RUTA
			    polylineOptions: {
			      	strokeColor: "#D9534F",
			      	strokeOpacity: 0.7,
    				strokeWeight: 5
				},
				//ELIMINA LOS PUNTOS A B C D E...
				suppressMarkers: true
			});
		    
		    var mapOptions = {
		    	zoom: 15,
		    	center: new google.maps.LatLng(locations[0][1], locations[0][2]),
				
			}
			map = new google.maps.Map(document.getElementById('map-trabajos'));
		    
		    directionsDisplay.setMap(map);
		    
		    var bounds = new google.maps.LatLngBounds();
		    var infowindow = new google.maps.InfoWindow();

		    var marker, i;

		    
		    for (i = 0; i < locations.length; i++) { 
		      	marker = new google.maps.Marker({
		        	position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		        	map: map,
		        	icon: "<?php echo base_url() ?>img/pointer-view.png"
		      	});
		      
		      	//extend the bounds to include each marker's position
	  			bounds.extend(marker.position);

		      	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		        	return function() {
		          		infowindow.setContent(locations[i][3]);
		          		infowindow.open(map, marker);
		        	}
		      	})(marker, i));
		      
		      	//now fit the map to the newly inclusive bounds
				
				
				//(optional) restore the zoom level after the map is done scaling
				/*var listener = google.maps.event.addListener(map, "idle", function () {
				    map.setZoom(3);
				    google.maps.event.removeListener(listener);
				});*/
				
				
		    }

		    map.fitBounds(bounds);       
			map.panToBounds(bounds);     
		    
		    if (locations.length > 1) {
				//calcRoute();
		    	setTimeout(calcRoute(),500);
			} else {
				$('#map-trabajos #km').html('');
			}
		}
	}
	
	function calcRoute() {
		var start = locations[0][1]+','+locations[0][2];
		  
		var lati_end = locations[locations.length-1][1];
		var longi_end = locations[locations.length-1][2];
		var end = lati_end+','+longi_end;
		  
		var waypts = [];
		for (var i = 0; i < locations.length; i++) {
		    waypts.push({
		    	location:locations[i][1]+','+locations[i][2],
		        stopover:true
		    });
		}

		var request = {
			origin: start,
		    destination: end,
		    waypoints: waypts,
		    optimizeWaypoints: true,
		    travelMode: google.maps.TravelMode.DRIVING
		};
		
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
		    	directionsDisplay.setDirections(response);
		    	var route = response.routes[0];
			    /*var summaryPanel = document.getElementById('directions_panel');
			    summaryPanel.innerHTML = '';*/
			    // For each route, display summary information.
			    var km = 0;
			    for (var i = 0; i < route.legs.length; i++) {
			      /*var routeSegment = i + 1;
			      summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
			      summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
			      summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
			      summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';*/
			      km = km + route.legs[i].distance.value;
			    }
			    
			    $('#map-trabajos').prev().html("<label><?php echo lang('mapa.ruta') ?> <span id='km'>"+(km/1000)+" <?php echo lang('km.aprox') ?></span></label>");
		  	}
		});
	}
	
	//GENERAMOS EL MAPA
	setTimeout(mapa(''),1000);
	
	//GUARDAR TRABAJO
	$('#btn_guardar').click(function(e){
		e.preventDefault();

		var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
		l.start();
		
		//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
		$("#trabajos *").removeClass("has-error has-feedback");
		$("#trabajos .text-danger").html('');
		
		var horario = $('#jornada_trabajo').val();
		horario = horario.split(' - ');
		
		var inspecciones = new Array();
		$('#inspecciones option:selected').each(function(){
			inspecciones.push($(this).val());
		});
		
		var inspecciones_avanzadas = new Array();
		$('#inspecciones_avanzadas option:selected').each(function(){
			inspecciones_avanzadas.push($(this).val());
		});
		
		var tr_repetir = new Array();
		$('#tr_repetir option:selected').each(function(){
			tr_repetir.push($(this).val());
		});
		
		var tr_repetir_cantidad = 1;
		
		if ($('#tr_repetir option:selected').index() != -1) {
			tr_repetir_cantidad = $('#tr_repetir_cantidad').val();
		}
		
		var presupuestos = '';
		$('#presupuestos_asignados span').each(function(){
			presupuestos = presupuestos + $(this).attr('pr_id') + ',';
		});
		
		var datos = {
			'tr_id' : tr_id,
			'tr_fecha_inicio' : horario[0],
			'tr_fecha_fin' : horario[1],
			'su_id' : $('#sucursales option:selected').val(),
			'man_id' : locations,
			'in_id' : inspecciones,
			'inv_id' : inspecciones_avanzadas,
			'emp_id' : emple,
			'tr_comentario' : $('#tr_comentario').val(),
			'tr_repetir' : tr_repetir,
			'tr_repetir_cantidad' : tr_repetir_cantidad,
			'pr_id' : presupuestos
		};

		<?php if(isset($_GET['tag'])){ ?>
        	var nuevo_estado = true;
        <?php } else{?>
        	var nuevo_estado = false;
        <?php } ?>

        <?php if($tr_estado == 1){ ?>
        	datos.tr_estado = 1;
        <?php } else{?>
        	datos.tr_estado = 0;
        <?php } ?>
        

		var entrar = false;
		
		//COMPROBAMOS SI TIENE REPETICIONES
		if (tr_repetir != 0 && tr_repetir_cantidad == '' && nuevo_estado) {
			entrar = false;
			$('#tr_repetir_cantidad').next().html('<?php echo lang("requerido") ?>');
			$('#tr_repetir_cantidad').parent().addClass("has-error has-feedback");
		} else {
			entrar = true;
		}
		
		if ($('#jornada_trabajo').val() != '') {
			if (locations.length > 0) {
				if (inspecciones.length > 0 || inspecciones_avanzadas.length > 0) {
					if (emple.length > 0) {
						entrar = true;
					} else {
						entrar = false;
						$('#empleados').next().html('<?php echo lang("requerido") ?>');
						$('#empleados').parent().addClass("has-error has-feedback");
					}
				} else {
					entrar = false;
					$('#inspecciones').next().html('<?php echo lang("requerido") ?>');
					$('#inspecciones').parent().addClass("has-error has-feedback");
					
					$('#inspecciones_avanzadas').next().html('<?php echo lang("requerido") ?>');
					$('#inspecciones_avanzadas').parent().addClass("has-error has-feedback");
				}
			} else {
				entrar = false;
				$('#clientes').next().html('<?php echo lang("trabajos.mantenimientos") ?>');
				$('#clientes').parent().addClass("has-error has-feedback");
			}
			
		} else {
			$('#jornada_trabajo').next().html('<?php echo lang("requerido") ?>');
			$('#jornada_trabajo').parent().addClass("has-error has-feedback");
		}		
		
		if (entrar) {
			$.ajax({
				type: "POST",
				dataType: "json",
			  	url: "<?php echo base_url().$this->lang->lang() ?>/trabajos/guardar_editar",
			  	data: datos,
			  	success: function(data) {
			  		if (data.sql) {
			  			llenar_bitacora();
			  			//MENSAJES ALERTA PARA EDITAR O CREAR
			  			if (datos['tr_id'] != '') {
							if (!data.editar) {
								bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								setTimeout("location.href='"+location+"'", 5000);
							}
						} else {
							if (!data.crear) {
								bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								setTimeout("location.href='"+location+"'", 5000);
							}
						}
							
			  			//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
						if (data.status) {
							if (tr_id == '') {			    		
						    	//MOSTRAMOS ALERTA
								bootbox.dialog({
									message: "<?php echo lang('trabajos.crear.ok') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('nuevo') ?>",
											className: "btn-success",
											callback: function(result) {												
												var url = '<?php echo base_url().$this->lang->lang() ?>/trabajos/nuevo_trabajo';
												$(location).attr('href',url);
											}
										},
										main: {
											label: "<?php echo lang('trabajos') ?>",
											className: "btn-primary",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/trabajos/';
												$(location).attr('href',url);
											}
										}
									}
								});
							} else {
								bootbox.alert('<?php echo lang("trabajos.guardar.ok") ?>');
								setTimeout("location.href='"+location+"'", 1000);
							}
						} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							if (data.errors.length > 0) {
								$.each(data.errors, function (ind, elem) {
									$.each(data.errors[ind], function (ind2, elem2) {
										$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									
										$('select[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('select[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
												
										$('textarea[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('textarea[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									}); 
								});
							}
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
				},
			  	error: function(XMLHttpRequest, textStatus, errorThrown) {
			   		bootbox.alert('<?php echo lang("error.ajax") ?>');
			   		l.stop();
			  	}
			});
		} else {
			l.stop();
		}
	});
	
	//PROCESAR TRABAJO
	$('#btn_procesar').click(function(e){
		e.preventDefault();

		var l = Ladda.create( document.querySelector( "#btn_procesar" ) );
		l.start();
		
		//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
		$("#trabajos *").removeClass("has-error has-feedback");
		$("#trabajos .text-danger").html('');
		
		var horario = $('#jornada_trabajo').val();
		horario = horario.split(' - ');
		
		var inspecciones = new Array();
		$('#inspecciones option:selected').each(function(){
			inspecciones.push($(this).val());
		});
		
		var inspecciones_avanzadas = new Array();
		$('#inspecciones_avanzadas option:selected').each(function(){
			inspecciones_avanzadas.push($(this).val());
		});
		
		var tr_repetir = new Array();
		$('#tr_repetir option:selected').each(function(){
			tr_repetir.push($(this).val());
		});
		
		var tr_repetir_cantidad = 1;
		
		if ($('#tr_repetir option:selected').index() != -1) {
			tr_repetir_cantidad = $('#tr_repetir_cantidad').val();
		}
		
		var presupuestos = '';
		$('#presupuestos_asignados span').each(function(){
			presupuestos = presupuestos + $(this).attr('pr_id') + ',';
		});
		
		var datos = {
			'tr_id' : tr_id,
			'tr_fecha_inicio' : horario[0],
			'tr_fecha_fin' : horario[1],
			'su_id' : $('#sucursales option:selected').val(),
			'man_id' : locations,
			'in_id' : inspecciones,
			'inv_id' : inspecciones_avanzadas,
			'emp_id' : emple,
			'tr_comentario' : $('#tr_comentario').val(),
			'tr_repetir' : tr_repetir,
			'tr_repetir_cantidad' : tr_repetir_cantidad,
			'pr_id' : presupuestos
		};

		<?php if(isset($_GET['tag'])){ ?>
        	var nuevo_estado = true;
        <?php } else{?>
        	var nuevo_estado = false;
        <?php } ?>
        
		if(nuevo_estado){
			if($(this).data("estado") == '1'){
				datos.tr_estado = 1;
			}else{
				datos.tr_estado = 0;
			}
		}

		datos.tr_estado = 1;

		var entrar = false;
		
		//COMPROBAMOS SI TIENE REPETICIONES
		if (tr_repetir != 0 && tr_repetir_cantidad == '' && nuevo_estado) {
			entrar = false;
			$('#tr_repetir_cantidad').next().html('<?php echo lang("requerido") ?>');
			$('#tr_repetir_cantidad').parent().addClass("has-error has-feedback");
		} else {
			entrar = true;
		}
		
		if ($('#jornada_trabajo').val() != '') {
			if (locations.length > 0) {
				if (inspecciones.length > 0 || inspecciones_avanzadas.length > 0) {
					if (emple.length > 0) {
						entrar = true;
					} else {
						entrar = false;
						$('#empleados').next().html('<?php echo lang("requerido") ?>');
						$('#empleados').parent().addClass("has-error has-feedback");
					}
				} else {
					entrar = false;
					$('#inspecciones').next().html('<?php echo lang("requerido") ?>');
					$('#inspecciones').parent().addClass("has-error has-feedback");
					
					$('#inspecciones_avanzadas').next().html('<?php echo lang("requerido") ?>');
					$('#inspecciones_avanzadas').parent().addClass("has-error has-feedback");
				}
			} else {
				entrar = false;
				$('#clientes').next().html('<?php echo lang("trabajos.mantenimientos") ?>');
				$('#clientes').parent().addClass("has-error has-feedback");
			}
			
		} else {
			$('#jornada_trabajo').next().html('<?php echo lang("requerido") ?>');
			$('#jornada_trabajo').parent().addClass("has-error has-feedback");
		}		
		
		if (entrar) {
			$.ajax({
				type: "POST",
				dataType: "json",
			  	url: "<?php echo base_url().$this->lang->lang() ?>/trabajos/guardar_editar",
			  	data: datos,
			  	success: function(data) {
			  		if (data.sql) {
			  			llenar_bitacora();
			  			//MENSAJES ALERTA PARA EDITAR O CREAR
			  			if (datos['tr_id'] != '') {
							if (!data.editar) {
								bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								setTimeout("location.href='"+location+"'", 5000);
							}
						} else {
							if (!data.crear) {
								bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								setTimeout("location.href='"+location+"'", 5000);
							}
						}
							
			  			//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
						if (data.status) {
							if (tr_id == '') {			    		
						    	//MOSTRAMOS ALERTA
								bootbox.dialog({
									message: "<?php echo lang('trabajos.crear.ok') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('nuevo') ?>",
											className: "btn-success",
											callback: function(result) {												
												var url = '<?php echo base_url().$this->lang->lang() ?>/trabajos/nuevo_trabajo';
												$(location).attr('href',url);
											}
										},
										main: {
											label: "<?php echo lang('trabajos') ?>",
											className: "btn-primary",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/trabajos/';
												$(location).attr('href',url);
											}
										}
									}
								});
							} else {
								bootbox.alert('<?php echo lang("trabajos.guardar.ok") ?>');
								location.reload(true);
								setTimeout("location.href='"+location+"'", 1000);
							}
						} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							if (data.errors.length > 0) {
								$.each(data.errors, function (ind, elem) {
									$.each(data.errors[ind], function (ind2, elem2) {
										$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									
										$('select[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('select[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
												
										$('textarea[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('textarea[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									}); 
								});
							}
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
				},
			  	error: function(XMLHttpRequest, textStatus, errorThrown) {
			   		bootbox.alert('<?php echo lang("error.ajax") ?>');
			   		l.stop();
			  	}
			});
		} else {
			l.stop();
		}
	});
	
	
	
	//SUPERVISORES
	if (tr_id != '' && emp_tipo <= 2) {
		//RECUPERAMOS LAS FOTOS DE LA INSPECCION
		/*function imagenes(in_id) {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url().$this->lang->lang() ?>/trabajos/imagenes",
				data: 'tr_id=<?php echo $tr_id ?>&in_id='+in_id+'&man_id='+$('#inspecc_mant option:selected').val(),
				success: function(data) {
					$('#in'+in_id+' .fot div:last').html(data);
				}
			});
		}
		
		//RECUPERAMOS LAS FOTOS DE LA INSPECCION
		function imagenes_avanzadas(inv_id) {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url().$this->lang->lang() ?>/trabajos/imagenes_avanzadas",
				data: 'tr_id=<?php echo $tr_id ?>&inv_id='+inv_id+'&man_id='+$('#inspecc_mant option:selected').val(),
				success: function(data) {
					$('#inv'+inv_id+' .fot div:last').html(data);
				}
			});
		}*/
		
		//CREAMOS LAS INSPECCIONES BASICAS Y AVANZADAS DE CADA MANTENIMIENTO
		function inspecciones_trabajo() {
			//INSPECCIONES DEL TRABAJO
			var in_id = '<?php echo isset($in_id)?$in_id:"" ?>';
			var inv_id = '<?php echo isset($inv_id)?$inv_id:"" ?>';
			var disable = '<?php echo $disable; ?>';
			
			//INSPECCIONES BASICAS
			if (in_id != '') {
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "<?php echo base_url().$this->lang->lang() ?>/inspecciones/inspecciones_trabajo",
					data: 'tr_id=<?php echo $tr_id ?>&in_id='+in_id+'&man_id='+$('#inspecc_mant option:selected').val(),
					success: function(data) {
						var insp = '';
						//alert(JSON.stringify(data));					
						$.each(data,function(indice) {
							//PREGUNTAMOS SI ESTA INSPECCION YA FUE CERRADA
							var read = '';
							if (data[indice]['ti_estado'] == 2) {
								read = 'disabled="disabled"';
							}
							//PREGUNTAMOS SI TIENE ACCESO A EDITAR LA INSPECCION
							if (disable != "") {
								read = 'disabled="disabled"';
							}
							
							insp = insp + '<div class="col-lg-12" id="in'+data[indice]['in_id']+'"><div class="box box-solid box-primary"><div class="box-header"><h3 class="box-title">' + data[indice]['in_nombre'] + '</h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm encoger_inspeccion"><i class="fa fa-minus"></i></button></div></div>';
										
							//TITULOS
							var in_titulo = data[indice]['in_titulo'];
							in_titulo = in_titulo.split(';');
							if (in_titulo[in_titulo.length-1] == '') {
								in_titulo.splice(in_titulo.length-1,1);
							}
							
							//DESCRIPCIONES
							var in_descripcion = data[indice]['in_descripcion'];
							in_descripcion = in_descripcion.split(';');
							if (in_descripcion[in_descripcion.length-1] == '') {
								in_descripcion.splice(in_descripcion.length-1,1);
							}
							
							//ANCHOS
							var in_ancho = data[indice]['in_ancho'];
							in_ancho = in_ancho.split(';');
							if (in_ancho[in_ancho.length-1] == '') {
								in_ancho.splice(in_ancho.length-1,1);
							}
							
							//ALTOS
							var in_alto = data[indice]['in_alto'];
							in_alto = in_alto.split(';');
							if (in_alto[in_alto.length-1] == '') {
								in_alto.splice(in_alto.length-1,1);
							}
							
							//CANTIDADES
							var in_cantidad = data[indice]['in_cantidad'];
							in_cantidad = in_cantidad.split(';');
							if (in_cantidad[in_cantidad.length-1] == '') {
								in_cantidad.splice(in_cantidad.length-1,1);
							}
							
							//MONTOS
							var in_monto = data[indice]['in_monto'];
							in_monto = in_monto.split(';');
							if (in_monto[in_monto.length-1] == '') {
								in_monto.splice(in_monto.length-1,1);
							}
										
							//PREGUNTAS	
							var in_preguntas = data[indice]['in_preguntas'];
							in_preguntas = in_preguntas.split(';');
							if (in_preguntas[in_preguntas.length-1] == '') {
								in_preguntas.splice(in_preguntas.length-1,1);
							}
										
							//RESPUESTAS MARCADAS
							var in_resp_marcas = new Array();
							var array_resp_marcas = new Array();
							if (data[indice]['in_resp_marcas'] != null) {
								in_resp_marcas = data[indice]['in_resp_marcas'];
								in_resp_marcas = in_resp_marcas.split('/');
								if (in_resp_marcas[in_resp_marcas.length-1] == '') {
									in_resp_marcas.splice(in_resp_marcas.length-1,1);
								}
										
								for(var t=0; t<in_resp_marcas.length; t++) {
									
									var aux = in_resp_marcas[t].split(';');
									if (in_resp_marcas[t][in_resp_marcas[t].length-1] == '') {
										in_resp_marcas[t].splice(in_resp_marcas[t].length-1,1);
									}
									array_resp_marcas.push(aux);
								}
							}
										
							insp = insp + '<div class="row">';
							for (var i=0; i < in_titulo.length; i++) {
								insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12"><div class="box-body">';
								//ABRRIR GRUPO
								insp = insp + '<table><tbody><tr>';
								insp = insp + '<td class="col-sm-2"><label style="width:100%"><?php echo lang("tipo") ?></label><input class="form-control tipo" name="in_titulo" value="' + in_titulo[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-3"><label style="width:100%"><?php echo lang("descripcion") ?></label><input class="form-control descripcion" name="in_descripcion" value="' + in_descripcion[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-1"><label style="width:50px !important"><?php echo lang("ancho") ?></label><input class="form-control ancho" name="in_ancho" value="' + in_ancho[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-1"><label style="width:50px !important"><?php echo lang("alto") ?></label><input class="form-control alto" name="in_alto" value="' + in_alto[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-1"><label style="width:50px !important"><?php echo lang("cantidad") ?></label><input class="form-control cantidad" name="in_cantidad" value="' + in_cantidad[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-1"><label style="width:50px !important"><?php echo lang("monto") ?></label><input class="form-control monto" name="in_monto" value="' + in_monto[i] + '" type="text" readonly></td>';
											
								var array_preguntas = in_preguntas[i].split(',');
								
								for (var x=0; x<array_preguntas.length; x++) {
									
									//AGREGAR PREGUNTAS
									if (array_resp_marcas.length > 0) { //Si existen respuestas asociadas a la inspeccion
										insp = insp + '<td class="pregunt col-sm-2" style="width:50px !important"><label style="width:50px !important">' + array_preguntas[x] + '</label><input class="form-control respuesta" name="resp' + indice + i + '_' + x + '" value="' + array_resp_marcas[i][x] + '" type="text" onKeypress="return soloNumeros(event)" '+read+'></td>';
									} else {
										insp = insp + '<td class="pregunt col-sm-2" style="width:50px !important"><label style="width:50px !important">' + array_preguntas[x] + '</label><input class="form-control respuesta" name="resp' + indice + i + '_' + x + '" value="" type="text" onKeypress="return soloNumeros(event)"></td>';
									}
								}
								insp = insp + '</tr></table></div></div>';
								
							}
										
							//insp = insp + '</div>'; /* FIN ROW */
							insp = insp + '<div class="row">';
									
							insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12 textar" style="padding: 0 50px 0 50px">';
							if (data[indice]['ti_comentario'] != null) {
								insp = insp + '<div class="form-group"><h4><strong><?php echo lang("comentario") ?></strong></h4><textarea '+read+'>'+ data[indice]['ti_comentario'] +'</textarea><span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger"><?php echo form_error("ti_comentario") ?></div></div>';
							} else {
								insp = insp + '<div class="form-group"><h4><strong><?php echo lang("comentario") ?></strong></h4><textarea '+read+'></textarea><span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger"><?php echo form_error("ti_comentario") ?></div></div>';
							}
							insp = insp + '</div>';
							insp = insp + '</div>'; /* FIN ROW */
							insp = insp + '<div class="">';
			
							var imagenes = (data[indice]["imagenes"]) ? '<div>'+data[indice]["imagenes"]+'</div>' : '';			
							insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12 fot"><h4><strong><?php echo lang("fotos") ?></strong></h4><div><form class="fotos" action="<?php echo base_url().$this->lang->lang() ?>/trabajos/upload" method="post" enctype="multipart/form-data"><div class="input-group"><span class="input-group-btn"><span class="btn btn-primary btn-file"><i class="fa fa-camera"></i><input type="file" name="archivos[]" multiple="multiple" '+read+'></span></span><input type="text" class="form-control" readonly></div></form></div><div class="text-danger"><?php echo form_error("fotos") ?></div>'+imagenes+'</div>';
										
							insp = insp + '</div>'; /* FIN ROW */
										
							//BOTONES GUARDAR Y CERRAR
							var botones = true;
							if (data[indice]['ti_estado'] != null ) {
								if (data[indice]['ti_estado'] == 2) {
									botones = false;
								}
							}
							if (disable != "") {
								botones = false;
							}
										
							if (botones) {
								insp = insp + '<div class="botons"><div class="col-lg-12 col-md-12 col-sm-12 text-right"><button id="close'+data[indice]['in_id']+'" type="button" class="btn btn-warning ladda-button completar_inspeccion" data-style="zoom-out" id="close'+data[indice]['in_id']+'"><i class="fa fa-eye"></i> <?php echo lang("completar.inspeccion") ?></button> <button class="btn btn-success ladda-button guardar_inspeccion" data-style="zoom-out" id="save'+data[indice]['in_id']+'"><i class="fa fa-save"></i> <?php echo lang("guardar") ?></button></div></div>';
							}
										
							insp = insp + '</div></div>';
							insp = insp + '</div>'; /* FIN ROW */
							
						});
						
						$('#ver_trabajo #inspec').append(insp);
						
						//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
						$(".box .form-group").removeClass("has-error has-feedback");
						$('.box .glyphicon-remove').hide();
						$(".box .text-danger").html('');
						
						//IMAGENES
						$(".fancybox").fancybox({
							helpers : {
					    		title : false
					    	},
					    	padding: 0,
							openEffect : 'elastic',
							openSpeed  : 150,
							closeEffect : 'elastic',
							closeSpeed  : 150,
							closeClick : false
						});
						
						//ESTILO Y BOTON FOTOS
						$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
							var input = $(this).parents('.input-group').find(':text'),
								log = numFiles > 1 ? numFiles + ' files selected' : label;
								if( input.length ) {
									input.val(log);
								} else {
									if( log ) alert(log);
								}	
						});
						
						//GUARDAR INSPECCION
						$('.guardar_inspeccion,.completar_inspeccion').on('click', function(){
							//LOADING BOTON
							var btn_id = $(this).attr('id');
							var l = Ladda.create( document.querySelector( '#'+btn_id ) );
						 	l.start();
							
							//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
							$(".box .form-group").removeClass("has-error has-feedback");
							$('.box .glyphicon-remove').hide();
							$(".box .text-danger").html('');
							
							//INSPECCION ELEGIDA
							var in_id = $(this).parent().parent().parent().parent().parent().attr('id');
							in_id = in_id.substr(2,in_id.length);
							console.log(in_id);
							//nombre
							var in_nombre = $(this).parent().parent().parent().parent().find('h3').html();
							
							//VARIABLES
							var v_in_titulo = '';
							var v_in_descripcion = '';
							var v_in_ancho = '';
							var v_in_alto = '';
							var v_in_cantidad = '';
							var v_in_monto = '';
							var v_in_preguntas = '';
							var v_in_respuestas = '';
							var final_resp_marcas = '';
							var cont_preg = 0;
							var cont_resp_marca = 0;
							var enviar = true;
							
							//MARCAS DE LA INSPECCION
							$(this).parent().parent().parent().find('.box-body').each(function(){
								//GUARDAMOS LOS TITULOS
								v_in_titulo = v_in_titulo + $(this).find('input.tipo').val() + ';';
								v_in_descripcion = v_in_descripcion + $(this).find('input.descripcion').val() + ';';
								v_in_ancho = v_in_ancho + $(this).find('input.ancho').val() + ';';
								v_in_alto = v_in_alto + $(this).find('input.alto').val() + ';';
								v_in_cantidad = v_in_cantidad + $(this).find('input.cantidad').val() + ';';
								v_in_monto = v_in_monto + $(this).find('input.monto').val() + ';';
								
								//GUARDAMOS LAS PREGUNTAS
								$(this).find('.pregunt').each(function(){
									cont_preg++;
									var pregunt = $(this).find('label').html();
									//pregunt = pregunt.substring(0, pregunt.length); //?
									//pregunt = pregunt.substring(1, pregunt.length);//? 
									v_in_preguntas = v_in_preguntas + pregunt + ',';
									
									//GUARDAMOS LAS RESPUESTAS
									$(this).find('input.respuesta').each(function(){
										v_in_respuestas = v_in_respuestas + $(this).val() + ',';
									});
									
									v_in_respuestas = v_in_respuestas.substring(0, v_in_respuestas.length-1);
									v_in_respuestas = v_in_respuestas + ';';
								});
								
								v_in_preguntas = v_in_preguntas.substring(0, v_in_preguntas.length-1);
								v_in_preguntas = v_in_preguntas + ';';
								
								v_in_respuestas = v_in_respuestas.substring(0, v_in_respuestas.length-1);
								v_in_respuestas = v_in_respuestas + '/';
								
								//GUARDAMOS LAS RESPUESTAS MARCADAS
								$(this).find('input.respuesta').each(function(){
									final_resp_marcas = final_resp_marcas + $(this).val() + ';';
									cont_resp_marca++;
								});
								
								if (final_resp_marcas != '') {
									final_resp_marcas = final_resp_marcas.substring(0, final_resp_marcas.length-1);
									final_resp_marcas = final_resp_marcas + '/';
								}
							});
							
							//ESTADO DE LA INSPECCION
							var ti_estado = 1;
							if ($(this).hasClass('completar_inspeccion')) {
								ti_estado = 2;
								
								if (cont_resp_marca != cont_preg) {
									enviar = false;
									bootbox.alert('<?php echo lang("trabajo.inspeccion.cerrado.ko") ?>');
								}
							}
							
							if (enviar) {
								var datos = {
									'in_id'	:	in_id,
									'tr_id'	:	tr_id,
									'man_id'	:	$('#inspecc_mant option:selected').val(),
									'ti_comentario'	:	$(this).parent().parent().prev().prev().find('textarea').val(),
									'in_titulo'	:	v_in_titulo,
									'in_descripcion'	:	v_in_descripcion,
									'in_ancho'	:	v_in_ancho,
									'in_alto'	:	v_in_alto,
									'in_cantidad'	:	v_in_cantidad,
									'in_monto'	:	v_in_monto,
									'in_preguntas'	:	v_in_preguntas,
									'in_respuestas'	:	v_in_respuestas,
									'in_resp_marcas'	:	final_resp_marcas,
									'in_nombre'	:	in_nombre,
									'ti_estado'	:	ti_estado
								};
								
								$(this).parent().parent().parent().find(".fotos").ajaxForm({
									dataType: 'json',
									data: datos,
									success: function(data) {
										if (data.sql) {
											if (data.status) {
												//TODO CORRECTO
												//MENSAJE GUARDAR INSPECCION
												if (ti_estado < 2) {
													bootbox.alert('<?php echo lang("trabajo.inspeccion.ok") ?>');
												} else {
													//MENSAJE CERRAR INSPECCION
													bootbox.alert('<?php echo lang("trabajo.inspeccion.cerrado.ok") ?>');
													$('#'+in_id+' .botons, #'+in_id+' .fotos').hide();
													$('#'+in_id+' input, #'+in_id+' textarea').attr('disabled','disabled');
													setTimeout("location.href='"+location+"'", 1000);
												}
												
												//RECARGA LAS FOTOS SI SE SUBIERON FOTOS NUEVAS
												if ($('#'+in_id).find('.fot input:eq(1)').val() != '') {
													//imagenes(in_id);
													$('#'+in_id).find('.fot input:eq(1)').val('')
												}
											} else {
												//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
												//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
												$.each(data.errors, function (ind, elem) {
													$.each(data.errors[ind], function (ind2, elem2) {
														//SI EXSITE ERROR EN EL CAMPO FOTO ENTRAMOS POR AQUI
														if (data.errors[ind][0] == 'fotos') {
															$('#'+in_id+' .fot').find('.text-danger').html(data.errors[ind][ind2]);
														} else {
															//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
															$('#'+in_id+' .textar').find('.text-danger').html(data.errors[ind][ind2]);
															$('#'+in_id+' .textar').find('.form-group').addClass("has-error has-feedback");
															$('#'+in_id+' .textar').find('.glyphicon-remove').show();
														}
													}); 
												});
											}
										} else {
											bootbox.alert('<?php echo lang("error.ajax") ?>');
										}
										l.stop();
									},
									error: function(XMLHttpRequest, textStatus, errorThrown) {
										bootbox.alert('<?php echo lang("error.ajax") ?>');
										l.stop();
									}
								}).submit();
							}
							l.stop();
						});
						
						
					},
					complete: function() {
						$('#cargar_inspeccion').hide();
					}
				});
			}
			
			//INSPECCIONES AVANZADAS
			if (inv_id !== '') {
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "<?php echo base_url().$this->lang->lang() ?>/inspecciones/inspecciones_avanzadas_trabajo",
					data: 'tr_id=<?php echo $tr_id ?>&inv_id='+inv_id+'&man_id='+$('#inspecc_mant option:selected').val(),
					success: function(data) {
						var insp = '';
						
						$.each(data,function(indice) {
							//PREGUNTAMOS SI ESTA INSPECCION YA FUE CERRADA
							var read = '';
							if (data[indice]['tia_estado'] == 2) {
								read = 'disabled="disabled"';
							}
							//PREGUNTAMOS SI TIENE ACCESO A EDITAR LA INSPECCION
							if (disable != "") {
								read = 'disabled="disabled"';
							}
							
							insp = insp + '<div class="col-lg-12" id="inv'+data[indice]['inv_id']+'"><div class="box box-solid box-primary"><div class="box-header"><h3 class="box-title">' + data[indice]['inv_nombre'] + '</h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm encoger_inspeccion"><i class="fa fa-minus"></i></button></div></div>';
							
							//EDIFICIOS			
							var inv_edificio = data[indice]['inv_edificio'];
							inv_edificio = inv_edificio.split(';');
							inv_edificio = inv_edificio.filter(Boolean);
							
							//PLANTAS
							var inv_planta = data[indice]['inv_planta'];;
							inv_planta = inv_planta.split('|');
							inv_planta = inv_planta.filter(Boolean);
							
							//PRODUCTOS
							var inv_producto = data[indice]['inv_producto'];
							inv_producto = inv_producto.split('^');
							inv_producto = inv_producto.filter(Boolean);
							
							//DESCRIPCIONES
							var inv_descripcion = data[indice]['inv_descripcion'];
							inv_descripcion = inv_descripcion.split('^');
							inv_descripcion = inv_descripcion.filter(Boolean);
							
							//ANCHO
							var inv_ancho = data[indice]['inv_ancho'];
							inv_ancho = inv_ancho.split('^');
							inv_ancho = inv_ancho.filter(Boolean);
							
							//ALTO
							var inv_alto = data[indice]['inv_alto'];
							inv_alto = inv_alto.split('^');
							inv_alto = inv_alto.filter(Boolean);
							
							//CANTIDAD
							var inv_cantidad = data[indice]['inv_cantidad'];
							inv_cantidad = inv_cantidad.split('^');
							inv_cantidad = inv_cantidad.filter(Boolean);
							
							//MONTO
							var inv_monto = data[indice]['inv_monto'];
							inv_monto = inv_monto.split('^');
							inv_monto = inv_monto.filter(Boolean);
							
							//RESPUESTAS
							var inv_respuesta = data[indice]['inv_respuesta'];
							inv_respuesta = inv_respuesta.split('^');
							inv_respuesta = inv_respuesta.filter(Boolean);
							
							//RESPUESTAS MARCADAS
							var inv_resp_marcas = data[indice]['inv_resp_marca'];
							if (inv_resp_marcas != null) {
								inv_resp_marcas = inv_resp_marcas.split('^');
								inv_resp_marcas = inv_resp_marcas.filter(Boolean);
							}
									
							insp = insp + '<div>';
							for (var e=0; e<inv_edificio.length; e++) {
								//ABRIR EDIFICIO
								insp = insp + '<div class="col-lg-12 box box-solid box-primary edificios" id="'+e+'"><div class="box-header"><h3 class="box-title" style="width: 50%;">'+inv_edificio[e]+'</h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm encoger_edificio"><i class="fa fa-minus"></i></button></div></div><div class="box-body"><div class="body_edificio">';
								
								var aux_planta = inv_planta[e].split(';');
								aux_planta = aux_planta.filter(Boolean);
								
								var aux_producto = inv_producto[e].split('|');
								aux_producto = aux_producto.filter(Boolean);
								
								var aux_descripcion = inv_descripcion[e].split('|');
								aux_descripcion = aux_descripcion.filter(Boolean);
								
								var aux_ancho = inv_ancho[e].split('|');
								aux_ancho = aux_ancho.filter(Boolean);
								
								var aux_alto = inv_alto[e].split('|');
								aux_alto = aux_alto.filter(Boolean);
								
								var aux_cantidad = inv_cantidad[e].split('|');
								aux_cantidad = aux_cantidad.filter(Boolean);
								
								var aux_monto = inv_monto[e].split('|');
								aux_monto = aux_monto.filter(Boolean);
													
								var aux_respuesta = inv_respuesta[e].split('|');
								aux_respuesta = aux_respuesta.filter(Boolean);
								
								var aux_resp_marcas;
								if (inv_resp_marcas != null) {
								aux_resp_marcas = inv_resp_marcas[e].split('|');
								aux_resp_marcas = aux_resp_marcas.filter(Boolean);	
								}
								
								for (var p=0; p<aux_planta.length; p++) {
									//ABRIMOS LAS PLANTAS
									insp = insp + '<div class="box box-solid box-info plantas plant'+p+'"><div class="box-header"><h3 class="box-title">'+aux_planta[p]+'</h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm encoger_planta"><i class="fa fa-minus"></i></button></div></div><div class="box-body body_planta">';
									
									var aux2_producto = aux_producto[p].split(';');
									aux2_producto = aux2_producto.filter(Boolean);
									
									var aux2_descripcion = aux_descripcion[p].split(';');
									aux2_descripcion = aux2_descripcion.filter(Boolean);
									
									var aux2_ancho = aux_ancho[p].split(';');
									aux2_ancho = aux2_ancho.filter(Boolean);
									
									var aux2_alto = aux_alto[p].split(';');
									aux2_alto = aux2_alto.filter(Boolean);
									
									var aux2_cantidad = aux_cantidad[p].split(';');
									aux2_cantidad = aux2_cantidad.filter(Boolean);
									
									var aux2_monto = aux_monto[p].split(';');
									aux2_monto = aux2_monto.filter(Boolean);
									
									var aux2_respuesta = aux_respuesta[p].split('}');
									aux2_respuesta = aux2_respuesta.filter(Boolean);
									
									var aux2_resp_marcas;
									if (inv_resp_marcas != null) {
										aux2_resp_marcas = aux_resp_marcas[p].split('}');
										aux2_resp_marcas = aux2_resp_marcas.filter(Boolean);
									}
									
									for (var a2p=0; a2p<aux2_producto.length; a2p++) {
										//ABRIMOS EL PRODUCTO
										
										insp = insp + '<div class="box productos product'+a2p+'"><div class="box-header">';
										
										insp = insp + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("tipo") ?></label><input name="producto" class="form-control" type="text" value="'+aux2_producto[a2p]+'" readonly></div></div>';
										insp = insp + '<div class="col-lg-2"><div class="form-group"><label><?php echo lang("descripcion") ?></label><input name="descripcion" class="form-control" type="text" value="'+aux2_descripcion[a2p]+'" readonly></div></div>';
										insp = insp + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("ancho") ?></label><input name="ancho" class="form-control" type="text" value="'+aux2_ancho[a2p]+'" readonly></div></div>';
										insp = insp + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("alto") ?></label><input name="alto" class="form-control" type="text" value="'+aux2_alto[a2p]+'" readonly></div></div>';
										insp = insp + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("cantidad") ?></label><input name="cantidad" class="form-control" type="text" value="'+aux2_cantidad[a2p]+'" readonly></div></div>';
										insp = insp + '<div class="col-lg-1"><div class="form-group"><label><?php echo lang("monto") ?></label><input name="monto" class="form-control" type="text" value="'+aux2_monto[a2p]+'" readonly></div></div>';
					
										var aux3_respuesta = aux2_respuesta[a2p].split(';');
										aux3_respuesta = aux3_respuesta.filter(Boolean);
										
										var aux3_resp_marcas;
										if (inv_resp_marcas != null) {
											aux3_resp_marcas = aux2_resp_marcas[a2p].split(';');
											aux3_resp_marcas = aux3_resp_marcas.filter(Boolean);
										}
										
										for (var r=0; r<aux3_respuesta.length; r++) {
											//MOSTRAMOS LAS RESPUESTAS
											if (inv_resp_marcas != null) {
												insp = insp + '<div class="col-lg-1"><div class="input-group respuesta"><label>'+aux3_respuesta[r]+'</label><input class="form-control" type="text" onKeypress="return soloNumeros(event)" value="'+aux3_resp_marcas[r]+'" '+read+' ></div></div>';
											} else {
												insp = insp + '<div class="col-lg-1"><div class="input-group respuesta"><label>'+aux3_respuesta[r]+'</label><input class="form-control" type="text" onKeypress="return soloNumeros(event)" value=""></div></div>';
											}
										}
										
										//insp = insp + '<div class="box-tools pull-right"><button class="btn btn-default btn-sm encoger_producto"><i class="fa fa-minus"></i></button></div>';
										
										//CERAMOS EL PRODUCTO
										insp = insp + '</div></div>';
									}
									
									//CERRAMOS LA PLANTA
									insp = insp + '<div class="row"></div></div></div>';
								}
								
								//CERRAR EDIFICIO
								insp = insp + '</div></div></div>'
							}
										
							//insp = insp + '</div>'; /* FIN ROW */
							insp = insp + '<div class="row">';
									
							insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12 textar" style="padding: 0 30px 0 30px">';
							if (data[indice]['tia_comentario'] != null) {
								insp = insp + '<div class="form-group"><h4><strong><?php echo lang("comentario") ?></strong></h4><textarea '+read+'>'+ data[indice]['tia_comentario'] +'</textarea><span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger"><?php echo form_error("ti_comentario") ?></div></div>';
							} else {
								insp = insp + '<div class="form-group"><h4><strong><?php echo lang("comentario") ?></strong></h4><textarea '+read+'></textarea><span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger"><?php echo form_error("ti_comentario") ?></div></div>';
							}
							insp = insp + '</div>';
							insp = insp + '</div>'; /* FIN ROW */
							insp = insp + '<div class="">';
										
							insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12 fot"><h4><strong><?php echo lang("fotos") ?></strong></h4><div><form class="fotos" action="<?php echo base_url().$this->lang->lang() ?>/trabajos/upload" method="post" enctype="multipart/form-data"><div class="input-group"><span class="input-group-btn"><span class="btn btn-primary btn-file"><i class="fa fa-camera"></i><input type="file" name="archivos[]" multiple="multiple" '+read+'></span></span><input type="text" class="form-control" readonly></div></form></div><div class="text-danger"><?php echo form_error("fotos") ?></div><div>'+data[indice]["imagenes"]+'</div></div>';
										
							insp = insp + '</div>'; /* FIN ROW */
										
							//BOTONES GUARDAR Y CERRAR
							var botones = true;
							if (data[indice]['tia_estado'] != null ) {
								if (data[indice]['tia_estado'] == 2) {
									botones = false;
								}
							}
							if (disable != "") {
								botones = false;
							}
										
							if (botones) {
								insp = insp + '<div class="botons"><div class="col-lg-12 col-md-12 col-sm-12 text-right"><button id="close'+data[indice]['inv_id']+'" type="button" class="btn btn-warning ladda-button completar_inspeccion_avanzada" data-style="zoom-out" id="close'+data[indice]['inv_id']+'"><i class="fa fa-eye"></i> <?php echo lang("completar.inspeccion") ?></button> <button class="btn btn-success ladda-button guardar_inspeccion_avanzada" data-style="zoom-out" id="save'+data[indice]['inv_id']+'"><i class="fa fa-save"></i> <?php echo lang("guardar") ?></button></div></div>';
							}
										
							insp = insp + '</div></div>';
							insp = insp + '</div>'; /* FIN ROW */
						});
						
						$('#ver_trabajo #inspec').append(insp);
						
						//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
						$(".box .form-group").removeClass("has-error has-feedback");
						$('.box .glyphicon-remove').hide();
						$(".box .text-danger").html('');
						
						//IMAGENES
						$(".fancybox").fancybox({
							helpers : {
					    		title : false
					    	},
					    	padding: 0,
							openEffect : 'elastic',
							openSpeed  : 150,
							closeEffect : 'elastic',
							closeSpeed  : 150,
							closeClick : false
						});
						
						//ESTILO Y BOTON FOTOS
						$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
							var input = $(this).parents('.input-group').find(':text'),
								log = numFiles > 1 ? numFiles + ' files selected' : label;
								if( input.length ) {
									input.val(log);
								} else {
									if( log ) alert(log);
								}	
						});
						
						//GUARDAR INSPECCION
						$('.guardar_inspeccion_avanzada,.completar_inspeccion_avanzada').on('click', function(){
							//LOADING BOTON
							var btn_id = $(this).attr('id');
							var l = Ladda.create( document.querySelector( '#'+btn_id ) );
						 	l.start();
							
							//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
							$(".box .form-group").removeClass("has-error has-feedback");
							$('.box .glyphicon-remove').hide();
							$(".box .text-danger").html('');
							
							var obj = $(this).parent().parent().parent().parent().parent();
							
							//INSPECCION ELEGIDA
							var inv_id = obj.attr('id');
							inv_id = inv_id.substr(3,inv_id.length);
							console.log(inv_id);
							//NOMBRE
							var inv_nombre = obj.find('h3').html();
							
							//VARIABLES
							var inv_edificio = '';
							var inv_planta = '';
							var inv_producto = '';
							var inv_descripcion = '';
							var inv_ancho = '';
							var inv_alto = '';
							var inv_cantidad = '';
							var inv_monto = '';
							var inv_respuesta = '';
							var inv_resp_marca = '';
							var cont_product = 0;
							var cont_resp_marca = 0;
							var enviar = true;
							
							//RECOPILAMOS LOS DATOS
							obj.find('.edificios').each(function(indice){
						    	var edificio = $(this).find('.box-header h3').html();
						    	
								inv_edificio = inv_edificio + edificio + ';';
								
						    	
								$(this).find('.box-body .plantas').each(function(indice2){
						    		var planta = $(this).find('.box-header h3').html();
						    		
									inv_planta = inv_planta + planta + ';';
									
									$(this).find('.box-body .productos').each(function(indice3){
										var producto = $(this).find('.box-header input[name=producto]').val();
										inv_producto = inv_producto + producto + ';';
										
										var descripcion = $(this).find('.box-header input[name=descripcion]').val();
										inv_descripcion = inv_descripcion + descripcion + ';';
										
										var ancho = $(this).find('.box-header input[name=ancho]').val();
										inv_ancho = inv_ancho + ancho + ';';
										
										var alto = $(this).find('.box-header input[name=alto]').val();
										inv_alto = inv_alto + alto + ';';
										
										var cantidad = $(this).find('.box-header input[name=cantidad]').val();
										inv_cantidad = inv_cantidad + cantidad + ';';
										
										var monto = $(this).find('.box-header input[name=monto]').val();
										inv_monto = inv_monto + monto + ';';
										
										$(this).find('.box-header .respuesta').each(function(indice4){
											cont_product++;
											var respuesta = $(this).find('label').text();
											inv_respuesta = inv_respuesta + respuesta + ';';
											
											var resp_marca = $(this).find('input').val();
											if (resp_marca) {
												inv_resp_marca = inv_resp_marca + resp_marca + ';';
												cont_resp_marca++;
											} else {
												inv_resp_marca = inv_resp_marca + 'null' + ';';
											}
										});
										
										inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
						    			inv_respuesta = inv_respuesta + '}';
						    			
						    			inv_resp_marca = inv_resp_marca.substring(0, inv_resp_marca.length-1);
						    			inv_resp_marca = inv_resp_marca + '}';
								});
							    	
									inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
						    		inv_respuesta = inv_respuesta + '|';
						    		
						    		inv_resp_marca = inv_resp_marca.substring(0, inv_resp_marca.length-1);
						    		inv_resp_marca = inv_resp_marca + '|';
						    		
						    		inv_producto = inv_producto.substring(0, inv_producto.length-1);
						    		inv_producto = inv_producto + '|';
									
									inv_descripcion = inv_descripcion.substring(0, inv_descripcion.length-1);
									inv_descripcion = inv_descripcion + '|';
									
									inv_ancho = inv_ancho.substring(0, inv_ancho.length-1);
									inv_ancho = inv_ancho + '|';
									
									inv_alto = inv_alto.substring(0, inv_alto.length-1);
									inv_alto = inv_alto + '|';
									
									inv_cantidad = inv_cantidad.substring(0, inv_cantidad.length-1);
									inv_cantidad = inv_cantidad + '|';
									
									inv_monto = inv_monto.substring(0, inv_monto.length-1);
									inv_monto = inv_monto + '|';
									
						    	});
						    	
								inv_planta = inv_planta.substring(0, inv_planta.length-1);
							    inv_planta = inv_planta + '|';
							    
							    inv_producto = inv_producto.substring(0, inv_producto.length-1);
							    inv_producto = inv_producto + '^';
								
								inv_descripcion = inv_descripcion.substring(0, inv_descripcion.length-1);
								inv_descripcion = inv_descripcion + '^';
								
								inv_ancho = inv_ancho.substring(0, inv_ancho.length-1);
								inv_ancho = inv_ancho + '^';
								
								inv_alto = inv_alto.substring(0, inv_alto.length-1);
								inv_alto = inv_alto + '^';
								
								inv_cantidad = inv_cantidad.substring(0, inv_cantidad.length-1);
								inv_cantidad = inv_cantidad + '^';
								
								inv_monto = inv_monto.substring(0, inv_monto.length-1);
								inv_monto = inv_monto + '^';
							    
							    inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
							    inv_respuesta = inv_respuesta + '^';
							    
							    inv_resp_marca = inv_resp_marca.substring(0, inv_resp_marca.length-1);
							    inv_resp_marca = inv_resp_marca + '^';
								
						    });
							
							//ESTADO DE LA INSPECCION
							var tia_estado = 1;
							if ($(this).hasClass('completar_inspeccion_avanzada')) {
								tia_estado = 2;
								
								if (cont_resp_marca != cont_product) {
									enviar = false;
									bootbox.alert('<?php echo lang("trabajo.inspeccion.avanzada.cerrado.ko") ?>');
								}
							}
							
							if (enviar) {
								var datos = {
									'inv_id'	:	inv_id,
									'tr_id'	:	tr_id,
									'man_id'	:	$('#inspecc_mant option:selected').val(),
									'tia_comentario'	:	$(this).parent().parent().prev().prev().find('textarea').val(),
									'inv_edificio'	:	inv_edificio,
									'inv_planta'	:	inv_planta,
									'inv_producto'	:	inv_producto,
									'inv_descripcion' : inv_descripcion,
									'inv_ancho' : inv_ancho,
									'inv_alto' : inv_alto,
									'inv_cantidad' : inv_cantidad,
									'inv_monto' : inv_monto,
									'inv_respuesta'	:	inv_respuesta,
									'inv_resp_marca': inv_resp_marca,
									'inv_nombre'	:	inv_nombre,
									'tia_estado'	:	tia_estado
								};
								
								$(this).parent().parent().parent().find(".fotos").ajaxForm({
									dataType: 'json',
									data: datos,
									success: function(data) {
										if (data.sql) {
											if (data.status) {
												//TODO CORRECTO
												//MENSAJE GUARDAR INSPECCION
												if (tia_estado < 2) {
													bootbox.alert('<?php echo lang("trabajo.inspeccion.ok") ?>');
												} else {
													//MENSAJE CERRAR INSPECCION
													bootbox.alert('<?php echo lang("trabajo.inspeccion.avanzada.cerrado.ok") ?>');
													$('#'+inv_id+' .botons, #'+inv_id+' .fotos').hide();
													$('#'+inv_id+' input, #'+inv_id+' textarea').attr('disabled','disabled');
													setTimeout("location.href='"+location+"'", 5000);
												}
												
												//RECARGA LAS FOTOS SI SE SUBIERON FOTOS NUEVAS
												if ($('#'+inv_id).find('.fot input:eq(1)').val() != '') {
													//imagenes_avanzadas(inv_id);
													$('#'+inv_id).find('.fot input:eq(1)').val('')
												}
											} else {
												//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
												//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
												$.each(data.errors, function (ind, elem) {
													$.each(data.errors[ind], function (ind2, elem2) {
														//SI EXSITE ERROR EN EL CAMPO FOTO ENTRAMOS POR AQUI
														if (data.errors[ind][0] == 'fotos') {
															$('#'+inv_id+' .fot').find('.text-danger').html(data.errors[ind][ind2]);
														} else {
															//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
															$('#'+inv_id+' .textar').find('.text-danger').html(data.errors[ind][ind2]);
															$('#'+inv_id+' .textar').find('.form-group').addClass("has-error has-feedback");
															$('#'+inv_id+' .textar').find('.glyphicon-remove').show();
														}
													}); 
												});
											}
										} else {
											bootbox.alert('<?php echo lang("error.ajax") ?>');
										}
										l.stop();
									},
									error: function(XMLHttpRequest, textStatus, errorThrown) {
										bootbox.alert('<?php echo lang("error.ajax") ?>');
										l.stop();
									}
								}).submit();
							}
							l.stop();
						});
						
						
					},
					complete: function() {
						$('#cargar_inspeccion').hide();
					}
				});
			}
		}
		
		//RELLENAMOS EL SELECT DE LOS MANTENIMIENTOS DE ESTE TRABAJO
		for (var i=0; i<locations.length; i++) {
			$('#inspecc_mant').append('<option value="'+locations[i][0]+'">'+locations[i][3]+'</option>');
		}
		
		//LLAMAMOS A LA FUNCION INSPECCIONES_TRABAJO CADA VEZ QUE CAMBIE EL SELECT
		$("#inspecc_mant").change(function () {
			$('#tab_2 #inspec').hide().html('<center><img src="<?php echo base_url() ?>img/load.gif" id="cargar_inspeccion" /></center>').fadeIn(800);
			inspecciones_trabajo();			
		}).trigger('change');
		
		//COLAPSAR Y EXPANDIR
		$("#ver_trabajo #inspec").on('click', '.encoger_inspeccion,.encoger_edificio,.encoger_planta,.encoger_producto', function () {
			//BOTON EXPANDIR Y COLAPSAR
			$header = $(this);
			//CONTENIDO
			$content = $header.parent().parent().next();
			//COLAPSAR O EXPANDIR
			$content.slideToggle(500, function () {
			//CAMBIAR ICONO
			$header.html(function () {
				return $content.is(":visible") ? '<i class="fa fa-minus"></i>' : '<i class="fa fa-plus"></i>';
			    });
			});
		});
	}

	//LLENAMOS BITACORA
	function llenar_bitacora(){
		$.post(
		"<?php echo base_url().$this->lang->lang() ?>/bitacora/buscar_todo",
		{'bi_idasociado':'<?php echo isset($tr_id)?$tr_id:"0" ?>','bi_tipo':'TR'},
		function(data){
			
			var table = '';
			$.each(data,function(indice) {
				table = table + '<tr id="<?php echo isset($tr_id)?$tr_id:'' ?>">';
				table = table + '<td>' + data[indice]['bi_fecha'] + '</td>';	
				var empleado = data[indice]['emp_nombre'] + ' ' + data[indice]['emp_apellido1']+ ' ' + data[indice]['emp_apellido2'];	
				table = table + '<td>' + empleado + '</td>';			
				table = table + '<td>' + data[indice]['bi_accion'] + '</td>';			
				table = table + '</tr>';
			});

			$('#bitacora tbody').html(table);
	                
		}, "json");
	}

	llenar_bitacora();
});

/* ESTILO Y BOTON FOTOS */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});
</script>