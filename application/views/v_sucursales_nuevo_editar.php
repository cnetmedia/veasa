<?php
//CREAMOS LOS INPUT DEL FORMULARIO
$input_nombre = array(
	'name'		=>	'su_nombre',
	'id'		=>	'su_nombre',
	'class'		=>	'form-control',
	'maxlength'	=>	'80',
	'value'		=>	isset($su_nombre)?$su_nombre:set_value('su_nombre')
);

$input_telefono = array(
	'name'		=>	'su_telefono',
	'id'		=>	'su_telefono',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'value'		=>	isset($su_telefono)?$su_telefono:set_value('su_telefono')
);

$input_email = array(
	'name'		=>	'su_email',
	'id'		=>	'su_email',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($su_email)?$su_email:set_value('su_email')
);

$input_provincia = array(
	'name'		=>	'su_provincia',
	'id'		=>	'su_provincia',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($su_provincia)?$su_provincia:set_value('su_provincia')
);

$input_postal = array(
	'name'		=>	'su_postal',
	'id'		=>	'su_postal',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	isset($su_postal)?$su_postal:set_value('su_postal')
);

$input_direccion = array(
	'name'		=>	'su_direccion',
	'id'		=>	'su_direccion',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($su_direccion)?$su_direccion:set_value('su_direccion')
);

$input_barriada = array(
	'name'		=>	'su_barriada',
	'id'		=>	'su_barriada',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($su_barriada)?$su_barriada:set_value('su_barriada')
);

$input_localidad = array(
	'name'		=>	'su_localidad',
	'id'		=>	'su_localidad',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($su_localidad)?$su_localidad:set_value('su_localidad')
);

$input_iva = array(
	'name'		=>	'su_iva',
	'id'		=>	'su_iva',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($su_iva)?$su_iva:set_value('su_iva')
);

$input_iva_reducido = array(
	'name'		=>	'su_iva_reducido',
	'id'		=>	'su_iva_reducido',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($su_iva_reducido)?$su_iva_reducido:set_value('su_iva_reducido')
);

$input_iva_superreducido = array(
	'name'		=>	'su_iva_superreducido',
	'id'		=>	'su_iva_superreducido',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($su_iva_superreducido)?$su_iva_superreducido:set_value('su_iva_superreducido')
);

$input_su_id = array(
	'name'		=>	'su_id',
	'id'		=>	'su_id',
	'type'		=>	'hidden',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	isset($su_id)?$su_id:set_value('su_id')
);

$input_em_id = array(
	'name'		=>	'em_id',
	'id'		=>	'em_id',
	'type'		=>	'hidden',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	$this->session->userdata('em_id')
);
?>

<div class="row">
	<!-- FORMULARIO SUCURSAL -->
	<div class="col-lg-12">
		<form id="datos_sucursal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('nombre') ?></label>
					    <?php echo form_input($input_nombre) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_nombre') ?></div>
					</div>
				</div>
				
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('zona.horaria') ?></label>
					    <?php echo timezone_menu(isset($su_zona_horaria)?$su_zona_horaria:'UTC','form-control','su_zona_horaria'); ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_zona_horaria') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('email') ?></label>
					    <?php echo form_input($input_email) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_email') ?></div>
					</div>
				</div>
				
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('provincia') ?></label>
					    <?php echo form_input($input_provincia) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_provincia') ?></div>
					</div>
				</div>
				
				<div class="col-lg-2 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('postal') ?></label>
					    <?php echo form_input($input_postal) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_postal') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-3 col-xs-12">
					<div class="form-group">
						<label><?php echo lang('telefono') ?></label>
					    <?php echo form_input($input_telefono) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_telefono') ?></div>
					</div>
				</div>
			
				<div class="col-lg-9 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('direccion') ?></label>
					    <?php echo form_input($input_direccion) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_direccion') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('barriada') ?></label>
					    <?php echo form_input($input_barriada) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_barriada') ?></div>
					</div>
				</div>
			
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('localidad') ?></label>
					    <?php echo form_input($input_localidad) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_localidad') ?></div>
					</div>
				</div>
				
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('pais') ?></label>
					    <select name="id_countries" id="paises" class="form-control"></select>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('id_countries') ?></div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('iva.general') ?></label>
					    <?php echo form_input($input_iva) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_iva') ?></div>
					</div>
				</div>
			
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('iva.reducido') ?></label>
					    <?php echo form_input($input_iva_reducido) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_iva_reducido') ?></div>
					</div>
				</div>
				
				<div class="col-lg-4 col-xs-12">
					<div class="form-group">
					    <label><?php echo lang('iva.superreducido') ?></label>
					    <?php echo form_input($input_iva_superreducido) ?>
					    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_iva_superreducido') ?></div>
					</div>
				</div>
			</div>
			
			<?php echo form_input($input_su_id) ?>
			<?php echo form_input($input_em_id) ?>
			
			<div class="alert alert-info">
				<div class="row">
					<div class="col-lg-3 col-md-3">
					    <label><?php echo lang('facturas') ?>:</label>
					    <span class="contador"><?php echo $contadores[0]->co_contador ?></span>
					</div>
					    		
					<div class="col-lg-3 col-md-3">
					    <label><?php echo lang('rectificativas') ?>:</label>
					    <span class="contador"><?php echo $contadores[1]->co_contador ?></span>
					</div>
					    		
					<div class="col-lg-3 col-md-3">
					    <label><?php echo lang('presupuestos') ?>:</label>
					    <span class="contador"><?php echo $contadores[2]->co_contador ?></span>
					</div>
					    
					<div class="col-lg-3 col-md-3">
					    <label><?php echo lang('anio.actual') ?>:</label>
					    <span class="anio"><?php echo $contadores[0]->co_year ?></span>
					</div>
				</div>
			</div>
			
			<div class="row">				
				<div class="col-lg-12 col-xs-12 text-right">
					<!-- SI PUEDE CREAR O EDITAR CONTENIDO MUESTRA EL BOTON -->
					<?php if ($this->session->userdata('emp_crear') == '1' || $this->session->userdata('emp_editar') == '1') { ?>
						<?php if ($su_estado == 1) { ?>
							<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
								<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
							</button>
						<?php } ?>
					<?php } ?>
					
					<!-- SI PUEDE EDITAR MOSTRAMOS EL BOTON DE HABILITAR Y DESHABILITAR -->
					<?php if ($su_id != '' && $this->session->userdata('emp_editar') == '1') { ?>
						<?php if ($su_estado == 0) { ?>
							<button id="btn_habilitar" type="button" class="btn btn-warning ladda-button" data-style="zoom-out">
								<i class="fa fa-check"></i> <?php echo lang('habilitar') ?>
							</button>
						<?php } else {?>
							<button id="btn_deshabilitar" type="button" class="btn btn-danger ladda-button" data-style="zoom-out">
								<i class="fa fa-times"></i> <?php echo lang('deshabilitar') ?>
							</button>
						<?php } ?>
					<?php } ?>

					<a href="<?php echo base_url().$this->lang->lang().'/sucursales' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){
	//PREGUNTAMOS SI PUEDE EDITAR
	var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	if (editar == 0) {
		$('#btn_guardar, #btn_habilitar, #btn_deshabilitar').hide();
		$('#datos_sucursal *').attr('readonly', true);
	}
	
	//RELLENAMOS EL CAMPO DE PAISES PARA AÑADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/sucursales/paises",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
			if (data[indice]["id_countries"] == '<?php echo isset($id_countries)?$id_countries:"" ?>') {
					$('#paises').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
			} else {
					$('#paises').append('<option value="'+data[indice]["id_countries"]+'">'+data[indice]["name"]+'</option>');
			}
	
		});
	}, "json");
	
	//HABILITAMOS LA VISION
	$('#btn_habilitar').click(function(e){
		e.preventDefault();
		var l = Ladda.create( document.querySelector( "#btn_habilitar" ) );
	 	l.start();
		
		$.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/sucursales/estado",
		  	data: {'su_id':'<?php echo $su_id ?>'},
		  	success: function(data) {
		  		if (data.sql) {
					if (!data.editar) {
						bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
						//REDIRIGIMOS A LOS 5 SEGUNDOS
						setTimeout("location.href='"+location+"'", 5000);
					} else {
						location.reload(true);
					}
		  		} else {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
				}
				l.stop();
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
	
	//ALERT CONFIRMANDO LA DESHABILITACION DEL ELEMENTO
	$('#btn_deshabilitar').click(function(e){
		e.preventDefault();
		bootbox.dialog({
			message: "<?php echo lang('confirmar.deshabilitar') ?>",
			buttons: {
				success: {
					label: "<?php echo lang('cancelar') ?>",
					className: "btn-default"
				},
				danger: {
					label: "<?php echo lang('deshabilitar') ?>",
					className: "btn-danger",
					callback: function(result) {
						var l = Ladda.create( document.querySelector( "#btn_deshabilitar" ) );
	 					l.start();
		    			
		    			$.ajax({
							type: "POST",
							dataType: "json",
						  	url: "<?php echo base_url().$this->lang->lang() ?>/sucursales/estado",
						  	data: {'su_id':'<?php echo $su_id ?>'},
						  	success: function(data) {
						  		if (data.sql) {
									if (!data.editar) {
										bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										setTimeout("location.href='"+location+"'", 5000);
									} else {
										location.reload(true);
									}
						  		} else {
									bootbox.alert('<?php echo lang("error.ajax") ?>');
								}
								l.stop();
						  	},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});
					}
				}
			}
		});
	});
	
	//SI LA SUCURSAL ESTA DESACTIVADA BLOQUEAMOS LOS CAMPOS
	var desactivado = '<?php echo isset($su_estado)?$su_estado:"" ?>';
	if ($('#su_id').val() != '' && desactivado == 0) {
		$('#datos_sucursal *').attr('readonly', true);
	}
	
	//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO
	$('#datos_sucursal .form-group span').hide();
	
	//EJECUTAMOS EL FORMULARIO AL DARLE AL BOTON
	$('#btn_guardar').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#datos_sucursal .form-group, #datos_sucursal .input-group").removeClass("has-error has-feedback");
	    $('#datos_sucursal .glyphicon-remove').hide();
	    $("#datos_sucursal .text-danger").html('');
	    
	    var datos = {
			'su_id' : '<?php echo isset($su_id)?$su_id:"" ?>',
			'su_nombre' : $('#su_nombre').val(),
			'su_zona_horaria' : $('select[name=su_zona_horaria] option:selected').val(),
			'su_email' : $('#su_email').val(),
			'su_provincia' : $('#su_provincia').val(),
			'su_postal' : $('#su_postal').val(),
			'su_telefono' : $('#su_telefono').val(),
			'su_direccion' : $('#su_direccion').val(),
			'su_barriada' : $('#su_barriada').val(),
			'su_localidad' : $('#su_localidad').val(),
			'su_pais' : $('#paises option:selected').val(),
			'su_iva' : $('#su_iva').val(),
			'su_iva_reducido' : $('#su_iva_reducido').val(),
			'su_iva_superreducido' : $('#su_iva_superreducido').val()
		};
		
	    $.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/sucursales/guardar_editar",
		  	data: datos,
		  	success: function(data) {
		  		if (data.sql) {
		  			//MENSAJES ALERTA PARA EDITAR O CREAR
		  			if (datos['su_id'] != '') {
						if (!data.editar) {
							bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
							//REDIRIGIMOS A LOS 5 SEGUNDOS
							setTimeout("location.href='"+location+"'", 5000);
						}
					} else {
						if (!data.crear) {
							bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
							//REDIRIGIMOS A LOS 5 SEGUNDOS
							setTimeout("location.href='"+location+"'", 5000);
						}
					}
					
					//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
		  			if (data.status) {
		  				//TODO CORRECTO
						//SI ES NUEVA SUCURSAL
						if ($('#su_id').val() == '') {
							$('#datos_sucursal input').val('');
							bootbox.dialog({
								message: "<?php echo lang('sucursal.nuevo.ok') ?>",
								buttons: {
									success: {
										label: "<?php echo lang('nuevo') ?>",
										className: "btn-success",
										callback: function(result) {
											var url = '<?php echo base_url().$this->lang->lang() ?>/sucursales/nueva_sucursal';
											$(location).attr('href',url);
										}
									},
									main: {
										label: "<?php echo lang('sucursales') ?>",
										className: "btn-primary",
										callback: function(result) {
											var url = '<?php echo base_url().$this->lang->lang() ?>/sucursales';
											$(location).attr('href',url);
										}
									}
								}
							});
						} else {
						//SI ES UNA SUCURSAL EDITADA
							bootbox.alert('<?php echo lang("sucursal.editado.ok") ?>');
						}
		  			} else {
						//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
						//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
						if (data.errors.length > 0) {
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
								}); 
							});
						}
					}
		  		} else {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
				}
				l.stop();
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
	
	//CERRAR AÑO
	$('#btn_cerrar_anio').click(function(e){
		e.preventDefault();
		var l = Ladda.create( document.querySelector( "#btn_cerrar_anio" ) );
	 	l.start();
		
		$.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/sucursales/cerrar_anio",
		  	data: {'su_id':'<?php echo $su_id ?>'},
		  	success: function(data) {
		  		if (data.sql) {
					bootbox.alert('<?php echo lang("cerrar.anio.ok") ?>');
					$('.contador').html('0');
					$('.anio').html(parseInt($('.anio').html()) + 1);
		  		} else {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
				}
				l.stop();
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
});
</script>