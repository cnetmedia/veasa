<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th>ID</th>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("email") ?></th>
                		<th><?php echo lang("cif") ?></th>
                		<th><?php echo lang("localidad") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th>ID</th>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("email") ?></th>
                		<th><?php echo lang("cif") ?></th>
                		<th><?php echo lang("localidad") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//MOSTRAR PROVEEDORES
	function mostrar_proveedores() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
			        var table = '';
					$.each(data,function(indice,valor) {
						table = table + '<tr id="'+data[indice]['pro_id']+'">';
						table = table + '<td>'+data[indice]['pro_id']+'</td>';
  						table = table + '<td>'+data[indice]['pro_nombre']+'</td>';
  						table = table + '<td>'+data[indice]['pro_telefono']+'</td>';
  						table = table + '<td>'+data[indice]['pro_email']+'</td>';
  						table = table + '<td>'+data[indice]['pro_cif']+'</td>';
  						table = table + '<td>'+data[indice]['pro_localidad']+'</td>';
  						table = table + '</tr>';
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 1, "asc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-th-large'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
					var dep = '<?php echo $this->session->userdata("de_id") ?>';
			        if (crear == '0') {
			        	$('#listar #btn_nuevo').hide();
			        } else {
						if (dep == '3') { //Si pertenece a cotizaciones no puede crear
							$('#listar #btn_nuevo').hide();
						}
					}
			        
			        //NUEVO PROVEEDOR
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/proveedores/nuevo_proveedor');
					});
					
					//REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
					
					//EDITAR O VER PROVEEDOR
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/proveedores/editar_proveedor/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_proveedores();
});
</script>