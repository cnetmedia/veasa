<?php
//CREAMOS LOS INPUT DEL FORMULARIO
$input_nombre = array(
	'name'		=>	'emp_nombre',
	'id'		=>	'emp_nombre',
	'class'		=>	'form-control',
	'maxlength'	=>	'20',
	'value'		=>	isset($emp_nombre)?$emp_nombre:set_value('emp_nombre')
);

$input_apellido1 = array(
	'name'		=>	'emp_apellido1',
	'id'		=>	'emp_apellido1',
	'class'		=>	'form-control',
	'maxlength'	=>	'40',
	'value'		=>	isset($emp_apellido1)?$emp_apellido1:set_value('emp_apellido1')
);

$input_apellido2 = array(
	'name'		=>	'emp_apellido2',
	'id'		=>	'emp_apellido2',
	'class'		=>	'form-control',
	'maxlength'	=>	'40',
	'value'		=>	isset($emp_apellido2)?$emp_apellido2:set_value('emp_apellido2')
);

$input_dni = array(
	'name'		=>	'emp_dni',
	'id'		=>	'emp_dni',
	'class'		=>	'form-control',
	'maxlength'	=>	'15',
	'value'		=>	isset($emp_dni)?$emp_dni:set_value('emp_dni')
);

$input_telefono = array(
	'name'		=>	'emp_telefono',
	'id'		=>	'emp_telefono',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($emp_telefono)?$emp_telefono:set_value('emp_telefono')
);

$input_email = array(
	'name'		=>	'emp_email',
	'id'		=>	'emp_email',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($emp_email)?$emp_email:set_value('emp_email')
);

$input_provincia = array(
	'name'		=>	'emp_provincia',
	'id'		=>	'emp_provincia',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($emp_provincia)?$emp_provincia:set_value('emp_provincia')
);

$input_postal = array(
	'name'		=>	'emp_postal',
	'id'		=>	'emp_postal',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	isset($emp_postal)?$emp_postal:set_value('emp_postal')
);

$input_direccion = array(
	'name'		=>	'emp_direccion',
	'id'		=>	'emp_direccion',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($emp_direccion)?$emp_direccion:set_value('emp_direccion')
);

$input_barriada = array(
	'name'		=>	'emp_barriada',
	'id'		=>	'emp_barriada',
	'class'		=>	'form-control',
	'maxlength'	=>	'50',
	'value'		=>	isset($emp_barriada)?$emp_barriada:set_value('emp_barriada')
);

$input_localidad = array(
	'name'		=>	'emp_localidad',
	'id'		=>	'emp_localidad',
	'class'		=>	'form-control',
	'maxlength'	=>	'100',
	'value'		=>	isset($emp_localidad)?$emp_localidad:set_value('emp_localidad')
);

$input_nacimiento = array(
	'name'		=>	'emp_nacimiento',
	'id'		=>	'emp_nacimiento',
	'class'		=>	'form-control',
	'maxlength'	=>	'10',
	'value'		=>	isset($emp_nacimiento)?$emp_nacimiento:set_value('emp_nacimiento')
);

$input_emp_id = array(
	'name'		=>	'emp_id',
	'id'		=>	'emp_id',
	'type'		=>	'hidden',
	'class'		=>	'form-control',
	'maxlength'	=>	'11',
	'value'		=>	isset($emp_id)?$emp_id:set_value('emp_id')
);

$input_clave_uno = array(
	'name'		=>	'emp_clave_uno',
	'id'		=>	'emp_clave_uno',
	'class'		=>	'form-control',
	'maxlength'	=>	'6',
	'value'		=>	set_value('emp_clave_uno')
);
		
$input_clave_dos = array(
	'name'		=>	'emp_clave_dos',
	'id'		=>	'emp_clave_dos',
	'class'		=>	'form-control',
	'maxlength'	=>	'6',
	'value'		=>	set_value('emp_clave_dos')
);
?>

<form id="datos_empleado" action="<?php echo base_url().$this->lang->lang() ?>/empleados/guardar_editar" method="post" enctype="multipart/form-data">

	<div class="row datos1">
		<!-- MOSTRAMOS LA FOTO SI EL CLIENTE ES A EDITAR -->
		<?php if ($emp_id != null) { ?>
		<div class="col-lg-4 col-md-4 col-xs-12 text-center">
			<img src="<?php echo isset($emp_id)?base_url().$this->lang->lang().'/empresa/view_file/'.$em_cif.'/emp/'.$emp_dni.'/perfil.jpg':base_url().$this->lang->lang().'empresa/view_file/perfil.jpg'; ?>" id="perfil" class="img-thumbnail" />			
		</div>
		<?php } ?>
		
		<!-- FORMULARIO CLIENTE -->
		<div class="<?php echo ($emp_id != '')?'col-lg-8':'col-lg-12' ?>">
				<div class="row">
					<div class="col-lg-4 col-xs-12">
						<div class="form-group">
							<label><?php echo lang('nombre') ?></label>
						    <?php echo form_input($input_nombre) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_nombre') ?></div>
						</div>
					</div>
					
					<div class="col-lg-4 col-xs-12">
						<div class="form-group">
							<label><?php echo lang('apellido1') ?></label>
						    <?php echo form_input($input_apellido1) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_apellido1') ?></div>
						</div>
					</div>
					
					<div class="col-lg-4 col-xs-12">
						<div class="form-group">
							<label><?php echo lang('apellido2') ?></label>
						    <?php echo form_input($input_apellido2) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_apellido2') ?></div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('email') ?></label>
						    <?php echo form_input($input_email) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_email') ?></div>
						</div>
					</div>
					
					<div class="col-lg-3 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('dni') ?></label>
						    <?php echo form_input($input_dni) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_dni') ?></div>
						</div>
					</div>
					
					<div class="col-lg-3 col-xs-12">
						<div class="form-group">
							<label><?php echo lang('telefono') ?></label>
						    <?php echo form_input($input_telefono) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_telefono') ?></div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('provincia') ?></label>
						    <?php echo form_input($input_provincia) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_provincia') ?></div>
						</div>
					</div>
					
					<div class="col-lg-2 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('postal') ?></label>
						    <?php echo form_input($input_postal) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_postal') ?></div>
						</div>
					</div>
				
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('direccion') ?></label>
						    <?php echo form_input($input_direccion) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_direccion') ?></div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('barriada') ?></label>
						    <?php echo form_input($input_barriada) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_barriada') ?></div>
						</div>
					</div>
				
					<div class="col-lg-4 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('localidad') ?></label>
						    <?php echo form_input($input_localidad) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_localidad') ?></div>
						</div>
					</div>
					
					<div class="col-lg-4 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('pais') ?></label>
						    <select name="emp_pais" id="paises_pais" class="form-control"></select>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_pais') ?></div>
						</div>
					</div>
				</div>
				
				<?php echo form_input($input_emp_id) ?>
				
				<div class="row">
					<div class="col-lg-4 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('nacionalidad') ?></label>
						    <select name="emp_nacionalidad" id="paises_nacionalidad" class="form-control"></select>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_nacionalidad') ?></div>
						</div>
					</div>
					
					<div class="col-lg-2 col-xs-12">
						<div class="form-group">
						    <label><?php echo lang('nacimiento') ?></label>
						    <?php echo form_input($input_nacimiento) ?>
						    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
							<div class="text-danger"><?php echo form_error('emp_nacimiento') ?></div>
						</div>
					</div>
					
					<div class="col-lg-6 col-xs-12 foto">
						<label><?php echo lang('foto.empleado') ?></label>					
						<div class="input-group">
							<span class="input-group-btn">
								<span class="btn btn-primary btn-file">
									<!--<input type="file" multiple>-->
									<i class="fa fa-camera"></i> <?php echo lang('cargar.foto') ?>
									<input type="file" name="foto">
								</span>
							</span>
							<input type="text" class="form-control" readonly>
						</div>
						<div class="text-danger"><?php echo form_error('foto') ?></div>
					</div>
				</div>
		</div>
	</div>
	
	<div class="row datos2">
		<div class="col-lg-4">
			<div class="panel panel-default">
	  			<div class="panel-body">
	  				<div class="form-group">
						<label><?php echo lang('sucursal') ?></label>
						<select id="sucursales" name="su_id" class="form-control"></select>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('su_id') ?></div>
					</div>
	  			
					<div class="form-group">
						<label><?php echo lang('departamentos') ?></label>
						<select id="departamentos" name="de_id" class="form-control"></select>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('de_id') ?></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-8">
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<div class="panel panel-default">
		  				<div class="panel-body">
		    				<div class="form-group" id="accesos">
		    					<label><?php echo lang('acceso') ?></label>
		    					<div class="radio">
									<label>
								    	<input type="radio" name="emp_acceso" value="1" <?php echo ($emp_acceso == '1')?'checked':'' ?> /> <?php echo lang('si') ?>
								    	<input type="radio" name="emp_acceso" value="0" <?php echo ($emp_acceso == '0')?'checked':'' ?> /> <?php echo lang('no') ?>
								  	</label>
								</div>
		    				</div>
		  				</div>
					</div>
				</div>
			
				<div class="col-lg-4 col-xs-12">
					<div class="panel panel-default">
		  				<div class="panel-body">
		    				<div class="form-group" id="editar">
		    					<label><?php echo lang('editar') ?></label>
		    					<div class="radio">
									<label>
								    	<input type="radio" name="emp_editar" value="1" <?php echo ($emp_editar == 1)?'checked':'' ?> /> <?php echo lang('si') ?>
								    	<input type="radio" name="emp_editar" value="0" <?php echo ($emp_editar == 0)?'checked':'' ?> /> <?php echo lang('no') ?>
								  	</label>
								</div>
		    				</div>
		  				</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-xs-12">
					<div class="panel panel-default">
		  				<div class="panel-body">
		    				<div class="form-group" id="crear">
		    					<label><?php echo lang('crear') ?></label>
		    					<div class="radio">
									<label>
								    	<input type="radio" name="emp_crear" value="1" <?php echo ($emp_crear == 1)?'checked':'' ?> /> <?php echo lang('si') ?>
								    	<input type="radio" name="emp_crear" value="0" <?php echo ($emp_crear == 0)?'checked':'' ?> /> <?php echo lang('no') ?>
								  	</label>
								</div>
		    				</div>
		  				</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
		  				<div class="panel-body">
		    				<div class="row">
		    					<div class="form-group" id="tipos">
		    						<div class="col-lg-12">
		    							<label><?php echo lang('tipo') ?></label>
		    						</div>
		    						
			    					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			    						<div class="radio">
											<label>
												<input type="radio" name="emp_tipo" value="0" <?php echo ($emp_tipo == 0)?'checked':'' ?> /> <?php echo lang('super') ?>
										  	</label>
										</div>
			    					</div>
			    					
			    					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
			    						<div class="radio">
											<label>
												<input type="radio" name="emp_tipo" value="1" <?php echo ($emp_tipo == 1)?'checked':'' ?> /> <?php echo lang('gerente') ?>
										  	</label>
										</div>
			    					</div>
			    					
			    					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			    						<div class="radio">
											<label>
												<input type="radio" name="emp_tipo" value="4" <?php echo ($emp_tipo == 4)?'checked':'' ?> /> <?php echo lang('administrador') ?>
										  	</label>
										</div>
			    					</div>
			    					
			    					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
			    						<div class="radio">
											<label>
												<input type="radio" name="emp_tipo" value="2" <?php echo ($emp_tipo == 2)?'checked':'' ?> /> <?php echo lang('supervisor') ?>
										  	</label>
										</div>
			    					</div>
			    					
			    					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
			    						<div class="radio">
											<label>
												<input type="radio" name="emp_tipo" value="3" <?php echo ($emp_tipo == 3)?'checked':'' ?> /> <?php echo lang('empleado') ?>
										  	</label>
										</div>
			    					</div>
		    					</div>
		    				</div>
		  				</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="panel panel-default">
		  		<div class="panel-body">
		    		<div class="form-group" id="ver">
		    			<label><?php echo lang('secciones') ?></label>
		    			<?php  $emp_ver = explode(",", $emp_ver) ?>
		    			<div class="row">
							<div class="col-lg-3">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="emp_ver[]" value="20" <?php echo (in_array("20", $emp_ver))?'checked':'' ?> /> <?php echo lang('informes') ?>
						    		</label>
						    	</div>
						    	<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="1" <?php echo (in_array("1", $emp_ver))?'checked':'' ?> /> <?php echo lang('facturas') ?>
						    		</label>
						    	</div>
						    	
						    	<div class="checkbox">
									<label>
										<input type="checkbox" name="emp_ver[]" value="3" <?php echo (in_array("3", $emp_ver))?'checked':'' ?> /> <?php echo lang('presupuestos') ?>
						    		</label>
						    	</div>
							</div>
										
							<div class="col-lg-3">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="emp_ver[]" value="6" <?php echo (in_array("6", $emp_ver))?'checked':'' ?> /> <?php echo lang('sucursales') ?>
						    		</label>
						    	</div>
								
								<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="7" <?php echo (in_array("7", $emp_ver))?'checked':'' ?> /> <?php echo lang('departamentos') ?>
						    		</label>
						    	</div>
						    	
						    	<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="8" <?php echo (in_array("8", $emp_ver))?'checked':'' ?> /> <?php echo lang('empleados') ?>
						    		</label>
						    	</div>
						    	
						    	<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="13" <?php echo (in_array("13", $emp_ver))?'checked':'' ?> /> <?php echo lang('calendario') ?>
						    		</label>
						    	</div>
							</div>
										
							<div class="col-lg-3">
						    	<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="4" <?php echo (in_array("4", $emp_ver))?'checked':'' ?> /> <?php echo lang('clientes') ?>
						    		</label>
						    	</div>
						    	
						    	<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="5" <?php echo (in_array("5", $emp_ver))?'checked':'' ?> /> <?php echo lang('mantenimientos') ?>
						    		</label>
						    	</div>
						    	
						    	<div class="checkbox">
									<label>
										<input type="checkbox" name="emp_ver[]" value="9" <?php echo (in_array("9", $emp_ver))?'checked':'' ?> /> <?php echo lang('inspecciones') ?>
						   			</label>
						   		</div>
						   		
						   		<div class="checkbox">
									<label>
										<input type="checkbox" name="emp_ver[]" value="19" <?php echo (in_array("19", $emp_ver))?'checked':'' ?> /> <?php echo lang('inspecciones.avanzadas') ?>
						   			</label>
						   		</div>
						   		
						   		<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="10" <?php echo (in_array("10", $emp_ver))?'checked':'' ?> /> <?php echo lang('trabajos') ?>
						   			</label>
						   		</div>
						   		
						   		<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="18" <?php echo (in_array("18", $emp_ver))?'checked':'' ?> /> <?php echo lang('taller') ?>
						   			</label>
						   		</div>
							</div>
										
							<div class="col-lg-3">
						   		<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="14" <?php echo (in_array("14", $emp_ver))?'checked':'' ?> /> <?php echo lang('almacenes') ?>
						   			</label>
						   		</div>
						   		
						   		<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="16" <?php echo (in_array("16", $emp_ver))?'checked':'' ?> /> <?php echo lang('entradas') ?>
						   			</label>
						   		</div>
						   		
						   		<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="17" <?php echo (in_array("17", $emp_ver))?'checked':'' ?> /> <?php echo lang('salidas') ?>
						   			</label>
						   		</div>
						   		
						   		<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="11" <?php echo (in_array("11", $emp_ver))?'checked':'' ?> /> <?php echo lang('proveedores') ?>
						    		</label>
						    	</div>
						   		
						   		<div class="checkbox">
									<label>
						    			<input type="checkbox" name="emp_ver[]" value="15" <?php echo (in_array("15", $emp_ver))?'checked':'' ?> /> <?php echo lang('tienda.online') ?>
						   			</label>
						   		</div>
							</div>
						</div>
						<div class="text-danger"><?php echo form_error('emp_ver') ?></div>
		    		</div>
		  		</div>
			</div>
		</div>
	</div>
	
	<div class="row datos4">
		<div class="col-lg-8 col-md-offset-4">
			<div class="panel panel-default" id="claves">
  				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo form_label(lang('login.clave')) ?>
								<?php echo form_password($input_clave_uno) ?>
								<span class="glyphicon glyphicon-remove form-control-feedback"></span>
								<div class="text-danger"><?php echo form_error('emp_clave_uno') ?></div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo form_label(lang('repetir.clave')) ?>
								<?php echo form_password($input_clave_dos) ?>
								<span class="glyphicon glyphicon-remove form-control-feedback"></span>
								<div class="text-danger"><?php echo form_error('emp_clave_dos') ?></div>
							</div>
						</div>
					</div>
  				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12 text-right">
			<!-- SI PUEDE CREAR O EDITAR CONTENIDO MUESTRA EL BOTON -->
			<?php if ($this->session->userdata('emp_crear') == '1' || $this->session->userdata('emp_editar') == '1') { ?>
				<button id="btn_guardar" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
					<i class="fa fa-save"></i> <?php echo lang('guardar') ?>
				</button>
			<?php } ?>
			
			<!-- SI PUEDE EDITAR MOSTRAMOS EL BOTON DE HABILITAR Y DESHABILITAR -->
			<?php if ($emp_id != '' && $this->session->userdata('emp_editar') == '1') { ?>
				<?php if ($emp_estado == 0) { ?>
					<button id="btn_habilitar" type="button" class="btn btn-warning ladda-button" data-style="zoom-out">
						<i class="fa fa-check"></i> <?php echo lang('habilitar') ?>
					</button>
				<?php } else {?>
					<button id="btn_deshabilitar" type="button" class="btn btn-danger ladda-button" data-style="zoom-out">
						<i class="fa fa-times"></i> <?php echo lang('deshabilitar') ?>
					</button>
				<?php } ?>
			<?php } ?>
			
			<a href="<?php echo base_url().$this->lang->lang().'/empleados' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
		</div>
	</div>
</form>

<script>
/* ESTILO Y BOTON FOTO */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function(){
	//DATOS EMPLEADO SESSION
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	var emp_id = '<?php echo isset($emp_id)?$emp_id:"" ?>';
	var su_id = '<?php echo isset($su_id)?$su_id:"" ?>';
	var de_id = '<?php echo isset($de_id)?$de_id:"" ?>';
	
	//PREGUNTAMOS SI PUEDE EDITAR
	var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	if (editar == 0) {
		$('.datos1 *, .datos2 *, .datos3 *').attr('readonly', true);
		$('.datos2 .radio label div').addClass('disabled');
		$('.datos3 .radio label div').addClass('disabled');
		$('#ver .checkbox label div').addClass('disabled');
	}
	
	//OCULTAMOS LA CONTRASEÑA SI ES NUEVO EMPLEADO O EL QUE MIRA NO ES EL MISMO EMPLEADO A EDITAR
	if ($('#emp_id').val() != '<?php echo $this->session->userdata("emp_id") ?>') {
		$('#claves').hide();
	}
	
	//OCULTAMOS LOS DATOS DEL EMPLEADO SI NO PUEDE EDITAR, PARA QUE SOLO PUEDA CAMBIAR LA CONTRASEÑA
	if (editar == 0 && $('#emp_id').val() == '<?php echo $this->session->userdata("emp_id") ?>') {
		$('.datos2, .datos3, .foto').hide();
	} else if (editar == 0) {
		$('#btn_guardar').hide();
	} else {
		$('#btn_guardar').show();
	}
	
	//RELLENAMOS LOS DEPARTAMENTOS DE LA SUCURSAL
	function departamentos() {
		$('#departamentos').html('');
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/departamentos/departamento_sucursal",
			{'su_id':$('#sucursales option:selected').val()},
			function(datos){
				$.each(datos,function(indice) {
						if (datos[indice]["de_id"] == de_id) {
							$('#departamentos').append('<option value="'+datos[indice]["de_id"]+'" selected>'+datos[indice]["de_nombre"]+'</option>');
							mostrar = false;
						} else if (datos[indice]["de_id"] == '<?php echo $this->session->userdata("de_id") ?>') {
							$('#departamentos').append('<option value="'+datos[indice]["de_id"]+'" selected>'+datos[indice]["de_nombre"]+'</option>');
						} else {
							$('#departamentos').append('<option value="'+datos[indice]["de_id"]+'">'+datos[indice]["de_nombre"]+'</option>');
						}
				});
			}, "json");
	}
	
	//RELLENAMOS EL CAMPO DE SUCURSALES PARA AÑADIR O EDITAR
	var depart = true;
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
				if (data[indice]['su_estado'] == '1') {
					if (data[indice]["su_id"] == su_id) {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
					} else if (data[indice]["su_id"] == '<?php echo $this->session->userdata("su_id") ?>') {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
					} else if (data[indice]["su_estado"] == 1) {
						$('#sucursales').append('<option value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
					}
				}
		});
		
		departamentos();
		depart = true;
	}, "json");
	
	//CARGAMOS LOS DEPARTAMENTOS SEGUN SUCURSAL
	$("#sucursales").change(function () {
		if (depart) {
			departamentos();
		}	
	}).trigger('change');
	
	//RELLENAMOS EL CAMPO DE PAISES PARA AÑADIR O EDITAR
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/sucursales/paises",
	{'buscar':''},
	function(data){
		$.each(data,function(indice) {
				//PAIS
				if (data[indice]["id_countries"] == '<?php echo isset($emp_pais)?$emp_pais:"" ?>') {
					$('#paises_pais').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
				} else {
					$('#paises_pais').append('<option value="'+data[indice]["id_countries"]+'">'+data[indice]["name"]+'</option>');
				}
				
				//NACIONALIDAD
				if (data[indice]["id_countries"] == '<?php echo isset($emp_nacionalidad)?$emp_nacionalidad:"" ?>') {
					$('#paises_nacionalidad').append('<option value="'+data[indice]["id_countries"]+'" selected>'+data[indice]["name"]+'</option>');
				} else {
					$('#paises_nacionalidad').append('<option value="'+data[indice]["id_countries"]+'">'+data[indice]["name"]+'</option>');
				}
		});
	}, "json");
	
	//BLOQUEAMOS O NO EL DNI SI EL EMPLEADO ES A EDITAR
	if (($('#emp_dni').val() != '') && (tipo != 0)) {
		$('#emp_dni').attr('readonly', true);
	}
	
	//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO
	$('#datos_empleado .form-group span').hide();
	
	//ESTILO Y BOTON FOTO
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {	
		var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}	
	});
	
	//EJECUTAMOS EL FORMULARIO AL DARLE AL BOTON
	$('#btn_guardar').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#btn_guardar" ) );
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#datos_empleado .form-group, #datos_empleado .input-group").removeClass("has-error has-feedback");
	    $('#datos_empleado .glyphicon-remove').hide();
	    $("#datos_empleado .text-danger").html('');
	    
	    var depart_id = $('#departamentos option').size();
	    if (depart_id > 0) {
			jQuery("#datos_empleado").ajaxForm({
		    	dataType: 'json',
		    	data: '',
				success: function(data) {
					if (data.sql) {
						//MENSAJES ALERTA PARA  CREAR
						if (!data.crear) {
							bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
							//REDIRIGIMOS A LOS 5 SEGUNDOS
							setTimeout("location.href='"+location+"'", 5000);
						}
						
						if (data.status) {
							//TODO CORRECTO
							//SI ES NUEVO CLIENTE
							if ($('#emp_id').val() == '') {
								//MOSTRAMOS ALERTA
								bootbox.dialog({
									message: "<?php echo lang('empleado.nuevo.ok') ?>",
									buttons: {
										success: {
											label: "<?php echo lang('nuevo') ?>",
											className: "btn-success",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/empleados/nuevo_empleado';
												$(location).attr('href',url);
											}
										},
										main: {
											label: "<?php echo lang('empleados') ?>",
											className: "btn-primary",
											callback: function(result) {
												var url = '<?php echo base_url().$this->lang->lang() ?>/empleados';
												$(location).attr('href',url);
											}
										}
									}
								});
							} else {
							//SI ES UN CLIENTE EDITADO
								$('#perfil').attr("src","<?php echo isset($emp_id)?base_url().$this->lang->lang().'/empresa/view_file/'.$em_cif.'/emp/'.$emp_dni.'/perfil.jpg':base_url().$this->lang->lang().'/empresa/view_file/'.'perfil.jpg'; ?>"+"?timestamp=" + new Date().getTime());
								bootbox.alert('<?php echo lang("empleado.editado.ok") ?>');
							}
						} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									//SI EXSITE ERROR EN EL CAMPO FOTO ENTRAMOS POR AQUI
									if (data.errors[ind][0] == 'foto') {
										$('input[name='+data.errors[ind][0]+']').parent().parent().parent().parent().find('.text-danger').html(data.errors[ind][ind2]);
										$('input[name='+data.errors[ind][0]+']').parent().parent().parent().addClass("has-error has-feedback");
									}
									
									if (data.errors[ind][0] == 'emp_ver') {
										$('#ver div:last').html(data.errors[ind][ind2]);
									}
									//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
								}); 
							});
							
							if (data.clave == true) {
								$('#emp_clave_uno, #emp_clave_dos').val('');
							}
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
					l.stop();
				}
			}).submit();
		} else {
			$('#departamentos').next().next().html("<?php echo lang('requerido') ?>");
			$("#departamentos").parent().addClass("has-error has-feedback");
	    	$('#departamentos .glyphicon-remove').next().show();
	    	l.stop();
		}
	});
	
	/* FECHA NACIMIENTO */
	if (editar == 1) {		
		var meses = '';
		var dias = '';
		var primerDia = 0;
		if ('<?php echo $this->lang->lang() ?>' == 'es') {
			meses = ['Enero', 'Febrero', 'marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Nobiembre', 'Diciembre'];
			
			dias = ['D', 'L', 'M', 'X', 'J', 'V','S'];
			primerDia = 1;
		}
		
		$('#emp_nacimiento').daterangepicker({
			singleDatePicker: true,
			format: 'YYYY-MM-DD',
			language: '<?php echo $this->lang->lang() ?>',
			locale: {
	            applyLabel: '<?php echo lang("aplicar") ?>',
	            cancelLabel: '<?php echo lang("cancelar") ?>',
	            fromLabel: '<?php echo lang("desde") ?>',
	            toLabel: '<?php echo lang("hasta") ?>',
	            firstDay: primerDia,
	            daysOfWeek: dias,
	            monthNames: meses
	        },
	        showDropdowns: true,
            showWeekNumbers: true,
            endDate: moment(),
            minDate: moment().subtract(70, 'Years'),
            maxDate: moment().subtract(0, 'Years'),
		});
	}
	
	/* RECTRICCIONES DE TIPO DE EMPLEADOS */
	function tipos_empleados() {		
		switch(tipo) {
			case '0':
				$('#tipos .radio').show();
				break;
			case '1':
				$('#tipos .radio:eq(0)').hide();
				$('#tipos .radio:eq(1), #tipos .radio:eq(2), #tipos .radio:eq(3)').show();
				break;
			case '2':
				$('#tipos .radio:eq(0), #tipos .radio:eq(1)').parent().hide();
				$('#tipos .radio:eq(2), #tipos .radio:eq(3)').show();
				break;
			case '3':
				$('#tipos .radio:eq(0), #tipos .radio:eq(1), #tipos .radio:eq(2)').parent().hide();
				$('#tipos .radio:eq(3)').show();
				break;
			case '4':
				$('#tipos .radio:eq(0), #tipos .radio:eq(1), #tipos .radio:eq(2)').parent().hide();
				$('#tipos .radio:eq(3)').show();
				break;
		}	
	}
	
	tipos_empleados();
	
	function secciones_empleados() {
		var aux = $('#tipos label input:checked').val();
		$('#ver .checkbox label div').removeClass('checked');
		$('#ver .checkbox label input').prop('checked', false);
		
		switch(aux) {
			case '0':
				$('#ver .checkbox label div').addClass('checked');
				$('#ver .checkbox label input').prop('checked', true);
				//$('#ver .checkbox label div').attr('aria-checked',true);
				break;
			case '1':
				$('#ver .checkbox label div').addClass('checked');
				$('#ver .checkbox label input').prop('checked', true);
				break;
			case '2':
				$('#ver .checkbox:eq(8) label div').addClass('checked');
				$('#ver .checkbox:eq(5) label div').addClass('checked');
				$('#ver .checkbox:eq(9) label div').addClass('checked');
				$('#ver .checkbox:eq(10) label div').addClass('checked');
				$('#ver .checkbox:eq(2) label div').addClass('checked');
				
				$('#ver .checkbox:eq(8) label input').prop('checked', true);
				$('#ver .checkbox:eq(5) label input').prop('checked', true);
				$('#ver .checkbox:eq(9) label input').prop('checked', true);
				$('#ver .checkbox:eq(10) label input').prop('checked', true);
				$('#ver .checkbox:eq(2) label input').prop('checked', true);
				break;
			case '3':
				$('#ver .checkbox:eq(10) label div').addClass('checked');
				$('#ver .checkbox:eq(10) label input').prop('checked', true);
				break;
			case '4':
				$('#ver .checkbox:eq(10) label div').addClass('checked');
				$('#ver .checkbox:eq(10) label input').prop('checked', true);
				break;
		}
	}
	
	$('#tipos label, #tipos label *').click(function() {
		//secciones_empleados();
	});
	
	if (emp_id == '') {
		//secciones_empleados();
	}
	
	//HABILITAMOS LA VISION
	$('#btn_habilitar').click(function(e){
		e.preventDefault();
		var l = Ladda.create( document.querySelector( "#btn_habilitar" ) );
	 	l.start();
		
		$.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/empleados/estado",
		  	data: {'emp_id':emp_id},
		  	success: function(data) {
		  		if (data.sql) {
					if (!data.editar) {
						bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
						//REDIRIGIMOS A LOS 5 SEGUNDOS
						setTimeout("location.href='"+location+"'", 5000);
					} else {
						location.reload(true);
					}
		  		} else {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
				}
				l.stop();
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
	
	//ALERT CONFIRMANDO LA DESHABILITACION DEL ELEMENTO
	$('#btn_deshabilitar').click(function(e){
		e.preventDefault();
		bootbox.dialog({
			message: "<?php echo lang('confirmar.deshabilitar') ?>",
			buttons: {
				success: {
					label: "<?php echo lang('cancelar') ?>",
					className: "btn-default"
				},
				danger: {
					label: "<?php echo lang('deshabilitar') ?>",
					className: "btn-danger",
					callback: function(result) {
						var l = Ladda.create( document.querySelector( "#btn_deshabilitar" ) );
	 					l.start();
		    			
		    			$.ajax({
							type: "POST",
							dataType: "json",
						  	url: "<?php echo base_url().$this->lang->lang() ?>/empleados/estado",
						  	data: {'emp_id':emp_id},
						  	success: function(data) {
						  		if (data.sql) {
									if (!data.editar) {
										bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										setTimeout("location.href='"+location+"'", 5000);
									} else {
										location.reload(true);
									}
						  		} else {
									bootbox.alert('<?php echo lang("error.ajax") ?>');
								}
								l.stop();
						  	},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});
					}
				}
			}
		});
	});
	
	//SI LA SUCURSAL ESTA DESACTIVADA BLOQUEAMOS LOS CAMPOS
	var desactivado = '<?php echo isset($emp_estado)?$emp_estado:"" ?>';
	if (emp_id != '' && desactivado == 0) {
		$('#datos_empleado input, #datos_empleado select').attr('readonly', true);
		
		$('.datos2 .radio label div').attr('aria-disabled', true);
		$('.datos2 .radio label div').addClass('disabled');
		
		$('#ver .checkbox label div').attr('aria-disabled', true);
		$('#ver .checkbox label div').addClass('disabled');
		
		$('#btn_guardar').hide();
	}
});
</script>