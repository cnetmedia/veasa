<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                		<th><?php echo lang("empleados") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                		<th><?php echo lang("empleados") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO Y EL DIV DE EDITAR Y NUEVO
	$('form .form-group span').hide();
	
	//MOSTRAR DEPARTAMENTOS
	function mostrar_departamentos() {		
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/departamentos/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
					var table = '';
					$.each(data,function(indice,valor) {
						//PREGUNTAMOS SI ES GERENTE PARA MOSTRAR LOS DESHABILITADOS O NO Y LA CLASE DE COLOR
						var mostrar = false;
						var desact = '';
						var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						if (data[indice]['de_estado'] == 0) {
							if (tipo <= 1) {
								mostrar = true;
								desact = 'desactivado';
							}
						} else {
							mostrar = true;
						}
						
						//MOSTRAMOS TODOS LOS DATOS SI ES SUPERUSUARIO O SOLO AL QUE PERTENECE SI NO LO ES
						/*var su_id = '<?php echo $this->session->userdata("su_id") ?>';
						if (tipo == 0) {
							mostrar = true;
						} else if (su_id == data[indice]['su_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}*/
						
						if (mostrar) {
							table = table + '<tr id="'+data[indice]['de_id']+'" class="'+desact+'">';
							table = table + '<td>'+data[indice]['de_nombre']+'</td>';
	  						table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  						table = table + '<td>'+data[indice]['emp_cantidad']+'</td>';
							table = table + '</tr>';
						}
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 2, "asc"],[ 1, "asc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-sitemap'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
			        if (crear == '0') {
			        	$('#listar #btn_nuevo').hide();
			        }
					
					//NUEVO DEPARTAMENTO
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
					    $(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/departamentos/nuevo_departamento');
					});
					
					//REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					    window.location.reload(true);
					});
					
					//EDITAR O VER SUCURSAL
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click","tbody tr",function(e) {
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/departamentos/editar_departamento/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_departamentos();	
});
</script>











