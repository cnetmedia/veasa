<?php
$disable = '';
if ($this->session->userdata('emp_tipo') > 1 && $pe_estado >= 1 || $this->session->userdata('emp_tipo') > 1 && $pe_estado == 3) {
	$disable = 'disabled';
}
?>

<div class="row bottom30">
	<div class="col-lg-4 col-md-4">
		<strong><?php echo lang('entrada'); ?></strong>
		<p><?php echo $pe_id ?></p>		
		<strong><?php echo lang('mantenimiento') ?></strong>
		<p><?php echo $man_nombre ?></p>		
		<strong><?php echo lang('estado'); ?></strong>
		
		<?php if ($pe_estado == 0 ) { ?>
			<?php if($this->session->userdata('emp_tipo') == 2){ ?>
				<select class="form-control" id="pe_estado">
				<option value="0" <?php echo ($pe_estado == 0)?'selected':''; ?>><?php echo lang("pendiente"); ?></option>
				<option value="3" <?php echo ($pe_estado == 3)?'selected':''; ?>><?php echo lang("anulado"); ?></option>
			<?php }else{ ?>
				<select class="form-control" id="pe_estado" <?php echo ($this->session->userdata('emp_tipo') > 1 || empty($emp_id_aprobado_dept_prod) )?'disabled':''; ?>>
				<option value="0" <?php echo ($pe_estado == 0)?'selected':''; ?>><?php echo lang("pendiente"); ?></option>
				<option value="1" <?php echo ($pe_estado == 1)?'selected':''; ?>><?php echo lang("aprobado"); ?></option>
				<option value="3" <?php echo ($pe_estado == 3)?'selected':''; ?>><?php echo lang("anulado"); ?></option>
			<?php } ?>
		<?php } else { ?>
			<select class="form-control" id="pe_estado" <?php echo ($pe_estado == 4 || $this->session->userdata('emp_tipo') > 1 || empty($emp_id_aprobado_dept_prod))?'disabled':''; ?>>
			<option value="0" <?php echo ($pe_estado == 0)?'selected':''; ?>><?php echo lang("pendiente"); ?></option>
			<option value="1" <?php echo ($pe_estado == 1)?'selected':''; ?>><?php echo lang("aprobado"); ?></option>
			<option value="2" <?php echo ($pe_estado == 2)?'selected':''; ?>><?php echo lang("enviado"); ?></option>
			<option value="3" <?php echo ($pe_estado == 3)?'selected':''; ?>><?php echo lang("anulado"); ?></option>
			<option value="4" <?php echo ($pe_estado == 4)?'selected':''; ?>><?php echo lang("completado"); ?></option>
		<?php } ?>
		</select>
	</div>
	
	<div class="col-lg-4 col-md-4">
		<strong><?php echo lang('fecha.pedido'); ?></strong>
		<p><?php echo $pe_fecha; ?></p>
		
		<strong><?php echo lang('fecha.aprobado'); ?></strong>
		<p><?php echo $fecha_aprobacion; ?></p>
		
		<strong><?php echo lang('fecha.completado'); ?></strong>
		<p><?php echo $fecha_completado; ?></p>
	</div>
	
	<div class="col-lg-4 col-md-4">
		<strong><?php echo lang('empleado.pedido') ?>:</strong>
		<p><?php echo $empleado_solicito->emp_nombre.' '.$empleado_solicito->emp_apellido1.' '.$empleado_solicito->emp_apellido2; ?></p>
		
		
		<strong><?php echo lang('empleado.pedido.aprobado') ?>:</strong>
		<p><?php echo $empleado_aprobo->emp_nombre.' '.$empleado_aprobo->emp_apellido1.' '.$empleado_aprobo->emp_apellido2; ?></p>
		
		
		<strong><?php echo lang('empleados.pedido.recepcion') ?>:</strong>
		<p>
			<?php
			for ($e=0; $e<count($empleados_recepcion); $e++) {
				echo $empleados_recepcion[$e]->emp_nombre.' '.$empleados_recepcion[$e]->emp_apellido1.' '.$empleados_recepcion[$e]->emp_apellido2.'<br>';
			}
			?>
		</p>
		
		<strong><?php echo lang('empleados.registro') ?>:</strong>
		<p>
			<?php
			for ($e=0; $e<count($empleados_recepcion); $e++) {
				echo $empleados_recepcion[$e]->emp_nombre.' '.$empleados_recepcion[$e]->emp_apellido1.' '.$empleados_recepcion[$e]->emp_apellido2.'<br>';
			}
			?>
		</p>
	</div>

	
</div>

<script>
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
return ((key >= 48 && key <= 57) || (key==8) || (key == 46));

}
</script>

<?php $total_pedido = 0; ?>

<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<?php if ($pe_estado == 0 || $pe_estado == 3) { ?>
                		<th class="col-md-1"><?php echo lang("eliminar.producto") ?></th>
                		<?php } else { ?>
                		<th class="col-md-1"><?php echo lang("producto.recibido") ?></th>
                		<?php } ?>
                		
                		<th class="col-md-1"><?php echo lang("referencia") ?></th>
                		<th><?php echo lang("descripcion") ?></th>
                		<th><?php echo lang("caracteristicas") ?></th>
                		<th><?php echo lang("proveedor") ?></th>
                		<th class="col-md-1"><?php echo lang("precio").' '.$moneda ?></th>
                		<th class="col-md-2"><?php echo lang("cantidad.pedida") ?></th>
                		<?php if ($pe_estado != 0) { ?>
                		<th class="col-md-2"><?php echo lang("cantidad.recibida") ?></th>
                		<?php } ?>
                		<th class="col-md-1 text-right"><?php echo lang("sub.total").' '.$moneda ?></th>
					</tr>
				</thead>
				
				<tbody id="lista_carrito" aria-relevant="all" aria-live="polite" role="alert">
				<?php for ($i=0; $i<count($productos); $i++) { ?>
					<tr product_id="<?php echo $productos[$i]->product_id; ?>" pro_id="<?php echo $productos[$i]->pro_id; ?>" cate_id="<?php echo $productos[$i]->cate_id; ?>">
						
						<td class="vcenter text-center">
						<?php if ($pe_estado == 0 || $pe_estado == 3) { ?>
					  		<i class="fa fa-times eliminar"></i>
					  	<?php } else { ?>
					  		<?php if ($productos[$i]->product_cantidad_recibido == $productos[$i]->cantidad) { ?>
					  			<div class="checkbox">
									<label>
										<input type="checkbox" name="product" disabled checked="checked" />
						   			</label>
						   		</div>
					  		<?php } else { ?>
					  			<div class="checkbox">
									<label>
										<input type="checkbox" name="product" <?php echo ($this->session->userdata('emp_editar') == 0)?'disabled':''; ?> />
						   			</label>
						   		</div>
					  		<?php } ?>
					  	<?php } ?>
					  	</td>
					  
					  	<td class="vcenter">
					  		<?php echo $productos[$i]->product_referencia; ?>
					  	</td>
					  	
					  	<td class="vcenter">
					    	<?php echo $productos[$i]->product_nombre; ?>
					    </td>
					    
					    <td class="vcenter">
					  	<!-- RECOGEMOS LAS OPCIONES DEL PRODUCTO -->
						<?php
						$opciones = $productos[$i]->product_caracteristicas;
						$opciones = explode(';',$opciones);
						$opciones = array_filter($opciones);
						
						$caracteristicas = '';
						for ($y=0; $y<count($opciones); $y++) {
							$aux = explode('/',$opciones[$y]);
							$aux = array_filter($aux);
							
							$caracteristicas = $caracteristicas.'<span class="opc_product_list"><strong>'.$aux[0].':</strong> <span>'.$aux[1].'</span></span>';
						}
						echo $caracteristicas;
						?>
					  	</td>
					  	
					  	<td class="vcenter">
					  		<?php echo $productos[$i]->pro_nombre; ?>
					  	</td>
					  	
					  	<td class="vcenter">
					  		<?php echo $productos[$i]->product_precio; ?>
					  	</td>
					  	
					    <td>
					    	<div class="form-group"> 
					    		<input class="form-control text-center" type="text" name="cantidad" value="<?php echo $productos[$i]->cantidad ?>" onKeypress="return soloNumeros(event)" <?php echo ($this->session->userdata("emp_tipo") > 1 && $pe_estado >= 1)?'disabled':''; ?> />
					    		<span class="glyphicon glyphicon-remove form-control-feedback"></span>
                			</div>
					    </td>
					    
					    <?php if ($pe_estado != 0) { ?>
                			<td>
                				<div class="input-group">
                				<?php if ($productos[$i]->product_cantidad_recibido == $productos[$i]->cantidad) { ?>
                					<input disabled name="recibido" onkeypress="return soloNumeros(event)" class=" text-center form-control" value="" type="text"><span class="input-group-addon" title="<?php echo $productos[$i]->product_cantidad_recibido.' '.lang('recibidos') ?>"><i class="fa fa-archive"></i> <span class="cantidad_recibido"><?php echo $productos[$i]->product_cantidad_recibido ?></span></span>
					  			<?php } else { ?>
					  				<input name="recibido" onkeypress="return soloNumeros(event)" class=" text-center form-control" value="" type="text"><span class="input-group-addon" title="<?php echo $productos[$i]->product_cantidad_recibido.' '.lang('recibidos') ?>"><i class="fa fa-archive"></i> <span class="cantidad_recibido"><?php echo $productos[$i]->product_cantidad_recibido ?></span></span>
					  			<?php } ?>
					  			</div>
                			</td>
                		<?php } ?>
					    
					    <td class="text-right vcenter">
					    	<?php
					    		$total_fila = $productos[$i]->product_precio * $productos[$i]->cantidad;
					    		$total_pedido = $total_pedido + $total_fila;
					    		echo $total_fila;
					    	?>
					    </td>
					</tr>
				<?php } ?>
				</tbody>
				
				<tfoot>
					<tr>
					  <td colspan="<?php echo ($pe_estado != 0)?'8':'7'; ?>" class="text-right"><strong><?php echo lang("total") ?></strong></td>
					  <td class="text-right"><strong><?php echo $total_pedido; ?></strong></td>
					</tr>
					<tr>
					  <td colspan="<?php echo ($pe_estado != 0)?'8':'7'; ?>" class="text-right"><strong><?php echo lang("diferencia.cotizacion") ?></strong></td>
					  <td class="text-right"><strong><?php echo 0; ?></strong></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<?php if ($pe_estado == 0 || $pe_estado == 3) { ?>
<div class="row">
    <div class="col-lg-6 col-md-6  text-left">		
    	<button id="agregar_producto" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-plus"></i> <?php echo lang('agregar.producto') ?></button>
    </div>
</div> 
<?php } ?>

<div class="row bottom30">
	<div class="col-lg-6 col-md-6 col-md-offset-6 text-right">
		<label><?php echo lang('presupuesto') ?></label>
	</div>
	
	<div class="col-lg-6 col-md-6 col-md-offset-6 text-right">
		<select class="selectpicker form-control" data-live-search="true" id="cotizaciones" <?php echo ($this->session->userdata('emp_tipo') > 1 && $pe_estado >= 1)?'disabled':''; ?>>
		</select>
	</div>
</div>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<label><?php echo lang('observaciones.empresa') ?></label>
		<textarea class="form-control" id="observaciones_empresa" <?php echo $disable ?>><?php echo $pe_observaciones_empresa ?></textarea>
	</div>
	
	<div class="col-lg-6 col-md-6">
		<label><?php echo lang('observaciones.proveedor') ?></label>
		<textarea class="form-control" id="observaciones_proveedor" <?php echo ($this->session->userdata('emp_tipo') > 1 && $pe_estado >= 1)?'disabled':''; ?>><?php echo $pe_observaciones_proveedor ?></textarea>
	</div>
</div>

<?php if ($pe_estado != 0 ) { ?>
<div class="row">
	<div class="col-lg-6 col-md-6" id="file-adjunto">
		<label><?php echo lang('adjunto.carrito') ?></label>
		<div style="margin:5px 0; border: 1px solid lightgray;padding: 5px;">			
			<?php 
				$archivos = explode('|', $pe_adjunto1); 
				$i = 1;
				foreach ($archivos as $key => $file) {
					if(strlen($file) > 0 ){					
						echo '<div style="padding: 5px;width:100%;" id="file_'.$i.'" >' .
								'<a target="_blank" href="'.base_url().'img/dynamic/adjuntos-carrito/'.$file . '">'.$file .'</a>' .
							'</div>';
							$i++;
					}
				}
			?>
		</div>
	</div>
</div>
<?php }else{ ?>
<div class="row">
	<div class="col-lg-6 col-xs-12 fot">
		<label><?php echo lang('adjunto.carrito') ?></label>
		<div>
			<form class="fotos" action="<?php echo base_url().$this->lang->lang() ?>/proveedores/upload" method="post" enctype="multipart/form-data">
			<div class="input-group">
				<span class="input-group-btn">
					<span class="btn btn-primary btn-file">
						<i class="fa fa-file"></i> <?php echo lang('cargar.adjunto.carrito') ?>
						<input type="file" id="file-adjunto" name="archivos[]" multiple="multiple">
					</span>
				</span>
				<input type="text" class="form-control" readonly>
			</div>
			</form>
		</div>
		<div class="text-danger"><?php echo form_error("fotos") ?></div>
		<div id="file-adjunto" style="margin-top: 5px;border: 1px solid lightgray;padding: 5px;">
			<?php 
				$archivos = explode('|', $pe_adjunto1); 
				$i = 1;
				foreach ($archivos as $key => $file) {
					if(strlen($file) > 0 ){					
						echo '<div style="padding: 5px;width:100%;" id="file_'.$i.'" >' .
								'<i class="fa fa-times pointer" file_name="'.$file.'" file_id="file_'.$i.'" style="margin-right: 5px;"></i>' .
								'<a target="_blank" href="'.base_url().'img/dynamic/adjuntos-carrito/'.$file . '">'.$file .'</a>' .
							'</div>';
							$i++;
					}
				}
			?>
		</div>
	</div>
</div>
<?php } ?>

<div class="col-lg-12 col-md-12" style="padding: 0;  margin: 2% 0;">
	<div class="col-lg-12 col-md-12" style="border: 1px solid #dfdfdf; padding: 3px 0;">
		<div class="col-lg-4 col-md-4">
			<div class="checkbox">
				<label>
					<input id="fechaProd" type="checkbox" value=""  <?php echo ( $this->session->userdata('emp_tipo') == '0' || ($this->session->userdata('emp_tipo') == '2' && $this->session->userdata('de_id') == '2')) ?'':'disabled'; ?> <?php echo ($fecha_aprobado_dept_prod)?'checked':''; ?>>
					<?php echo lang('aprobada.produccion') ?>
				</label>
			</div>
		</div>
		<div class="col-lg-4 col-md-4">
			<strong><?php echo lang('fecha.aprobado.produccion'); ?></strong>
			<p><?php echo $fecha_aprobado_dept_prod; ?></p>
		</div>
		<div class="col-lg-4 col-md-4">
			<strong><?php echo lang('empleado.pedido.aprobado.produccion'); ?>:</strong>
			<p><?php echo $empleado_aprobo_dept_prod->emp_nombre.' '.$empleado_aprobo_dept_prod->emp_apellido1.' '.$empleado_aprobo_dept_prod->emp_apellido2; ?></p>
		</div>
	</div>
</div>

<div class="row" >
	<div class="col-lg-12 text-right" style="margin-top:15px;">
		<?php if ($this->session->userdata('emp_tipo') <= 2 && $pe_estado < 1) { ?>
		<button id="recalcular_entrada" type="button" class="btn btn-default ladda-button" data-style="zoom-out"><i class="fa fa-calculator"></i> <?php echo lang('recalcular.entrada') ?></button>
		<?php } ?>
		
		<?php if (($this->session->userdata('emp_tipo') <= 1) || (($this->session->userdata('de_id') != '3') && ($this->session->userdata('de_id') != '4'))) { ?>
		
			<?php if ($this->session->userdata('emp_tipo') <= 2 && $pe_estado < 1) { ?>
			<button id="enviar_email" type="button" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-envelope-o"></i> <?php echo lang('email.proveedor') ?></button>
			<?php } ?>

			<?php if(isset($_GET['tag'])){ 
            	$solicitud = true;
            } else{
            	$solicitud = false;
            } ?>

			
			<?php if (($this->session->userdata('emp_tipo') < 1 && $pe_estado <= 1) || 
				($this->session->userdata('emp_tipo') > 1 && $pe_estado > 0) || 
				($this->session->userdata('emp_tipo') == 2 && $this->session->userdata('de_id') == '2') || 
				($this->session->userdata('emp_tipo') == 1 && $pe_estado == 0 && $solicitud) ||
				($this->session->userdata('emp_tipo') == 1 && $pe_estado == 1 && !$solicitud) ) { ?>

				<?php if(empty($emp_id_aprobado_dept_prod) && $pe_estado < 1 ){ ?>
					<button id="check_entrada" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-check"></i> <?php echo lang('aprobar.entrada') ?></button>
				<?php } ?>

	
				<button id="actualizar_entrada" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-refresh"></i> <?php echo lang('actualizar.entrada') ?></button>		
				

			<?php } ?>

			<?php if ($pe_estado == 1) { ?>
			<button id="almacenar_entrada" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-archive"></i> <?php echo lang('almacenar.entrada') ?></button>
			<?php } ?>
		
		<?php } ?>
		
		<a href="<?php echo base_url().$this->lang->lang().'/almacenes/entradas' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
	</div>
</div>
			
<script>
var files_delete = ''; 
/* ESTILO Y BOTON ADJUNTOS */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function(){

	$(document).on('click', '#file-adjunto .pointer', function() {
		console.log($(this).attr('file_id'));
		files_delete = files_delete + $(this).attr('file_name') + ',';
		$('#'+$(this).attr('file_id')).remove();	
	});

	/* ESTILO Y BOTON ADJUNTOS */
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
		var input = $(this).parents('.input-group').find(':text'),
			log = numFiles > 1 ? numFiles + ' archivos seleccionados' : label;
			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}	
	});

	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	$("#example1 .form-group").removeClass("has-error has-feedback");
	$('#example1 .glyphicon-remove').hide();
	
	//RELLENAMOS EL CAMPO DE MANTENIMIENTOS
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/presupuestos/buscador",
	{'buscar':''},
	function(data){
		<?php if ($pr_id == '0') { ?>
			$('#cotizaciones').append('<option value="0" selected>Para stock de inventario</option>');
		<?php } else { if ($pr_id == '') { ?>
			$('#cotizaciones').append('<option value="" selected>Seleccione</option>');
			$('#cotizaciones').append('<option value="0" selected>Para stock de inventario</option>');
		<?php } } ?>
		$.each(data,function(indice) {
			if (data[indice]["pr_estado"] != '3') {
				if (data[indice]["pr_id"] == '<?php echo $pr_id ?>') {
					$('#cotizaciones').append('<option value="'+data[indice]["pr_id"]+'" selected>'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>');
				} else {
					$('#cotizaciones').append('<option value="'+data[indice]["pr_id"]+'">'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>');
				}
			}
		});
		
		$('#cotizaciones').selectpicker();
	    $('#cotizaciones').selectpicker('refresh');
	}, "json");
	
	
	//CREAR ENTRADAS MANUALES
	function enviar_entrada_manual(datos) {
		
		var opciones = '';
		var opc = [];
		
		if(datos['product_caracteristicas'] != '') {
	
			var txt = datos['product_caracteristicas'],
				list = txt.split(";");
			
			for(var i=0; i<list.length-1; i++){
				opc = list[i].split("/");
				opciones = opciones+'<div class="opc_product_list"><strong>'+opc[0]+':</strong> <span>'+opc[1]+'</span></div>';
			}
			
		} else {
			
			opciones = datos['product_caracteristicas'];
			
		}
		
		var row_producto = '<tr product_id="'+datos['product_id']+'" pro_id="'+datos['pro_id']+ '" cate_id="'+datos['cate_id']+'">' +
							'<td class="vcenter text-center"><i class="fa fa-times eliminar"></i></td>' +
							'<td class="vcenter">'+datos['product_referencia']+'</td>' +
							'<td class="vcenter">'+datos['product_nombre']+'</td>' +
							'<td class="vcenter">'+opciones+
								'<input type="hidden" name="product_caracteristicas" id="product_caracteristicas" value="'+datos['product_caracteristicas']+'">'+ 
							'</td>' +
							'<td class="vcenter">'+datos['pro_nombre']+'</td>' +
							'<td class="vcenter">'+parseFloat(datos['product_precio']).toFixed(2)+'</td>' +
							'<td><div class="form-group">' +
									'<input class="form-control text-center" type="text" name="cantidad" value="'+parseFloat(datos['product_cantidad']).toFixed(2)+'" onKeypress="return soloNumeros(event)">' +
							'</div></td>' +
							'<td class="text-right vcenter">'+parseFloat(parseFloat(datos['product_precio'])*parseFloat(datos['product_cantidad'])).toFixed(2)+'</td>' +
							'</tr>';
		
		$('#lista_carrito').append(row_producto);
		
		//actualizar_carrito();
		calcular();
	}
	
	
	//AGREGAR PRODUCTOS
	$("#agregar_producto").click(function(e){
		e.preventDefault();
		
		//RELLENAMOS EL CAMPO DE PRODUCTOS
		$.post(
		"<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador_productos",
		{'buscar':''},
		function(data){
			
				var materiales = '<option value="" selected> </option>';
				var datos = {
					'product_id' : '',
					'product_cantidad' : '',
					'product_nombre' : '',
					'product_referencia' : '',
					'product_precio' : '',
					'product_caracteristicas' : '',
					'pro_nombre' : '',
					'pro_id' : '',
					'cate_id' : '',
				};
				
				ids = [];
				
				$('#lista_carrito tr').each(function(){
					var product_id = $(this).attr('product_id');
					ids.push(product_id);
				});

				$.each(data,function(indice) {
					if(ids.indexOf(data[indice]["product_id"])<0) {
						String.prototype.replaceArray = function(find, replace) {
						  var replaceString = this;
						  var regex; 
						  for (var i = 0; i < find.length; i++) {
							regex = new RegExp(find[i], "g");
							replaceString = replaceString.replace(regex, replace[i]);
						  }
						  return replaceString;
						};
						var caracteristicas = data[indice]["product_caracteristicas"];
						var find = [";", "/",];
						var replace = [" | ", ": ",];
						caracteristicas = caracteristicas.replaceArray(find, replace);
						materiales = materiales + '<option value="'+data[indice]["product_id"]+'" cateid="'+data[indice]["cate_id"]+'" proid="'+data[indice]["pro_id"]+'" pro_nombre="'+data[indice]["pro_nombre"]+'" ref="'+data[indice]["product_referencia"]+'" price="'+data[indice]["product_precio"]+'" options="'+data[indice]["product_caracteristicas"]+'" name="'+data[indice]["product_nombre"]+'" >'+data[indice]["product_nombre"]+' ('+data[indice]["product_referencia"]+') - '+caracteristicas+'</option>';	
					}
				});
				
				bootbox.dialog({
					message: "<div id='new_entrada'><h3><?php echo lang('productos') ?></h3><select id='productos' class='selectpicker form-control' data-live-search='true'>"+materiales+"</select></div><script>$(document).ready(function(){$('#new_entrada #productos').selectpicker();});<\/script>",
					buttons: {
							success: {
									label: "<?php echo lang('seleccionar') ?>",
									className: "btn-success",
									callback: function() {
										
										datos['product_id'] = $('#new_entrada #productos option:selected').val();
										
										//PRODUCTO ENCONTRADO SOLO AÑADIMOS LA CANTIDAD
										if (datos['product_id']  != '') {
											datos['product_precio'] = $('#new_entrada #productos option:selected').attr('price');
											datos['product_caracteristicas'] = $('#new_entrada #productos option:selected').attr('options');
											datos['product_nombre'] = $('#new_entrada #productos option:selected').attr('name');
											datos['product_referencia'] = $('#new_entrada #productos option:selected').attr('ref');
											datos['pro_nombre'] = $('#new_entrada #productos option:selected').attr('pro_nombre');
											datos['pro_id'] = $('#new_entrada #productos option:selected').attr('proid');
											datos['cate_id'] = $('#new_entrada #productos option:selected').attr('cateid');

											bootbox.dialog({
												message: "<div id='new_entrada'><label><?php echo lang('cantidad') ?></label><input type='text' name='product_cantidad' id='product_cantidad' value='' class='form-control'><label></div>",
												buttons: {
														success: {
																label: "<?php echo lang('guardar') ?>",
																className: "btn-success",
																callback: function() {
																   datos['product_cantidad'] = $('#new_entrada #product_cantidad').val();
																   	enviar_entrada_manual(datos);
																}
														},
														cancel: {
																label: "<?php echo lang('cancelar') ?>",
																className: "btn-default"
														}
												}
											});
										}
									}
							},
							cancel: {
									label: "<?php echo lang('cancelar') ?>",
									className: "btn-default"
							}
					}
				});
				
				
		}, "json");
	});
	
	
	//BORRAR PRODUCTO
	$('#example1 .eliminar').click(function(e){
		e.preventDefault();
		var emp_tipo = '<?php echo $this->session->userdata("emp_tipo"); ?>';
		
		if (emp_tipo <= 2) {
			
			var obj = $(this).parent().parent();
			
			bootbox.dialog({
				title: "<?php echo lang('eliminar.producto') ?>",
				message: "<?php echo lang('confirmar.eliminar.producto') ?>",
				buttons: {
					main: {
						label: "<?php echo lang('cancelar') ?>",
						className: "btn-default"
					},
					success: {
						label: "<?php echo lang('eliminar') ?>",
						className: "btn-danger",
						callback: function() {
							obj.remove();
						}
					}
				}
			});
		}
	});
	
	//RECALCULAR TOTALES
	function calcular() {
		var total = 0;
		
		$('#example1 tbody tr').each(function(){
	 		var suma = 0;
	 		suma = parseFloat($(this).find('input[name="cantidad"]').val()) * parseFloat($(this).find('input[name="cantidad"]').parent().parent().prev().html());
	 		$(this).find('td:last').html(suma.toFixed(2));
	 		total = total + suma;
	 	});
	 	
	 	$('#example1 tfoot td:last').html(total.toFixed(2));
	}
	
	$('#recalcular_entrada').click(function(e){
		e.preventDefault();
		
		//LOADING BOTON
		l = Ladda.create(document.querySelector("#recalcular_entrada"));
	 	l.start();
	 	calcular();
	 	l.stop();
	});
	
	//ACTUALIZAR ENTRADA
	//DATOS ENTRADA
	var pe_id = '<?php echo $pe_id ?>';
	var product_id = '';
	var product_cantidad = '';
	var product_cantidad_recibido = '';
	var product_cantidad_recibido_almacen = '<?php echo isset($product_cantidad_recibido_almacen)?$product_cantidad_recibido_almacen:"" ?>';
	var cantidad_recibido_total = '';
	var pe_observaciones_empresa = '';
	var pe_observaciones_proveedor = '';
	var product_id_check = '';
	var pr_id = '<?php echo $pr_id ?>';
	var pe_estado = '<?php echo $pe_estado ?>';
	var al_id = '';
	var l = '';
	var emp_id_recepcion = '<?php echo isset($emp_id_recepcion)?$emp_id_recepcion:""; ?>';
	var emp_id_aprobado = '<?php echo isset($emp_id_aprobado)?$emp_id_aprobado:""; ?>';
	var fecha_aprobacion = '<?php echo isset($fecha_aprobacion)?$fecha_aprobacion:""; ?>';
	var emp_id_aprobado_dept_prod = '<?php echo isset($emp_id_aprobado_dept_prod)?$emp_id_aprobado_dept_prod :""; ?>';
	var fecha_aprobado_dept_prod = '<?php echo isset($fecha_aprobado_dept_prod)?$fecha_aprobado_dept_prod:""; ?>';
	var emp_id_autoriza = '<?php echo isset($emp_id_autoriza)?$emp_id_autoriza :""; ?>';
	var fecha_autorizacion = '<?php echo isset($fecha_autorizacion)?$fecha_autorizacion:""; ?>';
	
	function pre_guardar(con_auto, emp_autoriza){
		var auto = false;

		if(con_auto){
			auto = con_auto;
		}else{
			if(!emp_id_autoriza){
				pe_estado = 0;
			}
		}

		if(auto){	//no tengo claro lo del estado		
			emp_id_autoriza = emp_autoriza;
			fecha_autorizacion = '<?php echo fechaHora_actual($this->session->userdata("su_zona_horaria")) ?>';
		}

		guardar();
	}

	//ENVIAR DATOS PARA ACTUALIZAR ENTRADA
	function guardar(con_check) {
		var check = false;
		
		if(con_check){
			check = con_check;
		}
		//GUARDAMOS FECHA Y EMPLEADO AL APROBAR ENTRADA
		if (pe_estado == 1 && emp_id_aprobado == 0) {
			emp_id_aprobado = '<?php echo $this->session->userdata("emp_id") ?>';
			fecha_aprobacion = '<?php echo fechaHora_actual($this->session->userdata("su_zona_horaria")) ?>';	
		}

		if(check && pe_estado == 0){			
			if((!emp_id_aprobado_dept_prod || emp_id_aprobado_dept_prod == 0) && $('#fechaProd').is(':checked')){
				emp_id_aprobado_dept_prod = '<?php echo $this->session->userdata("emp_id") ?>';
				fecha_aprobado_dept_prod = '<?php echo fechaHora_actual($this->session->userdata("su_zona_horaria")) ?>';
			}
		}

		//GUARDAR EMPLEADOS QUE RECIBEN EL MATERIAL
		if (product_id_check != '') {
			var aux = emp_id_recepcion.split(';');
			aux = aux.filter(Boolean);
			var emp = '<?php echo $this->session->userdata("emp_id") ?>';
			
			if ($.inArray(emp, aux) == -1) {
				emp_id_recepcion = emp_id_recepcion + emp + ';';
			}
		}
		
		//VERIFICAMOS SI HAY COTIZACION SELECCIONADA
		if($('#cotizaciones option:selected').val()) {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/actualizar_entrada",
				data: {'pe_id':pe_id, 'product_id':product_id, 'product_cantidad':product_cantidad, 'pe_observaciones_empresa':pe_observaciones_empresa,  'pe_observaciones_proveedor':pe_observaciones_proveedor, 'pr_id':pr_id, 'pe_estado':pe_estado, 'al_id':al_id, 'product_cantidad_recibido':product_cantidad_recibido, 'cantidad_recibido_total':cantidad_recibido_total, 'product_id_check':product_id_check,'emp_id_aprobado':emp_id_aprobado,'fecha_aprobacion':fecha_aprobacion,'emp_id_recepcion':emp_id_recepcion,'product_cantidad_recibido_almacen':product_cantidad_recibido_almacen,'emp_id_aprobado_dept_prod':emp_id_aprobado_dept_prod,'fecha_aprobado_dept_prod':fecha_aprobado_dept_prod, 'emp_id_autoriza':emp_id_autoriza, 'fecha_autorizacion':fecha_autorizacion},
				success: function(data) {
					if (data.status) {					
						bootbox.alert('<?php echo lang("entrada.editado.ok") ?>');
						if (pe_estado == 1) {
							$('#enviar_email').click();
						}
						l.stop();

						var files = $("#file-adjunto").get(0).files;
				        var fileData = new FormData();

				        if(files && files.length > 0){		        	
					        for (var i = 0; i < files.length; i++) {
					            fileData.append("form"+i+"-"+<?php echo $pe_id; ?>, files[i]);
					        }

					        fileData.append("eliminados",files_delete);
					        fileData.append("adjuntos_iniciales","<?php echo $car_adjuntos; ?>");

					        $.ajax({
					            type: "POST",
					            url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/upload_adjuntos",
					            dataType: "json",
					            contentType: false, // Not to set any content header
					            processData: false, // Not to process data
					            data: fileData,
					            success: function (result, status, xhr) {
									if (data) {
										//refrescar sin cache
										location.reload(true);
									}		                
					            },
					            error: function (xhr, status, error) {
					                alert(status + ' ' + error);
					            }
					        });
				        }else{
							if (data) {
								//refrescar sin cache
								location.reload(true);
							}
						}

					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
						l.stop();
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
					l.stop();
				}
			});
		} else {
			bootbox.alert('<?php echo lang("error.carrito.cotizaciones") ?>');
			l.stop();
		}
	}
	
	//RECOGEMOS LOS DATOS MODIFICADOS PARA MODIFICAR LA ENTRADA INCLUYENDO CHECK
	$('#check_entrada').click(function(e){
		e.preventDefault();
		
		//LOADING BOTON
		l = Ladda.create(document.querySelector("#check_entrada"));
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
		$("#example1 .form-group").removeClass("has-error has-feedback");
		$('#example1 .glyphicon-remove').hide();
	 	
	 	calcular();
	 	
	 	var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	 	if (emp_tipo <= 2 ) {
			pe_estado = $('#pe_estado option:selected').val();
		}
	 	
	 	pr_id = $('#cotizaciones option:selected').val();
		product_id = '';
		product_cantidad = '';
		product_cantidad_recibido = '';
		pe_observaciones_empresa = $.trim($('#observaciones_empresa').val());
		pe_observaciones_proveedor = $.trim($('#observaciones_proveedor').val());
		
		var errores = 0;
		
		$('#example1 tbody tr').each(function(){
			var cantidad = $.trim($(this).find('input[name="cantidad"]').val());
			if (cantidad != '') {
				product_cantidad = product_cantidad + cantidad + ';';
				product_cantidad_recibido = product_cantidad_recibido + '0;';
				cantidad_recibido_total = cantidad_recibido_total + '0;';
			} else {
				errores = errores + 1;
				$(this).find('input[name="cantidad"]').parent().addClass("has-error has-feedback");
				$(this).find('input[name="cantidad"]').parent().find('.glyphicon-remove').show();
			}	
			
			product_id = product_id + parseInt($(this).attr('product_id')) + ';';
		});
	 	
	 	if (errores == 0) {
			guardar(true);
		}
	});

	

	function autorizacion( errors ){
		var name = null;
		var valor = null;

		if(errors){
			name = errors[0][0];
			valor = errors[0][1];	
		}

		var formulario = "<form id='form_autorizacion'>";
			
		if(name && (name == 'emp_dni'|| name == 'emp_tipo')){
			formulario += "<div class='form-group has-error has-feedback'>";
		}else{
			formulario += "<div class='form-group'>";
		}

		formulario += "<label><?php echo lang('dni') ?></label>"+
					"<input type='text' name='emp_dni' id='emp_dni' value='' class='form-control'>";

		if(name && name == 'emp_dni'){
			formulario += "<span class='glyphicon glyphicon-remove form-control-feedback'></span>"+
			"<div class='text-danger'>"+ valor +"</div></div>";
		}else{
			formulario += "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>"+
			"<div class='text-danger'></div></div>";
		}
		
		if(name && (name == 'emp_clave' || name == 'emp_tipo')){
			formulario += "<div class='form-group has-error has-feedback'>";
		}else{
			formulario += "<div class='form-group'>";
		}			
					
		formulario += "<label><?php echo lang('login.clave') ?></label>"+
					"<input type='password' name='emp_clave' id='emp_clave' value='' class='form-control'>";
					
		if(name && name == 'emp_clave'){
			formulario += "<span class='glyphicon glyphicon-remove form-control-feedback'></span>"+
			"<div class='text-danger'>"+ valor +"</div>";
		}else{
			formulario += "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>"+
			"<div class='text-danger'></div>";
		}
		formulario += "</div>";

		if(name && name == 'emp_tipo'){
			formulario += "<div class='text-danger'>"+ valor +"</div>";
		}

		formulario += "</form>";

		bootbox.dialog({
			closeButton: false,
			message: "<div id='autorizacion'><h3>Autorización</h3>"+formulario+"</div>",
			buttons: {
				ok: {
					label: "<?php echo lang('guardar-sin-autorizacion') ?>",
					className: "btn-primary",
					callback: function() {
					    pre_guardar(false);
				success: {
					label: "<?php echo lang('guardar') ?>",
					className: "btn-success",
					callback: function() {
					    
					    var datos_emp = {
							'emp_dni' : $("#form_autorizacion #emp_dni").val(),
							'emp_clave' : Aes.Ctr.encrypt($("#form_autorizacion #emp_clave").val(), '123456', 256),
							'token' : '123456',
						};
					    
					    //INICIO PETICION AJAX
					    $.ajax({
							type: "POST",
							dataType: "json",
							url: "<?php echo base_url().$this->lang->lang() ?>/empleados/validar_solicitud",
							data: datos_emp,
							success: function(data) {
								console.log(data);
								if (data.status == false) {
									autorizacion(data.errors);
								} else {
									// guardar con autorizacion
									pre_guardar(true,data.emp_id);
								}
							}
						});
					}
				},
				cancel: {
					label: "<?php echo lang('cancelar') ?>",
					className: "btn-default",
					callback: function(){
						l.stop();
					}
				}
			}
		});
	}

	//RECOGEMOS LOS DATOS MODIFICADOS PARA MODIFICAR LA ENTRADA
	$('#actualizar_entrada').click(function(e){
		e.preventDefault();
		
		//LOADING BOTON
		l = Ladda.create(document.querySelector("#actualizar_entrada"));
	 	l.start();
	 	
	 	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
		$("#example1 .form-group").removeClass("has-error has-feedback");
		$('#example1 .glyphicon-remove').hide();
	 	
	 	calcular();
	 	
	 	var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	 	if (emp_tipo <= 2 ) {
			pe_estado = $('#pe_estado option:selected').val();
		}
	 	
	 	pr_id = $('#cotizaciones option:selected').val();
		product_id = '';
		product_cantidad = '';
		product_cantidad_recibido = '';
		pe_observaciones_empresa = $.trim($('#observaciones_empresa').val());
		pe_observaciones_proveedor = $.trim($('#observaciones_proveedor').val());
		
		var errores = 0;
		var errores_presu = 0;
		var man_id = "<?php echo $man_id ?>";
		
		if(man_id){
			$.ajax({
				type: "POST",
				url: "<?php echo base_url().$this->lang->lang() ?>/mantenimientos/get_presupuesto/<?php echo $man_id ?>",
				data: "pr_id="+pr_id,
				success: function (result, status, xhr) {
					var myObjStr = JSON.parse(result);

					var facturas = myObjStr[1];
					var solicitudes = myObjStr[0];
					var pedido = [];

					$('#example1 tbody tr').each(function(){
						var cantidad = $.trim($(this).find('input[name="cantidad"]').val());
						var precio = $.trim($(this).find('input[name="cantidad"]').parent().parent().prev().html());
						var cate_id = $(this).attr('cate_id');

						if (cantidad != '') {
							product_cantidad = product_cantidad + cantidad + ';';
							product_cantidad_recibido = product_cantidad_recibido + '0;';
							cantidad_recibido_total = cantidad_recibido_total + '0;';
							
							if(pedido[cate_id]){
								pedido[cate_id] = pedido[cate_id] + (parseFloat(cantidad) * parseFloat(precio));
							}else{
								pedido[cate_id] = parseFloat(cantidad) * parseFloat(precio);
							}							
						} else {
							errores = errores + 1;
							$(this).find('input[name="cantidad"]').parent().addClass("has-error has-feedback");
							$(this).find('input[name="cantidad"]').parent().find('.glyphicon-remove').show();
						}	
						
						product_id = product_id + parseInt($(this).attr('product_id')) + ';';
					});
				 	
					console.log('facturas',facturas);
					console.log('solicitudes',solicitudes);
					console.log('pedido',pedido);

				 	if (errores == 0) {
				 		pedido.forEach(function(item, i) {
				 		  var solicitud = 0;
				 		  if(parseFloat(solicitudes[i])){
				 		  	solicitud = parseFloat(solicitudes[i])
				 		  }

						  var presupuesto = parseFloat(facturas[i]) - solicitud;
						  if(item > presupuesto){
						  	errores_presu++;
						  }
						});

						if( errores_presu > 0 && !emp_id_autoriza && pe_estado == 1){
							autorizacion();
							//alert("autorizacion");
						}else{
							guardar();
							//alert("guardar");
						}
					}

				},
				error: function (xhr, status, error) {
				    alert(status + ' ' + error);
				}
			});
		}else{
			$('#example1 tbody tr').each(function(){
				var cantidad = $.trim($(this).find('input[name="cantidad"]').val());

				if (cantidad != '') {
					product_cantidad = product_cantidad + cantidad + ';';
					product_cantidad_recibido = product_cantidad_recibido + '0;';
					cantidad_recibido_total = cantidad_recibido_total + '0;';					
				} else {
					errores = errores + 1;
					$(this).find('input[name="cantidad"]').parent().addClass("has-error has-feedback");
					$(this).find('input[name="cantidad"]').parent().find('.glyphicon-remove').show();
				}	
				
				product_id = product_id + parseInt($(this).attr('product_id')) + ';';
			});
		 	
		 	if (errores == 0) {
				guardar();
			}
		}
		
		
	});
	
	//ALMACENAR PRODUCTOS RECIBIDOS
	$('#almacenar_entrada').click(function(e){
		e.preventDefault();
		
		//LOADING BOTON
		l = Ladda.create(document.querySelector("#almacenar_entrada"));
	 	l.start();
		
		//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
		$("#example1 *").removeClass("has-error has-feedback");
		$('#example1 .glyphicon-remove').hide();
		
		//VERIFICAMOS SI HAY COTIZACION SELECCIONADA
		if($('#cotizaciones option:selected').val()) {
		
	 	//OBTENEMOS LOS ALMACENES DISPONIBLES PARA GUARDAR LOS PRODUCTOS
	 	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador",
			data: {'buscar':''},
			success: function(data) {
				if (data != null) {
		   			var options = '';
		    		
		    		product_cantidad_recibido = '';
		    		cantidad_recibido_total = '';
					product_id = '';
					product_cantidad = '';
					pe_observaciones_empresa = $.trim($('#observaciones_empresa').val());
					pe_observaciones_proveedor = $.trim($('#observaciones_proveedor').val());
					var cont = 0;
					var total = 0;
					var errores = 0;
					
					$('#example1 tbody tr').each(function(){
						total = total + 1;
						
						var recibido = $.trim($(this).find('input[name="recibido"]').val());
						var cantidad_total = $(this).find('input[name="cantidad"]').val();
						var cantidad_recibido = $(this).find('.cantidad_recibido').html();
									
						if ($(this).find('td:first input[name="product"]').is(':checked')) {
							if (recibido != '' && (parseFloat(recibido) + parseFloat(cantidad_recibido)) <= cantidad_total) {
								cont = cont + 1;
								product_id_check = product_id_check + $(this).attr('product_id') + ';';
								product_cantidad_recibido = product_cantidad_recibido + recibido + ';';
								
								cantidad_recibido_total = cantidad_recibido_total + cantidad_recibido + ';';
							} else if (parseFloat(cantidad_total) == parseFloat(cantidad_recibido)) {
								product_cantidad_recibido = product_cantidad_recibido + cantidad_recibido + ';';
								cantidad_recibido_total = cantidad_recibido_total + cantidad_recibido + ';';
							} else {
								errores = errores + 1;
								$(this).find('input[name="recibido"]').parent().addClass("has-error has-feedback");
							}	
						} else {
							product_cantidad_recibido = product_cantidad_recibido + '0;';
							cantidad_recibido_total = cantidad_recibido_total + cantidad_recibido + ';';
						}
						
						product_cantidad = product_cantidad + $.trim($(this).find('input[name="cantidad"]').val()) + ';'; 
						product_id = product_id + parseInt($(this).attr('product_id')) + ';';
					});
					
					/*if (cont == total) {
						pe_estado = 4;
					}*/
									
					if (errores == 0 && cont > 0) {
						$.each(data,function(indice,valor) {
			   				if (data[indice]['al_tipo'] == 0) {
								if (indice == 0) {
									options = options + '<option value="'+data[indice]['al_id']+'" selected>'+data[indice]['al_nombre']+'</option>';		
								} else {
									options = options + '<option value="'+data[indice]['al_id']+'">'+data[indice]['al_nombre']+'</option>';	
								}
							}   				
			    		});
			    		
			    		//PREGUNTAMOS EN QUE ALMACEN SE GUARDARA LOS PRODUCTOS
			    		bootbox.dialog({
							message: "<select class='form-control' id='al_id'>"+options+"</select>",
							title: "<?php echo lang('guardar.entrada.almacen') ?>",
							buttons: {
								success: {
									label: "<?php echo lang('seleccionar') ?>",
									className: "btn-success",
									callback: function() {
										al_id = $('#al_id option:selected').val();
										
										guardar();
									}
								}
							}
						}); 		
					} else {
						l.stop();
					}
		    	} else {
					bootbox.alert('<?php echo lang("almacenes.vacios") ?>');
					l.stop();
				}
			},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
		} else {
			bootbox.alert('<?php echo lang("error.carrito.cotizaciones") ?>');
			l.stop();
		}
	});
	
	//ENVIAR EMAIL AL PROVEEDOR	
	$('#enviar_email').click(function(e){
		e.preventDefault();
		//LOADING BOTON
		l = Ladda.create(document.querySelector("#enviar_email"));
	 	l.start();
	 	var adjuntos;
	 	var pro_id = '';
		var product_id = '';
		var product_cantidad = '';
		var pe_observaciones_proveedor = $.trim($('#observaciones_proveedor').val());
		var files = $("#file-adjunto").get(0).files;
		var fileData = new FormData();
		
		$('#example1 tbody tr').each(function(){
			pro_id = pro_id + parseInt($(this).attr('pro_id')) + ';';
			product_id = product_id + parseInt($(this).attr('product_id')) + ';';
			product_cantidad = product_cantidad + parseFloat($(this).find('input[name="cantidad"]').val()) + ';';
		});
	 	
	 	var confirmacion = 0;
	 	if (pe_estado == 1) {
			confirmacion = 1;
		}

		if(files && files.length > 0){		        	
	        for (var i = 0; i < files.length; i++) {
	            fileData.append("form"+i+"-"+<?php echo $pe_id; ?>, files[i]);
	        }

	        fileData.append("eliminados",files_delete);
	        fileData.append("adjuntos_iniciales","<?php echo $car_adjuntos; ?>");
	 	
		 	$.ajax({
	            type: "POST",
	            url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/upload_adjuntos",
	            dataType: "json",
	            contentType: false, // Not to set any content header
	            processData: false, // Not to process data
	            data: fileData,
	            success: function (result, status, xhr) {
	            	adjuntos = result.result;
	            	console.log('result',result);
			        $.ajax({
						type: "POST",
						dataType: "json",
						url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/pedido_email_proveedor",
						data: {'adjuntos':adjuntos, 'pro_id':pro_id, 'product_id':product_id, 'product_cantidad':product_cantidad,  'pe_observaciones_proveedor':pe_observaciones_proveedor, 'pe_id':pe_id, 'confirmacion':confirmacion},
						success: function(data) {
							if (data) {
								if (confirmacion == 0) {
									bootbox.alert('<?php echo lang("email.enviado") ?>');
									var pagina = '<?php echo base_url().$this->lang->lang() ?>/almacenes/editar_entrada/'+pe_id;
									setTimeout("location.href='"+pagina+"'", 5000);
								} else {
									location.reload(true);
								}
							} else {
								bootbox.alert('<?php echo lang("error.ajax") ?>');
							}
							l.stop();
						},
					  	error: function(XMLHttpRequest, textStatus, errorThrown) {
					   		bootbox.alert('<?php echo lang("error.ajax") ?>');
					   		l.stop();
					  	}
					});									                
	            },
	            error: function (xhr, status, error) {
	                alert(status + ' ' + error);
	            }
	        });
	 	}else{
	 		adjuntos = "<?php echo $pe_adjunto1; ?>";
	 		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/pedido_email_proveedor",
				data: {'adjuntos':adjuntos, 'pro_id':pro_id, 'product_id':product_id, 'product_cantidad':product_cantidad,  'pe_observaciones_proveedor':pe_observaciones_proveedor, 'pe_id':pe_id, 'confirmacion':confirmacion},
				success: function(data) {
					if (data) {
						if (confirmacion == 0) {
							bootbox.alert('<?php echo lang("email.enviado") ?>');
							var pagina = '<?php echo base_url().$this->lang->lang() ?>/almacenes/editar_entrada/'+pe_id;
							setTimeout("location.href='"+pagina+"'", 5000);
						} else {
							location.reload(true);
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
				},
			  	error: function(XMLHttpRequest, textStatus, errorThrown) {
			   		bootbox.alert('<?php echo lang("error.ajax") ?>');
			   		l.stop();
			  	}
			});
	 	}
	});
});
</script>