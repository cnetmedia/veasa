<!DOCTYPE html>
<html lang="<?php echo $this->lang->lang() ?>">
    <head>
        <meta charset="<?php echo $this->config->item('charset');?>" />
        <title><?php echo isset($TITULO)?$TITULO:'' ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="robots" content="noindex, nofollow" />
        
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Ionicons -->
        <link href="<?php echo base_url() ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Morris chart -->
        <link href="<?php echo base_url() ?>css/morris/morris.css" rel="stylesheet" type="text/css" />
        
        <!-- jvectormap -->
        <link href="<?php echo base_url() ?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        
        <!-- fullCalendar -->
        <link href="<?php echo base_url() ?>css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        
        <!-- Daterange picker -->
        <link href="<?php echo base_url() ?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo base_url() ?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        
        <!-- Ladda jQuery -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/ladda-themeless.min.css">
        
        <!-- Estilos jQuery Ui -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/themes/base/jquery.ui.all.css">
        
        <!-- Estilo personal -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/style.css" />
        
        
        
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo base_url() ?>js/jquery.min.js"></script>
        
        <!-- jQuery UI 1.10.3 -->
        <script src="<?php echo base_url() ?>js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        
        <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
        
        <!-- Morris.js charts -->
        <script src="<?php echo base_url() ?>js/raphael-min.js"></script>
        <script src="<?php echo base_url() ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
        
        <!-- Sparkline -->
        <script src="<?php echo base_url() ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        
        <!-- jvectormap -->
        <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="<?php echo base_url() ?>js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url() ?>js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        
        <!-- daterangepicker -->
        <script src="<?php echo base_url() ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url() ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url() ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        
        <!-- InputMask -->
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        
        <!-- date-range-picker -->
        <script src="<?php echo base_url() ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url() ?>js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url() ?>js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() ?>js/AdminLTE/app.js" type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!--<script src="<?php echo base_url() ?>js/AdminLTE/dashboard.js" type="text/javascript"></script>--> 
        
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() ?>js/AdminLTE/demo.js" type="text/javascript"></script>
        
        <!-- Ladda jQuery -->
        <script src="<?php echo base_url() ?>js/spin.min.js"></script>
		<script src="<?php echo base_url() ?>js/ladda.min.js"></script>
		
		<!-- Pagination Plugin -->
		<link href="<?php echo base_url() ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url() ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		
		<!-- ENVIO DE IMAGENES POR AJAX-->
		<script src="<?php echo base_url() ?>js/jquery.form.min.js"></script>
		
		<!-- ALERT BOOTSTRAP -->
		<script src="<?php echo base_url() ?>js/bootbox.min.js"></script>
		
		<!-- MAPA Y COORDENADAS -->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&language=<?php echo $this->lang->lang() ?>"></script>
		<script src="<?php echo base_url() ?>js/markerclusterer.js"></script>
		<script src="<?php echo base_url() ?>js/markerwithlabel_packed.js"></script>
        
        <!-- Encriptacion AES -->
		<script src="<?php echo base_url() ?>js/aes.js"></script>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?php echo base_url() ?>js/html5shiv.js"></script>
          <script src="<?php echo base_url() ?>js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">

		<?php echo isset($BODY)?$BODY:'' ?>

    </body>
</html>