<?php $mostrar_campo_factura = ($this->session->userdata('de_id')!='2') && ($this->session->userdata('de_id')!='4') && ($this->session->userdata('de_id')!='5'); ?>
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
    
	<h4><?php echo lang('info.cliente') ?></h4>
	
	<div class="row cliente">
		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
			<?php echo $cl_nombre ?>
		</div>
		
		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
			<?php echo $cl_dni ?>
		</div>
	</div>

	<div class="row cliente">
		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
			<?php echo $cl_telefono ?>
		</div>
		
		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
			<?php echo $cl_email ?>
		</div>
	</div>

	<div class="row cliente">
		<div class="col-lg-12 col-xs-12">
			<?php echo $cl_direccion ?>
		</div>
	</div>

	<div class="row cliente">
		<div class="col-lg-12 col-xs-6">
			<?php
				if ($cl_postal != '') {
					echo $cl_postal.', '.$cl_localidad;
				} else {
					echo $cl_localidad;
				}
			?>
		</div>
	</div>

	<div class="row cliente">
		<div class="col-lg-12 col-xs-6">
			<?php echo $cl_provincia.', '.$name ?>
		</div>
	</div>

        <?php if ($pr_id != '') { ?>
        <h4><?php echo lang('mantenimiento') ?></h4>
        
        <div class="row cliente">
            <div class="col-lg-12 col-xs-6 mantenimiento">
                <?php echo $man_nombre ?>
            </div>
	</div>
        <?php } ?>
       <br/>
    </div>
    
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
    <table id="costos" class="table table-bordered table-striped table-hover dataTable">
        <thead>
            <tr>
                <th width="65%"><?php echo lang("descripcion") ?></th>
                <th class="text-right"><?php echo lang("coste.unitario").' / ' ?><span class="moneda"></span></th>
            </tr>
        </thead>
        <tbody>
		<?php 
		$costos_re = isset($re_costos)?$re_costos:'1001;2001;3001;4001;5001;6001;7001;8001/0;0;0;0;0;0;0;0';
		$costos_apart = explode('/',$costos_re);
		
		$costos_re = explode(';',$costos_apart[1]);
		$costos_re = array_filter($costos_re, "strlen");
		
		$id_costos = explode(';',$costos_apart[0]);
		$id_costos = array_filter($id_costos, "strlen");
		
		$categorias = $this->db->from("categorias")->get();
		
		$a=0;
		
		foreach($categorias->result() as $row) {
			
			$costo = ($costos_re[$a])?$costos_re[$a]:0;
			
		?>
                                                
        <tr>
            <td class="vcenter text-right"><?php echo $row->descripcion; ?></td>
            <td><input id="<?php echo $row->cate_id; ?>" value="<?php echo $costo; ?>" type="text" class="form-control text-center" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, '');"></td>
    	</tr>
        <?php $a++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="text-right text-right"><strong><?php echo lang('costos') ?></strong></td>
                <td class="text-right"></td>
            </tr>
        </tfoot>
      </table>
    </div>
	
	<br/>		
	
	<div class="row">
		<div class="col-lg-12">
			<!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                    	<a href="#tab_1" data-toggle="tab"><strong><?php echo lang('factura.rectificativa') ?></strong></a>
                                    </li>
                                    <li>
                                    	<a href="#tab_2" data-toggle="tab"><?php echo lang('factura.original') ?></a>
                                    </li>
                                </ul>
                                
                                <div class="tab-content">
                                
                                    <div class="tab-pane active" id="tab_1">
                                        <h4>
										<?php echo lang('detalles.factura.rectificativa').': <strong>'.$re_numero.'</strong>'; ?>
										</h4>
										
										<?php echo lang('fecha.factura.rectificativa').': '.$re_fecha.'<br>'; ?>
										
										<div class="form-group">
											<div class="radio radio_line">
												<label>
													<input type="radio" name="su_iva" value="<?php echo $su_iva ?>" <?php echo ($re_iva==$su_iva)?'checked':''; ?> /> <?php echo lang('iva.general') ?>
												</label>
												
												<label>
													<input type="radio" name="su_iva" value="<?php echo $su_iva_reducido ?>" <?php echo ($re_iva==$su_iva_reducido)?'checked':''; ?> /> <?php echo lang('iva.reducido') ?>
												</label>
												
												<label>
													<input type="radio" name="su_iva" value="<?php echo $su_iva_superreducido ?>" <?php echo ($re_iva==$su_iva_superreducido)?'checked':''; ?> /> <?php echo lang('iva.superreducido') ?>
												</label>
											</div>
										</div>
										
										<div id="listar" class="bottom30">
											<div class="box-body table-responsive">
												<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
													<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
										            	<thead>
										                	<tr>
										                		<th width="60%" colspan="2"><?php echo lang("descripcion") ?></th>
										                		<th width="10%" class="text-right"><?php echo lang("coste.unitario").' / ' ?><span class="moneda"></span></th>
										                		<th width="10%" class="text-right"><?php echo lang("cantidad") ?></th>
										                		<th width="10%" class="text-right"><?php echo lang("descuento") ?> / %</th>
										                		<th width="10%" style="display: none" class="text-right"><?php echo lang("ganancia") ?> / %</th>
										                		<th width="10%" class="text-right"><?php echo lang("precio").' / ' ?><span class="moneda"></span></th>
										                	</tr>
														</thead>
														
														<tbody aria-relevant="all" aria-live="polite" role="alert"></tbody>
														
														<tfoot>
															<tr class="botones_fila">
																<td colspan="8" class="text-center">
																	<a id="nueva_fila" href="#" class="btn btn-primary ladda-button" data-style="zoom-out">
																		<i class="fa fa-plus"></i> <?php echo lang("nueva.fila") ?>
																	</a>
																	
																	<a id="recalcular" href="#" class="btn btn-primary ladda-button" data-style="zoom-out">
																		<i class="fa fa-check-square-o"></i> <?php echo lang("recalcular") ?>
																	</a>
																</td>
															</tr>
															
															<tr>
																<td colspan="5" class="text-right"><?php echo lang('subtotal') ?></td>
																<td class="text-right"></td>
															</tr>
																
															<tr>
																<td colspan="5" class="text-right"><?php echo lang('descuento') ?></td>
																<td class="text-right"></td>
															</tr>
																
															<tr>
																<td colspan="5" class="text-right"><?php echo lang('base.imponible') ?></td>
																<td class="text-right"></td>
															</tr>
																
															<tr>
																<td colspan="5" class="text-right">(<span class="impuesto"></span> %) <?php echo lang('impuestos') ?></td>
																<td class="text-right"></td>
															</tr>
																
															<tr>
																<td colspan="5" class="text-right"><strong><?php echo lang('total') ?></strong></td>
																<td class="text-right"><strong></strong></td>
															</tr>
															<!--	
															<tr>
																<td colspan="5" class="text-right"><?php echo lang('ganancia') ?></td>
																<td class="text-right"><strong></strong></td>
															</tr>
															-->
														</tfoot>
													</table>	
												</div>
											</div>
										</div>
										
										<div class="row" id="re_textarea">
											<div class="col-lg-12">
												<div class="form-group">
		                                            <label><?php echo lang('rectificativa.info') ?></label>
		                                            <?php
		                                            $textarea = array(
														'name'		=>	're_info',
														'id'		=>	're_info',
														'class'		=>	'form-control',
														'value'		=>	isset($re_info)?$re_info:set_value('re_info'),
														'style'		=>	'max-height:100px;min-height:100px;max-width:100%;min-width:100%;'
													);
		                                            ?>
		                                            <?php echo form_textarea($textarea) ?>
		                                            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
													<div class="text-danger"><?php echo form_error('re_info') ?></div>
		                                        </div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-lg-12 col-xs-12 text-right">		
												
												<?php if ($mostrar_campo_factura) { ?>
												
													<a id="guardar_factura" href="#" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-save"></i> <?php echo lang('guardar') ?></a>		
													<a id="anulada" href="#" class="btn btn-danger ladda-button" data-style="zoom-out"><i class="fa fa-times"></i> <?php echo lang('anulada') ?></a>
													<a href="<?php echo base_url().$this->lang->lang().'/facturas/generar_pdf/'.$fa_id.'/R' ?>" class="btn btn-primary"><i class="fa fa-file-text"></i> <?php echo lang('pdf') ?></a>
													<a id="factura_rectificativa_email" href="#" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-envelope"></i> <?php echo lang('email') ?></a>
												
												<?php } ?>
												
												<a href="<?php echo base_url().$this->lang->lang().'/facturas' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
											</div>
										</div>
                                    </div><!-- /.tab-pane -->
                                    
                                    <div class="tab-pane" id="tab_2">
                                        <h4>
										<?php echo lang('detalles.factura').': <strong>'.$fa_numero.'</strong>'; ?>
										</h4>
										
										<?php echo lang('fecha.factura').': '.$fa_fecha.'<br>'; ?>
										
										<div class="box-body table-responsive">
											<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
												<table aria-describedby="example1_info" id="example2" class="table table-bordered table-striped table-hover dataTable">
									            	<thead>
									                	<tr>
									                		<th width="60%"><?php echo lang("descripcion") ?></th>
									                		<th width="10%" class="text-right"><?php echo lang("coste.unitario").' / ' ?><span class="moneda"></span></th>
									                		<th width="10%" class="text-right"><?php echo lang("cantidad") ?></th>
									                		<th width="10%" class="text-right"><?php echo lang("descuento") ?> / %</th>
									                		<th width="10%" style="display: none" class="text-right"><?php echo lang("ganancia") ?> / %</th>
									                		<th width="10%" class="text-right"><?php echo lang("precio").' / ' ?><span class="moneda"></span></th>
									                	</tr>
													</thead>
													
													<tbody aria-relevant="all" aria-live="polite" role="alert">
														<?php
															$servicios = explode(';',$fa_servicio);
															$precios = explode(';',$fa_precio);
															$cantidades = explode(';',$fa_cantidad);
															$descuentos = explode(';',$fa_descuento);
															$ganancias = explode(';',$fa_ganancia);
															
															$coste_descuento = 0;
															$coste_ganancia = 0;
															$subtotal = 0;
															
															$fila = '';
															for ($i = 0; $i < count($servicios)-1; $i++) {
																$fila = $fila.'<tr>';
																$fila = $fila.'<td>'.$servicios[$i].'</td>';
																$fila = $fila.'<td class="text-right">'.$precios[$i].'</td>';
																$fila = $fila.'<td class="text-right">'.$cantidades[$i].'</td>';
																$fila = $fila.'<td class="text-right">'.$descuentos[$i].'</td>';
																$fila = $fila.'<td style="display: none" class="text-right">'.$ganancias[$i].'</td>';
																
																$coste = $precios[$i]*$cantidades[$i];
																$subtotal = $subtotal+$coste;
																
																$descuento = ($coste*$descuentos[$i])/100;
																$coste_descuento = $coste_descuento + $descuento;
																
																$fila = $fila.'<td class="text-right">'.$coste.'</td>';
																$fila = $fila.'</tr>';
															}
															echo $fila;
														?>
													</tbody>
													
													<tfoot>
						<?php 
						$costos_fa = isset($fa_costos)?$fa_costos:'0;0;0;0;0;';
						$costos_fa = explode(';',$costos_fa);
						$costos_fa = array_filter($costos_fa, "strlen");
						
						$costos_fa_suma = 0;
						for ($x=0; $x<count($costos_fa); $x++) {
							$costos_fa_suma = $costos_fa_suma + (double)$costos_fa[$x];
						}
						?>
													
														<tr>
															<td colspan="4" class="text-right vcenter"><?php echo lang('aluminio') ?></td>
															<td class="text-right"><?php echo $costos_fa[0] ?></td>
														</tr>
														
														<tr>
															<td colspan="4" class="text-right vcenter"><?php echo lang('juntas.herrajes') ?></td>
															<td class="text-right"><?php echo $costos_fa[1] ?></td>
														</tr>
														
														<tr>
															<td colspan="4" class="text-right vcenter"><?php echo lang('vidrio') ?></td>
															<td class="text-right"><?php echo $costos_fa[2] ?></td>
														</tr>
														
														<tr>
															<td colspan="4" class="text-right vcenter"><?php echo lang('silicon') ?></td>
															<td class="text-right"><?php echo $costos_fa[3] ?></td>
														</tr>
														
														<tr>
															<td colspan="4" class="text-right vcenter"><?php echo lang('mano.de.obra') ?></td>
															<td class="text-right"><?php echo $costos_fa[4] ?></td>
														</tr>
														
														<tr>
															<td colspan="4" class="text-right vcenter"><?php echo lang('otros') ?></td>
															<td class="text-right"><?php echo $costos_fa[5] ?></td>
														</tr>
														
														<tr>
															<td colspan="4" class="text-right"><?php echo lang('costos') ?></td>
															<td class="text-right"><?php echo $costos_fa_suma ?></td>
														</tr>
													
														<tr>
															<td colspan="4" class="text-right"><?php echo lang('subtotal') ?></td>
															<td class="text-right"><?php echo $subtotal ?></td>
														</tr>
															
														<tr>
															<td colspan="4" class="text-right"><?php echo lang('descuento') ?></td>
															<td class="text-right"><?php echo $coste_descuento ?></td>
														</tr>
															
														<tr>
															<td colspan="4" class="text-right"><?php echo lang('base.imponible') ?></td>
															<?php $base_imponible = $subtotal - $coste_descuento; ?>
															<td class="text-right"><?php echo $base_imponible ?></td>
														</tr>
															
														<tr>
															<td colspan="4" class="text-right">(<span><?php echo $fa_iva ?></span> %) <?php echo lang('impuestos') ?></td>
															<?php $impuesto = ($base_imponible*$fa_iva)/100; ?>
															<td class="text-right"><?php echo $impuesto ?></td>
														</tr>
															
														<tr>
															<td colspan="4" class="text-right"><strong><?php echo lang('total') ?></strong></td>
															<td class="text-right"><strong><?php echo $fa_total ?></strong></td>
														</tr>
														<!--	
														<tr>
															<td colspan="4" class="text-right"><?php echo lang('ganancia') ?></td>
															<td class="text-right"><?php echo (double)$fa_ganancia_total - (double)$costos_fa_suma ?></td>
														</tr>
														-->
													</tfoot>
														</table>	
											</div>
										</div>
										
										<div class="row">
											<div class="col-lg-12 text-right">	
												<?php if ($pr_id != '') { ?>
													<a href="<?php echo base_url().$this->lang->lang() ?>/presupuestos/editar_presupuesto/<?php echo $pr_id ?>" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-eye"></i> <?php echo lang('ver.presupuesto') ?></a>
												<?php } ?>
																								
												
												<a href="<?php echo base_url().$this->lang->lang().'/facturas/generar_pdf/'.$fa_id.'/F' ?>" class="btn btn-primary"><i class="fa fa-file-text"></i> <?php echo lang('pdf') ?></a>
														
												<a id="factura_original_email" href="#" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-envelope"></i> <?php echo lang('email') ?></a>
															
												<a href="<?php echo base_url().$this->lang->lang().'/facturas' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
											</div>
										</div>
                                    </div><!-- /.tab-pane -->
                                    
                                </div><!-- /.tab-content -->
                            </div><!-- nav-tabs-custom -->
		</div>
	</div>
	

<script>
		$(document).ready(function(){
			//PREGUNTAMOS SI PUEDE EDITAR
			var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
			if (editar == 0) {
				$('#guardar_factura').hide();
			}
			
			//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    	$("#re_textarea .form-group").removeClass("has-error has-feedback");
	    	$('#re_textarea .glyphicon-remove').hide();
	    	$("#re_textarea .text-danger").html('');
					
			//VARIABLES GLOBALES
			var servicios = '<?php echo isset($re_servicio)?str_replace("<br />", "\\n", preg_replace("/\\n/m", "<br />", $re_servicio)):"" ?>';
			var costes = '<?php echo isset($re_precio)?$re_precio:"" ?>';
			var cantidades = '<?php echo isset($re_cantidad)?$re_cantidad:"" ?>';
			var descuentos = '<?php echo isset($re_descuento)?$re_descuento:"" ?>';
			var ganancias = '<?php echo isset($re_ganancia)?$re_ganancia:"" ?>';
			var enviar = false;
			var datos;
			var su_zona_horaria;
			var cl_id = '<?php echo $cl_id ?>';
			var su_id = '<?php echo $su_id ?>';
			var array_servicios = servicios.split(";");
			var array_costes = costes.split(";");
			var array_cantidades = cantidades.split(";");
			var array_descuentos = descuentos.split(";");
			var array_ganancias = ganancias.split(";");
			var fila = '';
			var coste_total = 0;
			var coste_descuento = 0;
			var coste_ganancia = 0;
			var subtotal = 0;
			var iva = '<?php echo $re_iva ?>';
			var fa_id = '<?php echo $fa_id ?>';
			var total = 0;
			
			
			
			//TBODY INPUTS PARA LA TABLA
			function anadir_fila() {
				fila = '<tr>';
							
				fila = fila + '<td width="3%" class="text-center vert-align"><i class="fa fa-times mano" title="<?php echo lang("borrar.fila") ?>" onclick="javascript: this.parentNode.parentNode.remove();"></i></td>';
								
				fila = fila + '<td><textarea name="fa_servicio" class="fa_servicio form-control" style="width:100%" maxlength="4000"></textarea><input name="fa_servicio" class="fa_servicio form-control" type="hidden" /><div class="text-danger"></div></td>';
								
				fila = fila + '<td class="text-right"><input name="re_precio" class="re_precio form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');"><div class="text-danger"></div></td>';
								
				fila = fila + '<td class="text-right"><input name="re_cantidad" class="re_cantidad form-control text-right" onkeyup="var pattern = /[^0-9]/g;this.value = this.value.replace(pattern, \'\');"><div class="text-danger"></div></td>';
								
				fila = fila + '<td class="text-right"><input name="re_descuento" class="re_descuento form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');"><div class="text-danger"></div></td>';
				
				fila = fila + '<td style="display: none" class="text-right"><input name="re_ganancia" value="0" class="re_ganancia form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');"><div class="text-danger"></div></td>';
								
				fila = fila + '<td class="text-right"><strong></strong></td>';
								
				fila = fila + '</tr>';
							
				$('#listar tbody').append(fila);
			}
						
			//LLAMAMOS LA FUNCION A�ADIR FILA
			$('#nueva_fila').click(function(e){
				e.preventDefault();
				var l = Ladda.create( document.querySelector( "#nueva_fila" ) );
				l.start();
				anadir_fila();
				l.stop();
			});
			
			//RE-CALCULAR PRESUPUESTO
			function recalcular() {
					
					$('#listar tbody td div').html('');
					$('#listar tbody input.fa_servicio').val($('#listar tbody textarea.fa_servicio').val());
					var errores = 0;
					//RECORREMOS TODOS LOS INPUT
					$('#listar tbody input').each(function(indice){
						//QUE NO ESTEN VACIOS
						if ($(this).val().length != 0) {
							//NO SUPEREN LOS 4000 CARACTERES
							if ($(this).val().length < 4000) {
								//SOLO PARA LOS NUMEROS
								if (indice == 2 || indice == 3 || indice == 4) {
									//CONTAMOS EL NUMEROS DE PUNTOS
									var cont=0;
									var txt = $(this).val();
									for (var i=0; i<txt.length; i++) {
										if (txt.charAt(i) == '.') {
											cont++;
										}
									}
											
									//COMPROBAMOS LOS PUNTOS
									if (cont > 1) {
					    				$(this).next().html('<?php echo lang("decimal.punto") ?>');
					    				errores++;
					  				} else {
										var ultimo_caracter = $(this).val().substring($(this).val().length-1, $(this).val().length);
					  					if (ultimo_caracter == '.') {
											$(this).next().html('<?php echo lang("ultimo.punto") ?>');
											errores++;
										}
									}
								}
							} else {
								$(this).next().html('<?php echo lang("maximo.4000") ?>');
								errores++;
							}
						} else {
							$(this).next().html('<?php echo lang("requerido") ?>');
							errores++;
						}
								
						//CONTROLAMOS QUE NO PUEDA METER UN CERO EN LA CANTIDAD
						if ($(this).attr('name') == 're_cantidad') {
							if ($(this).val() == '0') {
								$(this).next().html('<?php echo lang("no.cero") ?>');
								errores++;
							}
						}
					});
					
					//SI NO HAY ERRORES CALCULA LAS VARIABLES GLOBALES			
					if (errores == 0) {
						coste_total = 0;
						coste_descuento = 0;
						coste_ganancia = 0;
						subtotal = 0;
									
						$('#listar tbody tr').each(function(indice){
									
							var coste = $(this).find('input:eq(1)').val() * $(this).find('input:eq(2)').val();
							$(this).find('td:last').html(coste.toFixed(2));
							coste_total = coste_total + coste;
										
							var descuento = (coste*$(this).find('input:eq(3)').val())/100;
							coste_descuento = coste_descuento + descuento;
							
							var ganancia = ((coste-descuento)*$(this).find('input:eq(4)').val())/100;
							coste_ganancia = coste_ganancia + ganancia;
						});
							
						enviar = true;
					} else {
						enviar = false;
					}
					
					if (enviar) {
						calculos();
					}
			}
			
			//CALCULAMOS LOS DATOS DE LAS OPERACIONES FINALES
			function calculos() {
				var aux_costos = parseFloat($('#costos tr:eq(1) input').val()) + 
									parseFloat($('#costos tr:eq(2) input').val()) + 
									parseFloat($('#costos tr:eq(3) input').val()) + 
									parseFloat($('#costos tr:eq(4) input').val()) + 
									parseFloat($('#costos tr:eq(5) input').val()) + 
									parseFloat($('#costos tr:eq(6) input').val()) + 
									parseFloat($('#costos tr:eq(7) input').val()) +
									parseFloat($('#costos tr:eq(8) input').val());
				
				$('#costos tfoot tr:eq(0) td:last').html(aux_costos.toFixed(2));
				$('#listar tfoot tr:eq(1) td:last').html(coste_total.toFixed(2));
				$('#listar tfoot tr:eq(2) td:last').html(coste_descuento.toFixed(2));
				var precio_final_sin_iva = coste_total - coste_descuento;
				$('#listar tfoot tr:eq(3) td:last').html(precio_final_sin_iva.toFixed(2));
				var aux_iva = (precio_final_sin_iva * parseFloat(iva))/100;
				$('#listar tfoot tr:eq(4) td:last').html(aux_iva.toFixed(2));
				total = precio_final_sin_iva + aux_iva;
				total = total.toFixed(2)
				$('#listar tfoot tr:eq(5) td:last strong').html(total);
				
				var ganancia = parseFloat(coste_ganancia) - parseFloat(aux_costos);
				/*$('#listar tfoot tr:eq(13) td:last strong').html(ganancia);
				
				if (ganancia <= 0) {
					$('#listar tfoot tr:eq(13) td:last strong').addClass('rojo');
				} else {
					$('#listar tfoot tr:eq(13) td:last strong').removeClass('rojo');
				}*/
				
				crear_datos();
			}
			
			//LLAMA LAS FUNCIONES PARA RECALCULAR TODOS LOS DATOS Y LOS ERRORES	
			$('#recalcular').click(function(e){
				e.preventDefault();
				var l = Ladda.create( document.querySelector( "#recalcular" ) );
				l.start();
				recalcular();
				//calculos();
				l.stop();
			});
			
			//GENERA LOS DATOS A ENVIAR
			function crear_datos() {
				if (enviar) {
					var cont = 0;
					servicios = '';
					costes = '';
					cantidades= '';
					descuentos = '';
					ganancias = '';
					
					$('#listar tbody textarea').each(function(indice){
						servicios = servicios + $(this).val() + ';';
					});
					
					$('#listar tbody input').each(function(indice){
						switch(cont){
							case 0:
								//servicios = servicios + $(this).val() + ';';
								break;
							case 1:
								costes = costes + $(this).val() + ';';
								break;
							case 2:
								cantidades = cantidades + $(this).val() + ';';
								break;
							case 3:
								descuentos = descuentos + $(this).val() + ';';
								break;
							case 4:
								ganancias = ganancias + $(this).val() + ';';
								break;
						}
						
						cont++;
						
						if (cont > 4) {
							cont = 0;	
						}
					});
					
					var costos = $('#costos tr:eq(1) td:last input').attr('id') + ';' + $('#costos tr:eq(2) td:last input').attr('id') + ';' + $('#costos tr:eq(3) td:last input').attr('id') + ';' + $('#costos tr:eq(4) td:last input').attr('id') + ';' + $('#costos tr:eq(5) td:last input').attr('id') + ';' + $('#costos tr:eq(6) td:last input').attr('id') + ';'+ $('#costos tr:eq(7) td:last input').attr('id') + ';' + $('#costos tr:eq(8) td:last input').attr('id') + '/';
						costos = costos + $('#costos tr:eq(1) td:last input').val() + ';' + $('#costos tr:eq(2) td:last input').val() + ';' + $('#costos tr:eq(3) td:last input').val() + ';' + $('#costos tr:eq(4) td:last input').val() + ';' + $('#costos tr:eq(5) td:last input').val() + ';' + $('#costos tr:eq(6) td:last input').val() + ';' + $('#costos tr:eq(7) td:last input').val() + ';'+ $('#costos tr:eq(8) td:last input').val() + ';';
					
					
					datos = {
						'fa_id' : fa_id,
						're_servicio' : servicios,
						're_precio' : costes,
						're_cantidad' : cantidades,
						're_descuento' : descuentos,
						're_ganancia' : ganancias,
						're_iva' : iva,
						're_total' : total,
						're_numero' : '<?php echo $re_numero ?>',
						're_ganancia_total' : coste_ganancia.toFixed(2),
						're_info' : $('#re_info').val(),
						're_costos' : costos
					};
				}
			}
					
			//GUARDAR PRESUPUESTO
			$('#guardar_factura').click(function(e){
				e.preventDefault();
				
	    		$("#re_textarea .text-danger").html('');
	    		
				recalcular();
				//crear_datos();
				//calculos();
				
				if (enviar) {
					var l = Ladda.create( document.querySelector( "#guardar_factura" ) );
					//l.start();
					if ((servicios.length-1) > 0) {
						$.ajax({
							type: "POST",
							dataType: "json",
							url: "<?php echo base_url().$this->lang->lang() ?>/facturas/rectificativas_guardar_editar",
							data: datos,
							success: function(data) {
								if (data.sql) {
									//MENSAJES ALERTA PARA EDITAR O CREAR
									if (!data.editar) {
										bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										setTimeout("location.href='"+location+"'", 5000);
									}
									
									if (data.status) {
										bootbox.alert('<?php echo lang("factura.rectificativa.guardar.ok") ?>');
									} else {
										$('#re_textarea .text-danger').html(data.errors[0][1]);
									}
								} else {
									bootbox.alert('<?php echo lang("error.ajax") ?>');
								}
								l.stop();
							},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});
					} else {
						bootbox.alert('<?php echo lang("no.datos") ?>');
					}
				}
			});
			
			//CAMBIAMOS EL IVA CON LOS RADIOS BUTTONS
			$('.radio_line label').click(function(){
				if (editar == 1) {
					iva = $(this).find('input').val();
					//CALCULA LAS OPERACIONES FINALES SI EL PRESUPUESTO ES NUEVO O ESTA ABIERTO
					calculos();
					$('.impuesto').html(iva);
				}

			});
			
			$('.moneda').html('<?php echo isset($currrency_symbol)?$currrency_symbol:"" ?>');
				
					//RELLENAMOS LAS FILAS CON LOS INPUT DE LOS DATOS DEL PRESUPUESTO
					for (var y=0; y<array_servicios.length-1; y++) {
						fila = fila + '<tr>';
						
						if (editar == 1) {
							fila = fila + '<td width="3%" class="text-center vert-align"><i class="fa fa-times mano" title="<?php echo lang("borrar.fila") ?>" onclick="javascript: this.parentNode.parentNode.remove();"></i></td>';
							fila = fila + '<td><textarea name="fa_servicio" class="fa_servicio form-control" style="width:100%" maxlength="4000">'+array_servicios[y]+'</textarea><input name="fa_servicio" class="fa_servicio form-control" type="hidden" value="'+array_servicios[y]+'" /><div class="text-danger"></div></td>';
						} else {
							fila = fila + '<td colspan="2"><input name="fa_servicio" class="fa_servicio form-control" style="width:100%" maxlength="4000" value="'+array_servicios[y]+'" /><div class="text-danger"></div></td>';
						}
								
						fila = fila + '<td class="text-right"><input name="re_precio" class="re_precio form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');" value="'+array_costes[y]+'" /><div class="text-danger"></div></td>';
								
						fila = fila + '<td class="text-right"><input name="re_cantidad" class="re_cantidad form-control text-right" onkeyup="var pattern = /[^0-9]/g;this.value = this.value.replace(pattern, \'\');" value="'+array_cantidades[y]+'" /><div class="text-danger"></div></td>';
								
						fila = fila + '<td class="text-right"><input name="re_descuento" class="re_descuento form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');" value="'+array_descuentos[y]+'" /><div class="text-danger"></div></td>';
						
						fila = fila + '<td style="display: none" class="text-right"><input name="re_ganancia" value="0" class="re_ganancia form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');" value="'+array_ganancias[y]+'" /><div class="text-danger"></div></td>';
							
						fila = fila + '<td class="text-right"><strong></strong></td>';
								
						fila = fila + '</tr>';
					}
						
					$('#listar tbody').append(fila);
					
					if (editar == 0) {
						$('#example1 tbody input, #re_textarea textarea').attr('readonly', true);
						$('#example1 tfoot tr:eq(0) *').remove();
						$('.radio_line label div').addClass('disabled');
					}
					
					recalcular();
					//calculos();					
						
					if ((array_servicios.length-1) == 0) {
						//CARGAMOS LA PRIMERA FILA
						anadir_fila();
					}
				
				//MANDAMOS LA FACTURA RECTIFICATIVA POR EMAIL
				$('#factura_rectificativa_email').click(function(e){
					e.preventDefault();
					
					//LOADING BOTON
					var l = Ladda.create( document.querySelector( "#factura_rectificativa_email" ) );
				 	l.start();
						
					var cl_email = '<?php echo isset($cl_email)?$cl_email:"" ?>';
				 	var emails = '<?php echo isset($cl_emails)?$cl_emails:"" ?>';
				 	var emails_info = '<?php echo isset($cl_emails_info)?$cl_emails_info:"" ?>';
				 	
				 	if (emails != '') {
						emails = cl_email + ';' + emails;
						
					} else {
						emails = cl_email + ';';
					}
					
					emails = emails.split(";");
					emails_info = emails_info.split(";");
				 	
				 	var listar = '<div class="row"><div class="col-lg-8 col-xs-12"><strong><?php echo lang("email") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("no") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("para") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("cc") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("cco") ?></strong></div></div><br>';
				 	for (var i=0; i<emails.length-1;i++) {
						listar = listar + '<div class="row">';
						
						listar = listar + '<div class="col-lg-8 col-xs-12">';
						listar = listar + emails[i];
						listar = listar + '</div>';
						
						check = '';
						if (i == 0) {
							check = 'checked';
						}
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="0" checked>';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="1" '+check+'>';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="2">';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="3">';
						listar = listar + '</div>';
						
						var info = '';
						if (i > 0) {
							info = emails_info[i-1]
						}
						
						listar = listar + '<div class="col-lg-12">';
						listar = listar + '<p class="text-muted">' + info + '</p>';
						listar = listar + '</div>';
						
						listar = listar + '</div>';
					}
					
					bootbox.dialog({
						title: "<?php echo lang('emails.cliente') ?>",
						message: listar,
						buttons: {
							success: {
								label: "<?php echo lang('cancelar') ?>",
								className: "btn-default",
								callback: function(result) {
									l.stop();
								}
							},
							danger: {
								label: "<i class='fa fa-envelope'></i> <?php echo lang('enviar') ?>",
								className: "btn-primary",
								callback: function(result) {
									var datos = new Array();
									var cont = 0;
									$('.modal-content .modal-body .row').each(function(index) {
										if (index > 0) {
											var email = $(this).find('div:eq(0)').html();
											var value = '';
											
											$('.modal-content .modal-body .row:eq('+index+') input').each(function(){
												if ($(this).is(':checked')) {
													value = $(this).val();
													if (value > 0) {
														cont++;
													}
												}
											});
											
											var aux = {
												'email'	:	email,
												'value'	:	value
											};
											datos[datos.length] = aux;
										}
									});
									
									if (cont > 0) {
										$.post(
										    "<?php echo base_url().$this->lang->lang() ?>/facturas/enviar_email",
										    {'fa_id':fa_id,'tipo':'R','datos':datos},
										    function(data){
										    	bootbox.alert('<?php echo lang("factura.rectificativa.email.ok") ?>');
										    	l.stop();
										    }
										);
									} else {
										bootbox.alert('<?php echo lang("emails.seleccion") ?>');
										l.stop();
									}
								}
							}
						}
					});
				});
				
				//MANDAMOS LA FACTURA POR EMAIL
				$('#factura_original_email').click(function(e){
					e.preventDefault();
					
					//LOADING BOTON
					var l = Ladda.create( document.querySelector( "#factura_original_email" ) );
				 	l.start();
					
					var cl_email = '<?php echo isset($cl_email)?$cl_email:"" ?>';
				 	var emails = '<?php echo isset($cl_emails)?$cl_emails:"" ?>';
				 	var emails_info = '<?php echo isset($cl_emails_info)?$cl_emails_info:"" ?>';
				 	
				 	if (emails != '') {
						emails = cl_email + ';' + emails;
						
					} else {
						emails = cl_email + ';';
					}
					
					emails = emails.split(";");
					emails_info = emails_info.split(";");
				 	
				 	var listar = '<div class="row"><div class="col-lg-8 col-xs-12"><strong><?php echo lang("email") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("no") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("para") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("cc") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("cco") ?></strong></div></div><br>';
				 	for (var i=0; i<emails.length-1;i++) {
						listar = listar + '<div class="row">';
						
						listar = listar + '<div class="col-lg-8 col-xs-12">';
						listar = listar + emails[i];
						listar = listar + '</div>';
						
						check = '';
						if (i == 0) {
							check = 'checked';
						}
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="0" checked>';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="1" '+check+'>';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="2">';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="3">';
						listar = listar + '</div>';
						
						var info = '';
						if (i > 0) {
							info = emails_info[i-1]
						}
						
						listar = listar + '<div class="col-lg-12">';
						listar = listar + '<p class="text-muted">' + info + '</p>';
						listar = listar + '</div>';
						
						listar = listar + '</div>';
					}
					
					bootbox.dialog({
						title: "<?php echo lang('emails.cliente') ?>",
						message: listar,
						buttons: {
							success: {
								label: "<?php echo lang('cancelar') ?>",
								className: "btn-default",
								callback: function(result) {
									l.stop();
								}
							},
							danger: {
								label: "<i class='fa fa-envelope'></i> <?php echo lang('enviar') ?>",
								className: "btn-primary",
								callback: function(result) {
									var datos = new Array();
									var cont = 0;
									$('.modal-content .modal-body .row').each(function(index) {
										if (index > 0) {
											var email = $(this).find('div:eq(0)').html();
											var value = '';
											
											$('.modal-content .modal-body .row:eq('+index+') input').each(function(){
												if ($(this).is(':checked')) {
													value = $(this).val();
													if (value > 0) {
														cont++;
													}
												}
											});
											
											var aux = {
												'email'	:	email,
												'value'	:	value
											};
											datos[datos.length] = aux;
										}
									});
									
									if (cont > 0) {
										$.post(
										    "<?php echo base_url().$this->lang->lang() ?>/facturas/enviar_email",
										    {'fa_id':fa_id,'tipo':'F','datos':datos},
										    function(data){
										    	bootbox.alert('<?php echo lang("factura.email.ok") ?>');
										    	l.stop();
										    }
										);
									} else {
										bootbox.alert('<?php echo lang("emails.seleccion") ?>');
										l.stop();
									}
								}
							}
						}
					});
				});
				
				$('.impuesto').html(iva);
		});
</script>