<?php if ($this->cart->contents() == '' || $this->cart->contents() == null) { ?>
	<h2><i class="fa fa-shopping-cart"></i> <?php echo lang('carrito.vacio')?></h2>
<?php } else { ?>

<script>
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
return ((key >= 48 && key <= 57) || (key==8) || (key == 46));
}
</script>

<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th class="col-sm-1"><?php echo lang("eliminar.producto") ?></th>
                		<th><?php echo lang("referencia") ?></th>
                		<th><?php echo lang("descripcion") ?></th>
                		<th><?php echo lang("caracteristicas") ?></th>
                		<th class="col-sm-1"><?php echo lang("precio").' '.$moneda ?></th>
                		<th class="col-sm-1"><?php echo lang("cantidad") ?></th>
                		<th class="col-sm-2 text-right"><?php echo lang("sub.total").' '.$moneda ?></th>
					</tr>
				</thead>
                                        
				<tfoot>
					<tr>
					  <td colspan="6" class="text-right"><strong><?php echo lang("total") ?></strong></td>
					  <td id="getsubtotal" class="text-right"><strong><?php echo $this->cart->format_number($this->cart->total()) ?></strong></td>
					</tr>
				</tfoot>
				
				<tbody id="lista_carrito" aria-relevant="all" aria-live="polite" role="alert">
					<?php
					//ORDENAMOS EL CARRITO ANTES DE MOSTRAR
					$listado = array();
					
					foreach ($this->cart->contents() as $carro) {
						array_push($listado, $carro);
					}
					
					for ($t=0; $t<count($listado)-1; $t++) {
						for ($x=$t+1; $x<count($listado); $x++) {
							if ($listado[$t]['options']['pro_id'] > $listado[$x]['options']['pro_id']) {
								$aux = $listado[$t];
								$listado[$t] = $listado[$x];
								$listado[$x] = $aux;
							}	
						}
					}
					
					for ($l=0; $l<count($listado); $l++) {
						$opciones = '';
						$referencia = '';
                                                
						foreach ($listado[$l]['options'] as $option_name => $option_value) {
                                                    if ($option_name == 'referencia') {
                                                        $referencia = $option_value;
                                                    } else if ($option_name != 'product_id' && $option_name != 'pro_id') {
							$opciones = $opciones.'<span class="opc_product_list"><strong>'.$option_name.':</strong> <span>'.$option_value.'</span></span>';
                                                    }
						}
						
						echo '<tr product_id="'.$listado[$l]['options']['product_id'].'" pro_id="'.$listado[$l]['options']['pro_id'].'" rowid="'.$listado[$l]['rowid'].'"><td class="vcenter text-center"><i class="fa fa-times eliminar"></i></td><td class="vcenter">'.$referencia.'</td><td class="vcenter">'.$listado[$l]["name"].'</td><td class="vcenter">'.$opciones.'</td><td class="vcenter">'.(double)$listado[$l]["price"].'</td><td><input class="form-control text-center" type="text" name="'.$l.$listado[$l]["qty"].'" value="'.(double)$listado[$l]["qty"].'" onKeypress="return soloNumeros(event)"></td><td class="text-right vcenter">'.number_format(round((double)$listado[$l]["price"]*(double)$listado[$l]["qty"], 2),2,'.','').'</td></tr>';
					}
					 ?>
					
					
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-6 col-md-6">
	</div>
	
	<div class="col-lg-6 col-md-6">				
		<label><?php echo lang('presupuestos') ?></label>
		<select id="cotizaciones" name="pr_id" class="selectpicker form-control" data-live-search="true"></select>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<label><?php echo lang('observaciones.empresa') ?></label>
		<textarea class="form-control" id="observaciones_empresa"></textarea>
	</div>
	
	<div class="col-lg-6 col-md-6">
		<label><?php echo lang('observaciones.proveedor') ?></label>
		<textarea class="form-control" id="observaciones_proveedor"></textarea>
	</div>
</div>

<div class="row">
	<div class="col-lg-6 col-xs-12 fot">
		<label><?php echo lang('adjunto.carrito') ?></label>
		<div>
			<form class="fotos" action="<?php echo base_url().$this->lang->lang() ?>/proveedores/upload" method="post" enctype="multipart/form-data">
			<div class="input-group">
				<span class="input-group-btn">
					<span class="btn btn-primary btn-file">
						<i class="fa fa-file"></i> <?php echo lang('cargar.adjunto.carrito') ?>
						<input type="file" id="file-adjunto" name="archivos[]" multiple="multiple">
					</span>
				</span>
				<input type="text" class="form-control" readonly>
			</div>
			</form>
		</div>
		<div class="text-danger"><?php echo form_error("fotos") ?></div>
		<div id="archivos-adjuntos">			
			<?php 			
				$adjuntos = explode('|', $car_adjuntos); 
				foreach ($adjuntos as $key => $file) {
					if(strlen($file) > 0 ){					
						echo '<span file_name="'.$file.'" style="padding: 5px;">' .
								'<i class="fa fa-times pointer" style="margin-right: 5px;"></i>' .
								'<a target="_blank" href="'.base_url().'img/dynamic/adjuntos-carrito/'.$file . '">'.$file .'</a>' .
							'</span></br>';
					}
				}
			?>
		</div>
	</div>
</div>

<br/>

<div class="row">
					<div class="col-lg-12 text-right">
						<button id="eliminar_carrito" type="button" class="btn btn-default ladda-button" data-style="zoom-out"><i class="fa fa-trash-o"></i> <?php echo lang('eliminar.carrito') ?></button>
					
						
						<!-- SI PUEDE CREAR O EDITAR CONTENIDO MUESTRA EL BOTON -->
						<?php if ($this->session->userdata('emp_crear') == '1' || $this->session->userdata('emp_editar') == '1') { ?>
							<!-- <button id="actualizar_carrito" type="button" class="btn btn-success ladda-button" data-style="zoom-out">
								<i class="fa fa-refresh"></i> <?php echo lang('actualizar.carrito') ?></button> -->
							<button id="procesar_pedido" type="button" class="btn btn-danger ladda-button" data-style="zoom-out">
								<i class="fa fa-cart-arrow-down"></i> <?php echo lang('procesar.pedido') ?>
							</button>
                            <button id="guardar_carrito" type="button" class="btn btn-warning ladda-button" data-style="zoom-out">
								<i class="fa fa-save"></i> <?php echo lang('guardar.carrito') ?>
							</button>
						<?php } ?>
						
						<a href="<?php echo base_url().$this->lang->lang().'/proveedores/productos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
					</div>
				</div>

<script>
/* ESTILO Y BOTON ADJUNTOS */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function(){
	
	//RELLENAMOS EL CAMPO DE PRESUPUESTOS	
	$.ajax({
    	type: "POST",
        dataType: "json",
        url: "<?php echo base_url().$this->lang->lang() ?>/presupuestos/buscador",
        data:{'buscar':''},
        success: function(data) {
        	var listado = '<option value="0" selected>Para stock de inventario</option>';
	            
			$.each(data,function(indice) {
				if (data[indice]["pr_estado"] != '3') {
					listado = listado + '<option value="'+data[indice]["pr_id"]+'">'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>';
				}
			});
			
			$('#cotizaciones').append(listado);
        },
        complete: function() {
            $('#cotizaciones').selectpicker();
	        $('#cotizaciones').selectpicker('refresh'); 
        }
    });
	
	//DATOS CARRITO
	var datos = [];
	var l;
	
	/* ESTILO Y BOTON ADJUNTOS */
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
		var input = $(this).parents('.input-group').find(':text'),
			log = numFiles > 1 ? numFiles + ' archivos seleccionados' : label;
			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}	
	});
	
	//ENVIAR CARRITO
	function enviar_carrito() {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/actualizar_carrito",
			data: {'datos':datos},
			success: function(data) {
				//console.log($("#file-adjunto").get(0));
				var files = $("#file-adjunto").get(0).files;
		        var fileData = new FormData();

		        if(files.length > 0){		        	
			        for (var i = 0; i < files.length; i++) {
			            fileData.append("form"+i+"-"+data.car_id, files[i]);
			        }

			        $.ajax({
			            type: "POST",
			            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/upload_adjuntos",
			            dataType: "json",
			            contentType: false, // Not to set any content header
			            processData: false, // Not to process data
			            data: fileData,
			            success: function (result, status, xhr) {
							if (data) {
								//refrescar sin cache
								location.reload(true);
							}		                
			            },
			            error: function (xhr, status, error) {
			                alert(status + ' ' + error);
			            }
			        });
		        }else{
		        	if (data) {
						//refrescar sin cache
						location.reload(true);
					}
		        }
			},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	}
	
	//ELIMINAR TODO EL CARRITO
	$('#eliminar_carrito').click(function(e){
		e.preventDefault();
		
		l = Ladda.create( document.querySelector( "#eliminar_carrito" ) );
	 	l.start();
		
		datos.length = 0;
		
		$('#lista_carrito tr').each(function(){
			var aux = {
				'rowid' : $(this).attr('rowid'),
				'qty' : '0'
			};
			datos.push(aux);
		});
		
		enviar_carrito();
	});
	
	//BORRAR PRODUCTO
	$('.eliminar').click(function(e){
		e.preventDefault();
		
		l = Ladda.create( document.querySelector( "#actualizar_carrito" ) );
	 	l.start();
		
		var rowid = $(this).parent().parent().attr('rowid');
		
		datos.length = 0;
		
		$('#lista_carrito tr').each(function(){
			if ($(this).attr('rowid') == rowid) {
				var aux = {
					'rowid' : rowid,
					'qty' : '0'
				};
				datos.push(aux);
			}
		});
		
		enviar_carrito();
	});
	
	//ACTUALIZAR CARRITO
	function actualizar_carrito() {
		datos.length = 0;
		
		$('#lista_carrito tr').each(function(){
			var aux = {
				'rowid' : $(this).attr('rowid'),
				'qty' : $(this).find('input').val()
			};
			datos.push(aux);
		});
		
		enviar_carrito();
	}
	
	$('#actualizar_carrito').click(function(e){
		if($('#cotizaciones option:selected').val()) {
			l = Ladda.create( document.querySelector( "#actualizar_carrito" ) );
			l.start();
			actualizar_carrito();
		} else {
			bootbox.alert('<?php echo lang("error.carrito.cotizaciones") ?>');
		}
	});
	
	
	
	//GUARDAR PEDIDO Y PEDIR PRODUCTOS
	$('#procesar_pedido').click(function(e){
		e.preventDefault();
	
		l = Ladda.create( document.querySelector( "#procesar_pedido" ) );
	 	l.start();
		
		if($('#cotizaciones option:selected').val()) {
	 	
	 	var pro_id = '';
		var product_id = '';
		var product_cantidad = '';
		var product_cantidad_recibido = '';
		var pe_observaciones_empresa = $.trim($('#observaciones_empresa').val());
		var pe_observaciones_proveedor = $.trim($('#observaciones_proveedor').val());
		var pr_id = $('#cotizaciones option:selected').val();
		
		$('#lista_carrito tr').each(function(){
			pro_id = pro_id + parseInt($(this).attr('pro_id')) + ';';
			product_id = product_id + parseInt($(this).attr('product_id')) + ';';
			product_cantidad = product_cantidad + parseFloat($(this).find('input').val()) + ';';
			product_cantidad_recibido = product_cantidad_recibido + '0' + ';';
		});
		//data.car_id
		var dataPost = {
			//'car_id':car_id,
			'pro_id':pro_id, 
			'product_id':product_id, 
			'product_cantidad':product_cantidad, 
			'pe_observaciones_empresa':pe_observaciones_empresa,  
			'pe_observaciones_proveedor':pe_observaciones_proveedor, 
			'pr_id':pr_id, 
			'product_cantidad_recibido':product_cantidad_recibido,
		};
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/procesar_pedido",
			data: dataPost,
			success: function(data) {
				var files = $("#file-adjunto").get(0).files;
		        var fileData = new FormData();
		        var objCorreos = data.objCorreos;

		        if(files.length > 0){		        	
			        for (var i = 0; i < files.length; i++) {
			            fileData.append("form"+i+"-"+data.car_id, files[i]);
			        }

			        $.ajax({
			            type: "POST",
			            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/upload_adjuntos",
			            dataType: "json",
			            contentType: false, // Not to set any content header
			            processData: false, // Not to process data
			            data: fileData,
			            success: function (result, status, xhr) {
			            	objCorreos.adjuntos = result.result;

			            	$.ajax({
					            type: "POST",
					            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/send_mail_proveedores",
					            dataType: "json",
					            data: objCorreos,
					            success: function (result, status, xhr) {
									if (data.status) {					
										bootbox.alert('<?php echo lang("pedido.carrito.ok") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										var pagina = '<?php echo base_url().$this->lang->lang() ?>/proveedores/productos';
										setTimeout("location.href='"+pagina+"'", 5000);
									} else {
										l.stop();
									}		                
					            },
					            error: function (xhr, status, error) {
					                alert(status + ' ' + error);
					            }
					        });									                
			            },
			            error: function (xhr, status, error) {
			                alert(status + ' ' + error);
			            }
			        });
		        }else{

		        	objCorreos.adjuntos = "<?php echo $car_adjuntos; ?>";
					$.ajax({
			            type: "POST",
			            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/send_mail_proveedores",
			            dataType: "json",
			            data: objCorreos,
			            success: function (result, status, xhr) {
							if (data.status) {					
								bootbox.alert('<?php echo lang("pedido.carrito.ok") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								var pagina = '<?php echo base_url().$this->lang->lang() ?>/proveedores/productos';
								setTimeout("location.href='"+pagina+"'", 5000);
							} else {
								l.stop();
							}		                
			            },
			            error: function (xhr, status, error) {
			                alert(status + ' ' + error);
			            }
			        });
				}				
			},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
		
		} else {
			bootbox.alert('<?php echo lang("error.carrito.cotizaciones") ?>');
			l.stop();
		}
	});
	
	//GUARDAR CARRITO
	$('#guardar_carrito').click(function(e){
		e.preventDefault();
		
		l = Ladda.create( document.querySelector( "#guardar_carrito" ) );
	 	l.start();
		
		var total = 0;
		var totales = [];
		$('#lista_carrito tr td:last-child').each(function(){
			total = parseFloat($(this).text());
			totales.push(total);
		});
		var total = 0;
		while(totales.length > 0) { total += totales.pop() };
		$('#getsubtotal strong').text(parseFloat(total).toFixed(2));
	 	
	 	var pro_id = '';
		var product_id = '';
		var product_cantidad = '';
		var product_cantidad_recibido = '';
		var car_observaciones_empresa = $.trim($('#observaciones_empresa').val());
		var car_observaciones_proveedor = $.trim($('#observaciones_proveedor').val());
		var pr_id = $('#cotizaciones option:selected').val();
		var car_subtotal = parseFloat(total).toFixed(2);
		
		$('#lista_carrito tr').each(function(){
			pro_id = pro_id + parseInt($(this).attr('pro_id')) + ';';
			product_id = product_id + parseInt($(this).attr('product_id')) + ';';
			product_cantidad = product_cantidad + parseFloat($(this).find('input').val()) + ';';
			product_cantidad_recibido = product_cantidad_recibido + '0' + ';';
		});
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/guardar_carrito",
			data: {'pro_id':pro_id, 'product_id':product_id, 'product_cantidad':product_cantidad, 'car_observaciones_empresa':car_observaciones_empresa,  'car_observaciones_proveedor':car_observaciones_proveedor, 'car_subtotal':car_subtotal, 'pr_id':pr_id, 'product_cantidad_recibido':product_cantidad_recibido},
			success: function(data) {
				//console.log($("#file-adjunto").get(0));
				var files = $("#file-adjunto").get(0).files;
		        var fileData = new FormData();

		        if(files.length > 0){		        	
			        for (var i = 0; i < files.length; i++) {
			            fileData.append("form"+i+"-"+data.car_id, files[i]);
			        }

			        $.ajax({
			            type: "POST",
			            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/upload_adjuntos",
			            dataType: "json",
			            contentType: false, // Not to set any content header
			            processData: false, // Not to process data
			            data: fileData,
			            success: function (result, status, xhr) {
							if (data.status) {					
								bootbox.alert('<?php echo lang("guardar.carrito.ok") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								var pagina = '<?php echo base_url().$this->lang->lang() ?>/proveedores/carritos';
								setTimeout("location.href='"+pagina+"'", 5000);
							} else {
								l.stop();
							}		                
			            },
			            error: function (xhr, status, error) {
			                alert(status + ' ' + error);
			            }
			        });
		        }else{
		        	if (data.status) {					
						bootbox.alert('<?php echo lang("guardar.carrito.ok") ?>');
						//REDIRIGIMOS A LOS 5 SEGUNDOS
						var pagina = '<?php echo base_url().$this->lang->lang() ?>/proveedores/carritos';
						setTimeout("location.href='"+pagina+"'", 5000);
					} else {
						l.stop();
					}
		        }
			},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
});
</script>

<?php } ?>