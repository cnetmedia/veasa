<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th width="10%"><?php echo lang("referencia") ?></th>
                		<th width="25%"><?php echo lang("descripcion") ?></th>
                		<th width="20%"><?php echo lang("caracteristicas") ?></th>
                		<th width="10%"><?php echo lang("proveedor") ?></th>
                		<th width="5%"><?php echo lang("vista.previa") ?></th>
                		<th width="10%"><?php echo lang("precio") ?></th>
                		<th width="17%"><?php echo lang("pedir.producto") ?></th>
                		<th width="3%"><?php echo lang("editar") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("referencia") ?></th>
                		<th><?php echo lang("descripcion") ?></th>
                		<th><?php echo lang("caracteristicas") ?></th>
                		<th><?php echo lang("proveedor") ?></th>
                		<th><?php echo lang("vista.previa") ?></th>
                		<th><?php echo lang("precio") ?></th>
                		<th><?php echo lang("pedir.producto") ?></th>
                		<th><?php echo lang("editar") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	
	//MOSTRAR EMPLEADOS
	function mostrar_productos() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador_productos",
		    {'buscar':''},
		    function(data){
		    	
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE TIENDA GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						var pedido = false;
						if (data[indice]['cantidad'] != null) {
							pedido = true;
						}
												
							table = table + '<tr id="'+data[indice]['product_id']+'" pro_id="'+data[indice]['pro_id']+'">';
							table = table + '<td class="vcenter">'+data[indice]['product_referencia']+'</td>';
	  						table = table + '<td class="vcenter">'+data[indice]['product_nombre']+'</td>';
	  						
	  						var caracteristicas = data[indice]['product_caracteristicas'];
	  						caracteristicas = caracteristicas.split(';');
							//ELIMINAR ESPACIOS EN BLANCO
							caracteristicas = caracteristicas.filter(Boolean);
							for (var i=0; i<caracteristicas.length; i++) {
								caracteristicas[i] = caracteristicas[i].split('/');
							}
							
							var opciones = '';
							for (var i=0; i<caracteristicas.length; i++) {
								opciones = opciones + '<span class="opc_product_list"><strong>' + caracteristicas[i][0] + '</strong>: <span>' + caracteristicas[i][1] + '</span></span>';
							}
	  						
	  						table = table + '<td class="vcenter">'+opciones+'</td>';
	  						table = table + '<td class="vcenter">'+data[indice]['pro_nombre']+'</td>';
	  						table = table + '<td class="text-center vcenter">'+data[indice]['fotos']+'</td>';
	  						table = table + '<td class="text-center vcenter">'+data[indice]['product_precio']+' '+data[indice]['currrency_symbol']+'</td>';
	  						
	  						if (pedido) {
								table = table + '<td><div class="input-group margin"><input class="form-control text-center" type="text" style="min-width:70px" value="'+data[indice]['cantidad']+'" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');"><span class="input-group-btn"><button id="btn'+indice+'" class="btn btn-success btn-flat ladda-button" data-style="zoom-out" type="button"><i class="fa fa-check"></i> <span class="text"><?php echo lang("pedido") ?></span></button></span></div></td>';
							} else {
								table = table + '<td><div class="comprar input-group margin"><input class="form-control text-center" type="text" value="1" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');"><span class="input-group-btn"><button id="btn'+indice+'" class="btn btn-primary btn-flat ladda-button" data-style="zoom-out" type="button"><i class="fa fa-shopping-cart"></i> <span class="text"><?php echo lang("pedir") ?></span></button></span></div></td>';
							}
	  						
	  						if (tipo > 1) {
								table = table + '<td></td>';
							} else {
								table = table + '<td class="editar text-center vcenter"><i class="fa fa-pencil-square-o"></i></td>';
							}
	  						
	  						
	  						table = table + '</tr>';
						
					});
					
					$('#listar tbody').html(table);
					
					$("#example1").dataTable({
						"aaSorting": [[ 3, "asc"],[ 0, "asc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sImport": "<i class='fa fa-upload'></i> <?php echo lang('importar.excel') ?>",
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-th-large'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//RESTRINGIR ACCESO A IMPORTAR Y NUEVO
					if (tipo > 0) {
						$('#btn_import').hide();
						$('#listar #btn_nuevo').hide();
					}
					
					//PREGUNTAMOS SI PUEDE EDITAR
					var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
					if (editar == '0') {
						$('#listar table thead tr th:last, #listar #btn_import').hide();
						$('#listar table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#listar table tfoot tr th:last').hide();
					} else {
						//IMPORTAR EXCEL
						$('#btn_import').click(function(e){
							e.preventDefault();
							
							$.ajax({
								type: "POST",
								dataType: "json",
								url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador",
								data: {'buscar':''},
								async: true,
								success: function(data) {
									var options = '';
									
									options = options + '<option al_tipo="" value="">Ninguno (Tienda Online)</option>';									
									
									$.each(data,function(indice,valor) {
										var tipo = '<?php echo lang("deposito") ?>';
										if (data[indice]['al_tipo'] == 1) {
											tipo = '<?php echo lang("reciclaje") ?>';
										}
										
										options = options + '<option al_tipo="'+data[indice]['al_tipo']+'" value="'+data[indice]['al_id']+'">'+data[indice]['al_nombre']+' ('+tipo+')</option>';			
									});
									
									bootbox.dialog({
									message: "<form id='importar_productos' action='<?php echo base_url().$this->lang->lang() ?>/proveedores/importar_productos' method='post' enctype='multipart/form-data'><label><?php echo lang('excel.productos') ?></label><br><input type='file' id='documento' name='documento'><br><br><div class='form-group'><label><?php echo lang('almacenes') ?></label><select class='form-control' id='almacenes' name='al_id'>"+options+"</select><small>*Al seleccionar un almacen no se cargaran a la tienda</small></div><script>$(document).ready(function(){$('#almacenes').selectpicker();});<\/script></form>",
									buttons: {
										success: {
											label: "<?php echo lang('importar.productos') ?>",
											className: "btn-success",
											callback: function() {
												jQuery("#importar_productos").ajaxForm({
													dataType: 'json',
													data: {},
													success: function(data) {
														if (data.errors != '') {
															bootbox.alert(data.errors);
														} else {
															if (data.sql) {
																bootbox.alert('<?php echo lang("importar.excel.ok") ?>');
																//window.location.reload(true); 
															} else {
																bootbox.alert('<?php echo lang("error.ajax") ?>');
															}
														}
													},
													error: function(XMLHttpRequest, textStatus, errorThrown) {
														bootbox.alert('<?php echo lang("error.ajax") ?>');
													}
												}).submit();
											}
										}
									}
								});
								},
								error: function(XMLHttpRequest, textStatus, errorThrown) {
								bootbox.alert('<?php echo lang("error.ajax") ?>');
								l.stop();
								}
							});
						});
					}
					
					//IMAGENES
					$(".fancybox").fancybox({
						helpers : {
					   		title : false
					   	},
					   	padding: 0,
						openEffect : 'elastic',
						openSpeed  : 150,
						closeEffect : 'elastic',
						closeSpeed  : 150,
						closeClick : false
					});
			        
			        //PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
			        if (crear == '0' ) {
			        	$('#listar #btn_nuevo').hide();
			        }else if(tipo > 0){
			        	$('#listar #btn_nuevo').hide();
			        }
					
					//NUEVO PRODUCTO
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/proveedores/nuevo_producto');
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
			        
			        //EDITAR
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_color");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_color");
					});
					
					$("#example1").on("click","tbody tr td.editar",function(e) {
						e.preventDefault();
						
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/proveedores/editar_producto/'+$(this).parent().attr('id'));

					});
					
					//PEDIR PRODUCTO//
					//REINICIAR CANTIDAD
					$('.comprar input').focusout(function(e){
						e.preventDefault();
						if ($(this).val() == '') {
							$(this).val('1');
						}
					});
					
					//BOTON PEDIR
					$("#example1").on("click", ".comprar button", function(e){
						e.preventDefault();
						var idBtn = $(this).attr('id');
						if (!$('#'+idBtn).hasClass('btn-success')) {
							var l = Ladda.create( document.querySelector( '#'+idBtn ) );
	 						//l.start();
	 						
							var cantidad = $(this).parent().parent().find('input').val();
							var opciones = $(this).parent().parent().parent().prev().prev().prev().prev();
							var caracteristicas = 'pro_id/'+$(this).parent().parent().parent().parent().attr('pro_id')+';product_id/'+$(this).parent().parent().parent().parent().attr('id')+';referencia/'+$(this).parent().parent().parent().parent().find('td:first').html()+';';
							
							//CARACTERISTICAS
							$(opciones).find('.opc_product_list').each(function(){
								caracteristicas = caracteristicas + $(this).find('strong').html() + '/' + $(this).find('span').html() + ';';
							});
							
							var producto = {
								//'referencia': $(this).parent().parent().parent().parent().find('td:first').html(),
                                                                'nombre': $(this).parent().parent().parent().prev().prev().prev().prev().prev().html(),
								'caracteristicas': caracteristicas, 
								'precio': $(this).parent().parent().parent().prev().html(),
								'cantidad': cantidad,
                                                                'product_id': $(this).parent().parent().parent().parent().attr('id')
							};
							
							if (cantidad > 0 || cantidad != '') {
									$.ajax({
										type: "POST",
										dataType: "json",
									  	url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/pedir_producto",
									  	data: producto,
									  	success: function(data) {
									  		if (data) {
												$('#'+idBtn).find('i').removeClass('fa-shopping-cart');
										  		$('#'+idBtn).find('i').addClass('fa-check');
										  		$('#'+idBtn).removeClass('btn-primary');
										  		$('#'+idBtn).addClass('btn-success');
										  		$('#'+idBtn).find('.text').html('<?php echo lang("pedido") ?>');
											}
									  		
									  		l.stop();
									  	},
									  	error: function(XMLHttpRequest, textStatus, errorThrown) {
									   		bootbox.alert('<?php echo lang("error.ajax") ?>');
									   		l.stop();
									  	}
									});
							} else {
								bootbox.alert('<?php echo lang("pedir.producto.error") ?>');
								l.stop();
							}
						}
					});
				}
		    }, "json");
	}
	
	mostrar_productos();
});
</script>