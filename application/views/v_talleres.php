<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th>ID</th>
                		<th><?php echo lang("fecha.inicio") ?></th>
                		<th><?php echo lang("observaciones") ?></th>
                		<th><?php echo lang("presupuesto") ?></th>
                		<th><?php echo lang("mantenimiento") ?></th>
                		<th><?php echo lang("estado") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th>ID</th>
                		<th><?php echo lang("fecha.inicio") ?></th>
                		<th><?php echo lang("observaciones") ?></th>
                		<th><?php echo lang("presupuesto") ?></th>
                		<th><?php echo lang("mantenimiento") ?></th>
                		<th><?php echo lang("estado") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var em_id = '<?php echo $this->session->userdata("em_id") ?>';
	var su_id = '<?php echo $this->session->userdata("su_id") ?>';
	var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	var emp_editar = '<?php echo $this->session->userdata("emp_editar") ?>';
	var emp_crear = '<?php echo $this->session->userdata("emp_crear") ?>';
	
	//MOSTRAR EMPLEADOS
	function mostrar_trabajos_taller() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/talleres/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE ALMACENES GENERANDO LAS FILAS
					var table = '';
					var mostrar = false;
					$.each(data,function(indice,valor) {
						//if (data[indice]['ta_estado'] <= 2) {
							if (emp_tipo <= 1) {
								mostrar = true;
							} else if (emp_tipo > 1 && em_id == data[indice]['em_id']) {
								mostrar = true;
							} else {
								mostrar = false;
							}
							
							if (mostrar) {
								table = table + '<tr id="'+data[indice]['ta_id']+'">';
								table = table + '<td>'+data[indice]['ta_id']+'</td>';
		  						table = table + '<td>'+data[indice]['ta_inicio']+'</td>';
		  						table = table + '<td>'+data[indice]['ta_observaciones']+'</td>';
		  						table = table + '<td>'+data[indice]['pr_numero']+'</td>';
		  						
		  						var man_nombre = (data[indice]['man_nombre'] !== null) ? data[indice]['man_nombre'] : '';
		  						table = table + '<td>'+man_nombre+'</td>';
		  						
		  						var estado = '<span class="label label-danger"><?php echo lang("pendiente") ?></span';
		  						if (data[indice]['ta_estado'] == 1) {
									estado = '<span class="label label-warning"><?php echo lang("procesando") ?></span>';
								} else if (data[indice]['ta_estado'] == 2) {
									estado = '<span class="label label-success"><?php echo lang("completado") ?></span>';
								}
		  						
		  						table = table + '<td>'+estado+'</td>';
		  						table = table + '</tr>';
							}
						//}
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 1, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-archive'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
			        
			        //PREGUNTAMOS SI PUEDE CREAR
			        var dep = '<?php echo $this->session->userdata("de_id") ?>';
					if (emp_tipo > 2 || emp_crear == 0) {
			        	$('#listar #btn_nuevo').hide();
			        } else {
						if ((dep == '3')||(dep == '4')||(dep == '5')) { //Si pertenece a cotizaciones, contabilidad o produccion no puede crear
							$('#listar #btn_nuevo').hide();
						}
					}
					
					//NUEVO ALMACEN
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/taller/nuevo_trabajo_taller?tag=true');
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
			        
			        //EDITAR O VER ALMACEN
			        if (emp_tipo <= 2 && emp_editar == 1) {
						$("#example1").on("mouseover","tbody tr",function(event) {
							$(this).find('td').addClass("fila_tabla");
						});
					   
					   	$("#example1").on("mouseout","tbody tr",function(event) {
							$(this).find('td').removeClass("fila_tabla");
						});
						
						$("#example1").on("click", "tbody tr", function(e){
							e.preventDefault();
							$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/taller/editar_trabajo_taller/'+$(this).attr('id')+'?tag=true');
						});
					}
				}
		    }, "json");
	}
	
	mostrar_trabajos_taller();
});
</script>