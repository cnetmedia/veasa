<div class="nav-tabs-custom" id="trabajos">
	<ul class="nav nav-tabs">
    	<li class="active">
        	<a href="#pendientes" class="pendientes_btn" data-toggle="tab"><?php echo lang('trabajos.guardados') ?></a>
        </li>        
    </ul>
        
    <div class="tab-content">
    	<div class="tab-pane active" id="pendientes">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example1_info" id="pendientes_table" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th width="15%"><?php echo lang("fecha") ?></th>
                                <th width="5%"><?php echo lang("estado") ?></th>
                                <th><?php echo lang("mantenimientos") ?></th>
                                <th><?php echo lang("sucursal") ?></th>
                            </tr>
                        </thead>
                                                
                        <tfoot>
                            <tr>
                                <th><?php echo lang("fecha") ?></th>
                                <th><?php echo lang("estado") ?></th>
                                <th><?php echo lang("mantenimientos") ?></th>
                                <th><?php echo lang("sucursal") ?></th>
                            </tr>
                        </tfoot>
                        
                        <tbody aria-relevant="all" aria-live="polite" role="alert">
                            
                        </tbody>
                    </table>		
				</div>
			</div>
    	</div>        
    </div>
</div>

<script>
$(document).ready(function(){
	
	mostrar_trabajos_pendientes();
	
	$('.pendientes_btn').click(function(){
		mostrar_trabajos_pendientes();
	});
	
	
	//MOSTRAR PENDIENTES
	function mostrar_trabajos_pendientes() {
	
		$("#pendientes_table").dataTable().fnDestroy();
		
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/trabajos/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE SUCURSALES GENERANDO LAS FILAS
					var table = '';
					
					$.each(data,function(indice,valor) {
						//PREGUNTAMOS SI ES GERENTE PARA MOSTRAR LOS DESHABILITADOS O NO Y LA CLASE DE COLOR
						
						table = table + '<tr id="'+data[indice]['tr_id']+'">';
	  					table = table + '<td>'+data[indice]['tr_fecha_inicio']+'</td>';
						var estado;
	  					estado = '<span class="label label-danger"><?php echo lang("pendiente") ?></span>';
	  					table = table + '<td>'+estado+'</td>';
						
						var mantdata = data[indice]['man_id'];
						var mant = mantdata.split(";");
						table = table + '<td>';
						for(count = 0; count < mant.length-1; count++){
							datos = mant[count].split(",");
							table = table + '<strong>' + datos[0] + ': </strong>' + datos[3] + '<br/>';
						}	
						table = table + '</td>';
						
	  					table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  					table = table + '</tr>';
						
					});
					$('#pendientes_table tbody').html(table);
					
					$("#pendientes_table").dataTable({
						"aaSorting": [[ 0, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-gears'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>',
							"fnInitComplete": function() {
					
								$("#pendientes_table").removeAttr('style');
							}
						}
					});
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					if (tipo > 0) {
						$('#pendientes table thead tr th:last').hide();
						$('#pendientes table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#pendientes table tfoot tr th:last').hide();
					}
					
					//PREGUNTAMOS SI PUEDE CREAR
					var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
					var dep = '<?php echo $this->session->userdata("de_id") ?>';
			        if (crear == '0') {
                        $('#pendientes #btn_nuevo').hide();
			        } else {
						if ((dep == '3') || (dep == '4') || (dep == '5')) { //Si pertenece a contabilidad, cotizaciones o operaciones no puede crear
							 $('#pendientes #btn_nuevo').hide();
						}
					}
					
					//NUEVO TRABAJO
					$("#btn_nuevo").click(function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/trabajos/nuevo_trabajo');
					});
					
					//REFRESCAR
					$("#btn_refrecar").click(function(e){
						e.preventDefault();
						window.location.reload(true); 
					});
	
			        //EDITAR O VER TRABAJO
			        $("#pendientes_table").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#pendientes_table").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#pendientes_table").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/trabajos/editar_trabajo/'+$(this).attr('id')+'?trg=true');
					});
					
					//$("#pendientes_table, #pendientes_table thead th").removeAttr('style');
				}
		    }, "json");
	}	
});
</script>