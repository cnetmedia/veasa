<?php if ($DATOS_EMPLEADO == null) { ?>
<script>
	$(document).ready(function(){
		//MOSTRAMOS ALERTA
		bootbox.dialog({
			message: "<?php echo lang('empleado.restaurar.clave.ko') ?>",
			buttons: {
				success: {
					label: "<?php echo lang('login.titulo') ?>",
					className: "btn-primary",
					callback: function(result) {
						var url = '<?php echo base_url() ?>empleados/salir';
						$(location).attr('href',url);
					}
				}
			}
		});
		//REDIRIGIMOS A LA PAGINA DE LOGIN PASADOS 5 SEGUNDOS
		setTimeout("location.href='<?php echo base_url() ?>empleados/salir'", 5000);
	});
</script>
<?php } else { ?>
	<?php		
		$input_clave_uno = array(
			'name'		=>	'emp_clave_uno',
			'id'		=>	'emp_clave_uno',
			'class'		=>	'form-control',
			'maxlength'	=>	'6',
			'value'		=>	set_value('emp_clave_uno')
		);
		
		$input_clave_dos = array(
			'name'		=>	'emp_clave_dos',
			'id'		=>	'emp_clave_dos',
			'class'		=>	'form-control',
			'maxlength'	=>	'6',
			'value'		=>	set_value('emp_clave_dos')
		);
		
		$emp_id = array(
			'name'		=>	'emp_id',
			'id'		=>	'emp_id',
			'class'		=>	'form-control',
			'value'		=>	$DATOS_EMPLEADO->emp_id
		);
		
		$boton = array(
			'name'		=>	'btn_login',
			'id'		=>	'btn_login',
			'class'		=>	'btn btn-lg btn-primary btn-block',
			'value'		=>	lang('entrar')
		);
		
		$data = array(
		    'name'		=>	'btn_login',
		    'id'		=>	'btn_login',
		    'class'		=>	'btn btn-lg btn-primary btn-block ladda-button',
		    'value'		=>	lang('restaurar.clave'),
		    'content'	=>	lang('restaurar.clave'),
		    'type'		=>	'submit',
		    'data-style'=>	'zoom-out'
		);
		
		$lb_error = array(
			'class'		=>	'control-label'
		);

		?>

	<div class="panel panel-default" id="login">
	  <div class="panel-heading">
	  	<?php echo lang('mensaje.restaurar.clave.empleado') ?>
	  </div>
	  
	  <div class="panel-body">
	  	<!-- INICIAMOS EL FORMULARIO -->
		<?php echo form_open() ?>
		
		<div class="form-group">
			<?php echo form_label(lang('login.clave'),'lb_clave_uno',$lb_error) ?>
			<?php echo form_password($input_clave_uno) ?>
			<span class="glyphicon glyphicon-remove form-control-feedback"></span>
			<div class="text-danger"><?php echo form_error('emp_clave_uno') ?></div>
		</div>
		
		<div class="form-group">
			<?php echo form_label(lang('repetir.clave'),'lb_clave_dos',$lb_error) ?>
			<?php echo form_password($input_clave_dos) ?>
			<span class="glyphicon glyphicon-remove form-control-feedback"></span>
			<div class="text-danger"><?php echo form_error('emp_clave_dos') ?></div>
		</div>
		

		
		<div><?php echo form_button($data) ?></div>
		<?php echo form_close() ?>
		<!-- CERRAMOS EL FORMULARIO -->
	  </div>
	</div>

	<script>
	$(document).ready(function(){
		//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO
		$('#login .form-group span').hide();
		
		$("#login form button").click(function(e){
		    e.preventDefault();
		    //GENERAMOS Y CARGAMOS EL LOADING
		    var l = Ladda.create( document.querySelector( "#login form button" ) );
		 	l.start();
		    //ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
		    $("#login .form-group").removeClass("has-error has-feedback");
		    $('#login .form-group span').hide();
		    $("#login .form-group .text-danger").html('');
		    //INICIO PETICION AJAX
		   	//CAPTURAMOS EL ENVIO DEL FORMULARIO LOGIN
			$.ajax({
				type: "POST",
				dataType: "json",
			  	url: "<?php echo base_url().$this->lang->lang() ?>/empleados/restaurar_clave",
			  	data: 'emp_clave_uno='+$('#emp_clave_uno').val()+'&emp_clave_dos='+$('#emp_clave_dos').val()+'&emp_id=<?php echo $DATOS_EMPLEADO->emp_id ?>',
			  	success: function(data) {
			  		if (data.sql) {
			    		if (data.status) {
			    			//LOGIN CORRECTO, REDIRIGIMOS
							bootbox.dialog({
								message: "<?php echo lang('empleado.restaurar.clave.ok') ?>",
								buttons: {
									success: {
										label: "<?php echo lang('login.titulo') ?>",
										className: "btn-primary",
										callback: function(result) {
											var url = '<?php echo base_url() ?>empleados/salir';
											$(location).attr('href',url);
										}
									}
								}
							});
							
							setTimeout("location.href='<?php echo base_url() ?>empleados/salir'", 5000);
			    		} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('span').show();
								}); 
							});
							
							if (data.clave == true) {
								$('#emp_clave_uno, #emp_clave_dos').val('');
							}
						}
			    	} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
			  	},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
					l.stop();
				}
			});
		});
	});
	</script>
<?php } ?>