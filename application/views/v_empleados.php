<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
						<th><?php echo lang("apellido") ?></th>
						<th><?php echo lang("departamento") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("email") ?></th>
                		<th><?php echo lang("dni") ?></th>
                		<th><?php echo lang("tipo") ?></th>
                		<th><?php echo lang("registro") ?></th>
						<th><?php echo lang("sucursal") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
						<th><?php echo lang("apellido") ?></th>
						<th><?php echo lang("departamento") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("email") ?></th>
                		<th><?php echo lang("dni") ?></th>
                		<th><?php echo lang("tipo") ?></th>
						<th><?php echo lang("registro") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	//MOSTRAR EMPLEADOS
	function mostrar_empleados() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/empleados/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						//MOSTRAMOS TODOS LOS DATOS SI ES SUPERUSUARIO O SOLO AL QUE PERTENECE SI NO LO ES
						/*var su_id = '<?php echo $this->session->userdata("su_id") ?>';
						var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						if (tipo == 0) {
							mostrar = true;
						} else if (su_id == data[indice]['su_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}*/
						
						var mostrar = false;
						var desact = '';
						var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						if (data[indice]['emp_estado'] == 0) {
							if (tipo <= 1) {
								mostrar = true;
								desact = 'desactivado';
							}
						} else {
							mostrar = true;
						}
						
						if (mostrar) {
							table = table + '<tr id="'+data[indice]['emp_id']+'" class="'+desact+'">';
	  						table = table + '<td>'+data[indice]['emp_nombre']+'</td>';
							table = table + '<td>'+data[indice]['emp_apellido1']+' '+data[indice]['emp_apellido2']+'</td>';
	  						table = table + '<td>'+data[indice]['de_nombre']+'</td>';
							table = table + '<td>'+data[indice]['emp_telefono']+'</td>';
	  						table = table + '<td>'+data[indice]['emp_email']+'</td>';
	  						table = table + '<td>'+data[indice]['emp_dni']+'</td>';
	  						//ESCRIBIMOS EL TIPO DE EMPLEADO
	  						var tipo = data[indice]['emp_tipo'];
	  						var txt_tipo = '';
	  						switch (tipo) {
			                    case ('0'):
			                        txt_tipo = "<?php echo lang('super') ?>";
			                        break;
			                    case ('1'):
			                       txt_tipo = "<?php echo lang('gerente') ?>";
			                        break;
			                    case ('2'):
			               			txt_tipo = "<?php echo lang('supervisor') ?>";
			                        break;
			                    case ('3'):
			               			txt_tipo = "<?php echo lang('empleado') ?>";
			                        break;
			                    case ('4'):
			               			txt_tipo = "<?php echo lang('administrador') ?>";
			                        break;
			                }
	  						table = table + '<td>'+txt_tipo+'</td>';
	  						table = table + '<td>'+data[indice]['emp_registro']+'</td>';
							table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  						table = table + '</tr>';
						}
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 7, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-sitemap'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					if (tipo > 0) {
						$('#listar table thead tr th:last').hide();
						$('#listar table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#listar table tfoot tr th:last').hide();
					}
			        
			        //PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
			        if (crear == '0') {
			        	$('#listar #btn_nuevo').hide();
			        }
					
					//NUEVO EMPLEADO
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/empleados/nuevo_empleado');
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
			        
			        //EDITAR O VER SUCURSAL
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
                                        $("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/empleados/editar_empleado/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_empleados();
});
</script>