<div class="nav-tabs-custom" id="trabajos">
	<ul class="nav nav-tabs">
    	<li class="active">
        	<a href="#completados" class="completados_btn" data-toggle="tab"><?php echo lang('trabajos') ?></a>
        </li>               
    </ul>
        
    <div class="tab-content">
    	<div class="tab-pane active" id="completados">
    		<div class="box-body table-responsive">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
					<table aria-describedby="example2_info" id="completados_table" class="table table-bordered table-striped table-hover dataTable">
		            	<thead>
                            <tr>
                                <th width="15%"><?php echo lang("fecha") ?></th>
                                <th width="5%"><?php echo lang("estado") ?></th>
                                <th><?php echo lang("mantenimientos") ?></th>
                                <th><?php echo lang("sucursal") ?></th>
                            </tr>
                        </thead>
                                                
                        <tfoot>
                            <tr>
                                <th><?php echo lang("fecha") ?></th>
                                <th><?php echo lang("estado") ?></th>
                                <th><?php echo lang("mantenimientos") ?></th>
                                <th><?php echo lang("sucursal") ?></th>
                            </tr>
                        </tfoot>
                        
                        <tbody aria-relevant="all" aria-live="polite" role="alert">
							
						</tbody>
					</table>	
				</div>
			</div>
    	</div>
    </div>
</div>

<script>
$(document).ready(function(){
	
	mostrar_trabajos_completados();
	
	$('.completados_btn').click(function(){
		mostrar_trabajos_completados();
	});
	
	//MOSTRAR COMPLETADOS
	function mostrar_trabajos_completados() {
		
		$("#completados_table").dataTable().fnDestroy();
		
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/trabajos/buscador_completados",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE SUCURSALES GENERANDO LAS FILAS
					var table = '';
					
					$.each(data,function(indice,valor) {
						//PREGUNTAMOS SI ES GERENTE PARA MOSTRAR LOS DESHABILITADOS O NO Y LA CLASE DE COLOR
						
						table = table + '<tr id="'+data[indice]['tr_id']+'">';
	  					table = table + '<td>'+data[indice]['tr_fecha_inicio']+'</td>';
						
						var estado;
						if (data[indice]['tr_estado'] == 1) {
							estado = '<span class="label label-warning"><?php echo lang("procesando") ?></span>';
						} else if (data[indice]['tr_estado'] == 2) {
							estado = '<span class="label label-success"><?php echo lang("completado") ?></span>';
						}
	  					table = table + '<td>'+estado+'</td>';
						
						var mantdata = data[indice]['man_id'];
						var mant = mantdata.split(";");
						table = table + '<td>';
						for(count = 0; count < mant.length-1; count++){
							datos = mant[count].split(",");
							table = table + '<strong>' + datos[0] + ': </strong>' + datos[3] + '<br/>';
						}
						table = table + '</td>';
						
	  					table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  					table = table + '</tr>';
						
					});
					
					$('#completados_table tbody').html(table);
					
					$("#completados_table").dataTable({
						"aaSorting": [[ 0, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-gears'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>',
							"fnInitComplete": function() {
					
								$("#completados_table").removeAttr('style');
							}
						}
					});
					
					$("#completados_table, #completados_table thead th").removeAttr('style');
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					if (tipo > 0) {
						$('#completados table thead tr th:last').hide();
						$('#completados table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#completados table tfoot tr th:last').hide();
					}
					
					//PREGUNTAMOS SI PUEDE CREAR
					var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
					var dep = '<?php echo $this->session->userdata("de_id") ?>';
					if (crear == '0') {
						$('#completados #btn_nuevo').hide();
					} else {
						if ((dep == '3') || (dep == '4') || (dep == '5')) { //Si pertenece a contabilidad, cotizaciones o operaciones no puede crear
							 $('#completados #btn_nuevo').hide();
						}
					}
					
					//NUEVO TRABAJO
					$("#btn_nuevo").click(function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/trabajos/nuevo_trabajo');
					});
					
					//REFRESCAR
					$("#btn_refrecar").click(function(e){
						e.preventDefault();
						window.location.reload(true); 
					});
	
			        //EDITAR O VER TRABAJO
			        $("#completados_table").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#completados_table").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#completados_table").on("click", "tbody tr", function(e){
						e.preventDefault();
						//$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/trabajos/ver_trabajo/'+$(this).attr('id'));
						if ( tipo == 3) {
							$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/trabajos/ver_trabajo/'+$(this).attr('id'));
						} else {
							$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/trabajos/editar_trabajo/'+$(this).attr('id'));
						}
					});
					
				}
		    }, "json");
	}
		
});
</script>