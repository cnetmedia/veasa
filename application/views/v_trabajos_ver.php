<div id="ver_trabajo">
<!-- SOLO PARA SUPERVISORES -->
<?php if ($this->session->userdata('emp_tipo') <= 2) { ?>
	<div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
            	<a href="#tab_1" data-toggle="tab"><?php echo lang('mantenimientos') ?></a>
            </li>
            <li class="">
            	<a href="#tab_2" data-toggle="tab"><?php echo lang('inspecciones') ?></a>
            </li>
            <li class="">
	        	<a href="#tab_3" data-toggle="tab"><?php echo lang('bitacora') ?></a>
	        </li>
        </ul>
        
        <div class="tab-content">
        	<div class="tab-pane active" id="tab_1">
<?php } ?>

	<div class="row">
		<div class="col-lg-4 col-md-6" style="margin-bottom: 15px;">
			<div class="form-group">
				<label><?php echo lang('mapa.direccion.origen') ?></label>
			   	<div class="radio">
					<label>
				    	<input type="radio" name="direcc" value="0" /> <?php echo lang('mi.posicion') ?>
				    </label>
				    
				    <label>
				    	<input type="radio" name="direcc" value="1" checked /> <?php echo lang('direccion') ?>
				  	</label>
				</div>
			</div>
			
			<div class="input-group">
            	<span id="btn_direcc" class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input class="form-control" id="direccion" type="text">
            </div>
            
            <div class="row" style="margin-top: 15px;">
            	<div class="col-lg-12">
            		<label><?php echo lang('mapa.direccion.destino') ?></label>
            		<div id="mantenimientos"></div>
            	</div>
            </div>
		</div>
		
		<div class="col-lg-8 col-md-6" style="margin-bottom: 15px;">
			<div class="row" style="margin-bottom: 15px;">
				<div class="col-lg-6">
					<label><?php echo lang('tipo.mapa') ?></label>
					<select id="tipo_mapa" class="form-control">
			      		<option value="roadmap" selected><?php echo lang('mapa.carretera') ?></option>
			      		<option value="satellite"><?php echo lang('mapa.satelite') ?></option>
			      		<option value="hybrid"><?php echo lang('mapa.hibrido') ?></option>
			      		<option value="terrain"><?php echo lang('mapa.terrestre') ?></option>
			    	</select>
				</div>
				
				<div class="col-lg-6">
					<label><?php echo lang('tipo.ruta') ?></label>
					<select id="tipo_ruta" class="form-control">
			      		<option value="DRIVING" selected><?php echo lang('mapa.coche') ?></option>
			      		<option value="WALKING"><?php echo lang('mapa.andando') ?></option>
			      		<option value="BICYCLING"><?php echo lang('mapa.bicicleta') ?></option>
			    	</select>
				</div>
			</div>
			
			<div id="mapa_ver_trabajos"></div>
			<img id="load_map" src="<?php echo base_url() ?>img/load.gif" />
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<label><?php echo lang("indicaciones.ruta") ?></label>
		</div>
		
		<div class="col-lg-12" id="indicaciones"></div>
	</div>
	
	<!-- SOLO PARA SUPERVISORES -->
	<?php if ($this->session->userdata('emp_tipo') <= 2) { ?>
		</div>
            <div class="tab-pane" id="tab_2">
            	<div class="row">
            		<div class="col-lg-4 col-ms-4 col-sm-6 col-xs-12">
            			<label><?php echo lang('mantenimientos') ?></label>
            			<select id="inspecc_mant" class="form-control"></select>
            		</div>
            	</div>
            	
            	<br>
            	
                <div class="row" id="inspec">
                	
                </div>
            </div>

            <div class="tab-pane" id="tab_3">
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<table id="bitacora" class="table table-bordered">
	    					<thead>
	    						<th class="col-sm-2"><?php echo lang('fecha') ?></th>
	                            <th class="col-sm-3"><?php echo lang('empleados') ?></th>
	                            
	    					</thead>
	    					
	    					<tbody></tbody>
	    					
	    					<tfoot>
		    					<tr>
		    						
		    					</tr>
	    					</tfoot>
	    				</table>
	    			</div>
	    		</div>
	    	</div>
        </div>
    </div>
	<?php } ?>
	
	<div class="row">
		<div class="col-lg-12 text-right">	
			<a href="<?php echo base_url().$this->lang->lang().'/trabajos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
		</div>
	</div>
	
</div>

<script>
$(document).ready(function(){	
	var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	
	$('#load_map, #inspecciones div:eq(0) label').hide();
	$('#indicaciones').prev().find('label').hide();
	
	//VARIABLE GLOBALES
	var tr_id = '<?php echo $tr_id ?>';
	var ruta = new Array();
	var orig = 1;
	var direccion = '';
	var latitud = '';
	var longitud = '';
	var precision = '';
	var locations = '<?php echo $man_id ?>';
	locations = locations.split(';');
	//BORRA ELEMENTOS VACIOS
	locations = locations.filter(Boolean);
	var mantenimientos = new Array();
	for (var i=0; i<locations.length; i++) {
		var aux = locations[i].split(',');
		mantenimientos.push(aux);
	}
	locations = mantenimientos;
	
	var map;
	//MAPA CON PUNTOS DE LOCALIZACION
	var directionsDisplay;
	var directionsService = new google.maps.DirectionsService();
	var geocoder;
	
	//MANTENIMIENTOS
	var aux = '';
	for (var x=0; x<locations.length; x++) {
		if ((x%2) == 0) {
			aux = aux + '<div class="callout callout-info" id="'+locations[x][0]+'">';
		} else {
			aux = aux + '<div class="callout" id="'+locations[x][0]+'">';
		}
						
		aux = aux + '<div class="nombre">'+locations[x][3]+'</div>';
		aux = aux + '<div class="latitud">'+locations[x][1]+'</div>';
		aux = aux + '<div class="longitud">'+locations[x][2]+'</div>';
		aux = aux + '</div>';
	}
	$('#mantenimientos').html(aux);
	
	//OCULTAMOS LA LATITUD Y LONGITUD
	$('#mantenimientos .latitud, #mantenimientos .longitud').hide();
	
	//CALCULAR LA RUTA	
	function calcRoute() {		
		var start = ruta[0];
		var end = ruta[1];

		var request = {
			origin: start,
		    destination: end,
		    optimizeWaypoints: true,
		    travelMode: $('#tipo_ruta option:selected').val()
		};
		
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
			    directionsDisplay.setPanel(document.getElementById("indicaciones"));
			    directionsDisplay.setDirections(response);
			    $('#indicaciones').prev().find('label').show();
		  	} else if (status === google.maps.DirectionsStatus.ZERO_RESULTS) {
				bootbox.alert('<?php echo lang("ruta.error") ?>');
			}
		});
	}
	
	//COMPRUEBA QUE SE PUEDE OBTENER LA GEOLOCALIZACION
	function localize() {
		/* Si se puede obtener la localización */
	 	if (navigator.geolocation) {
	 		$('#mapa_ver_trabajos').css("opacity", 0.5);
	 		$('#load_map').show();
            navigator.geolocation.getCurrentPosition(mapa,error,{timeout: 10000});
        /* Si el navegador no soporta la recuperación de la geolocalización */
        } else {
            bootbox.alert('<?php echo lang("geolocalizacion.error") ?>');
            $('#mapa_ver_trabajos').css("opacity", 1);
            $('#load_map').hide();
        }
	}
	
	//MAPA
	function mapa(pos) {
		$('#indicaciones').html('');
		$('#indicaciones').prev().find('label').hide();
		
		/* Obtenemos los parámetros de la API de geolocalización HTML*/
		/* SOLO OBTENEMOS LAS COORDENADAS UNA VEZ */
		if (latitud == '' && orig == 0) {
			latitud = pos.coords.latitude;
			longitud = pos.coords.longitude;
			precision = pos.coords.accuracy;
		}
		
		$('#mapa_ver_trabajos').css("opacity", 1);
		$('#load_map').hide();
		
		/* A través del DOM obtenemos el div que va a contener el mapa */
		var contenedor = document.getElementById("mapa_ver_trabajos")
		/* Definimos las propiedades del mapa */
		var propiedades = {
            mapTypeId: $('#tipo_mapa option:selected').val()
		};

		/* Creamos el mapa pasandole el div que lo va a contener y las diferentes propiedades*/
		map = new google.maps.Map(contenedor, propiedades);

		/* Un servicio que proporciona la API de GM es colocar marcadores sobre el mapa */
		if (orig == 0) {
			ruta[0] = latitud+','+longitud;
			var centro = new google.maps.LatLng(latitud,longitud);
			var marcador = new google.maps.Marker({
	            position: centro,
	            map: map,
	            icon: "<?php echo base_url() ?>img/marker_person.png"
	        });
		} else {
			if (direccion != '') {
				geocoder = new google.maps.Geocoder();
				geocoder.geocode( { 'address': direccion}, function(results, status) {
				    if (status == google.maps.GeocoderStatus.OK) {
				      	var marker = new google.maps.Marker({
				        	map: map,
				          	position: results[0].geometry.location,
				          	icon: "<?php echo base_url() ?>img/marker_person.png"
				      	});
				      	ruta[0] = direccion;
				    } else {
				      	bootbox.alert('<?php echo lang("direccion.error") ?>');
				    }
				});
			}
		}
		
		directionsDisplay = new google.maps.DirectionsRenderer({
		    //COLOR RUTA
		    polylineOptions: {
		      	strokeColor: "#D9534F",
		      	strokeOpacity: 0.7,
	    		strokeWeight: 5
			},
			//ELIMINA LOS PUNTOS A B C D E...
			suppressMarkers: true
		});
		
		directionsDisplay.setMap(map);
	    
	    var bounds = new google.maps.LatLngBounds();
	    var infowindow = new google.maps.InfoWindow();

	    var marker, i;
	    
	    for (i = 0; i < locations.length; i++) {
	    	var entrar = false;
	    	var co = locations[i][1]+','+locations[i][2];
	    	
	    	if (ruta.length == 2) {
				if (co == ruta[0] || co == ruta[1]) {
					entrar = true;
				}
			} else {
				entrar = true;
			}
	    	
	    	if (entrar) {
				marker = new google.maps.Marker({
		        	position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		        	map: map,
		        	icon: "<?php echo base_url() ?>img/pointer-view.png"
		      	});
			    
		      	//extend the bounds to include each marker's position
				bounds.extend(marker.position);
				
		      	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		        	return function() {
		          		infowindow.setContent(locations[i][3]);
		          		infowindow.open(map, marker);
		        	}
		      	})(marker, i));
		      
		      	//now fit the map to the newly inclusive bounds
				map.fitBounds(bounds);
					
				//(optional) restore the zoom level after the map is done scaling
				/*var listener = google.maps.event.addListener(map, "idle", function () {
				    map.setZoom(3);
				    google.maps.event.removeListener(listener);
				});*/	
			}
	    }
        
        if (orig == 0) {
			$('#direccion').val(latitud+', '+longitud);
        	$('#direccion').attr('disabled',true);
		}
		
		if (ruta.length == 2) {
			calcRoute();
		}
	}
	
	//INICIALIZAMOS EL MAPA
	mapa();

	/* Gestion de errores */
	function error(errorCode) {
		alert(errorCode.code);
		if(errorCode.code == 1) {
			bootbox.alert('<?php echo lang("permiter.locatizalicon") ?>');
		} else if (errorCode.code==2) {
			bootbox.alert('<?php echo lang("localizacion.error") ?>');
		} else {
			bootbox.alert('<?php echo lang("map.error") ?>');
		}
		
		$('#mapa_ver_trabajos').css("opacity", 1);
		$('#load_map').hide();
	}
	
	//DIRECCION DE ORIGEN
	function origen() {		
		$('#ver_trabajo .radio input').each(function(){
			if ($(this).is(':checked')) {
				orig = $(this).val();
			}
		});
		
		if (orig == 0) {
			ruta = new Array();
			$('#mantenimientos .callout').removeClass('active');
			localize();			
		} else {
			$('#direccion').attr('disabled',false);
			$('#direccion').val(direccion);
			ruta = new Array();
			ruta[0] = direccion;
			$('#mantenimientos .callout').removeClass('active');
			mapa();
		}
	}
	
	$("#ver_trabajo .radio *").click(function () {
		origen();
	});
	
	//BUSCAR DIRECCION
	function capturar_direccion() {
		if (orig == 1) {
			//PREGUNTA SI YA TIENE ESCRITA UNA DIRECCION O NO
			if ($('#btn_direcc i').hasClass('fa-times')) {
				$('#direccion').val('');
				ruta = new Array();
				$('#btn_direcc i').removeClass('fa-times');
				$('#btn_direcc i').addClass('fa-map-marker');
				$('#mantenimientos .callout').removeClass('active');
			} else {
				$('#btn_direcc i').removeClass('fa-map-marker');
				$('#btn_direcc i').addClass('fa-times');
			}
			
			direccion = $('#direccion').val();
			mapa();
		}
	}
	
	$('#btn_direcc').click(function(){
		capturar_direccion();
	});
	
	$(document).keypress(function(e){
		if (e.which == 13) {
			capturar_direccion();
		}
	});
	
	//MANTENIMIENTOS
	$('#mantenimientos .callout').click(function(){
		var cor = $(this).find('.latitud').html()+','+$(this).find('.longitud').html();
		
		if ($(this).hasClass('active')) {		
			if (cor == ruta[0]) {
				ruta[0] = '';
			} else if (cor == ruta[1]) {
				ruta[1] = '';
			}
		} else {
			if (ruta.length == 0 || ruta[0] == '') {
				ruta[0] = $(this).find('.latitud').html()+','+$(this).find('.longitud').html();
			} else {
				ruta[1] = $(this).find('.latitud').html()+','+$(this).find('.longitud').html();
			}
		}
		
		$('#mantenimientos .callout').each(function(){
			var m = $(this).find('.latitud').html()+','+$(this).find('.longitud').html();
			
			if (m == ruta[0] || m == ruta[1]) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
		
		if (ruta[0] == '' || ruta[1] == '') {
			ruta = new Array();
		}
		
		mapa();
	});
	
	//VISTA MAPA
	$("#tipo_mapa").change(function () {
		mapa();
	}).trigger('change');
	
	//VISTA RUTA
	$("#tipo_ruta").change(function () {
		calcRoute();
	}).trigger('change');
	
	
	
	/* INSPECCIONES
	-------------------------------------------
	*/
	
	
	//SUPERVISORES
	var mant_cerrado = 0;
	if (emp_tipo <= 2) {
		//RECUPERAMOS LAS FOTOS DE LA INSPECCION
		/*function imagenes(in_id) {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url().$this->lang->lang() ?>/trabajos/imagenes",
				data: 'tr_id=<?php echo $tr_id ?>&in_id='+in_id+'&man_id='+$('#inspecc_mant option:selected').val(),
				success: function(data) {
					$('.inspecciones').each(function(){
						if ($(this).attr('id') == in_id) {
							console.log('ok');
							$(this).find('.fot div:last').html(data);
						}
					});
					//$('#'+in_id+' .fot div:last').html(data);
				}
			});
		}
		
		//RECUPERAMOS LAS FOTOS DE LA INSPECCION
		function imagenes_avanzadas(inv_id) {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url().$this->lang->lang() ?>/trabajos/imagenes_avanzadas",
				data: 'tr_id=<?php echo $tr_id ?>&inv_id='+inv_id+'&man_id='+$('#inspecc_mant option:selected').val(),
				success: function(datos) {
					return datos;
					$('.inspecciones_avanzadas').each(function(){
						if ($(this).attr('id') == inv_id) {
							console.log('okk');
							$(this).find('.fot div:last').html(data);
						}
					});
					//$('#'+inv_id+' .fot div:last').html(data);
				}
			});
		}*/
		
		//CREAMOS LAS INSPECCIONES BASICAS Y AVANZADAS DE CADA MANTENIMIENTO
		function inspecciones_trabajo() {
			//INSPECCIONES DEL TRABAJO
			var in_id = '<?php echo isset($in_id)?$in_id:"" ?>';
			var inv_id = '<?php echo isset($inv_id)?$inv_id:"" ?>';
			
			//INSPECCIONES BASICAS
			if (in_id != '') {
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "<?php echo base_url().$this->lang->lang() ?>/inspecciones/inspecciones_trabajo",
					data: 'tr_id=<?php echo $tr_id ?>&in_id='+in_id+'&man_id='+$('#inspecc_mant option:selected').val(),
					success: function(data) {
						var insp = '';
											
						$.each(data,function(indice) {
							//PREGUNTAMOS SI ESTA INSPECCION YA FUE CERRADA
							var read = '';
							if (data[indice]['ti_estado'] == 2) {
								read = 'disabled="disabled"';
							}
							
							insp = insp + '<div class="col-lg-12" id="in'+data[indice]['in_id']+'"><div class="box box-solid box-primary"><div class="box-header"><h3 class="box-title">' + data[indice]['in_nombre'] + '</h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm encoger_inspeccion"><i class="fa fa-minus"></i></button></div></div>';
										
							//TITULOS
							var in_titulo = data[indice]['in_titulo'];
							in_titulo = in_titulo.split(';');
							if (in_titulo[in_titulo.length-1] == '') {
								in_titulo.splice(in_titulo.length-1,1);
							}
							
							//DESCRIPCIONES
							var in_descripcion = data[indice]['in_descripcion'];
							in_descripcion = in_descripcion.split(';');
							if (in_descripcion[in_descripcion.length-1] == '') {
								in_descripcion.splice(in_descripcion.length-1,1);
							}
							
							//ANCHOS
							var in_ancho = data[indice]['in_ancho'];
							in_ancho = in_ancho.split(';');
							if (in_ancho[in_ancho.length-1] == '') {
								in_ancho.splice(in_ancho.length-1,1);
							}
							
							//ALTOS
							var in_alto = data[indice]['in_alto'];
							in_alto = in_alto.split(';');
							if (in_alto[in_alto.length-1] == '') {
								in_alto.splice(in_alto.length-1,1);
							}
							
							//CANTIDADES
							var in_cantidad = data[indice]['in_cantidad'];
							in_cantidad = in_cantidad.split(';');
							if (in_cantidad[in_cantidad.length-1] == '') {
								in_cantidad.splice(in_cantidad.length-1,1);
							}
							
							//MONTOS
							var in_monto = data[indice]['in_monto'];
							in_monto = in_monto.split(';');
							if (in_monto[in_monto.length-1] == '') {
								in_monto.splice(in_monto.length-1,1);
							}
										
							//PREGUNTAS	
							var in_preguntas = data[indice]['in_preguntas'];
							in_preguntas = in_preguntas.split(';');
							if (in_preguntas[in_preguntas.length-1] == '') {
								in_preguntas.splice(in_preguntas.length-1,1);
							}
										
							//RESPUESTAS MARCADAS
							var in_resp_marcas = new Array();
							var array_resp_marcas = new Array();
							if (data[indice]['in_resp_marcas'] != null) {
								in_resp_marcas = data[indice]['in_resp_marcas'];
								in_resp_marcas = in_resp_marcas.split('/');
								if (in_resp_marcas[in_resp_marcas.length-1] == '') {
									in_resp_marcas.splice(in_resp_marcas.length-1,1);
								}
										
								for(var t=0; t<in_resp_marcas.length; t++) {
									var aux = in_resp_marcas[t].split(';');
									if (in_resp_marcas[t][in_resp_marcas[t].length-1] == '') {
										in_resp_marcas[t].splice(in_resp_marcas[t].length-1,1);
									}
									array_resp_marcas.push(aux);
								}
							}
										
							insp = insp + '<div class="row">';
							for (var i=0; i < in_titulo.length; i++) {
								insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12"><div class="box-body">';
								//ABRRIR GRUPO
								insp = insp + '<table><tbody><tr>';
								insp = insp + '<td class="col-sm-2"><label style="width:100%"><?php echo lang("tipo") ?></label><input class="form-control tipo" name="in_titulo" value="' + in_titulo[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-3"><label style="width:100%"><?php echo lang("descripcion") ?></label><input class="form-control descripcion" name="in_descripcion" value="' + in_descripcion[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-1"><label style="width:80px !important"><?php echo lang("ancho") ?></label><input class="form-control ancho" name="in_ancho" value="' + in_ancho[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-1"><label style="width:80px !important"><?php echo lang("alto") ?></label><input class="form-control alto" name="in_alto" value="' + in_alto[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-1"><label style="width:80px !important"><?php echo lang("cantidad") ?></label><input class="form-control cantidad" name="in_cantidad" value="' + in_cantidad[i] + '" type="text" readonly></td>';
								insp = insp + '<td class="col-sm-1"><label style="width:80px !important"><?php echo lang("monto") ?></label><input class="form-control monto" name="in_monto" value="' + in_monto[i] + '" type="text" readonly></td>';
											
								var array_preguntas = in_preguntas[i].split(',');
								
								for (var x=0; x<array_preguntas.length; x++) {
									
									//AGREGAR PREGUNTAS
									insp = insp + '<td class="pregunt col-sm-2" style="width:100px !important"><label style="width:80px !important">' + array_preguntas[x] + '</label><input class="form-control respuesta" name="resp' + indice + i + '_' + x + '" value="' + array_resp_marcas[i][x] + '" type="text" onKeypress="return soloNumeros(event)" '+read+'></td>';
								}
								insp = insp + '</tr></table></div></div>';
							}
										
							//insp = insp + '</div>'; /* FIN ROW */
							insp = insp + '<div class="row">';
									
							insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12 textar" style="padding: 0 50px 0 50px">';
							if (data[indice]['ti_comentario'] != null) {
								insp = insp + '<div class="form-group"><h4><strong><?php echo lang("comentario") ?></strong></h4><textarea '+read+'>'+ data[indice]['ti_comentario'] +'</textarea><span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger"><?php echo form_error("ti_comentario") ?></div></div>';
							} else {
								insp = insp + '<div class="form-group"><h4><strong><?php echo lang("comentario") ?></strong></h4><textarea '+read+'></textarea><span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger"><?php echo form_error("ti_comentario") ?></div></div>';
							}
							insp = insp + '</div>';
							insp = insp + '</div>'; /* FIN ROW */
									
							insp = insp + '</div></div>';
							insp = insp + '</div>'; /* FIN ROW */
						});
						
						$('#ver_trabajo #inspec').append(insp);
						
						//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
						$(".box .form-group").removeClass("has-error has-feedback");
						$('.box .glyphicon-remove').hide();
						$(".box .text-danger").html('');
						
						//IMAGENES
						$(".fancybox").fancybox({
							helpers : {
					    		title : false
					    	},
					    	padding: 0,
							openEffect : 'elastic',
							openSpeed  : 150,
							closeEffect : 'elastic',
							closeSpeed  : 150,
							closeClick : false
						});
						
						//ESTILO Y BOTON FOTOS
						$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
							var input = $(this).parents('.input-group').find(':text'),
								log = numFiles > 1 ? numFiles + ' files selected' : label;
								if( input.length ) {
									input.val(log);
								} else {
									if( log ) alert(log);
								}	
						});
						
						//GUARDAR INSPECCION
						$('.guardar_inspeccion,.completar_inspeccion').on('click', function(){
							//LOADING BOTON
							var btn_id = $(this).attr('id');
							var l = Ladda.create( document.querySelector( '#'+btn_id ) );
						 	l.start();
							
							//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
							$(".box .form-group").removeClass("has-error has-feedback");
							$('.box .glyphicon-remove').hide();
							$(".box .text-danger").html('');
							
							//INSPECCION ELEGIDA
							var in_id = $(this).parent().parent().parent().parent().parent().attr('id');
							in_id = in_id.substr(2,in_id.length);
							console.log(in_id);
							//nombre
							var in_nombre = $(this).parent().parent().parent().parent().find('h3').html();
							
							//VARIABLES
							var v_in_titulo = '';
							var v_in_descripcion = '';
							var v_in_ancho = '';
							var v_in_alto = '';
							var v_in_cantidad = '';
							var v_in_monto = '';
							var v_in_preguntas = '';
							var v_in_respuestas = '';
							var final_resp_marcas = '';
							var cont_preg = 0;
							var cont_resp_marca = 0;
							var enviar = true;
							
							//MARCAS DE LA INSPECCION
							$(this).parent().parent().parent().find('.box-body').each(function(){
								//GUARDAMOS LOS TITULOS
								v_in_titulo = v_in_titulo + $(this).find('input.tipo').val() + ';';
								v_in_descripcion = v_in_descripcion + $(this).find('input.descripcion').val() + ';';
								v_in_ancho = v_in_ancho + $(this).find('input.ancho').val() + ';';
								v_in_alto = v_in_alto + $(this).find('input.alto').val() + ';';
								v_in_cantidad = v_in_cantidad + $(this).find('input.cantidad').val() + ';';
								v_in_monto = v_in_monto + $(this).find('input.monto').val() + ';';
								
								//GUARDAMOS LAS PREGUNTAS
								$(this).find('.pregunt').each(function(){
									cont_preg++;
									var pregunt = $(this).find('label').html();
									//pregunt = pregunt.substring(0, pregunt.length); //?
									//pregunt = pregunt.substring(1, pregunt.length);//? 
									v_in_preguntas = v_in_preguntas + pregunt + ',';
									
									//GUARDAMOS LAS RESPUESTAS
									$(this).find('input.respuesta').each(function(){
										v_in_respuestas = v_in_respuestas + $(this).val() + ',';
									});
									
									v_in_respuestas = v_in_respuestas.substring(0, v_in_respuestas.length-1);
									v_in_respuestas = v_in_respuestas + ';';
								});
								
								v_in_preguntas = v_in_preguntas.substring(0, v_in_preguntas.length-1);
								v_in_preguntas = v_in_preguntas + ';';
								
								v_in_respuestas = v_in_respuestas.substring(0, v_in_respuestas.length-1);
								v_in_respuestas = v_in_respuestas + '/';
								
								//GUARDAMOS LAS RESPUESTAS MARCADAS
								$(this).find('input.respuesta').each(function(){
									final_resp_marcas = final_resp_marcas + $(this).val() + ';';
									cont_resp_marca++;
								});
								
								if (final_resp_marcas != '') {
									final_resp_marcas = final_resp_marcas.substring(0, final_resp_marcas.length-1);
									final_resp_marcas = final_resp_marcas + '/';
								}
							});
							
							//ESTADO DE LA INSPECCION
							var ti_estado = 1;
							if ($(this).hasClass('completar_inspeccion')) {
								ti_estado = 2;
								
								if (cont_resp_marca != cont_preg) {
									enviar = false;
									bootbox.alert('<?php echo lang("trabajo.inspeccion.cerrado.ko") ?>');
								}
							}
							
							if (enviar) {
								var datos = {
									'in_id'	:	in_id,
									'tr_id'	:	tr_id,
									'man_id'	:	$('#inspecc_mant option:selected').val(),
									'ti_comentario'	:	$(this).parent().parent().prev().prev().find('textarea').val(),
									'in_titulo'	:	v_in_titulo,
									'in_descripcion'	:	v_in_descripcion,
									'in_ancho'	:	v_in_ancho,
									'in_alto'	:	v_in_alto,
									'in_cantidad'	:	v_in_cantidad,
									'in_monto'	:	v_in_monto,
									'in_preguntas'	:	v_in_preguntas,
									'in_respuestas'	:	v_in_respuestas,
									'in_resp_marcas'	:	final_resp_marcas,
									'in_nombre'	:	in_nombre,
									'ti_estado'	:	ti_estado
								};
								
								$(this).parent().parent().parent().find(".fotos").ajaxForm({
									dataType: 'json',
									data: datos,
									success: function(data) {
										if (data.sql) {
											if (data.status) {
												//TODO CORRECTO
												//MENSAJE GUARDAR INSPECCION
												if (ti_estado < 2) {
													bootbox.alert('<?php echo lang("trabajo.inspeccion.ok") ?>');
												} else {
													//MENSAJE CERRAR INSPECCION
													bootbox.alert('<?php echo lang("trabajo.inspeccion.cerrado.ok") ?>');
													$('#'+in_id+' .botons, #'+in_id+' .fotos').hide();
													$('#'+in_id+' input, #'+in_id+' textarea').attr('disabled','disabled');
												}
												
												//RECARGA LAS FOTOS SI SE SUBIERON FOTOS NUEVAS
												if ($('#'+in_id).find('.fot input:eq(1)').val() != '') {
													//imagenes(in_id);
													$('#'+in_id).find('.fot input:eq(1)').val('')
												}
											} else {
												//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
												//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
												$.each(data.errors, function (ind, elem) {
													$.each(data.errors[ind], function (ind2, elem2) {
														//SI EXSITE ERROR EN EL CAMPO FOTO ENTRAMOS POR AQUI
														if (data.errors[ind][0] == 'fotos') {
															$('#'+in_id+' .fot').find('.text-danger').html(data.errors[ind][ind2]);
														} else {
															//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
															$('#'+in_id+' .textar').find('.text-danger').html(data.errors[ind][ind2]);
															$('#'+in_id+' .textar').find('.form-group').addClass("has-error has-feedback");
															$('#'+in_id+' .textar').find('.glyphicon-remove').show();
														}
													}); 
												});
											}
										} else {
											bootbox.alert('<?php echo lang("error.ajax") ?>');
										}
										l.stop();
									},
									error: function(XMLHttpRequest, textStatus, errorThrown) {
										bootbox.alert('<?php echo lang("error.ajax") ?>');
										l.stop();
									}
								}).submit();
							}
							l.stop();
						});
						
						
					},
					complete: function() {
						$('#cargar_inspeccion').hide();
					}
				});
			}
			
			//INSPECCIONES AVANZADAS
			if (inv_id != '') {
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "<?php echo base_url().$this->lang->lang() ?>/inspecciones/inspecciones_avanzadas_trabajo",
					data: 'tr_id=<?php echo $tr_id ?>&inv_id='+inv_id+'&man_id='+$('#inspecc_mant option:selected').val(),
					success: function(data) {
						var insp = '';
						
						$.each(data,function(indice) {
							//PREGUNTAMOS SI ESTA INSPECCION YA FUE CERRADA
							var read = '';
							if (data[indice]['tia_estado'] == 2) {
								read = 'disabled="disabled"';
							}
							
							insp = insp + '<div class="col-lg-12 inspecciones_avanzadas" id="'+data[indice]['inv_id']+'"><div class="box box-solid box-primary"><div class="box-header"><h3 class="box-title">' + data[indice]['inv_nombre'] + '</h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm encoger_inspeccion"><i class="fa fa-minus"></i></button></div></div>';
							
							//EDIFICIOS			
							var inv_edificio = data[indice]['inv_edificio'];
							inv_edificio = inv_edificio.split(';');
							inv_edificio = inv_edificio.filter(Boolean);
							
							//PLANTAS
							var inv_planta = data[indice]['inv_planta'];;
							inv_planta = inv_planta.split('|');
							inv_planta = inv_planta.filter(Boolean);
							
							//PRODUCTOS
							var inv_producto = data[indice]['inv_producto'];;
							inv_producto = inv_producto.split('^');
							inv_producto = inv_producto.filter(Boolean);
							
							//RESPUESTAS
							var inv_respuesta = data[indice]['inv_respuesta'];;
							inv_respuesta = inv_respuesta.split('^');
							inv_respuesta = inv_respuesta.filter(Boolean);
							
							//RESPUESTAS MARCADAS
							var inv_resp_marcas = data[indice]['inv_resp_marca'];
							if (inv_resp_marcas != null) {
								inv_resp_marcas = inv_resp_marcas.split('^');
								inv_resp_marcas = inv_resp_marcas.filter(Boolean);
							}
									
							insp = insp + '<div class="row">';
							for (var e=0; e<inv_edificio.length; e++) {
								//ABRIR EDIFICIO
								insp = insp + '<div class="col-lg-12 box box-solid box-primary edificios" id="'+e+'"><div class="box-header"><h3 class="box-title" style="width: 50%;">'+inv_edificio[e]+'</h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm encoger_edificio"><i class="fa fa-minus"></i></button></div></div><div class="box-body"><div class="body_edificio">';
								
								var aux_planta = inv_planta[e].split(';');
								aux_planta = aux_planta.filter(Boolean);
								
								var aux_producto = inv_producto[e].split('|');
								aux_producto = aux_producto.filter(Boolean);
								
								var aux_respuesta = inv_respuesta[e].split('|');
								aux_respuesta = aux_respuesta.filter(Boolean);
								
								var aux_resp_marcas;
								if (inv_resp_marcas != null) {
								aux_resp_marcas = inv_resp_marcas[e].split('|');
								aux_resp_marcas = aux_resp_marcas.filter(Boolean);	
								}
								
								for (var p=0; p<aux_planta.length; p++) {
									//ABRIMOS LAS PLANTAS
									insp = insp + '<div class="box box-solid box-info plantas plant'+p+'"><div class="box-header"><h3 class="box-title">'+aux_planta[p]+'</h3><div class="box-tools pull-right"><button class="btn btn-primary btn-sm encoger_planta"><i class="fa fa-minus"></i></button></div></div><div class="box-body body_planta">';
									
									var aux2_producto = aux_producto[p].split(';');
									aux2_producto = aux2_producto.filter(Boolean);
									
									var aux2_respuesta = aux_respuesta[p].split('}');
									aux2_respuesta = aux2_respuesta.filter(Boolean);
									
									var aux2_resp_marcas;
									if (inv_resp_marcas != null) {
										aux2_resp_marcas = aux_resp_marcas[p].split('}');
										aux2_resp_marcas = aux2_resp_marcas.filter(Boolean);
									}
									
									for (var a2p=0; a2p<aux2_producto.length; a2p++) {
										//ABRIMOS EL PRODUCTO
										insp = insp + '<div class="col-lg-4"><div class="box productos product'+a2p+'"><div class="box-header"><h3 class="box-title">'+aux2_producto[a2p]+'</h3><div class="box-tools pull-right"><button class="btn btn-default btn-sm encoger_producto"><i class="fa fa-minus"></i></button></div></div><div class="box-body body_producto">';
										
										var aux3_respuesta = aux2_respuesta[a2p].split(';');
										aux3_respuesta = aux3_respuesta.filter(Boolean);
										
										var aux3_resp_marcas;
										if (inv_resp_marcas != null) {
											aux3_resp_marcas = aux2_resp_marcas[a2p].split(';');
											aux3_resp_marcas = aux3_resp_marcas.filter(Boolean);
										}
										
										for (var r=0; r<aux3_respuesta.length; r++) {
											//MOSTRAMOS LAS RESPUESTAS
											if (inv_resp_marcas != null) {
												if (aux3_respuesta[r] == aux3_resp_marcas[r]) {
													insp = insp + '<div class="form-group item"><input type="checkbox" checked="true" '+read+' /><span>'+aux3_respuesta[r]+'</span></div>';
												} else {
													insp = insp + '<div class="form-group item"><input type="checkbox" /><span>'+aux3_respuesta[r]+'</span></div>';
											}
											} else {
												insp = insp + '<div class="form-group item"><input type="checkbox" /><span>'+aux3_respuesta[r]+'</span></div>';
											}
										}
										
										//CERAMOS EL PRODUCTO
										insp = insp + '</div></div></div>';
									}
									
									//CERRAMOS LA PLANTA
									insp = insp + '<div class="row"></div></div></div>';
								}
								
								//CERRAR EDIFICIO
								insp = insp + '</div></div></div>'
							}
										
							//insp = insp + '</div>'; /* FIN ROW */
							insp = insp + '<div class="row">';
									
							insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12 textar" style="padding: 0 30px 0 30px">';
							if (data[indice]['tia_comentario'] != null) {
								insp = insp + '<div class="form-group"><h4><strong><?php echo lang("comentario") ?></strong></h4><textarea '+read+'>'+ data[indice]['tia_comentario'] +'</textarea><span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger"><?php echo form_error("ti_comentario") ?></div></div>';
							} else {
								insp = insp + '<div class="form-group"><h4><strong><?php echo lang("comentario") ?></strong></h4><textarea '+read+'></textarea><span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger"><?php echo form_error("ti_comentario") ?></div></div>';
							}
							insp = insp + '</div>';
							insp = insp + '</div>'; /* FIN ROW */
							insp = insp + '<div class="row">';
										
										
										
							insp = insp + '<div class="col-lg-12 col-md-12 col-sm-12 fot"><h4><strong><?php echo lang("fotos") ?></strong></h4><div><form class="fotos" action="<?php echo base_url().$this->lang->lang() ?>/trabajos/upload" method="post" enctype="multipart/form-data"><div class="input-group"><span class="input-group-btn"><span class="btn btn-primary btn-file"><i class="fa fa-camera"></i><input type="file" name="archivos[]" multiple="multiple" '+read+'></span></span><input type="text" class="form-control" readonly></div></form></div><div class="text-danger"><?php echo form_error("fotos") ?></div><div>'+data[indice]["imagenes"]+'</div></div>';
										
							insp = insp + '</div>'; /* FIN ROW */
										
							//BOTONES GUARDAR Y CERRAR
							var botones = true;
							var dep = '<?php echo $this->session->userdata("de_id") ?>';
							if (data[indice]['tia_estado'] != null ) {
								if (data[indice]['tia_estado'] == 2) {
									botones = false;
								} else {
									if ((dep == '3') || (dep == '4') || (dep == '5')) { //Si pertenece a contabilidad, cotizaciones o operaciones no puede crear
										 botones = false;
									}
								}
							}
										
							if (botones) {
								insp = insp + '<div class="row botons"><div class="col-lg-12 col-md-12 col-sm-12 text-right"><button id="close'+data[indice]['inv_id']+'" type="button" class="btn btn-warning ladda-button completar_inspeccion_avanzada" data-style="zoom-out" id="close'+data[indice]['inv_id']+'"><i class="fa fa-eye"></i> <?php echo lang("completar.inspeccion") ?></button> <button class="btn btn-success ladda-button guardar_inspeccion_avanzada" data-style="zoom-out" id="save'+data[indice]['inv_id']+'"><i class="fa fa-save"></i> <?php echo lang("guardar") ?></button></div></div>';
							}
										
							insp = insp + '</div></div>';
							insp = insp + '</div>'; /* FIN ROW */
						});
						
						$('#ver_trabajo #inspec').append(insp);
						
						//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
						$(".box .form-group").removeClass("has-error has-feedback");
						$('.box .glyphicon-remove').hide();
						$(".box .text-danger").html('');
						
						//IMAGENES
						$(".fancybox").fancybox({
							helpers : {
					    		title : false
					    	},
					    	padding: 0,
							openEffect : 'elastic',
							openSpeed  : 150,
							closeEffect : 'elastic',
							closeSpeed  : 150,
							closeClick : false
						});
						
						//ESTILO Y BOTON FOTOS
						$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
							var input = $(this).parents('.input-group').find(':text'),
								log = numFiles > 1 ? numFiles + ' files selected' : label;
								if( input.length ) {
									input.val(log);
								} else {
									if( log ) alert(log);
								}	
						});
						
						//GUARDAR INSPECCION
						$('.guardar_inspeccion_avanzada,.completar_inspeccion_avanzada').on('click', function(){
							//LOADING BOTON
							var btn_id = $(this).attr('id');
							var l = Ladda.create( document.querySelector( '#'+btn_id ) );
						 	l.start();
							
							//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
							$(".box .form-group").removeClass("has-error has-feedback");
							$('.box .glyphicon-remove').hide();
							$(".box .text-danger").html('');
							
							var obj = $(this).parent().parent().parent().parent().parent();
							
							//INSPECCION ELEGIDA
							var inv_id = obj.attr('id');
							//NOMBRE
							var inv_nombre = obj.find('h3').html();
							
							//VARIABLES
							var inv_edificio = '';
							var inv_planta = '';
							var inv_producto = '';
							var inv_respuesta = '';
							var inv_resp_marca = '';
							var cont_product = 0;
							var cont_resp_marca = 0;
							var enviar = true;
							
							//RECOPILAMOS LOS DATOS
							obj.find('.edificios').each(function(indice){
						    	var edificio = $(this).find('.box-header h3').html();
						    	
								inv_edificio = inv_edificio + edificio + ';';
								
						    	
								$(this).find('.box-body .plantas').each(function(indice2){
						    		var planta = $(this).find('.box-header h3').html();
						    		
									inv_planta = inv_planta + planta + ';';
									
									$(this).find('.box-body .productos').each(function(indice3){
										var producto = $(this).find('.box-header h3').html();
										inv_producto = inv_producto + producto + ';';
										
										$(this).find('.body_producto .item').each(function(indice4){
											cont_product++;
											var respuesta = $(this).find('span').html();
											inv_respuesta = inv_respuesta + respuesta + ';';
											var resp_marca = $(this).find('input').is(':checked');
											
											if (resp_marca) {
												inv_resp_marca = inv_resp_marca + respuesta + ';';
												cont_resp_marca++;
											} else {
												inv_resp_marca = inv_resp_marca + 'null' + ';';
											}
										});
										
										inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
						    			inv_respuesta = inv_respuesta + '}';
						    			
						    			inv_resp_marca = inv_resp_marca.substring(0, inv_resp_marca.length-1);
						    			inv_resp_marca = inv_resp_marca + '}';
								});
							    	
									inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
						    		inv_respuesta = inv_respuesta + '|';
						    		
						    		inv_resp_marca = inv_resp_marca.substring(0, inv_resp_marca.length-1);
						    		inv_resp_marca = inv_resp_marca + '|';
						    		
						    		inv_producto = inv_producto.substring(0, inv_producto.length-1);
						    		inv_producto = inv_producto + '|';
									
						    	});
						    	
								inv_planta = inv_planta.substring(0, inv_planta.length-1);
							    inv_planta = inv_planta + '|';
							    
							    inv_producto = inv_producto.substring(0, inv_producto.length-1);
							    inv_producto = inv_producto + '^';
							    
							    inv_respuesta = inv_respuesta.substring(0, inv_respuesta.length-1);
							    inv_respuesta = inv_respuesta + '^';
							    
							    inv_resp_marca = inv_resp_marca.substring(0, inv_resp_marca.length-1);
							    inv_resp_marca = inv_resp_marca + '^';
								
						    });
							
							//ESTADO DE LA INSPECCION
							var tia_estado = 1;
							if ($(this).hasClass('completar_inspeccion_avanzada')) {
								tia_estado = 2;
								
								if (cont_resp_marca != cont_product) {
									enviar = false;
									bootbox.alert('<?php echo lang("trabajo.inspeccion.avanzada.cerrado.ko") ?>');
								}
							}
							
							if (enviar) {
								var datos = {
									'inv_id'	:	inv_id,
									'tr_id'	:	tr_id,
									'man_id'	:	$('#inspecc_mant option:selected').val(),
									'tia_comentario'	:	$(this).parent().parent().prev().prev().find('textarea').val(),
									'inv_edificio'	:	inv_edificio,
									'inv_planta'	:	inv_planta,
									'inv_producto'	:	inv_producto,
									'inv_respuesta'	:	inv_respuesta,
									'inv_resp_marca'	:	inv_resp_marca,
									'inv_nombre'	:	inv_nombre,
									'tia_estado'	:	tia_estado
								};
								
								$(this).parent().parent().parent().find(".fotos").ajaxForm({
									dataType: 'json',
									data: datos,
									success: function(data) {
										if (data.sql) {
											if (data.status) {
												//TODO CORRECTO
												//MENSAJE GUARDAR INSPECCION
												if (tia_estado < 2) {
													bootbox.alert('<?php echo lang("trabajo.inspeccion.ok") ?>');
												} else {
													//MENSAJE CERRAR INSPECCION
													bootbox.alert('<?php echo lang("trabajo.inspeccion.avanzada.cerrado.ok") ?>');
													$('#'+inv_id+' .botons, #'+inv_id+' .fotos').hide();
													$('#'+inv_id+' input, #'+inv_id+' textarea').attr('disabled','disabled');
												}
												
												//RECARGA LAS FOTOS SI SE SUBIERON FOTOS NUEVAS
												if ($('#'+inv_id).find('.fot input:eq(1)').val() != '') {
													//imagenes_avanzadas(inv_id);
													$('#'+inv_id).find('.fot input:eq(1)').val('')
												}
											} else {
												//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
												//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
												$.each(data.errors, function (ind, elem) {
													$.each(data.errors[ind], function (ind2, elem2) {
														//SI EXSITE ERROR EN EL CAMPO FOTO ENTRAMOS POR AQUI
														if (data.errors[ind][0] == 'fotos') {
															$('#'+inv_id+' .fot').find('.text-danger').html(data.errors[ind][ind2]);
														} else {
															//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
															$('#'+inv_id+' .textar').find('.text-danger').html(data.errors[ind][ind2]);
															$('#'+inv_id+' .textar').find('.form-group').addClass("has-error has-feedback");
															$('#'+inv_id+' .textar').find('.glyphicon-remove').show();
														}
													}); 
												});
											}
										} else {
											bootbox.alert('<?php echo lang("error.ajax") ?>');
										}
										l.stop();
									},
									error: function(XMLHttpRequest, textStatus, errorThrown) {
										bootbox.alert('<?php echo lang("error.ajax") ?>');
										l.stop();
									}
								}).submit();
							}
							l.stop();
						});
						
						
					},
					complete: function() {
						$('#inspec #cargar_inspeccion').hide();
					}
				});
			}
			
		}
		
		//RELLENAMOS EL SELECT DE LOS MANTENIMIENTOS DE ESTE TRABAJO
		for (var i=0; i<locations.length; i++) {
			$('#inspecc_mant').append('<option value="'+locations[i][0]+'">'+locations[i][3]+'</option>');
		}
		
		//LLAMAMOS A LA FUNCION INSPECCIONES_TRABAJO CADA VEZ QUE CAMBIE EL SELECT
		$("#inspecc_mant").change(function () {
			$('#tab_2 #inspec').hide().html('<center><img src="<?php echo base_url() ?>img/load.gif" id="cargar_inspeccion" /></center>').fadeIn(800);
			inspecciones_trabajo();			
		}).trigger('change');
		
		//COLAPSAR Y EXPANDIR
		$("#ver_trabajo #inspec").on('click', '.encoger_inspeccion,.encoger_edificio,.encoger_planta,.encoger_producto', function () {
			//BOTON EXPANDIR Y COLAPSAR
			$header = $(this);
			//CONTENIDO
			$content = $header.parent().parent().next();
			//COLAPSAR O EXPANDIR
			$content.slideToggle(500, function () {
			//CAMBIAR ICONO
			$header.html(function () {
				return $content.is(":visible") ? '<i class="fa fa-minus"></i>' : '<i class="fa fa-plus"></i>';
			    });
			});
		});
	}

	//LLENAMOS BITACORA
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/bitacora/buscar_todo",
	{'bi_idasociado':'<?php echo isset($tr_id)?$tr_id:"0" ?>','bi_tipo':'TR'},
	function(data){
		
		var table = '';
		$.each(data,function(indice) {
			table = table + '<tr id="<?php echo isset($tr_id)?$tr_id:'' ?>">';
			table = table + '<td>' + data[indice]['bi_fecha'] + '</td>';	
			var empleado = data[indice]['emp_nombre'] + ' ' + data[indice]['emp_apellido1']+ ' ' + data[indice]['emp_apellido2'];	
			table = table + '<td>' + empleado + '</td>';			
			table = table + '</tr>';
		});

		$('#bitacora tbody').html(table);
                
	}, "json");
});

/* ESTILO Y BOTON FOTOS */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});
</script>