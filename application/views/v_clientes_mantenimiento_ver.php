<div id="cliente_mant">
	<div class="row">
		<div class="col-lg-6 col-md-6">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h4><?php echo lang('informacion.contacto') ?></h4>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6">
					<label><?php echo lang('nombre') ?></label>
					<p><?php echo $mantenimiento->man_nombre ?></p>
					
					<label><?php echo lang('contacto') ?></label>
					<p><?php echo $mantenimiento->man_contacto ?></p>
					<?php
					if ($mantenimiento->man_barriada != '') {
						$direcc = $mantenimiento->man_direccion.' ('.$mantenimiento->man_barriada.')';
					} else {
						$direcc = $mantenimiento->man_direccion;
					}
					?>
					<label><?php echo lang('direccion') ?></label>
					<p><?php echo $direcc ?></p>
					
					<label><?php echo lang('email') ?></label>
					<p><?php echo $mantenimiento->man_email ?></p>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6">
					<?php
					if ($mantenimiento->man_postal != '') {
						$postal = $mantenimiento->man_postal.', '.$mantenimiento->man_localidad;
					} else {
						$postal = $mantenimiento->man_localidad;
					}
					?>
					<label><?php echo lang('localidad') ?></label>
					<p><?php echo $postal ?></p>
					
					<label><?php echo lang('provincia').', '.lang('pais') ?></label>
					<p><?php echo $mantenimiento->man_provincia.', '.$mantenimiento->name ?></p>
					
					<label><?php echo lang('telefono') ?></label>
					<p><?php echo $mantenimiento->man_telefono ?></p>
				</div>
			</div>
			
			<div class="row" id="contadores">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="completados"></div>
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="procesando"></div>
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="pendientes"></div>
			</div>
		</div>
		
		<div class="col-lg-6 col-md-6">
			<div id="mapa_mantenimientos_ver"></div>
			
			<div class="text-right">
				<h6><?php echo lang('latitud').': '.$mantenimiento->man_latitud.' '.lang('longitud').': '.$mantenimiento->man_longitud ?></h6>
			</div>
		</div>
	</div>

	<br>

	<div class="row">
		<div class="col-lg-12" id="historico_mant">
			<!--<center>
				<img src="<?php echo base_url() ?>img/load.gif">
			</center>-->
			
			<ul class="timeline">
				<?php
					$result = '';
					
					foreach ($trabajos as $trab) {
						//AÑADIMOS LOS EMPLEADOS
						if (isset($trab->empleados)) {
							//TRABAJO PENDIENTE O PROCESANDO
							$empleados = '';
							
							foreach ($trab->empleados as $emp) {
								$empleados = $empleados.'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fot_emple" alt="'.$emp[0]->emp_id.'"><div><img src="'.base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/emp/'.$emp[0]->emp_dni.'/perfil.jpg" /></div><span><strong>'.lang("nombre").': </strong>'.$emp[0]->emp_nombre.' '.$emp[0]->emp_apellido1.' '.$emp[0]->emp_apellido2.'</span><br><span><strong>'.lang("dni").': </strong>'.$emp[0]->emp_dni.'</span>';
								//BOTONES DE ASISTENCIA PARA EMPLEADOS
								if ($emp[1]) {
									$empleados = $empleados.'<div class="row asistencias">
										<div class="col-lg-4"><a href="#" class="btn btn-success ladda-button" data-style="zoom-out" alt="0">'.lang("asistio").'</a></div>
										<div class="col-lg-4"><a href="#" class="btn btn-warning ladda-button" data-style="zoom-out" alt="1">'.lang("retraso").'</a></div>
										<div class="col-lg-4"><a href="#" class="btn btn-danger ladda-button" data-style="zoom-out" alt="2">'.lang("ausente").'</a></div>
									</div>';
								}
								
								$empleados = $empleados.'</div>';
							}
						}
						
						//AÑADIR LA INSPECCION
						if ($trab->tr_estado >= 1) {
							$color = "green";
							if ($trab->tr_estado == 1) {
								$color = "yellow";
							}
							//TRABAJO COMPLETADO O PROCESANDO
							$result = $result.'<li class="time-label"><span class="bg-'.$color.'">'.$trab->tr_fecha_inicio.'</span></li>';
							
							$insp = $trab->inspecciones;
							for ($i=0; $i<count($insp); $i++) {
								for ($x=0;$x<count($insp[$i]); $x++) {
									$cont_preg = 0;
									$cont_resp_marcas = 0;
									
									//TITULOS DE LAS PREGUNTAS
									$titulos = explode(';',$insp[$i][$x]->in_titulo);
									$titulos = array_filter($titulos);
									
									//PREGUNTAS
									$preguntas = explode(';',$insp[$i][$x]->in_preguntas);
									$preguntas = array_filter($preguntas);
									
									//RESPUESTAS
									$respuestas = explode('/',$insp[$i][$x]->in_respuestas);
									$respuestas = array_filter($respuestas);
									
									//RESPUESTAS MARCADAS
									$resp_marcas = explode('/',$insp[$i][$x]->in_resp_marcas);
									$resp_marcas = array_filter($resp_marcas);
									
									$result = $result.'<li><i class="fa fa-eye bg-aqua"></i><div class="timeline-item"><div class="time"></div><h3 class="timeline-header">'.$insp[$i][$x]->in_nombre.'</h3><div class="timeline-body">';
									
									for ($t=0; $t<count($titulos); $t++) {
										$result = $result.'<div class="titulo">'.$titulos[$t].'</div>';
										
										//PREGUNTAS SEPARADAS POR TITULOS
										$preg_final = explode(',',$preguntas[$t]);
										$preg_final = array_filter($preg_final);
										
										
										//RESPUESTAS SEPARADAS POR TITULOS
										$resp_final = explode(';',$respuestas[$t]);
										$resp_final = array_filter($resp_final);
										
										//RESPUESTAS MARCAS SEPARADAS POR TITULO
										if (isset($resp_marcas[$t])) {
											$resp_marcas_final = explode(';',$resp_marcas[$t]);
											$resp_marcas_final = array_filter($resp_marcas_final);
										}
										
										for ($pf=0; $pf<count($preg_final); $pf++) {
											$cont_preg++;
											
											$result = $result.'<div>&#191;'.$preg_final[$pf].'&#63;</div>';
											
											//RESPUESTAS SEPARADAS POR PREGUNTAS
											$resp = explode(',',$resp_final[$pf]);
											$resp = array_filter($resp);
											
											for ($r=0; $r<count($resp); $r++) {
												$active = '';
												if (isset($resp_marcas_final[$pf])) {
													if ($resp[$r] == $resp_marcas_final[$pf]) {
														$cont_resp_marcas++;
														$active = 'active';
													}	
												}
												
												$result = $result.'<div class="resp_left '.$active.'">'.$resp[$r].'</div>';
											}
											$result = $result.'<div style="clear:both;"></div>';
										}
										
									}
									
									//CERRAMOS TIMELINE BODY
									$result = $result.'</div>';
									
									//AÑADIMOS COMENTARIO SI EXISTE
									if ($insp[$i][$x]->ti_comentario != '') {
										$result = $result.'<h3 class="timeline-header">'.lang('comentario').'</h3><div class="timeline-body">'.$insp[$i][$x]->ti_comentario.'</div>';
									}
									
									//IMAGENES DE LA INSPECCION
									$ruta = base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/in/'.$trab->tr_id.'_'.$insp[$i][$x]->in_id.'_'.$mantenimiento->man_id;
									//PREGUNTA SI EXISTE EL DIRECTORIO
									if (file_exists($ruta)) {
										$directorio = opendir($ruta);
										$fotos = '';
										while ($archivo = readdir($directorio))  {
											//verificamos si es o no un directorio
								    		if (!is_dir($archivo)) {
								    			if ($archivo != 'index.html') {
								    				$fotos = $fotos.'<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12"><a class="fancybox-effects-d" href="'.base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/in/'.$trab->tr_id.'_'.$insp[$i][$x]->in_id.'_'.$mantenimiento->man_id.'/'.$archivo.'" data-fancybox-group="'.$trab->tr_id.'_'.$insp[$i][$x]->in_id.'_'.$mantenimiento->man_id.'"><img src="'.base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/in/'.$trab->tr_id.'_'.$insp[$i][$x]->in_id.'_'.$mantenimiento->man_id.'/'.$archivo.'" alt="'.$archivo.'" title="'.$archivo.'"></a></div>';
								    			}
								    		}
										}
										
										$result = $result.'<h3 class="timeline-header">'.lang('fotos').'</h3><div class="timeline-body row fot_incid">'.$fotos.'</div>';
									}
									
									//BARRA PROGRESO
									if ($trab->tr_estado == 1) {
										$progreso = ($cont_resp_marcas*100)/$cont_preg;
										$result = $result.'<div class="timeline-body"><div class="progress progress-striped active"><div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="'.$progreso.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$progreso.'%"><span>'.round($progreso).'% '.lang("completado").'</span></div></div></div>';
									}
									
									//CERRAMOS TIMELINE ITEM
									$result = $result.'</div></li>';
								}
							}
							
							//MUESTRA LOS EMPLEADOS
							if ($trab->tr_estado < 2 && $empleados != '') {
								$result = $result.'<li><i class="fa fa-users bg-purple"></i><div class="timeline-item"><h3 class="timeline-header">'.lang('empleados').'</h3><div class="timeline-body row">'.$empleados.'</div></div></li>';
							}
						} else {
							//TRABAJO PENDIENTE
							$color = 'red';
							if ($trab->tr_estado == 1) {
								$color = 'yellow';
							}
								
							$result = $result.'<li class="time-label" alt="'.$trab->tr_id.'"><span class="bg-'.$color.'">'.$trab->tr_fecha_inicio.'</span></li><li><i class="fa fa-users bg-purple"></i><div class="timeline-item"><h3 class="timeline-header">'.lang("cliente.trabajo.empleados").'</h3><div class="timeline-body"><div class="row empleados">'.$empleados.'</div></div></div></li>';
						}
					}
					
					echo $result;
				?>
			</ul>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		//VARIABLES GLOBALES
		var man_id = '<?php echo $mantenimiento->man_id ?>';
		var completados = $('.timeline span.bg-green').size();
		var procesando = $('.timeline span.bg-yellow').size();
		var pendientes = $('.timeline span.bg-red').size();
		var insp = '<ul class="timeline">'
		var cont = 0;
		
		$('#contadores #completados').html('<h1>'+completados+'</h1><p><?php echo lang("trabajos.completados") ?></p>');
		
		$('#contadores #procesando').html('<h1>'+procesando+'</h1><p><?php echo lang("trabajos.procesando") ?></p>');
		
		$('#contadores #pendientes').html('<h1>'+pendientes+'</h1><p><?php echo lang("trabajos.pendientes") ?></p>');
		
		//FANCYBOX
		$(".fancybox-effects-d").fancybox({
			helpers : {
				title : false
			},
			padding: 0,
			openEffect : 'elastic',
			openSpeed  : 150,
			closeEffect : 'elastic',
			closeSpeed  : 150,
			closeClick : false
		});
		
		//MAPA
		var centro = new google.maps.LatLng('<?php echo $mantenimiento->man_latitud ?>','<?php echo $mantenimiento->man_longitud ?>');
		
		var mapOptions = {
		    zoom: 15,
		    center: centro
		};
		  
		var map = new google.maps.Map(
			document.getElementById('mapa_mantenimientos_ver'),
			mapOptions
		);
		  
		var marcador = new google.maps.Marker({
			position: centro,
			map: map,
			icon: "<?php echo base_url() ?>img/pointer-view.png"
		});
		
		//CAPTURAR ASISTENCIA EMPLEADO
		$('.asistencias a').on('click',function(e){
			e.preventDefault();
			var l = Ladda.create( this );
	 		l.start();
	 		
	 		var obj = $(this).parent().parent();
	 		
	 		$.ajax({
				type: "POST",
				dataType: "json",
			  	url: "<?php echo base_url().$this->lang->lang() ?>/empleados/trabajo_asistencia",
			  	data: {'ea_estado':$(this).attr('alt'),'emp_id':$(this).parent().parent().parent().attr('alt'),'tr_id':$(this).parent().parent().parent().parent().parent().parent().parent().prev().attr('alt'),'man_id':'<?php echo $mantenimiento->man_id ?>'},
			  	success: function(data) {
			  		if (data.sql) {
						if (data.status) {
							obj.hide();
						}
			  		} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					l.stop();
			  	},
			  	error: function(XMLHttpRequest, textStatus, errorThrown) {
			   		bootbox.alert('<?php echo lang("error.ajax") ?>');
			   		l.stop();
			  	}
			});
		});
	});
</script>