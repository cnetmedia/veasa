<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("preguntas") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("preguntas") ?></th>
                		<th><?php echo lang("sucursal") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//DATOS DE LA SESSION
	var su_id = '<?php echo $this->session->userdata("su_id") ?>';
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	
	//MOSTRAR EMPLEADOS
	function mostrar_inspecciones() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/inspecciones/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						//MOSTRAMOS TODOS LOS DATOS SI ES SUPERUSUARIO O SOLO AL QUE PERTENECE SI NO LO ES
						/*if (tipo == 0) {
							mostrar = true;
						} else if (su_id == data[indice]['su_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}*/
						
							var preguntas = data[indice]['in_preguntas'].split(';');
							
							table = table + '<tr id="'+data[indice]['in_id']+'">';
	  						table = table + '<td>'+data[indice]['in_nombre']+'</td>';
	  						table = table + '<td>'+(preguntas.length-1)+'</td>';
	  						table = table + '<td>'+data[indice]['su_nombre']+'</td>';
	  						table = table + '</tr>';
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 2, "asc"],[ 0, "asc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-eye'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					if (tipo > 0) {
						$('#listar table thead tr th:last').hide();
						$('#listar table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#listar table tfoot tr th:last').hide();
					}
			        
			        //PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
					var dep = '<?php echo $this->session->userdata("de_id") ?>';
			        if (crear == '0') {
                        $('#listar #btn_nuevo').hide();
			        } else {
						if ((dep == '3') || (dep == '4') || (dep == '5')) { //Si pertenece a cotizaciones, contabilidad o produccion no puede crear
							 $('#listar #btn_nuevo').hide();
						}
					}
					
					//NUEVA INSPECCION
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/inspecciones/nueva_inspeccion');
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
			        
			        //EDITAR O VER SUCURSAL
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/inspecciones/editar_inspeccion/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_inspecciones();
});
</script>