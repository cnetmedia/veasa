	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
    
    <h4><?php echo lang('info.cliente') ?></h4>
	
	<div class="row info_cliente">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="form-group">
				<label><?php echo lang('sucursal') ?></label>
				<select id="sucursales" name="su_id" class="form-control"></select>
			</div>
		</div>
		
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="form-group">
				<label><?php echo lang('cliente') ?></label>
				<select id="clientes" name="cl_id" class="selectpicker form-control" data-live-search="true"></select>
			</div>
		</div>
            
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="form-group">
				<label><?php echo lang('mantenimiento') ?></label>
				<select id="mantenimientos" name="man_id" class="selectpicker form-control" data-live-search="true"></select>
			</div>
		</div>
	</div>
	
	<div class="row cliente">
		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12 nombre"></div>
		
		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12 dni"></div>
	</div>

	<div class="row cliente">
		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12 telefono"></div>
		
		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12 email"></div>
	</div>

	<div class="row cliente">
		<div class="col-lg-12 col-xs-12 direccion"></div>
	</div>

	<div class="row cliente">
		<div class="col-lg-12 col-xs-6 postal"></div>
	</div>

	<div class="row cliente">
		<div class="col-lg-12 col-xs-6 provincia"></div>
	</div>
        
        <?php if ($pr_id != '') { ?>
        <h4><?php echo lang('mantenimiento') ?></h4>
        
        <div class="row cliente">
            <div class="col-lg-12 col-xs-6 mantenimiento">
                <?php echo $man_nombre ?>
            </div>
	</div>
        <?php } ?>
	
	<br/>
	
	<h4>
	<?php echo lang('detalles.presupuesto') ?>
	<?php echo isset($pr_numero)?': <strong>'.$pr_numero.'</strong>':'' ?>
	</h4>
	
	<?php echo isset($pr_fecha)?lang('fecha.presupuesto').': '.$pr_fecha.'<br>':''; ?>
	
	<?php $nomb_empleado = isset($emp_nombre)?$emp_nombre.' '.$emp_apellido1:''; ?>
	<?php $nomb_empleado = isset($emp_apellido2)?$nomb_empleado.' '.$emp_apellido2:''; ?>
	<?php echo isset($emp_nombre)?lang('empleado').': '.$nomb_empleado:''; ?>
        <br>
        <?php
        $estado = lang('pendiente');
        /*if ($pr_estado == 1) {
            $estado = lang('cerrada');
        } else if ($pr_estado == 2) {
            $estado = lang('caliente');
        } else if ($pr_estado == 3) {
            $estado = lang('cancelada');
        } else if ($pr_estado == 4) {
            $estado = lang('aprobada');
		}*/
		if ($pr_estado == 1) {
            $estado = lang('aprobada');
        } else if ($pr_estado == 2) {
            $estado = lang('caliente');
        } else if ($pr_estado == 3) {
            $estado = lang('cancelada');
        } else if ($pr_estado == 4) {
            $estado = lang('anulada');
		}
        
        echo lang('estado').': '.$estado ?>
	
	<div class="form-group">
		<div class="radio radio_line">
			<label>
				<input type="radio" name="su_iva" value="<?php echo $su_iva ?>" <?php echo ($pr_iva==$su_iva)?'checked':'';echo ($pr_iva=='')?'checked':''; ?> /> <?php echo lang('iva.general') ?>
			</label>
			
			<label>
				<input type="radio" name="su_iva" value="<?php echo $su_iva_reducido ?>" <?php echo ($pr_iva==$su_iva_reducido)?'checked':''; ?> /> <?php echo lang('iva.reducido') ?>
			</label>
			
			<label>
				<input type="radio" name="su_iva" value="<?php echo $su_iva_superreducido ?>" <?php echo ($pr_iva==$su_iva_superreducido)?'checked':''; ?> /> <?php echo lang('iva.superreducido') ?>
			</label>
		</div>
	</div>  
    
    </div>
    
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
     <table id="costos" class="table table-bordered table-striped table-hover dataTable">
        <thead>
            <tr>
                <th width="65%"><?php echo lang("descripcion") ?></th>
                <th class="text-right"><?php echo lang("coste.unitario").' / ' ?><span class="moneda"></span></th>
            </tr>
        </thead>
        <tbody>
		<?php 
        $costos = isset($pr_costos)?$pr_costos:'1001;2001;3001;4001;5001;6001;7001;8001/0;0;0;0;0;0;0;0';
		$costos_apart = explode('/',$costos);
		
		$costos = explode(';',$costos_apart[1]);
		$costos = array_filter($costos, "strlen");
		
		$id_costos = explode(';',$costos_apart[0]);
		$id_costos = array_filter($id_costos, "strlen");
        ?>
        
        <?php if (($pr_estado != 1) && ($this->session->userdata('de_id')!='4') && ($this->session->userdata('de_id')!='5')) { 
		//if ($pr_estado != 1) { 
		
		$categorias = $this->db->from("categorias")->get();
		
		$a=0;
		
		foreach($categorias->result() as $row) {
			
			$costo = ($costos[$a])?$costos[$a]:0;
			
		?>
        <tr>
            <td class="text-right vcenter"><?php echo $row->descripcion; ?></td>
            <td><input id="<?php echo $row->cate_id; ?>" value="<?php echo $costo; ?>" type="text" class="form-control text-center" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, '');"></td>
        </tr>

        <?php $a++; } } else { 
		
		for($i=0;$i<=count($costos)-1;$i++) {
			
			$categoria = $this->db->from('categorias')
						->where('cate_id',$id_costos[$i])
						->get()->row()->descripcion;
		?>
        
        <tr>
            <td class="vcenter text-right"><?php echo $categoria; ?></td>
            <td class="text-right" id="<?php echo $id_costos[$i] ?>"><?php echo $costos[$i] ?></td>
        </tr>
        
        <?php } } ?>
    	</tbody>
        <tfoot>
            <tr>
                <td class="text-right"><strong><?php echo lang('costos') ?></strong></td>
                <td class="text-right total"></td>
            </tr>
        </tfoot>
      </table>
    </div>		
	
	<div id="listar" class="bottom30">
		<div class="box-body table-responsive">
			<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
				<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
	            	<thead>
	                	<tr>
	                		<th width="60%" colspan="2"><?php echo lang("descripcion") ?></th>
	                		<th width="10%" class="text-right"><?php echo lang("coste.unitario").' / ' ?><span class="moneda"></span></th>
	                		<th width="10%" class="text-right"><?php echo lang("cantidad") ?></th>
	                		<th width="10%" class="text-right"><?php echo lang("descuento") ?> / %</th>
	                		<th width="10%" style="display: none" class="text-right"><?php echo lang("ganancia") ?> / %</th>
	                		<th width="10%" class="text-right"><?php echo lang("precio").' / ' ?><span class="moneda"></span></th>
	                	</tr>
					</thead>
					
					<tbody aria-relevant="all" aria-live="polite" role="alert"></tbody>
					
					<tfoot>
						
						<tr class="botones_fila">
							<td colspan="8" class="text-center">
								<?php if (($this->session->userdata('de_id') != '4') && ($this->session->userdata('de_id') != '5')) { ?>
								<a id="nueva_fila" href="#" class="btn btn-primary ladda-button" data-style="zoom-out">
									<i class="fa fa-plus"></i> <?php echo lang("nueva.fila") ?>
								</a>
								
								<a id="recalcular" href="#" class="btn btn-primary ladda-button" data-style="zoom-out">
									<i class="fa fa-check-square-o"></i> <?php echo lang("recalcular") ?>
								</a>
								<?php } ?>
							</td>
						</tr>
						
						<tr>
							<td colspan="5" class="text-right"><?php echo lang('subtotal') ?></td>
							<td class="text-right"></td>
						</tr>
							
						<tr>
							<td colspan="5" class="text-right"><?php echo lang('descuento') ?></td>
							<td class="text-right"></td>
						</tr>
							
						<tr>
							<td colspan="5" class="text-right"><?php echo lang('base.imponible') ?></td>
							<td class="text-right"></td>
						</tr>
							
						<tr>
							<td colspan="5" class="text-right">(<span class="impuesto"></span> %) <?php echo lang('impuestos') ?></td>
							<td class="text-right"></td>
						</tr>
							
						<tr>
							<td colspan="5" class="text-right"><strong><?php echo lang('total') ?></strong></td>
							<td class="text-right"><strong></strong></td>
						</tr>
						<!--	
						<tr>
							<td colspan="5" class="text-right">< ?php echo lang('ganancia') ?></td>
							<td class="text-right"><strong></strong></td>
						</tr>
						-->
					</tfoot>
				</table>	
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12 col-xs-12 text-right">
			<?php if (($this->session->userdata('de_id') != '2') && ($this->session->userdata('de_id') != '4') && ($this->session->userdata('de_id') != '5')) { ?>
				<?php if ($pr_estado != 1) { ?>
				<a id="guardar_presupuesto" href="#" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-save"></i> <?php echo lang('guardar') ?></a>
				<?php } ?>
					
				<?php if ($pr_id != '' && $pr_estado != 1) { ?>
							<a id="aprobar" href="#" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-heart"></i> <?php echo lang('aprobada') ?></a>
							<?php } ?>
							
							<?php if ($pr_id != '' && $pr_estado != 1 && $pr_estado != 2) { ?>
							<a id="caliente" href="#" class="btn btn-warning ladda-button" data-style="zoom-out"><i class="fa fa-fire"></i> <?php echo lang('caliente') ?></a>
							<?php } ?>
							
							<?php if ($pr_id != '' && $pr_estado != 3 && $pr_estado != 1) { ?>
							<a id="cancelada" href="#" class="btn btn-danger ladda-button" data-style="zoom-out"><i class="fa fa-times"></i> <?php echo lang('cancelada') ?></a>
							<?php } ?>
						  
				<?php if ($pr_estado == 1) { ?>  
					<a id="ver_factura" href="#" class="btn btn-primary"><i class="fa fa-eye"></i> <?php echo lang('ver.factura') ?></a>
					<a id="anular" href="#" class="btn btn-danger ladda-button" data-style="zoom-out"><i class="fa fa-times"></i> <?php echo lang('anular.presupuesto') ?></a>
				<?php } ?>
						
				<?php if ($pr_id != '') { ?>
				<a id="generar_pdf" href="<?php echo isset($pr_id)?base_url().$this->lang->lang().'/presupuestos/generar_pdf/'.$pr_id:"" ?>" class="btn btn-primary"><i class="fa fa-file-text"></i> <?php echo lang('pdf') ?></a>
						
				<a id="mandar_email" href="#" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-envelope"></i> <?php echo lang('email') ?></a>
				<?php } ?>
			<?php } ?>
						
			<a href="<?php echo base_url().$this->lang->lang().'/presupuestos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
		</div>
	</div>
	
<script>
		$(document).ready(function(){
			//PREGUNTAMOS SI PUEDE EDITAR
			var editar = '<?php echo $this->session->userdata("emp_editar") ?>';
			if (editar == 0) {
				$('#guardar_presupuesto, #cerrar_presupuesto, #btn_deshabilitar').hide();
			}
						
			//VARIABLES GLOBALES
			var servicios = '<?php echo isset($pr_servicio)?str_replace("<br />", "\\n", preg_replace("/\\n/m", "<br />", $pr_servicio)):"" ?>';
			var costes = '<?php echo isset($pr_precio)?$pr_precio:"" ?>';
			var cantidades = '<?php echo isset($pr_cantidad)?$pr_cantidad:"" ?>';
			var descuentos = '<?php echo isset($pr_descuento)?$pr_descuento:"" ?>';
			var ganancias = '<?php echo isset($pr_ganancia)?$pr_ganancia:"" ?>';
			var enviar = false;
			var datos;
			var su_zona_horaria;
			var pr_estado = '<?php echo $pr_estado ?>';
			var cl_id = '<?php echo isset($cl_id)?$cl_id:"" ?>';
            var man_id = '<?php echo isset($man_id)?$man_id:"" ?>';
			var array_servicios = servicios.split(";");
			var array_costes = costes.split(";");
			var array_cantidades = cantidades.split(";");
			var array_descuentos = descuentos.split(";");
			var array_ganancias = ganancias.split(";");
			var fila = '';
			var coste_total = 0;
			var coste_descuento = 0;
			var coste_ganancia = 0;
			var subtotal = 0;
			var iva = '<?php echo $pr_iva ?>';
			var pr_id = '<?php echo isset($pr_id)?$pr_id:"" ?>';
			var total = 0;
			var dep = '<?php echo $this->session->userdata('de_id') ?>';
			
			
			//TBODY INPUTS PARA LA TABLA
			function anadir_fila() {
				fila = '<tr>';
							
				fila = fila + '<td width="3%" class="text-center vert-align"><i class="fa fa-times mano" title="<?php echo lang("borrar.fila") ?>" onclick="javascript: this.parentNode.parentNode.remove();"></i></td>';
								
				fila = fila + '<td><textarea name="pr_servicio" class="pr_servicio form-control" style="width:100%" maxlength="4000"></textarea><input name="pr_servicio" class="pr_servicio form-control" type="hidden" /><div class="text-danger"></div></td>';
								
				fila = fila + '<td class="text-right"><input name="pr_precio" class="pr_precio form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');"><div class="text-danger"></div></td>';
								
				fila = fila + '<td class="text-right"><input name="pr_cantidad" class="pr_cantidad form-control text-right" onkeyup="var pattern = /[^0-9]/g;this.value = this.value.replace(pattern, \'\');"><div class="text-danger"></div></td>';
								
				fila = fila + '<td class="text-right"><input name="pr_descuento" class="pr_descuento form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');"><div class="text-danger"></div></td>';
				
				fila = fila + '<td style="display: none" class="text-right"><input name="pr_ganancia" class="pr_ganancia form-control text-right" value="0" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');"><div class="text-danger"></div></td>';
								
				fila = fila + '<td class="text-right"><strong></strong></td>';
								
				fila = fila + '</tr>';
							
				$('#listar tbody').append(fila);
			}
						
			//LLAMAMOS LA FUNCION AÑADIR FILA
			$('#nueva_fila').click(function(e){
				e.preventDefault();
				var l = Ladda.create( document.querySelector( "#nueva_fila" ) );
				l.start();
				anadir_fila();
				$('textarea').textareaAutoSize();
				l.stop();
			});
			
			//RE-CALCULAR PRESUPUESTO
			function recalcular() {
				$('#listar tbody input.pr_servicio').val($('#listar tbody textarea.pr_servicio').val());
				$('textarea').textareaAutoSize();
				//PRESUPUESTO ABIERTO
				if ((pr_estado != 1) && (dep != '4') && (dep != '5')) {
				//if (pr_estado != 1) {
					$('#listar tbody td div').html('');
					var errores = 0;
					//RECORREMOS TODOS LOS INPUT
					$('#listar tbody input').each(function(indice){
						//QUE NO ESTEN VACIOS
						if ($(this).val().length != 0) {
							//NO SUPEREN LOS 4000 CARACTERES
							if ($(this).val().length < 4000) {
								//SOLO PARA LOS NUMEROS
								if (indice == 2 || indice == 3 || indice == 4) {
									//CONTAMOS EL NUMEROS DE PUNTOS
									var cont=0;
									var txt = $(this).val();
									for (var i=0; i<txt.length; i++) {
										if (txt.charAt(i) == '.') {
											cont++;
										}
									}
											
									//COMPROBAMOS LOS PUNTOS
									if (cont > 1) {
					    				$(this).next().html('<?php echo lang("decimal.punto") ?>');
					    				errores++;
					  				} else {
										var ultimo_caracter = $(this).val().substring($(this).val().length-1, $(this).val().length);
					  					if (ultimo_caracter == '.') {
											$(this).next().html('<?php echo lang("ultimo.punto") ?>');									errores++;
										}
									}
								}
							} else {
								$(this).next().html('<?php echo lang("maximo.4000") ?>');
								errores++;
							}
						} else {
							$(this).next().html('<?php echo lang("requerido") ?>');
							errores++;
						}
								
						//CONTROLAMOS QUE NO PUEDA METER UN CERO EN LA CANTIDAD
						if ($(this).attr('name') == 'pr_cantidad') {
							if ($(this).val() == '0') {
								$(this).next().html('<?php echo lang("no.cero") ?>');
								errores++;
							}
						}
					});
					
					//SI NO HAY ERRORES CALCULA LAS VARIABLES GLOBALES			
					if (errores == 0) {
						coste_total = 0;
						coste_descuento = 0;
						coste_ganancia = 0;
						subtotal = 0;
									
						$('#listar tbody tr').each(function(indice){
							var coste = $(this).find('input:eq(1)').val() * $(this).find('input:eq(2)').val();
							$(this).find('td:last').html(coste.toFixed(2));
							coste_total = coste_total + coste;
										
							var descuento = (coste*$(this).find('input:eq(3)').val())/100;
							coste_descuento = coste_descuento + descuento;
							
							var ganancia = ((coste-descuento)*$(this).find('input:eq(4)').val())/100;
							coste_ganancia = coste_ganancia + ganancia;
						});
						
						enviar = true;
					} else {
						enviar = false;
					}
				} else {
					//PRESUPUESTO CERRADO, SOLO CALCULA VARIBLES GLOBALES SIN LOS INPUTS
					$('#listar tbody tr').each(function(indice){		
						var coste = $(this).find('td:eq(1)').html() * $(this).find('td:eq(2)').html();
						$(this).find('td:last').html(coste.toFixed(2));
						coste_total = coste_total + coste;
									
						var descuento = (coste*$(this).find('td:eq(3)').html())/100;
						coste_descuento = coste_descuento + descuento;
						
						var ganancia = ((coste-descuento)*$(this).find('td:eq(4)').html())/100;
						coste_ganancia = coste_ganancia + ganancia;
					});
					
					enviar = true;
				}
				
				if (enviar) {
					calculos();
				}
			}
			
			//CALCULAMOS LOS DATOS DE LAS OPERACIONES FINALES
			function calculos() {
				var aux_costos = '';
				
				//if (pr_estado != 1) {
				if ((pr_estado != 1) && (dep != '4') && (dep != '5')) {
					aux_costos = parseFloat($('#costos tr:eq(1) input').val()) + parseFloat($('#costos tr:eq(2) input').val()) + parseFloat($('#costos tr:eq(3) input').val()) + parseFloat($('#costos tr:eq(4) input').val()) + parseFloat($('#costos tr:eq(5) input').val()) + parseFloat($('#costos tr:eq(6) input').val()) + parseFloat($('#costos tr:eq(7) input').val()) + parseFloat($('#costos tr:eq(8) input').val());
				} else {
					aux_costos = parseFloat($('#costos tr:eq(1) td:last').html()) + parseFloat($('#costos tr:eq(2) td:last').html()) + parseFloat($('#costos tr:eq(3) td:last').html()) + parseFloat($('#costos tr:eq(4) td:last').html()) + parseFloat($('#costos tr:eq(5) td:last').html()) + parseFloat($('#costos tr:eq(6) td:last').html()) + parseFloat($('#costos tr:eq(7) td:last').html()) + parseFloat($('#costos tr:eq(8) td:last').html());
				}
				
				$('#costos tfoot tr:eq(0) td:last').html(aux_costos.toFixed(2));
				$('#listar tfoot tr:eq(1) td:last').html(coste_total.toFixed(2));
				$('#listar tfoot tr:eq(2) td:last').html(coste_descuento.toFixed(2));
				var precio_final_sin_iva = coste_total - coste_descuento;
				$('#listar tfoot tr:eq(3) td:last').html(precio_final_sin_iva.toFixed(2));
				var aux_iva = (precio_final_sin_iva * parseFloat(iva))/100;
				$('#listar tfoot tr:eq(4) td:last').html(aux_iva.toFixed(2));
				total = precio_final_sin_iva + aux_iva;
				total = total.toFixed(2);
				$('#listar tfoot tr:eq(5) td:last strong').html(total);
				
				var ganancia = parseFloat(coste_ganancia) - parseFloat(aux_costos);
				/*$('#listar tfoot tr:eq(13) td:last strong').html(ganancia);
				
				if (ganancia <= 0) {
					$('#listar tfoot tr:eq(13) td:last strong').addClass('rojo');
				} else {
					$('#listar tfoot tr:eq(13) td:last strong').removeClass('rojo');
				}*/
				
				crear_datos();
			}
			
			//LLAMA LAS FUNCIONES PARA RECALCULAR TODOS LOS DATOS Y LOS ERRORES	
			$('#recalcular').click(function(e){
				e.preventDefault();
				var l = Ladda.create( document.querySelector( "#recalcular" ) );
				l.start();
				recalcular();
				//calculos();
				l.stop();
			});
			
			//GENERA LOS DATOS A ENVIAR
			function crear_datos() {
				if (enviar) {
					var cont = 0;
					servicios = '';
					costes = '';
					cantidades= '';
					descuentos = '';
					ganancias = '';

					
					$('#listar tbody textarea').each(function(indice){
						servicios = servicios + $(this).val() + ';';
					});
					
					$('#listar tbody input').each(function(indice){
						switch(cont){
							case 0:
								//servicios = servicios + $(this).val() + ';';
								break;
							case 1:
								costes = costes + $(this).val() + ';';
								break;
							case 2:
								cantidades = cantidades + $(this).val() + ';';
								break;
							case 3:
								descuentos = descuentos + $(this).val() + ';';
								break;
							case 4:
								ganancias = ganancias + $(this).val() + ';';
								break;
						}
						
						cont++;
						
						if (cont > 4) {
							cont = 0;	
						}
					});
					
					if ((pr_estado == 0) && (dep != '4') && (dep != '5')) {
					//if (pr_estado == 0) {
						
						var costos = $('#costos tr:eq(1) td:last input').attr('id') + ';' + $('#costos tr:eq(2) td:last input').attr('id') + ';' + $('#costos tr:eq(3) td:last input').attr('id') + ';' + $('#costos tr:eq(4) td:last input').attr('id') + ';' + $('#costos tr:eq(5) td:last input').attr('id') + ';' + $('#costos tr:eq(6) td:last input').attr('id') + ';' + $('#costos tr:eq(7) td:last input').attr('id') + ';' + $('#costos tr:eq(8) td:last input').attr('id') + '/';
							costos = costos + $('#costos tr:eq(1) td:last input').val() + ';' + $('#costos tr:eq(2) td:last input').val() + ';' + $('#costos tr:eq(3) td:last input').val() + ';' + $('#costos tr:eq(4) td:last input').val() + ';' + $('#costos tr:eq(5) td:last input').val() + ';' + $('#costos tr:eq(6) td:last input').val() + ';' + $('#costos tr:eq(7) td:last input').val() + ';' + $('#costos tr:eq(8) td:last input').val() + ';';
						
					} else {
						
						var costos = $('#costos tr:eq(1) td:last').attr('id') + ';' + $('#costos tr:eq(2) td:last').attr('id') + ';' + $('#costos tr:eq(3) td:last').attr('id') + ';' + $('#costos tr:eq(4) td:last').attr('id') + ';' + $('#costos tr:eq(5) td:last').attr('id') + ';' + $('#costos tr:eq(6) td:last').attr('id') + ';' + $('#costos tr:eq(7) td:last').attr('id') + ';' + $('#costos tr:eq(8) td:last').attr('id') + '/';
						    costos = costos + $('#costos tr:eq(1) td:last').html() + ';' + $('#costos tr:eq(2) td:last').html() + ';' + $('#costos tr:eq(3) td:last').html() + ';' + $('#costos tr:eq(4) td:last').html() + ';' + $('#costos tr:eq(5) td:last').html() + ';' + $('#costos tr:eq(6) td:last').html() + ';' + $('#costos tr:eq(7) td:last').html() + ';' + $('#costos tr:eq(8) td:last').html() + ';';
					}
					
					var su_id = '<?php echo isset($su_id)?$su_id:"" ?>';
					if (su_id == '') {
						su_id = $("#sucursales option:selected").val();
					}
					
					if (cl_id == '') {
						cl_id = $('#clientes option:selected').val();
					}
                                        
                                        if (man_id == '') {
						man_id = $('#mantenimientos option:selected').val();
					}
					
					datos = {
						'pr_id' : pr_id,
						'pr_servicio' : servicios,
						'pr_precio' : costes,
						'pr_cantidad' : cantidades,
						'pr_descuento' : descuentos,
						'pr_ganancia' : ganancias,
						'su_id' : su_id,
						'su_zona_horaria' : su_zona_horaria,
						'cl_id' : cl_id,
                        'man_id' : man_id,
						'pr_iva' : iva,
						'pr_total' : total,
						'pr_ganancia_total' : coste_ganancia.toFixed(2),
						'pr_costos' : costos
					};
				}
			}
						
			//APROBAR Y CERRAR PRESUPUESTO
			$('#aprobar').click(function(e){
				e.preventDefault();
				recalcular();
				
				if (enviar) {
					var l = Ladda.create( document.querySelector( "#aprobar" ) );
 					l.start();
					if ((servicios.length-1) > 0) {
						$.ajax({
							type: "POST",
							dataType: "json",
						  	url: "<?php echo base_url().$this->lang->lang() ?>/presupuestos/cerrar",
						  	data: datos,
						  	success: function(data) {
						  		if (data.sql) {
						  			//MENSAJES ALERTA PARA EDITAR O CREAR
									if (!data.editar) {
										bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										setTimeout("location.href='"+location+"'", 5000);
									}
						  			
						  			if (data.status) {
						  				bootbox.alert('<?php echo lang("presupuesto.cerrar.ok") ?>');
							    		//REFRESCAMOS LA PAGINA SIN CACHE
		    							location.reload(true);
						  			}
						  		} else {
									bootbox.alert('<?php echo lang("error.ajax") ?>');
								}
								l.stop();
						  	},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});
					} else {
						bootbox.alert('<?php echo lang("no.datos") ?>');
					}
				}
			});
            
			//GUARDAR PRESUPUESTO
			$('#guardar_presupuesto, #caliente, #cancelada').click(function(e){
				e.preventDefault();
				recalcular();
				
                var selector = '#guardar_presupuesto';
                
                if ($(this).attr('id') == 'caliente') {
                    selector = '#caliente';
                    datos['pr_estado'] = 2;
                } else if ($(this).attr('id') == 'cancelada') {
                    selector = '#cancelada';
                    datos['pr_estado'] = 3;
				}
                                
				if (enviar) {
					var l = Ladda.create( document.querySelector( selector ) );
					l.start();
					if ((servicios.length-1) > 0) {
						$.ajax({
							type: "POST",
							dataType: "json",
						  	url: "<?php echo base_url().$this->lang->lang() ?>/presupuestos/guardar_editar",
						  	data: datos,
						  	success: function(data) {
						  		if (data.sql) {
						  			//MENSAJES ALERTA PARA EDITAR O CREAR
						  			if (datos['pr_id'] != '') {
										if (!data.editar) {
											bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
											//REDIRIGIMOS A LOS 5 SEGUNDOS
											setTimeout("location.href='"+location+"'", 5000);
										}
									} else {
										if (!data.crear) {
											bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
											//REDIRIGIMOS A LOS 5 SEGUNDOS
											setTimeout("location.href='"+location+"'", 5000);
										}
									}
									
						  			//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
		  							if (data.status) {
										if (pr_id == '') {			    		
									    	//MOSTRAMOS ALERTA
											bootbox.dialog({
												message: "<?php echo lang('presupuesto.crear.ok') ?>",
												buttons: {
													success: {
														label: "<?php echo lang('nuevo') ?>",
														className: "btn-success",
														callback: function(result) {
														var url = '<?php echo base_url().$this->lang->lang() ?>/presupuestos/nuevo_presupuesto';
														$(location).attr('href',url);
													}
													},
													main: {
														label: "<?php echo lang('presupuestos') ?>",
														className: "btn-primary",
															callback: function(result) {
																var url = '<?php echo base_url().$this->lang->lang() ?>/presupuestos/';
																$(location).attr('href',url);
															}
														}
													}
												});
											} else {
												bootbox.alert('<?php echo lang("presupuesto.guardar.ok") ?>');
											}
									}
						  		} else {
									bootbox.alert('<?php echo lang("error.ajax") ?>');
								}
								l.stop();
						  	},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});
					} else {
						bootbox.alert('<?php echo lang("no.datos") ?>');
					}
				}
			});
			
			//ANULAR PRESUPUESTO
			$('#anular').click(function(e){
				e.preventDefault();
				recalcular();
				
				if (enviar) {
					var l = Ladda.create( document.querySelector( "#anular" ) );
 					l.start();
					//alert(servicios.length);
					//if ((servicios.length-1) > 0) {
						$.ajax({
							type: "POST",
							dataType: "json",
						  	url: "<?php echo base_url().$this->lang->lang() ?>/presupuestos/anular",
						  	data: datos,
						  	success: function(data) {
						  		if (data.sql) {
						  			//MENSAJES ALERTA PARA EDITAR O CREAR
									if (!data.editar) {
										bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										setTimeout("location.href='"+location+"'", 5000);
									}
						  			
						  			if (data.status) {
						  				bootbox.alert('<?php echo lang("presupuesto.anular.ok") ?>');
							    		//REFRESCAMOS LA PAGINA SIN CACHE
		    							location.reload(true);
						  			}
						  		} else {
									bootbox.alert('<?php echo lang("error.ajax") ?>');
								}
								l.stop();
						  	},
						  	error: function(XMLHttpRequest, textStatus, errorThrown) {
						   		bootbox.alert('<?php echo lang("error.ajax") ?>');
						   		l.stop();
						  	}
						});
					//} else {
					//	bootbox.alert('<?php echo lang("no.datos") ?>');
					//}
				}
			});
			
			//OCULTAMOS EL BOTON CERRAR PRESUPUESTO SI ES UN NUEVO PRESUPUESTO
			/*if (pr_id == '') {
				$('#generar_pdf, #mandar_email, #cerrar_presupuesto, #ver_factura').hide();
			}
			
			//OCULTAMOS LOS BOTONES DE GUARDAR Y CERRAR SI EL PRESUPUESTO YA ESTA CERRADO
			if (pr_estado == 1) {
				$('#cerrar_presupuesto, #cerrar_presupuesto').hide();
			} else {
				$('#ver_factura').hide();
			}*/
			
			//CAMBIAMOS EL IVA CON LOS RADIOS BUTTONS
			$('.radio_line label').click(function(){
				if (editar == 1) {
					iva = $(this).find('input').val();
					//CALCULA LAS OPERACIONES FINALES SI EL PRESUPUESTO ES NUEVO O ESTA ABIERTO
					if (pr_estado == 0) {
						recalcular();
					}
					$('.impuesto').html(iva);
				}
			});
			
			//SI NO EXISTE pr_id ES UNO NUEVO PRESUPUESTO
			//-----------------------------------------------------------------
			if (pr_id == '') {
				anadir_fila()
				
				//EVITAMOS LA PRIMERA EJECUCION DE SUCURSALES AL CARGAR LA PAGINA
				var cambio = false;
				
				//EVITAMOS LA PRIMERA EJECUCION DE LOS DATOS DEL CLIENTE
				var cliente = false;
				
				function info_cliente() {
					$.post(
					"<?php echo base_url().$this->lang->lang() ?>/clientes/cliente",
					{'cl_id':$('#clientes option:selected').val()},
					function(data){
						$('.cliente .nombre').html(data["cl_nombre"]);
						$('.cliente .dni').html(data["cl_dni"]);
						$('.cliente .telefono').html(data["cl_telefono"]);
						$('.cliente .email').html(data["cl_email"]);
						
						var barriada = '';
						if (data["cl_barriada"] != '') {
							barriada = ' - ('+data["cl_barriada"]+')';
						}
						
						$('.cliente .direccion').html(data["cl_direccion"]+barriada);
						
						var postal = '';
						if (data["cl_postal"] != '') {
							postal = data["cl_postal"] + ', ';
						}
						
						$('.cliente .postal').html(postal+data["cl_localidad"]);
						$('.cliente .provincia').html(data["cl_provincia"]+', '+data["name"]);
						$('.radio_line input:eq(0)').val(data['su_iva']);
						$('.radio_line input:eq(1)').val(data['su_iva_reducido']);
						$('.radio_line input:eq(2)').val(data['su_iva_superreducido']);
                                                
                                                
						//CARGAMOS EL IVA POR PRIMERA VEZ
						iva = data['su_iva'];
						$('.impuesto').html(iva);
						su_zona_horaria = data["su_zona_horaria"];
						$('.moneda').html(data["currrency_symbol"]);
					}, "json");
					cliente = true;
				}
                                
                                //CARGAMOS MANTENIMIENTOS SEGUN EL CLIENTE
				function load_mantenimientos() {
					//RECUPERAMOS LOS CLIENTES
					$('#mantenimientos').html('');
					$.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?php echo base_url().$this->lang->lang() ?>/mantenimientos/mantenimientos_cliente",
                        data:{'cl_id':$('#clientes option:selected').val()},
                        success: function(data) {
                                $.each(data,function(indice) {
                                    if (indice == 0) {
                                        $('#mantenimientos').append('<option value="'+data[indice]["man_id"]+'" selected>'+data[indice]["man_nombre"]+'</option>');
                                    } else {
                                        $('#mantenimientos').append('<option value="'+data[indice]["man_id"]+'">'+data[indice]["man_nombre"]+'</option>');
                                    }
		
	});
	
                        },
                        complete: function() {
                            if (cliente) {
                                $('#mantenimientos').selectpicker('refresh'); 
                            } else {
                                $('#mantenimientos').selectpicker(); 
                            }
                        }
                    });
				}
				
				//CARGAMOS CLIENTES SEGUN LA SUCURSAL
				function load_clientes() {
					//RECUPERAMOS LOS CLIENTES
					$('#clientes, .cliente div').html('');
					$.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?php echo base_url().$this->lang->lang() ?>/clientes/clientes_sucursal",
                        data:{'su_id':$('#sucursales option:selected').val()},
                        success: function(data) {
                                $.each(data,function(indice) {
                                    if (indice == 0) {
                                       $('#clientes').append('<option value="'+data[indice]["cl_id"]+'" selected>'+data[indice]["cl_nombre"]+'</option>'); 
                                    } else {
                                        $('#clientes').append('<option value="'+data[indice]["cl_id"]+'">'+data[indice]["cl_nombre"]+'</option>');
                                    }
		
                                });
                        },
                        complete: function() {
                            if (cambio) {
                                $('#clientes').selectpicker('refresh'); 
                            } else {
                                $('#clientes').selectpicker(); 
                            }
                            
                            info_cliente();
                            load_mantenimientos();
                        }
                    });
				}
				
				//RELLENAMOS EL CAMPO DE SUCURSALES PARA AÑADIR O EDITAR
				$.post(
				"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
				{'buscar':''},
				function(data){
					$.each(data,function(indice) {
						//MOSTRAMOS TODOS LOS DATOS SI ES SUPERUSUARIO O SOLO AL QUE PERTENECE SI NO LO ES
						var su_id = '<?php echo $this->session->userdata("su_id") ?>';
						
							if (data[indice]['su_estado'] == '1') {    
                                                                if (data[indice]["su_id"] == su_id) {
									$('#sucursales').append('<option value="'+data[indice]["su_id"]+'" selected>'+data[indice]["su_nombre"]+'</option>');
								} else {
									$('#sucursales').append('<option value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
								}
							}
					});
					cambio = true;
					load_clientes();
				}, "json");
				
				//CARGAMOS LA INFORMACION DEL CLIENTE
				$("#clientes").change(function () {
					if (cliente) {
						info_cliente();
                                                load_mantenimientos();
					}	
				}).trigger('change');
				
				//BUSCAMOS LOS CLIENTES DE LA SUCUSAL AL CAMBIAR LA SUCURSAL
				$("#sucursales").change(function () {
					if (cambio) {
						load_clientes();
					}
				}).trigger('change');
				
			} else {
				
				//YA EXISTE PRESUPUESTO, OCULTAMOS LOS SELECT
				$('.info_cliente').hide();
				//RECOGEMOS DEL CONTROLADOR LA INFO DEL PRESUPUESTO
				$('.cliente .nombre').html('<?php echo isset($cl_nombre)?$cl_nombre:"" ?>');
				$('.cliente .dni').html('<?php echo isset($cl_dni)?$cl_dni:"" ?>');
				$('.cliente .telefono').html('<?php echo isset($cl_telefono)?$cl_telefono:"" ?>');
				$('.cliente .email').html('<?php echo isset($cl_email)?$cl_email:"" ?>');
				
				var barriada = '<?php echo isset($cl_barriada)?$cl_barriada:"" ?>';
				if (barriada != '') {
					barriada = ' - ('+barriada+')';
				}
				
				$('.cliente .direccion').html('<?php echo isset($cl_direccion)?$cl_direccion:"" ?>'+barriada);
				
				var postal = '<?php echo isset($cl_postal)?$cl_postal:"" ?>';
				if (postal != '') {
					postal = postal + ', ';
				}
				
				$('.cliente .postal').html(postal+'<?php echo isset($cl_localidad)?$cl_localidad:"" ?>');
				$('.cliente .provincia').html('<?php echo isset($cl_provincia)?$cl_provincia:"" ?>'+', '+'<?php echo isset($name)?$name:"" ?>');
				$('.moneda').html('<?php echo isset($currrency_symbol)?$currrency_symbol:"" ?>');
				
				//PRESUPUESTO ABIERTO
				if ((pr_estado != 1) && (dep != '4') && (dep != '5')) {
				//if (pr_estado != 1) {
					//RELLENAMOS LAS FILAS CON LOS INPUT DE LOS DATOS DEL PRESUPUESTO
					for (var y=0; y<array_servicios.length-1; y++) {
						fila = fila + '<tr>';
						
						if (editar == 1) {
							fila = fila + '<td width="3%" class="text-center vert-align"><i class="fa fa-times mano" title="<?php echo lang("borrar.fila") ?>" onclick="javascript: this.parentNode.parentNode.remove();"></i></td>';
							fila = fila + '<td><textarea name="pr_servicio" class="pr_servicio form-control" style="width:100%" maxlength="4000">'+array_servicios[y]+'</textarea><input name="pr_servicio" class="pr_servicio form-control" type="hidden" value="'+array_servicios[y]+'" /><div class="text-danger"></div></td>';
						} else {
							fila = fila + '<td colspan="2"><textarea name="pr_servicio" class="pr_servicio form-control" style="width:100%" maxlength="4000"></textarea><input name="pr_servicio" class="pr_servicio form-control" type="hidden" value="'+array_servicios[y]+'" /><div class="text-danger"></div></td>';
						}
								
						fila = fila + '<td class="text-right"><input name="pr_precio" class="pr_precio form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');" value="'+array_costes[y]+'" /><div class="text-danger"></div></td>';
								
						fila = fila + '<td class="text-right"><input name="pr_cantidad" class="pr_cantidad form-control text-right" onkeyup="var pattern = /[^0-9]/g;this.value = this.value.replace(pattern, \'\');" value="'+array_cantidades[y]+'" /><div class="text-danger"></div></td>';
								
						fila = fila + '<td class="text-right"><input name="pr_descuento" class="pr_descuento form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');" value="'+array_descuentos[y]+'" /><div class="text-danger"></div></td>';
						
						fila = fila + '<td style="display: none" class="text-right"><input name="pr_ganancia" class="pr_ganancia form-control text-right" onkeyup="var pattern = /[^0-9\.]/g;this.value = this.value.replace(pattern, \'\');" value="'+array_ganancias[y]+'" /><div class="text-danger"></div></td>';
							
						fila = fila + '<td class="text-right"><strong></strong></td>';
								
						fila = fila + '</tr>';
					}
						
					$('#listar tbody').append(fila);
					
					if (editar == 0) {
						$('#example1 tbody input').attr('readonly', true);
						$('#example1 tfoot tr:eq(0) *').remove();
						$('.radio_line label div').addClass('disabled');
					}
					
					recalcular();					
						
					if ((array_servicios.length-1) == 0) {
						//CARGAMOS LA PRIMERA FILA
						anadir_fila();
					}
				} else {
					//PRESUPUESTO CERRADO
					//$('#guardar_presupuesto, #cerrar_presupuesto, .botones_fila, .radio_line').hide();
					//CARGAMOS LAS FILAS CON LOS DATOS DEL PRESUPUESTO
					for (var y=0; y<array_servicios.length-1; y++) {
						fila = fila + '<tr>';
								
						fila = fila + '<td colspan="2">'+array_servicios[y].split('\n').join('<br />')+'</td>';
								
						fila = fila + '<td class="text-right">'+array_costes[y]+'</td>';
								
						fila = fila + '<td class="text-right">'+array_cantidades[y]+'</td>';
								
						fila = fila + '<td class="text-right">'+array_descuentos[y]+'</td>';
						
						fila = fila + '<td style="display: none" class="text-right">'+array_ganancias[y]+'</td>';
								
						fila = fila + '<td class="text-right"></td>';
								
						fila = fila + '</tr>';
					}
						
					$('#listar tbody').append(fila);
					recalcular();
				}
				
				//MANDAMOS LA FACTURA POR EMAIL
				$('#mandar_email').click(function(e){
					e.preventDefault();
						
					//LOADING BOTON
					var l = Ladda.create( document.querySelector( "#mandar_email" ) );
				 	l.start();
				 	
				 	var cl_email = '<?php echo isset($cl_email)?$cl_email:"" ?>';
				 	var emails = '<?php echo isset($cl_emails)?$cl_emails:"" ?>';
				 	var emails_info = '<?php echo isset($cl_emails_info)?$cl_emails_info:"" ?>';
				 	
				 	if (emails != '') {
						emails = cl_email + ';' + emails;
					} else {
						emails = cl_email + ';';
					}
					
					emails = emails.split(";");
					emails_info = emails_info.split(";");
				 	
				 	var listar = '<div class="row"><div class="col-lg-8 col-xs-12"><strong><?php echo lang("email") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("no") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("para") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("cc") ?></strong></div><div class="col-lg-1 col-xs-4 text-center"><strong><?php echo lang("cco") ?></strong></div></div><br>';
				 	for (var i=0; i<emails.length-1;i++) {
						listar = listar + '<div class="row">';
						
						listar = listar + '<div class="col-lg-8 col-xs-12">';
						listar = listar + emails[i];
						listar = listar + '</div>';
						
						check = '';
						if (i == 0) {
							check = 'checked';
						}
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="0" checked>';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="1" '+check+'>';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="2">';
						listar = listar + '</div>';
						
						listar = listar + '<div class="col-lg-1 col-xs-4 text-center">';
						listar = listar + '<input type="radio" name="email'+i+'" value="3">';
						listar = listar + '</div>';
						
						var info = '';
						if (i > 0) {
							info = emails_info[i-1]
						}
						
						listar = listar + '<div class="col-lg-12">';
						listar = listar + '<p class="text-muted">' + info + '</p>';
						listar = listar + '</div>';
						
						listar = listar + '</div>';
					}
					
					bootbox.dialog({
						title: "<?php echo lang('emails.cliente') ?>",
						message: listar,
						buttons: {
							success: {
								label: "<?php echo lang('cancelar') ?>",
								className: "btn-default",
								callback: function(result) {
									l.stop();
								}
							},
							danger: {
								label: "<i class='fa fa-envelope'></i> <?php echo lang('enviar') ?>",
								className: "btn-primary",
								callback: function(result) {
									var datos = new Array();
									var cont = 0;
									$('.modal-content .modal-body .row').each(function(index) {
										if (index > 0) {
											var email = $(this).find('div:eq(0)').html();
											var value = '';
											
											$('.modal-content .modal-body .row:eq('+index+') input').each(function(){
												if ($(this).is(':checked')) {
													value = $(this).val();
													if (value > 0) {
														cont++;
													}
												}
											});
											
											var aux = {
												'email'	:	email,
												'value'	:	value
											};
											datos[datos.length] = aux;
										}
									});
									
									if (cont > 0) {
										$.post(
										    "<?php echo base_url().$this->lang->lang() ?>/presupuestos/enviar_email",
										    {'pr_id':pr_id,'datos':datos},
										    function(data){
										    	bootbox.alert('<?php echo lang("presupuesto.email.ok") ?>');
										    	l.stop();
										    }
										);
									} else {
										bootbox.alert('<?php echo lang("emails.seleccion") ?>');
										l.stop();
									}
								}
							}
						}
					});
				});
				
				//REDIRIGE A LA FACTURA CREADA POR EL PRESUPUESTO CERRADO
				$('#ver_factura').click(function(e){
					e.preventDefault();
					$.post(
					    "<?php echo base_url().$this->lang->lang() ?>/facturas/factura_presupuesto",
					    {'pr_id':pr_id},
					    function(data){
							var url = '<?php echo base_url().$this->lang->lang() ?>/facturas/editar_factura/'+data['fa_id'];
							$(location).attr('href',url);
					    },'json'
					);
				});
				
				$('.impuesto').html(iva);
			}
		});
</script>