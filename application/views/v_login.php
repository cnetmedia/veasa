<?php

	//GENERAMOS LOS ATRIBUTOS PARA EL FORMULARIO
	$atributos_form = array(
		'class' 	=>	'form-signin'
	);
	
	$input_dni = array(
		'name'		=>	'emp_dni',
		'id'		=>	'emp_dni',
		'class'		=>	'form-control',
		'maxlength'	=>	'50',
		'value'		=>	set_value('emp_dni')
	);
	
	$input_email_cliente = array(
		'name'		=>	'cliente',
		'id'		=>	'cliente',
		'class'		=>	'form-control',
		'maxlength'	=>	'50',
		'value'		=>	set_value('cliente')
	);
	
	$input_clave = array(
		'name'		=>	'emp_clave',
		'id'		=>	'emp_clave',
		'class'		=>	'form-control',
		'maxlength'	=>	'6',
		'value'		=>	set_value('emp_clave')
	);
	
	$input_clave_cliente = array(
		'name'		=>	'cl_clave',
		'id'		=>	'cl_clave',
		'class'		=>	'form-control',
		'maxlength'	=>	'6',
		'value'		=>	set_value('cl_clave')
	);
	
	$lb_error = array(
		'class'		=>	'control-label'
	);
?>

<div class="panel panel-default" id="login">  
	<div class="">
	  	<div class="nav-tabs-custom">
	    	<ul class="nav nav-tabs">
	        	<li class="active">
	        		<a href="#tab_1" data-toggle="tab"><?php echo lang('empleados') ?></a>
	        	</li>
	            <li class="">
	            	<a href="#tab_2" data-toggle="tab"><?php echo lang('clientes') ?></a>
	            </li>
	        </ul>
	        
	        <div class="tab-content">
	        	<div class="tab-pane active" id="tab_1">
	            	<!-- INICIAMOS EL FORMULARIO -->
					<form id="empleados">
					<div class="form-group">
						<?php echo form_label(lang('dni'),'lb_clave',$lb_error) ?>
						<?php echo form_input($input_dni) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('emp_dni') ?></div>
					</div>
					
					<div class="form-group">
						<?php echo form_label(lang('login.clave'),'lb_clave',$lb_error) ?>
						<?php echo form_password($input_clave) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('emp_clave') ?></div>
					</div>
					
					<div>
						<button id="btn_empleados" data-style="zoom-out" class="btn btn-lg btn-primary btn-block ladda-button"><?php echo lang('entrar') ?></button>
					</div>
					<?=form_hidden('token',$token)?>
					</form>
					<!-- CERRAMOS EL FORMULARIO -->
	            </div>
	            
	            
	            
	            <div class="tab-pane" id="tab_2">
	            	<!-- INICIAMOS EL FORMULARIO -->
					<form id="clientes">
					<div class="form-group">
						<?php echo form_label(lang('email.mantenimiento'),'lb_clave',$lb_error) ?>
						<?php echo form_input($input_email_cliente) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('cliente') ?></div>
					</div>
					
					<div class="form-group">
						<?php echo form_label(lang('login.clave'),'lb_clave',$lb_error) ?>
						<?php echo form_password($input_clave_cliente) ?>
						<span class="glyphicon glyphicon-remove form-control-feedback"></span>
						<div class="text-danger"><?php echo form_error('cl_clave') ?></div>
					</div>
					
					<div>
						<button id="btn_clientes" data-style="zoom-out" class="btn btn-lg btn-primary btn-block ladda-button"><?php echo lang('entrar') ?></button>
					</div>
					<?=form_hidden('token',$token)?>
					</form>
					<!-- CERRAMOS EL FORMULARIO -->
	            </div>
	        </div>
	    </div>
	</div>

  	<div class="panel-footer text-right">
  		<img src="<?php echo base_url() ?>img/load.gif" width="16" height="16"/>
  		<a href="#"><?php echo lang('restaurar.clave') ?></a>
  	</div>
</div>

<script>
$(document).ready(function(){
	//OCULTAMOS TODOS LOS CAMPOS ERRONEOS DE COLOR ROJO
	$('#login .form-group span, .panel-footer img').hide();
	
	//CAPTURAMOS EL ENVIO DE LOS EMPLEADOS
	$("#btn_empleados").click(function(e){
	    e.preventDefault();
	    //GENERAMOS Y CARGAMOS EL LOADING
	    var l = Ladda.create( document.querySelector( "#btn_empleados" ) );
	 	l.start();
	 	
	    //ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#empleados .form-group").removeClass("has-error has-feedback");
	    $('#empleados .form-group span').hide();
	    $("#empleados .form-group .text-danger").html('');
	    
	    var datos_emp = {
			'emp_dni' : $("#empleados #emp_dni").val(),
			'emp_clave' : Aes.Ctr.encrypt($("#empleados #emp_clave").val(), $("#empleados input[name=token]").val(), 256),
			'token' : $("#empleados input[name=token]").val()
		};
	    
	    //INICIO PETICION AJAX
	    $.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/empleados/entrar",
			data: datos_emp,
			success: function(data) {
				if (data.status == false) {
					//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
					//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
					$.each(data.errors, function (ind, elem) {
						$.each(data.errors[ind], function (ind2, elem2) {
							$('#empleados input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
							$('#empleados input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
							$('#empleados input[name='+data.errors[ind][0]+']').parent().find('span').show();
						}); 
					});
					
					l.stop();
				} else {
					//LOGIN CORRECTO, REDIRIGIMOS
					$(location).attr('href',data.url);
				}
			}
		});
	});
	
	//CAPTURAMOS EL ENVIO DE LOS CLIENTES
	$("#btn_clientes").click(function(e){
	    e.preventDefault();
	    //GENERAMOS Y CARGAMOS EL LOADING
	    var l = Ladda.create( document.querySelector( "#btn_clientes" ) );
	 	l.start();
	 	
	    //ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#clientes .form-group").removeClass("has-error has-feedback");
	    $('#clientes .form-group span').hide();
	    $("#clientes .form-group .text-danger").html('');
	    
	    var datos_cl = {
			'cliente' : $("#clientes #cliente").val(),
			'cl_clave' : Aes.Ctr.encrypt($("#clientes #cl_clave").val(), $("#clientes input[name=token]").val(), 256),
			'token' : $("#clientes input[name=token]").val()
		};
	    
	    //INICIO PETICION AJAX
	    $.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/clientes/entrar",
			data: datos_cl,
			success: function(data) {
				if (data.status == false) {
					//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
					//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
					$.each(data.errors, function (ind, elem) {
						$.each(data.errors[ind], function (ind2, elem2) {
							$('#clientes input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
							$('#clientes input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
							$('#clientes input[name='+data.errors[ind][0]+']').parent().find('span').show();
						}); 
					});
					
					l.stop();
				} else {
					//LOGIN CORRECTO, REDIRIGIMOS
					$(location).attr('href',data.url);
				}
			}
		});
	});
	
	//RECORDAR CONTRASEÑA
	/*$('.panel-footer a').click(function(e){
		e.preventDefault();
		$('a').css({'color':'grey','cursor':'default'});
		$('.panel-footer img').show();
		//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#login .form-group").removeClass("has-error has-feedback");
	    $('#login .form-group span').hide();
	    $("#login .form-group .text-danger").html('');
	    
	    //RECORDAMOS LA CLAVE DEL CLIENTE
			$.ajax({
				type: "POST",
				dataType: "json",
			  	url: "<?php echo base_url().$this->lang->lang() ?>/clientes/recordar_clave",
			  	data: {'cliente' : $("#cliente").val()},
			  	success: function(data) {
			  		if (data.sql) {
				  		if (data.status) {
				  				bootbox.alert('<?php echo lang("clientes.recordar.clave.ok") ?>');
				  		} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('span').show();
								}); 
							});	
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
					}
					$('a').css({'color':'#3c8dbc','cursor':'pointer'});
					$('.panel-footer img').hide();
			  	},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
					$('a').css({'color':'#3c8dbc','cursor':'pointer'});
					$('.panel-footer img').hide();
				}
			});
	});*/
	
	//RESTAURAR CLAVE DE EMPLEADO
	$('.panel-footer a').click(function(e){
		e.preventDefault();
		$('a').css({'color':'grey','cursor':'default'});
		$('.panel-footer img').show();
		//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	    $("#login .form-group").removeClass("has-error has-feedback");
	    $('#login .form-group span').hide();
	    $("#login .form-group .text-danger").html('');
		
	    //RESTAURAMOS LA CLAVE DEL EMPLEADO
			$.ajax({
				type: "POST",
				dataType: "json",
			  	url: "<?php echo base_url().$this->lang->lang() ?>/empleados/recordar_clave",
			  	data: {'emp_dni' : $("#empleados #emp_dni").val()},
			  	success: function(data) {
			  		if (data.sql) {
				  		if (data.status) {
				  				var msg = '<?php echo lang("recordar.clave.ok") ?>';
								bootbox.alert(msg.replace('{{EMAIL}}',data.email));
				  		} else {
							//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
							//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('span').show();
								}); 
							});	
						}
					} else {
						bootbox.alert('<?php echo lang("error.ajax") ?> ERROR DE DATA');
					}
					$('a').css({'color':'#3c8dbc','cursor':'pointer'});
					$('.panel-footer img').hide();
			  	},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('<?php echo lang("error.ajax") ?> ERROR DEL METODO' +' '+ textStatus +' '+ errorThrown);
					$('a').css({'color':'#3c8dbc','cursor':'pointer'});
					$('.panel-footer img').hide();
				}
			});
	});
	
});
</script>