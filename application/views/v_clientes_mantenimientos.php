<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("contacto") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("email") ?></th>
                		<th><?php echo lang("localidad") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("nombre") ?></th>
                		<th><?php echo lang("contacto") ?></th>
                		<th><?php echo lang("telefono") ?></th>
                		<th><?php echo lang("email") ?></th>
                		<th><?php echo lang("localidad") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//MOSTRAR EMPLEADOS
	function mostrar_mantenimientos() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/clientes/mantenimientos_panel_cliente",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
						table = table + '<tr id="'+data[indice]['man_id']+'">';
						table = table + '<td>'+data[indice]['man_nombre']+'</td>';
	  					table = table + '<td>'+data[indice]['man_contacto']+'</td>';
	  					table = table + '<td>'+data[indice]['man_telefono']+'</td>';
	  					table = table + '<td>'+data[indice]['man_email']+'</td>';
	  					table = table + '<td>'+data[indice]['man_localidad']+'</td>';
	  					table = table + '</tr>';
					});
					$('#listar tbody').html(table);
					
					$("#example1").dataTable({
						"aaSorting": [[ 0, "asc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
			        
			        //OCULTAMOS EL BOTON DE NUEVO
			        $('#listar #btn_nuevo').hide();
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					    window.location.reload(true);
					});
					
					//EDITAR O VER MANTENIMIENTO
				   	$("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click","tbody tr",function(e) {
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/clientes/ver_mantenimiento/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_mantenimientos();
});
</script>