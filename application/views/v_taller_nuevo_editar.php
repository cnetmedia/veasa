<?php
$disable = '';
$enviar = false;
if ($ta_id != '' && $this->session->userdata('emp_tipo') > 2) {
	$disable = 'disabled';
}
if ($ta_estado == '1' && $this->session->userdata('emp_tipo') > 1) {
	$disable = 'disabled';
	$enviar = true;
}
if($this->session->userdata('emp_tipo') == 0){
	$disable = '';
}
if($ta_estado >= 2){
	$disable = 'disabled';
}
?>
<script>
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
return ((key >= 48 && key <= 57) || (key==8) || (key == 46));
}
</script>

<div id="taller">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
	    	<li class="active">
	        	<a href="#tab_1" data-toggle="tab"><?php echo lang('trabajo.taller') ?></a>
	        </li>
	        <?php if ($ta_estado <= 1 && $disable == '') { ?>
	        <li class="">
	        	<a href="#tab_2" data-toggle="tab"><?php echo lang('productos.almacen') ?></a>
	        </li>
	        <?php } ?>
	        <li class="">
	        	<a href="#tab_3" data-toggle="tab"><?php echo lang('bitacora') ?></a>
	        </li>
	        <li class="">
	        	<a href="#tab_4" data-toggle="tab"><?php echo lang('autorizacion') ?></a>
	        </li>
	    </ul>
	        
	    <div class="tab-content">
	    	<div class="tab-pane active" id="tab_1">
	    		<div id="info" class="row">
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label><?php echo lang('fecha.trabajo.taller') ?></label>
									<input class="form-control" id="fecha_trabajo" <?php echo $disable ?> />
								</div>
							</div>
							
							<div class="col-lg-12">
								<div class="form-group">
									<label><?php echo lang('presupuesto') ?></label>
									<select class="selectpicker form-control" data-live-search="true" id="cotizaciones" name="pr_id" <?php echo $disable ?>></select>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-lg-8 col-md-8 col-sm-6">
						<div class="form-group">
							<label><?php echo lang('observaciones') ?></label>
							<textarea class="form-control" id="observaciones_taller"><?php echo isset($ta_observaciones)?$ta_observaciones:'' ?></textarea>
						</div>
					</div>
					
					<div class="col-lg-8 col-md-8 col-sm-6">
						<div class="form-group">
							<label><?php echo lang('empleados.registro') ?></label>
						</div>
					</div>
				</div>
				
				<br>
				
				<div class="row">
	    			<div class="col-lg-12">
	    				<label><?php  echo lang('trabajos.realizar.taller')?></label>
	    				
	    				<table id="trabajos-taller" class="table table-bordered">
	    					<thead>
	    						<?php if ($ta_estado == 0 || $disable == '') { ?>
	    						<th></th>
	    						<?php } ?>
	    						<th class="col-sm-2"><?php echo lang('referencia') ?></th>
                                <th class="col-sm-3"><?php echo lang('descripcion') ?></th>
                                <th class="col-sm-1"><?php echo lang('ancho') ?></th>
                                <th class="col-sm-1"><?php echo lang('alto') ?></th>
                                <th class="col-sm-1"><?php echo lang('longitud') ?></th>
	    						<th class="col-sm-1"><?php echo lang('cantidad') ?></th>
	    						<th class="col-sm-1"><?php echo lang('cantidad.para.enviar.corto') ?></th>
                                <th class="col-sm-2"><?php echo lang('observaciones') ?></th>
                                <th class="col-sm-1"><?php echo lang('reposicion') ?></th>
	    					</thead>
	    					
	    					<tbody></tbody>
	    					
	    					<tfoot>
		    					<tr>
		    						<?php if ($ta_estado <= 1) { ?>
		    						<td></td>
		    						<?php } ?>
		    						<td colspan="9" class="text-center">
		    						<?php if ($disable == '') { ?>
										<button id="anadir_trabajo" type="button" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-plus"></i> <?php echo lang('anadir.trabajo.taller') ?></button>
                                         <button id="anadir_trabajo_compuesto" type="button" class="btn btn-primary ladda-button" data-style="zoom-out"><i class="fa fa-plus"></i> <?php echo lang('anadir.trabajo.compuesto.taller') ?></button>
									<?php } ?>
	    								
                                    </td>
		    					</tr>
	    					</tfoot>
	    				</table>
	    			</div>
	    		</div>
				
				<br>
				
				<div class="row">
					<div class="col-lg-12">
						<label><?php echo lang('productos.trabajo.taller') ?></label>
						
						<table aria-describedby="example1_info" id="example2" class="table table-bordered table-striped table-hover dataTable">
					    	<thead>
					        	<tr>
					        		<?php if ($ta_id == '' || $disable == '') {echo '<th class="col-sm-1">'.lang("eliminar").'</th>';} ?>
					            	<th class="col-sm-1" class="col-md-2"><?php echo lang("referencia") ?></th>
					                <th class="col-sm-2"><?php echo lang("descripcion") ?></th>
					                <th class="col-sm-3"><?php echo lang("caracteristicas") ?></th>
					                <th class="col-sm-1"><?php echo lang("proveedor") ?></th>
					                <th class="col-sm-1"><?php echo lang("vista.previa") ?></th>
					                <th class="col-sm-1"><?php echo lang("cantidad") ?></th>
					                <?php if ($ta_estado == 2) { ?>
					                <th class="col-sm-3"><?php echo lang("reciclaje") ?></th>
					                <?php } ?>
					           	</tr>
							</thead>
					                                       
							<tfoot>
					              	<tr>
					              		<?php if ($ta_id == '' || $disable == '') {echo '<th class="col-sm-1">'.lang("eliminar").'</th>';} ?>
					              		<th><?php echo lang("referencia") ?></th>
					              		<th><?php echo lang("descripcion") ?></th>
					              		<th><?php echo lang("caracteristicas") ?></th>
					              		<th><?php echo lang("proveedor") ?></th>
					              		<th><?php echo lang("vista.previa") ?></th>
					              		<th id="cantidad"><?php echo lang("cantidad") ?></th>
					              		<?php if ($ta_estado == 2) { ?>
						                <th><?php echo lang("reciclaje") ?></th>
						                <?php } ?>
					              	</tr>
							</tfoot>
									
							<tbody aria-relevant="all" aria-live="polite" role="alert">
							<?php
							$product_cantidad = isset($product_cantidad)?$product_cantidad:'';
							
							if ($product_cantidad != '') {
								$table = '';
								
								$cantidad = explode(';',$product_cantidad);
								$cantidad = array_filter($cantidad);
								
								//ALMACEN DONDE ESTA EL PRODUCTO CUANDO SE GUARDO PARA EL TALLER
								$almacen = explode(';',$al_id);
								$almacen = array_filter($almacen);
								
								for ($i=0; $i<count($productos); $i++) {
									$opciones = '';
									if ($productos[$i]->product_caracteristicas != '') {
										$aux = explode(';',$productos[$i]->product_caracteristicas);
										$aux = array_filter($aux);
										for ($t=0; $t<count($aux); $t++) {
											$aux2 = explode('/',$aux[$t]);
											$aux2 = array_filter($aux2);
											
											for ($z=0; $z<count($aux2); $z++) {
											$opciones = $opciones.'<span class="opc_product_list"><strong>'.$aux2[0].'</strong>: <span>'.$aux2[1].'</span></span>';	
											}
										}
									}
									
									//CARACTERISTICAS SOLO PARA RECICLADOS
									$reciclaje = '';
									if (isset($productos[$i]->pra_reciclaje)) {
										if ($productos[$i]->pra_reciclaje != '') {
											$reciclaje = '<br>* '.$productos[$i]->pa_reciclaje;
										}
									}
									
									
									$table = $table.'<tr nuevo="false" al_id="'.$almacen[$i].'" product_id="'.$productos[$i]->product_id.'">';

									if ($ta_id == '' || $disable == '') {
										$table = $table.'<td class="text-center"><i class="fa fa-times eliminar"></i></td>';
									}
									
									$table = $table.'<td class="vcenter"><span>'.$productos[$i]->product_referencia.'</span></td>';
									$table = $table.'<td class="vcenter">'.$productos[$i]->product_nombre.'</td>';
									$table = $table.'<td class="vcenter">'.$opciones.$reciclaje.'</td>';
									$table = $table.'<td class="vcenter">'.$productos[$i]->pro_nombre.'</td>';
									$table = $table.'<td class="text-center vcenter">'.$productos[$i]->fotos.'</td>';
									$table = $table.'<td class="text-center vcenter">'.$cantidad[$i].'</td>';
									
									if ($ta_estado == 2) {
										$table = $table.'<td><div class="input-group margin" style="width:34%;display:inline-block"><label>Tamaño</label></div><div class="input-group margin" style="display:inline"><label>Cantidad</label></div><div class="reciclar input-group margin"><input placeholder="Tamaño/Caracteristicas" type="text" name="pra_reciclaje[]" class="form-control pra_reciclaje" value="" style="width:50%;display:inline; margin-right:10px">';
										
										$table = $table.'<input placeholder="'.lang("cantidad.reciclaje").'" type="text" name="cantidad_reciclaje[]" onKeypress="return soloNumeros(event)" class="text-center form-control cantidad_reciclaje" value="" style="width:30%;display:inline">
										<span style="width:15%" class="input-group-btn"><button id="btn0" class="btn btn-primary btn-flat ladda-button" data-style="zoom-out" type="button"><i class="fa fa-plus-circle"></i></button></span></div>';
									}
									
									$table = $table.'</tr>';
								}
								
								echo $table;
							}
							?>
							</tbody>
						</table>
					</div>
				</div>

				<br>
				
				<div class="row">
					<div class="col-lg-12 text-right">
						<?php 
						if ($this->session->userdata('emp_tipo') <= 2 && $ta_estado <= 2) {
							if ($this->session->userdata('emp_editar') == 1 || $this->session->userdata('emp_crear') == 1) {
						?>
						<?php if ($ta_estado == '2') { ?>
						 	<button id="terminar_trabajo" type="button" class="btn btn-warning ladda-button" data-style="zoom-out"><i class="fa fa-check"></i> <?php echo lang('cerrar') ?></button>
						<?php } ?>
						 
						<?php if ($ta_estado != '2') { ?>
							<?php if (($this->session->userdata('de_id') != '3') && ($this->session->userdata('de_id') != '4') && ($this->session->userdata('de_id') != '5')) { ?>
								<?php if(!$enviar): ?>
									<button id="guardar_trabajo" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-save"></i> <?php echo lang('guardar') ?></button>
								<?php else: ?>
									<button id="guardar_trabajo" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-save"></i> <?php echo lang('enviar') ?></button>
								<?php endif; ?>
							<?php } ?>
						<?php } ?>
						<?php if ($ta_estado == '0') { ?>
							<?php if (($this->session->userdata('de_id') != '3') && ($this->session->userdata('de_id') != '4') && ($this->session->userdata('de_id') != '5')) { ?>
								<button id="procesar_trabajo" type="button" class="btn btn-warning ladda-button" data-style="zoom-out" data-estado="1"><i class="fa fa-check"></i> <?php echo lang('talleres.procesar') ?></button>
							<?php } ?>
						<?php } ?>
						
						<?php }
						}
						?>
					<?php if(isset($_GET['view'])): ?>
						<a href="<?php echo base_url().$this->lang->lang().'/informes' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i><?php echo lang('atras') ?></a>
					<?php elseif(isset($_GET['tag'])): ?>
						<a href="<?php echo base_url().$this->lang->lang().'/talleres' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i><?php echo lang('atras') ?></a>
                     <?php else: ?>
                     	<a href="<?php echo base_url().$this->lang->lang().'/taller' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i><?php echo lang('atras') ?></a>
                     <?php endif; ?>
					</div>
				</div>
	    	</div>
	    
	    	<div class="tab-pane" id="tab_2">
	    		<div class="row">
	    			<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label><?php echo lang('almacenes') ?></label>
							<select class="form-control" id="almacenes"></select>
						</div>
					</div>
	    		</div>
	    		
	    		<br>
	    		
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<div class="box-body table-responsive">
							<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
								<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
					            	<thead>
					                	<tr>
					                		<th class="col-sm-1"><?php echo lang("referencia") ?></th>
					                		<th class="col-sm-2"><?php echo lang("descripcion") ?></th>
					                		<th class="col-sm-3"><?php echo lang("caracteristicas") ?></th>
					                		<th class="col-sm-3"><?php echo lang("proveedor") ?></th>
					                		<th class="col-sm-1"><?php echo lang("vista.previa") ?></th>
					                		<th class="col-sm-1"><?php echo lang("cantidad.disponible") ?></th>
					                		<th class="col-sm-1"><?php echo lang("seleccionar.cantidad") ?></th>
					                	</tr>
									</thead>
					                                        
									<tfoot>
					                	<tr>
					                		<th><?php echo lang("referencia") ?></th>
					                		<th><?php echo lang("descripcion") ?></th>
					                		<th><?php echo lang("caracteristicas") ?></th>
					                		<th><?php echo lang("proveedor") ?></th>
					                		<th><?php echo lang("vista.previa") ?></th>
					                		<th><?php echo lang("cantidad.disponible") ?></th>
					                		<th><?php echo lang("seleccionar.cantidad") ?></th>
					                	</tr>
									</tfoot>
									
									<tbody aria-relevant="all" aria-live="polite" role="alert"></tbody>
								</table>	
							</div>
						</div>
	    			</div>
	    		</div>
	    		
	    		<br>
	    		
	    		<div class="row">
					<div class="col-lg-12 text-right">
						<a href="<?php echo base_url().$this->lang->lang().'/taller' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
					</div>
				</div>
	    	</div>

	    	<div class="tab-pane" id="tab_3">
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<table id="bitacora" class="table table-bordered">
	    					<thead>
	    						<th class="col-sm-2"><?php echo lang('fecha') ?></th>
                                <th class="col-sm-3"><?php echo lang('empleados') ?></th>
                                <th class="col-sm-3"><?php echo lang('accion') ?></th>
	    					</thead>
	    					
	    					<tbody></tbody>
	    					
	    					<tfoot>
		    					<tr>
		    						
		    					</tr>
	    					</tfoot>
	    				</table>
	    			</div>
	    		</div>
	    	</div>

	    	<div class="tab-pane" id="tab_4">
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<table id="autorizaciones" class="table table-bordered">
	    					<thead>
                                <th class="col-sm-3">Usuario</th>
	    						<th class="col-sm-2">Categoria</th>
	    						<th class="col-sm-2">Costo adicional</th>
	    						<th class="col-sm-2"><?php echo lang('fecha') ?></th>
	    					</thead>
	    					
	    					<tbody></tbody>
	    					
	    					<tfoot>
		    					<tr>
		    						
		    					</tr>
	    					</tfoot>
	    				</table>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('#terminar_trabajo').hide();
	var disable = '<?php echo $disable ?>';
	var emp_tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	var pr_id = '<?php echo isset($pr_id)?$pr_id:"" ?>';
	var product_id = '<?php echo isset($product_id)?$product_id:"" ?>';
	var ta_id = '<?php echo isset($ta_id)?$ta_id:"" ?>';
	var ta_estado = '<?php echo isset($ta_estado)?$ta_estado:"0" ?>';
	var pr_ta_estado = '<?php echo isset($ta_estado)?$ta_estado:"0" ?>';
	var pr_eliminados = '';
	var donde = '';
        
	var trabajos_codigo = '<?php echo isset($ta_trabajos_codigo)?$ta_trabajos_codigo:"" ?>';
    var trabajos_ancho = '<?php echo isset($ta_trabajos_ancho)?$ta_trabajos_ancho:"" ?>';
    var trabajos_alto = '<?php echo isset($ta_trabajos_alto)?$ta_trabajos_alto:"" ?>';
    var trabajos_descripcion = '<?php echo isset($ta_trabajos_descripcion)?$ta_trabajos_descripcion:"" ?>';
    var trabajos_observaciones = '<?php echo isset($ta_trabajos_observaciones)?$ta_trabajos_observaciones:"" ?>';
    var trabajos_reposiciones = '<?php echo isset($ta_trabajos_reposiciones)?$ta_trabajos_reposiciones:"" ?>';
	var trabajos_cantidad = '<?php echo isset($ta_trabajos_cantidad)?$ta_trabajos_cantidad:"" ?>';
	var trabajos_terminados = '<?php echo isset($ta_trabajos_terminados)?$ta_trabajos_terminados:"" ?>';
    var trabajos_longitud = '<?php echo isset($ta_trabajos_longitud)?$ta_trabajos_longitud:"" ?>';
    
    var trabajos_compuestos_codigo = '<?php echo isset($ta_trabajos_compuestos_codigo)?$ta_trabajos_compuestos_codigo:"" ?>';
    var trabajos_compuestos_ancho = '<?php echo isset($ta_trabajos_compuestos_ancho)?$ta_trabajos_compuestos_ancho:"" ?>';
    var trabajos_compuestos_alto = '<?php echo isset($ta_trabajos_compuestos_alto)?$ta_trabajos_compuestos_alto:"" ?>';
    var trabajos_compuestos_descripcion = '<?php echo isset($ta_trabajos_compuestos_descripcion)?$ta_trabajos_compuestos_descripcion:"" ?>';
    var trabajos_compuestos_observaciones = '<?php echo isset($ta_trabajos_compuestos_observaciones)?$ta_trabajos_compuestos_observaciones:"" ?>';
    var trabajos_compuestos_reposiciones = '<?php echo isset($ta_trabajos_compuestos_reposiciones)?$ta_trabajos_compuestos_reposiciones:"" ?>';
	var trabajos_compuestos_cantidad = '<?php echo isset($ta_trabajos_compuestos_cantidad)?$ta_trabajos_compuestos_cantidad:"" ?>';
	var trabajos_compuestos_terminados = '<?php echo isset($ta_trabajos_compuestos_terminados)?$ta_trabajos_compuestos_terminados:"" ?>';
    var trabajos_compuestos_longitud = '<?php echo isset($ta_trabajos_compuestos_longitud)?$ta_trabajos_compuestos_longitud:"" ?>';
        
	var enviados_trabajos = true;
	var almacenes_reciclaje = '';
	
	//PESTAÑA 1
	//RELLENAMOS EL CAMPO DE PRESUPUESTOS
	$.post(
	"<?php echo base_url().$this->lang->lang() ?>/presupuestos/buscador",
	{'buscar':''},
	function(data){
		//MANTENIMIENTO ENCONTRADO
		var encontrado = false;
		
		$.each(data,function(indice) {
			if (data[indice]["pr_estado"] != '3') {
				if (pr_id == data[indice]["pr_id"]) {
					$('#cotizaciones').append('<option value="'+data[indice]["pr_id"]+'" man_id="'+data[indice]["man_id"]+'" selected>'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>');
				} else {
					$('#cotizaciones').append('<option value="'+data[indice]["pr_id"]+'" man_id="'+data[indice]["man_id"]+'">'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>');
				}
			}
		});
                
        $('#cotizaciones').selectpicker();
        $('#cotizaciones').selectpicker('refresh');
	}, "json");
	
	//CALENDARIO TRABAJO TALLER
	var ta_inicio = '<?php echo isset($ta_inicio)?$ta_inicio:"" ?>';
	var ta_final = '<?php echo isset($ta_final)?$ta_final:"" ?>';
	
	if (ta_inicio != '' && ta_final != '') {
		$('#fecha_trabajo').val(ta_inicio + ' - ' + ta_final);
	}
	
	var meses = '';
	var dias = '';
	var primerDia = 0;
	if ('<?php echo $this->lang->lang() ?>' == 'es') {
		meses = ['<?php echo lang("enero") ?>', '<?php echo lang("febrero") ?>', '<?php echo lang("marzo") ?>', '<?php echo lang("abril") ?>', '<?php echo lang("mayo") ?>', '<?php echo lang("junio") ?>', '<?php echo lang("julio") ?>', '<?php echo lang("agosto") ?>', '<?php echo lang("septiembre") ?>', '<?php echo lang("octubre") ?>', '<?php echo lang("noviembre") ?>', '<?php echo lang("diciembre") ?>'];
		
		dias = ['<?php echo lang("d") ?>', '<?php echo lang("l") ?>', '<?php echo lang("m") ?>', '<?php echo lang("x") ?>', '<?php echo lang("j") ?>', '<?php echo lang("v") ?>','<?php echo lang("s") ?>'];
		primerDia = 1;
	}
	
	var dateInicio = '<?php echo isset($ta_inicio)?$ta_inicio:"" ?>';
	if (dateInicio == '') {
		dateInicio = moment();
	}
	
	var dateFin = '<?php echo isset($ta_final)?$ta_final:"" ?>';
	if (dateFin == '') {
		dateFin = moment();
	}
	
	//CALENDARIO PARA LA JORNADA DE TRABAJO
	$('#fecha_trabajo').daterangepicker({
		timePicker: true,
		timePickerIncrement: 1,
		format: 'YYYY-MM-DD HH:mm',
		language: '<?php echo $this->lang->lang() ?>',
		timePicker12Hour: false,
		locale: {
            applyLabel: '<?php echo lang("aplicar") ?>',
            cancelLabel: '<?php echo lang("cancelar") ?>',
            fromLabel: '<?php echo lang("desde") ?>',
            toLabel: '<?php echo lang("hasta") ?>',
            firstDay: primerDia,
            daysOfWeek: dias,
            monthNames: meses
        },
        showDropdowns: true,
        showWeekNumbers: true,
        startDate: dateInicio,
        endDate: dateFin,
        minDate: dateInicio
	});
	
        //AÑADIR ITEMS
        function anadir_items(fila) {    
            $(fila).find('.atributos').append('<input type="text" value="" class="form-control" name="ta_trabajos_compuestos_codigo" />');
            $(fila).find('.atributos_descripcion').append('<input type="text" value="" class="form-control" name="ta_trabajos_compuestos_descripcion" />');
            $(fila).find('.atributos_ancho').append('<input type="text" value="" class="form-control text-center" name="ta_trabajos_compuestos_ancho" />');
            $(fila).find('.atributos_alto').append('<input type="text" value="" class="form-control text-center" name="ta_trabajos_compuestos_alto" />');
            $(fila).find('.atributos_longitud').append('<input type="text" value="" class="form-control text-center" name="ta_trabajos_compuestos_longitud" />');
            $(fila).find('.atributos_cantidad').append('<input type="text" value="" onKeypress="return soloNumeros(event)" class="text-center form-control" name="ta_trabajos_compuestos_cantidad" />');
            $(fila).find('.atributos_enviar').append('<div class="input-group"><input type="text" name="ta_trabajos_compuestos_terminados" onKeypress="return soloNumeros(event)" style="min-width: 47px !important;" class=" text-center form-control" value="0"><span class="input-group-addon" title="0 <?php echo lang("cantidad.enviada") ?>"><i class="fa fa-truck"></i> <span>0</span></span></div>');
            $(fila).find('.atributos_observaciones').append('<input type="text" value="" class="form-control" name="ta_trabajos_compuestos_observaciones" />');
            $(fila).find('.atributos_reposicion').append('<input type="checkbox" class="form-control" name="ta_trabajos_compuestos_reposicion" />');
        }
        
        //MOSTRAR Y OCULTAR ITEMS
        function mostrar_items(mostrar, fila) {
            //MOSTRAR
            if (mostrar || $(fila).find('.desplegar').find('i').hasClass('fa-arrow-down')) {
                $(fila).find('.atributos').show();
                $(fila).find('.atributos_descripcion').show();
                $(fila).find('.atributos_ancho').show();
                $(fila).find('.atributos_alto').show();
                $(fila).find('.atributos_longitud').show();
                $(fila).find('.atributos_observaciones').show();
                $(fila).find('.atributos_reposicion').show();
                $(fila).find('.atributos_cantidad').show();
                $(fila).find('.atributos_enviar').show();
                $(fila).find('.desplegar').find('i').removeClass('fa-arrow-down');
                $(fila).find('.desplegar').find('i').addClass('fa-arrow-up');
            } else {
                //OCULTAR
                $(fila).find('.atributos').hide();
                $(fila).find('.atributos_descripcion').hide();
                $(fila).find('.atributos_ancho').hide();
                $(fila).find('.atributos_alto').hide();
                $(fila).find('.atributos_longitud').hide();
                $(fila).find('.atributos_observaciones').hide();
                $(fila).find('.atributos_reposicion').hide();
                $(fila).find('.atributos_cantidad').hide();
                $(fila).find('.atributos_enviar').hide();
                $(fila).find('.desplegar').find('i').removeClass('fa-arrow-up');
                $(fila).find('.desplegar').find('i').addClass('fa-arrow-down');
            }  
        }
        
	//TRABAJOS A REALIZAR
	if (trabajos_codigo != '' || trabajos_compuestos_codigo != '') {
		//TRABAJOS SIMPLES
        var trabajos = trabajos_codigo.split(';');
		trabajos = trabajos.filter(Boolean);
                
        var ancho = trabajos_ancho.split(';');
		ancho = ancho.filter(Boolean);
                
        var alto = trabajos_alto.split(';');
		alto = alto.filter(Boolean);
                
        var longitud = trabajos_longitud.split(';');
		longitud = longitud.filter(Boolean);
                
        var descripcion = trabajos_descripcion.split(';');
		descripcion = descripcion.filter(Boolean);
                
        var observaciones = trabajos_observaciones.split(';');
		observaciones = observaciones.filter(Boolean);
		
		var reposiciones = trabajos_reposiciones.split(';');
		reposiciones = reposiciones.filter(Boolean);
		
		var cantidad = trabajos_cantidad.split(';');
		cantidad = cantidad.filter(Boolean);
		
		var terminados = trabajos_terminados.split(';');
		terminados = terminados.filter(Boolean);
		
		var table = '';
		
		var read = '';

		if(disable != ''){			
			if (ta_estado > 0 || disable != '') {
				$('#anadir_trabajo').hide();
			}
			if (ta_estado > 1 || disable != '') {
				read = 'disabled';
			}
		}
                
		for (var i=0; i<trabajos.length; i++) {
			table = table + '<tr tipo="simple" class="taller_simple">';			
				
				if (ta_estado == 0 || disable == '') {
					table = table + '<td class="text-center vcenter">';
					if (disable == '') {
						table = table + '<i class="fa fa-times pointer eliminar-compuesto"></i>';
					}
					table = table + '</td>';
				}
				
			
            table = table + '<td><input type="text" value="'+trabajos[i]+'" class="form-control" name="ta_trabajos_codigo" '+read+' /></td>';
            table = table + '<td><input type="text" value="'+descripcion[i]+'" class="form-control" name="ta_trabajos_descipcion" '+read+' /></td>';
            table = table + '<td><input type="text" value="'+ancho[i]+'" class="form-control text-center" name="ta_trabajos_ancho" '+read+' /></td>';
			table = table + '<td><input type="text" value="'+alto[i]+'" class="form-control text-center" name="ta_trabajos_alto" '+read+' /></td>';
            table = table + '<td><input type="text" value="'+longitud[i]+'" class="form-control text-center" name="ta_trabajos_longitud" '+read+' /></td>';
			table = table + '<td><input type="text" value="'+cantidad[i]+'" onKeypress="return soloNumeros(event)" class="text-center form-control" name="ta_trabajos_cantidad" '+read+' /></td>';
			
			var enviado_todo = '';
			if (cantidad[i] == terminados[i] || ta_estado == 2) {
				enviado_todo = 'readonly';
			} else {
				enviados_trabajos = false;
			}
			
			table = table + '<td><div class="input-group"><input type="text" name="ta_trabajos_terminados" style="min-width: 47px !important;" onKeypress="return soloNumeros(event)" class=" text-center form-control" value="0" '+enviado_todo+'>';
			table = table + '<span class="input-group-addon" title="'+terminados[i]+' <?php echo lang("cantidad.enviada") ?>"><i class="fa fa-truck"></i> <span>'+terminados[i]+'</span></span>';
			table = table + '</div></td>';
            table = table + '<td><input type="text" value="'+observaciones[i]+'" class="form-control" name="ta_trabajos_observaciones" '+read+' /></td>';
            
            if (reposiciones[i] == 'true') {
				table = table + '<td><input type="checkbox" class="form-control" name="ta_trabajos_reposicion" '+read+' checked/></td>';
			} else {
				table = table + '<td><input type="checkbox" class="form-control" name="ta_trabajos_reposicion" '+read+' /></td>';
			}
                        
			table = table + '</tr>';
		}
		
		$('#trabajos-taller tbody').prepend(table);
                
        //TRABAJOS COMPUESTOS
        var trabajos_compuestos = trabajos_compuestos_codigo.split(';');
		trabajos_compuestos = trabajos_compuestos.filter(Boolean);
                
		var descripcion_compuestos = trabajos_compuestos_descripcion.split(';');
		descripcion_compuestos = descripcion_compuestos.filter(Boolean);
                
        var ancho_compuestos = trabajos_compuestos_ancho.split(';');
		ancho_compuestos = ancho_compuestos.filter(Boolean);
                
        var alto_compuestos = trabajos_compuestos_alto.split(';');
		alto_compuestos = alto_compuestos.filter(Boolean);
                
        var longitud_compuestos = trabajos_compuestos_longitud.split(';');
		longitud_compuestos = longitud_compuestos.filter(Boolean);
                
        var observaciones_compuestos = trabajos_compuestos_observaciones.split(';');
		observaciones_compuestos = observaciones_compuestos.filter(Boolean);
		
		var reposiciones_compuestos = trabajos_compuestos_reposiciones.split(';');
		reposiciones_compuestos = reposiciones_compuestos.filter(Boolean);
		
		var cantidad_compuestos = trabajos_compuestos_cantidad.split(';');
		cantidad = cantidad.filter(Boolean);
		
		var terminados_compuestos = trabajos_compuestos_terminados.split(';');
		terminados = terminados.filter(Boolean);
		
		var table = '';
		var read = '';

		if(disable != ''){
			if (ta_estado > 1 || disable != '') {
				read = 'disabled';
			}

			if (ta_estado > 0 || disable != '') {
				$('#anadir_trabajo_compuesto').hide();
			}
		}
                
		for (var i=0; i<trabajos_compuestos.length; i++) {
			//alert(i);
            var aux_trabajos_compuestos = trabajos_compuestos[i].split('|');
            aux_trabajos_compuestos = aux_trabajos_compuestos.filter(Boolean);
            
            var aux_descripcion_compuestos = descripcion_compuestos[i].split('|');
            aux_descripcion_compuestos = aux_descripcion_compuestos.filter(Boolean);
            
            var aux_ancho_compuestos = ancho_compuestos[i].split('|');
            aux_ancho_compuestos = aux_ancho_compuestos.filter(Boolean);
            
            var aux_alto_compuestos = alto_compuestos[i].split('|');
            aux_alto_compuestos = aux_alto_compuestos.filter(Boolean);
            
            var aux_longitud_compuestos = longitud_compuestos[i].split('|');
            aux_longitud_compuestos = aux_longitud_compuestos.filter(Boolean);
            
            var aux_observaciones_compuestos = observaciones_compuestos[i].split('|');
            aux_observaciones_compuestos = aux_observaciones_compuestos.filter(Boolean);
            
            var aux_reposiciones_compuestos = reposiciones_compuestos[i].split('|');
            aux_reposiciones_compuestos = aux_reposiciones_compuestos.filter(Boolean);
            
            var aux_cantidad_compuestos = cantidad_compuestos[i].split('|');
            aux_cantidad_compuestos = aux_cantidad_compuestos.filter(Boolean);
            
            var aux_terminados_compuestos = terminados_compuestos[i].split('|');
            aux_terminados_compuestos = aux_terminados_compuestos.filter(Boolean);
            
            table = table + '<tr tipo="compuesto" class="taller_compuesto">';
            
            var borrar = '';
            var add = '';
           
			if (ta_estado < 1 || disable == '') {
				borrar = '<td class="text-center vcenter">';
				add = '<span class="input-group-addon anadir"><i class="fa fa-plus"></i></span>';
				if (disable == '') {
					borrar = borrar + '<i class="fa fa-times pointer eliminar-compuesto"></i>';
				}
				borrar = borrar + '</td>';
			}
            
            table = table + borrar + '<td><div class="input-group"><input type="text" name="ta_trabajos_compuestos_codigo" class="form-control" value="'+aux_trabajos_compuestos[0]+'" '+read+'>'+add+'<span class="input-group-addon desplegar"><i class="fa fa-arrow-down"></i></span></div><div class="atributos">';
            for (var t=1; t<aux_trabajos_compuestos.length; t++) {
                table = table + '<input type="text" class="form-control" name="ta_trabajos_compuestos_codigo" value="'+aux_trabajos_compuestos[t]+'" '+read+'/>';
            }
            table = table + '</div></td>';
            
            table = table + '<td><div class="atributos_descripcion">';
            for (var t=0; t<aux_descripcion_compuestos.length; t++) {
                table = table + '<input type="text" value="'+aux_descripcion_compuestos[t]+'" class="form-control" name="ta_trabajos_compuestos_descripcion" '+read+'/>';
            }
            table = table + '</div></td>';
            
            table = table + '<td><div class="atributos_ancho">';
            for (var t=0; t<aux_ancho_compuestos.length; t++) {
                table = table + '<input type="text" value="'+aux_ancho_compuestos[t]+'" class="text-center form-control" name="ta_trabajos_compuestos_ancho" '+read+'/>';
            }
            table = table + '</div></td>';
            
            table = table + '<td><div class="atributos_alto">';
            for (var t=0; t<aux_alto_compuestos.length; t++) {
                table = table + '<input type="text" value="'+aux_alto_compuestos[t]+'" class="text-center form-control" name="ta_trabajos_compuestos_alto" '+read+'/>';
            }
            table = table + '</div></td>';
            
            table = table + '<td><div class="atributos_longitud">';
            for (var t=0; t<aux_longitud_compuestos.length; t++) {
                table = table + '<input type="text" value="'+aux_longitud_compuestos[t]+'" class="text-center form-control" name="ta_trabajos_compuestos_longitud" '+read+'/>';
            }
            table = table + '</div></td>';
            
            table = table + '<td><div class="atributos_cantidad">';
            for (var t=0; t<aux_cantidad_compuestos.length; t++) {
                table = table + '<input type="text" value="'+aux_cantidad_compuestos[t]+'" onKeypress="return soloNumeros(event)" class="text-center form-control" name="ta_trabajos_compuestos_cantidad" '+read+'/>';
            }
            table = table + '</div></td>';
            
            
            table = table + '<td><div class="atributos_enviar">';
            for (var t=0; t<aux_terminados_compuestos.length; t++) {
            	var enviado_todo = '';
				if (aux_cantidad_compuestos[t] == aux_terminados_compuestos[t] || ta_estado == 2) {
					enviado_todo = 'readonly';
				}
                table = table + '<div class="input-group"><input type="text" name="ta_trabajos_compuestos_terminados" onKeypress="return soloNumeros(event)" class=" text-center form-control" value="0" style="min-width: 47px !important;" '+enviado_todo+'><span class="input-group-addon" title="'+aux_terminados_compuestos[t]+' <?php echo lang("cantidad.enviada") ?>"><i class="fa fa-truck"></i> <span>'+aux_terminados_compuestos[t]+'</span></span></div>';
            }
            table = table + '</div></td>';
            
            table = table + '<td><div class="atributos_observaciones">';
            for (var t=0; t<aux_observaciones_compuestos.length; t++) {
                table = table + '<input type="text" value="'+aux_observaciones_compuestos[t]+'" class="text-center form-control" name="ta_trabajos_compuestos_observaciones" '+read+'/>';
            }
            table = table + '</div></td>';
            
            table = table + '<td><div class="atributos_reposicion">';
            for (var t=0; t<aux_reposiciones_compuestos.length; t++) {
                if (aux_reposiciones_compuestos[t] == 'true') {
					table = table + '<input type="checkbox" class="form-control" name="ta_trabajos_compuestos_reposiciones" '+read+' checked />';
				} else {
					table = table + '<input type="checkbox" class="form-control" name="ta_trabajos_compuestos_reposiciones" '+read+'/>';
				}
            }
            table = table + '</div></td>';
            
            table = table + '</tr>';
			//alert(table);
		}
		
		$('#trabajos-taller tbody').prepend(table);
		
            //OCULTAMOS LOS ITEMS
            $('#trabajos-taller tbody .atributos').hide();
            $('#trabajos-taller tbody .atributos_cantidad').hide();
            $('#trabajos-taller tbody .atributos_enviar').hide();
            $('#trabajos-taller tbody .atributos_descripcion').hide();
            $('#trabajos-taller tbody .atributos_ancho').hide();
            $('#trabajos-taller tbody .atributos_alto').hide();
            $('#trabajos-taller tbody .atributos_longitud').hide();
            $('#trabajos-taller tbody .atributos_observaciones').hide();
            $('#trabajos-taller tbody .atributos_reposicion').hide();
            
            //AÑADIR ITEM TRABAJO COMPUESTO
            $('#trabajos-taller tbody .anadir').unbind('click').click(function(){
                anadir_items($(this).parent().parent().parent());
                mostrar_items(true, $(this).parent().parent().parent());
            });
            
            $('#trabajos-taller tbody .desplegar').unbind('click').click(function(){
                mostrar_items(null, $(this).parent().parent().parent());
            });
                
		//EVITAMOS DEJAR EL CAMPO ENVIAR A CERO
		$('#trabajos-taller tbody input[name="ta_trabajos_terminados"]').focusout(function(){
			if ($(this).val() == '') {
				$(this).val(0);
			}
		});
		
		if (enviados_trabajos) {
			$('#terminar_trabajo').show();
		}
	}
	
	//AÑADIR TRABAJO
	$('#anadir_trabajo').click(function(e){
        e.preventDefault();
		
		var borrar = '';
		
		if (ta_estado <= 1 || disable != '') {
			borrar = '<td class="text-center vcenter pointer"><i class="fa fa-times eliminar-simples"></i></td>';
		}
				
		var fila = '<tr tipo="simple" class="taller_simple">'+borrar+'<td><input type="text" value="" class="form-control" name="ta_trabajos_codigo" /></td><td><input type="text" value="" class="form-control" name="ta_trabajos_descripcion" /></td><td><input type="text" value="" class="form-control text-center" name="ta_trabajos_ancho" /></td><td><input type="text" value="" class="form-control text-center" name="ta_trabajos_alto" /></td><td><input type="text" value="" class="form-control text-center" name="ta_trabajos_longitud" /></td><td><input type="text" value="" onKeypress="return soloNumeros(event)" class="text-center form-control text-center" name="ta_trabajos_cantidad" /></td><td><div class="input-group"><input type="text" name="ta_trabajos_terminados" style="min-width: 47px !important;" onKeypress="return soloNumeros(event)" class=" text-center form-control" value="0"><span class="input-group-addon" title="0 <?php echo lang("cantidad.enviada") ?>"><i class="fa fa-truck"></i> <span>0</span></span></div></td><td><input type="text" value="" class="form-control" name="ta_trabajos_observaciones" /></td><td><input type="checkbox" class="form-control" name="ta_trabajos_reposicion" /></td></tr>';
		
		$('#trabajos-taller tbody').append(fila);
	});
        
        //AÑADIR TRABAJO COMPUESTO
	$('#anadir_trabajo_compuesto').click(function(e){
		e.preventDefault();
		
		var borrar = '';
		if (ta_estado < 1 || disable == '') {
			borrar = '<td class="text-center vcenter"><i class="fa fa-times pointer eliminar-compuesto"></i></td>';
		}

		if(ta_estado < 1 || disable == ''){
        	var fila = '<tr tipo="compuesto" class="taller_compuesto">'+borrar+'<td><div class="input-group"><input type="text" name="ta_trabajos_compuestos_codigo" class="form-control"><span class="input-group-addon anadir"><i class="fa fa-plus"></i></span><span class="input-group-addon desplegar"><i class="fa fa-arrow-down"></i></span></div><div class="atributos"></div></td><td><div class="atributos_descripcion"></div></td><td><div class="atributos_ancho"></div></td><td><div class="atributos_alto"></div></td><td><div class="atributos_longitud"></div></td><td><div class="atributos_cantidad"></div></td><td><div class="atributos_enviar"></div></td><td><div class="atributos_observaciones"></div></td><td><div class="atributos_reposicion"></div></td></tr>';
		}
		
		$('#trabajos-taller tbody').append(fila);
                
                //AÑADIR ITEM TRABAJO COMPUESTO
                $('#trabajos-taller tbody .anadir').unbind('click').click(function(){
                    anadir_items($(this).parent().parent().parent());
                    mostrar_items(true, $(this).parent().parent().parent());
                });
                
                $('#trabajos-taller tbody .desplegar').unbind('click').click(function(){
                    mostrar_items(null, $(this).parent().parent().parent());
                });
	});        
        
	
	//GUARDAR TRABAJO DEL TALLER
	$('#guardar_trabajo,#procesar_trabajo').click(function(e){
		e.preventDefault();
		donde = $(this).attr('id');
		validar(false);
	});

	function validar(autorizado){



		var aplica_validacion = false;
		var product_id = '';
		var product_cantidad = '';
		var cotizacion = $('#cotizaciones option:selected').val();
		productsList = {};

		$('table#example2 tbody tr').each(function(){
            product_id = product_id + $(this).attr('product_id') + ';';
            item_product_id = $(this).attr('product_id');

            if (ta_estado <= 2) {
                product_cantidad = product_cantidad + $.trim($(this).find('td:last').html()) + ';';
                item_product_cantidad = parseFloat($.trim($(this).find('td:last').html()));
            } else {
                product_cantidad = product_cantidad + $.trim($(this).find('td:last').prev().prev().html()) + ';';
                item_product_cantidad = parseFloat($.trim($(this).find('td:last').prev().prev().html()));
            } 

            if(item_product_id != ''){  
            	if(productsList[item_product_id] && productsList[item_product_id].cantidad > 0){
            		productsList[item_product_id].cantidad += item_product_cantidad; 
            	} else{
            		productsList[item_product_id] = {};             		
            		productsList[item_product_id].cantidad = item_product_cantidad;             		
            	}                  	 
            	productsList[item_product_id].product_id = item_product_id;             		
            }                                   
        });

		//llamar la validacion
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/taller/get_validacion_talleres/" + cotizacion,
			data: {
				"ta_id":ta_id,
				"product_list":productsList,
			},
			success: function(data) {				
				if(data.aplica_validacion && ta_estado <= 1){
					if(!autorizado){
						autorizacion(false , data.cat_por_aprobar);
					}else{
						guardar(false);
					}
				}else{
					guardar(false, false);
				}
			}
		});		
	}

	function guardar(con_autorizacion,id_empleado,data_categorias){
		$('#tab_1 *').removeClass('has-error has-feedback');
		$('#tab_1 .text-danger').html('');
		
		var enviar = true;
		
		if ($('#fecha_trabajo').val() != '') {
			var horario = $.trim($('#fecha_trabajo').val()).split(' - ');
			horario = horario.filter(Boolean);
		} else {
			$('#fecha_trabajo').parent().addClass('has-error has-feedback');
			enviar = false;
		}
		
		if (enviar) {
            var ta_trabajos = '';
            var ta_trabajos_descripcion = '';
            var ta_trabajos_ancho = '';
            var ta_trabajos_alto = '';
            var ta_trabajos_longitud = '';
            var ta_trabajos_observaciones = '';
            var ta_trabajos_reposiciones = '';
			var ta_trabajos_cantidad = '';
			var ta_trabajos_terminados = '';
			var ta_trabajo_enviado = '';
                        
            var ta_trabajos_compuestos = '';
            var ta_trabajos_compuestos_descripcion = '';
            var ta_trabajos_compuestos_ancho = '';
            var ta_trabajos_compuestos_alto = '';
            var ta_trabajos_compuestos_longitud = '';
            var ta_trabajos_compuestos_observaciones = '';
            var ta_trabajos_compuestos_reposiciones = '';
			var ta_trabajos_compuestos_cantidad = '';
			var ta_trabajos_compuestos_terminados = '';
            var ta_trabajos_compuestos_enviado = '';
			
			var total_trabajos = 0;
			var trabajos_grabados = 0;
			var todos_enviados = true;
                       
			$('#trabajos-taller tbody tr').each(function(){
				total_trabajos++;
				var fila = $(this);
                var cantidad_total = '';
                var cantidad_enviar = '';
                var aux_cantidad_enviar = '';
                var cantidad_enviada = '';
                var aux_cantidad_enviada = '';
                var trabajos = '';
                var descripcion = '';
                var observaciones = '';
                var reposiciones = '';
                var ancho = '';
                var alto = '';
                var longitud = '';
                var emp_tipo = <?php echo $this->session->userdata('emp_tipo');?> 
                
                if ($(fila).attr('tipo') == 'simple') {
                    cantidad_total = parseInt($.trim($(this).find('td:last').prev().prev().prev().find('input').val()));
                    cantidad_enviar = parseInt($.trim($(this).find('td:last').prev().prev().find('input').val()));
                    cantidad_enviada = parseInt($.trim($(this).find('td:last').prev().prev().find('span span').html()));
                    
                    if ((ta_estado <= 1 && emp_tipo <= 1) || (ta_estado < 1 && emp_tipo == 2)) {
						trabajos = $.trim($(fila).find('td:first').next().find('input').val());
                        descripcion = $.trim($(fila).find('td:first').next().next().find('input').val());
                        ancho = $.trim($(fila).find('td:first').next().next().next().find('input').val());
                        alto = $.trim($(fila).find('td:first').next().next().next().next().find('input').val());
                        longitud = $.trim($(fila).find('td:first').next().next().next().next().next().find('input').val());
					} else {
						trabajos = $.trim($(fila).find('td:first input').val());
                        descripcion = $.trim($(fila).find('td:first').next().find('input').val());
                        ancho = $.trim($(fila).find('td:first').next().next().find('input').val());
                        alto = $.trim($(fila).find('td:first').next().next().next().find('input').val());
                        longitud = $.trim($(fila).find('td:first').next().next().next().next().find('input').val());
					}

                    observaciones = $.trim($(fila).find('td:last').prev().find('input').val());
                    if($(fila).find('td:last input').is(':checked')) {
                    	reposiciones = true;
                    } else {
						reposiciones = false;
					}
                    
                    if (ancho == '') {
                        ancho = ' ';
                    }
                    
                    if (alto == '') {
                        alto = ' ';
                    }
                    
                    if (longitud == '') {
                        longitud = ' ';
                    }
                    
                    if (observaciones == '') {
                        observaciones = ' ';
                    }
                    
                    if (trabajos == '') {
                        if (ta_estado <= 1) {
							$(fila).find('td:first').next().addClass('has-error has-feedback');	
						} else {
							$(fila).find('td:first').addClass('has-error has-feedback');
						}
                        
                        enviar = false;
                    } else {
                        if (descripcion == '') {
                            if (ta_estado <= 1) {
								$(fila).find('td:first').next().addClass('has-error has-feedback');
                            	enviar = false;
							} /*else {
								$(fila).find('td:first').next().addClass('has-error has-feedback');
							}*/
                            
                        } else {
                            if (cantidad_total == '' || cantidad_total == null || isNaN(cantidad_total)) {
                                $(fila).find('td:last').prev().prev().prev().addClass('has-error has-feedback');
                                enviar = false;
                            } else {
                                
                                /*if (observaciones == '') {
                                    observaciones = ' ';
                                }*/
                                
                                if ((cantidad_enviar+cantidad_enviada) <= cantidad_total) {
                                    trabajos_grabados++;
                                    ta_trabajos = ta_trabajos + trabajos + ';';
                                    ta_trabajos_descripcion = ta_trabajos_descripcion + descripcion + ';';
                                    ta_trabajos_ancho = ta_trabajos_ancho + ancho + ';';
                                    ta_trabajos_alto = ta_trabajos_alto + alto + ';';
                                    ta_trabajos_longitud = ta_trabajos_longitud + longitud + ';';
                                    ta_trabajos_observaciones = ta_trabajos_observaciones + observaciones + ';';
                                    ta_trabajos_reposiciones = ta_trabajos_reposiciones + reposiciones + ';';
                                    ta_trabajos_cantidad = ta_trabajos_cantidad + cantidad_total + ';';
                                    ta_trabajos_terminados = ta_trabajos_terminados + (cantidad_enviada+cantidad_enviar) + ';';
                                    ta_trabajo_enviado = ta_trabajo_enviado + cantidad_enviar + ';';

                                    if ((parseInt(cantidad_enviar) + parseInt(cantidad_enviada)) != parseInt(cantidad_total)) {
                                        todos_enviados = false;
                                    }
                                } else {
                                    $(fila).find('td:last').addClass('has-error has-feedback');
                                    $(fila).find('td:last').prev().prev().addClass('has-error has-feedback');
                                    enviar = false;
                                }       		
                            }
                        }
                    }
                } else {
                    $(fila).find('td .atributos input').each(function(indice){
                        cantidad_total = '';
                        cantidad_enviar = '';
                        aux_cantidad_enviar = '';
                        cantidad_enviada = '';
                        aux_cantidad_enviada = '';
                        trabajos = '';
                        descripcion = '';
                        observaciones = '';
                        reposiciones = '';
                        ancho = '';
                        alto = '';
                        longitud = '';
                        
                        if (ta_estado <= 1) {
							var titulo_compuesto = $.trim($(fila).find('td:first').next().find('input').val());
						} else {
							var titulo_compuesto = $.trim($(fila).find('td:first input').val());
						}
                        
                        if (titulo_compuesto != '') {
                            if (indice == 0) {
                                if (ta_estado <= 1) {
                                	trabajos = trabajos + $.trim($(fila).find('td:first').next().find('input').val()) + '|';
                                } else {
									trabajos = trabajos + $.trim($(fila).find('td:first input').val()) + '|';
								} 
                            } else {
                                total_trabajos++;
                            }

                            var aux = $.trim($(this).val());
                            if (aux == '') {
                                if (ta_estado <= 1) {
                                	$(fila).find('td:first').next().addClass('has-error has-feedback');
                                } else {
									$(fila).find('td:first').addClass('has-error has-feedback');
								}
                                enviar = false;
                            } else {
                               trabajos = trabajos + aux + '|'; 
                               
                               var aux2 = $.trim($(fila).find('.atributos_descripcion input:eq('+indice+')').val());
                               if (aux2 == '' || aux2 == null) {
                               		if (ta_estado <= 1) {
                               			$(fila).find('td:first').addClass('has-error has-feedback');
                               		} else {
										$(fila).find('td:first').prev().addClass('has-error has-feedback');
									}
                                   
                                   enviar = false;
                               } else {
                                    descripcion = descripcion + aux2 + '|';
                                   
                                    var aux3 = $.trim($(fila).find('.atributos_ancho input:eq('+indice+')').val());
                                    if (aux3 == '') {
                                        aux3 = ' ';
                                    }
                                    ancho = ancho + aux3 + '|';
                                        
                                        var aux4 = $.trim($(fila).find('.atributos_alto input:eq('+indice+')').val());
                                        if (aux4 == '') {
                                            aux4 = ' ';
                                        }
                                        alto = alto + aux4 + '|';
                                        
                                        var aux7 = $.trim($(fila).find('.atributos_longitud input:eq('+indice+')').val());
                                        if (aux7 == '') {
                                            aux7 = ' ';
                                        }
                                        longitud = longitud + aux7 + '|';
                                            
                                            var aux5 = $.trim($(fila).find('.atributos_observaciones input:eq('+indice+')').val());
                                                if (aux5 == '') {
                                                    aux5 = ' ';
                                                }
                                                observaciones = observaciones + aux5 + '|';
                                                
                                                var aux8 = $(fila).find('.atributos_reposicion input:eq('+indice+')');
                                                if(aux8.is(':checked')) {
                                                	reposiciones = reposiciones + 'true|';
                                                 } else {
												 	reposiciones = reposiciones + 'false|';
												 }
                                                
                                                var aux6 = $.trim($(fila).find('.atributos_cantidad input:eq('+indice+')').val());
                                                if (aux6 == '' || aux6 == null) {
                                                    $(fila).find('td:last').prev().prev().addClass('has-error has-feedback');
                                                    enviar = false;
                                                } else {
                                                    cantidad_total = cantidad_total + aux6 + '|';

                                                    var cantidad_enviar = $.trim($(fila).find('.atributos_enviar input:eq('+indice+')').val());                                               
                                                    aux_cantidad_enviar = aux_cantidad_enviar + cantidad_enviar + '|';

                                                    var cantidad_enviada = $.trim($(fila).find('.atributos_enviar input:eq('+indice+')').next().find('span').html());
                                                    aux_cantidad_enviada = aux_cantidad_enviada + (parseInt(cantidad_enviada) + parseInt(cantidad_enviar)) + '|';

                                                    if ((parseInt(cantidad_enviar) + parseInt(cantidad_enviada)) <= parseInt(cantidad_total)) {
                                                         trabajos_grabados++;

                                                         ta_trabajos_compuestos = ta_trabajos_compuestos + trabajos;
                                                         ta_trabajos_compuestos_descripcion = ta_trabajos_compuestos_descripcion + descripcion;
                                                         ta_trabajos_compuestos_ancho = ta_trabajos_compuestos_ancho + ancho;
                                                         ta_trabajos_compuestos_alto = ta_trabajos_compuestos_alto + alto;
                                                         ta_trabajos_compuestos_longitud = ta_trabajos_compuestos_longitud + longitud;
                                                         ta_trabajos_compuestos_observaciones = ta_trabajos_compuestos_observaciones + observaciones;
                                                         ta_trabajos_compuestos_reposiciones = ta_trabajos_compuestos_reposiciones + reposiciones;
                                                         ta_trabajos_compuestos_cantidad = ta_trabajos_compuestos_cantidad + cantidad_total;
                                                         ta_trabajos_compuestos_terminados = ta_trabajos_compuestos_terminados + aux_cantidad_enviada;
                                                         ta_trabajos_compuestos_enviado = ta_trabajos_compuestos_enviado + aux_cantidad_enviar;

															if ((parseInt(cantidad_enviar) + parseInt(cantidad_enviada)) != aux6) {
                                                             todos_enviados = false;
                                                         }
                                                    } else {
                                                       $(fila).find('td:last').prev().addClass('has-error has-feedback');
                                                       $(fila).find('td:last').prev().prev().addClass('has-error has-feedback');
                                                       enviar = false;
                                                    }
                                                }
                                           
                                        
                                  
                               }
                            }
                        } else {
                        	if (ta_estado <= 1) {
								$(fila).find('td:first').next().addClass('has-error has-feedback');
							} else {
								$(fila).find('td:first').addClass('has-error has-feedback');
							}
                           
                           enviar = false;
                        }
                    });
                    
                    ta_trabajos_compuestos = ta_trabajos_compuestos.substring(0, ta_trabajos_compuestos.length-1);
                    ta_trabajos_compuestos_descripcion = ta_trabajos_compuestos_descripcion.substring(0, ta_trabajos_compuestos_descripcion.length-1);
                    ta_trabajos_compuestos_ancho = ta_trabajos_compuestos_ancho.substring(0, ta_trabajos_compuestos_ancho.length-1);
                    ta_trabajos_compuestos_alto = ta_trabajos_compuestos_alto.substring(0, ta_trabajos_compuestos_alto.length-1);
                    ta_trabajos_compuestos_longitud = ta_trabajos_compuestos_longitud.substring(0, ta_trabajos_compuestos_longitud.length-1);
                    ta_trabajos_compuestos_observaciones = ta_trabajos_compuestos_observaciones.substring(0, ta_trabajos_compuestos_observaciones.length-1);
                    ta_trabajos_compuestos_reposiciones = ta_trabajos_compuestos_reposiciones.substring(0, ta_trabajos_compuestos_reposiciones.length-1);
                    ta_trabajos_compuestos_cantidad = ta_trabajos_compuestos_cantidad.substring(0, ta_trabajos_compuestos_cantidad.length-1);
                    ta_trabajos_compuestos_terminados = ta_trabajos_compuestos_terminados.substring(0, ta_trabajos_compuestos_terminados.length-1);
                    ta_trabajos_compuestos_enviado = ta_trabajos_compuestos_enviado.substring(0, ta_trabajos_compuestos_enviado.length-1);
                    
                    ta_trabajos_compuestos = ta_trabajos_compuestos + ';';
                    ta_trabajos_compuestos_descripcion = ta_trabajos_compuestos_descripcion + ';';
                    ta_trabajos_compuestos_ancho = ta_trabajos_compuestos_ancho + ';';
                    ta_trabajos_compuestos_alto = ta_trabajos_compuestos_alto + ';';
                    ta_trabajos_compuestos_longitud = ta_trabajos_compuestos_longitud + ';';
                    ta_trabajos_compuestos_observaciones = ta_trabajos_compuestos_observaciones + ';';
                    ta_trabajos_compuestos_reposiciones = ta_trabajos_compuestos_reposiciones + ';';
                    ta_trabajos_compuestos_cantidad = ta_trabajos_compuestos_cantidad + ';';
                    ta_trabajos_compuestos_terminados = ta_trabajos_compuestos_terminados + ';';
                    ta_trabajos_compuestos_enviado = ta_trabajos_compuestos_enviado + ';';
                }   
			});

			<?php if(isset($_GET['tag'])){ ?>
            	var nuevo_estado = true;
            <?php } else{?>
            	var nuevo_estado = false;
            <?php } ?>
            
            //TODOS LOS TRABAJOS ENVIADOS
			if (ta_id == '') {
				ta_estado = 0;
			} else if (ta_id != '' && todos_enviados) {
				ta_estado = 2;
			} else {
				ta_estado = 1;
			}

			if(nuevo_estado){
				if(pr_ta_estado == '0' && donde == 'guardar_trabajo'){
					ta_estado = 0;
				}else if(pr_ta_estado == '1' && donde == 'guardar_trabajo' && ta_estado == '1'){
					ta_estado = 1;
				}else if(pr_ta_estado == '1' && donde == 'guardar_trabajo' && ta_estado == '2'){
					ta_estado = 2;
				}
			}

			if(con_autorizacion == true && !id_empleado){
				ta_estado = 0;
			}

			if(donde == 'guardar_trabajo' && (ta_estado == '' || ta_estado == 0) ){
				ta_estado = 0;
			}			
		
            if (enviar) {
                var product_id = '';
                var product_cantidad = '';
                var al_id = '';
                var nuevo = '';
                $('table#example2 tbody tr').each(function(){
                        product_id = product_id + $(this).attr('product_id') + ';';
                        if (ta_estado <= 2) {
                            product_cantidad = product_cantidad + $.trim($(this).find('td:last').html()) + ';';
                        } else {
                            product_cantidad = product_cantidad + $.trim($(this).find('td:last').prev().prev().html()) + ';';
                        }
                        
                        
                        al_id = al_id + $(this).attr('al_id') + ';';
                        nuevo = nuevo + $(this).attr('nuevo') + ';';
                });

                if (product_id == '') {
                    $('#example2 .text-danger').html('<?php echo lang("productos.taller.requerido") ?>');
                    enviar = false;
                }
            }           

            if (enviar) {
                var l = Ladda.create( document.querySelector( "#guardar_trabajo" ) );
                l.start();

                var datos = {
                        'ta_id'	:	ta_id,
                        'ta_inicio'	: horario[0],
                        'ta_final'	: horario[1],
                        'ta_estado'	:	ta_estado,
                        'pr_id'	:	$('#cotizaciones option:selected').val(),
                        'ta_observaciones'	:	$.trim($('#observaciones_taller').val()),
                        'man_id'	:	$('#cotizaciones option:selected').attr('man_id'),
                        'product_id'	:	product_id,
                        'product_cantidad'	:	product_cantidad,
                        'ta_trabajos_codigo'	:	ta_trabajos,
                        'ta_trabajos_descripcion'	:	ta_trabajos_descripcion,
                        'ta_trabajos_ancho'	:	ta_trabajos_ancho,
                        'ta_trabajos_alto'	:	ta_trabajos_alto,
                        'ta_trabajos_longitud'	:	ta_trabajos_longitud,
                        'ta_trabajos_observaciones'	:	ta_trabajos_observaciones,
                        'ta_trabajos_reposiciones'	:	ta_trabajos_reposiciones,
                        'ta_trabajos_cantidad'	:	ta_trabajos_cantidad,
                        'ta_trabajos_terminados'	:	ta_trabajos_terminados,
                        'ta_trabajos_compuestos_codigo'	:	ta_trabajos_compuestos,
                        'ta_trabajos_compuestos_descripcion'	:	ta_trabajos_compuestos_descripcion,
                        'ta_trabajos_compuestos_ancho'	:	ta_trabajos_compuestos_ancho,
                        'ta_trabajos_compuestos_alto'	:	ta_trabajos_compuestos_alto,
                        'ta_trabajos_compuestos_longitud'	:	ta_trabajos_compuestos_longitud,
                        'ta_trabajos_compuestos_observaciones'	:	ta_trabajos_compuestos_observaciones,
                        'ta_trabajos_compuestos_reposiciones'	:	ta_trabajos_compuestos_reposiciones,
                        'ta_trabajos_compuestos_cantidad'	:	ta_trabajos_compuestos_cantidad,
                        'ta_trabajos_compuestos_terminados'	:	ta_trabajos_compuestos_terminados,
                        'ta_trabajo_enviado'	:	ta_trabajo_enviado,
                        'ta_trabajos_compuestos_enviado'	:	ta_trabajos_compuestos_enviado,
                        'nuevo'	:	nuevo,
                        'al_id'	:	al_id,
                        'pr_eliminados': pr_eliminados,
                        'id_empleado' : id_empleado,
                        'data_categorias' : data_categorias,
                };
                
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?php echo base_url().$this->lang->lang() ?>/taller/guardar_editar",
                    data:datos,
                    success: function(data) {

                            if (data.sql) {
                                    //MENSAJES ALERTA PARA EDITAR O CREAR
                                    if (ta_id != '') {
                                            if (!data.editar) {
                                                    bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
                                                    //REDIRIGIMOS A LOS 5 SEGUNDOS
                                                    setTimeout("location.href='"+location+"'", 5000);
                                            }
                                    } else {
                                            if (!data.crear) {
                                                    bootbox.alert('<?php echo lang("permiso.crear.ko") ?>');
                                                    //REDIRIGIMOS A LOS 5 SEGUNDOS
                                                    setTimeout("location.href='"+location+"'", 5000);
                                            }
                                    }

                                    //PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
                                    if (data.status) {
                                            bootbox.alert('<?php echo lang("trabajo.taller.guardado.ok") ?>');

                                            if (ta_id != '') {
                                                    location.reload(true);
                                            } else {
                                                    var url = "<?php echo base_url().$this->lang->lang() ?>/taller";
                                                    $(location).attr('href',url);
                                            }
                                    } else {
                                            if (data.stock) {
                                                    $('#tab_1 #example2 tbody tr').each(function(){
                                                            var tr_id = $(this).attr('product_id');
                                                            var fila_tr = $(this);

                                                            $(data.stock).each(function(indice){
                                                                    if (tr_id == data.stock[indice]['product_id'] && $(fila_tr).attr('nuevo') == 'true') {
                                                                            $(fila_tr).css('color','red');
                                                                            $(fila_tr).find('td:last').append('('+data.stock[indice]['stock_disponible']+')');
                                                                    }
                                                            });
                                                    });

                                                    bootbox.alert('<?php echo lang("stock.almacen.ko") ?>');
                                            } else {
                                                    bootbox.alert('<?php echo lang("trabajo.taller.guardado.ko") ?>');
                                            }

                                            l.stop();
                                    }
                            } else {
                                    bootbox.alert('<?php echo lang("error.ajax") ?>');
                                    l.stop();
                            }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                    bootbox.alert('<?php echo lang("error.ajax") ?>');
                    l.stop();
                    }
                });
            }
        }
	}
	
	function autorizacion( errors , data_categorias){
		var name = null;
		var valor = null;

		if(errors){
			name = errors[0][0];
			valor = errors[0][1];	
		}

		var formulario = "<form id='form_autorizacion'>";
			
		if(name && (name == 'emp_dni'|| name == 'emp_tipo')){
			formulario += "<div class='form-group has-error has-feedback'>";
		}else{
			formulario += "<div class='form-group'>";
		}

		formulario += "<label><?php echo lang('dni') ?></label>"+
					"<input type='text' name='emp_dni' id='emp_dni' value='' class='form-control'>";

		if(name && name == 'emp_dni'){
			formulario += "<span class='glyphicon glyphicon-remove form-control-feedback'></span>"+
			"<div class='text-danger'>"+ valor +"</div></div>";
		}else{
			formulario += "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>"+
			"<div class='text-danger'></div></div>";
		}
		
		if(name && (name == 'emp_clave' || name == 'emp_tipo')){
			formulario += "<div class='form-group has-error has-feedback'>";
		}else{
			formulario += "<div class='form-group'>";
		}			
					
		formulario += "<label><?php echo lang('login.clave') ?></label>"+
					"<input type='password' name='emp_clave' id='emp_clave' value='' class='form-control'>";
					
		if(name && name == 'emp_clave'){
			formulario += "<span class='glyphicon glyphicon-remove form-control-feedback'></span>"+
			"<div class='text-danger'>"+ valor +"</div>";
		}else{
			formulario += "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>"+
			"<div class='text-danger'></div>";
		}
		formulario += "</div>";

		if(name && name == 'emp_tipo'){
			formulario += "<div class='text-danger'>"+ valor +"</div>";
		}

		formulario += "</form>";

		formulario += "<div style='width:90%;margin-top: 25px;'>";
		var footer = false;
		Object.keys(data_categorias).forEach(function(k, i) {
			if(i==0){
				formulario += '<table>';
				formulario += '<tr>';
				formulario += '<th style="width:9%;text-align: center; padding: 2px; background: #faffbd;border: 1px solid;border-collapse: collapse;">Aprobar</th>';	
				formulario += '<th style="width:30%;text-align: center; padding: 2px; background: #faffbd;border: 1px solid;border-collapse: collapse;">Categoría</th>';	
				formulario += '<th style="width:20.33%;text-align: center; padding: 2px; background: #faffbd;border: 1px solid;border-collapse: collapse;">Costo Disponible</th>';	
				formulario += '<th style="width:20.33%;text-align: center; padding: 2px; background: #faffbd;border: 1px solid;border-collapse: collapse;">Compra Stock Inv.</th>';	
				formulario += '<th style="width:20.33%;text-align: center; padding: 2px; background: #faffbd;border: 1px solid;border-collapse: collapse;">Sobrepasa Costo</th>';
				formulario += '</tr>';		
				footer = true;			
			}
		    console.log('data_categorias[k]',data_categorias[k]);
		    formulario += '<tr>';
		    formulario += '<td style="border-collapse:collapse;border:1px solid;padding:2px;text-align:center;"><input type="checkbox" class="icheckbox_minimal" name="check-'+data_categorias[k].categoria+'" id="check-'+data_categorias[k].categoria+'"></td>';
			formulario += '<td style="border-collapse:collapse;border:1px solid;padding:2px;text-align:center;">'+data_categorias[k].nombre_categoria+'</td>';
			formulario += '<td style="border-collapse:collapse;border:1px solid;padding:2px;text-align:right;"> B/.'+data_categorias[k].disponible+'</td>';
			formulario += '<td style="border-collapse:collapse;border:1px solid;padding:2px;text-align:right;"> B/.'+(data_categorias[k].nuevo_usado) +'</td>';
			formulario += '<td style="border-collapse:collapse;border:1px solid black;padding:2px;text-align:right;color:red;"> B/.'+ Math.abs(data_categorias[k].disponible - data_categorias[k].nuevo_usado) +'</td>';
			formulario += '</tr>';

		});
		if(footer){			
			formulario += "</table>";
		}

		formulario += "</div>";
		

		bootbox.dialog({
			closeButton: false,
			message: "<div id='autorizacion'><h3>Autorización</h3>"+formulario+"</div>",
			buttons: {
				ok: {
					label: "<?php echo lang('guardar-sin-autorizacion') ?>",
					className: "btn-primary",
					callback: function() {
					    guardar(true,false,null);
					}
				},
				success: {
					label: "<?php echo lang('guardar') ?>",
					className: "btn-success",
					callback: function() {

					    Object.keys(data_categorias).forEach(function(k) {
					    	data_categorias[k].check_value = $('#check-'+data_categorias[k].categoria).is(':checked');
					    });

					    console.log('data_categorias',data_categorias);
					    var datos_emp = {
							'emp_dni' : $("#form_autorizacion #emp_dni").val(),
							'emp_clave' : Aes.Ctr.encrypt($("#form_autorizacion #emp_clave").val(), '123456', 256),
							'token' : '123456',
						};
					    
					    //INICIO PETICION AJAX
					    $.ajax({
							type: "POST",
							dataType: "json",
							url: "<?php echo base_url().$this->lang->lang() ?>/empleados/validar_solicitud",
							data: datos_emp,
							success: function(data) {
								console.log(data);
								if (data.status == false) {
									autorizacion(data.errors,data_categorias);
								} else {
									// guardar con autorizacion
									guardar(true,data.emp_id,data_categorias);
								}
							}
						});
						
					}
				},
				cancel: {
					label: "<?php echo lang('cancelar') ?>",
					className: "btn-default",
					callback: function(){
						
					}
				}
			}
		});
	}

	//TERMINAR Y CERRAR TRABAJO DEL TALLER
	$('#terminar_trabajo').click(function(e){
		e.preventDefault();
		
		$('#tab_1 *').removeClass('has-error has-feedback');
		
		if (enviados_trabajos) {
			var product_id = '';
			var product_cantidad = '';
			var pra_reciclaje = '';
			var id_almacen = '';
			var enviar = true;
			
			//PRODUCTOS SOBRANTES (RECICLAJE)
			$('table#example2 tbody tr').each(function(){
				
				var cantidad = [];
				var reciclaje = [];
				
				$(this).find('.reciclar').each(function(){
					var cant = $.trim($(this).find('input.cantidad_reciclaje').val());
					var rec = $.trim($(this).find('input.pra_reciclaje').val());
					
					/*if (cant != '' && rec == '' || cant == '' && rec != '') {
						$(this).parent().addClass('has-error has-feedback');
						$(this).parent().prev().addClass('has-error has-feedback');
					} else {
						if (cant != '' && rec != '') {
							product_id = product_id + $(this).parent().parent().attr('product_id') + ';';
							product_cantidad = product_cantidad + cant + ';';
							pra_reciclaje = pra_reciclaje + rec + ';';
						}
						cantidad.push(cant);
						reciclaje.push(rec);
					}*/
					
					cant = (cant == '') ? 0 : cant;
					
					if (cant != '' && rec != '') {
						product_id = product_id + $(this).parent().parent().attr('product_id') + ';';
						product_cantidad = product_cantidad + cant + ';';
						pra_reciclaje = pra_reciclaje + rec + ';';
					}
					cantidad.push(cant);
					reciclaje.push(rec);
	
				});
				
				
			});
			
			var datos = [];
			
			if (enviar) {
				var l = Ladda.create( document.querySelector( "#terminar_trabajo" ) );
				l.start();
				
				
				if (pra_reciclaje != '') {
					bootbox.dialog({
						message: "<label><?php echo lang('almacenes') ?></label><br><select id='reciclaje' class='form-control'>"+almacenes_reciclaje+"</select>",
						buttons: {
							success: {
								label: "<?php echo lang('seleccionar.almacen') ?>",
								className: "btn-success",
								callback: function() {
									
									datos = {
										'ta_id'	:	ta_id,
										'ta_estado'	:	'3',
										'product_cantidad'	:	product_cantidad,
										'pra_reciclaje'	:	pra_reciclaje,
										'product_id'	:	product_id,
										'al_id'	:	$('#reciclaje option:selected').val()
									};
									
									
									
									$.ajax({
										type: "POST",
										dataType: "json",
										url: "<?php echo base_url().$this->lang->lang() ?>/taller/cerrar_taller",
										data:datos,
										success: function(data) {
											
											if (data.sql) {
												//MENSAJES ALERTA PARA EDITAR O CREAR
											  	if (ta_id != '') {
													if (!data.editar) {
														bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
														//REDIRIGIMOS A LOS 5 SEGUNDOS
														setTimeout("location.href='"+location+"'", 5000);
													}
												}
												
												//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
										  		if (data.status) {
										  			bootbox.alert('<?php echo lang("trabajo.taller.cerrado.ok") ?>');
										  			setTimeout("location.href='"+location+"'", 1500);
										  		} else {
										  			//ERROR
													l.stop();
												}
											} else {
												bootbox.alert('<?php echo lang("error.ajax") ?>');
												l.stop();
											}
										},
										error: function(XMLHttpRequest, textStatus, errorThrown) {
										bootbox.alert('<?php echo lang("error.ajax") ?>');
										l.stop();
										}
									});
								}
							}
						}
					});
				} else {
					datos = {
						'ta_id'	:	ta_id,
						'ta_estado'	:	'3'
					};
					
					$.ajax({
						type: "POST",
						dataType: "json",
						url: "<?php echo base_url().$this->lang->lang() ?>/taller/cerrar_taller",
						data:datos,
						success: function(data) {
										
							if (data.sql) {
								//MENSAJES ALERTA PARA EDITAR O CREAR
							  	if (ta_id != '') {
									if (!data.editar) {
										bootbox.alert('<?php echo lang("permiso.editar.ko") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										setTimeout("location.href='"+location+"'", 5000);
									}
								}
											
								//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
						  		if (data.status) {
						  			bootbox.alert('<?php echo lang("trabajo.taller.cerrado.ok") ?>');
						  			setTimeout("location.href='"+location+"'", 1500);
						  		} else {
						  			//ERROR
									l.stop();
								}
							} else {
								bootbox.alert('<?php echo lang("error.ajax") ?>');
								l.stop();
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
						bootbox.alert('<?php echo lang("error.ajax") ?>');
						l.stop();
						}
					});
				}
			}
		} else {
			bootbox.alert('<?php echo lang("taller.enviar.trabajos") ?>');
		}	
	});
	
	//PESTAÑA 2
	//ALMACENES Y PRODUCTOS
	var almacenes_privera_vez = false;
	
	function buscar_productos() {
		productos = '';
		
		$('#tab_2 table tbody').html('');
		
		if (almacenes_privera_vez) {
			var oTable = $('#tab_2 #example1').dataTable();
			oTable.fnDestroy();
		}
		
		$.ajax({
			type: "POST",
			dataType: "json",
			async: true,
			url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador_productos_almacen",
			data: {'al_id':$('#almacenes option:selected').val(),'al_tipo':$('#almacenes option:selected').attr('al_tipo')},
			success: function(data) {
				var table= '';
				
				$(data).each(function(indice,valor){				
					table = table + '<tr product_id="'+data[indice]['product_id']+'">';
					table = table + '<td class="vcenter">'+data[indice]['product_referencia']+'</td>';
					table = table + '<td class="vcenter">'+data[indice]['product_nombre']+'</td>';
					
					var caracteristicas = data[indice]['product_caracteristicas'];
	  				caracteristicas = caracteristicas.split(';');
					//ELIMINAR ESPACIOS EN BLANCO
					caracteristicas = caracteristicas.filter(Boolean);
					for (var i=0; i<caracteristicas.length; i++) {
						caracteristicas[i] = caracteristicas[i].split('/');
					}
							
					var opciones = '';
					for (var i=0; i<caracteristicas.length; i++) {
						opciones = opciones + '<span class="opc_product_list"><strong>' + caracteristicas[i][0] + '</strong>: <span>' + caracteristicas[i][1] + '</span></span>';
					}
					
					var reciclaje = '';
					if (data[indice]['pra_caracteristicas'] != null) {
						reciclaje = '<br><?php echo lang("reciclado") ?>: '+data[indice]['pra_caracteristicas'];
					}
					
					table = table + '<td class="vcenter">'+opciones+reciclaje+'</td>';
					table = table + '<td class="vcenter col-sm-3">'+data[indice]['pro_nombre']+'</td>';
					table = table + '<td class="text-center vcenter">'+data[indice]['fotos']+'</td>';
					table = table + '<td class="text-center vcenter col-sm-1">'+data[indice]['product_cantidad']+'</td>';
					
					table = table + '<td class="vcenter col-sm-1"><div class="input-group" style="width:90px;"><input type="text" name="product_cantidad" onKeypress="return soloNumeros(event)" class=" text-center form-control" value=""><span class="pointer input-group-addon anadir_producto" title="<?php echo lang("anadir") ?>"><i class="fa fa-plus"></i></span></div></td>';
					
					table = table + '</tr>';
				});
							
				$('#tab_2 tbody').html(table);
				almacenes_privera_vez = true;
				
				$("#tab_2 #example1").dataTable({
					"aaSorting": [[ 3, "asc"],[ 0, "asc"]],
					"oLanguage": {
					  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
					  	"oPaginate": {
				           	"sPrevious": "",
				           	"sNext":""
				       	},
				       	"sRefresh": "<?php echo lang('refrescar') ?>",
				       	"sNuevo": "<i class='fa fa-th-large'></i> <?php echo lang('nuevo') ?>",
				       	"sLengthMenu": '<select class="form-control">'+
			       		'<option value="10">10</option>'+
			       		'<option value="20">20</option>'+
			       		'<option value="30">30</option>'+
			      		'<option value="40">40</option>'+
			       		'<option value="50">50</option>'+
			       		'<option value="-1">All</option>'+
			       		'</select> <?php echo lang("por.pagina") ?>'
					}
				});
				
				$("#tab_2 #example1").css('width','100%');
				
				$('#tab_2 #btn_nuevo,#tab_2 #btn_refrecar').hide();
				
				
			},
			  	error: function(XMLHttpRequest, textStatus, errorThrown) {
			   	bootbox.alert('<?php echo lang("error.ajax") ?>');
			}
		});
	}
					
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "<?php echo base_url().$this->lang->lang() ?>/almacenes/buscador",
		data: {'buscar':''},
		async: true,
		success: function(data) {
			var options = '';
								
			$.each(data,function(indice,valor) {
				var tipo = '<?php echo lang("deposito") ?>';
				if (data[indice]['al_tipo'] == 1) {
					tipo = '<?php echo lang("reciclaje") ?>';
				}
				
				if (indice == 0) {
					options = options + '<option al_tipo="'+data[indice]['al_tipo']+'" value="'+data[indice]['al_id']+'" selected>'+data[indice]['al_nombre']+' ('+tipo+')</option>';		
				} else {
					options = options + '<option al_tipo="'+data[indice]['al_tipo']+'" value="'+data[indice]['al_id']+'">'+data[indice]['al_nombre']+' ('+tipo+')</option>';	
				} 
				
				//ALMACENES PARA EL RECICLAJE
				if (data[indice]['al_tipo'] == 1) {
					if (almacenes_reciclaje == '') {
						almacenes_reciclaje = almacenes_reciclaje + '<option value="'+data[indice]['al_id']+'" selected>'+data[indice]['al_nombre']+'</option>';
					} else {
						almacenes_reciclaje = almacenes_reciclaje + '<option value="'+data[indice]['al_id']+'">'+data[indice]['al_nombre']+'</option>';
					}
				} 				
	   		});
	   		
	   		$('#almacenes').append(options);
	   		
	   		$("#almacenes").change(function () {
				buscar_productos();
			}).trigger('change');
	}});
	
	//IMAGENES
	$(".fancybox").fancybox({
		helpers : {
	   		title : false
	   	},
	   	padding: 0,
		openEffect : 'elastic',
		openSpeed  : 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick : false
	});
	
	//AÑADIR PRODUCTO NUEVO
	$(document).on('click','#tab_2 .anadir_producto',function(e){
		e.preventDefault();
		//COGEMOS EL PRODUCTO QUE QUEREMOS AÑADIR
		var fila = $(this).parent().parent().parent();
		var cantidad = $(this).prev().val();
		var cantidad_total = $(fila).find('td:last').prev().html();
		var disable = '<?php echo $disable ?>';	
		var producto = '';
		
		if (parseFloat(cantidad) <= parseFloat(cantidad_total) && parseFloat(cantidad) > 0) {
			//NO ENCONTRO EL PRODUCTO POR LO QUE LO AÑADIMOS NUEVO
			producto = '<tr nuevo="true" al_id="'+$('#almacenes option:selected').val()+'" product_id="'+$(fila).attr("product_id")+'">';
			
			if (ta_id == '' || disable == '') {
				producto = producto + '<td class="text-center"><i class="fa fa-times eliminar"></i></td>';
				producto = producto + '<td>'+$(fila).find('td:first').html()+'</td>';
			} 
			
			producto = producto + '<td>' + $(fila).find('td:first').next().html() + '</td>';
			producto = producto + '<td>' +  $(fila).find('td:first').next().next().html() + '</td>';
			producto = producto + '<td>' +  $(fila).find('td:first').next().next().next().html() + '</td>';
			producto = producto + '<td class="text-center">' +  $(fila).find('td:first').next().next().next().next().html() + '</td>';
			producto = producto + '<td class="text-center">'+cantidad+'</td>';
			producto = producto + '</tr>';
			
			$('#tab_1 #example2 tbody').append(producto);
			
			$(this).find('i').removeClass('fa-plus');
			$(this).find('i').addClass('fa-check');
		}
	});
	
	//ELIMINAR TRABAJO
	$(document).on('click','.eliminar-simples, .eliminar-compuesto',function(){
		$(this).parent().parent().remove();
	});

	//ELIMINAR PRODUCTO AÑADIDO
	$(document).on('click','#tab_1 #example2 .eliminar', function(){
		if($(this).parent().parent().attr('nuevo') == 'false'){
			var aux = $(this).parent().parent().attr('al_id') + '-'+ $(this).parent().parent().attr('product_id');
			var aux = aux + '-' + $(this).parent().parent().find('td:last').html() + ';';
			pr_eliminados = pr_eliminados + aux;
		}

		$(this).parent().parent().remove();
	});	
		
	var disable = '<?php echo $disable ?>';		
	if (disable != '') {
		$('#info input, textarea').attr('readonly',true);
		$('select, #fecha_trabajo').attr('disabled',true);
	}
	
	$(document).on('click','.reciclar button',function(){
		var isValid = true;
		$(this).parent().parent().parent().find("input").each(function() {
		   var element = $(this);
		   if (element.val() == "") {
			   isValid = false;
		   }
		});
		if(isValid == true) {
			$(this).parent().parent().clone(false).appendTo($(this).parent().parent().parent()).find('input').attr('value', function() {
				return $(this).val('');  
			});
			$(this).remove();
		}
	});

	//LLENAMOS BITACORA
	function llenar_bitacora(){		
		$.post(
		"<?php echo base_url().$this->lang->lang() ?>/bitacora/buscar_todo",
		{'bi_idasociado':'<?php echo isset($ta_id)?$ta_id:"0" ?>','bi_tipo':'TA'},
		function(data){
			
			var table = '';
			$.each(data,function(indice) {
				table = table + '<tr id="<?php echo isset($ta_id)?$ta_id:'' ?>">';
				table = table + '<td>' + data[indice]['bi_fecha'] + '</td>';	
				var empleado = data[indice]['emp_nombre'] + ' ' + data[indice]['emp_apellido1']+ ' ' + data[indice]['emp_apellido2'];	
				table = table + '<td>' + empleado + '</td>';
				table = table + '<td>' + data[indice]['bi_accion'] + '</td>';			
				table = table + '</tr>';
			});

			$('#bitacora tbody').html(table);
	                
		}, "json");
	}

	llenar_bitacora();

	//LLENAMOS AUTORIZACIONES
	function llenar_autorizaciones(){	
		var my_id="<?php echo isset($ta_id)?$ta_id:'' ?>";
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/taller/get_autorizaciones_taller/"+my_id,
			async: true,
			success: function(data) {
				console.log(data);

				if(data.categoria_autorizadas != null){
					var table = '';
					$.each(data.categoria_autorizadas,function(indice) {
						table = table + '<tr id="<?php echo isset($ta_id)?$ta_id:'' ?>">';
						table = table + '<td>' + data.categoria_autorizadas[indice]['usuario_aprobacion'] + '</td>';
						table = table + '<td>' + data.categoria_autorizadas[indice]['nombre_categoria'] + '</td>';
						table = table + '<td style="text-align:right;">' + data.categoria_autorizadas[indice]['costo'] + '</td>';			
						table = table + '<td>' + data.categoria_autorizadas[indice]['fecha_aprobacion'] + '</td>';			
						table = table + '</tr>';
					});

					$('#autorizaciones tbody').html(table);
				}
			}
		});
	}

	llenar_autorizaciones();
});
</script>