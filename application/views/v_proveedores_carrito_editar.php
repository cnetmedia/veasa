<script>
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
return ((key >= 48 && key <= 57) || (key==8) || (key == 46));
}
</script>
<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th class="col-sm-1"><?php echo lang("eliminar.producto") ?></th>
                		<th><?php echo lang("referencia") ?></th>
                		<th><?php echo lang("descripcion") ?></th>
                		<th><?php echo lang("caracteristicas") ?></th>
                		<th class="col-sm-1"><?php echo lang("precio").' '.$moneda ?></th>
                		<th class="col-sm-1"><?php echo lang("cantidad") ?></th>
                		<th class="col-sm-2 text-right"><?php echo lang("sub.total").' '.$moneda ?></th>
					</tr>
				</thead>
                                        
				<tfoot>
					<tr>
					  <td colspan="6" class="text-right"><strong><?php echo lang("total") ?></strong></td>
					  <td id="getsubtotal" class="text-right"><strong><?php echo $car_subtotal; ?></strong></td>
					</tr>
				</tfoot>
				
				<tbody id="lista_carrito" aria-relevant="all" aria-live="polite" role="alert">
					<?php

					//ORDENAMOS EL CARRITO ANTES DE MOSTRAR
					$listado = array();
					$product = explode(";", $product_id);
					$product_cantidad = explode(";", $product_cantidad);
					$product_cantidad_recibido = explode(";", $product_cantidad_recibido);
					
					$count = 0;
					
					foreach ($product as $product) {
						$query = $this->db->get_where('productos_proveedores', array('product_id' => $product));
						$listado[$count] = $query->row_array();
						$listado[$count]['product_cantidad'] = $product_cantidad[$count];
						$listado[$count]['product_cantidad_recibido'] = $product_cantidad_recibido[$count];
						$count++;
					}

					for ($l=0; $l<count($listado)-1; $l++) {
						
						$opciones = '';
						
						$caracteristicas = explode(";", $listado[$l]['product_caracteristicas']);
						
						for ($y=0; $y<count($caracteristicas)-1; $y++) {
    	
							$options = explode("/", $caracteristicas[$y]);
							
							$opciones = $opciones.'<div class="opc_product_list"><strong>'.$options[0].':</strong> <span>'.$options[1].'</span></div>';
		
						}
						
						echo '<tr product_id="'.$listado[$l]['product_id'].'" pro_id="'.$listado[$l]['pro_id'].'"><td class="vcenter text-center"><i class="fa fa-times eliminar"></i></td><td class="vcenter">'.$listado[$l]["product_referencia"].'</td><td class="vcenter">'.$listado[$l]["product_nombre"].'</td><td class="vcenter">'.$opciones.'<input type="hidden" name="product_caracteristicas" id="product_caracteristicas" value="'.$caracteristicas.'"></td><td class="vcenter">'.(double)$listado[$l]["product_precio"].'</td><td><input class="form-control text-center qty" type="text" name="cantidad" value="'.(double)$listado[$l]["product_cantidad"].'" onKeypress="return soloNumeros(event)"><input class="rec_qty" type="hidden" value="'.(double)$listado[$l]["product_cantidad_recibido"].'"></td><td class="text-right vcenter subtotal">'.number_format(round((double)$listado[$l]["product_precio"]*(double)$listado[$l]["product_cantidad"], 2),2,'.','').'</td></tr>';
					}
					 ?>
					
					
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-6  text-left">		
    	<button id="agregar_producto" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-plus"></i> <?php echo lang('agregar.producto') ?></button>
    </div>
</div>

<div class="row">
	<div class="col-lg-6 col-md-6">
	</div>
	
	<div class="col-lg-6 col-md-6">				
		<label><?php echo lang('presupuesto') ?></label>
		<select id="cotizaciones" name="pr_id" class="selectpicker form-control" data-live-search="true"></select>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<label><?php echo lang('observaciones.empresa') ?></label>
		<textarea class="form-control" id="observaciones_empresa"><?php echo $car_observaciones_empresa; ?></textarea>
	</div>
	
	<div class="col-lg-6 col-md-6">
		<label><?php echo lang('observaciones.proveedor') ?></label>
		<textarea class="form-control" id="observaciones_proveedor"><?php echo $car_observaciones_proveedor; ?></textarea>
	</div>
</div>

<div class="row">
	<div class="col-lg-6 col-xs-12 fot">
		<label><?php echo lang('adjunto.carrito') ?></label>
		<div>
			<form class="fotos" action="<?php echo base_url().$this->lang->lang() ?>/proveedores/upload" method="post" enctype="multipart/form-data">
			<div class="input-group">
				<span class="input-group-btn">
					<span class="btn btn-primary btn-file">
						<i class="fa fa-file"></i> <?php echo lang('cargar.adjunto.carrito') ?>
						<input type="file" id="file-adjunto" name="archivos[]" multiple="multiple">
					</span>
				</span>
				<input type="text" class="form-control" readonly>
			</div>
			</form>
		</div>
		<div class="text-danger"><?php echo form_error("fotos") ?></div>
		<div id="archivos-adjuntos" style="margin-top: 5px;border: 1px solid lightgray;padding: 5px;">
			<?php 
				$archivos = explode('|', $car_adjuntos); 
				$i = 1;
				foreach ($archivos as $key => $file) {
					if(strlen($file) > 0 ){					
						echo '<div style="padding: 5px;width:100%;" id="file_'.$i.'" >' .
								'<i class="fa fa-times pointer" file_name="'.$file.'" file_id="file_'.$i.'" style="margin-right: 5px;"></i>' .
								'<a target="_blank" href="'.base_url().'img/dynamic/adjuntos-carrito/'.$file . '">'.$file .'</a>' .
							'</div>';
							$i++;
					}
				}
			?>
		</div>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-12 text-right">
		<?php if ($this->session->userdata('emp_tipo') <= 1) { ?>
		<button id="recalcular_entrada" type="button" class="btn btn-default ladda-button" data-style="zoom-out"><i class="fa fa-calculator"></i> <?php echo lang('recalcular.entrada') ?></button>
		<?php } ?>

		<button id="eliminar_carrito" type="button" class="btn btn-default ladda-button" data-style="zoom-out"><i class="fa fa-trash-o"></i> <?php echo lang('eliminar.carrito') ?></button>
	
		<button id="actualizar_carrito" type="button" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-refresh"></i> <?php echo lang('actualizar.carrito') ?></button>
		
		<!-- SI PUEDE CREAR O EDITAR CONTENIDO MUESTRA EL BOTON -->
		<?php if ($this->session->userdata('emp_crear') == '1' || $this->session->userdata('emp_editar') == '1') { ?>
			<button id="procesar_pedido" type="button" class="btn btn-danger ladda-button" data-style="zoom-out">
				<i class="fa fa-cart-arrow-down"></i> <?php echo lang('procesar.pedido') ?>
			</button>
		<?php } ?>
		
		<a href="<?php echo base_url().$this->lang->lang().'/proveedores/carritos' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
	</div>
</div>
                
<script>
var files_delete = ''; 
/* ESTILO Y BOTON ADJUNTOS */
$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(document).on('change', '.qty', function() {
	var cantidad = parseFloat($(this).val());
	var precio = parseFloat($(this).parent().prev().text());
	var subtotal = parseFloat(cantidad*precio).toFixed(2);
	$(this).parent().next().text(subtotal);
	var total = 0;
	var elements = $("#lista_carrito .subtotal");
	for (var i = 0; i < elements.length; i++) {
		total += parseFloat(elements[i].innerHTML);
	};
	$('#example1 tfoot tr:eq(0) td:last').text(parseFloat(total).toFixed(2));
});

$(document).ready(function(){

	$(document).on('click', '#archivos-adjuntos .pointer', function() {
		console.log($(this).attr('file_id'));
		files_delete = files_delete + $(this).attr('file_name') + ',';
		$('#'+$(this).attr('file_id')).remove();	
	});
	
	cargarPresupuestos();
	
	calcularTotal();
	
	function calcularTotal() {
		var total = 0;
		var totales = [];
		$('#lista_carrito tr td:last-child').each(function(){
			total = parseFloat($(this).text());
			totales.push(total);
		});
		var total = 0;
		while(totales.length > 0) { total += totales.pop() };
		$('#getsubtotal strong').text(parseFloat(total).toFixed(2));
	}
	
	function cargarPresupuestos() {
		//RELLENAMOS EL CAMPO DE PRESUPUESTOS	
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/presupuestos/buscador",
			data:{'buscar':''},
			success: function(data) {
				var listado = '<option value="0">Para stock de Inventario</option>';
					
				$.each(data,function(indice) {
					if (data[indice]["pr_estado"] != '3') {
						if(data[indice]["pr_id"] == '<?php echo $pr_id; ?>') {
							listado = listado + '<option value="'+data[indice]["pr_id"]+'" selected>'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>';
						} else  {
							listado = listado + '<option value="'+data[indice]["pr_id"]+'">'+data[indice]["pr_numero"]+' - '+data[indice]["man_nombre"]+' - '+data[indice]["pr_fecha"]+'</option>';
						}
					}
				});
				
				$('#cotizaciones').append(listado);
			},
			complete: function() {
				$('#cotizaciones').selectpicker();
				$('#cotizaciones').selectpicker('refresh'); 
			}
		});
	}
	
	//DATOS CARRITO
	var datos = [];
	var l;
	
	/* ESTILO Y BOTON ADJUNTOS */
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
		var input = $(this).parents('.input-group').find(':text'),
			log = numFiles > 1 ? numFiles + ' archivos seleccionados' : label;
			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}	
	});
	
	//ENVIAR CARRITO
	function actualiza_carrito(datos) {

		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/actualizar_carritos",
			data: datos,
			success: function(data) {
				//console.log($("#file-adjunto").get(0));
				var files = $("#file-adjunto").get(0).files;
		        var fileData = new FormData();

		        if(files.length > 0){		        	
			        for (var i = 0; i < files.length; i++) {
			            fileData.append("form"+i+"-"+<?php echo $car_id; ?>, files[i]);
			        }

			        fileData.append("eliminados",files_delete);
			        fileData.append("adjuntos_iniciales","<?php echo $car_adjuntos; ?>");

			        $.ajax({
			            type: "POST",
			            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/upload_adjuntos",
			            dataType: "json",
			            contentType: false, // Not to set any content header
			            processData: false, // Not to process data
			            data: fileData,
			            success: function (result, status, xhr) {
							if (data) {
								//refrescar sin cache
								location.reload(true);
							}		                
			            },
			            error: function (xhr, status, error) {
			                alert(status + ' ' + error);
			            }
			        });
		        }else{
					if (data) {
						//refrescar sin cache
						location.reload(true);
					}
				}
			},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	}
	
	//ELIMINAR CARRITO
	function eliminar_carrito() {
		
		var datos = {
			'car_id' : <?php echo $car_id; ?>
		};
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/eliminar_carrito",
			data: datos,
			success: function(data) {
				if (data) {
					//volcer a la lista de carritos
					location.href = "<?php echo base_url().$this->lang->lang() ?>/proveedores/carritos";
				}
			},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	}
	
	$('#eliminar_carrito').click(function(e){
		e.preventDefault();
		l = Ladda.create( document.querySelector( "#eliminar_carrito" ) );
	 	l.start();
		eliminar_carrito();
	});
	
	//GUARDAR PEDIDO Y PEDIR PRODUCTOS
	$('#procesar_pedido').click(function(e){
		e.preventDefault();
	
		l = Ladda.create( document.querySelector( "#procesar_pedido" ) );
	 	l.start();
		
		if($('#cotizaciones option:selected').val()) {
	 	
	 	var car_id = <?php echo $car_id; ?>;
		var pro_id = '';
		var product_id = '';
		var product_cantidad = '';
		var product_cantidad_recibido = '';
		var pe_observaciones_empresa = $.trim($('#observaciones_empresa').val());
		var pe_observaciones_proveedor = $.trim($('#observaciones_proveedor').val());
		var pr_id = $('#cotizaciones option:selected').val();
		
		$('#lista_carrito tr').each(function(){
			pro_id = pro_id + parseInt($(this).attr('pro_id')) + ';';
			product_id = product_id + parseInt($(this).attr('product_id')) + ';';
			product_cantidad = product_cantidad + parseFloat($(this).find('input.qty').val()) + ';';
			product_cantidad_recibido = product_cantidad_recibido + '0' + ';';
		});

		var dataPost = {
			'car_id':car_id,
			'pro_id':pro_id, 
			'product_id':product_id, 
			'product_cantidad':product_cantidad, 
			'pe_observaciones_empresa':pe_observaciones_empresa,  
			'pe_observaciones_proveedor':pe_observaciones_proveedor, 
			'pr_id':pr_id, 
			'product_cantidad_recibido':product_cantidad_recibido,
		};
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/procesar_pedido",
			data: dataPost,
			success: function(data) {
				var files = $("#file-adjunto").get(0).files;
		        var fileData = new FormData();
		        var objCorreos = data.objCorreos;

		        if(files.length > 0){		        	
			        for (var i = 0; i < files.length; i++) {
			            fileData.append("form"+i+"-"+<?php echo $car_id; ?>, files[i]);
			        }

			        fileData.append("eliminados",files_delete);
			        fileData.append("adjuntos_iniciales","<?php echo $car_adjuntos; ?>");

			        $.ajax({
			            type: "POST",
			            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/upload_adjuntos",
			            dataType: "json",
			            contentType: false, // Not to set any content header
			            processData: false, // Not to process data
			            data: fileData,
			            success: function (result, status, xhr) {
			            	objCorreos.adjuntos = result.result;
			            	console.log('data',data);
			            	$.ajax({
					            type: "POST",
					            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/send_mail_proveedores",
					            dataType: "json",
					            data: objCorreos,
					            success: function (result, status, xhr) {
									if (data.status) {					
										bootbox.alert('<?php echo lang("pedido.carrito.ok") ?>');
										//REDIRIGIMOS A LOS 5 SEGUNDOS
										var pagina = '<?php echo base_url().$this->lang->lang() ?>/proveedores/productos';
										setTimeout("location.href='"+pagina+"'", 5000);
									} else {
										l.stop();
									}		                
					            },
					            error: function (xhr, status, error) {
					                alert(status + ' ' + error);
					            }
					        });									                
			            },
			            error: function (xhr, status, error) {
			                alert(status + ' ' + error);
			            }
			        });
		        }else{
		        	
		        	objCorreos.adjuntos = "<?php echo $car_adjuntos; ?>";
		        	//console.log('data',data);
					$.ajax({
			            type: "POST",
			            url: "<?php echo base_url().$this->lang->lang() ?>/proveedores/send_mail_proveedores",
			            dataType: "json",
			            data: objCorreos,
			            success: function (result, status, xhr) {
							if (data.status) {					
								bootbox.alert('<?php echo lang("pedido.carrito.ok") ?>');
								//REDIRIGIMOS A LOS 5 SEGUNDOS
								var pagina = '<?php echo base_url().$this->lang->lang() ?>/proveedores/productos';
								setTimeout("location.href='"+pagina+"'", 5000);
							} else {
								l.stop();
							}		                
			            },
			            error: function (xhr, status, error) {
			                alert(status + ' ' + error);
			            }
			        });
				}				
			},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
		
		} else {
			bootbox.alert('<?php echo lang("error.carrito.cotizaciones") ?>');
			l.stop();
		}
	});
	
	//ACTUALIZAR CARRITO
	function actualizar_carrito() {
		
		$('#cotizaciones').selectpicker('refresh');
		
		var datos = {
			'car_id' : <?php echo $car_id; ?>,
			'car_observaciones_proveedor' : $('#observaciones_proveedor').val(),
			'car_observaciones_empresa' : $('#observaciones_empresa').val(),
			'car_subtotal' : parseFloat($('#example1 tfoot tr:eq(0) td:last').text()),
			'pr_id' : $('#cotizaciones option:selected').val()
		};
		
		datos['products'] = [];
		
		$('#lista_carrito tr').each(function(){
			datos['products'].push({ 'product_id' : $(this).attr('product_id'), 'product_cantidad' : $(this).find('input.qty').val(), 'product_cantidad_recibido' : $(this).find('input.rec_qty').val() });
		});
			
		actualiza_carrito(datos);
	}
	
	$('#actualizar_carrito').click(function(e){
		e.preventDefault();
		l = Ladda.create( document.querySelector( "#actualizar_carrito" ) );
		l.start();
		
		if($('#cotizaciones option:selected').val()) {
			actualizar_carrito();
		} else {
			bootbox.alert('<?php echo lang("error.carrito.cotizaciones") ?>');
			l.stop();
		}
	});
	
	
	//BORRAR PRODUCTO
	$('.eliminar').click(function(e){
		e.preventDefault();
		
		l = Ladda.create( document.querySelector( "#actualizar_carrito" ) );
	 	l.start();
		
		$(this).parent().parent().remove();
		
		actualizar_carrito();
	});

	//CREAR ENTRADAS MANUALES
	function enviar_entrada_manual(datos) {
		console.log(datos);
		var opciones = '';
		var opc = [];
		
		if(datos['product_caracteristicas'] != '') {
	
			var txt = datos['product_caracteristicas'],
				list = txt.split(";");
			
			for(var i=0; i<list.length-1; i++){
				opc = list[i].split("/");
				opciones = opciones+'<div class="opc_product_list"><strong>'+opc[0]+':</strong> <span>'+opc[1]+'</span></div>';
			}
			
		} else {
			
			opciones = datos['product_caracteristicas'];
			
		}
		
		var row_producto = '<tr product_id="'+datos['product_id']+'" pro_id="'+datos['pro_id']+'">' + 
							'<td class="vcenter text-center"><i class="fa fa-times eliminar"></i></td>' +
							'<td class="vcenter">'+datos['product_referencia']+'</td>' +
							'<td class="vcenter">'+datos['product_nombre']+'</td>' +
							'<td class="vcenter">'+opciones+
								'<input type="hidden" name="product_caracteristicas" id="product_caracteristicas" value="'+datos['product_caracteristicas']+'">'+ 
							'</td>' +
							'<td class="vcenter">'+parseFloat(datos['product_precio']).toFixed(2)+'</td>' +
							'<td>' +
									'<input class="form-control text-center qty" type="text" name="cantidad" value="'+parseFloat(datos['product_cantidad']).toFixed(2)+'" onKeypress="return soloNumeros(event)">' +
							'</td>' +
							'<td class="text-right vcenter">'+parseFloat(parseFloat(datos['product_precio'])*parseFloat(datos['product_cantidad'])).toFixed(2)+'</td>' +
							'</tr>';
		
		$('#lista_carrito').append(row_producto);
		
		//actualizar_carrito();
		calcular();
	}
	
	//RECALCULAR TOTALES
	function calcular() {
		var total = 0;
		
		$('#example1 tbody tr').each(function(){
	 		var suma = 0;
	 		suma = parseFloat($(this).find('input[name="cantidad"]').val()) * parseFloat($(this).find('input[name="cantidad"]').parent().prev().html());
	 		$(this).find('td:last').html(suma.toFixed(2));
	 		total = total + suma;
	 	});
	 	
	 	$('#example1 tfoot td:last').html(total.toFixed(2));
	}

	$('#recalcular_entrada').click(function(e){
		e.preventDefault();
		
		//LOADING BOTON
		l = Ladda.create(document.querySelector("#recalcular_entrada"));
	 	l.start();
	 	calcular();
	 	l.stop();
	});
	
	//AGREGAR PRODUCTOS
	$("#agregar_producto").click(function(e){
		e.preventDefault();
		
			//RELLENAMOS EL CAMPO DE PRODUCTOS
			$.post(
			"<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador_productos",
			{'buscar':''},
			function(data){
				
					var materiales = '<option value="" selected> </option>';
					var datos = {
						'product_id' : '',
						'product_cantidad' : '',
						'product_nombre' : '',
						'product_referencia' : '',
						'product_precio' : '',
						'product_caracteristicas' : '',
						'pro_id' : ''
					};
					
					ids = [];
					
					$('#lista_carrito tr').each(function(){
						var product_id = $(this).attr('product_id');
						ids.push(product_id);
					});
	
					$.each(data,function(indice) {
						if(ids.indexOf(data[indice]["product_id"])<0) {
							String.prototype.replaceArray = function(find, replace) {
							  var replaceString = this;
							  var regex; 
							  for (var i = 0; i < find.length; i++) {
								regex = new RegExp(find[i], "g");
								replaceString = replaceString.replace(regex, replace[i]);
							  }
							  return replaceString;
							};
							var caracteristicas = data[indice]["product_caracteristicas"];
							var find = [";", "/",];
							var replace = [" | ", ": ",];
							caracteristicas = caracteristicas.replaceArray(find, replace);
							materiales = materiales + '<option value="'+data[indice]["product_id"]+'" proid="'+data[indice]["pro_id"]+'" ref="'+data[indice]["product_referencia"]+'" price="'+data[indice]["product_precio"]+'" options="'+data[indice]["product_caracteristicas"]+'" name="'+data[indice]["product_nombre"]+'" >'+data[indice]["product_nombre"]+' ('+data[indice]["product_referencia"]+') - '+caracteristicas+'</option>';	
						}
					});
					
					bootbox.dialog({
						message: "<div id='new_entrada'><h3><?php echo lang('productos') ?></h3><select id='productos' class='selectpicker form-control' data-live-search='true'>"+materiales+"</select></div><script>$(document).ready(function(){$('#new_entrada #productos').selectpicker();});<\/script>",
						buttons: {
								success: {
										label: "<?php echo lang('seleccionar') ?>",
										className: "btn-success",
										callback: function() {
											
											datos['product_id'] = $('#new_entrada #productos option:selected').val();
											
											//PRODUCTO ENCONTRADO SOLO AÑADIMOS LA CANTIDAD
											if (datos['product_id']  != '') {
												datos['product_precio'] = $('#new_entrada #productos option:selected').attr('price');
												datos['product_caracteristicas'] = $('#new_entrada #productos option:selected').attr('options');
												datos['product_nombre'] = $('#new_entrada #productos option:selected').attr('name');
												datos['product_referencia'] = $('#new_entrada #productos option:selected').attr('ref');
												datos['pro_id'] = $('#new_entrada #productos option:selected').attr('proid');

												bootbox.dialog({
													message: "<div id='new_entrada'><label><?php echo lang('cantidad') ?></label><input type='text' name='product_cantidad' id='product_cantidad' value='' class='form-control'><label></div>",
													buttons: {
															success: {
																	label: "<?php echo lang('guardar') ?>",
																	className: "btn-success",
																	callback: function() {
																	   datos['product_cantidad'] = $('#new_entrada #product_cantidad').val();
																	   enviar_entrada_manual(datos);
																	}
															},
															cancel: {
																	label: "<?php echo lang('cancelar') ?>",
																	className: "btn-default"
															}
													}
												});
											}
										}
								},
								cancel: {
										label: "<?php echo lang('cancelar') ?>",
										className: "btn-default"
								}
						}
					});
					
					
			}, "json");
		});
	
});
</script>