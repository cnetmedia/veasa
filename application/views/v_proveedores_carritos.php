<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th width="3%"><?php echo lang("numero.corto") ?></th>
                		<th width="15%"><?php echo lang("fecha") ?></th>
                		<th width="30%"><?php echo lang("cliente") ?></th>
                        <th width="20%"><?php echo lang("mantenimiento") ?></th>
                        <th width="10%"><?php echo lang("presupuesto") ?></th>
						<th width="10%"><?php echo lang("stock.col") ?></th>
                		<th width="10%"><?php echo lang("importe") ?></th>
                		<th width="5%"><?php echo lang("estado") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("numero.corto") ?></th>
                		<th><?php echo lang("fecha") ?></th>
                		<th><?php echo lang("cliente") ?></th>
                        <th><?php echo lang("mantenimiento") ?></th>
                        <th><?php echo lang("presupuesto") ?></th>
                		<th><?php echo lang("stock.col") ?></th>
						<th><?php echo lang("importe") ?></th>
                		<th><?php echo lang("estado") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//MOSTRAR EMPLEADOS
	function mostrar_carritos() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/proveedores/buscador_carritos",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE CLIENTES GENERANDO LAS FILAS
					var table = '';
					
					$.each(data,function(indice,valor) {
						var su_id = '<?php echo $this->session->userdata("su_id") ?>';
						var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
						
							table = table + '<tr id="'+data[indice]['car_id']+'">';
	  						table = table + '<td>'+data[indice]['car_id']+'</td>';
	  						table = table + '<td>'+data[indice]['car_fecha']+'</td>';
							if(data[indice]['cl_nombre'] !== null) {
	  						table = table + '<td>'+data[indice]['cl_nombre']+'</td>';
							} else {
								table = table + '<td></td>';
							}
							
							if(data[indice]['man_id'] !== null) {
	  						table = table + '<td><b>'+data[indice]['man_id']+'</b> - '+data[indice]['man_nombre']+'</td>';
							} else {
								table = table + '<td></td>';
							}
							
							if(data[indice]['pr_numero'] !== null) {
	  						table = table + '<td>'+data[indice]['pr_numero']+'</td>';
							} else {
								table = table + '<td></td>';
							}
							
							if(data[indice]['pr_numero'] == null) {
	  						table = table + '<td><i class="fa fa-check"></i></td>';
							} else {
								table = table + '<td></td>';
							}
							
	  						table = table + '<td>'+data[indice]['car_subtotal']+'</td>';
	  						
	  						var estado;
	  						if (data[indice]['car_estado'] == 0) {
								estado = '<span class="label label-info"><?php echo lang("pendiente") ?></span>';
							} else if (data[indice]['car_estado'] == 1) {
								estado = '<span class="label label-primary"><?php echo lang("aprobada") ?></span>';
							} else if (data[indice]['car_estado'] == 2) {
								estado = '<span class="label label-warning"><?php echo lang("caliente") ?></span>';
							} else if (data[indice]['car_estado'] == 3) {
								estado = '<span class="label label-danger"><?php echo lang("cancelada") ?></span>';
							}
	  						
	  						table = table + '<td class="text-center">'+estado+'</td>';
	  						table = table + '</tr>';
					});
					$('#listar tbody').html(table);
					
					$("#example1").dataTable({
						"aaSorting": [[ 1, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-signal'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//SI NO ES SUPER USUARIO OCULTAMOS LA COLUMNA SUCURSALES
					var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
					if (tipo > 0) {
						$('#listar table thead tr th:last').hide();
						$('#listar table tbody tr').each(function(indice,valor) {
							$(this).find('td:last').hide();
						});
						$('#listar table tfoot tr th:last').hide();
					}
			        
			        //PREGUNTAMOS SI PUEDE CREAR
			        var crear = '<?php echo $this->session->userdata("emp_crear") ?>';
			        if (crear == '0') {
			        	$('#listar #btn_nuevo').hide();
			        }
					
					//NUEVA FACTURA
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/proveedores/productos');
					});
			        
			        //REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					    window.location.reload(true); 
					});
			        
			        //EDITAR O VER SUCURSAL
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
                        $("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/proveedores/editar_carrito/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_carritos();
});
</script>