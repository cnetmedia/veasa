<!-- NUEVA NOTA -->
<?php if ($no_id == '') { ?>

<div class="row" id="notas">
	<div class="col-lg-4 col-xs-12">
		<div class="form-group">
		    <label><?php echo lang('tipo.nota') ?></label>

			<div class="radio radio_line">
				<?php if ($this->session->userdata("emp_tipo") != '3') { ?>
					<label>
						<input type="radio" name="no_tipo" value="0" <?php echo ($no_tipo == 0)?'checked':'' ?> /> <?php echo lang('empresa') ?>
					</label>
				<?php } ?>
				
				<label>
					<input type="radio" name="no_tipo" value="1" <?php echo ($no_tipo == 1)?'checked':'' ?> /> <?php echo lang('privada') ?>
				</label>
			</div>
		</div>
		
		<div class="form-group suc">
			<label><?php echo lang('sucursal') ?></label>
			<select id="sucursales" name="su_id" class="form-control"></select>
			<span class="glyphicon glyphicon-remove form-control-feedback"></span>
			<div class="text-danger"><?php echo form_error('su_id') ?></div>
		</div>
		
		<div class="form-group dep">
			<label><?php echo lang('departamento') ?></label>
			<select id="departamentos" name="de_id" class="form-control"></select>
			<span class="glyphicon glyphicon-remove form-control-feedback"></span>
			<div class="text-danger"><?php echo form_error('de_id') ?></div>
		</div>
		
		<div class="form-group emp">
			<label><?php echo lang('empleado') ?></label>
			<select id="empleados" name="no_emp_id" class="form-control"></select>
			<span class="glyphicon glyphicon-remove form-control-feedback"></span>
			<div class="text-danger"><?php echo form_error('no_emp_id') ?></div>
		</div>
	</div>
	
	<div class="col-lg-8 col-xs-12">
		<?php
			$input_titulo = array(
				'name'		=>	'no_titulo',
				'id'		=>	'no_titulo',
				'class'		=>	'form-control',
				'maxlength'	=>	'150',
				'value'		=>	isset($no_titulo)?$no_titulo:set_value('no_titulo')
			);

			$input_mensaje = array(
				'name'		=>	'no_mensaje',
				'id'		=>	'no_mensaje',
				'class'		=>	'form-control',
				'maxlength'	=>	'4000',
				'style'		=>	'max-height:135px;min-height:135px;max-width:100%;min-width:100%;',
				'value'		=>	isset($no_mensaje)?$no_mensaje:set_value('no_mensaje')
			);
		?>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label><?php echo lang('titulo') ?></label>
				    <?php echo form_input($input_titulo) ?>
				    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
					<div class="text-danger"><?php echo form_error('no_titulo') ?></div>
				</div>
			</div>
			
			<div class="col-lg-12">
				<div class="form-group">
					<label><?php echo lang('mensaje') ?></label>
				    <?php echo form_textarea($input_mensaje) ?>
				    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
					<div class="text-danger"><?php echo form_error('no_mensaje') ?></div>
				</div>
			</div>
			
			<div class="col-lg-12 text-right">
				<a id="guardar_nota" href="#" class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-save"></i> <?php echo lang('guardar') ?></a>
				
				<a href="<?php echo base_url().$this->lang->lang().'/notas' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> <?php echo lang('atras') ?></a>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//DATOS GLOBALES
	var tipo = '<?php echo $this->session->userdata("emp_tipo") ?>';
	var no_emp_id = '<?php echo isset($no_emp_id)?$no_emp_id:"" ?>';
	var emp_id = '<?php echo $this->session->userdata("emp_id") ?>';
	var su_id = '<?php echo $this->session->userdata("su_id") ?>';
	var de_id = '<?php echo $this->session->userdata("de_id") ?>';
	var depart = false;
	var emple = false;
	var no_tipo = ''
	
	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	$("#notas .form-group, #notas .input-group").removeClass("has-error has-feedback");
	$('#notas .glyphicon-remove').hide();
	$("#notas .text-danger").html('');
	
	//MOSTRAR Y OCULTAR SEGUN EL TIPO
	function datos_nota() {
		no_tipo = $('.radio_line input:checked').val();
		
		if (no_tipo == 0) {
			$('.suc, .dep, .emp').show();
			sucursales();
		} else {
			$('.suc, .dep, .emp').hide();
		}
	}
	
	//CAMBIO DE TIPO DE NOTA
	datos_nota();
	
	$('.radio_line *').click(function(){
		datos_nota();
	});
	
	//RELLENAMOS LOS EMPLEADOS DEL DEPARTAMENTO
	function empleados() {
		$('#empleados').html('');
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/empleados/empleados_departamento",
			{'su_id':$('#sucursales option:selected').val(),'de_id':$('#departamentos option:selected').val()},
			function(datos){
				$('#empleados').append('<option value="" selected><?php echo lang("select.empleado") ?></option>');
				
				$.each(datos,function(indice) {
					var mostrar = false;
					//MOSTRAMOS TODOS LOS DATOS SI ES DIFERENTE DE EMPLEADO O SOLO AL QUE PERTENECE SI NO LO ES
					if (datos[indice]['de_id'] == $('#departamentos option:selected').val()) {
						if (tipo != 3) {
							mostrar = true;
						} else if (emp_id == datos[indice]['emp_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}
					}
					
					if (mostrar) {
						$('#empleados').append('<option value="'+datos[indice]["emp_id"]+'">'+datos[indice]["emp_nombre"]+' '+datos[indice]["emp_apellido1"]+' '+datos[indice]["emp_apellido2"]+'</option>');
					}
				});
			}, "json");
	}
	
	//RELLENAMOS LOS DEPARTAMENTOS DE LA SUCURSAL
	function departamentos() {
		$('#departamentos').html('');
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/departamentos/departamento_sucursal",
			{'su_id':$('#sucursales option:selected').val()},
			function(datos){
				$('#departamentos').append('<option value="" selected><?php echo lang("select.departamento") ?></option>');
				
				$.each(datos,function(indice) {
					var mostrar = false;
					//MOSTRAMOS TODOS LOS DATOS SI ES SUPERUSUARIO,ADMINISTRADOR O GERENTE O SOLO AL QUE PERTENECE SI NO LO ES
					if (datos[indice]['su_id'] == $('#sucursales option:selected').val()) {
						if (tipo != 2 && tipo != 3) {
							mostrar = true;
						} else if (de_id == datos[indice]['de_id']) {
							mostrar = true;
						} else {
							mostrar = false;
						}
					}
					
					if (mostrar) {
						$('#departamentos').append('<option value="'+datos[indice]["de_id"]+'">'+datos[indice]["de_nombre"]+'</option>');
					}
				});
				
				empleados();
				emple = true;
			}, "json");
	}
	
	//RELLENAMOS EL CAMPO DE SUCURSALES PARA A�ADIR O EDITAR
	function sucursales() {
		$.post(
			"<?php echo base_url().$this->lang->lang() ?>/sucursales/buscador",
			{'buscar':''},
			function(data){
				$('#sucursales').append('<option value="" selected><?php echo lang("select.sucursal") ?></option>');
				
				$.each(data,function(indice) {
					
						if (data[indice]['su_estado'] == '1') {
							$('#sucursales').append('<option value="'+data[indice]["su_id"]+'">'+data[indice]["su_nombre"]+'</option>');
						}
				});
				
				departamentos();
				depart = true;
			}, 
		"json");
	}
	
	//CARGAMOS LOS DEPARTAMENTOS SEGUN SUCURSAL
	$("#sucursales").change(function () {
		if (depart) {
			departamentos();
		}	
	}).trigger('change');
	
	//CARGAMOS LOS EMPLEADOS SEGUN DEPARTAMENTO
	$("#departamentos").change(function () {
		if (emple) {
			empleados();
		}	
	}).trigger('change');
	
	//GUARDAR NOTA
	$('#guardar_nota').click(function(e){
		e.preventDefault();
		
		var l = Ladda.create( document.querySelector( "#guardar_nota" ) );
		l.start();
		
		$("#notas .form-group, #notas .input-group").removeClass("has-error has-feedback");
		$('#notas .glyphicon-remove').hide();
		$("#notas .text-danger").html('');
		
		var datos = {
			'no_tipo' : no_tipo,
			'su_id' : (no_tipo == 0)?$('#sucursales option:selected').val():'',
			'de_id' : (no_tipo == 0)?$('#departamentos option:selected').val():'',
			'emp_id' : (no_tipo == 0)?$('#empleados option:selected').val():'',
			'no_titulo' : $('#no_titulo').val(),
			'no_mensaje' : $('#no_mensaje').val(),
		};
		
		$.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/notas/guardar_editar",
		  	data: datos,
		  	success: function(data) {
		  		if (data.sql) {					
					//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
		  			if (data.status) {
		  				//TODO CORRECTO
							$('#notas input, #notas textarea').val('');
							bootbox.dialog({
								message: "<?php echo lang('nota.nuevo.ok') ?>",
								buttons: {
									success: {
										label: "<?php echo lang('nuevo') ?>",
										className: "btn-success",
										callback: function(result) {
											var url = '<?php echo base_url().$this->lang->lang() ?>/notas/nueva_nota';
											$(location).attr('href',url);
										}
									},
									main: {
										label: "<?php echo lang('notas') ?>",
										className: "btn-primary",
										callback: function(result) {
											var url = '<?php echo base_url().$this->lang->lang() ?>/notas';
											$(location).attr('href',url);
										}
									}
								}
							});
		  			} else {
						//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
						//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
						if (data.errors.length > 0) {
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
									$('input[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('input[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
									
									$('textarea[name='+data.errors[ind][0]+']').parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('textarea[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
									$('textarea[name='+data.errors[ind][0]+']').parent().find('.glyphicon-remove').show();
								}); 
							});
						}
					}
		  		} else {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
				}
				l.stop();
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
});
</script>

<?php } else { ?>
<!-- EDITAR NOTA -->

<div class="box box-success" id="nota">
	<div class="box-header">
    	<i class="fa fa-file-text"></i>
        <h3 class="box-title"><?php echo $no_titulo ?></h3>
    </div>
    
    <div>
    	<div class="box-body chat" id="chat-box">
        	<!-- chat item -->
        	<div class="item">
            	<img src="<?php echo base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif").'/emp/'.$emp_dni.'/perfil.jpg' ?>" class="online">
                
                <p class="message">
                	<a href="#" class="name" style="cursor:default;">
                    	<small class="text-muted pull-right">
                    		<i class="fa fa-clock-o"></i> <?php echo $no_fecha ?>
                    	</small>
                        <?php echo $emp_nombre.' '.$emp_apellido1.' '.$emp_apellido2 ?>
                    </a>
                    
                    <?php echo $no_mensaje ?>
                </p>

            </div>
		</div>
	</div>
	
	<div class="box-footer">
    	<div class="input-group">
        	<input name="nr_respuesta" id="nr_respuesta" class="form-control" placeholder="<?php echo lang('nota.respuesta') ?>">
        	
        	<div class="input-group-btn">
            	<button class="btn btn-success ladda-button" data-style="zoom-out"><i class="fa fa-plus"></i> <?php echo lang('enviar') ?></button>
        	</div>
        </div>
        
		<div class="text-danger"><?php echo form_error('su_iva_superreducido') ?></div>
    </div>
</div>

<div id="respuestas"></div>


<script>
$(document).ready(function(){
	//ELIMINAMOS CLASES DE ERROR, OCULTAMOS EL CAMPO ERROR Y LO LIMPIAMOS
	$("#nota .input-group").removeClass("has-error has-feedback");
	$('#nota .glyphicon-remove').hide();
	$("#nota .text-danger").html('');
	
	//RESPONDER NOTA
	$('#nota button').click(function(e){
		e.preventDefault();
		
		//LOADING BOTON
		var l = Ladda.create( document.querySelector( "#nota button" ) );
	 	l.start();
	 	
	 	$("#nota .input-group").removeClass("has-error has-feedback");
		$('#nota .glyphicon-remove').hide();
		$("#nota .text-danger").html('');
	 	
	 	$.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/notas/responder",
		  	data: 'no_id=<?php echo $no_id ?>&nr_respuesta='+$("#nr_respuesta").val(),
		  	success: function(data) {
		  		if (data.sql) {					
					//PASA Y COMPRUEBA LOS ERRORES O DATOS CORRECTOS
		  			if (data.status) {
		  				//TODO CORRECTO
							$('#nota input').val('');
							bootbox.alert('<?php echo lang("nota.respuesta.ok") ?>');
							respuestas();
		  			} else {
						//RECORREMOS LOS INPUT MARCANDO LOS ERRORES Y SUS MENSAJES
						//RECORREMOS EL ARRAY DE ARRAYS RECIBIDO DEL CONTROLADOR
						if (data.errors.length > 0) {
							$.each(data.errors, function (ind, elem) {
								$.each(data.errors[ind], function (ind2, elem2) {
									//MUESTRAS LOS ERRORES MENOS EL DE LA FOTO
									$('input[name='+data.errors[ind][0]+']').parent().parent().find('.text-danger').html(data.errors[ind][ind2]);
									$('input[name='+data.errors[ind][0]+']').parent().addClass("has-error has-feedback");
								}); 
							});
						}
					}
		  		} else {
					bootbox.alert('<?php echo lang("error.ajax") ?>');
				}
				l.stop();
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		   		l.stop();
		  	}
		});
	});
	
	//MOSTRAR RESPUESTAS
	function respuestas() {
		$.ajax({
			type: "POST",
			dataType: "json",
		  	url: "<?php echo base_url().$this->lang->lang() ?>/notas/ver_respuestas",
		  	data: 'no_id=<?php echo $no_id ?>',
		  	success: function(data) {
		  		var aux = '';
		  		
		  		if (data.length > 0) {
					aux = '<h4><?php echo lang("respuestas") ?></h4>';
				}
		  		
		  		$.each(data, function (ind, elem) {		  			
					aux = aux + '<div class="box"><div><div class="box-header"></div><div class="box-body chat" id="chat-box"><div class="item"><img src="<?php echo base_url().$this->lang->lang().'/empresa/view_file/'.$this->session->userdata("em_cif") ?>/emp/'+data[ind]["emp_dni"]+'/perfil.jpg" alt="user image" class="online"><p class="message"><a href="#" class="name" style="cursor:default;"><small class="text-muted pull-right"><i class="fa fa-clock-o"></i> '+data[ind]["nr_fecha"]+'</small>'+data[ind]["emp_nombre"]+' '+data[ind]["emp_apellido1"]+' '+data[ind]["emp_apellido2"]+'</a>'+data[ind]["nr_respuesta"]+'</p></div></div></div></div>';
				}); 
		  		
		  		$('#respuestas').html(aux);
		  	},
		  	error: function(XMLHttpRequest, textStatus, errorThrown) {
		   		bootbox.alert('<?php echo lang("error.ajax") ?>');
		  	}
		});
	}
	
	respuestas();
	
	//MARCAR LA NOTA COMO VISTA PASADOS 5 SEGUNDOS
	setTimeout(function() {
    	$.post(
		    "<?php echo base_url().$this->lang->lang() ?>/notas/nota_vista",
		    {'no_id':'<?php echo $no_id ?>'},
		    function(data){
		    	$.notas_no_leidas();
		    }
		);
	}, 5000);
});
</script>

<?php } ?>