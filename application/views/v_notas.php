<div id="listar" class="bottom30">
	<div class="box-body table-responsive">
		<div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<table aria-describedby="example1_info" id="example1" class="table table-bordered table-striped table-hover dataTable">
            	<thead>
                	<tr>
                		<th><?php echo lang("titulo") ?></th>
                		<th><?php echo lang("respuestas") ?></th>
                		<th><?php echo lang("fecha") ?></th>
                	</tr>
				</thead>
                                        
				<tfoot>
                	<tr>
                		<th><?php echo lang("titulo") ?></th>
                		<th><?php echo lang("respuestas") ?></th>
                		<th><?php echo lang("fecha") ?></th>
                	</tr>
				</tfoot>
				
				<tbody aria-relevant="all" aria-live="polite" role="alert">
					
				</tbody>
			</table>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	//VARIABLES DE SESSION
	var emp_id = '<?php echo $this->session->userdata("emp_id") ?>';
	
	//MOSTRAR SUCURSALES
	function mostrar_notas() {
	    //INICIO PETICION AJAX
	    $.post(
		    "<?php echo base_url().$this->lang->lang() ?>/notas/buscador",
		    {'buscar':''},
		    function(data){
			    
			    if (data != null) {
			    	//CREAMOS LA TABLA
			    	//RECORREMOS ARRAY DE SUCURSALES GENERANDO LAS FILAS
					var table = '';
					$.each(data,function(indice,valor) {
							table = table + '<tr id="'+data[indice]['no_id']+'">';
	  						table = table + '<td>'+data[indice]['no_titulo']+'</td>';
	  						table = table + '<td>'+data[indice]['respuestas_cantidad']+'</td>';
	  						table = table + '<td>'+data[indice]['no_fecha']+'</td>';
	  						table = table + '</tr>';
						
					});
					$('#listar tbody').html(table);
					$("#example1").dataTable({
						"aaSorting": [[ 2, "desc"]],
						"oLanguage": {
						  	"sInfo": "<?php echo lang('viendo') ?> _START_ <?php echo lang('a') ?> _END_ <?php echo lang('de') ?> _TOTAL_ <?php echo lang('registros') ?>",
						  	"oPaginate": {
				            	"sPrevious": "",
				            	"sNext":""
				           	},
				           	"sRefresh": "<?php echo lang('refrescar') ?>",
				           	"sNuevo": "<i class='fa fa-file-text'></i> <?php echo lang('nuevo') ?>",
				           	"sLengthMenu": '<select class="form-control">'+
			             		'<option value="10">10</option>'+
			             		'<option value="20">20</option>'+
			             		'<option value="30">30</option>'+
			             		'<option value="40">40</option>'+
			             		'<option value="50">50</option>'+
			             		'<option value="-1">All</option>'+
			             		'</select> <?php echo lang("por.pagina") ?>'
						}
					});
					
					//NUEVA NOTA
					$("#btn_nuevo").click(function(e){
					    e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/notas/nueva_nota');
					});
					
					//REFRESCAR
					$("#btn_refrecar").click(function(e){
					    e.preventDefault();
					 	window.location.reload(true); 
					});
			        
			        //EDITAR O VER NOTA
			        $("#example1").on("mouseover","tbody tr",function(event) {
						$(this).find('td').addClass("fila_tabla");
					});
				   
				   	$("#example1").on("mouseout","tbody tr",function(event) {
						$(this).find('td').removeClass("fila_tabla");
					});
					
					$("#example1").on("click", "tbody tr", function(e){
						e.preventDefault();
						$(location).attr('href','<?php echo base_url().$this->lang->lang() ?>/notas/ver_nota/'+$(this).attr('id'));
					});
				}
		    }, "json");
	}
	
	mostrar_notas();
});
</script>