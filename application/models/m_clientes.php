<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_clientes extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'clientes';
    }
    
    //DEVUELVE LOS clientes O NULL SI NO ENCUENTRA
	public function get_clientes() {
		$clientes = $this->db->from($this->tabla)
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = clientes.cl_pais')
			->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();

		return $clientes->result();
    }
    
    //DEVUELVE EL CLIENTE SEGUN LA SUCURSAL O NULL SI NO ENCUENTRA
	public function get_clientes_sucursal($su_id) {
		$clientes = $this->db->from($this->tabla)
			->where('clientes.em_id',$this->session->userdata('em_id'))
			->where('clientes.su_id',$su_id)
			->join('countries', 'countries.id_countries = clientes.cl_pais')
			->get();

		return $clientes->result();
    }
    
    //DEVUELVE LA FILA DEL CLIENTE SEGUN SU EMAIL, O NULL SI NO EXISTE
	public function get_cliente_email($ident) {
        $cliente = $this->db->from($this->tabla)
			->where('clientes.cl_email',$ident)
			->join('empresas', 'empresas.em_id = clientes.em_id')
			->get()->row();
		return $cliente;
    }
    
    //DEVUELVE LOS DATOS DEL CLIENTE SEGUN SU CL_ID
	public function get_cliente($cl_id) {	
		$cliente = $this->db->from($this->tabla)
			->where('cl_id',$cl_id)
			->where('clientes.em_id',$this->session->userdata('em_id'))
			->join('empresas', 'empresas.em_id = clientes.em_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = clientes.cl_pais')
			->get()->row();
			
		return $cliente;
	}
	
	//DEVUELVE LOS DATOS DEL CLIENTE SEGUN SU DNI
	public function get_cliente_dni($cl_dni) {	
		$cliente = $this->db->from($this->tabla)
			->where('cl_dni',$cl_dni)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
			
		return $cliente;
	}
	
	//AÑADIMOS O EDITAMOS EL CLIENTE
	public function set_cliente($cliente) {
		$cliente['em_id'] = $this->session->userdata('em_id');
		
		$cliente['cl_dni'] = strtoupper($cliente['cl_dni']);
		$cliente['cl_dni'] = str_replace(' ', "-", $cliente['cl_dni']);
		$ruta = PRIVADO.$this->session->userdata('em_cif').'/cl/'.$cliente['cl_dni'];
		
		unset($cliente['foto']);
		//NUEVO CLIENTE
		if ($cliente['cl_id'] == '') {
			unset($cliente['cl_id']);
			$this->load->library('encrypt');
			$clave = rand(100000, 999999);
			$cliente['cl_clave'] = $this->encrypt->encode($clave);
			
			//SACAMOS LA ZONA HORARIA DE LA SUCURSAL
			$sucursal = $this->db->from('sucursales')
			->where('su_id',$cliente['su_id'])
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
			
			$cliente['cl_registro'] = fechaHora_actual($sucursal->su_zona_horaria);
			$this->db->insert($this->tabla, $cliente);
			$id = $this->db->insert_id();
			
			//PREGUNTAMOS SI SE INSERTO BIEN
			if ($this->db->trans_status()) {
				//SE CREA LA CARPETA DEL CLIENTE Y SE COPIA LA FOTO DEL PERFIL POR DEFECTO
				mkdir($ruta, 0755);
				copy(PRIVADO.'index.html', $ruta.'/index.html');
				if (empty($_FILES['foto'])) {
					copy(PRIVADO.'perfil.jpg', $ruta.'/perfil.jpg');
				}
			}
		//EDITAR CLIENTE
		} else {
			$this->db->where('cl_id', $cliente['cl_id']);
			$this->db->update($this->tabla, $cliente);
			$id = $cliente['cl_id'];
		}
		
		if ($this->db->trans_status()) {
			if (!empty($_FILES['foto'])) {
				$name2 = $_FILES['foto']['name']; //get the name of the image
				list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
				$actual_image_name2 = "/perfil.".$ext; //actual image name going to store in your folder
				$tmp2 = $_FILES['foto']['tmp_name']; //get the temporary uploaded image name
				move_uploaded_file($tmp2, $ruta.$actual_image_name2); //move the file to the folder
				
				$config['image_library'] = 'GD2';
				$config['source_image'] = $ruta.'/perfil.jpg';
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['master_dim'] = 'auto';
				$config['width'] = 378;
				$config['height'] = 378;
				$config['quality'] = 90;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();	
			}
		}

		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
		}
	}
	
	//RESTAURAR CLAVE DEL EMPLEADO
	function set_recordar_clave($cliente) {
			//GENERAMOS EL EMAIL
			$this->load->library('email');
			$nombre = "=?UTF-8?B?".base64_encode($cliente->cl_nombre)."=?=";
			$this->email->from(EMAIL, $nombre);
			$this->email->to($cliente->cl_email);
			$this->email->subject(lang('recordar.clave.clientes'));
			
			$this->load->library('encrypt');
			
			$datos = array(
				'cl_clave' => $this->encrypt->decode($cliente->cl_clave),
				'cl_email' => $cliente->cl_email
			);
				
			$data = array(
				'BODY' => $this->load->view('emails/v_email_clientes_recordar_clave', $datos, true),
				'em_nombre' => $cliente->em_nombre
			);
			$email = $this->load->view('emails/v_email', $data, TRUE);
			$this->email->message($email);
				    	
			$this->email->send();
		
		return true;
	}
	
	/*
	FIN CLIENTES DE LA APLICACION
	INICIO INFORMACION DE CLIENTES
	*/
	
	//DEVUELVE LA FILA DEL CLIENTE SEGUN SU EMAIL, O NULL SI NO EXISTE
	public function get_cliente_mantenimiento_email($ident) {
        $cliente = $this->db->from($this->tabla)
        	->join('empresas', 'empresas.em_id = clientes.em_id')
			->join('mantenimientos', 'mantenimientos.cl_id = clientes.cl_id', 'left')
			->where('clientes.cl_email',$ident)
			->or_where('mantenimientos.man_email',$ident)
			->get()->row();
		return $cliente;
    }
}

/* End of file M_clientes.php */
/* Location: ./application/controllers/M_clientes.php */