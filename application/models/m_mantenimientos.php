<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_mantenimientos extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'mantenimientos';
    }
    
    //DEVUELVE LOS MANTENIMIENTOA O NULL SI NO ENCUENTRA
	public function get_mantenimientos() {
		$presupuestos = $this->db->from($this->tabla)
			->join('clientes', 'clientes.cl_id = mantenimientos.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->where('mantenimientos.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();
		
		return $presupuestos->result();
    }
    
    //DEVUELVE LOS DATOS DEL MANTENIMIENTO
	public function get_mantenimiento($man_id) {	
		$mantenimiento = $this->db->from($this->tabla)
			->where('mantenimientos.man_id',$man_id)
			->where('mantenimientos.em_id',$this->session->userdata('em_id'))
			->join('clientes', 'clientes.cl_id = mantenimientos.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = mantenimientos.man_pais')
			->get()->row();
			
		return $mantenimiento;
	}
	
	//DEVUELVE LOS DATOS DEL MANTENIMIENTO PARA EL CLIENTE
	public function get_mantenimiento_cliente($man_id) {	
		$mantenimiento = $this->db->from($this->tabla)
			->where('mantenimientos.man_id',$man_id)
			->join('clientes', 'clientes.cl_id = mantenimientos.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('empresas', 'empresas.em_id = mantenimientos.em_id')
			//->join('countries', 'countries.id_countries = sucursales.su_pais')
			->get()->row();
			
		return $mantenimiento;
	}
    
    //NUMERO ALEATORIO PARA NUEVO MANTENIMIENTO
    public function numero_aleatorio($cl_su_id,$cl_id) {
		$numero_mantenimiento = 'M'.$cl_su_id.$cl_id.rand(100, 9999);
		
		//RECUPERAMOS EL MANTENIMIENTO
		$mantenimiento = $this->db->from($this->tabla)
			->where('em_id',$this->session->userdata('em_id'))
			->where('man_id',$numero_mantenimiento)
			->get()->row();
		
		//PREGUNTAMOS SI ENCONTRO UN MANTENIMIENTO CON EL MISMO NUMERO
		if ($mantenimiento != null) {
			$this->numero_aleatorio($cl_su_id,$cl_id);
		} else {
			return $numero_mantenimiento;
		}
	}
	
	//DEVUELVE LOS LOS MANTENIMIENTOS SEGUN SU CLIENTE
	public function get_mantenimientos_cliente($cl_id) {
		//MANTENIMIENTOS PARA EL PANEL DE CLIENTES
		if ($this->session->userdata('cl_tipo') == 1) {
			$mantenimientos = $this->db->from($this->tabla)
			->select('mantenimientos.*,clientes.*')
			->join('clientes', 'clientes.cl_id = mantenimientos.cl_id')
			->where('mantenimientos.man_email',$this->session->userdata('cliente'))
			->where('mantenimientos.em_id',$this->session->userdata('em_id'))
			->get();
		} else {
			//MANTENIMIENTOS PARA LOS EMPLEADOS Y EL PANEL DE CLIENTES SI ES UN SUPER CLIENTE
			$mantenimientos = $this->db->from($this->tabla)
			->select('mantenimientos.*,clientes.*')
			->join('clientes', 'clientes.cl_id = mantenimientos.cl_id')
			->where('clientes.cl_id',$cl_id)
			->where('mantenimientos.em_id',$this->session->userdata('em_id'))
			->get();
		}
		
		return $mantenimientos->result();
    }
    
    //GUARDAMOS EL MANTENIMIENTO
    public function set_mantenimiento($mantenimiento) {
		$mantenimiento['em_id'] = $this->session->userdata('em_id');
		//CLAVE MANTENIMIENTO
		$this->load->library('encrypt');
		$clave = rand(100000, 999999);
		$cliente['man_clave'] = $this->encrypt->encode($clave);
		
		//PREGUNTAMOS SI ES UN NUEVO MANTENIMIENTO
		if ($mantenimiento['man_id'] == '') {
			$this->db->trans_begin();
			
			unset($mantenimiento['man_id']);
			
			//RECUPERAMOS EL CLIENTE
			$cliente = $this->db->query('SELECT * FROM clientes WHERE em_id='.$this->session->userdata("em_id").' AND cl_id='.$mantenimiento['cl_id']);
			
			foreach ($cliente->result() as $row) {
	   			$cl_su_id = $row->su_id;
	   			$cl_email = $row->cl_email;
	   			$cl_id = $row->cl_id;
			}
			
			//GENERAMOS EL NUMERO ALEATORIO
			$mantenimiento['man_id'] = '';
			while($mantenimiento['man_id'] == null || $mantenimiento['man_id'] == ''){
				$mantenimiento['man_id'] = $this->numero_aleatorio($cl_su_id,$cl_id);
			}
			$id = $mantenimiento['man_id'];
			$mantenimiento['man_registro'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			$this->db->insert($this->tabla, $mantenimiento);
			
		} else {
			$this->db->where('man_id', $mantenimiento['man_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($this->tabla, $mantenimiento);
			$id = $mantenimiento['man_id'];
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
		   	$this->db->trans_rollback();
		   	return false;
		} else {
			$this->db->trans_commit();
			return $id;
			/*$this->load->library('email');
			$nombre = "=?UTF-8?B?".base64_encode($this->session->userdata('em_nombre'))."=?=";
			$this->email->from(EMAIL, $nombre);
			$this->email->subject(lang('nuevo.mantenimiento'));
			$datos = array (
				'man_id'	=>	$mantenimiento['man_id'],
				'clave'		=>	$clave
			);
			$data = array(
				'BODY' => $this->load->view('emails/v_email_nuevo_mantenimiento',$datos, TRUE)
			);
			$email = $this->load->view('emails/v_email', $data, TRUE);
			$this->email->message($email);
			$emails = $mantenimiento['man_email'].','.$cl_email;
			$this->email->to($emails);
			$this->email->send();*/
		}
	}
	
	//RESTAURAR CLAVE DEL EMPLEADO
	function set_recordar_clave($cliente) {
			//GENERAMOS EL EMAIL
			$this->load->library('email');
			$nombre = "=?UTF-8?B?".base64_encode($cliente->cl_nombre)."=?=";
			$this->email->from($EMAIL, $nombre);
			$this->email->to($cliente->man_email);
			$this->email->subject(lang('recordar.clave.clientes'));
			
			$this->load->library('encrypt');
			
			$datos = array(
				'clave' => $this->encrypt->decode($cliente->man_clave),
				'usuario' => $cliente->man_email
			);
				
			$data = array(
				'BODY' => $this->load->view('emails/v_email_clientes_recordar_clave', $datos, true),
				'em_nombre' => $cliente->em_nombre
			);
			$email = $this->load->view('emails/v_email', $data, TRUE);
			$this->email->message($email);
				    	
			$this->email->send();
		
		return true;
	}
}

/* End of file M_mantenimientos.php */
/* Location: ./application/controllers/M_mantenimientos.php */