<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_facturas extends MY_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'facturas';
    }
    
    //DEVUELVE LAS facturas O NULL SI NO ENCUENTRA
	public function get_facturas() {		
		$facturas = $this->db->from($this->tabla)
			->select('facturas.*,rectificativas.re_numero,rectificativas.re_total,clientes.*,sucursales.*,countries.*, mantenimientos.*')
			->join('rectificativas', 'facturas.fa_id = rectificativas.fa_id', 'left')
			->join('clientes', 'clientes.cl_id = facturas.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
            ->join('mantenimientos', 'mantenimientos.man_id = facturas.man_id')
			->where('facturas.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->distinct()
			->get();			
		
		
		return $facturas->result();
    }

    public function get_facturas_anuladas() {
		$facturas = $this->db->from($this->tabla)
			->select('facturas.*,rectificativas.re_numero,rectificativas.re_total,clientes.*,sucursales.*,countries.*, mantenimientos.*')
			->join('rectificativas', 'facturas.fa_id = rectificativas.fa_id', 'left')
			->join('clientes', 'clientes.cl_id = facturas.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
            ->join('mantenimientos', 'mantenimientos.man_id = facturas.man_id')
			->where('facturas.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
            ->where('facturas.fa_estado',4)
			->distinct()
			->get();
		
		return $facturas->result();
    }
    
    //OBTENEER FACTURAS DEL MANTENIMIENTO
    public function get_facturas_presupuesto($pr_id) {
		$facturas = $this->db->from($this->tabla)
			->where('pr_id',$pr_id)
	        ->where('em_id',$this->session->userdata('em_id'))
	        ->get()->result();
        
        return $facturas;
	}
	
	//OBTENEER RECTIFICATIVAS DEL MANTENIMIENTO
    public function get_rectificativas_presupuesto($pr_id) {
		$rectificativas = $this->db->from('rectificativas')
			->join('facturas','facturas.fa_id = rectificativas.fa_id')
			->where('facturas.pr_id',$pr_id)
	        ->where('rectificativas.em_id',$this->session->userdata('em_id'))
	        ->get()->result();
        
        return $rectificativas;
	}
    
    //DEVUELVE LOS DATOS DE LA FACTURA SEGUN SU FA_ID
	public function get_factura($fa_id) {
		$factura = $this->db->from($this->tabla)
			->select('facturas.*,sucursales.*,countries.*,empresas.*,clientes.*, empleados.emp_nombre, empleados.emp_apellido1, empleados.emp_apellido2, mantenimientos.man_nombre')
			->join('clientes', 'clientes.cl_id = facturas.cl_id')
			->join('empleados', 'empleados.emp_id = facturas.emp_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
			->join('empresas', 'empresas.em_id = facturas.em_id')
                        ->join('mantenimientos', 'mantenimientos.man_id = facturas.man_id')
			->where('facturas.fa_id',$fa_id)	
			->where('facturas.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $factura;
	}
	
	//DEVUELVE LOS DATOS DE LA FACTURA RECTIFICATIVA SEGUN SU FA_ID
	public function get_rectificativa($fa_id) {
		$rectificativa = $this->db->from('rectificativas')
			->join('facturas', 'facturas.fa_id = rectificativas.fa_id')
			->join('clientes', 'clientes.cl_id = facturas.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
			->join('empresas', 'empresas.em_id = rectificativas.em_id')
                        ->join('mantenimientos', 'mantenimientos.man_id = facturas.man_id')
			->where('rectificativas.fa_id',$fa_id)	
			->where('rectificativas.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $rectificativa;
	}
	
	//DEVUELVE LOS DATOS DE LA FACTURA SEGUN SU PRESUPUESTO
	public function get_factura_presupuesto($pr_id) {	
		$factura = $this->db->from($this->tabla)
			->select('fa_id')
			->where('pr_id',$pr_id)	
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $factura;
	}
	
	//GUARDAMOS LOS DATOS DE LA FACTURA
	public function set_factura($factura) {		
		if ($factura['fa_id'] == '') {
			//APLICAMOS LOS DATOS A LA FACTURA
			$factura['em_id'] = $this->session->userdata('em_id');
			$factura['fa_fecha'] = fechaHora_actual($factura['su_zona_horaria']);
			$factura['fa_estado'] = '0';
			$factura['emp_id'] = $this->session->userdata('emp_id');
			unset($factura['su_zona_horaria']);
			unset($factura['su_id']);
			unset($factura['fa_id']);
			unset($factura['re_numero']);
			$this->db->insert($this->tabla, $factura);
			$id = $this->db->insert_id();
		} else {
			unset($factura['su_id']);
			unset($factura['cl_id']);
			unset($factura['su_zona_horaria']);
			unset($factura['re_numero']);
			$this->db->where('fa_id', $factura['fa_id']);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update($this->tabla, $factura);
			$id = $factura['fa_id'];
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
		}
	}
	
	//COPIAMOS LOS DATOS DE LA FACTURA A LA FACTURA RECTIFICATIVA
	public function set_rectificar_factura($factura) {		
		$factura['em_id'] = $this->session->userdata('em_id');
		
		//SI ENCUENTRA NUMERO ES UNA EDICION DE LA FACTURA RECTIFICATIVA
		if ($factura['re_numero'] != '') {
			$this->db->where('fa_id', $factura['fa_id']);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update('rectificativas', $factura);
			$id = $factura['re_numero'];
		} else {
			//NUEVA FACTURA RECTIFICATIVA
			//USAMOS CONSULTAS TRANSACIONALES PARA EJECUTAR VARIAS A LA VEZ
			$this->db->trans_begin();
                        
                        //COMPROBAMOS AÑO Y CONTADORES
                        $this->contadores();
                        
			//RECUPERAMOS EL CONTADOR DE LA FACTURA RECTIFICATIVA
			$contador = $this->db->query('SELECT * FROM contadores WHERE em_id='.$this->session->userdata("em_id").' AND co_tipo=1 AND su_id='.$factura['su_id']);
			
			foreach ($contador->result() as $row) {
	   			$numero = $row->co_contador + 1;
	   			$anio = $row->co_year;
			}
			
			//SUMAMOS 1 AL CONTADOR
			$this->db->query('UPDATE contadores SET co_contador = '.$numero.' WHERE em_id='.$this->session->userdata("em_id").' AND co_tipo=1 AND su_id='.$factura["su_id"]);
			
			$factura['re_numero'] = 'R'.$factura['su_id'].$anio.$numero;
			$factura['re_fecha'] = fechaHora_actual($factura['su_zona_horaria']);
			$factura['re_servicio'] = $factura['fa_servicio'];
			$factura['re_cantidad'] = $factura['fa_cantidad'];
			$factura['re_precio'] = $factura['fa_precio'];
			$factura['re_descuento'] = $factura['fa_descuento'];
			$factura['re_iva'] = $factura['fa_iva'];
			$factura['re_total'] = $factura['fa_total'];
			$factura['re_ganancia_total'] = $factura['fa_ganancia_total'];
			$factura['re_ganancia'] = $factura['fa_ganancia'];
			$factura['re_costos'] = $factura['fa_costos'];
			
			unset($factura['su_id']);
			unset($factura['su_zona_horaria']);
			unset($factura['fa_servicio']);
			unset($factura['fa_cantidad']);
			unset($factura['fa_precio']);
			unset($factura['fa_descuento']);
			unset($factura['fa_iva']);
			unset($factura['fa_total']);
			unset($factura['fa_ganancia_total']);
			unset($factura['fa_ganancia']);
			unset($factura['fa_numero']);
			unset($factura['cl_id']);
			unset($factura['fa_costos']);
			
			$this->db->insert('rectificativas', $factura);
			$id = $factura['re_numero'];
		}

		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
		}
	}
	
	//CERRAMOS LA FACTURA
	public function set_cerrar_factura($factura) {
		//USAMOS CONSULTAS TRANSACIONALES PARA EJECUTAR VARIAS A LA VEZ
		$this->db->trans_begin();
		
                //COMPROBAMOS AÑO Y CONTADORES
                $this->contadores();
                
		//RECUPERAMOS EL CONTADOR DE LA FACTURA
		$contador = $this->db->query('SELECT * FROM contadores WHERE em_id='.$this->session->userdata("em_id").' AND co_tipo=0 AND su_id='.$factura['su_id']);
		
		foreach ($contador->result() as $row) {
   			$numero = $row->co_contador + 1;
   			$anio = $row->co_year;
		}
		$numero_factura = 'F'.$factura['su_id'].$anio.$numero;
		
		//SUMAMOS 1 AL CONTADOR
		$this->db->query('UPDATE contadores SET co_contador = '.$numero.' WHERE em_id='.$this->session->userdata("em_id").' AND co_tipo=0 AND su_id='.$factura["su_id"]);
		
		//APLICAMOS LOS DATOS A LA FACTURA
		/*$this->db->query('UPDATE '.$this->tabla.' SET fa_numero = "'.$numero_factura.'",fa_estado=1 WHERE fa_id='.$factura["fa_id"].' AND em_id='.$this->session->userdata("em_id"));*/
		unset($factura['su_id']);
		unset($factura['cl_id']);
		unset($factura['re_numero']);
		unset($factura['su_zona_horaria']);
		
		$factura['fa_numero'] = $numero_factura;
		$factura['fa_estado'] = 1;
		
		$this->db->where('fa_id', $factura['fa_id']);
		$this->db->where('em_id',$this->session->userdata('em_id'));
		$this->db->update($this->tabla, $factura);
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
    		$this->db->trans_rollback();
		} else {
    		$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
}

/* End of file M_facturas.php */
/* Location: ./application/controllers/M_facturas.php */