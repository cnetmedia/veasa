<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_trabajos extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'trabajos';
    }
    
    //DEVUELVE TODOS LOS TRABAJOS
	public function get_trabajos() {		
		$trabajos = $this->db->from($this->tabla)
			->join('sucursales', 'sucursales.su_id = trabajos.su_id')
			->where('trabajos.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();
			
		return $trabajos->result();
	}
	
	//DEVUELVE LOS TRABAJOS QUE NO ESTEN COMPLETADOS
	public function get_trabajos_pendientes() {		
		$trabajos = $this->db->from($this->tabla)
			->join('sucursales', 'sucursales.su_id = trabajos.su_id')
			->where_in('trabajos.tr_estado', array(0))
			->where('trabajos.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();
			
		return $trabajos->result();
	}
	
	//DEVUELVE LOS TRABAJOS QUE ESTEN EN PROCESADO O COMPLETADO
	public function get_trabajos_completados() {		
		$trabajos = $this->db->from($this->tabla)
			->join('sucursales', 'sucursales.su_id = trabajos.su_id')
			->where_not_in('trabajos.tr_estado', array(0,2))
			->where('trabajos.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();
			
		return $trabajos->result();
	}
	
	//DEVUELVE LOS DATOS DEL TRABAJO SI SU TR_ID
	public function get_trabajo($tr_id) {	
		$trabajo = $this->db->from($this->tabla)
			->where('tr_id',$tr_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $trabajo;
	}
	
	//DEVUELVE LOS TRABAJOS SEGUN SU PRESUPUESTO
	public function get_trabajos_presupuesto($pr_id) {	
		$trabajos = $this->db->from($this->tabla)
			->like('pr_id',$pr_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get()->result();
			
		return $trabajos;
	}
	
	//DEVUELVE LAS INSPECCIONES DEL TRABAJO
	function get_trabajos_inspecciones($tr_id) {
		$inspecciones = $this->db->from('trabajos_inspecciones')
			->where('tr_id',$tr_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
			
		return $inspecciones->result();
	}
	
	//DEVUELVE LAS INSPECCIONES AVANZADAS DEL TRABAJO
	function get_trabajos_inspecciones_avanzadas($tr_id) {
		$inspecciones = $this->db->from('trabajos_inspecciones_avanzadas')
			->where('tr_id',$tr_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
			
		return $inspecciones->result();
	}
	
	//GUARDAR TRABAJO
	function set_trabajo($trabajo,$fechas=null) {
		$trabajo['em_id'] = $this->session->userdata('em_id');
		
		//STRING MANTENIMIENTOS
		if (isset($trabajo['man_id'])) {
			$mant = '';
			for ($i = 0; $i < count($trabajo['man_id']); $i++) {
				for ($x = 0; $x < count($trabajo['man_id'][$i]); $x++) {
					$mant = $mant.$trabajo['man_id'][$i][$x].',';
				}
				$mant = substr($mant, 0, -1);
				$mant = $mant.';';
			}
			$trabajo['man_id'] = $mant;
		}
		
		
		//STRING DE LOS EMPLEADOS
		if (isset($trabajo['emp_id'])) {
			$emple = '';
			for ($i = 0; $i < count($trabajo['emp_id']); $i++) {
				$emple = $emple.$trabajo['emp_id'][$i].',';
			}
			$trabajo['emp_id'] = $emple;
		}
		
		
		//STRING DE LAS INSPECCIONES
		if (isset($trabajo['in_id'])) {
			$inspec = '';
			for ($i = 0; $i < count($trabajo['in_id']); $i++) {
				$inspec = $inspec.$trabajo['in_id'][$i].',';
			}
			$trabajo['in_id'] = $inspec;
		}
		
		//STRING DE LAS INSPECCIONES AVANZADAS
		if (isset($trabajo['inv_id'])) {
			$inspec_avanz = '';
			for ($i = 0; $i < count($trabajo['inv_id']); $i++) {
				$inspec_avanz = $inspec_avanz.$trabajo['inv_id'][$i].',';
			}
			$trabajo['inv_id'] = $inspec_avanz;
		}
		
        if ($trabajo['tr_id'] == '') {
        	$tr_repetir = $trabajo['tr_repetir'];
            $tr_repetir_cantidad = $trabajo['tr_repetir_cantidad']; 
        }
                
        unset($trabajo['tr_repetir']);
        unset($trabajo['tr_repetir_cantidad']);
		
		//NUEVO TRABAJO
		if ($trabajo['tr_id'] == '') {
			unset($trabajo['tr_id']);
			$trabajo['tr_estado'] = 0;
			
			for ($f=0; $f<count($fechas); $f++) {
				$trabajo['tr_fecha_inicio'] = $fechas[$f][0];
				$trabajo['tr_fecha_fin'] = $fechas[$f][1];
				$this->db->insert($this->tabla, $trabajo);
				$id = $this->db->insert_id();
				//SE GUARDA LA BITACORA
				$trabajo['tr_id'] = $id;
				$trabajo['accion'] = 'Crear';
				$this->set_bitacora_trabajo($trabajo);
			}
		} else {
			//EDITAR TRABAJO
			$this->db->where('tr_id', $trabajo['tr_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($this->tabla, $trabajo);
			//SE GUARDA LA BITACORA
			$trabajo['accion'] = 'Editar';
			$this->set_bitacora_trabajo($trabajo);
		}
		
		return $this->db->trans_status();
	}
	
	//GUARDAR TRABAJO CON SUS INSPECCIONES
	function set_trabajo_inspeccion($trabajo) {
		$this->db->trans_begin();
		
		unset($trabajo['archivos']);
		
		//BUSCA LA INSPECCION DEL MANTENIMIENTO
		$trab_inpec = $this->db->from('trabajos_inspecciones')
			->where('tr_id',$trabajo['tr_id'])
			->where('in_id',$trabajo['in_id'])
			->where('man_id',$trabajo['man_id'])
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
		
		//SI EXISTE ACTUALIZAMOS
		if ($trab_inpec != null) {
			
			//PREGUNTAMOS SI CIERRA LA INSPECCION
			if ($trabajo['ti_estado'] == 2) {
				$trabajo['ti_fecha_fin'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			}
			
			$this->db->where('tr_id', $trabajo['tr_id']);
			$this->db->where('in_id', $trabajo['in_id']);
			$this->db->where('man_id', $trabajo['man_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update('trabajos_inspecciones', $trabajo);
		} else {
			//NO EXISTE POR LO TANTO CREAMOS LA ENTRADA DE LA INSPECCION
			$trabajo['em_id'] = $this->session->userdata('em_id');
			$trabajo['ti_fecha_inicio'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			$trabajo['su_id'] = $this->session->userdata('su_id');
			$trabajo['emp_id'] = $this->session->userdata('emp_id');
			$this->db->insert('trabajos_inspecciones', $trabajo);	
		}
		
		//ACTUALIZAMOS EL TRABAJO
		$estado['tr_estado'] = 1;
		$this->db->where('tr_id', $trabajo['tr_id']);
		$this->db->where('em_id', $this->session->userdata('em_id'));
		$this->db->update($this->tabla, $estado);
		$trabajo['accion'] = 'Completar inspección';
		$this->set_bitacora_trabajo($trabajo);
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
		} else {
	    	$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
	
	//GUARDAR TRABAJO CON SUS INSPECCIONES AVANZADAS
	function set_trabajo_inspeccion_avanzada($trabajo) {
		$this->db->trans_begin();
		
		unset($trabajo['archivos']);
		
		//BUSCA LA INSPECCION AVANZADA DEL MANTENIMIENTO
		$trab_inpec = $this->db->from('trabajos_inspecciones_avanzadas')
			->where('tr_id',$trabajo['tr_id'])
			->where('inv_id',$trabajo['inv_id'])
			->where('man_id',$trabajo['man_id'])
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
		
		//SI EXISTE ACTUALIZAMOS
		if ($trab_inpec != null) {
			
			//PREGUNTAMOS SI CIERRA LA INSPECCION
			if ($trabajo['tia_estado'] == 2) {
				$trabajo['tia_fecha_fin'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			}
			
			$this->db->where('tr_id', $trabajo['tr_id']);
			$this->db->where('inv_id', $trabajo['inv_id']);
			$this->db->where('man_id', $trabajo['man_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update('trabajos_inspecciones_avanzadas', $trabajo);
		} else {
			//NO EXISTE POR LO TANTO CREAMOS LA ENTRADA DE LA INSPECCION AVANZADA
			$trabajo['em_id'] = $this->session->userdata('em_id');
			$trabajo['tia_fecha_inicio'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			$trabajo['su_id'] = $this->session->userdata('su_id');
			$trabajo['emp_id'] = $this->session->userdata('emp_id');
			$this->db->insert('trabajos_inspecciones_avanzadas', $trabajo);	
		}
		
		//ACTUALIZAMOS EL TRABAJO
		$estado['tr_estado'] = 1;
		$this->db->where('tr_id', $trabajo['tr_id']);
		$this->db->where('em_id', $this->session->userdata('em_id'));
		$this->db->update($this->tabla, $estado);
		$this->set_bitacora_trabajo($trabajo);
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
		} else {
	    	$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
	
	/*
	FIN TRABAJOS Empleados
	INICIO TRABAJOS CLIENTES
	*/
	
	//DEVUELVE LOS TRABAJOS PARA EL PANEL DEL MANTENIMIENTOS EN LOS CLIENTES
	public function get_trabajos_cliente($man_id) {
		$trabajos = $this->db->from($this->tabla)
			->where('em_id',$this->session->userdata('em_id'))
			->like('man_id',$man_id)
			->order_by('tr_fecha_inicio','desc')
			->get();
			
		return $trabajos->result();
	}
	
	//DEVUELVE LOS TRABAJOS PARA EL PANEL DEL MANTENIMIENTOS EN LOS CLIENTES
	public function get_trabajos_mantenimiento($man_id) {
		$trabajos = $this->db->from($this->tabla)
			->where('em_id',$this->session->userdata('em_id'))
			->like('man_id',$man_id)
			->order_by('tr_fecha_inicio','desc')
			->get();
			
		return $trabajos->result();
	}
	
	//DEVUELVE LAS INSPECCIONES DEL TRABAJO PARA EL PANEL DEL MANTENIMIENTOS EN LOS CLIENTES
	public function get_mantenimiento_inspecciones($in_id,$tr_id,$man_id) {
		$trabajos = $this->db->from('trabajos_inspecciones')
			->where('em_id',$this->session->userdata('em_id'))
			->where('in_id',$in_id)
			->where('tr_id',$tr_id)
			->where('man_id',$man_id)
			->order_by('ti_fecha_inicio','desc')
			->get();
			
		return $trabajos->result();
	}
	
	//ELIMINAR INSPECCION
	function get_eliminar_inspeccion($id_inpec) {
		//INSPECCION BASICA
		$colum = 'in_id';
		$tabla = 'inspecciones';
		//INSPECCION AVANZADA
		if (isset($id_inpec['inv_id'])) {
			$colum = 'inv_id';
			$tabla = 'inspecciones_avanzadas';
		}
		
		$trabajos = $this->db->from('trabajos')
			->where('em_id',$this->session->userdata('em_id'))
			->where('tr_estado',0)
			->like($colum,$id_inpec[$colum])
			->get()->result();
		
		if ($trabajos == null) {
			//ELIMINAR INSPECCION
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->where($colum, $id_inpec[$colum]);
			$this->db->delete($tabla); 
			
			return true;
		} else {
			return false;
		}
	}

	private function set_bitacora_trabajo($trabajo){
		$data = array( 
	        'bi_tipo'		=>  'TR', 
	        'bi_idasociado'	=>  $trabajo['tr_id'], 
	        'emp_id'		=>  $this->session->userdata('emp_id'),
	        'bi_accion'		=>  $trabajo['accion'],
	    );
		$this->db->insert('bitacora', $data);
	}
}

/* End of file m_trabajos.php */
/* Location: ./application/controllers/m_trabajos.php */