<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sucursales extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'sucursales';
    }
    
    //DEVUELVE LAS sucursales O NULL SI NO ENCUENTRA Y EL NUMERO TOTAL DE departamentos ASOCIADOS A CADA SUCURSAL
	public function get_sucursales() {
		$sucursal = $this->db->query('SELECT sucursales.su_id,sucursales.em_id,sucursales.su_nombre,sucursales.su_telefono,sucursales.su_email,sucursales.su_pais,countries.name,sucursales.su_localidad, sucursales.su_estado,COUNT(departamentos.su_id) as de_cantidad,departamentos.de_id
			FROM '.$this->tabla.'
			LEFT JOIN countries ON countries.id_countries = sucursales.su_pais
			LEFT JOIN departamentos ON departamentos.su_id = sucursales.su_id
			WHERE sucursales.em_id = '.$this->session->userdata("em_id").'
			GROUP BY sucursales.su_id');
			 
		return $sucursal->result();
    }
	
	//DEVOLVEMOS LOS CONTADORES DE LA SUCURSAL
    public function get_contadores($su_id) {
		$sucursal = $this->db->from('contadores')
			->where('su_id',$su_id)
			->where('em_id',$this->session->userdata('em_id'))
			->order_by('co_tipo')
			->get();
			
		return $sucursal->result();
	}
	
	//CERRAR A�O DE LA SUCURSAL
	public function set_cerrar_anio($su_id) {
		//USAMOS CONSULTAS TRANSACIONALES PARA EJECUTAR VARIAS A LA VEZ
		$this->db->trans_begin();
		
		$year = $this->get_contadores($su_id);
		
		$contadores['co_year'] = $year[0]->co_year + 1;
		$contadores['co_contador'] = 0;
		$this->db->where('su_id', $su_id);
		$this->db->where('em_id', $this->session->userdata('em_id'));
		$this->db->update('contadores', $contadores);
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return true;
		}
	}
    
    //DEVUELVE LOS DATOS DE LA SUCURSAL SEGUN SU_ID
	public function get_sucursal($su_id) {	
		$sucursal = $this->db->from($this->tabla)
			->where('sucursales.su_id',$su_id)
			->where('sucursales.em_id',$this->session->userdata('em_id'))
			->join('countries', 'countries.id_countries = sucursales.su_pais')
			->get()->row();
			
		return $sucursal;
	}
    
    //A�ADIMOS O EDITAMOS sucursales
	public function set_sucursal($sucursal) {		
		$this->db->trans_begin();
		
		if ($sucursal['su_iva_reducido'] == '') {
			$sucursal['su_iva_reducido'] = 0;
		}
		
		if ($sucursal['su_iva_superreducido'] == '') {
			$sucursal['su_iva_superreducido'] = 0;
		}
		
		if ($sucursal['su_id'] == '') {
			//SU_ID DE LA NUEVA SUCURSAL
			$sucursales = $this->db->from($this->tabla)
				->where('em_id',$this->session->userdata('em_id'))
				->get()->result();
			
			$numero = count($sucursales) + 1;
			
			$sucursal['su_id'] = $numero;
			$sucursal['su_estado'] = '1';
			$sucursal['em_id'] = $this->session->userdata('em_id');
			$this->db->insert($this->tabla,$sucursal);
			$id = $numero;
			
			//CALCULAMOS EL A�O SEGUN LA ZONA HORARIA
			$fecha = fechaHora_actual($sucursal['su_zona_horaria']);
			$fecha = new DateTime($fecha);
			$fecha = $fecha->format('Y');
			
			//HACEMOS 3 INSERT PARA LOS contadores: FACTURAS,RECTIFICATIVAS,PRESUPUESTOS
			for ($i=0; $i<3; $i++) {
				$contador['su_id'] = $numero;
				$contador['em_id'] = $this->session->userdata('em_id');
				$contador['co_contador'] = 0;
				$contador['co_year'] = $fecha;
				$contador['co_tipo'] = $i;
				$this->db->insert('contadores',$contador);
			}
		} else {
			$this->db->where('su_id', $sucursal['su_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($this->tabla, $sucursal);
			$id = $sucursal['su_id'];
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
    		$this->db->trans_rollback();
    		return false;
		} else {
    		$this->db->trans_commit();
    		return $id;
		}
	}
	
	//CAMBIAMOS EL ESTADO
	public function set_estado($su_id) {
		$sucursal = $this->db->from($this->tabla)
			->select('su_estado')
			->where('su_id',$su_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
		
		foreach ($sucursal->result() as $row) {
		   $estado = $row->su_estado;
		}
		if ($estado == 0) {
			$data = array('su_id'=>$su_id,'su_estado' => '1');
			$this->db->where('su_id', $su_id);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update($this->tabla, $data); 
		} else {
			$data = array('su_id'=>$su_id,'su_estado' => '0');
			$this->db->where('su_id', $su_id);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update($this->tabla, $data);
		}

		$this->set_bitacora_sucursal($su_id,$estado);
		
		return $this->db->trans_status();
	}
	
	public function get_paises() {
		$paises = $this->db->from('countries')
			->get();
			
		return $paises->result();
	}

	private function set_bitacora_sucursal($su_id,$estado){
		$data = array( 
	        'bi_tipo'		=>  'SU', 
	        'bi_idasociado'	=>  $su_id, 
	        'bi_accion'		=>  ($estado == 0)?'Habilitar':'Deshabilitar',
	        'emp_id'		=>  $this->session->userdata('emp_id'),
	    );
	    
		$this->db->insert('bitacora', $data);
	}
}

/* End of file m_sucursales.php */
/* Location: ./application/controllers/m_sucursales.php */