<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_inspecciones extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'inspecciones';
    }
    
    //OBTENEMOS LAS INSPECCIONES
    public function get_inspecciones() {
		$inspecciones = $this->db->from($this->tabla)
			->join('sucursales', 'sucursales.su_id = inspecciones.su_id')
			->where('inspecciones.em_id',$this->session->userdata('em_id'))
                        ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();
			
		return $inspecciones->result();
    }
    
    //OBTENEMOS LAS INSPECCIONES AVANZADAS
    public function get_inspecciones_avanzadas() {
		$inspecciones = $this->db->from('inspecciones_avanzadas')
			->join('sucursales', 'sucursales.su_id = inspecciones_avanzadas.su_id')
			->where('inspecciones_avanzadas.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();
			
		return $inspecciones->result();
    }
    
    //OBTENEMOS LAS INSPECCIONES DEL TRABAJO
    public function get_inspecciones_trabajo($in_id,$tr_id,$man_id) {
		$inspeccion = $this->db->from('trabajos_inspecciones')
			->where('in_id',$in_id)
			->where('tr_id',$tr_id)
			->where('man_id',$man_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $inspeccion;
    }
    
    //OBTENEMOS LAS INSPECCIONES DEL TRABAJO
    public function get_inspecciones_avanzadas_trabajo($inv_id,$tr_id,$man_id) {
		$inspeccion = $this->db->from('trabajos_inspecciones_avanzadas')
			->where('inv_id',$inv_id)
			->where('tr_id',$tr_id)
			->where('man_id',$man_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $inspeccion;
    }
    
    //OBTENEMOS LA INSECCION PARA EDITAR
    public function get_inspeccion($in_id) {
		$inspeccion = $this->db->from($this->tabla)
			->join('sucursales', 'sucursales.su_id = inspecciones.su_id')
			->where('inspecciones.in_id',$in_id)
			->where('inspecciones.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $inspeccion;
    }
    
    //OBTENEMOS LA INSECCION AVANZADA PARA EDITAR
    public function get_inspeccion_avanzada($inv_id) {
		$inspeccion = $this->db->from('inspecciones_avanzadas')
			->join('sucursales', 'sucursales.su_id = inspecciones_avanzadas.su_id')
			->where('inspecciones_avanzadas.inv_id',$inv_id)
			->where('inspecciones_avanzadas.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $inspeccion;
    }
    
    //DEVUELVE LAS INSPECCIONES DE LA SUCURSAL
	public function get_inspecciones_sucursal($su_id) {
		$inspecciones = $this->db->from($this->tabla)
			->where('su_id',$su_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
		
		return $inspecciones->result();
    }
    
    //DEVUELVE LAS INSPECCIONES AVANZADAS DE LA SUCURSAL
	public function get_inspecciones_avanzadas_sucursal($su_id) {
		$inspecciones = $this->db->from('inspecciones_avanzadas')
			->where('su_id',$su_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
		
		return $inspecciones->result();
    }
    
    //GUARDAMOS LA Inspecciones
    public function set_inspeccion($inspeccion) {
		$inspeccion['em_id'] = $this->session->userdata('em_id');
		
		if ($inspeccion['in_id'] == '') {
			//NUEVA INSPECCION
			unset($inspeccion['in_id']);
			$this->db->insert($this->tabla,$inspeccion);
			$id = $this->db->insert_id();
		} else {
			//EDITAR INSPECCION
			$this->db->where('in_id',$inspeccion['in_id']);
			$this->db->where('em_id',$inspeccion['em_id']);
			$this->db->update($this->tabla,$inspeccion);
			$id = $inspeccion['in_id'];
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
		}
	}
	
	//GUARDAMOS LAS INSPECCIONES AVANZADAS
    public function set_inspeccion_avanzada($inspeccion) {
		$inspeccion['em_id'] = $this->session->userdata('em_id');
		
		if ($inspeccion['inv_id'] == '') {
			//NUEVA INSPECCION
			unset($inspeccion['inv_id']);
			$this->db->insert('inspecciones_avanzadas',$inspeccion);
			$id = $this->db->insert_id();
		} else {
			//EDITAR INSPECCION
			$this->db->where('inv_id',$inspeccion['inv_id']);
			$this->db->where('em_id',$inspeccion['em_id']);
			$this->db->update('inspecciones_avanzadas',$inspeccion);
			$id = $inspeccion['inv_id'];
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
		};
	}
}

/* End of file m_inspecciones.php */
/* Location: ./application/controllers/m_inspecciones.php */