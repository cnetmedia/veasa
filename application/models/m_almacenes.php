<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_almacenes extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'almacenes';
    }
    
    //DEVUELVE LOS almacenes O NULL SI NO ENCUENTRA
	public function get_almacenes() {	
		$almacenes = $this->db->query('SELECT almacenes.*,COUNT(productos_almacenes.al_id) as pro_cantidad,COUNT(productos_reciclados_almacenes.al_id) as pro_rec_cantidad
			FROM '.$this->tabla.'
			LEFT JOIN productos_almacenes ON almacenes.al_id = productos_almacenes.al_id
			LEFT JOIN productos_reciclados_almacenes ON almacenes.al_id = productos_reciclados_almacenes.al_id
			WHERE almacenes.em_id = '.$this->session->userdata("em_id").'
			GROUP BY almacenes.al_id');
		
		return $almacenes->result();
    }
    
    //DEVUELVE LOS DATOS DEL ALMACEN SEGUN SU AL_ID
	public function get_almacen($al_id) {	
		$almacen = $this->db->from($this->tabla)
			->join('countries', 'countries.id_countries = almacenes.al_pais')
			->where('almacenes.al_id',$al_id)
			->where('almacenes.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $almacen;
	}
	
	//DEVUELVE LOS PRODUCTOS DEL ALMACEN SEGUN SU AL_ID
	public function get_almacen_productos($almacen) {	
		$tabla = 'productos_almacenes';
		$ordenar = 'product_id';
		if (isset($almacen['al_tipo'])) {
			if ($almacen['al_tipo'] == 1) {
				$tabla = 'productos_reciclados_almacenes'; 
				$ordenar = 'pra_id';
			}
		}
		
		$almacen_productos = $this->db->from($tabla)
			->join('productos_proveedores', 'productos_proveedores.product_id = '.$tabla.'.product_id')
			->join('proveedores', 'proveedores.pro_id = productos_proveedores.pro_id')
			->join('empresas', 'empresas.em_id = proveedores.em_id')
			->join('sucursales', 'sucursales.em_id = empresas.em_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
			->where($tabla.'.al_id',$almacen['al_id'])
			->where($tabla.'.em_id',$this->session->userdata('em_id'))
			->group_by($tabla.'.'.$ordenar)
			->get()->result();
		
		return $almacen_productos;
	}
	
	//DEVUELVE EL PRODUCTO DEL ALMACEN SEGUN SU AL_ID Y PRODUCT_ID
	public function get_almacen_producto($al_id,$product_id) {	
		$almacen = $this->get_almacen($al_id);
		
		$tabla = 'productos_almacenes';
		if ($almacen->al_tipo == 1) {
			$tabla = 'productos_reciclados_almacenes';
		}
		
		$almacen_producto = $this->db->from($tabla)
			->join('almacenes', $tabla.'.al_id = almacenes.al_id')
			->where($tabla.'.al_id',$al_id)
			->where($tabla.'.product_id',$product_id)
			->where($tabla.'.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $almacen_producto;
	}
	
	//DEVUELVE LOS PEDIDOS y PROYECTOS ASOCIADOS A UN PRODUCTO
	public function get_pedidos_producto($product_id) {	
		
		$product_id_val = (int)$product_id;
		
		$pedidos_producto = $this->db->from('pedidos_proveedores')
			->join('presupuestos', 'pedidos_proveedores.pr_id = presupuestos.pr_id')
			->join('mantenimientos','presupuestos.man_id=mantenimientos.man_id')
			->like('product_id',$product_id_val)
			->get();
			
		return $pedidos_producto->result();
	}

	//DEVUELVE LOS PEDIDOS ASOCIADOS A UN PRESUPUESTO/COTIZACION
	public function get_costos_pedidos_cotizacion($man_id, $pr_id=NULL, $ta_id = NULL) {			
		$result = array();

		/*$pedidos_producto = $this->db->from('pedidos_proveedores')
			->join('presupuestos', 'pedidos_proveedores.pr_id = presupuestos.pr_id')			
			->join('mantenimientos','presupuestos.man_id=mantenimientos.man_id')
			->join('facturas', 'facturas.man_id = mantenimientos.man_id','left')
			->join('rectificativas', 'rectificativas.man_id = mantenimientos.man_id','left')
			->where('mantenimientos.man_id',$man_id)	
			->where('presupuestos.em_id',$this->session->userdata('em_id'))
			->where('presupuestos.pr_estado',1)
			->where('pedidos_proveedores.pe_estado IN (1,2,4)')
			->order_by('presupuestos.pr_fecha','desc');*/

		$pedidos_producto = $this->db->from('facturas') 
			->join('mantenimientos', 'facturas.man_id = mantenimientos.man_id')
			->join('presupuestos','presupuestos.man_id=mantenimientos.man_id')
			->join('rectificativas', 'rectificativas.man_id = mantenimientos.man_id','left')
			->join('pedidos_proveedores', 'pedidos_proveedores.pr_id = presupuestos.pr_id AND pedidos_proveedores.pe_estado IN (1,2,4)','left')
						
			->where('mantenimientos.man_id',$man_id)	
			->where('presupuestos.em_id',$this->session->userdata('em_id'))
			->where('presupuestos.pr_estado',1)
			->order_by('presupuestos.pr_fecha','desc');

		if(!empty($pr_id)){
			$pedidos_producto = $pedidos_producto->where('presupuestos.pr_id',$pr_id);
		}

		$pedidos = $pedidos_producto->get()->result();		

		foreach ($pedidos as $key => $pedido) {
			if(!empty($pedido->re_costos)){
				$pr_costos = explode('/', $pedido->re_costos); 
			}else{
				$pr_costos = explode('/', $pedido->pr_costos); 				
			}

			$pr_costos[0] = explode(';', $pr_costos[0]); 
			$pr_costos[1] = explode(';', $pr_costos[1]); 
			$pr_costos[2] = array();

			foreach ($pr_costos[0] as $key => $value) {
				$pr_costos[2][$value] = floatval($pr_costos[1][$key]);
			}

			$result[$pedido->pr_id]['presupuesto'] = $pr_costos[2];

			//$productos = explode(';', $pedido->product_cantidad_recibido_almacen);
			$productos = explode(';', $pedido->product_id);
			$productos_cant = explode(';', $pedido->product_cantidad);

			foreach ($productos as $ind => $producto) {
				if(!empty($producto)){					
					$producto_detalle = $this->db->from('productos_proveedores')
			            ->where('productos_proveedores.product_id',$producto)
						->where('productos_proveedores.em_id',$this->session->userdata('em_id'))
						->get()->row();					

					$max_presupuesto = $pr_costos[2][$producto_detalle->cate_id];
					if(empty($result[$pedido->pr_id]['productos'][$producto]['cantidad_pedida'])){
						$result[$pedido->pr_id]['productos'][$producto]['cantidad_pedida'] = floatval($productos_cant[$ind]);
						$result[$pedido->pr_id]['productos'][$producto]['precio'] = floatval($producto_detalle->product_precio);
						$result[$pedido->pr_id]['productos'][$producto]['categoria'] = $producto_detalle->cate_id;
						$result[$pedido->pr_id]['productos'][$producto]['max_presupuesto'] = $max_presupuesto;
					}else{
						$result[$pedido->pr_id]['productos'][$producto]['cantidad_pedida'] += floatval($productos_cant[$ind]);			
					}

					$result[$pedido->pr_id]['productos'][$producto]['producto_id'] = $producto;
					$result[$pedido->pr_id]['productos'][$producto]['cantidad_usada'] = $result[$pedido->pr_id]['productos'][$producto]['cantidad_pedida'];
				}
			}
		}

		$talleres = $this->db->from('taller')
			->select('taller.*, presupuestos.*')
			->join('presupuestos', 'taller.pr_id = presupuestos.pr_id')
			->join('mantenimientos','presupuestos.man_id=mantenimientos.man_id')
			->where('mantenimientos.man_id',$man_id)	
			->where('presupuestos.em_id',$this->session->userdata('em_id'))
			->where('taller.ta_estado >= 0');

		if(!empty($ta_id)){
			$talleres = $talleres->where('taller.ta_id <>',$ta_id);
		}
		$talleres = $talleres->get()->result();
		
		foreach ($talleres as $ind => $taller) {
			$products_id = explode(';', $taller->product_id);
			$products_cantidad = explode(';', $taller->product_cantidad);

			foreach ($products_id as $key => $product_id) {	
				if(!empty($product_id)){	
					$max_presupuesto = 0;
					$producto_detalle = $this->db->from('productos_proveedores')
				            ->where('productos_proveedores.product_id',$product_id)
							->where('productos_proveedores.em_id',$this->session->userdata('em_id'))
							->get()->row();

					$max_presupuesto = $pr_costos[2][$producto_detalle->cate_id];					

					if(empty($result[$taller->pr_id]['productos'][$product_id]['cantidad_pedida'])){
						$result[$taller->pr_id]['productos'][$product_id]['cantidad_pedida'] = 0;
					}

					if(empty($result[$taller->pr_id]['productos'][$product_id]['cantidad_alm'])){
						$result[$taller->pr_id]['productos'][$product_id]['cantidad_alm'] = $products_cantidad[$key];
						$result[$taller->pr_id]['productos'][$product_id]['precio'] = $producto_detalle->product_precio;
						$result[$taller->pr_id]['productos'][$product_id]['categoria'] = $producto_detalle->cate_id;
						$result[$taller->pr_id]['productos'][$product_id]['max_presupuesto'] = $max_presupuesto;
					}else{			
						$result[$taller->pr_id]['productos'][$product_id]['cantidad_alm'] += floatval($products_cantidad[$key]);					
					}
				}
			}
		}

		$categorias = $this->db->from("categorias")->get()->result();
		foreach ($result as $key => $coti) {
			foreach ($categorias as $ind => $cate) {
				$result[$key]['categorias'][$cate->cate_id]['pedido'] = 0;
				$result[$key]['categorias'][$cate->cate_id]['usado'] = 0;

				foreach ($coti['productos'] as $indCoti => $producto) {
					if($producto['categoria'] == $cate->cate_id){
						$result[$key]['categorias'][$cate->cate_id]['pedido'] += ($producto['cantidad_pedida'] * $producto['precio']);	
						$result[$key]['categorias'][$cate->cate_id]['usado'] += ($producto['cantidad_pedida'] * $producto['precio']);	

						if($producto['cantidad_alm'] > $producto['cantidad_pedida'] ){
							$costo = abs($producto['cantidad_pedida'] - $producto['cantidad_alm']) * $producto['precio'];
							$result[$key]['categorias'][$cate->cate_id]['usado'] += $costo;
						}	
					}
				}
			}
		}

		$resultTotal = array();
		$resultTotal['cotizacion'] = $result;
		$resultTotal['general'] = array();
		

		foreach ($result as $key => $coti) {
			foreach ($coti['categorias'] as $ind => $cate) {
				if(empty($resultTotal['general'][$ind])){
					$resultTotal['general'][$ind]['usado']= $cate['usado'];
					$resultTotal['general'][$ind]['pedido']= $cate['pedido'];
				}else{
					$resultTotal['general'][$ind]['usado']+= $cate['usado'];
					$resultTotal['general'][$ind]['pedido']+= $cate['pedido'];
				}
			}
		}

		return $resultTotal;
	}
	
	//DEVUELVE LAS SALIDAS ASOCIADA A UN PRODUCTO y UN PROYECTO EN UN ALMACEN
	public function get_salidas_producto($product_id, $al_id, $man_id) {	
		
		$product_id_val = (int)$product_id;
		$al_id_val = (int)$al_id;
		
		$salidas_producto = $this->db->from('salidas')
			->join('mantenimientos','salidas.man_id=mantenimientos.man_id')
			->where('salidas.product_id',$product_id_val)
			->where('salidas.al_id',$al_id_val)
			->where('salidas.man_id',$man_id)
			->get();
			
		return $salidas_producto->result();
	}
	
	//DEVUELVE LAS SALIDAS ASOCIADA A UN PRODUCTO y UN PROYECTO EN UN TALLER
	public function get_salidas_taller_producto($product_id, $man_id) {	
		
		$product_id_val = (int)$product_id;
		
		$salidas_producto = $this->db->from('taller')
			->join('presupuestos', 'taller.pr_id = presupuestos.pr_id')
			->join('mantenimientos','presupuestos.man_id=mantenimientos.man_id')
			->like('taller.product_id',$product_id_val)
			->where('mantenimientos.man_id',$man_id)
			->get();
			
		return $salidas_producto->result();
	}
	
	//A�ADIMOS O EDITAMOS EL ALMACEN
	public function set_almacen($almacen) {
		$this->db->trans_begin();
		
		$almacen['em_id'] = $this->session->userdata('em_id');
		
		//NUEVO ALMACEN
		if ($almacen['al_id'] == '') {
			unset($almacen['al_id']);
			$this->db->insert($this->tabla, $almacen);
			$id = $this->db->insert_id();
		//EDITAR ALMACEN
		} else {
			$this->db->where('al_id', $almacen['al_id']);
			$this->db->update($this->tabla, $almacen);
			$id = $almacen['al_id'];
		}
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
	    }
	}
	
	//EDITAMOS EL PRODUCTO DEL ALMACEN
	public function set_product_almacen($producto) {
		$this->db->trans_begin();
		
		$al_tipo = $producto['al_tipo'];
		unset($producto['al_tipo']);
		$tabla = 'productos_almacenes';
		if ($al_tipo == 1) {
			$tabla = 'productos_reciclados_almacenes';
		}
		
		//ELIMINAMOS EL PRODUCTO DEL ALMACEN
		if ($producto['product_cantidad'] == 0) {
			$this->db->where('al_id', $producto['al_id']);
			$this->db->where('product_id', $producto['product_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->delete($tabla);
		} else {
			//EDITAMOS EL STOCK
			$this->db->where('al_id', $producto['al_id']);
			$this->db->where('product_id', $producto['product_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($tabla, $producto);
		}
		
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return true;
	    }
	}
	
	//A�ADIMOS UN PRODUCTO INDIVIDUAL AL ALMACEN
	public function set_anadir_product_almacen($producto) {
		$this->db->trans_begin();
		
		$producto['em_id'] = $this->session->userdata('em_id');
		
		$al_tipo = $producto['al_tipo'];
		unset($producto['al_tipo']);
		$tabla = 'productos_almacenes';
		if ($al_tipo == 1) {
			$tabla = 'productos_reciclados_almacenes';
			$producto['pa_reciclaje'] = '';
		}
		
		$this->db->insert($tabla,$producto);
		$id = $this->db->insert_id();

		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
	    }
	}
	
	//MOVEMOS EL PRODUCTO DE ALMACEN
	public function set_mover_product_almacen($producto) {
		$this->db->trans_begin();
		
		//OBTENGO TODOS LOS PROUCTOS DEL NUEVO ALMACEN
		$almacen = $this->get_almacen_productos($producto);
		$encontrado = false;
		
		$al_tipo = $producto['al_tipo'];
		unset($producto['al_tipo']);
		$tabla = 'productos_almacenes';
		if ($al_tipo == 1) {
			$tabla = 'productos_reciclados_almacenes';
		}
		
		$old_al_id = $producto['old_al_id'];
		$old_product_cantidad = $producto['old_product_cantidad'];
		unset($producto['old_al_id']);
		unset($producto['old_product_cantidad']);
		
		//SI LA CANTIDAD NUEVA ES IGUAL A LA ANTIGUA ELIMINAMOS EL PRODUCTO DEL ALMACEN DE ORIGEN
		if ($old_product_cantidad == $producto['product_cantidad']) {
			$this->db->where('al_id', $old_al_id);
			$this->db->where('product_id', $producto['product_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->delete($tabla);
		} else if ($producto['product_cantidad'] < $old_product_cantidad) {
		//SI LA CANTIDAD NUEVA ES MENOR A LA CANTIDAD ANTIGUA, ACTUALIZAMOS EL STOCK DEL PRODUCTO DEL ALMACEN DE ORIGEN
			$aux = array(
				'product_cantidad' => ($old_product_cantidad - $producto['product_cantidad'])
			);
			
			$this->db->where('al_id', $old_al_id);
			$this->db->where('product_id', $producto['product_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($tabla, $aux);
		}
		
		//PREGUNTOS SI ENCUENTRA EL PRODUCTO EN EL NUEVO ALMACEN
		for ($i=0; $i<count($almacen); $i++) {
			if ($almacen[$i]->product_id == $producto['product_id']) {
				$encontrado = true;
				$producto['product_cantidad'] = $producto['product_cantidad'] + $almacen[$i]->product_cantidad;
			}
		}
		
		if ($encontrado) {
			//ENCONTRO EL PRODUCTO EN EL ALMACEN POR LO QUE ACTUALIZO EL PRODUCTO DEL ALMACEN DE DESTINO
			$this->db->where('al_id', $producto['al_id']);
			$this->db->where('product_id', $producto['product_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($tabla, $producto);
		} else {
			//NO LO ENCONTRO ENTONCES INSERTO EL PRODUCTO NUEVO
			$producto['em_id'] = $this->session->userdata('em_id');
			$this->db->insert($tabla, $producto);
		}
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return true;
	    }
	}
	
	//ENVIAMOS EL PRODUCTO AL CLIENTE
	public function set_salida_producto($salida) {
		$this->db->trans_begin();
		
		$al_tipo = $salida['al_tipo'];
		unset($salida['al_tipo']);
		$tabla = 'productos_almacenes';
		if ($al_tipo == 1) {
			$tabla = 'productos_reciclados_almacenes';
		}
		
		$old_product_cantidad = $salida['old_product_cantidad'];
		unset($salida['old_product_cantidad']);
		
		//ACTUALIZAR STOCK
		//SI LA CANTIDAD NUEVA ES IGUAL A LA ANTIGUA ELIMINAMOS EL PRODUCTO DEL ALMACEN DE ORIGEN
		if ($old_product_cantidad == $salida['sa_cantidad']) {
			$this->db->where('al_id', $salida['al_id']);
			$this->db->where('product_id', $salida['product_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->delete($tabla);
		} else if ($salida['sa_cantidad'] < $old_product_cantidad) {
		//SI LA CANTIDAD NUEVA ES MENOR A LA CANTIDAD ANTIGUA, ACTUALIZAMOS EL STOCK DEL PRODUCTO DEL ALMACEN DE ORIGEN
			$aux = array(
				'product_cantidad' => ($old_product_cantidad - $salida['sa_cantidad'])
			);
			
			$this->db->where('al_id', $salida['al_id']);
			$this->db->where('product_id', $salida['product_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($tabla, $aux);
		}
		
		//ENVIAR
		$salida['sa_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
		$salida['em_id'] = $this->session->userdata('em_id');
		$salida['emp_id'] = $this->session->userdata('emp_id');
        $salida['sa_estado'] = 0;
		$this->db->insert('salidas', $salida);
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return true;
	    }
	}
	
	//ENVIAMOS EL PRODUCTO AL CLIENTE
	public function set_salida_productos($salida) {
		$this->db->trans_begin();
		
		$products = $salida['product_id'];
		
		for($k=0;$k<=count($products)-1;$k++) {
		
			$al_tipo = (int)$salida['al_tipo'];
			$pra_id = (int)$salida['pra_id'][$k];
			$product_id = (int)$salida['product_id'][$k];
			$sa_cantidad = (int)$salida['sa_cantidad'][$k];
			$sa_temporal = (int)$salida['sa_temporal'][$k];
			
			$tabla = 'productos_almacenes';
			if ($al_tipo == 1) {
				$tabla = 'productos_reciclados_almacenes';
			}
			
			$old_product_cantidad = $salida['old_product_cantidad'][$k];
	
			//ACTUALIZAR STOCK
			//SI LA CANTIDAD NUEVA ES IGUAL A LA ANTIGUA ELIMINAMOS EL PRODUCTO DEL ALMACEN DE ORIGEN
			if ($old_product_cantidad == $sa_cantidad) {
				
				if($pra_id > 0) {
					
					$this->db->where('al_id', $salida['al_id']);
					$this->db->where('product_id', $product_id );
					$this->db->where('pra_id', $pra_id);
					$this->db->where('em_id', $this->session->userdata('em_id'));
					$this->db->delete($tabla);
				
				} else {
				
					$this->db->where('al_id', $salida['al_id']);
					$this->db->where('product_id', $product_id );
					$this->db->where('em_id', $this->session->userdata('em_id'));
					$this->db->delete($tabla);
				
				}
				
			} else if ($sa_cantidad < $old_product_cantidad) {
			//SI LA CANTIDAD NUEVA ES MENOR A LA CANTIDAD ANTIGUA, ACTUALIZAMOS EL STOCK DEL PRODUCTO DEL ALMACEN DE ORIGEN
				$aux = array(
					'product_cantidad' => ($old_product_cantidad - $sa_cantidad)
				);
				
				if($pra_id > 0) {
					
					$this->db->where('al_id', $salida['al_id']);
					$this->db->where('product_id', $product_id );
					$this->db->where('pra_id', $pra_id);
					$this->db->where('em_id', $this->session->userdata('em_id'));
					$this->db->update($tabla, $aux);
				
				} else {
				
					$this->db->where('al_id', $salida['al_id']);
					$this->db->where('product_id', $product_id );
					$this->db->where('em_id', $this->session->userdata('em_id'));
					$this->db->update($tabla, $aux);
				
				}
			}
			
			$aux2 = array();
			
			$aux2['product_id'] = $product_id;
			$aux2['al_id'] = $salida['al_id'];
			$aux2['man_id'] = $salida['man_id'];
			$aux2['sa_cantidad'] = $sa_cantidad;
			$aux2['sa_temporal'] = $sa_temporal;
			
			//ENVIAR
			$aux2['sa_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			$aux2['em_id'] = $this->session->userdata('em_id');
			$aux2['emp_id'] = $this->session->userdata('emp_id');
			$aux2['sa_estado'] = 0;
			$this->db->insert('salidas', $aux2);
		}
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
	}
	
	//DEVUELVE LAS ENTRADAS DE PRODUCTOS PARA LOS ALMACENES
	/* public function get_almacenes_entradas() {	
		$entradas = $this->db->from('pedidos_proveedores')
			->join('productos_proveedores', 'productos_proveedores.product_id = pedidos_proveedores.product_id','left')
			->join('proveedores', 'proveedores.pro_id = productos_proveedores.pro_id','left')
			->where('pedidos_proveedores.em_id',$this->session->userdata('em_id'))
			->get();

		return $entradas->result();
    } */
	
	public function get_almacenes_entradas($tipo="") {	
		$cond_aprobadas = ($tipo == "aprobadas") ? 'pe_estado =' : 'pe_estado !=';
			
		$entradas = $this->db->from('pedidos_proveedores')
			->where('em_id',$this->session->userdata('em_id'))
			->where($cond_aprobadas,'1')
			->get();

		return $entradas->result();
    }
	
	//DEVUELVE LAS ENTRADAS DE PRODUCTOS PARA LOS ALMACENES
	public function get_almacenes_entradas_proveedores() {	
		$entradas = $this->db->from('pedidos_proveedores')
			->join('productos_proveedores', 'productos_proveedores.product_id = pedidos_proveedores.product_id','left')
			->join('proveedores', 'proveedores.pro_id = productos_proveedores.pro_id','left')
			->where('pedidos_proveedores.em_id',$this->session->userdata('em_id'))
			->group_by('proveedores.pro_id')
			->get();

		return $entradas->result();
    }
    
    //DEVUELVE LOS DATOS DE LA ENTRADA (PEDIDO) SEGUN SU PE_ID
	public function get_almacenes_entrada($pe_id) {	
		$entrada = $this->db->from('pedidos_proveedores')
			->where('pe_id',$pe_id)
			->get()->row();
			
		return $entrada;
	}
	
	//ACTUALIZAMOS LA ENTRADA (PEDIDO)
	public function set_entrada($entrada) {
		$this->db->trans_begin();
		
		//$entrada = array_filter($entrada);
		//print_r($entrada); exit();
		//CAMBIAR SOLO EL ESTADO DE LA ENTRADA
		if ($entrada['pe_estado'] == 0) { //Pendiente
			$datos = array(
				'pe_estado' => $entrada['pe_estado'],
				'product_cantidad' => $entrada['product_cantidad'],
				'product_cantidad_recibido' => $entrada['product_cantidad_recibido'],
				'product_id' => $entrada['product_id'],
				'pe_observaciones_empresa' => $entrada['pe_observaciones_empresa'],
				'pe_observaciones_proveedor' => $entrada['pe_observaciones_proveedor'],
				'pr_id' => $entrada['pr_id'],
				'emp_id_aprobado_dept_prod' => !empty($entrada['emp_id_aprobado_dept_prod'])?$entrada['emp_id_aprobado_dept_prod'] : null,
				'fecha_aprobado_dept_prod' => !empty($entrada['fecha_aprobado_dept_prod'])?$entrada['fecha_aprobado_dept_prod']: null,
				'emp_id_autoriza' => !empty($entrada['emp_id_autoriza'])?$entrada['emp_id_autoriza'] : null,
				'fecha_autorizacion' => !empty($entrada['fecha_autorizacion'])?$entrada['fecha_autorizacion']: null,
			);
			
			$this->db->where('pe_id', $entrada['pe_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update('pedidos_proveedores', $datos);
		} else {
			//ACTUALIZAR ENTRADA YA APROBADA
			$al_id = $entrada['al_id'];
			unset($entrada['al_id']);
			
			//SUMAMOS EL PRODUCTO AL ALMACEN
			//SEPARAMOS EN ARRAY EL PRODUCT_ID
			$productos = explode(';',$entrada['product_id']);
			$productos = array_filter($productos);
			
			//SEPARAMOS EN ARRAY LAS CANTIDADES RECIBIDAS DE LOS PRODUCTOS
			$cantidad_recibido = explode(';',$entrada['product_cantidad_recibido']);
			//$cantidad_recibido = array_filter($cantidad_recibido);
			
			//SEPARAMOS EN ARRAY LAS CANTIDADES YA RECIBIDAS DE LOS PRODUCTOS
			$cantidad_recibido_total = explode(';',$entrada['cantidad_recibido_total']);
			//$cantidad_recibido_total = array_filter($cantidad_recibido_total);
			
			//SEPARAMOS EN ARRAY LAS CANTIDADES ORIGINALES DEL PRODUCTO
			$product_cantidad = explode(';',$entrada['product_cantidad']);
			//$product_cantidad = array_filter($product_cantidad);
			
			//OBTENEMOS LOS PRODUCTOS DEL ALMACEN SELECCIONADO
			$aux = array('al_id' => $al_id);
			$almacen = $this->get_almacen_productos($aux);
			
			//LISTA DE PRODUCTOS CHECKEADOS COMO RECIBIDOS
			$product_id_check = explode(';',$entrada['product_id_check']);
			//$product_id_check = array_filter($product_id_check);
			
			//GUARDAMOS LA CANTIDAD RECIBIDA TOTAL
			$aux_product_cantidad_recibido = '';
			
			//ENTRADA COMPLETADA
			$completado = 0;
			
			//RECORREMOS LOS PRODUCTOS A GUARDAR
			$product_almacen = $entrada['product_cantidad_recibido_almacen'];
			for ($i=0; $i<count($productos); $i++) {
				if (in_array($productos[$i], $product_id_check)) {
					$encontrado = false;
					$pos_almacen = '';
					
					$product_almacen = $product_almacen.$productos[$i].','.$cantidad_recibido[$i].','.$al_id.';';
					
					//RECORREMOS EL ALMACEN BUSCANDO SI ESTAN LOS PRODUCTOS
					for ($t=0; $t<count($almacen); $t++) {
						if ($almacen[$t]->product_id == $productos[$i]) {
							$encontrado = true;
							$pos_almacen = $t;
						}
					}
					
					if ($encontrado) {
						//ACTUALIZAMOS LA CANTIDAD DEL PRODUCTO
						$aux = array(
							'product_cantidad' => ((float)$cantidad_recibido[$i] + (float)$almacen[$pos_almacen]->product_cantidad)
						);
								
						$this->db->where('al_id', $al_id);
						$this->db->where('product_id', $productos[$i]);
						$this->db->where('em_id', $this->session->userdata('em_id'));
						$this->db->update('productos_almacenes', $aux);
					} else {
						//INSERTAMOS EL PRODUCTO NUEVO AL ALMACEN
						$aux = array(
							'em_id' => $this->session->userdata('em_id'),
							'product_id' => $productos[$i],
							'product_cantidad' => $cantidad_recibido[$i],
							'al_id' => $al_id
						);
							
						$this->db->insert('productos_almacenes', $aux);
					}
					
					//PREGUNTAMOS SI ESTA TODO COMPLETADO
					if (((float)$cantidad_recibido[$i] + (float)$cantidad_recibido_total[$i]) == (float)$product_cantidad[$i]) {
						$completado++;
					}
					
					$aux_product_cantidad_recibido = $aux_product_cantidad_recibido.((float)$cantidad_recibido[$i] + (float)$cantidad_recibido_total[$i]).';';
				} else {
					$aux_product_cantidad_recibido = $aux_product_cantidad_recibido.$cantidad_recibido_total[$i].';';
				} 
			}
			
			//ACTUALIZAMOS LA ENTRADA
			$datos = array(
				'product_id' => $entrada['product_id'],
				'pe_observaciones_empresa' => $entrada['pe_observaciones_empresa'],
				'pr_id' => $entrada['pr_id'],
				'pe_observaciones_proveedor' => $entrada['pe_observaciones_proveedor'],
				'pe_estado' => $entrada['pe_estado'],
				'product_cantidad_recibido' => $aux_product_cantidad_recibido,
				'product_cantidad' => $entrada['product_cantidad'],
				'fecha_aprobacion' => $entrada['fecha_aprobacion'],
				'emp_id_aprobado' => $entrada['emp_id_aprobado'],
				'emp_id_recepcion' => $entrada['emp_id_recepcion'],
				'product_cantidad_recibido_almacen' => $product_almacen,
				'emp_id_aprobado_dept_prod' => !empty($entrada['emp_id_aprobado_dept_prod'])?$entrada['emp_id_aprobado_dept_prod'] : null,
				'fecha_aprobado_dept_prod' => !empty($entrada['fecha_aprobado_dept_prod'])?$entrada['fecha_aprobado_dept_prod']: null,
				'emp_id_autoriza' => !empty($entrada['emp_id_autoriza'])?$entrada['emp_id_autoriza'] : null,
				'fecha_autorizacion' => !empty($entrada['fecha_autorizacion'])?$entrada['fecha_autorizacion']: null,
			);
			
			if ($aux_product_cantidad_recibido == $entrada['product_cantidad']) {
				$datos['pe_estado'] = 4; //Completado
				$datos['fecha_completado'] = fechaHora_actual($this->session->userdata("su_zona_horaria"));
			}
			
			//GUARDAMOS EL EMPLEADO QUE RECEPCIONO LOS PRODUCTOS
			/*if (count($product_id_check) > 0) {
				$datos['emp_id_recepcion'] = $this->session->userdata('emp_id').';';
			}*/
			
			$this->db->where('pe_id', $entrada['pe_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update('pedidos_proveedores', $datos);
		}
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return true;
	    }
	}
	
	//DEVUELVE LAS SALIDAS DE LOS ALMACENES (SOL LAS SALIDAS DESDE EL TALLER)
	public function get_almacenes_salidas($estado) {

		$valor = ($estado) ? '( sa_estado != 2 AND sa_estado != 3 )' : 'sa_estado IS NOT NULL';
		$salidas = $this->db->from('salidas')
			->select('salidas.*, clientes.cl_nombre, mantenimientos.man_id, mantenimientos.man_nombre, productos_proveedores.product_referencia, productos_proveedores.product_nombre, productos_proveedores.product_caracteristicas, almacenes.al_tipo, empleados.emp_nombre, empleados.emp_apellido1, empleados.emp_apellido2,')
			->join('mantenimientos', 'mantenimientos.man_id = salidas.man_id','left')
            ->join('productos_proveedores', 'productos_proveedores.product_id = salidas.product_id','left')
            ->join('almacenes', 'almacenes.al_id = salidas.al_id','left')
			->join('clientes', 'clientes.cl_id = mantenimientos.cl_id','left')
			->join('taller', 'taller.ta_id = salidas.ta_id','left')
			->join('empleados', 'empleados.emp_id = salidas.emp_id','left')
			->where('salidas.em_id',$this->session->userdata('em_id'))
			->where($valor)
			->get()->result();
			
		return $salidas;
    }
    
    //AÑADIMOS LA SALIDA MANUAL
    public function set_salida_manual($salida) {
	$this->db->trans_begin();
		
        $salida['em_id'] = $this->session->userdata('em_id');
        $salida['emp_id'] = $this->session->userdata('emp_id');
        $salida['sa_estado'] = 0;
        $salida['sa_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
		
		if($salida['pra_id']>0) {
			$producto = $this->db->from('productos_proveedores')
			->join('productos_reciclados_almacenes', 'productos_reciclados_almacenes.product_id = productos_proveedores.product_id','left')
			->where('productos_reciclados_almacenes.pra_id',$salida['pra_id'])
			->where('productos_proveedores.product_id',$salida['product_id'])
			->get()->row();
		}  else {
			$producto = $this->db->from('productos_proveedores')
			->where('product_id',$salida['product_id'])
			->get()->row();
		}
		$referencia = $producto->product_referencia;
		$caracteristicas = $producto->product_caracteristicas;
		
		$caracteristicas = explode(';', $caracteristicas);
		
		$ta_trabajo = '<div><strong>'.$referencia.'</strong></div>';
		
		for ($y=0; $y<count($caracteristicas); $y++) {
			
			$refe = explode('/', $caracteristicas[$y]);
						
			$ta_trabajo = $ta_trabajo.'<div>';
			
				$ta_trabajo = $ta_trabajo.$refe[0].': '.$refe[1].' ';
				
			$ta_trabajo = $ta_trabajo.'</div>';
		}
		
		if ($salida['descripcion'] != '') {
			$ta_trabajo = '<div>'.$ta_trabajo.lang('descripcion').': '.$salida['descripcion'].'</div>';
		}
		
		if ($producto->pra_caracteristicas) {
			$ta_trabajo = '<div>'.$ta_trabajo.'Reciclaje: '.$producto->pra_caracteristicas.'</div>';
		}
		
		$salida['ta_trabajo'] = $nombre;
		
		unset($salida['descripcion']);
		unset($salida['pra_id']);
		
	$this->db->insert('salidas', $salida);
	$id = $this->db->insert_id();

	if ($this->db->trans_status() === FALSE) {
	    $this->db->trans_rollback();
	    return false;
	} else {
	    $this->db->trans_commit();
	    return $id;
	}
    }
    
    //CAMBIAR ESTADO SALIDAS
    public function set_estado_salida($salida) {
		
		if(is_array($salida['sa_cantidad'])) {
			
			$array_ta_id = $salida['ta_id'];
			$array_al_id = $salida['al_id'];
			$array_product_id = $salida['product_id'];
			$array_sa_cantidad = $salida['sa_cantidad'];
			$array_man_id = $salida['man_id'];
			$array_ta_trabajo = $salida['ta_trabajo'];
			$array_al_tipo = $salida['al_tipo'];
			$array_compuesto_padre = $salida['compuesto_padre'];
			$array_sa_id = $salida['sa_id'];
			$array_sa_temporal = $salida['sa_temporal'];
			
			unset($salida['ta_id']);
			unset($salida['al_id']);
			unset($salida['product_id']);
			unset($salida['sa_cantidad']);
			unset($salida['man_id']);
			unset($salida['ta_trabajo']);
			unset($salida['al_tipo']);
			unset($salida['compuesto_padre']);
			unset($salida['sa_id']);
			
			$k=0;
			
			foreach($array_sa_cantidad as $sa_cantidad) {
				
				$ta_id = $array_ta_id[$k];
				$al_id = $array_al_id[$k];
				$product_id = $array_product_id[$k];
				$sa_cantidad = $array_sa_cantidad[$k];
				$ta_trabajo = $array_ta_trabajo[$k];
				$al_tipo = $array_al_tipo[$k];
				$compuesto_padre = $array_compuesto_padre[$k];
				$sa_id = $array_sa_id[$k];
				
				$salida['sa_temporal']= $array_sa_temporal[$k];
				$sa_temporal = $array_sa_temporal[$k];
				
				//CENCELACION DE ENVIO, VOLVEMOS A SUMAR LAS UNIDADES
				if ($salida['sa_estado'] == 3) {
					//PRODUCTO DEL ALMACEN
					if ($product_id != '') {
						//SUMAMOS LAS UNIDADES DEVUELTAS AL ALMACEN
						$producto = $this->get_almacen_producto($al_id,$product_id);
						
						$tabla = 'productos_almacenes';
						if ($al_tipo == 1) {
							$tabla = 'productos_reciclados_almacenes';
						}
				
						//PREGUNTAMOS SI EL PRODUCTO DEL ALMACEN EXISTE PARA ACTUALIZARLO O CREARLO
						if (isset($producto->product_cantidad)) {
							$devolucion = array(
								'product_cantidad' => (int)$producto->product_cantidad + (int)$sa_cantidad
							);
		
							$this->db->where('al_id', $al_id);
							$this->db->where('product_id', $product_id);
							$this->db->where('em_id', $this->session->userdata('em_id'));
							$this->db->update($tabla, $devolucion);
						} else {
							$devolucion = array(
								'product_cantidad' => $sa_cantidad,
								'em_id' => $this->session->userdata('em_id'),
								'al_id' => $al_id,
								'product_id' => $product_id
							);
		
							$this->db->insert($tabla, $devolucion);
						}
					} else if ($ta_trabajo != '') {
						//TRABAJO DEL TALLER
						$taller = $this->db->from('taller')
							->where('ta_id',$ta_id)
							->where('em_id',$this->session->userdata('em_id'))
							->get()->row();
						
						$ta_trabajos_compuestos_codigo = $taller->ta_trabajos_compuestos_codigo;
						$ta_trabajos_compuestos_codigo = explode(';',$ta_trabajos_compuestos_codigo);
						$ta_trabajos_compuestos_codigo = array_filter($ta_trabajos_compuestos_codigo);
						
						$ta_trabajos_compuestos_terminados = $taller->ta_trabajos_compuestos_terminados;
						$ta_trabajos_compuestos_terminados = explode(';',$ta_trabajos_compuestos_terminados);
						//$ta_trabajos_compuestos_terminados = array_filter($ta_trabajos_compuestos_terminados);
						
						$terminados = '';
						
						for ($x=0; $x<count($ta_trabajos_compuestos_codigo); $x++) {
							$aux = explode('|',$ta_trabajos_compuestos_codigo[$x]);
							$aux = array_filter($aux);
							
							$aux2 = explode('|',$ta_trabajos_compuestos_terminados[$x]);
							//$aux2 = array_filter($aux2);
							
							if ((string)$aux[0] == (string)$compuesto_padre) {
								for ($y=0; $y<count($aux2); $y++) {
									if ((string)$aux[$y+1] == (string)$ta_trabajo) {
										$resta = (int)$aux2[$y] - (int)$sa_cantidad;
										$terminados = $terminados.$resta.'|';
									} else {
									   $terminados = $terminados.$aux2[$y].'|'; 
									}
								}
								
								$terminados = substr($terminados, 0, -1);
								$terminados = $terminados.';';
							} else {
							   $terminados = $terminados.$ta_trabajos_compuestos_terminados[$x].';';
							}
						}  
						
						$observ = '';
						if ($taller->ta_observaciones != '' && $salida['sa_cancelacion_salida'] != '') {
							$observ = $taller->ta_observaciones.'. '.$observ;
						} else {
							$observ = $taller->ta_observaciones.$salida['sa_cancelacion_salida'];
						}
						
						$devolucion = array(
							'ta_trabajos_compuestos_terminados' => $terminados,
							'ta_observaciones' => $observ
						);
						
						$this->db->where('ta_id', $ta_id);
						$this->db->where('em_id', $this->session->userdata('em_id'));
						$this->db->update('taller', $devolucion);
					}
				}
				
				//FECHA DE SALIDA
				if ($salida['sa_estado'] == 2) {
				   $salida['sa_fecha_salida'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
				   $salida['emp_id_salida'] = $this->session->userdata('emp_id');
				} else if ($salida['sa_estado'] == 1) {
					$salida['fecha_aprobo'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
					$salida['emp_id_aprobo'] = $this->session->userdata('emp_id');
				} else if ($salida['sa_estado'] == 3) {
					$salida['fecha_cancelacion'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
					$salida['emp_id_cancelo'] = $this->session->userdata('emp_id');
				}
				
				$this->db->where('sa_id', $sa_id);
				$this->db->where('em_id', $this->session->userdata('em_id'));
				$this->db->update('salidas', $salida);
				$this->set_bitacora_salida($sa_id,$salida['sa_estado']); 
				
				//PREGUNTAMOS SI LA SALIDA ES TEMPORAL Y SI SE APROBO PARA CREAR UNA ENTRADA
				if ($salida['sa_estado'] == 2 && $sa_temporal == 1 && $product_id != '') {
					$entrada = array(
						'em_id' => $this->session->userdata('em_id'),
						'product_id' => $product_id.';',
						'product_cantidad' => $sa_cantidad.';',
						'pe_fecha' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
						'emp_id' => $this->session->userdata('emp_id'),
						'emp_id_aprobado' => $this->session->userdata('emp_id'),
						'pe_estado' => 1, //Aprobada
						'product_cantidad_recibido' => '0;',
						'fecha_aprobacion' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
						'pe_auto' => '1'
					);
					
					$this->db->insert('pedidos_proveedores',$entrada);
				}
				
				$k++;
			}
			
		} else {
			
		$ta_id = $salida['ta_id'];
        $al_id = $salida['al_id'];
        $product_id = $salida['product_id'];
        $sa_cantidad = $salida['sa_cantidad'];
        $ta_trabajo = $salida['ta_trabajo'];
        $al_tipo = $salida['al_tipo'];
        $compuesto_padre = $salida['compuesto_padre'];
        
        unset($salida['ta_id']);
        unset($salida['al_id']);
        unset($salida['product_id']);
        unset($salida['sa_cantidad']);
        unset($salida['ta_trabajo']);
        unset($salida['al_tipo']);
        unset($salida['compuesto_padre']);
        
        //CENCELACION DE ENVIO, VOLVEMOS A SUMAR LAS UNIDADES
        if ($salida['sa_estado'] == 3) {
            //PRODUCTO DEL ALMACEN
            if ($product_id != '') {
                //SUMAMOS LAS UNIDADES DEVUELTAS AL ALMACEN
                $producto = $this->get_almacen_producto($al_id,$product_id);
                
				$tabla = 'productos_almacenes';
				if ($al_tipo == 1) {
					$tabla = 'productos_reciclados_almacenes';
				}
		
                //PREGUNTAMOS SI EL PRODUCTO DEL ALMACEN EXISTE PARA ACTUALIZARLO O CREARLO
                if (isset($producto->product_cantidad)) {
                    $devolucion = array(
                        'product_cantidad' => (int)$producto->product_cantidad + (int)$sa_cantidad
                    );

                    $this->db->where('al_id', $al_id);
                    $this->db->where('product_id', $product_id);
                    $this->db->where('em_id', $this->session->userdata('em_id'));
                    $this->db->update($tabla, $devolucion);
                } else {
                    $devolucion = array(
                        'product_cantidad' => $sa_cantidad,
                        'em_id' => $this->session->userdata('em_id'),
                        'al_id' => $al_id,
                        'product_id' => $product_id
                    );

                    $this->db->insert($tabla, $devolucion);
                }
            } else if ($ta_trabajo != '') {
                //TRABAJO DEL TALLER
                $taller = $this->db->from('taller')
                    ->where('ta_id',$ta_id)
                    ->where('em_id',$this->session->userdata('em_id'))
                    ->get()->row();
                
                $ta_trabajos_compuestos_codigo = $taller->ta_trabajos_compuestos_codigo;
                $ta_trabajos_compuestos_codigo = explode(';',$ta_trabajos_compuestos_codigo);
                $ta_trabajos_compuestos_codigo = array_filter($ta_trabajos_compuestos_codigo);
                
                $ta_trabajos_compuestos_terminados = $taller->ta_trabajos_compuestos_terminados;
                $ta_trabajos_compuestos_terminados = explode(';',$ta_trabajos_compuestos_terminados);
                //$ta_trabajos_compuestos_terminados = array_filter($ta_trabajos_compuestos_terminados);
                
                $terminados = '';
                
                for ($x=0; $x<count($ta_trabajos_compuestos_codigo); $x++) {
                    $aux = explode('|',$ta_trabajos_compuestos_codigo[$x]);
                    $aux = array_filter($aux);
                    
                    $aux2 = explode('|',$ta_trabajos_compuestos_terminados[$x]);
                    //$aux2 = array_filter($aux2);
                    
                    if ((string)$aux[0] == (string)$compuesto_padre) {
                        for ($y=0; $y<count($aux2); $y++) {
                            if ((string)$aux[$y+1] == (string)$ta_trabajo) {
                                $resta = (int)$aux2[$y] - (int)$sa_cantidad;
                                $terminados = $terminados.$resta.'|';
                            } else {
                               $terminados = $terminados.$aux2[$y].'|'; 
                            }
                        }
                        
                        $terminados = substr($terminados, 0, -1);
                        $terminados = $terminados.';';
                    } else {
                       $terminados = $terminados.$ta_trabajos_compuestos_terminados[$x].';';
                    }
                }  
                
                $observ = '';
                if ($taller->ta_observaciones != '' && $salida['sa_cancelacion_salida'] != '') {
                    $observ = $taller->ta_observaciones.'. '.$observ;
                } else {
                    $observ = $taller->ta_observaciones.$salida['sa_cancelacion_salida'];
                }
                
                $devolucion = array(
                    'ta_trabajos_compuestos_terminados' => $terminados,
                    'ta_observaciones' => $observ
                );
                
                $this->db->where('ta_id', $ta_id);
                $this->db->where('em_id', $this->session->userdata('em_id'));
                $this->db->update('taller', $devolucion);
            }
        }
        
        //FECHA DE SALIDA
        if ($salida['sa_estado'] == 2) {
           $salida['sa_fecha_salida'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
           $salida['emp_id_salida'] = $this->session->userdata('emp_id');
        } else if ($salida['sa_estado'] == 1) {
			$salida['fecha_aprobo'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
        	$salida['emp_id_aprobo'] = $this->session->userdata('emp_id');
		} else if ($salida['sa_estado'] == 3) {
			$salida['fecha_cancelacion'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
        	$salida['emp_id_cancelo'] = $this->session->userdata('emp_id');
		}
        
        $this->db->where('sa_id', $salida['sa_id']);
        $this->db->where('em_id', $this->session->userdata('em_id'));
        $this->db->update('salidas', $salida); 
        $this->set_bitacora_salida($salida['sa_id'],$salida['sa_estado']);
		
		//PREGUNTAMOS SI LA SALIDA ES TEMPORAL Y SI SE APROBO PARA CREAR UNA ENTRADA
		if ($salida['sa_estado'] == 2 && $salida['sa_temporal'] == 1 && $product_id != '') {
			$entrada = array(
				'em_id' => $this->session->userdata('em_id'),
				'product_id' => $product_id.';',
				'product_cantidad' => $sa_cantidad.';',
				'pe_fecha' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
				'emp_id' => $this->session->userdata('emp_id'),
				'emp_id_aprobado' => $this->session->userdata('emp_id'),
				'pe_estado' => 1, //Aprobada
				'product_cantidad_recibido' => '0;',
				'fecha_aprobacion' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
				'pe_auto' => '1'
			);
			
			$this->db->insert('pedidos_proveedores',$entrada);
		}
	}
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return false;
			} else {
				$this->db->trans_commit();
				return true;
			}
    }
    
    //DEVUELVE TODOS LOS PRODUCTOS DE TODOS LOS ALMACENES
    public function get_productos_almacenes() {	
        $almacenes = $this->get_almacenes();
        $productos = array();
        
        for ($x=0; $x<count($almacenes); $x++) {            
            $tabla = 'productos_almacenes';
            if (isset($almacenes[$x]->al_tipo)) {
		if ($almacenes[$x]->al_tipo == 1) {
                    $tabla = 'productos_reciclados_almacenes'; 
		}
                
                $aux = $this->db->from($tabla)
                    ->join('almacenes', 'almacenes.al_id = '.$tabla.'.al_id')
                    ->join('productos_proveedores', 'productos_proveedores.product_id = '.$tabla.'.product_id')
                    ->where($tabla.'.al_id',$almacenes[$x]->al_id)
                    ->where($tabla.'.em_id',$this->session->userdata('em_id'))
                    ->get()->result();
                
                $productos = array_merge($productos, $aux);
            }
        }
        	
	return $productos;
    }
	
	public function get_id_producto($pro_id, $product_nombre, $product_referencia, $product_caracteristicas) {
		$producto = $this->db->from('productos_proveedores')
            ->where('pro_id',$pro_id)
			->where('product_nombre',$product_nombre)
			->where('product_referencia',$product_referencia)
			->where('product_caracteristicas',$product_caracteristicas)
			->get()->row();
			
		return $producto->product_id;
	}
    
    //GUARDAMOS LA ENTRADA MANUAL
    public function set_entrada_manual($producto) {
		$this->db->trans_begin();
		$cantProductos = count($producto['product_id']);

        //PREGUNTAMOS SI HEMOS SELECCIONADO UN PRODUCTO
		if (($cantProductos > 0) && ($producto['nuevo_producto'] == 0)) {
			
				$product_id = implode(";", $producto['product_id']);
				$pra_id = implode(";", $producto['pra_id']);
				$product_cantidad = implode(";", $producto['product_cantidad']);
				
				for($i=0;$i<$cantProductos;$i++) {
					$cantidad_recibido_almacen[$i] = $producto['product_id'][$i].','.$producto['product_cantidad'][$i].','.$producto['al_id'][$i];
				}
				
				$product_cantidad_recibido_almacen = implode(";", $cantidad_recibido_almacen);
				
				if ($producto['pr_id'] == '') { //Si no tiene cotizacion asignada
					$producto['pr_id'] = 0;
				}
				
	
				//GUARDAMOS EL PRODUCTO EN LA ENTRADA
				$aux = array(
					'em_id' => $this->session->userdata('em_id'),
					'product_id' => $product_id.';'.$cantProductos,
					'pra_id' => $pra_id.';',
					'product_cantidad' => $product_cantidad.';',
					'pe_fecha' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
					'fecha_aprobacion' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
					'fecha_completado' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
					'fecha_aprobado_dept_prod' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
					'emp_id' => $this->session->userdata('emp_id'),
					'pe_estado' => 4, //Completado
					'product_cantidad_recibido' => $product_cantidad.';',
					'pe_observaciones_empresa' => $producto['pe_observaciones_empresa'],
					'pe_observaciones_proveedor' => '',
					'product_cantidad_recibido_almacen' => $product_cantidad_recibido_almacen.';',
					'emp_id_recepcion' => $this->session->userdata('emp_id'),
					'emp_id_aprobado' => $this->session->userdata('emp_id'),
					'emp_id_aprobado_dept_prod' => $this->session->userdata('emp_id'),
					'pr_id' => $producto['pr_id']
				);
				$this->db->insert('pedidos_proveedores', $aux);
				$id = $this->db->insert_id();

				//ACTUALIZAMOS STOCK DEL PRODUCTO EN EL ALMACEN
				for($i=0; $i<$cantProductos; $i++) {
					
					if ($producto['al_tipo'][$i] == 1) {
						$tabla = 'productos_reciclados_almacenes'; 
					} else {
						$tabla = 'productos_almacenes';
					}
			
					$old_producto = $this->get_almacen_producto($producto['al_id'][$i],$producto['product_id'][$i]);
					
					$aux2 = array(
						'product_cantidad' => (int)$producto['product_cantidad'][$i] + (int)$old_producto->product_cantidad
					);
					
					$this->db->where('al_id', $producto['al_id'][$i]);
					$this->db->where('product_id', $producto['product_id'][$i]);
					if ($producto['al_tipo'][$i] == 1) {
						$this->db->where('pra_id', $producto['pra_id'][$i]);
					}
					$this->db->where('em_id', $this->session->userdata('em_id'));
					$this->db->update($tabla, $aux2);
			
				}
			
        } else {
            //ES UN PRODUCTO NUEVO Y DEBEMOS AÑADIRLO
			
			$pro_id = implode(";", $producto['pro_id']);
			$product_nombre = implode(";", $producto['product_nombre']);
			$product_precio = implode(";", $producto['product_precio']);
			$product_cantidad = implode(";", $producto['product_cantidad']);
			$product_referencia = implode(";", $producto['product_referencia']);
			$product_tienda = implode(";", $producto['product_tienda']);
			
			$product_ids = array();
			$product_cantidad_recibido_almacen = array();
			
			for($i=0; $i<count($producto['pro_id']); $i++) {
				$aux = array(
					'pro_id' => $producto['pro_id'][$i],
					'em_id' => $this->session->userdata('em_id'),
					'product_nombre' => $producto['product_nombre'][$i],
					'product_precio' => $producto['product_precio'][$i],
					'product_referencia' => $producto['product_referencia'][$i],
					'product_tienda' => $producto['product_tienda'][$i]
				);
				$this->db->insert('productos_proveedores', $aux);
				
				//RECOGEMOS EL ID DEL ULTIMO PRODUCTO AÑADIDO
				array_push($product_ids, $this->db->insert_id());
				array_push($product_cantidad_recibido_almacen, $this->db->insert_id().','.$producto['product_cantidad'][$i].','.$producto['al_id']);
			}
			
			$product_id = implode(";", $product_ids);
			$product_cantidad_recibido_almacen = implode(";", $product_cantidad_recibido_almacen);
            
            //GUARDAMOS EL PRODUCTO EN LA ENTRADA
			if ($producto['pr_id'] == '') { //Si no tiene cotizacion asignada
				$producto['pr_id'] = 0;
			}
			$aux2 = array(
                'em_id' => $this->session->userdata('em_id'),
                'product_id' => $product_id.';',
                'product_cantidad' => $product_cantidad.';',
                'pe_fecha' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
                'fecha_aprobacion' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
                'fecha_completado' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
                'emp_id' => $this->session->userdata('emp_id'),
                'pe_estado' => 4, //Completado
                'product_cantidad_recibido' => $product_cantidad.';',
                'pe_observaciones_empresa' => $producto['pe_observaciones_empresa'],
                'pe_observaciones_proveedor' => '',
                'product_cantidad_recibido_almacen' => $product_cantidad_recibido_almacen.';',
                'emp_id_recepcion' => $this->session->userdata('emp_id'),
                'emp_id_aprobado' => $this->session->userdata('emp_id'),
                'pr_id' => $producto['pr_id']
            );
            $this->db->insert('pedidos_proveedores', $aux2);
			$id = $this->db->insert_id();
			
			$x=0;			
			foreach($product_ids as $product_id) {
				
				if ($producto['al_tipo'] == 1) {
					$tabla = 'productos_reciclados_almacenes'; 
				} else {
					$tabla = 'productos_almacenes';
				}
				
				//GUARDAMOS EL PRODUCTO EN EL ALMACEN
				$aux3 = array(
					'al_id' => $producto['al_id'],
					'em_id' => $this->session->userdata('em_id'),
					'product_id' => $product_id,
					'product_cantidad' => $producto['product_cantidad'][$x]
				);
				$this->db->insert($tabla, $aux3);
				$x++;
			}
			
			//SI VIENE EL PRESUPUESTO
			/*if($producto['pr_id']>0) {
				//GENERAMOS LAS SALIDAS
				$x=0;
			
				foreach($product_ids as $product_id) {
	
					$tabla = 'productos_almacenes';
			
					//ACTUALIZAR STOCK
					//ELIMINAMOS EL PRODUCTO DEL ALMACEN DE ORIGEN	
					$this->db->where('al_id', $producto['al_id']);
					$this->db->where('product_id', $product_id );
					$this->db->where('em_id', $this->session->userdata('em_id'));
					$this->db->delete($tabla);
					
					$aux2 = array();
					
					$presupuesto = $this->db->from('presupuestos')
						->select('mantenimientos.man_id')
						->join('mantenimientos', 'mantenimientos.man_id = presupuestos.man_id')
						->where('presupuestos.pr_id', $producto['pr_id'])	
						->get()->row_array();
					
					$aux2['product_id'] = $product_id;
					$aux2['al_id'] = $producto['al_id'];
					$aux2['man_id'] = $presupuesto['man_id'];
					$aux2['sa_cantidad'] = $producto['product_cantidad'][$x];
					$aux2['sa_temporal'] = 0;
					
					//ENVIAR
					$aux2['sa_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
					$aux2['em_id'] = $this->session->userdata('em_id');
					$aux2['emp_id'] = $this->session->userdata('emp_id');
					$aux2['sa_estado'] = 0;
					$this->db->insert('salidas', $aux2);
					
					$x++;
				}
			}*/
        }
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return $id;
		}
    }

    private function set_bitacora_salida($sa_id,$estado){
		$data = array( 
	        'bi_tipo'		=>  'SA', 
	        'bi_idasociado'	=>  $sa_id, 
	        'bi_accion'		=>  ($estado == 1)?'Aprobar':(($estado == 2)?'Enviar':'Cancelar'),
	        'emp_id'		=>  $this->session->userdata('emp_id'),
	    );
	    
		$this->db->insert('bitacora', $data);
	}
}

/* End of file M_clientes.php */
/* Location: ./application/controllers/M_clientes.php */