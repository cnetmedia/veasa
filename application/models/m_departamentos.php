<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_departamentos extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'departamentos';
    }
    
    //DEVUELVE LOS departamentos O NULL SI NO ENCUENTRA Y EL NUMEO DE TOTAL DE empleados POR DEPARTAMENTO
	public function get_departamentos() {
		$departamentos = $this->db->query('SELECT departamentos.de_id,departamentos.de_nombre,sucursales.su_nombre,sucursales.su_id,COUNT(empleados.de_id) as emp_cantidad, departamentos.de_estado
			FROM '.$this->tabla.'
			LEFT JOIN sucursales ON sucursales.su_id = departamentos.su_id
			LEFT JOIN empleados ON empleados.de_id = departamentos.de_id
			WHERE departamentos.em_id = '.$this->session->userdata("em_id").' AND 
                            sucursales.em_id = '.$this->session->userdata("em_id").'
			GROUP BY departamentos.de_id');
		
		return $departamentos->result();
    }
    
    //DEVUELVE EL DEPARTAMENTO DE LA SUCURSAL
	public function get_departamento($de_nombre,$su_id) {
		$departamento = $this->db->from($this->tabla)
			->where('de_nombre',$de_nombre)
			->where('su_id',$su_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
		
		return $departamento->result();
    }
    
    //DEVUELVE EL DEPARTAMENTO SEGUN SU SUCURSAL
	public function get_departamento_sucursal($su_id) {
		$departamento = $this->db->from($this->tabla)
			->where('su_id',$su_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
		
		return $departamento->result();
    }
    
    //DEVUELVE EL DEPARTAMENTO SEGUN SU ID
	public function get_departamento_id($de_id) {
		$departamento = $this->db->from($this->tabla)
			->where('de_id',$de_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
		
		return $departamento;
    }
    
    //A�ADIMOS O EDITAMOS departamentos
	public function set_departamento($depart) {		
		if ($depart['de_id'] == '') {
			$this->db->trans_begin();
			
			unset($depart['de_id']);
			$depart['de_estado'] = '1';
			$depart['em_id'] = $this->session->userdata('em_id');
			$this->db->insert($this->tabla, $depart);
			$id = $this->db->insert_id();
		} else {
			$this->db->where('de_id', $depart['de_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($this->tabla, $depart); 
			$id = $depart['de_id'];
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
    		$this->db->trans_rollback();
    		return false;
		} else {
    		$this->db->trans_commit();
    		return $id;
		}
		
	}
	
	//CAMBIAMOS EL ESTADO
	public function set_estado($de_id) {
		$departamento = $this->db->from($this->tabla)
			->select('de_estado')
			->where('de_id',$de_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
		
		foreach ($departamento->result() as $row) {
		   $estado = $row->de_estado;
		}
		
		if ($estado == 0) {
			$data = array('de_id'=>$de_id,'de_estado' => '1');
			$this->db->where('de_id', $de_id);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update($this->tabla, $data); 
		} else {
			$data = array('de_id'=>$de_id,'de_estado' => '0');
			$this->db->where('de_id', $de_id);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update($this->tabla, $data); 
		}

		$this->set_bitacora_depatamento($de_id,$estado);
		
		return $this->db->trans_status();
	}

	private function set_bitacora_depatamento($de_id,$estado){
		$data = array( 
	        'bi_tipo'		=>  'DE', 
	        'bi_idasociado'	=>  $de_id, 
	        'bi_accion'		=>  ($estado == 0)?'Habilitar':'Deshabilitar',
	        'emp_id'		=>  $this->session->userdata('emp_id'),
	    );
	    
		$this->db->insert('bitacora', $data);
	}
}

/* End of file m_departamentos.php */
/* Location: ./application/controllers/m_departamentos.php */