<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_informes extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'informes';
    }
    
    function conectados() {
		$conectados = $this->db->from('ci_sessions')
			->get();
			
		return $conectados->result();
	}
}

/* End of file m_informes.php */
/* Location: ./application/controllers/m_informes.php */