<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_presupuestos extends MY_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'presupuestos';
    }
    
    //DEVUELVE LOS presupuestos O NULL SI NO ENCUENTRA
	public function get_presupuestos() {
		$presupuestos = $this->db->from($this->tabla)
			->join('clientes', 'clientes.cl_id = presupuestos.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
            ->join('mantenimientos', 'mantenimientos.man_id = presupuestos.man_id')
			->where('presupuestos.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();
		
		return $presupuestos->result();
    }	

    public function get_presupuestos_anulados() {
		$presupuestos = $this->db->from($this->tabla)
			->join('clientes', 'clientes.cl_id = presupuestos.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
            ->join('mantenimientos', 'mantenimientos.man_id = presupuestos.man_id')
			->where('presupuestos.em_id',$this->session->userdata('em_id'))
            ->where('sucursales.em_id',$this->session->userdata('em_id'))
			->where('presupuestos.pr_estado',4)
			->get();
		
		return $presupuestos->result();
    }
    
    //DEVUELVE LOS DATOS DEL PRESUPUESTO SEGUN SU FA_ID
	public function get_presupuesto($pr_id) {	
		$presupuesto = $this->db->from($this->tabla)
			->select('presupuestos.*,sucursales.*,countries.*,empresas.*,clientes.*, empleados.emp_nombre, empleados.emp_apellido1, empleados.emp_apellido2, mantenimientos.man_nombre, mantenimientos.man_latitud, mantenimientos.man_longitud, mantenimientos.man_id')
			->join('empleados', 'empleados.emp_id = presupuestos.emp_id')
			->join('clientes', 'clientes.cl_id = presupuestos.cl_id')
			->join('sucursales', 'sucursales.su_id = clientes.su_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
			->join('empresas', 'empresas.em_id = presupuestos.em_id')
			->join('facturas', 'facturas.pr_id = presupuestos.pr_id','left')
            ->join('mantenimientos', 'mantenimientos.man_id = presupuestos.man_id')
			->where('presupuestos.pr_id',$pr_id)	
			->where('presupuestos.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $presupuesto;
	}
	
	//OBTENEMOS LOS PRESUPUESTOS DEL MANTENIMIENTO
	public function get_presupuestos_mantenimiento($man_id, $pr_id=NULL) {
		$presupuestos = $this->db->from($this->tabla)
			->where('man_id',$man_id)	
			->where('em_id',$this->session->userdata('em_id'))
			->order_by('pr_fecha','desc');

		if(!empty($pr_id)){
			$presupuestos = $presupuestos->where('pr_id',$pr_id);
		}			
			
		return $presupuestos->get()->result();
	}
	
	//AÑADIMOS O EDITAR presupuestos
	public function set_presupuesto($presupuesto) {	
		if ($presupuesto['pr_id'] == '') {
			//USAMOS CONSULTAS TRANSACIONALES PARA EJECUTAR VARIAS A LA VEZ
			$this->db->trans_begin();
			
            //COMPROBAMOS AÑO Y CONTADORES
            $this->contadores();
                        
			//RECUPERAMOS EL CONTADOR DEL PRESUPUESTO
			$contador = $this->db->query('SELECT * FROM contadores WHERE em_id='.$this->session->userdata("em_id").' AND co_tipo="2" AND su_id='.$presupuesto['su_id']);
			
			foreach ($contador->result() as $row) {
	   			$numero = $row->co_contador + 1;
	   			$anio = $row->co_year;
			}
			$numero_presupuesto = 'C'.$presupuesto['su_id'].$anio.$numero;
			
			//SUMAMOS 1 AL CONTADOR
			$this->db->query('UPDATE contadores SET co_contador = '.$numero.' WHERE em_id='.$this->session->userdata("em_id").' AND co_tipo=2'.' AND su_id='.$presupuesto["su_id"]);
			
			//APLICAMOS LOS DATOS AL PRESUPUESTO
			$presupuesto['em_id'] = $this->session->userdata('em_id');
			$presupuesto['pr_fecha'] = fechaHora_actual($presupuesto['su_zona_horaria']);
			$presupuesto['pr_numero'] = $numero_presupuesto;
			$presupuesto['pr_estado'] = '0';
			$presupuesto['emp_id'] = $this->session->userdata('emp_id');
			unset($presupuesto['su_zona_horaria']);
			unset($presupuesto['su_id']);
			unset($presupuesto['pr_id']);
			$this->db->insert($this->tabla,$presupuesto);
			$id = $this->db->insert_id();

		} else {
			unset($presupuesto['su_id']);
			unset($presupuesto['cl_id']);
			unset($presupuesto['su_zona_horaria']);
			$this->db->where('pr_id', $presupuesto['pr_id']);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update($this->tabla, $presupuesto); 
			$id = $presupuesto['pr_id'];
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
		}
	}
	
	//GENERAMOS LA FACTURA DEL PERSUPUESTO
	public function set_cerrar_presupuesto($presupuesto) {
		//USAMOS CONSULTAS TRANSACIONALES PARA EJECUTAR VARIAS A LA VEZ
		$this->db->trans_begin();
		
		//APLICAMOS LOS DATOS AL PRESUPUESTO
		$this->db->query('UPDATE '.$this->tabla.' SET pr_estado=1 WHERE pr_id='.$presupuesto['pr_id'].' AND em_id='.$this->session->userdata("em_id"));
		
		//DATOS DE LA SUCURSAL PARA GENERAR LA FECHA DE LA FACTURA
		$sucursal = $this->db->query('SELECT * FROM sucursales WHERE em_id='.$this->session->userdata("em_id").' AND su_id='.$presupuesto['su_id']);
		
		foreach ($sucursal->result() as $row) {
   			$zona = $row->su_zona_horaria;
		}
		
		//GENERAMOS LA FACTURA
		$datos = array(
			'cl_id' => $presupuesto['cl_id'],
			'fa_numero' => '',
			'fa_servicio' => $presupuesto['pr_servicio'],
			'fa_precio' => $presupuesto['pr_precio'],
			'fa_descuento' => $presupuesto['pr_descuento'],
			'fa_fecha' => fechaHora_actual($zona),
			'fa_total' => $presupuesto['pr_total'],
			'fa_ganancia' => $presupuesto['pr_ganancia'],
			'fa_iva' => $presupuesto['pr_iva'],
			'fa_cantidad' => $presupuesto['pr_cantidad'],
			'fa_estado' => '0',
			'fa_costos' => $presupuesto['pr_costos'],
			'pr_id' => $presupuesto['pr_id'],
			'em_id' => $this->session->userdata('em_id'),
			'emp_id' => $this->session->userdata('emp_id'),
            'man_id' => $presupuesto['man_id']
		);
		
		$this->db->insert('facturas', $datos);
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
    		$this->db->trans_rollback();
		} else {
    		$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
	
	//ANULAMOS UN PERSUPUESTO
	public function set_anular_presupuesto($presupuesto) {
		//USAMOS CONSULTAS TRANSACIONALES PARA EJECUTAR VARIAS A LA VEZ
		$this->db->trans_begin();
		
		//APLICAMOS LOS DATOS AL PRESUPUESTO
		$this->db->query('UPDATE '.$this->tabla.' SET pr_estado=4 WHERE pr_id='.$presupuesto['pr_id'].' AND em_id='.$this->session->userdata("em_id"));
		
		//BUSCAMOS TODAS LAS FACTURAS ASOCIADAS A ESTE PRESUPUESTO PARA ANULARLAS
		$this->db->query('UPDATE facturas SET fa_estado=4 WHERE pr_id='.$presupuesto['pr_id']);
		
		//BUSCAMOS TODAS LAS FACTURAS RECTIFICTIVAS ASOCIADAS A ESTE PRESUPUESTO PARA ANULARLAS
		//$this->db->query('UPDATE facturas SET fa_estado=4 WHERE pr_id='.$presupuesto['pr_id']);
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
    		$this->db->trans_rollback();
		} else {
    		$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
	
}

/* End of file M_presupuestos.php */
/* Location: ./application/controllers/M_presupuestos.php */