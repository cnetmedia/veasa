<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_empresa extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'empresas';
    }
    
    //DEVUELVE LA FILA DE LA EMPRESA O NULL SI NO EXISTE
	public function get_empresa($ident) {
        $empresa = $this->db->from($this->tabla)
			->where('em_id',$ident)
			->get()->row();
		return $empresa;
    }
	
	//AÑADIMOS O EDITAMOS LA EMPRESA
	public function set_empresa($empresa) {
		$empresa['em_id'] = $this->session->userdata('em_id');
		
		$empresa['em_cif'] = strtoupper($empresa['em_cif']);
		$ruta = PRIVADO.$empresa['em_cif'];
		
		unset($empresa['foto']);
		
		$carpeta_publica = 'veasa.visualcode.es';
		
		if ($empresa['em_id'] == '') {
			unset($empresa['em_id']);
			
			$this->db->insert($this->tabla, $empresa);
			//SE CREAN LAS CARPETAS CORRECTAS
			mkdir($ruta, 0755);
			mkdir($ruta.'/../../'.$carpeta_publica.'/company', 0755); /* CARPETA PUBLICA */
			mkdir($ruta.'/cl', 0755); /* CLIENTES */
			mkdir($ruta.'/emp', 0755); /* EMPLEDOS */
			mkdir($ruta.'/ma', 0755); /* MATERIALES */
			mkdir($ruta.'/in', 0755); /* INCIDENCIAS */
			mkdir($ruta.'/inv', 0755); /* INCIDENCIAS AVANZADAS */
			mkdir($ruta.'/pro', 0755); /* PROVEEDORES */
			mkdir($ruta.'/pro_product', 0755); /* PRODUCTOS PROVEEDORES - TIENDA */
			copy(PRIVADO.'index.html', $ruta.'/index.html');
			copy(PRIVADO.'index.html', $ruta.'/cl/index.html');
			copy(PRIVADO.'index.html', $ruta.'/emp/index.html');
			copy(PRIVADO.'index.html', $ruta.'/ma/index.html');
			copy(PRIVADO.'index.html', $ruta.'/in/index.html');
			copy(PRIVADO.'index.html', $ruta.'/inv/index.html');
			copy(PRIVADO.'index.html', $ruta.'/pro/index.html');
			copy(PRIVADO.'index.html', $ruta.'/pro_product/index.html');
			if (empty($_FILES['foto'])) {
				copy(PRIVADO.'logo.jpg', $ruta.'/logo.jpg');
				copy(PRIVADO.'logo.jpg', PRIVADO.'../../'.$carpeta_publica.'/company/'.$empresa['em_cif'].'/logo.jpg');
			}
		} else {
			$this->db->where('em_id', $empresa['em_id']);
			$this->db->update($this->tabla, $empresa); 
		}
		
		if (!empty($_FILES['foto'])) {
			$name2 = $_FILES['foto']['name']; //get the name of the image
			list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
			$actual_image_name2 = "logo.".$ext; //actual image name going to store in your folder
			$tmp2 = $_FILES['foto']['tmp_name']; //get the temporary uploaded image name
			move_uploaded_file($tmp2, $ruta.'/'.$actual_image_name2); //move the file to the folder
			
			$config['image_library'] = 'GD2';
			$config['source_image'] = $ruta.'/logo.jpg';
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['master_dim'] = 'auto';
			$config['width'] = 370;
			$config['height'] = 90;
			$config['quality'] = 90;
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			copy(PRIVADO.$empresa['em_cif'].'/logo.jpg', PRIVADO.'../../'.$carpeta_publica.'/company/'.$empresa['em_cif'].'/logo.jpg');
		}
	}
}

/* End of file m_empresa.php */
/* Location: ./application/controllers/m_empresa.php */