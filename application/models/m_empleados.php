<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_empleados extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'empleados';
    }
	
	//DEVUELVE LA FILA DEL EMPLEADO SEGUN SU EMAIL, O NULL SI NO EXISTE
	public function get_empleado_email($ident) {
        $empleado = $this->db->from($this->tabla)
        	->join('departamentos', 'departamentos.de_id = empleados.de_id')
        	->join('sucursales', 'sucursales.su_id = departamentos.su_id')
        	->join('empresas', 'empresas.em_id = sucursales.em_id')
			->where('empleados.emp_email',$ident)
			->get()->row();
		return $empleado;
    }
    
    //DEVUELVE LOS DATOS DEL EMPLEADO SEGUN SU DNI
	/*public function get_empleado_dni($emp_dni) {	
		$empleado = $this->db->from($this->tabla)
			->where('emp_dni',$emp_dni)
			->get()->row();
			
		return $empleado;
	}*/
	
	public function get_empleado_dni($emp_dni) {	
		$empleado = $this->db->from($this->tabla)
			->join('departamentos', 'departamentos.de_id = empleados.de_id')
        	->join('sucursales', 'sucursales.su_id = departamentos.su_id')
        	->join('empresas', 'empresas.em_id = sucursales.em_id')
			->where('empleados.emp_dni',$emp_dni)
			->get()->row();
			
		return $empleado;
	}
	
	//DEVUELVE LOS DATOS DEL EMPLEADO SEGUN SU ID
	public function get_empleado_id($emp_id) {
		$empleado = $this->db->from($this->tabla)
			->select('empleados.emp_nombre,empleados.emp_apellido1,empleados.emp_apellido2,empleados.emp_telefono,empleados.emp_email,empleados.emp_dni,empleados.emp_tipo,empleados.emp_id,empleados.emp_estado,empleados.emp_registro')
			->join('departamentos', 'departamentos.de_id = empleados.de_id')
			->join('sucursales', 'sucursales.su_id = departamentos.su_id')
			->where('empleados.em_id',$this->session->userdata('em_id'))
			->where('emp_id',$emp_id)
			->get()->row();
		
		return $empleado;
    }
    
    //DEVUELVE LOS empleados O NULL SI NO ENCUENTRA
	public function get_empleados() {
		$empleado = $this->db->from($this->tabla)
			->select('empleados.emp_nombre,empleados.emp_apellido1,empleados.emp_apellido2,empleados.emp_telefono,empleados.emp_email,empleados.emp_dni,empleados.emp_tipo,empleados.emp_id,empleados.emp_estado,empleados.emp_registro,departamentos.*,sucursales.*')
			->join('departamentos', 'departamentos.de_id = empleados.de_id')
			->join('sucursales', 'sucursales.su_id = departamentos.su_id')
			->where('sucursales.em_id',$this->session->userdata('em_id'))
                        ->where('empleados.em_id',$this->session->userdata('em_id'))
			->get();
		
		return $empleado->result();
    }
    
    //DEVUELVE LOS DATOS DEL EMPLEADO SEGUN SU EMP_ID
	public function get_empleado($emp_id) {	
		$empleado = $this->db->from($this->tabla)
			->where('emp_id',$emp_id)
			->where('empleados.em_id',$this->session->userdata('em_id'))
			->join('empresas', 'empresas.em_id = empleados.em_id')
			->get()->row();
			
		return $empleado;
	}
	
	//DEVUELVE LOS EMPLEADOS SEGUN SU DEPARTAMENTO
	public function get_empleados_departamento($su_id,$de_id) {
		$empleados = $this->db->from($this->tabla)
			->select('departamentos.*,sucursales.*,empleados.emp_nombre,empleados.emp_apellido1,empleados.emp_apellido2,empleados.emp_id,empleados.emp_estado,empleados.emp_registro')
			->join('departamentos', 'departamentos.de_id = empleados.de_id')
			->join('sucursales', 'sucursales.su_id = departamentos.su_id')
			->where('sucursales.su_id',$su_id)
			->where('departamentos.de_id',$de_id)
			->where('empleados.em_id',$this->session->userdata('em_id'))
			->get();
		
		return $empleados->result();
    }
    
    //DEVUELVE LOS EMPLEADOS SEGUN SU SUCURSAL
	public function get_empleados_sucursal($su_id) {
		$empleados = $this->db->from($this->tabla)
			->select('departamentos.*,sucursales.*,empleados.emp_nombre,empleados.emp_apellido1,empleados.emp_apellido2,empleados.emp_id,empleados.emp_estado,empleados.emp_registro')
			->join('departamentos', 'departamentos.de_id = empleados.de_id')
			->join('sucursales', 'sucursales.su_id = departamentos.su_id')
			->where('sucursales.su_id',$su_id)
			->where('empleados.em_id',$this->session->userdata('em_id'))
			->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();
		
		return $empleados->result();
    }
    
    //DEVUELVE LOS EMPLEADOS LIBRES QUE NO ESTEN TRABAJANDO
	public function get_empleados_libres_trabajos($su_id,$fecha,$tr_id,$emp_id=null) {
		//SEPARAMOS LA FECHA PASADA POR POST
		$fecha = explode(' - ',$fecha);
		$fecha = array_values(array_diff($fecha, array('')));
		$aux = array();
		
		//COMPROBAMOS QUE LAS FECHAS ESTA BIEN: FECHA INICIO MAS PEQUEÑA QUE LA FECHA DE FIN
		if ($fecha[0] < $fecha[1]) {
			//OBTENEMOS LOS EMPLEADOS DE LA SUCURSAL			
			$empleados = $this->get_empleados_sucursal($su_id);
			
			//NUEVO TRABAJO
			if ($tr_id == '' || $tr_id == null) {
				
				//OBTENEMOS LOS TRABAJOS DE LA SUCURSAL
				$trabajos = $this->db->query('select * from trabajos where em_id = '.$this->session->userdata("em_id").' and su_id = '.$su_id.' and tr_estado < 2');
				
				//RECORREMOS LOS EMPLEADOS ENCONTRADOS
				foreach ($empleados as $row) {
					//SOLO EMPLEADOS ACTIVOS
					if ($row->emp_estado > 0) {
						//RECORREMOS LOS TRABAJOS
						//CONTADOR DE FALLOS PARA SABER SI UN EMPLEADO ESTA LIBRE
						$cont = 0;
						foreach ($trabajos->result() as $rw) {
							//CREAMOS UN ARRAY DE LOS EMPLEADOS DE CADA TRABAJO
							$emple  = $rw->emp_id;
							$emple = explode(",", $emple);
							$emple = array_values(array_diff($emple, array('')));
							//PREGUTNA SI EL EMPLEADO ESTA EN EL ARRAY DE EMPLEADOS DEL TRABAJO
							if (in_array($row->emp_id,$emple)) {
								//COMPROBAMOS QUE LA FECHA PASADA NO ESTE ENTRE LA FECHA DEL TRABAJO
								if ($fecha[0] <= $rw->tr_fecha_inicio and $fecha[1] >= $rw->tr_fecha_fin) {
									$cont++;
								} else if ($fecha[0] >= $rw->tr_fecha_inicio and $fecha[1] <= $rw->tr_fecha_fin) {
									$cont++;
								} else if ($fecha[0] <= $rw->tr_fecha_inicio) {
									if ($fecha[1] >= $rw->tr_fecha_inicio and $fecha[1] <= $rw->tr_fecha_fin) {
									$cont++;	
									}
								} else if ($fecha[0] >= $rw->tr_fecha_inicio and $fecha[0] <= $rw->tr_fecha_fin) {
									if ($fecha[1] >= $rw->tr_fecha_fin) {
									$cont++;	
									}
								}
							}
						}
							
						//SI NO ENCONTRO CONCURRENCIAS EL EMPLEADO ESTA LIBRE
						if ($cont == 0) {
							$dato = array(
							 	"emp_id" => $row->emp_id,
							 	"emp_nombre" => $row->emp_nombre,
								"emp_apellido1" => $row->emp_apellido1,
								"emp_apellido2" => $row->emp_apellido2,
								"emp_estado" => $row->emp_estado,
								"de_id" => $row->de_id
							);
								
							array_push($aux, $dato);
						}
					}
				}
			} else {
				//EDITAR TRABAJO
				//OBTENEMOS EL TRABAJO A EDITAR
				$trabajo = $this->db->query('select * from trabajos where em_id = '.$this->session->userdata("em_id").' and su_id = '.$su_id.' and tr_id='.$tr_id);
				//ARRAY DE NUEVOS EMPLEADOS SI LOS HUBIESE
				$new_emp = Array();
				
				
				//OBTENEMOS LOS EMPLEADOS DEL TRABAJO A EDITAR
				$old_emp = '';
				foreach ($trabajo->result() as $rw) {
					$old_emp = $rw->emp_id;
				}
				$old_emp = explode(',',$old_emp);
				$old_emp = array_values(array_diff($old_emp, array('')));
			
				//SI LOS EMPLEADOS PASADOS POR POST SON MAS QUE LOS QUE TIENE EL TRABAJO SIGINIFICA QUE QUEREMOS AÑADIR A ESTE TRABAJO MAS EMPLEADOS
				if (count($emp_id) > count($old_emp)) {
					//OBTENEMOS LOS EMPLEADOS NUEVOS QUE SE QUIEREN AÑADIR
					for ($i=0; $i<count($emp_id); $i++) {
						if (!in_array($emp_id[$i],$old_emp)) {
							array_push($new_emp,$emp_id[$i]);
						}
					}
				}
				
				//RECORREMOS LOS NUEVOS EMPLEADOS
				if (count($new_emp) > 0) {
					//OBTENEMOS LOS TRABAJOS DE LA SUCURSAL
					$trabajos = $this->db->query('select * from trabajos where em_id = '.$this->session->userdata("em_id").' and su_id = '.$su_id.' and tr_id != '.$tr_id.' and tr_estado < 2');
					//RECORREMOS LOS NUEVOS EMPLEADOS
					for ($x=0; $x<count($new_emp); $x++) {
						//RECORREMOS LOS TRABAJOS
						//CONTADOR DE FALLOS PARA SABER SI UN EMPLEADO ESTA LIBRE
						$cont = 0;
						foreach ($trabajos->result() as $rwt) {
							//CREAMOS UN ARRAY DE LOS EMPLEADOS QUE TIENE CADA TRABAJO
							$emple  = $rwt->emp_id;
							$emple = explode(",", $emple);
							$emple = array_values(array_diff($emple, array('')));
							//SI ENCUENTRA EL NUEVO EMPLEADO EN EL ARRAY DE LOS EMPLEADOS DE TRABAJO ENTRA
							if (in_array($new_emp[$x],$emple)) {
								//COMPROBAMOS QUE LA FECHA PASADA NO ESTE ENTRE LA FECHA DEL TRABAJO
								if ($fecha[0] <= $rw->tr_fecha_inicio and $fecha[1] >= $rw->tr_fecha_fin) {
									$cont++;
								} else if ($fecha[0] >= $rw->tr_fecha_inicio and $fecha[1] <= $rw->tr_fecha_fin) {
									$cont++;
								} else if ($fecha[0] <= $rw->tr_fecha_inicio) {
									if ($fecha[1] >= $rw->tr_fecha_inicio and $fecha[1] <= $rw->tr_fecha_fin) {
									$cont++;	
									}
								} else if ($fecha[0] >= $rw->tr_fecha_inicio and $fecha[0] <= $rw->tr_fecha_fin) {
									if ($fecha[1] >= $rw->tr_fecha_fin) {
									$cont++;	
									}
								}
							}
						}
							
						//SI ENCONTRO CONCURRENCIAS EL EMPLEADO ESTA OCUPADO
						if ($cont == 0) {
							foreach ($empleados as $ep) {
								if ($ep->emp_estado > 0) {
									if (in_array($ep->emp_id,$new_emp)) {
										$dato = array(
										 	"emp_id" => $ep->emp_id,
										 	"emp_nombre" => $ep->emp_nombre,
											"emp_apellido1" => $ep->emp_apellido1,
											"emp_apellido2" => $ep->emp_apellido2,
											"emp_estado" => $ep->emp_estado,
											"de_id" => $ep->de_id
										);
												
										array_push($aux, $dato);
									}
								}
							}
						}
					}
				}

				//RECORREMOS LOS EMPLEADOS ENCONTRADOS
				foreach ($empleados as $row) {
					//SOLO EMPLEADOS ACTIVOS
					if ($row->emp_estado > 0) {
						//RECORREMOS LOS TRABAJOS
						//CONTADOR DE FALLOS PARA SABER SI UN EMPLEADO ESTA LIBRE
						$cont = 0;
						$rw = $trabajo->result();
							//CREAMOS UN ARRAY DE LOS EMPLEADOS DE CADA TRABAJO
							$emple  = $rw->emp_id;
							$emple = explode(",", $emple);
							$emple = array_values(array_diff($emple, array('')));
							//PREGUTNA SI EL EMPLEADO ESTA EN EL ARRAY DE EMPLEADOS DEL TRABAJO
							if (in_array($row->emp_id,$emple)) {
								//COMPROBAMOS QUE LA FECHA PASADA NO ESTE ENTRE LA FECHA DEL TRABAJO
								if ($fecha[0] <= $rw->tr_fecha_inicio and $fecha[1] >= $rw->tr_fecha_fin) {
									$cont++;
								} else if ($fecha[0] >= $rw->tr_fecha_inicio and $fecha[1] <= $rw->tr_fecha_fin) {
									$cont++;
								} else if ($fecha[0] <= $rw->tr_fecha_inicio) {
									if ($fecha[1] >= $rw->tr_fecha_inicio and $fecha[1] <= $rw->tr_fecha_fin) {
									$cont++;	
									}
								} else if ($fecha[0] >= $rw->tr_fecha_inicio and $fecha[0] <= $rw->tr_fecha_fin) {
									if ($fecha[1] >= $rw->tr_fecha_fin) {
									$cont++;	
									}
								}
							}
						
							
						//SI NO ENCONTRO CONCURRENCIAS EL EMPLEADO ESTA LIBRE
						if ($cont == 0) {
							if (!in_array($row->emp_id,$old_emp)) {
								$dato = array(
								 	"emp_id" => $row->emp_id,
								 	"emp_nombre" => $row->emp_nombre,
									"emp_apellido1" => $row->emp_apellido1,
									"emp_apellido2" => $row->emp_apellido2,
									"emp_estado" => $row->emp_estado,
									"de_id" => $row->de_id
								);
									
								array_push($aux, $dato);
							}
						}
					}
				}
			}
		}	
		return $aux;
    }
	
	//AÑADIMOS O EDITAMOS EL EMPLEADO
	public function set_empleado($empleado,$clave) {
		$this->db->trans_begin();
		
		$empleado['em_id'] = $this->session->userdata('em_id');
		//GUARDAMOS LA CLAVE SI NO ESTA VACIA
		if ($clave != '') {
			$empleado['emp_clave'] = $clave;
		}
		
		$empleado['emp_dni'] = strtoupper($empleado['emp_dni']);
		$empleado['emp_dni'] = str_replace(' ', "-", $empleado['emp_dni']);
		$empleado['emp_dni'] = str_replace('/', "-", $empleado['emp_dni']);
		$ruta = PRIVADO.$this->session->userdata('em_cif').'/emp/'.$empleado['emp_dni'];
		
		unset($empleado['foto']);
		unset($empleado['emp_clave_uno']);
		unset($empleado['emp_clave_dos']);
		unset($empleado['su_id']);
		
		//GENERAMOS EL STRING DE LAS SECCIONES QUE PUEDE VER
		if ($empleado['emp_acceso'] == '1') {
			$ver = $empleado['emp_ver'];
			$aux = '';
			for ($i = 0; $i < count($ver); $i++) {
				$aux = $aux.$ver[$i].',';
			}
			$empleado['emp_ver'] = $aux;
		}else{
			$empleado['emp_ver'] = '';
		}
		
		//PREGUNTAMOS SI ES UN NUEVO EMPLEADO
		if ($empleado['emp_id'] == '') {
			//BORRAMOS EL CAMPO ID EMPLEADO
			unset($empleado['emp_id']);
			
			//SACAMOS LA ZONA HORARIA DE LA SUCURSAL
			$sucursal = $this->db->from('departamentos')
			->where('departamentos.de_id',$empleado['de_id'])
			->join('sucursales', 'sucursales.su_id = departamentos.su_id')
			->where('departamentos.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
			//COGEMOS LA FECHA ACTUAL DEL REGISTRO
			$empleado['emp_registro'] = fechaHora_actual($sucursal->su_zona_horaria);			
			$empleado['emp_estado'] = 1;
			$this->db->insert($this->tabla, $empleado);
			$id = $this->db->insert_id();

			//PREGUNTAMOS SI SE INSERTO BIEN
			if ($this->db->trans_status()) {
				$id_numevo_empleado = $this->db->insert_id();
				//SE CREA LA CARPETA DEL EMPLEADO Y SE COPIA LA FOTO DEL PERFIL POR DEFECTO
				mkdir($ruta, 0755);
				copy(PRIVADO.'index.html', $ruta.'/index.html');
				if (empty($_FILES['foto'])) {
					copy(PRIVADO.'perfil.jpg', $ruta.'/perfil.jpg');
				}
				
				//MANDAMOS EMAIL DE NUEVO USUARIO
				if ($empleado['emp_acceso'] == '1') {
					//AGREGAMOS LA INFO DEL EMPLEADO A LA TABLA DE validaciones DE CUENTA
					$validar = array(
						'em_id' => $this->session->userdata('em_id'),
						'emp_id' => $id_numevo_empleado,
						'va_estado' => '0'
					);
					$this->db->insert('validaciones', $validar);
					
					//GENERAMOS EL EMAIL
					$this->load->library('email');
					$nombre = "=?UTF-8?B?".base64_encode($this->session->userdata('em_nombre'))."=?=";
					$this->email->from(EMAIL, $nombre);
					$this->email->to($empleado['emp_email']);
					$this->email->subject(lang('nuevo.empleado'));
					
					$id = array('emp_id_nuevo' => $id_numevo_empleado, 'nombre' => $empleado['emp_nombre'], 'apellido' => $empleado['emp_apellido1'], 'dni' => $empleado['emp_dni']);
					
					$data = array(
						'BODY' => $this->load->view('emails/v_email_nuevo_empleado', $id, TRUE)
					);
			    	$email = $this->load->view('emails/v_email', $data, TRUE);
			    	$this->email->message($email);
			    	
					$this->email->send();
				}
			}

		//EDITAR EMPLEADO	
		} else {
			$this->db->where('emp_id', $empleado['emp_id']);
			$this->db->where('em_id', $this->session->userdata('em_id'));
			$this->db->update($this->tabla, $empleado);
			$id =  $empleado['emp_id'];
		}
		
		if ($this->db->trans_status()) {
			//SI ENCUENTRA UNA FOTO LA EDITA Y GUARDA
			if (!empty($_FILES['foto'])) {
				$name2 = $_FILES['foto']['name']; //get the name of the image
				list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
				$actual_image_name2 = "/perfil.".$ext; //actual image name going to store in your folder
				$tmp2 = $_FILES['foto']['tmp_name']; //get the temporary uploaded image name
				move_uploaded_file($tmp2, $ruta.$actual_image_name2); //move the file to the folder
				
				$config['image_library'] = 'GD2';
				$config['source_image'] = $ruta.'/perfil.jpg';
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['master_dim'] = 'auto';
				$config['width'] = 378;
				$config['height'] = 378;
				$config['quality'] = 90;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
		}
		
	}
	
	//PREGUNTA SI EXISTE EL EMPLEADO A VALIDAR
	function get_valiar($emp_id) {
		$empleado = $this->db->from('validaciones')
			->where('emp_id',$emp_id)
			->where('va_estado',0)
			->get()->row();
			
		return $empleado;
	}
	
	//VALIDAMOS Y ACTIVAMOS LA CUENTA DEL EMPLEADO
	function set_validar($empleado) {
		$this->db->trans_begin();
		
		//GUARDAMOS LA CLAVE DEL EMPLEADO
		$this->db->where('emp_id',$empleado['emp_id']);
		$this->db->update($this->tabla, $empleado);
		
		//BORRAMOS EL EMPLEADO DE LA TABLA DE EMPLEADOS POR ACTIVAR
		unset($empleado['emp_clave']);
		unset($empleado['va_estado']);
		$this->db->where('emp_id',$empleado['emp_id']);
		$this->db->delete('validaciones', $empleado);
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
		} else {
	    	$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
	
	//RESTAURAR CLAVE DEL EMPLEADO
	function set_recordar_clave($empleado) {
		$this->db->trans_begin();
		
		$aux['em_id'] = $empleado->em_id;
		$aux['emp_id'] = $empleado->emp_id;
		$aux['rc_fecha'] = fechaHora_actual($empleado->su_zona_horaria);
		$this->db->insert('restaurar_clave', $aux);
		
		if ($this->db->trans_status()) {
			//GENERAMOS EL EMAIL
			$this->load->library('email');
			$nombre = "=?UTF-8?B?".base64_encode($empleado->em_nombre)."=?=";
			$this->email->from(EMAIL, $nombre);
			$this->email->to($empleado->emp_email);
			$this->email->subject(lang('restaurar.clave'));
						
			$datos = array(
				'rc_id' => $this->db->insert_id(),
				'emp_id' => $empleado->emp_id,
				'em_nombre' => $empleado->em_nombre
			);
				
			$data = array(
				'BODY' => $this->load->view('emails/v_email_empleado_restaurar_clave', $datos, TRUE),
				'em_nombre' => $empleado->em_nombre
			);
			$email = $this->load->view('emails/v_email', $data, TRUE);
			$this->email->message($email);
				    	
			$this->email->send();
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
		} else {
	    	$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
	
	//OBTENEMOS LOS DATOS PARA SABER SI PUEDE CAMBIAR LA CLAVE O NO
	public function get_restaurar_clave($rc_id,$emp_id) {		
		$empleado = $this->db->from('restaurar_clave')
			->where('rc_id',$rc_id)
			->where('emp_id',$emp_id)
			->get()->row();
			
		return $empleado;
	}
	
	//RESTAURA LA CLAVE DEL EMPLEADO
	public function set_restaurar_clave($empleado) {
		$this->db->trans_begin();
		
		//AGREGAMOS LA CLAVE AL EMPLEADO
		$this->db->where('emp_id',$empleado['emp_id']);
		$this->db->update($this->tabla, $empleado);
		
		//BORRAMOS LA FILA DONDE SE CONFIRMA QUE PUEDA CAMBIAR LA CLAVE
		$this->db->where('emp_id', $empleado['emp_id']);
		$this->db->delete('restaurar_clave'); 
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
		} else {
	    	$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
	
	//CAMBIAMOS EL ESTADO
	public function set_estado($emp_id) {
		$empleado = $this->db->from($this->tabla)
			->select('emp_estado')
			->where('emp_id',$emp_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
		
		foreach ($empleado->result() as $row) {
		   $estado = $row->emp_estado;
		}
		
		if ($estado == 0) {
			$data = array('emp_estado' => '1');
			$this->db->where('emp_id', $emp_id);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update($this->tabla, $data); 
		} else {
			$data = array('emp_estado' => '0');
			$this->db->where('emp_id', $emp_id);
			$this->db->where('em_id',$this->session->userdata('em_id'));
			$this->db->update($this->tabla, $data); 
		}
		
		$this->set_bitacora_empleado($emp_id,$estado);
		return $this->db->trans_status();
	}
	
	//PREGUNTAMOS SI ESTE EMPLEADO YA SE NOTIFICO SU ASISTENCIA DIARIA AL TRABAJO
	public function get_trabajo_asistencia($emp_id,$tr_fecha_inicio,$tr_fecha_fin) {
		//CAPTURAMOS EL DIA ACTUAL
		$fecha_actual = fechaHora_actual($this->session->userdata('su_zona_horaria'));
		$fecha_actual = date("Y-m-d", strtotime($fecha_actual));
		
		//PREGUNTAMOS SI ESTE EMPLEADO YA SE NOTIFICO ESTE DIA
		$emp_asist = $this->db->from('empleados_asistencias')
			->where('emp_id',$emp_id)
			->where('em_id',$this->session->userdata('em_id'))
			->like('ea_fecha',$fecha_actual)
			->get()->result();
		
		//SI SE NOTIFICO YA NO PUEDES VOLVER A NOTIFICARLO, SI NO, SI
		if ($emp_asist != null) {
			/*$ea_fecha = '';
			foreach ($emp_asist as $row) {
				$ea_fecha = date("Y-m-d", strtotime($row->ea_fecha));
				
				
			}
			
			if ($fecha_actual == $ea_fecha) {
				return false;
			}*/
			return false;
		} else {
			//PREGUNTAMOS SI EL DIA ACTUAL ESTA ENTRE LOS DIAS DEL TRABAJO
			$tr_fecha_inicio = date("Y-m-d", strtotime($tr_fecha_inicio));
			$tr_fecha_fin = date("Y-m-d", strtotime($tr_fecha_fin));
			if ($tr_fecha_inicio >= $fecha_actual AND $tr_fecha_fin <= $fecha_actual) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	//GUARDAMOS LA ASISTENCIA DEL EMPLEADO A SU TRABAJO
	public function set_trabajo_asistencia($ea_estado,$emp_id,$tr_id,$man_id) {
		$data = array(
			'emp_id'	=>	$emp_id,
			'ea_estado'	=>	$ea_estado,
			'tr_id'		=>	$tr_id,
			'em_id'		=>	$this->session->userdata('em_id'),
			'ea_fecha'	=>	fechaHora_actual($this->session->userdata('su_zona_horaria'))
		);
		
		$this->db->insert('empleados_asistencias', $data);
		
		$sql = $this->db->trans_status();
		
		//SI SE INSERTO LA ASISTENCIA CORRECTAMENTE Y EL ESTADO ES RETRASO O AUSENTE, ENVIAMOS UN EMAIL
		if ($sql AND $ea_estado > 0) {
			$this->load->model('M_mantenimientos');
			$this->load->model('M_sucursales');
			$this->load->model('M_trabajos');
			
			$empleado = $this->M_empleados->get_empleado($emp_id);
			$mantenimiento = $this->M_mantenimientos->get_mantenimiento($man_id);
			$sucursal = $this->M_sucursales->get_sucursal($this->session->userdata('su_id'));
			$trabajo = $this->M_trabajos->get_trabajo($tr_id);
			
			
			//GENERAMOS EL EMAIL
			$this->load->library('email');
			$nombre = "=?UTF-8?B?".base64_encode($this->session->userdata('em_nombre'))."=?=";
			$this->email->from(EMAIL, $nombre);
			$this->email->to($sucursal->su_email);
			$this->email->subject(lang('empleado.asistencia'));
			
			$data_emp = array(
				'emp_nombre' => $empleado->emp_nombre,
				'emp_apellido1' => $empleado->emp_apellido1,
				'emp_apellido2' => $empleado->emp_apellido2,
				'emp_dni' => $empleado->emp_dni,
				'man_nombre' => $mantenimiento->man_nombre,
				'man_telefono' => $mantenimiento->man_telefono,
				'man_email' => $mantenimiento->man_email,
				'man_contacto' => $mantenimiento->man_contacto,
				'tr_fecha_inicio' => $trabajo->tr_fecha_inicio,
				'fecha_actual'		=>	$data['ea_fecha'],
				'ea_estado' => $ea_estado
			);
			
			$data = array(
				'BODY' => $this->load->view('emails/v_email_asistencia_empleado', $data_emp, TRUE)
			);
			$email = $this->load->view('emails/v_email', $data, TRUE);
			$this->email->message($email);
			$this->email->send();
		}
		
		return $sql;
	}

	private function set_bitacora_empleado($emp_id,$estado){
		$data = array( 
	        'bi_tipo'		=>  'EM', 
	        'bi_idasociado'	=>  $emp_id, 
	        'bi_accion'		=>  ($estado == 0)?'Habilitar':'Deshabilitar',
	        'emp_id'		=>  $this->session->userdata('emp_id'),
	    );
	    
		$this->db->insert('bitacora', $data);
	}
}

/* End of file M_empleados.php */
/* Location: ./application/controllers/M_empleados.php */