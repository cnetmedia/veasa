<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_taller extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'taller';
    }
    
    //DEVUELVE LOS DATOS DEL TRABAJO DEL TALLER
	public function get_taller($ta_id) {	
		$taller = $this->db->from($this->tabla)
			->where('ta_id',$ta_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $taller;
	}
	
	//DEVUELVE LOS DATOS DEL TALLER SEGUN SU PRESUPUESTO
	public function get_taller_presupuesto($pr_id) {	
		$taller = $this->db->from($this->tabla)
			->where('pr_id',$pr_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get()->result();
			
		return $taller;
	}
    
    //DEVUELVE LOS TRABAJOS DEL TALLER
	public function get_trabajos_taller() {	
		$trabajos_taller = $this->db->query('SELECT taller.*,mantenimientos.man_nombre,presupuestos.pr_numero 
			FROM '.$this->tabla.' 
			JOIN presupuestos ON presupuestos.pr_id = taller.pr_id
			JOIN mantenimientos ON mantenimientos.man_id = presupuestos.man_id 
			WHERE taller.ta_estado > 0 AND taller.em_id = '.$this->session->userdata("em_id"));
		
		return $trabajos_taller->result();
    }

    //DEVUELVE LOS TRABAJOS DEL TALLER GUARDADO
	public function get_trabajos_taller_guardado() {	
		$trabajos_taller = $this->db->query('SELECT taller.*,mantenimientos.man_nombre,presupuestos.pr_numero 
			FROM '.$this->tabla.' 
			JOIN presupuestos ON presupuestos.pr_id = taller.pr_id
			JOIN mantenimientos ON mantenimientos.man_id = presupuestos.man_id 
			WHERE taller.ta_estado = 0 AND taller.em_id = '.$this->session->userdata("em_id"));
		
		return $trabajos_taller->result();
    }
	
	//DEVUELVE LOS TRABAJOS DEL TALLER
	public function get_trabajos_taller_todos() {	
		$trabajos_taller = $this->db->query('SELECT taller.*,mantenimientos.man_nombre,presupuestos.pr_numero 
			FROM '.$this->tabla.' 
			JOIN presupuestos ON presupuestos.pr_id = taller.pr_id
			JOIN mantenimientos ON mantenimientos.man_id = presupuestos.man_id
			');
		
		return $trabajos_taller->result();
    }
    
    //DEVUELVE LOS PRODUCTOS DEL TALLER
	public function get_producto_taller($product_id) {	
		$productos_taller = $this->db->from('productos_proveedores')
			->select('productos_proveedores.product_id,productos_proveedores.pro_id,productos_proveedores.product_nombre,productos_proveedores.product_referencia,productos_proveedores.product_caracteristicas,proveedores.pro_nombre,productos_reciclados_almacenes.pra_caracteristicas')
			->where('productos_proveedores.product_id',$product_id)
			->where('productos_proveedores.em_id',$this->session->userdata('em_id'))
			->join('productos_almacenes', 'productos_almacenes.product_id = productos_proveedores.product_id','left')
			->join('productos_reciclados_almacenes', 'productos_reciclados_almacenes.product_id = productos_proveedores.product_id','left')
			->join('proveedores', 'proveedores.pro_id = productos_proveedores.pro_id')
			->get()->row();
		
		return $productos_taller;
    }
    
    //AÑADIMOS O EDITAMOS EL TRABAJO DEL TALLER
	public function set_trabajo_taller($trabajo_taller) {		
		$this->load->model('M_almacenes');
		$this->load->model('M_empleados');

		$cat_por_aprobar = $trabajo_taller['data_categorias'];
		unset($trabajo_taller['data_categorias']);

		if(empty($trabajo_taller['id_empleado'])){
			$trabajo_taller['ta_id_empleado_autorizacion'] =  NULL;
			$empleado_autorizacion = NULL;
		}else{
			$trabajo_taller['ta_id_empleado_autorizacion'] =  $trabajo_taller['id_empleado'];
			$empleado_autorizacion = $this->M_empleados->get_empleado_id($trabajo_taller['ta_id_empleado_autorizacion']);
			$empleado = $empleado_autorizacion->emp_nombre . ' ' . $empleado_autorizacion->emp_apellido1 . ' ' . $empleado_autorizacion->emp_apellido2;

			$autorizaciones = array();

			if($trabajo_taller['ta_id'] != ''){
				$taller_old = $this->db->from($this->tabla)
					->select('ta_autorizaciones')
					->where('ta_id', $trabajo_taller['ta_id'])
					->get()->row();
				if(!empty($taller_old->ta_autorizaciones)) {
					$categoria_autorizadas = json_decode($taller_old->ta_autorizaciones,TRUE);
					foreach ($categoria_autorizadas as $key => $categoria) {
						$autorizaciones[$categoria['categoria']] = $categoria;	
					}
				}
			}

			foreach ($cat_por_aprobar as $key => $categoria) {	
				if($categoria['check_value'] == 'true'){					
					$autorizaciones[$categoria['categoria']]['categoria'] = $categoria['categoria'];
					$autorizaciones[$categoria['categoria']]['costo'] = $categoria['costo'];
					$autorizaciones[$categoria['categoria']]['estado'] = 'Autorizado';
					$autorizaciones[$categoria['categoria']]['fecha_aprobacion'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
					$autorizaciones[$categoria['categoria']]['id_usuario_aprobacion'] = $trabajo_taller['ta_id_empleado_autorizacion'];
					$autorizaciones[$categoria['categoria']]['usuario_aprobacion'] = $empleado;					
				}										
			}
			$trabajo_taller['ta_autorizaciones'] = json_encode($autorizaciones);
		}
		unset($trabajo_taller['id_empleado']);		

		$this->db->trans_begin();
		//DEVOLVEMOS A STOCK PRODUCTOS ELIMINADOS DEL TALLER
		$pr_eliminados = $trabajo_taller['pr_eliminados'];
		unset($trabajo_taller['pr_eliminados']);
		$pr_eliminados = explode(';',$pr_eliminados);
		$pr_eliminados = array_filter($pr_eliminados);
		
		for ($i=0; $i<count($pr_eliminados); $i++) {
			if(!empty($pr_eliminados[$i])){
				$info_producto = explode('-',$pr_eliminados[$i]);
				//RECUPERAMOS LA INFORMACION DEL PRODUCTO DEL ALMACEN PARA SABER SU STOCK REAL
				$producto = $this->M_almacenes->get_almacen_producto($info_producto[0],$info_producto[1]);	

				$tabla = 'productos_almacenes';	
				$nuevo_stock = $producto->product_cantidad + $info_producto[2];	

				//ACTUALIZAMOS LA CANTIDAD
				$aux = array(
				    'product_cantidad' => $nuevo_stock
				);
				
				//EDITAMOS EL STOCK
				$this->db->where('al_id',$info_producto[0]);
				$this->db->where('product_id',$info_producto[1]);
				$this->db->where('em_id',$this->session->userdata('em_id'));
				$this->db->update($tabla,$aux);	
			}
		}

		//RECUPERAMOS LOS PRODUCTOS QUE SE AÑADIERON NUEVOS, PARA REBAJAR EL STOCK DEL ALMACEN
		$nuevos = $trabajo_taller['nuevo'];
		unset($trabajo_taller['nuevo']);
		$nuevos = explode(';',$nuevos);
		$nuevos = array_filter($nuevos);

		$product_id = explode(';',$trabajo_taller['product_id']);
		$product_id = array_filter($product_id);
		
		$almacenes = explode(';',$trabajo_taller['al_id']);
		$almacenes = array_filter($almacenes);
		
		$cantidad = explode(';',$trabajo_taller['product_cantidad']);
		$cantidad = array_filter($cantidad);
		
		
		$error_stock = array();
		
		for ($i=0; $i<count($nuevos); $i++) {
			if ($nuevos[$i] == 'true') {
				//RECUPERAMOS LA INFORMACION DEL PRODUCTO DEL ALMACEN PARA SABER SU STOCK REAL
				$producto = $this->M_almacenes->get_almacen_producto($almacenes[$i],$product_id[$i]);
				
				$tabla = 'productos_almacenes';
				if ($producto->al_tipo == 1) {
					$tabla = 'productos_reciclados_almacenes';	
				}
				
				if ($producto->product_cantidad >= $cantidad[$i]) {
					$nuevo_stock = $producto->product_cantidad - $cantidad[$i];
					
					//SI EL STOCK SE QUEDA A CERO, ELIMINAMOS EL PRODUCTO DEL ALMACEN
					if ($nuevo_stock == 0) {
						$this->db->where('al_id',$almacenes[$i]);
						$this->db->where('product_id',$product_id[$i]);
						$this->db->where('em_id', $this->session->userdata('em_id'));
						$this->db->delete($tabla);
					} else {
						//SI AUN QUEDA STOCK ACTUALIZAMOS LA CANTIDAD
						$aux = array(
						    'product_cantidad' => $nuevo_stock
						);
						
						//EDITAMOS EL STOCK
						$this->db->where('al_id',$almacenes[$i]);
						$this->db->where('product_id',$product_id[$i]);
						$this->db->where('em_id',$this->session->userdata('em_id'));
						$this->db->update($tabla,$aux);
					}
				} else {
					//ERROR DE STOCK
					$aux2 = array(
					    'product_id' => $product_id[$i],
					    'stock_disponible' => $producto->product_cantidad
					);
					
					array_push($error_stock,$aux2);
				}
			}
		}
		
		//PREGUNTAMOS SI SALIERON SIMPLES
		$trabajos_enviados = explode(';',$trabajo_taller['ta_trabajo_enviado']);
		//$trabajos_enviados = array_filter($trabajos_enviados);
		unset($trabajo_taller['ta_trabajo_enviado']);
		
		$trabajo = explode(';',$trabajo_taller['ta_trabajos_codigo']);
		$trabajo = array_filter($trabajo);
		
		$ancho_simple = explode(';',$trabajo_taller['ta_trabajos_ancho']);
        $alto_simple = explode(';',$trabajo_taller['ta_trabajos_alto']);
        $long_simple = explode(';',$trabajo_taller['ta_trabajos_longitud']);
        $desc_simple = explode(';',$trabajo_taller['ta_trabajos_descripcion']);
        $reposc_simple = explode(';',$trabajo_taller['ta_trabajos_reposiciones']);
		
		for ($y=0; $y<count($trabajos_enviados); $y++) {
			if ((int)$trabajos_enviados[$y] > 0) {
				$nombre = '<div><strong>'.$trabajo[$y].'</strong></div>';
                            
                $nombre = $nombre.'<div>';
                if ($ancho_simple[$y] != ' ') {
					$nombre = $nombre.lang('ancho').': '.$ancho_simple[$y].' ';
				}
				
				if ($alto_simple[$y] != ' ') {
					$nombre = $nombre.lang('alto').': '.$alto_simple[$y].' ';
				}
				
				if ($long_simple[$y] != ' ') {
					$nombre = $nombre.lang('longitud').': '.$long_simple[$y];
				}
				$nombre = $nombre.'</div>';
				
				if ($desc_simple[$y] != ' ') {
					$nombre = '<div>'.$nombre.lang('descripcion').': '.$desc_simple[$y].'</div>';
				}
				
				$reposicion = 0;
				if ($reposc_simple[$y] == 'true') {
					//$nombre = '<div>'.$nombre.lang('reposicion').': '.lang('si').'</div>';
					$reposicion = 1;
				}
				
				$salida = array(
					'ta_id' => $trabajo_taller['ta_id'],
					'em_id' => $this->session->userdata('em_id'),
					'emp_id' => $this->session->userdata('emp_id'),
					'ta_trabajo' => $nombre,
					'man_id' => $trabajo_taller['man_id'],
					'sa_fecha' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
					'sa_cantidad' => $trabajos_enviados[$y],
                    'sa_estado' => 0,
                    'ta_reposicion' => $reposicion
				);
				
				$this->db->insert('salidas', $salida);
			}
		}
                
        //PREGUNTAMOS SI SALIERON COMPUESTOS
		$trabajos_compuestos_enviados = explode(';',$trabajo_taller['ta_trabajos_compuestos_enviado']);
		//$trabajos_enviados = array_filter($trabajos_enviados);
		unset($trabajo_taller['ta_trabajos_compuestos_enviado']);
		
		$ancho_compuesto = explode(';',$trabajo_taller['ta_trabajos_compuestos_ancho']);
        $alto_compuesto = explode(';',$trabajo_taller['ta_trabajos_compuestos_alto']);
        $long_compuesto = explode(';',$trabajo_taller['ta_trabajos_compuestos_longitud']);
        $desc_compuesto = explode(';',$trabajo_taller['ta_trabajos_compuestos_descripcion']);
        $trabajo_compuesto = explode(';',$trabajo_taller['ta_trabajos_compuestos_codigo']);
        $reposc_compuesto = explode(';',$trabajo_taller['ta_trabajos_compuestos_reposiciones']);
		
		for ($y=0; $y<count($trabajos_compuestos_enviados); $y++) {
            $aux_enviados = explode('|', $trabajos_compuestos_enviados[$y]);
            
            
            $aux_trabajo_compuesto = explode('|',$trabajo_compuesto[$y]);
            $aux_ancho_compuesto = explode('|',$ancho_compuesto[$y]);
			$aux_alto_compuesto = explode('|',$alto_compuesto[$y]);
			$aux_long_compuesto = explode('|',$long_compuesto[$y]);
			$aux_desc_compuesto = explode('|',$desc_compuesto[$y]);
			$aux_reposc_compuesto = explode('|',$reposc_compuesto[$y]);
            
            
            for ($z=0; $z<count($aux_enviados); $z++) {
                if ((int)$aux_enviados[$z] > 0) {
                    $nombre = '<div><strong>'.$aux_trabajo_compuesto[0].'|'.$aux_trabajo_compuesto[$z+1].'</strong></div>';
                    
                    $nombre = $nombre.'<div>';
                    if ($aux_ancho_compuesto[$z] != ' ') {
						$nombre = $nombre.lang('ancho').': '.$aux_ancho_compuesto[$z].' ';
					}
					
					if ($aux_alto_compuesto[$z] != ' ') {
						$nombre = $nombre.lang('alto').': '.$aux_alto_compuesto[$z].' ';
					}
					
					if ($aux_long_compuesto[$z] != ' ') {
						$nombre = $nombre.lang('longitud').': '.$aux_long_compuesto[$z];
					}
					$nombre = $nombre.'</div>';
					
					if ($aux_desc_compuesto[$z] != ' ') {
						$nombre = '<div>'.$nombre.lang('descripcion').': '.$aux_desc_compuesto[$z].'</div>';
					}
					
					$reposicion = 0;
					if ($aux_reposc_compuesto[$z] == 'true') {
						//$nombre = '<div>'.$nombre.lang('reposicion').': '.lang('si').'</div>';
						$reposicion = 1;
					}
                    
                    $salida = array(
						'ta_id' => $trabajo_taller['ta_id'],
						'em_id' => $this->session->userdata('em_id'),
						'emp_id' => $this->session->userdata('emp_id'),
						'ta_trabajo' => $nombre,
						'man_id' => $trabajo_taller['man_id'],
						'sa_fecha' => fechaHora_actual($this->session->userdata('su_zona_horaria')),
						'sa_cantidad' => $aux_enviados[$z],
                        'sa_estado' => 0,
                        'ta_reposicion' => $reposicion
                    );
		
                    $this->db->insert('salidas', $salida);
				}
            }                    
		}
		
		//PREGUNTAMOS SI YA MANDO TODO Y TERMINO PARA GUARDAR LA FECHA DE FIN
		$trabajos_terminados = explode(';',$trabajo_taller['ta_trabajos_terminados']);
		$trabajos_terminados = array_filter($trabajos_terminados);
		
		$trabajos_cantidad = explode(';',$trabajo_taller['ta_trabajos_cantidad']);
		$trabajos_cantidad = array_filter($trabajos_cantidad);                
               
        if ($trabajo_taller['ta_estado'] == 2) {
            $trabajo_taller['ta_terminado'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
        }
		
		//PREGUNTAMOS SI ENCONTRO ERRORES AL ACTUALIZAR EL STOCK DE LOS ALMACENES
		if (count($error_stock) == 0) {
			//NO ENCONTRO ERRORES
			$trabajo_taller['em_id'] = $this->session->userdata('em_id');
			$trabajo_taller['su_id'] = $this->session->userdata('su_id');
			
			//NUEVO TRABAJO TALLER
			if ($trabajo_taller['ta_id'] == '') {
				unset($trabajo_taller['ta_id']);
				unset($trabajo_taller['man_id']);				
				$this->db->insert($this->tabla, $trabajo_taller);
				$id = $this->db->insert_id();
			//EDITAR TRABAJO TALLER
			} else {
				unset($trabajo_taller['man_id']);
				$this->db->where('ta_id', $trabajo_taller['ta_id']);
				$this->db->update($this->tabla, $trabajo_taller);
				$id = $trabajo_taller['ta_id'];
			}
			
			if ($this->db->trans_status() === FALSE) {
		    	$this->db->trans_rollback();
		    	return false;
			} else {
		    	$this->db->trans_commit();
		    	return $id;
		    }
		} else {
			//DEVOLVEMOS LOS ERRORES
			$this->db->trans_rollback();
		    return $error_stock;
		}
	}
	
	//TERMINAR Y CERRAR TALLER
	public function set_cerrar_trabajo_taller($taller) {
		$this->db->trans_begin();
		
		
		//ACTUALIZAMOS EL ESTADO DEL TALLER
		$aux = array('ta_estado' => $taller['ta_estado']);
		unset($taller['ta_estado']);
		$this->db->where('ta_id', $taller['ta_id']);
		$this->db->where('em_id', $this->session->userdata('em_id'));
		$this->db->update($this->tabla, $aux);
		
		//GUARDAMOS LOS PRODUCTOS SOBRANTES (RECICLADO)
		if (isset($taller['pra_reciclaje'])) {
			if ($taller['pra_reciclaje'] != '') {
				$product_id = explode(';',$taller['product_id']);
				$product_id = array_filter($product_id);
				
				$reciclaje = explode(';',$taller['pra_reciclaje']);
				$reciclaje = array_filter($reciclaje);
				
				$cantidad = explode(';',$taller['product_cantidad']);
				$cantidad = array_filter($cantidad);
				
				for ($i=0; $i<count($product_id)-1; $i++) {
					$producto = array(
						'al_id'	=>	$taller['al_id'],
						'em_id'	=>	$this->session->userdata('em_id'),
						'product_id'	=>	$product_id[$i],
						'product_cantidad'	=>	$cantidad[$i],
						'pra_caracteristicas'	=>	$reciclaje[$i]
					);
					
					$this->db->insert('productos_reciclados_almacenes', $producto);
				}
			}
		}
		
		if ($this->db->trans_status() === FALSE) {
		   	$this->db->trans_rollback();
		   	return false;
		} else {
		    $this->db->trans_commit();
		    return true;
		}
	}

	private function set_bitacora_taller($taller){
		$data = array( 
	        'bi_tipo'		=>  'TA', 
	        'bi_idasociado'	=>  $taller['ta_id'], 
	        'emp_id'		=>  $this->session->userdata('emp_id'),
	    );
		$this->db->insert('bitacora', $data);
	}
}