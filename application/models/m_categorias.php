<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_categorias extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'categorias';
    }
    
	public function get_categorias() {
		$categorias = $this->db->query('SELECT * FROM '.$this->tabla);		
		return $categorias->result();
    }
}

/* End of file M_categorias.php */
/* Location: ./application/controllers/M_categorias.php */