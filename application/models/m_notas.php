<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_notas extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'notas';
    }
    
    //DEVUELVE LAS NOTAS O NULL SI NO ENCUENTRA
	public function get_notas() {
		$notas = $this->db->query('SELECT notas.*,COUNT(notas_respuestas.no_id) as respuestas_cantidad
			FROM '.$this->tabla.' 
			LEFT JOIN notas_respuestas ON notas_respuestas.no_id = notas.no_id 
			WHERE notas.em_id = '.$this->session->userdata("em_id").' 
			GROUP BY notas.no_id ORDER BY notas.no_fecha DESC ');
		
		return $notas->result();
    }
    
    //DEVUELVE LOS DATOS DE LA NOTA SEGUN NO_ID
	public function get_nota($no_id) {	
		$nota = $this->db->from($this->tabla)
			->select('notas.*,empleados.emp_nombre,empleados.emp_apellido1,empleados.emp_apellido2,empleados.emp_dni')
			->where('notas.no_id',$no_id)
			->where('notas.em_id',$this->session->userdata('em_id'))
			->join('empleados', 'empleados.emp_id = notas.no_emp_id')
			->get()->row();
			
		return $nota;
	}
	
	//GUARDAR Nota
	public function set_nota($nota) {
		$nota['em_id'] = $this->session->userdata('em_id');
		$nota['no_emp_id'] = $this->session->userdata('emp_id');
		$nota['no_visto'] = $this->session->userdata('emp_id').',';
		
		if ($nota['no_tipo'] == 0) {
			if ($nota['su_id'] != '') {
				//RECUPERAMOS LA SUCURSAL
				$sucursal = $this->db->query('SELECT * FROM sucursales WHERE em_id='.$this->session->userdata("em_id").' AND su_id='.$nota['su_id']);
			
				foreach ($sucursal->result() as $row) {
	   				$zona = $row->su_zona_horaria;
				}
				
				$nota['no_fecha'] = fechaHora_actual($zona);
			} else {
				$nota['no_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			}
			
		} else {
			$nota['no_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			$nota['emp_id'] = $this->session->userdata('emp_id');
			$nota['su_id'] = $this->session->userdata('su_id');
			$nota['de_id'] = $this->session->userdata('de_id');
		}
		
		$this->db->insert($this->tabla, $nota);
		return $this->db->trans_status();
	}
	
	//GUARDAR RESPUESTA
	public function set_respuesta($respuesta) {
		$this->db->trans_begin();
		
		//PONEMOS LA NOTA A VISTA POR LA PERSONA QUE RESPONDIO
		$data = array('no_visto'=>$this->session->userdata("emp_id").",");
		$this->db->where('no_id', $respuesta['no_id']);
		$this->db->where('em_id',$this->session->userdata('em_id'));
		$this->db->update($this->tabla, $data);
		
		//AÑADIMOS LA RESPUESTA
		$respuesta['em_id'] = $this->session->userdata('em_id');
		$respuesta['emp_id'] = $this->session->userdata('emp_id');
		$respuesta['nr_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
		
		$this->db->insert('notas_respuestas', $respuesta);
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
		} else {
	    	$this->db->trans_commit();
		}
		
		return $this->db->trans_status();
	}
	
	//OBTENER LAS RESPUESTAS
	public function get_respuestas($nota) {
		$respuestas = $this->db->from('notas_respuestas')
			->select('notas_respuestas.*,empleados.emp_nombre,empleados.emp_apellido1,empleados.emp_apellido2,empleados.emp_dni')
			->join('empleados', 'empleados.emp_id = notas_respuestas.emp_id')
			->where('notas_respuestas.no_id',$nota['no_id'])
			->where('notas_respuestas.em_id',$this->session->userdata('em_id'))
			->order_by('notas_respuestas.nr_fecha','desc')
			->get();
			
		return $respuestas->result();
	}
	
	//MARCAR LA NOTA COMO VISTA
	public function set_nota_vista($no_id,$visto) {
		$data = array('no_visto'=>$visto);
		$this->db->where('no_id', $no_id);
		$this->db->where('em_id',$this->session->userdata('em_id'));
		$this->db->update($this->tabla, $data);
	}
}

/* End of file M_notas.php */
/* Location: ./application/controllers/M_notas.php */