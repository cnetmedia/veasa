<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proveedores extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'proveedores';
		$this->tablacar = 'carritos';
		$this->tablacarprod = 'carritos_productos';
    }
    
    //DEVUELVE LOS proveedores O NULL SI NO ENCUENTRA
	public function get_proveedores() {
		$proveedor = $this->db->from($this->tabla)
			->join('countries', 'countries.id_countries = proveedores.pro_pais')
			->where('proveedores.em_id',$this->session->userdata('em_id'))
			->get();

		return $proveedor->result();
    }
    
    //DEVUELVE LOS DATOS DEL PROVEEDOR SEGUN SU pro_ID
	public function get_proveedor($pro_id) {	
		$proveedor = $this->db->from($this->tabla)
			->where('pro_id',$pro_id)
			->where('proveedores.em_id',$this->session->userdata('em_id'))
			->join('empresas', 'empresas.em_id = proveedores.em_id')
			->join('countries', 'countries.id_countries = proveedores.pro_pais')
			->get()->row();
			
		return $proveedor;
	}
	
	//DEVUELVE LOS DATOS DEL PROVEEDOR SEGUN SU CIF
	public function get_proveedor_cif($pro_cif) {	
		$proveedor = $this->db->from($this->tabla)
			->where('pro_cif',$pro_cif)
			->where('em_id',$this->session->userdata('em_id'))
			->get();
			
		return $proveedor;
	}
	
	//AÑADIMOS O EDITAMOS EL PROVEEDOR
	public function set_proveedor($proveedor) {
		//USAMOS CONSULTAS TRANSACIONALES PARA EJECUTAR VARIAS A LA VEZ
		$this->db->trans_begin();
		
		$proveedor['em_id'] = $this->session->userdata('em_id');
		
		$proveedor['pro_cif'] = strtoupper($proveedor['pro_cif']);
		$proveedor['pro_cif'] = str_replace(' ', "-", $proveedor['pro_cif']);
		$ruta = PRIVADO.$this->session->userdata('em_cif').'/pro/'.$proveedor['pro_cif'];
		
		unset($proveedor['foto']);
		//NUEVO PROVEEDOR
		if ($proveedor['pro_id'] == '') {
			
			$this->db->insert($this->tabla, $proveedor);
			$id = $this->db->insert_id();
			
			//PREGUNTAMOS SI SE INSERTO BIEN
			if ($this->db->trans_status()) {
				//SE CREA LA CARPETA DEL PROVEEDOR Y SE COPIA LA FOTO DEL PERFIL POR DEFECTO
				mkdir($ruta, 0755);
				copy(PRIVADO.'index.html', $ruta.'/index.html');
				if (empty($_FILES['foto'])) {
					copy(PRIVADO.'logo.jpg', $ruta.'/logo.jpg');
				}
			}
		//EDITAR PROVEEDOR
		} else {
			$this->db->where('pro_id', $proveedor['pro_id']);
			$this->db->update($this->tabla, $proveedor);
			$id = $proveedor['pro_id'];
		}
		
		if ($this->db->trans_status()) {
			//SI ENCUENTRA UNA FOTO LA EDITA Y GUARDA
			if (!empty($_FILES['foto'])) {
				$name2 = $_FILES['foto']['name']; //get the name of the image
				list($txt, $ext) = explode(".", $name2); //extract the name and extension of the image
				$actual_image_name2 = "/logo.".$ext; //actual image name going to store in your folder
				$tmp2 = $_FILES['foto']['tmp_name']; //get the temporary uploaded image name
				move_uploaded_file($tmp2, $ruta.$actual_image_name2); //move the file to the folder
				
				$config['image_library'] = 'GD2';
				$config['source_image'] = $ruta.'/logo.jpg';
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['master_dim'] = 'auto';
				$config['width'] = 370;
				$config['height'] = 90;
				$config['quality'] = 90;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
	    	return $id;
		}
	}
	
	/* PRODUCTOS PROVEEDOR */
	
	//DEVUELVE LOS PRODUCTOS DE LOS PROVEEDORES O NULL SI NO ENCUENTRA
	public function get_productos() {
		$productos = $this->db->from('productos_proveedores')
			->join('proveedores', 'proveedores.pro_id = productos_proveedores.pro_id')
			->join('empresas', 'empresas.em_id = productos_proveedores.em_id')
			->join('sucursales', 'sucursales.su_id = empresas.em_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
			->where('productos_proveedores.em_id',$this->session->userdata('em_id'))
			->where('sucursales.em_id',$this->session->userdata('em_id'))
			->get();

		return $productos->result();
    }
	
	//DEVUELVE LOS PRODUCTOS QUE ESTEN ASOCIADOS A LA TIENDA ONLINE
	public function get_productos_tienda() {
		$productos = $this->db->from('productos_proveedores')
			->join('proveedores', 'proveedores.pro_id = productos_proveedores.pro_id')
			->join('empresas', 'empresas.em_id = productos_proveedores.em_id')
			->join('sucursales', 'sucursales.su_id = empresas.em_id')
			->join('countries', 'countries.id_countries = sucursales.su_pais')
			->where('productos_proveedores.em_id',$this->session->userdata('em_id'))
			->where('sucursales.em_id',$this->session->userdata('em_id'))
			->where('productos_proveedores.product_tienda',1)
			->get();

		return $productos->result();
    }
    
    //DEVUELVE LOS PRODUCTOS DEL PROVEEDOR O NULL SI NO ENCUENTRA
	public function get_productos_proveedor($pro_id) {
		$productos = $this->db->from('productos_proveedores')
			->where('pro_id',$pro_id)
			->where('em_id',$this->session->userdata('em_id'))
			->where('product_tienda',1)
			->get();

		return $productos->result();
    }
    
    //DEVUELVE LOS DATOS DEL PRODUCTO SEGUN SU product_ID
	public function get_producto($product_id) {	
		$producto = $this->db->from('productos_proveedores')
            ->join('proveedores', 'proveedores.pro_id = productos_proveedores.pro_id')
            ->where('productos_proveedores.product_id',$product_id)
			->where('productos_proveedores.em_id',$this->session->userdata('em_id'))
			->get()->row();
			
		return $producto;
	}
	
	public function get_producto_existente($pro_id, $product_nombre, $product_referencia, $product_caracteristicas) {
		$producto = $this->db->from('productos_proveedores')
            ->where('pro_id',$pro_id)
			->where('product_nombre',$product_nombre)
			->where('product_referencia',$product_referencia)
			->where('product_caracteristicas',$product_caracteristicas)
			->get()->row();
			
		return $producto;
	}
	
	//En esta funcion no comparo con las caracteristicas, para poderlas editar
	public function get_producto_existente_sc($pro_id, $product_nombre, $product_referencia) {
		$producto = $this->db->from('productos_proveedores')
            ->where('pro_id',$pro_id)
			->where('product_nombre',$product_nombre)
			->where('product_referencia',$product_referencia)
			->get()->row();
			
		return $producto;
	}
	
	public function get_almacen_existente($al_id, $product_id) {
		$almacen = $this->db->from('productos_almacenes')
            ->where('al_id',$al_id)
			->where('product_id',$product_id)
			->get()->row();
			
		return $almacen;
	}
    
    //AÑADIMOS O EDITAMOS EL PRODUCTO
	public function set_producto($producto) {
		//USAMOS CONSULTAS TRANSACIONALES PARA EJECUTAR VARIAS A LA VEZ
		$this->db->trans_begin();
		
		$producto['em_id'] = $this->session->userdata('em_id');
		
		unset($producto['fotos']);
		unset($producto['archivos']);
		
		if ($producto['product_id'] != '') {
			//Se hizo este ajuste para que se pudieran editar las caracteristicas del producto
			//$producto_buscar = $this->get_producto_existente($producto['pro_id'], $producto['product_nombre'], $producto['product_referencia'], $producto['product_caracteristicas']);
			$producto_buscar = $this->get_producto_existente_sc($producto['pro_id'], $producto['product_nombre'], $producto['product_referencia']);
		}
		
		//NUEVO PRODUCTO
		if ($producto_buscar->product_id == '') {
			//unset($producto['product_id']);
			$this->db->insert('productos_proveedores', $producto);
			$product_id = $this->db->insert_id();
		//EDITAR PRODUCTO
		} else {
			//Si ya el producto estaba en tienda online, se mantiene el valor
			if ($producto_buscar->product_tienda == '1') {
				$producto['product_tienda'] = '1';
			}
			$this->db->where('product_id', $producto_buscar->product_id);
			$this->db->update('productos_proveedores', $producto);
			$product_id = $producto_buscar->product_id;
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
			//AÑADIMOS LAS FOTOS U ARCHIVOS
			if ($this->db->trans_status()) {
				$id = '';
				
				if ($producto['product_id'] == '') {
					$ruta = PRIVADO.$this->session->userdata('em_cif').'/pro_product/'.$product_id;
					//SE CREA LA CARPETA DEL PRODUCTO
					mkdir($ruta, 0755);
					copy(PRIVADO.'index.html', $ruta.'/index.html');
				} else {
					$ruta = PRIVADO.$this->session->userdata('em_cif').'/pro_product/'.$producto['product_id'];
				}			
			}
			
	    	$this->db->trans_commit();
	    	return $product_id;
		}
	}
	
	//GUARDAMOS EL ARRAY IMPORTADO DEL EXCEL DE PRODUCTOS
	public function set_importar_productos($lista) {
		$this->db->trans_begin();
		
		for ($i=0; $i<count($lista); $i++) {
			$lista[$i]['product_tienda'] = '1';
			//Busco si el producto ya existe en la tienda. Si ya existe, lo actualizo. Sino lo crea
			$producto_buscar = $this->get_producto_existente($lista[$i]['pro_id'], $lista[$i]['product_nombre'], $lista[$i]['product_referencia'], $lista[$i]['product_caracteristicas']);
			if ($producto_buscar->product_id == '') {
				$this->db->insert('productos_proveedores',$lista[$i]);
			} else {
				//Si ya el producto estaba en tienda online, se mantiene el valor
				if ($producto_buscar->product_tienda == '1') {
					$lista[$i]['product_tienda'] = '1';
				}
				$this->db->where('product_id', $producto_buscar->product_id);
				$this->db->update('productos_proveedores', $lista[$i]);
			}
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
	}
	
	//GUARDAMOS EL ARRAY IMPORTADO DEL EXCEL DE PRODUCTOS
	public function set_importar_productos_almacen($lista, $almacen) {
		$this->db->trans_begin();
		
		for ($i=0; $i<count($lista); $i++) {
			$product_cantidad = $lista[$i]['product_cantidad'];
			unset($lista[$i]['product_cantidad']);
			//Cuando se agregar un producto a un almacen, este no se debe mostrar en la tienda online
			$lista[$i]['product_tienda'] = '0';
			//Busco si el producto ya existe en la tienda. Si ya existe, lo actualizo. Sino lo crea
			$producto_buscar = $this->get_producto_existente($lista[$i]['pro_id'], $lista[$i]['product_nombre'], $lista[$i]['product_referencia'], $lista[$i]['product_caracteristicas']);
			if ($producto_buscar->product_id == '') {
				$this->db->insert('productos_proveedores',$lista[$i]);
				$product_id = $this->db->insert_id();
			} else {
				//Si ya el producto estaba en tienda online, se mantiene el valor
				if ($producto_buscar->product_tienda == '1') {
					$lista[$i]['product_tienda'] = '1';
				}
				$this->db->where('product_id', $producto_buscar->product_id);
				$this->db->update('productos_proveedores', $lista[$i]);
				$product_id = $producto_buscar->product_id;
			}
			
			$almacen_buscar = $this->get_almacen_existente($almacen, $product_id);
			
			if ($almacen_buscar->al_id == '') {
				$aux = array(
					'product_id' => $product_id,
					'product_cantidad' => $product_cantidad,
					'em_id' => $this->session->userdata('em_id'),
					'al_id' => $almacen
				);
				
				$this->db->insert('productos_almacenes',$aux);
			} else {
				$aux = array(
					'product_id' => $product_id,
					'product_cantidad' => ((int)$product_cantidad + (int)$almacen_buscar->product_cantidad),
					'em_id' => $this->session->userdata('em_id'),
					'al_id' => $almacen
				);
				
				$this->db->where('al_id', $almacen);
				$this->db->where('product_id', $product_id);
				$this->db->update('productos_almacenes',$aux);
			}
			
		}
		
		//SI TODO SALIO BIEN COMPLETA LA ACCION SINO NO
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
	}
	
	//DEVUELVE LOS CARRITOS DEL EMPLEADO O NULL SI NO ENCUENTRA
	public function get_carritos() {
		$carritos = $this->db->from('carritos')
			->join('presupuestos','presupuestos.pr_id = carritos.pr_id','left')
			->join('clientes','clientes.cl_id = presupuestos.cl_id','left')
			->join('empresas', 'empresas.em_id = carritos.em_id')
			->join('mantenimientos', 'mantenimientos.man_id = presupuestos.man_id','left')
			->where('carritos.car_estado',0)
			->get();

		return $carritos->result();
    }
	
	//DEVUELVE EL CARRITO O NULL SI NO ENCUENTRA
	public function get_carrito($car_id) {
		$carrito = $this->db->from('carritos')
			->join('presupuestos','presupuestos.pr_id = carritos.pr_id','left')
			->join('clientes','clientes.cl_id = presupuestos.cl_id','left')
			->join('empresas', 'empresas.em_id = carritos.em_id')
			->where('carritos.car_id',$car_id)
			->get()->row();
			
		return $carrito;
    }
	
	//DEVUELVE LOS proveedores O NULL SI NO ENCUENTRA
	public function get_categorias() {
		$categorias = $this->db->from('categorias')
			->get();

		return $categorias->result();
    }
	
	//ELIMINAMOS EL CARRITO
	public function set_eliminar_carrito($carrito) {
		$this->db->trans_begin();
		
		//GUARDAMOS EL ID DEL CARRITO
		$car_id = $carrito['car_id'];
	
		$this->db->where('car_id', $car_id);
		$this->db->delete('carritos'); 
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
			return true;
		}
	}
	
	//ACTUALIZAMOS EL CARRITO
	public function set_actualizar_carrito($carrito) {
		$this->db->trans_begin();
		
		$carrito['em_id'] = $this->session->userdata('em_id');
		$carrito['emp_id'] = $this->session->userdata('emp_id');
		$carrito['car_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
		
		//GUARDAMOS EL ID DEL CARRITO
		$car_id = $carrito['car_id'];
		unset($carrito['car_id']);
		
		//GUARDAMOS EL ARRAY DE PRODUCTOS
		$productos = $carrito['products'];
		unset($carrito['products']);
		
		//CONVERTIMOS EN STRING EL ARRAY PASADO POR AJAX DESDE EL CARRITO
		$productos_id = array();
		$productos_cantidad = array();
		$productos_cantidad_recibido = array();
		
		foreach($productos as $product) {
			array_push($productos_id,$product['product_id']);
			array_push($productos_cantidad,$product['product_cantidad']);
			array_push($productos_cantidad_recibido,$product['product_cantidad_recibido']);
		}
		
		$carrito['product_id'] = implode(';',$productos_id).';';
		$carrito['product_cantidad'] = implode(';',$productos_cantidad).';';
		$carrito['product_cantidad_recibido'] = implode(';',$productos_cantidad_recibido).';';
		
		$this->db->where('car_id', $car_id);
		$this->db->update('carritos', $carrito);
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
			return true;
		}
	}
	
	//GUARDAMOS EL CARRITO
	public function set_guardar_carrito($carrito) {
		$this->db->trans_begin();
		
		$carrito['em_id'] = $this->session->userdata('em_id');
		$carrito['emp_id'] = $this->session->userdata('emp_id');
		$carrito['car_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
		$carrito['car_estado'] = 0;
		
		//CONVERTIMOS EN ARRAY EL STRING PASADO POR AJAX DESDE EL CARRITO
		$pro_id = explode(';',$carrito['pro_id']);
		$pro_id = array_filter($pro_id);
		unset($carrito['pro_id']);
		
		$this->db->insert('carritos', $carrito);
		$pe_id = $this->db->insert_id();
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return 0;
		} else {
	    	$this->db->trans_commit();
					
			$product_cantidad = explode(';',$carrito['product_cantidad']);
			$product_cantidad = array_filter($product_cantidad);
			
			$product_id = explode(';',$carrito['product_id']);
			$product_id = array_filter($product_id);
					
			//VARIABLES AUXILIARES PARA LOS EMAILS
			$product_cantidad_aux = array();
			$product_nombre_aux = array();
			$product_referencia_aux = array();
			$product_caracteristicas_aux = array();
					
			//RECORREMOS TODOS LOS PROVEEDORES DEL CARRITO
			for ($i=0; $i<count($pro_id); $i++) {
				//RECOGEMOS LOS DATOS DEL PRODUCTO
				$producto = $this->get_producto($product_id[$i]);
				
				//AÑADIMOS EL PRODUCTO A LA LISTA PARA EL EMAIL
				array_push($product_cantidad_aux,$product_cantidad[$i]);
				array_push($product_nombre_aux,$producto->product_nombre);
				array_push($product_referencia_aux,$producto->product_referencia);
				
				//CARACTERISTICAS DEL PRODUCTO
				$caracteristicas = $producto->product_caracteristicas;
				$caracteristicas = explode(';',$caracteristicas);
				$caracteristicas = array_filter($caracteristicas);
				$opcion_aux = '';
				for ($y=0; $y<count($caracteristicas); $y++) {
					$aux = explode('/',$caracteristicas[$y]);
					$aux = array_filter($aux);
					//OCULTAMOS EL PRECIO DE VENTA AL PROVEEDOR
					if (strpos(strtolower($aux[0]), 'tarifa') === FALSE) {
						$opcion_aux = $opcion_aux.$aux[0].': '.$aux[1].', ';
					}
					//$opcion_aux = $opcion_aux.$aux[0].': '.$aux[1].', ';
				}
				$opcion_aux = substr($opcion_aux, 0, -2);
				array_push($product_caracteristicas_aux, $opcion_aux);
			}
					
			//DESTRUIMOS EL CARRITO
			$this->load->library('Cart');
			$this->cart->destroy();
	    	
	    	return $pe_id;
		}
	}
	
	//PROCESAMOS EL PEDIDO Y LO GUARDAMOS
	public function set_procesar_pedido($carrito) {
		$this->db->trans_begin();
		
		$carrito['em_id'] = $this->session->userdata('em_id');
		$carrito['emp_id'] = $this->session->userdata('emp_id');
	
		//CONVERTIMOS EN ARRAY EL STRING PASADO POR AJAX DESDE EL CARRITO
		$pro_id = explode(';',$carrito['pro_id']);
		$pro_id = array_filter($pro_id);
		unset($carrito['pro_id']);
		
		
		if($carrito['car_id']) {
			$carrito['car_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
			$carrito['car_estado'] = 1;
			$carrito['car_observaciones_empresa'] = $carrito['pe_observaciones_empresa'];
			$carrito['car_observaciones_proveedor'] = $carrito['pe_observaciones_proveedor'];
			unset($carrito['pe_observaciones_empresa']);
			unset($carrito['pe_observaciones_proveedor']);
			
			//GUARDAMOS EL ID DEL CARRITO
			$car_id = $carrito['car_id'];
			unset($carrito['car_id']);
			
			//GUARDAMOS EL ARRAY DE PRODUCTOS
			$productos = $carrito['products'];
			unset($carrito['products']);
	
			$this->db->where('car_id', $car_id);
			$this->db->update('carritos', $carrito);
			
			unset($carrito['car_estado']);
			unset($carrito['car_fecha']);
			$carrito['pe_observaciones_empresa'] = $carrito['car_observaciones_empresa'];
			$carrito['pe_observaciones_proveedor'] = $carrito['car_observaciones_proveedor'];
			unset($carrito['car_observaciones_empresa']);
			unset($carrito['car_observaciones_proveedor']);
			unset($carrito['car_subtotal']);
		}
		
		$carrito['pe_fecha'] = fechaHora_actual($this->session->userdata('su_zona_horaria'));
		$carrito['pe_estado'] = 0;
		
		$this->db->insert('pedidos_proveedores', $carrito);
		$pe_id = $this->db->insert_id();
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
			$product_cantidad = explode(';',$carrito['product_cantidad']);
			$product_cantidad = array_filter($product_cantidad);
			
			$product_id = explode(';',$carrito['product_id']);
			$product_id = array_filter($product_id);
					
			//VARIABLES AUXILIARES PARA LOS EMAILS
			$product_cantidad_aux = array();
			$product_nombre_aux = array();
			$product_referencia_aux = array();
			$product_caracteristicas_aux = array();
			$objCorreos = array();					
					
			//RECORREMOS TODOS LOS PROVEEDORES DEL CARRITO
			for ($i=0; $i<count($pro_id); $i++) {
				//RECOGEMOS LOS DATOS DEL PRODUCTO
				$producto = $this->get_producto($product_id[$i]);
				
				//AÑADIMOS EL PRODUCTO A LA LISTA PARA EL EMAIL
				array_push($product_cantidad_aux,$product_cantidad[$i]);
				array_push($product_nombre_aux,$producto->product_nombre);
				array_push($product_referencia_aux,$producto->product_referencia);
				
				//CARACTERISTICAS DEL PRODUCTO
				$caracteristicas = $producto->product_caracteristicas;
				$caracteristicas = explode(';',$caracteristicas);
				$caracteristicas = array_filter($caracteristicas);
				$opcion_aux = '';
				for ($y=0; $y<count($caracteristicas); $y++) {
					$aux = explode('/',$caracteristicas[$y]);
					$aux = array_filter($aux);
					//OCULTAMOS EL PRECIO DE VENTA AL PROVEEDOR
					if (strpos(strtolower($aux[0]), 'tarifa') === FALSE) {
						$opcion_aux = $opcion_aux.$aux[0].': '.$aux[1].', ';
					}
					//$opcion_aux = $opcion_aux.$aux[0].': '.$aux[1].', ';
				}
				$opcion_aux = substr($opcion_aux, 0, -2);
				array_push($product_caracteristicas_aux, $opcion_aux);
				
				//PREGUNTAMOS SI EL SIGUIENTE PROVEEDOR ES IGUAL O NO PARA ENVIAR YA EL EMAIL
				$enviar = false;
				if (isset($pro_id[$i+1])) {
					if ($pro_id[$i] != $pro_id[$i+1]) {
						$enviar = true;
					}
				} else {
					$enviar = true;
				}
				
				if ($enviar) {
					//EXTRAEMOS EL EMAIL DEL PROVEEDOR
					$proveedor = $this->get_proveedor($pro_id[$i]);
					
					//EXTRAEMOS DATOS DE LA EMPRESA
					$this->load->model('M_empresa');
					$empresa = $this->M_empresa->get_empresa($this->session->userdata('em_id'));
					
					//EXTRAEMOS EL NOMBRE DEL EMPLEADO
					$this->load->model('M_empleados');
					$empleado = $this->M_empleados->get_empleado_id($this->session->userdata('emp_id'));
					
					$nombre = "=?UTF-8?B?".base64_encode($this->session->userdata('em_nombre'))."=?=";
					$list = array($empresa->em_email_contabilidad,$empresa->em_email_opcional,$empresa->em_email_opcional2);
					
					//GENERAMOS EL EMAIL
					/*$this->load->library('email');
					$this->email->from(EMAIL, $nombre);
					$this->email->to($proveedor->pro_email);
					$this->email->cc($proveedor->pro_emails);
					$this->email->bcc($list);
					$this->email->subject(lang('pedido.carrito') . ' - ' . $proveedor->pro_nombre . ' (' . lang('pedido.nro') . ' ' . $pe_id . ')');*/
					
					$productos = array(
						'nombre' => $product_nombre_aux,
						'cantidad' => $product_cantidad_aux,
						'referencia' => $product_referencia_aux,
						'caracteristicas' => $product_caracteristicas_aux,
						'nombre_proveedor' => $proveedor->pro_nombre,
						'nombre_empleado' => $empleado->emp_nombre,
						'apellido1_empleado' => $empleado->emp_apellido1,
						'apellido2_empleado' => $empleado->emp_apellido2,
						'fecha' => $carrito['pe_fecha'],
						'observaciones' => $carrito['pe_observaciones_proveedor'],
						'pe_id' => $pe_id
					);

					$objCorreos[] = array(
						'proveedor' => $proveedor,
						'empresa' => $empresa,
						'empleado' => $empleado,
						'carrito' => $carrito,
						'productos' => $productos,
						'datos_correo' => array(
							'from' => $nombre,
							'to' => $proveedor->pro_email,
							'cc' => $proveedor->pro_emails,
							'bbc' => $list,
							'subject' => lang('pedido.carrito') . ' - ' . $proveedor->pro_nombre . ' (' . lang('pedido.nro') . ' ' . $pe_id . ')'
						)
					);
					
					/*$data = array(
						'BODY' => $this->load->view('emails/v_email_procesar_pedido', $productos, TRUE)
					);
					
					$email = $this->load->view('emails/v_email', $data, TRUE);
					$this->email->message($email);
					$this->email->send();*/
					
					unset($product_cantidad_aux);
					unset($product_nombre_aux);
					unset($product_referencia_aux);
					unset($product_caracteristicas_aux);
					$product_cantidad_aux = array();
					$product_nombre_aux = array();
					$product_referencia_aux = array();
					$product_caracteristicas_aux = array();
				}
			}
					
			//DESTRUIMOS EL CARRITO
			$this->load->library('Cart');
			$this->cart->destroy();
	    	
	    	$arrDatos['datos'] = $objCorreos;
	    	$arrDatos['status'] = TRUE;
	    	return $arrDatos;
		}
	}
	
	//DEVOLVEMOS EL PEDIDO SEGUN SI PR_ID (PRSUPUESTO)
	public function get_pedidos_presupuesto($pr_id) {
		$pedido = $this->db->from('pedidos_proveedores')
			->where('pr_id',$pr_id)
			->where('em_id',$this->session->userdata('em_id'))
			->get()->result();
			
		return $pedido;
	}

	//ACTUALIZAMOS ADJUNTO EN TABLA CARRITO
	public function set_adjuntos_carrito($carrito) {
		$this->db->trans_begin();
		$data = array(
			'car_adjuntos' => $carrito['adjuntos']
		);
		
		//GUARDAMOS EL ID DEL CARRITO
		$car_id = $carrito['car_id'];
	
		$this->db->where('car_id', $car_id);
		$this->db->update('carritos',$data); 
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
			return true;
		}
	}

	public function set_adjuntos_pedido($pedido) {
		$this->db->trans_begin();

		$data = array(
			'pe_adjunto1' => $pedido['pe_adjunto1']
		);
		
		//GUARDAMOS EL ID DEL PEDIDO
		$pe_id = $pedido['pe_id'];
	
		$this->db->where('pe_id', $pe_id);
		$this->db->update('pedidos_proveedores',$data); 
		
		if ($this->db->trans_status() === FALSE) {
	    	$this->db->trans_rollback();
	    	return false;
		} else {
	    	$this->db->trans_commit();
			return true;
		}
	}
}

/* End of file M_clientes.php */
/* Location: ./application/controllers/M_clientes.php */