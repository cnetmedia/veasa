<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bitacora extends CI_Model {
	
	function __construct() {
        parent::__construct();
        $this->tabla = 'bitacora';
    }
    
    //DEVUELVE LOS REGISTROS DE LA BITACORA
	public function get_lista_bitacora($param) {	
		$sql = "select bi.*,emp.emp_nombre,emp.emp_apellido1,emp.emp_apellido2 
			FROM ".$this->tabla." bi  
			JOIN empleados emp ON emp.emp_id = bi.emp_id ";			

		if(!empty($param['bi_idasociado'])){
			$sql .= " WHERE bi.bi_idasociado = ".$param['bi_idasociado'] . 
			" AND bi.bi_tipo = '".$param['bi_tipo'] . "'";			
		}

		$sql .= " ORDER BY bi_id DESC";

		$bitacora_taller = $this->db->query($sql);
		
		return $bitacora_taller->result();
    }

    public function set_bitacora($datos){
		$data = array( 
	        'bi_tipo'		=>  $datos['tipo'], 
	        'bi_idasociado'	=>  $datos['asociado'], 
	        'emp_id'		=>  $this->session->userdata('emp_id'),
	        'bi_accion'		=>  $datos['accion'],
	    );
		$this->db->insert('bitacora', $data);
	}
}