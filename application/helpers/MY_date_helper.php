<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//OBTENEMOS LA HORA PASANDOLE LA ZONA HORARIA
function fechaHora_actual($zonaHoraria) {
	//Asignamos CodeIgniter dentro de una variable con get_instance(). 
	$CI = & get_instance();
	//COMPROBAMOS SI ES HORARIO DE VERANO
	$esVerano = date('I', now());
	//FECHA ACTUAL EN GMT
	$fechaGMTUnix = now();
	//CONVERTIMOS LA FECHA ACTUAL GMT A LOCAL A PARTIR DEL CODIGO DE ZONA HORARIA
	$fechaLocalUnix = gmt_to_local($fechaGMTUnix, $zonaHoraria, $esVerano);
	//FORMATO ESPA�OL (dd/mm/yyyy HH:mm:ss)
	$fechaLocalFormateada = mdate("%Y-%m-%d %H:%i:%s", $fechaLocalUnix);
	//devolvemos el resultado
	return $fechaLocalFormateada;
}

//OBTENEMOS LA HORA PASANDOLE LA HORA ACTUAL UNIX Y ZONA HORARIA
function convertir_fecha_gmt_a_local($fechaHoraGMT, $zonaHoraria) {
	//Asignamos CodeIgniter dentro de una variable con get_instance(). 
	$CI = & get_instance();
	//COMPROBAMOS SI ES HORARIO DE VERANO
	$esVerano = date('I', $fechaHoraGMT);
	//CONVERTIMOS LA FECHA ACTUAL GMT A LOCAL A PARTIR DEL CODIGO DE ZONA HORARIA
	$fechaLocalUnix = gmt_to_local($fechaHoraGMT, $zonaHoraria, $esVerano);
	//FORMATO ESPA�OL (dd/mm/yyyy HH:mm:ss)
	$fechaLocalFormateada = mdate("%Y-%m-%d %H:%i:%s", $fechaLocalUnix);
	//devolvemos el resultado
	return $fechaLocalFormateada;
}


/*echo fechaHora_actual('UP1');
echo '<br>';
echo convertir_fecha_gmt_a_local(now(),'UP1');*/